package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String BRANCH_ID = "";
    public static final String ORGANIZATION_ID = "21";
    public static final String DIRECTORYNAME = "/.BHARTIDB/";
    public static final String DATABASENAME = "BHARTIDB";
    public static final String CRASH_REPORT_KEY = "23de233b";
    public static final String MIXPANEL_PROJECT_TOKEN = "16f7dae6e85ddca4b2db2be433cad865";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
    public static final String APP_VERSION = "1.9";
    public static final boolean SHOW_GALLERY = true;
}
