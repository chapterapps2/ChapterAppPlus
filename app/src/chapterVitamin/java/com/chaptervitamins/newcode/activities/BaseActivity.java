package com.chaptervitamins.BaseCustomView;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by Tanuj Pand. on 04-10-2017.
 */

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        setTheme(R.style.AppTheme_Airtel);
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        setStatusbarColor();
    }

    private void setStatusbarColor() {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                    if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1())) {
                        window.setStatusBarColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color1()));
                    } else
                        window.setStatusBarColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color1()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setToolbarColor() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        /*try {
            if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())) {
                if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2())) {
                    toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(WebServices.mLoginUtility.getBranch_color2())));
                } else
                    toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        setToolbarColor();
    }

   /* @Override
    public Resources getResources() {
        if (res == null) {
            res = new Res(super.getResources());
        }
        return res;
    }*/
}
