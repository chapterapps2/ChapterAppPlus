package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String BRANCH_ID = "1";
    public static final String ORGANIZATION_ID = "5";
    public static final String DIRECTORYNAME = "/.EBIXDB/";
    public static final String DATABASENAME = "EBIXDB";
    public static final String CRASH_REPORT_KEY = "b8570fa3";
    public static final String MIXPANEL_PROJECT_TOKEN = "4e99b6de90a36adbed5470e95be01efe";
    public static final String BASE = "http://trainings.ebixcash.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
    public static final String APP_VERSION = "1.3";
    public static final boolean SHOW_GALLERY = true;
}
