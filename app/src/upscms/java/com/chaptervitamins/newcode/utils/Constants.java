package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "33";
    public static final String DIRECTORYNAME = "/.upsc/";
    public static final String DATABASENAME = "upscDB";
    public static final String CRASH_REPORT_KEY = "47a5ec0b";
    public static final String MIXPANEL_PROJECT_TOKEN="f03cedda51d0f659f90dc4efd8af670d";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
    public static final String BRANCH_ID = "45";
    public static final String APP_VERSION = "1.2.0";
    public static final boolean SHOW_GALLERY = true;
}
