package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "34";
    public static final String DIRECTORYNAME = "/.ChapterVitaminsDB/";
    public static final String DATABASENAME = "ChapterVitaminsDB";
    public static final String CRASH_REPORT_KEY = "746d11e8";
    public static final String MIXPANEL_PROJECT_TOKEN="20860d23c829bc0190a83e7a01b7af37";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static final String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "NO";
    public static final int LOCATION_PERM = 1001;
    public static final String BRANCH_ID = "57";
    public static final String APP_VERSION = "1.0";
    public static final boolean SHOW_GALLERY = true;
    public static final String Second_ORGANIZATION_ID = "26";
    public static final String Second_BRANCH_ID = "40";
}
