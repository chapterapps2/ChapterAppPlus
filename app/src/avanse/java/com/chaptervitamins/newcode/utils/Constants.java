package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "38";
    public static final String DIRECTORYNAME = "/.Avanse/";
    public static final String DATABASENAME = "AvanseDB";
    public static final String CRASH_REPORT_KEY = "47a5ec0b";
    public static final String MIXPANEL_PROJECT_TOKEN="95f33c65d63a59e03407ac63dd0e30bd";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
    public static final String BRANCH_ID = "56";
    public static final String APP_VERSION = "1.0";
    public static final boolean SHOW_GALLERY = true;
}
