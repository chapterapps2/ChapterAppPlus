package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "1";
    public static final String BRANCH_ID = "";
    public static final String DIRECTORYNAME = "/.CentumDB/";
    public static final String DATABASENAME = "CentumDB";
    public static final String CRASH_REPORT_KEY = "649d227a";//649d227a
    public static final String MIXPANEL_PROJECT_TOKEN="a5545d98fd380359957da45a961b3b1a";
    public static final String BASE = "http://www.centumlearnpro.com/";
    public static String OTP_RECEIVER_STRING = "-CLLPRO";
    public static final String SHOW_UPDATE = "yes";
    public static final boolean SHOW_GALLERY = false;
}
