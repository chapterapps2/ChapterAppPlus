package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "13";
    public static final String DIRECTORYNAME = "/.data/";
    public static final String DATABASENAME = "MFCDB";
    public static final String CRASH_REPORT_KEY = "4d68cf61";
    public static final String MIXPANEL_PROJECT_TOKEN = "18f1bc782b942928e30089cda7782230";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
    public static final String BRANCH_ID = "";
    public static final String APP_VERSION = "1.0";
    public static final boolean SHOW_GALLERY = true;
}
