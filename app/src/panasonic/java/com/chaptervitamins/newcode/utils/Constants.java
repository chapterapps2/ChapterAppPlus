 package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "1";
    public static final String BRANCH_ID = "25";
    public static final String DIRECTORYNAME = "/.PanasonicDB/";
    public static final String DATABASENAME = "PanasonicDB";
    public static final String CRASH_REPORT_KEY = "8998d911";
    public static final String MIXPANEL_PROJECT_TOKEN = "f2a785b56c63f9019c2270c9c8a4411d";
    public static final String BASE = "http://www.centumlearnpro.com/";
    public static String OTP_RECEIVER_STRING = "-CLLPRO";
    public static final String SHOW_UPDATE = "yes";
    public static final boolean SHOW_GALLERY = false;
    public static final String APP_VERSION = "1.2.1";
}