package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "25";
    public static final String BRANCH_ID = "32";
    public static final String DIRECTORYNAME = "/.FIIBDB/";
    public static final String DATABASENAME = "FIIBDB";
    public static final String CRASH_REPORT_KEY = "6649d227a";//6649d227a
    public static final String MIXPANEL_PROJECT_TOKEN="7e976b18eef8569fa2660e045dc1cefc";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CLLPRO";
    public static final String SHOW_UPDATE = "yes";
    public static final String APP_VERSION = "1.0";
    public static final boolean SHOW_GALLERY = true;
}
