package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "13";
    public static final String DIRECTORYNAME = "/.fg/";
    public static final String DATABASENAME = "FGDataBase";
    public static final String CRASH_REPORT_KEY = "02490b66";
    public static final String MIXPANEL_PROJECT_TOKEN = "d486ea18937e98a45617ffcd8a12898a";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
}
