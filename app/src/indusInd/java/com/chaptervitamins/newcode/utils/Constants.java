package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "4";
    public static final String DIRECTORYNAME = "/.IndusIndDB/";
    public static final String DATABASENAME = "IndusIndDB";
    public static final String CRASH_REPORT_KEY = "86e5dd0e";
    public static final String MIXPANEL_PROJECT_TOKEN="d6c24d5cd139a0f25adbc29a9209b940";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
}
