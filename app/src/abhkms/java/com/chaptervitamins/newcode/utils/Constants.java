package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String ORGANIZATION_ID = "3";
    public static final String DIRECTORYNAME = "/.DCBDB/";
    public static final String DATABASENAME = "DCBDB";
    public static final String CRASH_REPORT_KEY = "b8570fa3";
    public static final String MIXPANEL_PROJECT_TOKEN = "5f520ca374518de12eac913a6025fc92";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
}
