package com.chaptervitamins.newcode.utils;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class Constants {
    public static final String BRANCH_ID = "";
    public static final String ORGANIZATION_ID = "2";
    public static final String DIRECTORYNAME = "/.MAXDB/";
    public static final String DATABASENAME = "MAXDB";
    public static final String CRASH_REPORT_KEY = "aa3c66cb";//649d227a
    public static final String MIXPANEL_PROJECT_TOKEN="e3860ccb8d191b2e975b02d218f56d86";
    public static final String BASE = "https://cloudlms.chaptervitamins.com/";
    public static String OTP_RECEIVER_STRING = "-CHAPTR";
    public static final String SHOW_UPDATE = "yes";
    public static final String APP_VERSION = "1.9";
    public static final boolean SHOW_GALLERY = true;
}
