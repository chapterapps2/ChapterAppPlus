package com.chaptervitamins.myprofile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.CustomView.Custom_Progress;
import com.chaptervitamins.CustomView.WordUtils;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DashBoardActivity extends BaseActivity {
    private ImageView profile_image, back;
    private DataBase dataBase;
    private Bitmap imageBitmap;
    String ImageDecode;
    private TextView toolBoxTitle;
    private TextView profile_name, tv_accessed_material, profile_Designation, total_module_txt, total_material_txt, total_accessed_txt, total_att_quiz_txt, total_quiz_txt, total_comp_quiz_txt, tv_click_here;
    private WebServices webServices;
    Custom_Progress donutProgress;
    private int TotalModules = 0, TotalMaterials = 0, TotalMaterialsAccessed = 0, TotalQuiz = 0, TotalQuizCompleted = 0, TotalQuizAttempted = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dash_board);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        back = (ImageView) findViewById(R.id.back);
        profile_name = (TextView) findViewById(R.id.profile_name);
        toolBoxTitle = (TextView) findViewById(R.id.toolbar_title);
        profile_Designation = (TextView) findViewById(R.id.profile_Designation);
        tv_accessed_material = (TextView) findViewById(R.id.tv_accessed_material);
        donutProgress = (Custom_Progress) findViewById(R.id.donut_progress);
        total_module_txt = (TextView) findViewById(R.id.total_module_txt);
        tv_click_here = (TextView) findViewById(R.id.tv_click_here);
        total_material_txt = (TextView) findViewById(R.id.total_material_txt);
        total_accessed_txt = (TextView) findViewById(R.id.total_accessed_txt);
        total_quiz_txt = (TextView) findViewById(R.id.total_quiz_txt);
        total_comp_quiz_txt = (TextView) findViewById(R.id.total_comp_quiz_txt);
        total_att_quiz_txt = (TextView) findViewById(R.id.total_att_quiz_txt);
        if (Utils.getColorPrimary() != 0) {
            Drawable drawable = createShapeByColor(getResources().getColor(R.color.white), 270, 2, Utils.getColorPrimary());
            total_material_txt.setBackground(drawable);
            total_accessed_txt.setBackground(drawable);
        }
        total_module_txt.setText(TotalModules + "");
        total_material_txt.setText(TotalMaterials + "");
        total_accessed_txt.setText(TotalMaterialsAccessed + "");
        total_quiz_txt.setText(TotalQuiz + "");
        total_comp_quiz_txt.setText(TotalQuizCompleted + "");
        total_att_quiz_txt.setText(TotalQuizAttempted + "");
        donutProgress.setStartingDegree(270);
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("38") &&
                Constants.BRANCH_ID.equalsIgnoreCase("56")) {
            toolBoxTitle.setText("Learning Snapshot");
        }else{
            toolBoxTitle.setText("DashBoard");
        }

        tv_click_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, UnseenMaterialActivity.class);
                startActivity(intent);
            }
        });

        dataBase = DataBase.getInstance(DashBoardActivity.this);
        webServices = new WebServices();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String fname = WordUtils.capitalize(WebServices.mLoginUtility.getFirstname());
        profile_name.setText(fname);

        profile_Designation.setText(WebServices.mLoginUtility.getBranch_name());
        SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
        /*if (WebServices.isNetworkAvailable(DashBoardActivity.this)) {
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (imageBitmap != null) {
                        profile_image.setImageBitmap(imageBitmap);
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {
                    imageBitmap = webServices.getImage(WebServices.mLoginUtility.getPhoto());
                    if (imageBitmap != null)
                        dataBase.updateProfileImageData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), imageBitmap);
                    handler.sendEmptyMessage(0);
                }
            }.start();

        } else if (dataBase.getProfileImage(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", "")) != null) {
            Bitmap bitmap = dataBase.getProfileImage(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""));
            profile_image.setImageBitmap(bitmap);

        }*/

        String profilePicUrl = WebServices.mLoginUtility.getPhoto();
        if (!TextUtils.isEmpty(profilePicUrl) && !profilePicUrl.contains("no-image"))
            Picasso.with(this).load(profilePicUrl).placeholder(R.drawable.profile).error(R.drawable.profile).into(profile_image);
        else
            setDefaultImage();

        for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
            if (HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) || HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.CourseType.SURVEY)) {
                TotalModules = TotalModules + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size();
                for (int j = 0; j < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size(); j++) {
                    TotalMaterials = TotalMaterials + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size();
                    ArrayList<String> materialids = new ArrayList<>();
                    for (int k = 0; k < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size(); k++) {
                        MeterialUtility utility = HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k);
                        if (utility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                            TotalQuiz = TotalQuiz + 1;
                        }
//                        if(utility.getIsComplete()){
//                            TotalMaterialsAccessed = TotalMaterialsAccessed + 1;
//                            materialids.add(utility.getMaterial_id());
//                        }

                        for (int Tk = 0; Tk < HomeActivity.mReadResponse.size(); Tk++) {
//                            if (HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getCourse_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getMaterial_id())) {
                                if (!materialids.contains(utility.getMaterial_id())) {
                                    materialids.add(utility.getMaterial_id());
                                    TotalMaterialsAccessed = TotalMaterialsAccessed + 1;
//                                    }
                                }
                            }
                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getCourse_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getCourse_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    TotalQuizAttempted = TotalQuizAttempted + 1;
                                    break;
                                }
//                                }
                            }
                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    System.out.println("======MultichoiceActivity.mReadResponse.get(k1).getResult()===" + HomeActivity.mReadResponse.get(k1).getResult());
                                    if (!utility.getPassing_percentage().equalsIgnoreCase("") && utility.getPassing_percentage() != null && !HomeActivity.mReadResponse.get(k1).getResult().equalsIgnoreCase("") && HomeActivity.mReadResponse.get(k1).getResult() != null)
                                        if (Float.parseFloat(HomeActivity.mReadResponse.get(k1).getResult()) >= Float.parseFloat(utility.getPassing_percentage())) {
                                            TotalQuizCompleted = TotalQuizCompleted + 1;
                                            break;
                                        }
                                }
                            }
//                            }
                        }
                    }
                }
            }
        }

        total_module_txt.setText(TotalModules + "");
        total_material_txt.setText((TotalMaterials) + "");
        total_accessed_txt.setText((TotalMaterialsAccessed) + "");
        total_quiz_txt.setText(TotalQuiz + "");
        total_comp_quiz_txt.setText(TotalQuizCompleted + "");
        total_att_quiz_txt.setText(TotalQuizAttempted + "");

        int per = (int) (((double) TotalMaterialsAccessed / (double) TotalMaterials) * 100);
        String styledText = "This is <font color='red'>simple</font>.";
        String accessPerMsg = "You have accessed " + "<font color='red' size='25'>" + per + "%</font>" + " of the materials assigned to you.";
        tv_accessed_material.setText(Html.fromHtml(accessPerMsg + " " + getMesggae(per)));
//        tv_accessed_material.setText(Html.fromHtml(styledText));
        if (per < 100)
            tv_click_here.setText(Html.fromHtml("Please " + "<font color='red' size ='25'>click here</font> to see the materials you have not seen."));
        else
            tv_click_here.setVisibility(View.GONE);
        int percentage = 0;
        if (TotalQuizAttempted != 0 && TotalQuiz != 0) {
            percentage = ((TotalQuizAttempted * 100) / TotalQuiz);
        }
        donutProgress.setProgress(percentage);
    }

    private String getMesggae(double score) {
        if (score >= 0 && score <= 20)
            return "You are a low usage user.";
        else if (score >= 21 && score <= 50)
            return "You are a moderate usage user.";
        else if (score >= 51 && score <= 70)
            return "You are a good usage user.";
        else
            return "You are a excellent usage user.";
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        TotalModules = 0;
        TotalMaterials = 0;
        TotalMaterialsAccessed = 0;
        TotalQuiz = 0;
        TotalQuizCompleted = 0;
        TotalQuizAttempted = 0;

        for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
            if (HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) || HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.CourseType.SURVEY)) {
                TotalModules = TotalModules + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size();
                for (int j = 0; j < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size(); j++) {
                    TotalMaterials = TotalMaterials + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size();
                    ArrayList<String> materialids = new ArrayList<>();
                    for (int k = 0; k < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size(); k++) {
                        MeterialUtility utility = HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k);
                        if (utility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                            TotalQuiz = TotalQuiz + 1;
                        }
                        for (int Tk = 0; Tk < HomeActivity.mReadResponse.size(); Tk++) {
//                            if (HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getCourse_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getMaterial_id())) {
                                if (!materialids.contains(utility.getMaterial_id())) {
                                    materialids.add(utility.getMaterial_id());
                                    TotalMaterialsAccessed = TotalMaterialsAccessed + 1;
//                                    }
                                }
                            }
                        }
//                        if(utility.getIsComplete()){
//                            TotalMaterialsAccessed = TotalMaterialsAccessed + 1;
//                            materialids.add(utility.getMaterial_id());
//                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getCourse_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getCourse_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    TotalQuizAttempted = TotalQuizAttempted + 1;
                                    break;
                                }
//                                }
                            }
                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    System.out.println("======MultichoiceActivity.mReadResponse.get(k1).getResult()===" + HomeActivity.mReadResponse.get(k1).getResult());
                                    if (!utility.getPassing_percentage().equalsIgnoreCase("") && utility.getPassing_percentage() != null && !HomeActivity.mReadResponse.get(k1).getResult().equalsIgnoreCase("") && HomeActivity.mReadResponse.get(k1).getResult() != null)
                                        if (Float.parseFloat(HomeActivity.mReadResponse.get(k1).getResult()) >= Float.parseFloat(utility.getPassing_percentage())) {
                                            TotalQuizCompleted = TotalQuizCompleted + 1;
                                            break;
                                        }
                                }
                            }
//                            }
                        }
                    }
                }
            }
        }

        total_module_txt.setText(TotalModules + "");
        total_material_txt.setText(TotalMaterials + "");
        total_accessed_txt.setText(TotalMaterialsAccessed + "");
        total_quiz_txt.setText(TotalQuiz + "");
        total_comp_quiz_txt.setText(TotalQuizCompleted + "");
        total_att_quiz_txt.setText(TotalQuizAttempted + "");

        int per = (int) (((double) TotalMaterialsAccessed / (double) TotalMaterials) * 100);
        String styledText = "This is <font color='red'>simple</font>.";
        String accessPerMsg = "You have accessed " + "<font color='red' size='25'>" + per + "%</font>" + " of the materials assigned to you.";
        tv_accessed_material.setText(Html.fromHtml(accessPerMsg + " " + getMesggae(per)));
//        tv_accessed_material.setText(Html.fromHtml(styledText));
        if (per < 100)
            tv_click_here.setText(Html.fromHtml("Please " + "<font color='red' size ='25'>click here</font> to see the materials you have not seen."));
        else
            tv_click_here.setVisibility(View.GONE);
        int percentage = 0;
        if (TotalQuizAttempted != 0 && TotalQuiz != 0) {
            percentage = ((TotalQuizAttempted * 100) / TotalQuiz);
        }
        donutProgress.setProgress(percentage);
    }
}
