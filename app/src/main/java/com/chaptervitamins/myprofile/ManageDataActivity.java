package com.chaptervitamins.myprofile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

public class ManageDataActivity extends BaseActivity {
    private TextView nodata_txt;
    private ListView listview;
    private ImageView ivDelete;
    private CheckBox select_chk;
    private ArrayList<MeterialUtility> alOfflineMaterials;
    private DataBase dataBase;
    private LayoutInflater mInflater;
    private ListAdapter adapter;
    private ImageView back;
    private boolean row_chk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_material_list);
        alOfflineMaterials = new ArrayList<>();
        nodata_txt = (TextView) findViewById(R.id.nodata_txt);
        listview = (ListView) findViewById(R.id.listview);
        ivDelete = (ImageView) findViewById(R.id.iv_delete);
        select_chk = (CheckBox) findViewById(R.id.select_chk);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dataBase = DataBase.getInstance(ManageDataActivity.this);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getOfflineContents();
        if (alOfflineMaterials.size() != 0) {
            nodata_txt.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            adapter = new ListAdapter();
            listview.setAdapter(adapter);
            select_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!row_chk) {
                        for (int i = 0; i < alOfflineMaterials.size(); i++) {
                            alOfflineMaterials.get(i).setSelected(isChecked);
                        }
                        enableDelete();
                    }
                    adapter.notifyDataSetChanged();
                }
            });

        } else {
            nodata_txt.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        }
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_Dialog();
            }
        });
    }

    private void delete_Dialog() {
        final Dialog dialog = new Dialog(ManageDataActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_dialog);
        TextView title_dialog = (TextView) dialog.findViewById(R.id.title_dialog);
        title_dialog.setText("Delete");
        TextView desc_dialog = (TextView) dialog.findViewById(R.id.desc_dialog);
        desc_dialog.setText("Are you sure you want to delete the selected item(s)?");
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                final ProgressDialog progressDialog = ProgressDialog.show(ManageDataActivity.this, "", "Deleting...");
                progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (progressDialog != null) progressDialog.dismiss();
                        Toast.makeText(ManageDataActivity.this, "Item(s) deleted successfully!", Toast.LENGTH_LONG).show();
                        ivDelete.setVisibility(View.INVISIBLE);
                        select_chk.setChecked(false);
                        row_chk = true;
                        if (alOfflineMaterials.size() != 0)
                            adapter.notifyDataSetChanged();
                        else {
                            nodata_txt.setVisibility(View.VISIBLE);
                            listview.setVisibility(View.GONE);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        for (int i = 0; i < alOfflineMaterials.size(); i++) {
                            if (alOfflineMaterials.get(i).isSelected())
                                Utils.deleteFile(ManageDataActivity.this, alOfflineMaterials.get(i), dataBase);
                        }
                        getOfflineContents();
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        });
        dialog.show();
    }

    private void getOfflineContents() {
        alOfflineMaterials.clear();
        ArrayList<MeterialUtility> alMaterials = FlowingCourseUtils.getAllMaterials();
        for (int i = 0; i < alMaterials.size(); i++) {
            MeterialUtility meterialUtility = alMaterials.get(i);
            if(Constants.ORGANIZATION_ID.equalsIgnoreCase("38")&&Constants.BRANCH_ID.
                    equalsIgnoreCase("56")) {
                if (!meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.FLASHCARD)) {
                    if (Utils.checkDownloadStatus(ManageDataActivity.this, meterialUtility, dataBase)) {
                        meterialUtility.setSelected(false);
                        alOfflineMaterials.add(meterialUtility);
                    }
                }
            }else{
                if (Utils.checkDownloadStatus(ManageDataActivity.this, meterialUtility, dataBase)) {
                    meterialUtility.setSelected(false);
                    alOfflineMaterials.add(meterialUtility);
                }
            }
        }
    }


    class ListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return alOfflineMaterials.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.offline_material_list_row, null);
                ImageView type_img = (ImageView) convertView.findViewById(R.id.type_img);
                TextView material_name_txt = (TextView) convertView.findViewById(R.id.material_name_txt);
                TextView material_size_txt = (TextView) convertView.findViewById(R.id.material_size_txt);
                final CheckBox select_material_chk = (CheckBox) convertView.findViewById(R.id.select_material_chk);
                LinearLayout row_ll = (LinearLayout) convertView.findViewById(R.id.row_ll);
                final MeterialUtility meterialUtility = alOfflineMaterials.get(position);
                material_name_txt.setText(meterialUtility.getTitle());
                if (!TextUtils.isEmpty(meterialUtility.getFile_size()) && !meterialUtility.getFile_size().equals("0") &&
                        !meterialUtility.getFile_size().equals("0.0")) {
                    material_size_txt.setText(meterialUtility.getFile_size() + "MB");
                }else
                    material_size_txt.setVisibility(View.GONE);
                select_material_chk.setChecked(meterialUtility.isSelected());

                Utils.setMaterialImage(ManageDataActivity.this, meterialUtility, type_img);
                select_material_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        meterialUtility.setSelected(isChecked);
                        enableDelete();
                    }
                });
                row_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (meterialUtility.isSelected()) {
                            meterialUtility.setSelected(false);
                            select_material_chk.setChecked(false);
                        } else {
                            meterialUtility.setSelected(true);
                            select_material_chk.setChecked(true);
                        }
                        enableDelete();
                    }
                });
            }
            return convertView;
        }
    }

    private void enableDelete() {
        row_chk = false;
        boolean isSelected = false;
        for (int i = 0; i < alOfflineMaterials.size(); i++) {
            if (alOfflineMaterials.get(i).isSelected()) {
                isSelected = true;
                break;
            }
        }
        if (isSelected) ivDelete.setVisibility(View.VISIBLE);
        else ivDelete.setVisibility(View.INVISIBLE);
        allSelected();
    }

    private void allSelected() {
        boolean isSelected = true;
        for (int i = 0; i < alOfflineMaterials.size(); i++) {
            if (!alOfflineMaterials.get(i).isSelected()) {
                isSelected = false;
                row_chk = true;
                break;
            }
        }
        if (isSelected) select_chk.setChecked(true);
        else select_chk.setChecked(false);
    }
}
