package com.chaptervitamins.myprofile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 08-09-2017.
 */

public class UnseenMaterialAdapter extends RecyclerView.Adapter<UnseenMaterialAdapter.ViewHolder> {

    private ArrayList<MeterialUtility> assSummaryModels;
    private Context mContext;

    public UnseenMaterialAdapter(ArrayList<MeterialUtility> assSummaryModels) {
        this.assSummaryModels = assSummaryModels;
    }

    @Override
    public UnseenMaterialAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_unseen_layout, parent, false);

        return new UnseenMaterialAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UnseenMaterialAdapter.ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            MeterialUtility meterialUtility = assSummaryModels.get(position);
            if(meterialUtility!=null) {
                holder.tv_material_name.setText(meterialUtility.getTitle());
                Utils.setMaterialImage(mContext,meterialUtility,holder.imageView);
            }
        }
    }

    @Override
    public int getItemCount() {
        return assSummaryModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_material_name;
        ImageView imageView;
        LinearLayout llMain;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_material_name = (TextView) itemView.findViewById(R.id.tv_material_name);
            llMain = (LinearLayout) itemView.findViewById(R.id.type_ll);
            imageView = (ImageView) itemView.findViewById(R.id.type_img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.showDialog(mContext, "Are you sure to open this material?", new Runnable() {
                        @Override
                        public void run() {
                            FlowingCourseUtils.openMaterial(mContext, assSummaryModels.get(getAdapterPosition()).getMaterial_id(), false);
                        }
                    }, null);

                }
            });


        }
    }
}

