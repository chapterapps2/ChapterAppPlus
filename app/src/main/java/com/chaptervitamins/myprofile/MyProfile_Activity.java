package com.chaptervitamins.myprofile;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.WordUtils;
import com.chaptervitamins.R;
import com.chaptervitamins.Suggestions.File_Uploader;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.home.HistoryFragment;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.customviews.CustomLinearLayoutForFirstColor;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.quiz.ViewCertificateActivity;
import com.chaptervitamins.utility.Certificate_Utils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfile_Activity extends BaseActivity implements UploadListener {
    private ImageView back, statistic_img;
    private CircleImageView profile_image;
    private LinearLayout pic_edit_ll, allcertificate_ll, no_certi_ll, contactadmin_ll;
    private TextView profile_name, tvTotalEarnedCoins;
    private TextView profile_Designation, email_txt, mobile_txt, save_profile_txt, sec_lag_txt, tvUsername;
    private Spinner query_spinner;
    private Button manage_data_btn, quiz_history_btn;
    private Uri file;
    private WebServices webServices;
    private DataBase dataBase;
    private Bitmap imageBitmap;
    String ImageDecode;
    private static int IMG_RESULT = 1;
    Intent intent;
    String[] FILE;
    final int PIC_CROP = 2;
    private String FILENAME = "", SELECTED_LANGUAGE = "";
    private ArrayList<String> languages = new ArrayList<>();
    private LayoutInflater mInflater;
    private Spinner_Adapter adapter;
    private int MEDIA_ID, Selected_Lang = 0;
    private ArrayList<Certificate_Utils> materials_utilses = new ArrayList<>();
    private static final int CAMERA = 101;
    private static final int GALLERY = 102;


    private Dialog mDialog;
    private ProgressBar mBar;
    private TextView totalPer_txt, title_txt, total_file_txt, tvRank, tvRankLabel;
    private Button abort_btn = null;
    private boolean isDownload = true;
    String MSG = "Download Sucessfully!";
    private String FILEPATH = null;
    private int totallenghtOfFile = 0;
    private String totalVal = "-1";

    Handler timeout = null;
    int[] count = {10};//for 40 seconds to wait for buffering after it will finish the activity
    boolean istimeout = true;
    private boolean isUpdated = false;
    private Button btnAssessment, btnModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_profile);
        tvTotalEarnedCoins = (TextView) findViewById(R.id.tv_coins_count);
        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        btnAssessment = (Button) findViewById(R.id.btn_assessment);
        btnModule = (Button) findViewById(R.id.btn_module);
        statistic_img = (ImageView) findViewById(R.id.statistic_img);
        back = (ImageView) findViewById(R.id.back);
        tvRank = (TextView) findViewById(R.id.tv_rank);
        tvRankLabel = (TextView) findViewById(R.id.tv_rank_label);
        tvUsername = (TextView) findViewById(R.id.tv_user_name);
        pic_edit_ll = (LinearLayout) findViewById(R.id.pic_edit_ll);
        contactadmin_ll = (CustomLinearLayoutForFirstColor) findViewById(R.id.contactadmin_ll);
        allcertificate_ll = (LinearLayout) findViewById(R.id.allcertificate_ll);
        no_certi_ll = (LinearLayout) findViewById(R.id.no_certi_ll);
        profile_name = (TextView) findViewById(R.id.profile_name);
        profile_Designation = (TextView) findViewById(R.id.profile_Designation);
        email_txt = (TextView) findViewById(R.id.email_txt);
        mobile_txt = (TextView) findViewById(R.id.mobile_txt);
        save_profile_txt = (TextView) findViewById(R.id.save_profile_txt);
        sec_lag_txt = (TextView) findViewById(R.id.sec_lag_txt);
        query_spinner = (Spinner) findViewById(R.id.query_spinner);
        manage_data_btn = (Button) findViewById(R.id.manage_data_btn);
        quiz_history_btn = (Button) findViewById(R.id.quiz_history_btn);
        quiz_history_btn.setVisibility(View.GONE);
        webServices = new WebServices();
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("38")&&
                Constants.BRANCH_ID.equalsIgnoreCase("56")) {
            btnModule.setVisibility(View.GONE);
            btnAssessment.setVisibility(View.GONE);
        }else{
            btnModule.setVisibility(View.VISIBLE);
            btnAssessment.setVisibility(View.VISIBLE);
        }
        if (Utils.getColorPrimary() != 0) {
            statistic_img.setBackground(createShapeByColor(Utils.getColorPrimary(), 50, 0, Utils.getColorPrimary()));
        }
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (APIUtility.SECOND_LANGUAGE_PREFERENCE.equalsIgnoreCase("") || APIUtility.SECOND_LANGUAGE_PREFERENCE.equalsIgnoreCase("Select Second Language")) {
            APIUtility.SECOND_LANGUAGE_PREFERENCE = "";
            languages.add("Select Second Language");
        }
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getRank()))
            tvRank.setText(WebServices.mLoginUtility.getRank());
        tvTotalEarnedCoins.setText(WebServices.mLoginUtility.getCoins_collected());
        String[] langu = APIUtility.organizationModel.getLanguagePreference().split(",");
        for (int i = 0; i < langu.length; i++) {
            if (!langu[i].equalsIgnoreCase("")) {
                languages.add(langu[i]);
                if (langu[i].equalsIgnoreCase(APIUtility.SECOND_LANGUAGE_PREFERENCE))
                    Selected_Lang = i;
            }
        }
        btnModule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfile_Activity.this, ModuleSummaryActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.btn_coins_earned).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfile_Activity.this, PointsSummaryActivity.class);
                startActivity(intent);
            }
        });
        btnAssessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfile_Activity.this, AssSummaryActivity.class);
                startActivity(intent);
            }
        });
        quiz_history_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfile_Activity.this, HistoryFragment.class);
                startActivity(intent);
            }
        });
        statistic_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfile_Activity.this, DashBoardActivity.class);
                startActivity(intent);
            }
        });
        adapter = new Spinner_Adapter(languages);
        query_spinner.setAdapter(adapter);
        if (!APIUtility.SECOND_LANGUAGE_PREFERENCE.equalsIgnoreCase("")) {
            query_spinner.setSelection(Selected_Lang);
            sec_lag_txt.setText("Current Second Language: " + APIUtility.SECOND_LANGUAGE_PREFERENCE);
        }
        query_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                save_profile_txt.setVisibility(View.VISIBLE);
                SELECTED_LANGUAGE = languages.get(position);
                if (view != null)
                    view.findViewById(R.id.selected_lan_img).setVisibility(View.GONE);
                if (!APIUtility.SECOND_LANGUAGE_PREFERENCE.equalsIgnoreCase("")) {
                    if (!APIUtility.SECOND_LANGUAGE_PREFERENCE.equalsIgnoreCase(SELECTED_LANGUAGE)) {
                        isUpdated = true;
                    }
                } else {
                    if (!"Select Second Language".equalsIgnoreCase(SELECTED_LANGUAGE)) {
                        isUpdated = true;
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        save_profile_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (WebServices.isNetworkAvailable(MyProfile_Activity.this)) {
                    ProfileUpdate();
                } else {
                    Toast.makeText(MyProfile_Activity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });
        dataBase = DataBase.getInstance(MyProfile_Activity.this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUpdated) {
                    SaveChangesDialog();
                } else {
                    finish();
                }
            }
        });
        String fname = WordUtils.capitalize(WebServices.mLoginUtility.getFirstname());
//        String fchar = fname;
//        String lchar = "";
//        if (fchar.length() > 1) {
//            fchar = fname.substring(0, 1).toUpperCase();
//            System.out.println("====" + fchar);
//            lchar = fname.substring(1);
//        }

//        if (WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null")) {

//            profile_name_txt.setText("Hi, " + fchar + lchar + "!");
        profile_name.setText(fname);
       /* } else {
            String flname = WordUtils.capitalize(WebServices.mLoginUtility.getLastname());
//            String flchar = flname;
//            String llchar = "";
//            if (flname.length() > 1) {
//                flchar = flname.substring(0, 1).toUpperCase();
//                System.out.println("====" + flchar);
//                llchar = flname.substring(1);
//            }
//            profile_name_txt.setText("Hi, " + fchar + lchar + " " + flchar + llchar + "!");
            profile_name.setText(fname + " " + flname);
        }*/
        profile_Designation.setText(WebServices.mLoginUtility.getBranch_name());
        email_txt.setText(WebServices.mLoginUtility.getEmail());
        mobile_txt.setText(WebServices.mLoginUtility.getPhone());
        manage_data_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfile_Activity.this, ManageDataActivity.class);
                startActivity(intent);
            }
        });
        pic_edit_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooserDialog();
            }
        });
        contactadmin_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contact_admin = "contact@com.chapterapps.net";
                if (!APIUtility.organizationModel.getAdminEmail().equalsIgnoreCase(""))
                    contact_admin = APIUtility.organizationModel.getAdminEmail();
                String[] bcc = {"contact@com.chapterapps.net"};
                Intent i = new Intent(Intent.ACTION_SENDTO);
//                i.setType("text/plain");

                i.putExtra(Intent.EXTRA_EMAIL, new String[]{contact_admin});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                i.putExtra(Intent.EXTRA_BCC, bcc);
                i.setType("message/rfc822");
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", contact_admin, null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(MyProfile_Activity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (SplashActivity.mPref == null)
            SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getPhoto()) && !WebServices.mLoginUtility.getPhoto().contains("no_image"))
            Picasso.with(this).load(WebServices.mLoginUtility.getPhoto()).placeholder(R.drawable.profile).error(R.drawable.profile).resize(90, 90).into(profile_image);
        else {
            setDefaultImage();
        }
        /*if (WebServices.isNetworkAvailable(MyProfile_Activity.this)) {
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (imageBitmap != null) {
                        int nh = (int) ( imageBitmap.getHeight() * (512.0 / imageBitmap.getWidth()) );
                        Bitmap scaled = Bitmap.createScaledBitmap(imageBitmap, 512, nh, true);
                        profile_image.setImageBitmap(scaled);
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {
                    imageBitmap = webServices.getImage(WebServices.mLoginUtility.getPhoto());
                    if (imageBitmap != null)
                        dataBase.updateProfileImageData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), imageBitmap);
                    handler.sendEmptyMessage(0);
                }
            }.start();

        } else if (dataBase.getProfileImage(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", "")) != null) {
            Bitmap bitmap = dataBase.getProfileImage(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""));
            profile_image.setImageBitmap(bitmap);

        }*/
//        if (!getIntent().getStringExtra("loginscreen").equalsIgnoreCase("")) {
//            profile_ll.setVisibility(View.VISIBLE);
        passInQuiz();
        if (materials_utilses.size() == 0) {
            no_certi_ll.setVisibility(View.VISIBLE);
            allcertificate_ll.removeAllViews();
        } else {
            no_certi_ll.setVisibility(View.GONE);
            allcertificate_ll.removeAllViews();
            for (int i = 0; i < materials_utilses.size(); i++) {
                View convertView = mInflater.inflate(R.layout.certificate_row, null);
                TextView material_name_txt = (TextView) convertView.findViewById(R.id.todo_row_desc);
                TextView todo_row_title = (TextView) convertView.findViewById(R.id.todo_row_title);
                TextView todo_row_date = (TextView) convertView.findViewById(R.id.todo_row_date);
                LinearLayout row_ll = (LinearLayout) convertView.findViewById(R.id.row_ll);

                todo_row_title.setVisibility(View.GONE);
                todo_row_date.setVisibility(View.GONE);
                todo_row_title.setText(materials_utilses.get(i).getMaterial_id());
                material_name_txt.setText(materials_utilses.get(i).getMaterial_name());
                final int pos = i;
                row_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (WebServices.isNetworkAvailable(MyProfile_Activity.this)) {
                            File dir = new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + materials_utilses.get(pos).getMaterial_id() + ".pdf");
                            if (dir.exists()) {
                                Intent intent = new Intent(MyProfile_Activity.this, ViewCertificateActivity.class);
                                intent.putExtra("filename", dir.getAbsoluteFile().getPath());
                                intent.putExtra("matname", materials_utilses.get(pos).getMaterial_name() + ".pdf");
                                startActivity(intent);
                            } else {
                                String url = APIUtility.GENERATE_CERTIFICATE + materials_utilses.get(pos).getMaterial_id() + "/" + WebServices.mLoginUtility.getUser_id();
                                DownloadELearning(MyProfile_Activity.this, url, materials_utilses.get(pos).getMaterial_id() + ".pdf");
                            }
                        } else {
                            Toast.makeText(MyProfile_Activity.this, getResources().getText(R.string.no_internet), Toast.LENGTH_LONG).show();
                        }
                    }
                });
                allcertificate_ll.addView(convertView);
            }
        }

    }


    @Override
    public void onBackPressed() {
        if (isUpdated) {
            SaveChangesDialog();
        } else {
            super.onBackPressed();
        }

    }

    private void ProfileUpdate() {
        final ProgressDialog dialog = ProgressDialog.show(MyProfile_Activity.this, "", "Please wait...");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (dialog != null) dialog.dismiss();
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(MyProfile_Activity.this, APIUtility.UPDATE_PROFILE + APIUtility.LOGIN);
                    return;
                }
                sec_lag_txt.setText("Current Second Language: " + SELECTED_LANGUAGE);
                String[] langu = APIUtility.organizationModel.getLanguagePreference().split(",");
                for (int i = 0; i < langu.length; i++) {
                    if (!langu[i].equalsIgnoreCase("")) {

                        if (langu[i].equalsIgnoreCase(APIUtility.SECOND_LANGUAGE_PREFERENCE))
                            Selected_Lang = i;
                    }
                }
                isUpdated = false;
                Toast.makeText(MyProfile_Activity.this, "Profile Updated Successfully!", Toast.LENGTH_SHORT).show();
                Bitmap bitmap = ((BitmapDrawable) profile_image.getDrawable()).getBitmap();
                if (bitmap != null) {
                    dataBase.updateProfileImageData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), bitmap);
                    tvUsername.setVisibility(View.GONE);
                }
                setResult(RESULT_OK);
                finish();
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("media_id", MEDIA_ID + ""));
                nameValuePair.add(new BasicNameValuePair("second_language_preference", SELECTED_LANGUAGE));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                String resp = webServices.getLogin(nameValuePair, APIUtility.UPDATE_PROFILE);
                if (webServices.isValid(resp)) {
                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("email", WebServices.mLoginUtility.getEmail()));
                    nameValuePair.add(new BasicNameValuePair("password", SplashActivity.mPref.getString("pass", "")));
                    nameValuePair.add(new BasicNameValuePair("push_token", WebServices.mLoginUtility.getDevice().getPush_token()));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
                    nameValuePair.add(new BasicNameValuePair("branch_id", Constants.BRANCH_ID));
                    resp = webServices.getLogin(nameValuePair, APIUtility.LOGIN);
                    Log.d(" Response:", resp.toString());
                    boolean isSuccess = webServices.isLogined(resp);
                    if (isSuccess) {

                        if (dataBase.isData(SplashActivity.mPref.getString("id", ""), "Login"))
                            dataBase.addLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), resp);
                        else {
                            dataBase.updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), resp);
                        }
                    }
                }
                handler.sendEmptyMessage(0);

            }
        }.start();
    }


    private void ChooserDialog() {
        final Dialog dialog = new Dialog(MyProfile_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.chooser_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        LinearLayout camera_ll = (LinearLayout) dialog.findViewById(R.id.camera_ll);
        LinearLayout gallery_ll = (LinearLayout) dialog.findViewById(R.id.gallery_ll);
        camera_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                FILENAME = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
                try {
                    file = Uri.fromFile(APIUtility.getFileFromURL(MyProfile_Activity.this, FILENAME));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

                startActivityForResult(intent, 100);*/
                checkPermission(CAMERA);
            }
        });
        gallery_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               /* intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, IMG_RESULT);*/
                checkPermission(GALLERY);
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void EditPicDialog(final Uri file) {
        final Dialog dialog = new Dialog(MyProfile_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        TextView title_dialog = (TextView) dialog.findViewById(R.id.title_dialog);
        TextView desc_dialog = (TextView) dialog.findViewById(R.id.desc_dialog);
        title_dialog.setText("Edit");
        cancel_btn.setText("Not this time");
        cancel_btn.setVisibility(View.GONE);
        ok_btn.setText("OK");
        desc_dialog.setText("Enhance this picture ");

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

//
                Bitmap thePic = null;
                try {
                    thePic = MediaStore.Images.Media.getBitmap(getContentResolver(), file);
                    long size = sizeOf(thePic);
                    String file1 = saveToInternalStorage(thePic);
                    profile_image.setImageURI(file);
                    tvUsername.setVisibility(View.GONE);
                    new File_Uploader(MyProfile_Activity.this, file1, APIUtility.UPLOAD_FILE, FILENAME, size, "IMAGE", false, MyProfile_Activity.this);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(MyProfile_Activity.this, "Please try again", Toast.LENGTH_LONG).show();
                }

            }
        });
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try {
                    //call the standard crop action intent (the user device may not support it)
                    Intent cropIntent = new Intent("com.android.camera.action.CROP");
                    //indicate image type and Uri
                    cropIntent.setDataAndType(file, "image/*");
                    //set crop properties
                    cropIntent.putExtra("crop", "true");
                    //indicate aspect of desired crop
                    cropIntent.putExtra("aspectX", 1);
                    cropIntent.putExtra("aspectY", 1);
                    //indicate output X and Y
                    cropIntent.putExtra("outputX", 80);
                    cropIntent.putExtra("outputY", 80);
                    //retrieve data on return
                    cropIntent.putExtra("return-data", true);
                    //start the activity - we handle returning in onActivityResult
                    startActivityForResult(cropIntent, PIC_CROP);
                } catch (Exception e) {
                    Bitmap thePic = null;
                    try {
                        thePic = MediaStore.Images.Media.getBitmap(getContentResolver(), file);
                        long size = sizeOf(thePic);
                        String file1 = saveToInternalStorage(thePic);
                        profile_image.setImageURI(file);
                        tvUsername.setVisibility(View.GONE);
                        new File_Uploader(MyProfile_Activity.this, file1, APIUtility.UPLOAD_FILE, FILENAME, size, "IMAGE", false, MyProfile_Activity.this);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        Toast.makeText(MyProfile_Activity.this, "Please try again", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        dialog.show();
    }

    private void SaveChangesDialog() {
        final Dialog dialog = new Dialog(MyProfile_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        TextView title_dialog = (TextView) dialog.findViewById(R.id.title_dialog);
        TextView desc_dialog = (TextView) dialog.findViewById(R.id.desc_dialog);
        title_dialog.setText("Save");
        cancel_btn.setText("Not this time");
        ok_btn.setText("Yes");
        desc_dialog.setText("Would you like to save changes ");

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (WebServices.isNetworkAvailable(MyProfile_Activity.this)) {
                    ProfileUpdate();
                } else {
                    Toast.makeText(MyProfile_Activity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });
        dialog.show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath.getAbsolutePath();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File fileObject = null;
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                Bitmap thePic = null;
                try {
                    thePic = MediaStore.Images.Media.getBitmap(getContentResolver(), file);

//                    String file1 = saveToInternalStorage(thePic);
                    startCropImageActivity(file);
//                    submitDialog();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                EditPicDialog(file);
//                profile_image.setImageURI(file);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                file = result.getUri();
                submitDialog();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }//user is returning from cropping the image
        /*else if (requestCode == PIC_CROP) {
//get the returned data
            try {
                Bundle extras = data.getExtras();

//get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");
                profile_image.setImageBitmap(thePic);
                long size = sizeOf(thePic);
                String file = saveToInternalStorage(thePic);
                System.out.println("============size=====" + size / (1024 * 1024));
                new File_Uploader(MyProfile_Activity.this, file, APIUtility.UPLOAD_FILE, FILENAME, size, "IMAGE", true, this);
            } catch (Exception e) {
                Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                        .show();
                return;
            }
        }*/
        try {

            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {
                file = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA};
//do
                Cursor cursor = getContentResolver().query(file,
                        FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();
                startCropImageActivity(file);
            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
            return;
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        //start picker to get image for cropping and then use the image in cropping activity
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
// start cropping activity for pre-acquired image saved on the device
//        CropImage.activity(imageUri)
//                .start(this);

    }

    @Override
    public void error(String error) {

    }

    @Override
    public void complete(int media_id, String type, String res) {
        save_profile_txt.setVisibility(View.VISIBLE);
        MEDIA_ID = media_id;
        if (new File(FILENAME).exists()) new File(FILENAME).delete();
        System.out.println("==================image Uploaded===============" + media_id);
        if (!APIUtility.SECOND_LANGUAGE_PREFERENCE.equalsIgnoreCase(SELECTED_LANGUAGE)) {
            isUpdated = true;
        }
    }

    private void submitDialog() {
        new AlertDialog.Builder(MyProfile_Activity.this)
                .setTitle(getString(R.string.app_name))
                .setMessage("Do you want to set this profile image?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bitmap thePic = null;
                        try {
                            thePic = MediaStore.Images.Media.getBitmap(getContentResolver(), file);
//                            long size = sizeOf(thePic);
                            String filepath = file.getPath();
//                            Bitmap bitmapImage = BitmapFactory.decodeFile(filepath);
                            int nh = (int) (thePic.getHeight() * (512.0 / thePic.getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(thePic, 512, nh, true);
                            if (thePic != null) {
                                profile_image.setImageBitmap(scaled);
                                tvUsername.setVisibility(View.GONE);
                            }
//                            String file1 = saveToInternalStorage(thePic);

                            File fileTemp = new File(filepath);
                            long size = 0;
                            if (fileTemp.exists())
                                size = fileTemp.length();
                            new File_Uploader(MyProfile_Activity.this, filepath, APIUtility.UPLOAD_FILE, FILENAME, size, "IMAGE", true, MyProfile_Activity.this);

                   /* if(file1!=null){
                        questionList.get(questionIndex).setImage_url(file1);
                    }*/
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(MyProfile_Activity.this, "Please try again", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(MyProfile_Activity.this, "Please try again", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialog != null && !isFinishing())
                            dialog.dismiss();
                    }
                })
                .show();
    }


    private class Spinner_Adapter extends BaseAdapter {
        ArrayList<String> mStrings = new ArrayList<>();

        public Spinner_Adapter(ArrayList<String> strings) {
            mStrings = strings;
        }

        @Override
        public int getCount() {
            return mStrings.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.query_spinner_row, null);
                TextView title = (TextView) convertView.findViewById(R.id.query_spinner_row_txt);
                title.setText(mStrings.get(position));
                LinearLayout row_ll = (LinearLayout) convertView.findViewById(R.id.row_ll);
                ImageView selected_lan_img = (ImageView) convertView.findViewById(R.id.selected_lan_img);
                if (!APIUtility.SECOND_LANGUAGE_PREFERENCE.equalsIgnoreCase("") && !mStrings.get(position).equalsIgnoreCase(""))
                    if (Selected_Lang == position) selected_lan_img.setVisibility(View.VISIBLE);

            }
            return convertView;
        }
    }

    private void passInQuiz() {
        ArrayList<MeterialUtility> allMaterialList = FlowingCourseUtils.getAllMaterials();
        for (int i = 0; i < allMaterialList.size(); i++) {
            MeterialUtility meterialUtility = allMaterialList.get(i);
            boolean shouldShow=false;
            if (meterialUtility != null)
                if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                    if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && meterialUtility.isPassed()) {
                        if (meterialUtility.getIsResultPublishRequired().equalsIgnoreCase("YES")) {
                            if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes"))
                                shouldShow = true;
                            else
                                shouldShow = false;
                        } else
                            shouldShow = true;
                    } else
                        shouldShow = false;

                } else if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
                    if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && !TextUtils.isEmpty(meterialUtility.getScrom_status()) && meterialUtility.getScrom_status().equalsIgnoreCase("COMPLETED") && meterialUtility.isPassed())
                        shouldShow = true;
                }
            if (shouldShow) {
                Certificate_Utils utils = new Certificate_Utils();
                utils.setMaterial_name(meterialUtility.getTitle());
                utils.setMaterial_id(meterialUtility.getMaterial_id());
                utils.setCourse_id(meterialUtility.getCourse_id());
                materials_utilses.add(utils);
            }
        }
    }


    private void DownloadELearning(final Context context, final String mUrl, final String filename) {
        isDownload = false;
        timeout = null;
        count = new int[]{30};//for 40 seconds to wait for buffering after it will finish the activity
        istimeout = true;
        //        private ProgressDialog pDialog;
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mDialog.dismiss();
                if (msg.what == 0) {
                    if (FILEPATH != null) {
                        Intent intent = new Intent(MyProfile_Activity.this, ViewCertificateActivity.class);
                        intent.putExtra("filename", FILEPATH);
                        startActivity(intent);

                    } else {
                        try {
                            boolean isdelete = new File(MyProfile_Activity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + filename).delete();
                            System.out.println("=isdelete===" + isdelete + "===" + filename);
                            APIUtility.isDeletedFileFromURL(context, filename + "zip");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                        AppUtility.showToast(context, MSG);
                        Toast.makeText(MyProfile_Activity.this, MSG, Toast.LENGTH_LONG).show();
                    }
                } else if (msg.what == 5) {
                    try {
                        boolean isdelete = new File(MyProfile_Activity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + filename).delete();
                        System.out.println("=isdelete===" + isdelete + "===" + filename);
                        APIUtility.isDeletedFileFromURL(context, filename + "zip");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        boolean isdelete = new File(MyProfile_Activity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + filename).delete();
                        System.out.println("=isdelete===" + isdelete + "===" + filename);
                        APIUtility.isDeletedFileFromURL(context, filename + "zip");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    AppUtility.showToast(context, MSG);
                    Toast.makeText(MyProfile_Activity.this, MSG, Toast.LENGTH_LONG).show();
                }
            }
        };
        timeout = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                System.out.println("value of count=" + msg.getData().getLong("count"));
                if (msg.getData().getBoolean("valid")) {
                    if (msg.getData().getLong("count") == 0) {

                        if (mDialog != null && mDialog.isShowing()) {
                            if (istimeout) {
                                try {
                                    isDownload = false;
                                    FILEPATH = null;
                                    mDialog.dismiss();
                                    handler.sendEmptyMessage(5);
                                    Toast.makeText(MyProfile_Activity.this, "Internet too slow to download", Toast.LENGTH_LONG).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }

            }
        };
        timeout.postDelayed(null, 0);
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (count[0] > 0) {
                    if (!isDownload)
                        if (istimeout) {
                            try {
                                Thread.sleep(1000);
                            } catch (Exception e) {
                            }

                            Message msg = new Message();
                            Bundle b = new Bundle();
                            b.putBoolean("valid", true);
                            b.putLong("count", --count[0]);
                            msg.setData(b);
                            timeout.sendMessage(msg);
                        } else {
                            --count[0];
                        }

                }

            }
        }).start();

        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDownload = false;
                FILEPATH = null;
                istimeout = false;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(1);
            }
        });
        mDialog.show();

        new Thread() {
            @Override
            public void run() {
                int lenghtOfFile = 0;
                int count;
                File file = null;

                try {
                    URL url = new URL(mUrl);

                    URLConnection conection = url.openConnection();
                    conection.setConnectTimeout(10000);
                    conection.setReadTimeout(10000);
                    conection.connect();
                    // getting file length
                    lenghtOfFile = conection.getContentLength();
                    System.out.println("=====file length===" + (lenghtOfFile / (1024 * 1024)));
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);

                    // Output stream to write file
                    //Download epub file in app private directory /sdcard/Android/data/<package-name>/files/chapters
                    file = getFileFromURL(context, filename);
                    OutputStream output = new FileOutputStream(file);
                    byte data[] = new byte[1024];
//                    runnable.run();
                    long total = 0;
                    isDownload = true;
                    while ((count = input.read(data)) != -1) {
                        istimeout = false;
                        if (isDownload) {
                            total += count;
                            totalVal = "" + (int) ((total * 100) / lenghtOfFile);
                            totallenghtOfFile = lenghtOfFile;
//                            publishProgress();
//                            runnable.notify();//run();
                            // writing data to file

                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    abort_btn.setEnabled(true);
                                    if (totallenghtOfFile != -1) {
                                        String total = (totallenghtOfFile / (1024 * 1000f)) + "";
                                        String[] t = total.split("\\.");
                                        total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                    } else {
                                        total_file_txt.setText("0 MB");
                                    }
                                    title_txt.setText("Fetching... Please wait!");
                                    if (!totalVal.contains("-")) {
                                        mBar.setProgress(Integer.parseInt(totalVal));
                                        totalPer_txt.setText(Integer.valueOf(totalVal) + " %");
                                    } else {
//                                        isDownload = false;
//                                        MSG = "Network issues.. You have been Timed-out";
//                                        mBar.setProgress(0);
//                                        totalPer_txt.setText("0 %");
//                                        handler.sendEmptyMessage(2);
                                    }
                                }
                            });
                            output.write(data, 0, count);
                        } else {
                            // flushing output
                            output.flush();

                            // closing streams
                            output.close();
                            input.close();
                            return;
                        }
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    MSG = "Network issues.. You have been Timed-out";
                    FILEPATH = null;
                    handler.sendEmptyMessage(2);
                    return;
                }
                FILEPATH = file.getAbsolutePath();

                handler.sendEmptyMessage(0);
            }

        }.start();


    }

    public static File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());

        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    private void checkPermission(int permissionType) {
        if (ContextCompat.checkSelfPermission(MyProfile_Activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(MyProfile_Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(MyProfile_Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MyProfile_Activity.this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(MyProfile_Activity.this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        permissionType);

            } else {
                ActivityCompat.requestPermissions(MyProfile_Activity.this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        permissionType);
            }
        } else {
            if (permissionType == CAMERA) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                FILENAME = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
                try {
                    file = Uri.fromFile(APIUtility.getFileFromURL(MyProfile_Activity.this, FILENAME));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

                startActivityForResult(intent, 100);
            } else {
                intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, IMG_RESULT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    FILENAME = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
                    try {
                        file = Uri.fromFile(APIUtility.getFileFromURL(MyProfile_Activity.this, FILENAME));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

                    startActivityForResult(intent, 100);
                }
                break;
            case GALLERY:
                if (grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    intent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, IMG_RESULT);
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
