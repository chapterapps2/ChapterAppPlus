package com.chaptervitamins.myprofile;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.chaptervitamins.utility.ReadResponseUtility;

import java.util.ArrayList;

public class PointsSummaryActivity extends BaseActivity {
    private RecyclerView rvCoinsSummary;
    private TextView tvNoDataFound;
    private ArrayList<MaterialCoinModel> materialCoinsAl;
    private ArrayList<MeterialUtility> allMaterialAl = new ArrayList<>();
    private ArrayList<MaterialCoinModel> allMaterialCoinsAl = new ArrayList<>();
    private ArrayList<MaterialCoinModel> otherMaterialCoinsAl = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coins_summary);
        try {
            findViews();
            setData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void findViews() {
        rvCoinsSummary = (RecyclerView) findViewById(R.id.rv_coin_summary);
        tvNoDataFound = (TextView) findViewById(R.id.tv_no_data_found);
    }

    private void setData() {
        getAllMaterialList();
        getRedeemMaterial();

    }

    private void getAllMaterialList() {
        int courseSize = HomeActivity.courseUtilities.size();
        for (int j = 0; j < courseSize; j++) {
            CourseUtility courseUtility = HomeActivity.courseUtilities.get(j);
            if (courseUtility != null && courseUtility.getModulesUtilityArrayList() != null) {
                int moduleSize = courseUtility.getModulesUtilityArrayList().size();
                for (int k = 0; k < moduleSize; k++) {
                    ModulesUtility modulesUtility = courseUtility.getModulesUtilityArrayList().get(k);
                    if (modulesUtility != null && modulesUtility.getMeterialUtilityArrayList() != null) {
                        int materialSize = modulesUtility.getMeterialUtilityArrayList().size();
                        for (int l = 0; l < materialSize; l++) {
                            allMaterialAl.add(modulesUtility.getMeterialUtilityArrayList().get(l));

                        }
                    }
                }
            }
        }
    }

    private void getRedeemMaterial() {
        materialCoinsAl = new ArrayList<>();
        /*for (int i = 0; i < HomeActivity.mReadResponse.size(); i++) {
            MaterialCoinModel materialCoinModel = new MaterialCoinModel();
            ReadResponseUtility readResponseUtility = HomeActivity.mReadResponse.get(i);
            String coinsAllocated = readResponseUtility.getCoins_allocated();
            if (!TextUtils.isEmpty(coinsAllocated) && !coinsAllocated.equals("0")) {
                materialCoinModel.setMaterialId(readResponseUtility.getMaterial_id());
                materialCoinModel.setEarnedCoins(readResponseUtility.getCoins_allocated());
                materialCoinModel.setMaterialName(readResponseUtility.getTitle());
                materialCoinModel.setType("item");
                materialCoinModel.setTotalCoins(readResponseUtility.getCoins_allocated());
                materialCoinsAl.add(materialCoinModel);
            }

       }*/
        ArrayList<ReadResponseUtility> submitResponse = DataBase.getInstance(PointsSummaryActivity.this).getResponseData();
        for (int i = 0; i < submitResponse.size(); i++) {
            MaterialCoinModel materialCoinModel = new MaterialCoinModel();
            ReadResponseUtility readResponseUtility = submitResponse.get(i);
            String coinsAllocated = readResponseUtility.getCoins_allocated();
            if (!TextUtils.isEmpty(coinsAllocated) && !coinsAllocated.equals("0")) {
                materialCoinModel.setMaterialId(readResponseUtility.getMaterial_id());
                materialCoinModel.setEarnedCoins(readResponseUtility.getCoins_allocated());
                materialCoinModel.setMaterialName(readResponseUtility.getTitle());
                materialCoinModel.setType("item");
                materialCoinModel.setTotalCoins(readResponseUtility.getCoins_allocated());
                materialCoinsAl.add(materialCoinModel);
            }
        }

        for (int i = 0; i < allMaterialAl.size(); i++) {
            MaterialCoinModel materialCoinModel = new MaterialCoinModel();
            MeterialUtility meterialUtility = allMaterialAl.get(i);
            if (meterialUtility != null) {
                CoinsAllocatedModel coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
                if (coinsAllocatedModel != null) {
                    String coinsAllocated = coinsAllocatedModel.getEarnCoins();
                    if (coinsAllocatedModel.getRedeem().equalsIgnoreCase("TRUE") && !TextUtils.isEmpty(coinsAllocated)) {
                        materialCoinModel.setMaterialId(meterialUtility.getMaterial_id());
                        materialCoinModel.setEarnedCoins(coinsAllocated);
                        materialCoinModel.setMaterialName(meterialUtility.getTitle());
                        materialCoinModel.setType("item");
                        materialCoinModel.setTotalCoins(coinsAllocatedModel.getMaxCoins());
                        materialCoinsAl.add(materialCoinModel);
                    }
                }
            }
        }
        allMaterialCoinsAl.addAll(materialCoinsAl);
        MaterialCoinModel headerModel = new MaterialCoinModel();
        headerModel.setType("header");
        headerModel.setHeaderType("1");
        allMaterialCoinsAl.add(headerModel);

//        MaterialCoinModel headerModel1 = new MaterialCoinModel();
//        headerModel1.setType("header");
//        headerModel1.setHeaderType("2");
//        otherMaterialCoinsAl.add(headerModel1);
        int allMaterialSize = allMaterialAl.size();
        int readmaterialSize = materialCoinsAl.size();

        for (int j = 0; j < allMaterialSize; j++) {
            MeterialUtility meterialUtility = allMaterialAl.get(j);
            if (meterialUtility.getCoinsAllocatedModel() != null && !TextUtils.isEmpty(meterialUtility.getCoinsAllocatedModel().getMaxCoins()) && !meterialUtility.getCoinsAllocatedModel().getMaxCoins().equals("0")) {
                if (readmaterialSize > 0) {
                    for (int k = 0; k < readmaterialSize; k++) {
                        MaterialCoinModel materialCoinModel1 = materialCoinsAl.get(k);
                        if (materialCoinModel1.getMaterialId().equals(meterialUtility.getMaterial_id())) {
                            materialCoinModel1.setTotalCoins(meterialUtility.getTotal_coins());
                            if (meterialUtility.getCoinsAllocatedModel() != null && meterialUtility.getCoinsAllocatedModel().getRedeem().equalsIgnoreCase("FALSE") && !TextUtils.isEmpty(meterialUtility.getTotal_coins()) && !meterialUtility.getTotal_coins().equals("0")) {
                                MaterialCoinModel materialCoinModel2 = new MaterialCoinModel();
                                materialCoinModel2.setMaterialId(meterialUtility.getMaterial_id());
                                materialCoinModel2.setEarnedCoins("0");
                                materialCoinModel2.setMaterialName(meterialUtility.getTitle());
                                materialCoinModel2.setTotalCoins(meterialUtility.getTotal_coins());
                                materialCoinModel2.setType("item");
                                allMaterialCoinsAl.add(materialCoinModel2);
                            }
                            break;
                        } else if (meterialUtility.getCoinsAllocatedModel() != null && meterialUtility.getCoinsAllocatedModel().getRedeem().equalsIgnoreCase("FALSE") && !TextUtils.isEmpty(meterialUtility.getTotal_coins()) && !meterialUtility.getTotal_coins().equals("0")) {
                            MaterialCoinModel materialCoinModel2 = new MaterialCoinModel();
                            materialCoinModel2.setMaterialId(meterialUtility.getMaterial_id());
                            materialCoinModel2.setEarnedCoins("0");
                            materialCoinModel2.setMaterialName(meterialUtility.getTitle());
                            materialCoinModel2.setTotalCoins(meterialUtility.getTotal_coins());
                            materialCoinModel2.setType("item");
                            allMaterialCoinsAl.add(materialCoinModel2);
                            break;
                        }
                    }
                } else {
                    MaterialCoinModel materialCoinModel2 = new MaterialCoinModel();
                    materialCoinModel2.setMaterialId(meterialUtility.getMaterial_id());
                    materialCoinModel2.setEarnedCoins("0");
                    materialCoinModel2.setMaterialName(meterialUtility.getTitle());
                    materialCoinModel2.setTotalCoins(meterialUtility.getTotal_coins());
                    materialCoinModel2.setType("item");
                    allMaterialCoinsAl.add(materialCoinModel2);
                }
            } else { // for setting status of complete/incomplete of other materials
                /*String status = "Incomplete";
                MaterialCoinModel materialCoinModel2 = new MaterialCoinModel();
                materialCoinModel2.setStatus("Incomplete");
                for (int i = 0; i < HomeActivity.mReadResponse.size(); i++) {
                    if (meterialUtility.getMaterial_id().equals(HomeActivity.mReadResponse.get(i).getMaterial_id())) {
                        status = "Complete";
                        break;
                    }
                }
                CoinsAllocatedModel coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
                if (status.equals("Incomplete") && coinsAllocatedModel == null) {
                    materialCoinModel2.setMaterialId(meterialUtility.getMaterial_id());
                    materialCoinModel2.setEarnedCoins("0");
                    materialCoinModel2.setMaterialName(meterialUtility.getTitle());
                    materialCoinModel2.setTotalCoins(meterialUtility.getTotal_coins());
                    materialCoinModel2.setType("other");
                    materialCoinModel2.setStatus("Incomplete");
                    otherMaterialCoinsAl.add(materialCoinModel2);
                }*/

            }
        }

        if (materialCoinsAl != null && materialCoinsAl.size() > 1) {
            allMaterialCoinsAl.removeAll(materialCoinsAl);
            allMaterialCoinsAl.addAll(0, materialCoinsAl);

        }
        if (allMaterialCoinsAl.size() > 1)
            if (!TextUtils.isEmpty(allMaterialCoinsAl.get(allMaterialCoinsAl.size() - 1).getHeaderType()) && allMaterialCoinsAl.get(allMaterialCoinsAl.size() - 1).getHeaderType().equals("1"))
                allMaterialCoinsAl.remove(allMaterialCoinsAl.size() - 1);
        if (otherMaterialCoinsAl.size() > 1) {
            allMaterialCoinsAl.addAll(allMaterialCoinsAl.size(), otherMaterialCoinsAl);
        } else
            otherMaterialCoinsAl.clear();

        if (allMaterialCoinsAl != null && allMaterialCoinsAl.size() > 1) {
            PointsSummaryAdapter coinsSummaryAdapter = new PointsSummaryAdapter(allMaterialCoinsAl);
            LinearLayoutManager manager = new LinearLayoutManager(PointsSummaryActivity.this);
            rvCoinsSummary.setLayoutManager(manager);
            rvCoinsSummary.setAdapter(coinsSummaryAdapter);
        } else {
            findViewById(R.id.main_layout).setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onRestart() {
        if (materialCoinsAl != null)
            materialCoinsAl.clear();
        if (allMaterialAl != null)
            allMaterialAl.clear();
        if (allMaterialCoinsAl != null)
            allMaterialCoinsAl.clear();
        if (otherMaterialCoinsAl != null)
            otherMaterialCoinsAl.clear();
        setData();
        super.onRestart();
    }
}
