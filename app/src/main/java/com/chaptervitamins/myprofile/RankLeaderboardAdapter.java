package com.chaptervitamins.myprofile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 03-08-2017.
 */

public class RankLeaderboardAdapter extends RecyclerView.Adapter<RankLeaderboardAdapter.ViewHolder> {

    private ArrayList<RankHolderModel> rankHolderModelArrayList;
    private Context mContext;

    public RankLeaderboardAdapter(ArrayList<RankHolderModel> rankHolderModelArrayList) {
        this.rankHolderModelArrayList = rankHolderModelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_leaderboard_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            RankHolderModel rankHolderModel = rankHolderModelArrayList.get(position);
            holder.tvRank.setText(rankHolderModel.getRank());
            holder.tvName.setText(rankHolderModel.getFirstName());
            holder.tvCoins.setText(rankHolderModel.getCoins());
        }

    }

    @Override
    public int getItemCount() {
        return rankHolderModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvRank, tvName, tvCoins;

        public ViewHolder(View itemView) {
            super(itemView);
            tvRank = (TextView) itemView.findViewById(R.id.tv_rank);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvCoins = (TextView) itemView.findViewById(R.id.tv_coins);
        }
    }
}
