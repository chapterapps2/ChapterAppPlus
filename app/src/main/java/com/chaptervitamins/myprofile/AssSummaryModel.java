package com.chaptervitamins.myprofile;

import android.support.annotation.NonNull;

import com.chaptervitamins.utility.MeterialUtility;

/**
 * Created by Vijay Antil on 06-09-2017.
 */

public class AssSummaryModel implements Comparable<AssSummaryModel> {
    String materialId, assignMaterialId, name, score, type;
    private MeterialUtility meterialUtility;

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getAssignMaterialId() {
        return assignMaterialId;
    }

    public void setAssignMaterialId(String assignMaterialId) {
        this.assignMaterialId = assignMaterialId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int compareTo(@NonNull AssSummaryModel another) {
        if (this.getAssignMaterialId().equalsIgnoreCase(another.getAssignMaterialId()))
            return 1;

        return 0;
    }

    public MeterialUtility getMeterialUtility() {
        return meterialUtility;
    }

    public void setMeterialUtility(MeterialUtility meterialUtility) {
        this.meterialUtility = meterialUtility;
    }
}
