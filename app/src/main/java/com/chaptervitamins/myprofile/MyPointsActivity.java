package com.chaptervitamins.myprofile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyPointsActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvNoOfCoins, tvRank, tvRankLabel, tvNoRankMessage, tvCoinsCountNoRank;
    private Button btnPointsDetails, btnPointsDetailsNoRank;
    private RecyclerView rvLeaderboard;
    private ImageView ivBack;
    private LinearLayout llRank, llNoRank;
    private ArrayList<RankHolderModel> leaderBoardAl;
    private ArrayList<MeterialUtility> allMaterialAl = new ArrayList<>();
    private boolean isAvailable = false;
    private RankLeaderboardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_points);
        findViews();
        leaderBoardAl = new ArrayList<>();
        setOnClickListener();
        setData();
    }

    private void setOnClickListener() {
        btnPointsDetails.setOnClickListener(this);
        btnPointsDetailsNoRank.setOnClickListener(this);
        ivBack.setOnClickListener(this);
    }

    private void findViews() {
        tvNoOfCoins = (TextView) findViewById(R.id.tv_coins_count);
        tvRank = (TextView) findViewById(R.id.tv_rank);
        tvRankLabel = (TextView) findViewById(R.id.tv_rank_label);
        tvNoRankMessage = (TextView) findViewById(R.id.tv_no_rank_message);
        tvCoinsCountNoRank = (TextView) findViewById(R.id.tv_coins_count_no_rank);
        btnPointsDetails = (Button) findViewById(R.id.btn_points_detail);
        btnPointsDetailsNoRank = (Button) findViewById(R.id.btn_points_detail_no_rank);
        rvLeaderboard = (RecyclerView) findViewById(R.id.rv_leaderboard);
        ivBack = (ImageView) findViewById(R.id.back);
        llRank = (LinearLayout) findViewById(R.id.ll_rank);
        llNoRank = (LinearLayout) findViewById(R.id.ll_no_rank);
    }

    private void setData() {

        leaderBoardAl.addAll(parseProfileData(DataBase.getInstance(MyPointsActivity.this).getProfileData(WebServices.mLoginUtility.getUser_id())));
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getRank()) && Integer.parseInt(WebServices.mLoginUtility.getRank()) == 0) {
            llRank.setVisibility(View.GONE);
            llNoRank.setVisibility(View.GONE);
            findViewById(R.id.tv_no_course).setVisibility(View.VISIBLE);
        } else if (!TextUtils.isEmpty(WebServices.mLoginUtility.getRank()) && Integer.parseInt(WebServices.mLoginUtility.getRank()) > 0 && Integer.parseInt(WebServices.mLoginUtility.getRank()) <= 10) {
            llRank.setVisibility(View.VISIBLE);
            llNoRank.setVisibility(View.GONE);
        } else {
            llNoRank.setVisibility(View.VISIBLE);
            llRank.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(WebServices.mLoginUtility.getShowMessage()) && WebServices.mLoginUtility.getShowMessage().equalsIgnoreCase("yes")) {
                tvNoRankMessage.setText(WebServices.mLoginUtility.getRankMessage());
                tvCoinsCountNoRank.setText(WebServices.mLoginUtility.getCoins_collected());
            } else {
                llRank.setVisibility(View.VISIBLE);
                llNoRank.setVisibility(View.GONE);
            }


        }
//        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getRank()) && !WebServices.mLoginUtility.getRank().equals("0")) {
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getTotal_users())) {
            tvRank.setText(WebServices.mLoginUtility.getRank() + "/" + WebServices.mLoginUtility.getTotal_users());
        } else {
            tvRank.setText(WebServices.mLoginUtility.getRank());
        }
        tvNoOfCoins.setText(WebServices.mLoginUtility.getCoins_collected());
//        } else
//            llRank.setVisibility(View.GONE);
        if (checkMaterial())
            findViewById(R.id.ll_earn_points).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.ll_earn_points).setVisibility(View.GONE);
        setLeaderBoardData();
    }

    private void setLeaderBoardData() {
        if (leaderBoardAl.size() > 0) {
            if (adapter == null) {
                LinearLayoutManager manager = new LinearLayoutManager(MyPointsActivity.this);
                rvLeaderboard.setLayoutManager(manager);
                adapter = new RankLeaderboardAdapter(leaderBoardAl);
                rvLeaderboard.setAdapter(adapter);
            } else
                adapter.notifyDataSetChanged();
        } else {
            findViewById(R.id.leader).setVisibility(View.GONE);
        }
    }

    private ArrayList parseProfileData(String resp) {
        ArrayList<RankHolderModel> leaderBoardAl = new ArrayList<>();
        if (resp != null) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    JSONObject object = jsonObject.optJSONObject("status");
                    if (object != null && object.optString("success").equalsIgnoreCase("TRUE")) {
                        object = jsonObject.getJSONObject("data");
                        if (object != null) {
                            JSONArray topFiveArray = object.optJSONArray("top_five_rank");
                            if (topFiveArray != null && topFiveArray.length() > 0) {
                                for (int i = 0; i < topFiveArray.length(); i++) {
                                    JSONObject rankHolderObject = (JSONObject) topFiveArray.get(i);
                                    if (rankHolderObject != null) {
                                        leaderBoardAl.add(new RankHolderModel(rankHolderObject.optString("rank"), rankHolderObject.optString("email"),
                                                rankHolderObject.optString("firstname"), rankHolderObject.optString("coins_allocated")));

                                    }
                                }

                            }

                            WebServices.mLoginUtility.setTotal_users(object.optString("total_users"));
                            String coinsCollected = object.optString("coins_allocated");
                            WebServices.mLoginUtility.setRank(object.optString("rank"));
                            if (TextUtils.isEmpty(coinsCollected))
                                WebServices.mLoginUtility.setCoins_collected("0");
                            else
                                WebServices.mLoginUtility.setCoins_collected(coinsCollected);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return leaderBoardAl;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_points_detail_no_rank:
            case R.id.btn_points_detail:
                Intent intent = new Intent(MyPointsActivity.this, PointsSummaryActivity.class);
                startActivity(intent);
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    private boolean checkMaterial() {
        getAllMaterialList();
        for (int i = 0; i < allMaterialAl.size(); i++) {
            MeterialUtility meterialUtility = allMaterialAl.get(i);
            if (meterialUtility != null) {
                CoinsAllocatedModel coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
                if (coinsAllocatedModel != null) {
                    return true;
                }
            }
        }
        return false;
    }

    private void getAllMaterialList() {
        int courseSize = HomeActivity.courseUtilities.size();
        for (int j = 0; j < courseSize; j++) {
            CourseUtility courseUtility = HomeActivity.courseUtilities.get(j);
            if (courseUtility != null && courseUtility.getModulesUtilityArrayList() != null) {
                int moduleSize = courseUtility.getModulesUtilityArrayList().size();
                for (int k = 0; k < moduleSize; k++) {
                    ModulesUtility modulesUtility = courseUtility.getModulesUtilityArrayList().get(k);
                    if (modulesUtility != null && modulesUtility.getMeterialUtilityArrayList() != null) {
                        int materialSize = modulesUtility.getMeterialUtilityArrayList().size();
                        for (int l = 0; l < materialSize; l++) {
                            allMaterialAl.add(modulesUtility.getMeterialUtilityArrayList().get(l));
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getProfileData();
    }

    private void getProfileData() {
        if (WebServices.isNetworkAvailable(MyPointsActivity.this)) {
            final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait..");
            ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
            nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
            new GenericApiCall(MyPointsActivity.this, APIUtility.GETMYPROFILE, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    if (mWebServices.isValid((String) result)) {
                        mWebServices.isProfileData(MyPointsActivity.this, (String) result);
                        leaderBoardAl.clear();
                    }
                    if (dialog != null)
                        dialog.dismiss();
                    setData();
                }

                @Override
                public void onError(ErrorModel error) {
                    if (dialog != null)
                        dialog.dismiss();
                    Toast.makeText(MyPointsActivity.this, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
                    setData();
                }
            }).execute();
        } else {
            Toast.makeText(MyPointsActivity.this, "You are offline now!", Toast.LENGTH_SHORT).show();
            setData();
        }

    }

}
