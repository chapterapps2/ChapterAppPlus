package com.chaptervitamins.myprofile;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

public class ModuleSummaryActivity extends BaseActivity {
   private ListView listView;
   ModuleSummaryAdapter moduleSummaryAdapter;
   private int TotalModules = 0;
   ArrayList<ModulesUtility>modulesUtilityListView;
    int moduleCompleted;
    TextView assTextView;
    TextView toolbar_title;
    TextView comTextView;
    ImageView back;
    String lastAccessed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_module_summary);
        toolbar_title=(TextView)findViewById(R.id.toolbar_title);
        assTextView=(TextView)findViewById(R.id.assTextView);
        comTextView=(TextView)findViewById(R.id.comTextView);
        back=(ImageView)findViewById(R.id.back);
        listView=(ListView)findViewById(R.id.listView);

        modulesUtilityListView=new ArrayList<>();

        toolbar_title.setText(getString(R.string.module_sum));
        try {
            int CourseUtilSize= HomeActivity.courseUtilities.size();
            for (int i = 0; i < CourseUtilSize; i++) {
                CourseUtility courseUtility = HomeActivity.courseUtilities.get(i);
                if (courseUtility.getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) && !courseUtility.getCourse_name().equalsIgnoreCase(AppConstants.CourseType.SURVEY)) {
                    TotalModules = TotalModules + courseUtility.getModulesUtilityArrayList().size();
                    int ModuleListSize=courseUtility.getModulesUtilityArrayList().size();

                    for(int j=0;j<ModuleListSize;j++) {
                     ModulesUtility modulesUtility = courseUtility.getModulesUtilityArrayList().get(j);
                       if(modulesUtility.getCompletion_per().equals("100"))
                           moduleCompleted ++;
                        for (int k = 0; k < modulesUtility.getMeterialUtilityArrayList().size(); k++) {
                            MeterialUtility utility = modulesUtility.getMeterialUtilityArrayList().get(k);
                            for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
                                    if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                        lastAccessed = HomeActivity.mReadResponse.get(k1).getFinish_time();
                                    }
                            }
                        }

                        modulesUtility.setLastaccessed(lastAccessed);
                        modulesUtilityListView.add(modulesUtility);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        assTextView.setText(TotalModules+"");
        comTextView.setText(moduleCompleted+"");

        moduleSummaryAdapter=new ModuleSummaryAdapter(ModuleSummaryActivity.this,listView,modulesUtilityListView);
        listView.setAdapter(moduleSummaryAdapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
   /* private boolean isCompletedMaterials(ArrayList< MeterialUtility > tempmeterialUtilityArrayList) {
        boolean iscompleted = false;
//        MultichoiceActivity.mReadResponse.addAll(dataBase.getResponseData());
        for (int i = 0; i < tempmeterialUtilityArrayList.size(); i++) {
            iscompleted = false;
            for (int j = 0; j < MultichoiceActivity.mReadResponse.size(); j++) {
                if (MultichoiceActivity.ASSIGNEDCOURSEID.equalsIgnoreCase(MultichoiceActivity.mReadResponse.get(j).getAssign_course_id())) {
                    if (tempmeterialUtilityArrayList.get(i).getMaterial_id().equalsIgnoreCase(MultichoiceActivity.mReadResponse.get(j).getMaterial_id())) {
                        iscompleted = true;
                        break;
                    }
                }
            }
            if (!iscompleted) return false;
        }
        return iscompleted;
    }*/


}
