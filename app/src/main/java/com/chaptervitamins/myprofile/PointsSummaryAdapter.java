package com.chaptervitamins.myprofile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;

import java.util.ArrayList;

/**
 * Created by Sagar on 20-07-2017.
 */

public class PointsSummaryAdapter extends RecyclerView.Adapter<PointsSummaryAdapter.ViewHolder> {

    private final ArrayList<MaterialCoinModel> materialCoinModelArrayList;
    private Context mContext;
    private static final int TYPE_HEADER = 1, TYPE_ITEM = 2, TYPE_OTHER = 3;

    public PointsSummaryAdapter(ArrayList<MaterialCoinModel> materialCoinModelArrayList) {
        this.materialCoinModelArrayList = materialCoinModelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        View view = null;
        switch (viewType) {
            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_coin_summary, parent, false);
                break;
            case TYPE_HEADER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_earn_more_layout, parent, false);
                break;
            case TYPE_OTHER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_other_layout, parent, false);
                break;
        }

        return new ViewHolder(view, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        switch (materialCoinModelArrayList.get(position).getType()) {
            case "header":
                return TYPE_HEADER;
            case "item":
                return TYPE_ITEM;
            case "other":
                return TYPE_OTHER;
        }
        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            MaterialCoinModel materialCoinModel = materialCoinModelArrayList.get(position);
            if (materialCoinModel != null) {
                switch (materialCoinModel.getType()) {
                    case "item":
                        holder.tvMaterialName.setText(materialCoinModel.getMaterialName());
                        holder.tvCoins.setText(materialCoinModel.getTotalCoins());
                        holder.tvMyPoints.setText(materialCoinModel.getEarnedCoins());
                        break;
                    case "header":
                        if (materialCoinModel.getHeaderType().equals("2")) {
                            holder.tvEarnMore.setText("Other materials");
                        }
                        break;
                    case "other":
                        holder.tvOtherMaterialName.setText(materialCoinModel.getMaterialName());
                        holder.tvItemStatus.setText(materialCoinModel.getStatus());
                        break;
                }

            }
        }
    }

    @Override
    public int getItemCount() {
        return materialCoinModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMaterialName, tvCoins, tvMyPoints, tvEarnMore, tvItemStatus, tvOtherMaterialName;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            initUi(itemView, viewType);
        }

        private void initUi(View itemView, int viewType) {
            switch (viewType) {
                case TYPE_ITEM:
                    tvMaterialName = (TextView) itemView.findViewById(R.id.tv_material_name);
                    tvCoins = (TextView) itemView.findViewById(R.id.tv_total_coins);
                    tvMyPoints = (TextView) itemView.findViewById(R.id.tv_my_points);
                    itemClick(itemView);
                    break;
                case TYPE_HEADER:
                    tvEarnMore = (TextView) itemView.findViewById(R.id.tv_earn_more);
                    break;
                case TYPE_OTHER:
                    tvOtherMaterialName = (TextView) itemView.findViewById(R.id.tv_other_material_name);
                    tvItemStatus = (TextView) itemView.findViewById(R.id.tv_item_status);
                    itemClick(itemView);
                    break;
            }

        }

        private void itemClick(View itemView) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final MaterialCoinModel materialCoinModel = materialCoinModelArrayList.get(getAdapterPosition());
                    DialogUtils.showDialog(mContext, "Are you sure to open this material?", new Runnable() {
                        @Override
                        public void run() {
                            FlowingCourseUtils.openMaterial(mContext, materialCoinModel.getMaterialId(),false);
                        }
                    }, null);

                }
            });
        }


    }
}
