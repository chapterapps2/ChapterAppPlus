package com.chaptervitamins.myprofile;

/**
 * Created by Vijay Antil on 03-08-2017.
 */

public class RankHolderModel {
    private String rank,email,firstName,coins;

    public RankHolderModel(String rank, String email, String firstName, String coins) {
        this.rank = rank;
        this.email = email;
        this.firstName = firstName;
        this.coins = coins;
    }

    public String getRank() {
        return rank;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getCoins() {
        return coins;
    }
}
