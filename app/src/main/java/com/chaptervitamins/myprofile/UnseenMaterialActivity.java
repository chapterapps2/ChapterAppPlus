package com.chaptervitamins.myprofile;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.CustomView.WordUtils;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UnseenMaterialActivity extends BaseActivity {
    RecyclerView recyclerView;
    private ArrayList<MeterialUtility> allMaterialAl = new ArrayList<>();
    private ArrayList<MeterialUtility> allDuplicate = new ArrayList<>();
    private ImageView profile_image;
    private TextView profile_name;
    private TextView profile_Designation;
    private DataBase dataBase;
    private Bitmap imageBitmap;
    private WebServices webServices;
    private UnseenMaterialAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unseen_material);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        profile_name = (TextView) findViewById(R.id.profile_name);
        profile_Designation = (TextView) findViewById(R.id.profile_Designation);
        recyclerView = (RecyclerView) findViewById(R.id.rv_unseen);
        dataBase = DataBase.getInstance(UnseenMaterialActivity.this);
        webServices = new WebServices();
        String fname = WordUtils.capitalize(WebServices.mLoginUtility.getFirstname());
        profile_Designation.setText(WebServices.mLoginUtility.getBranch_name());
        profile_name.setText(fname);
        SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);

        String profilePicUrl = WebServices.mLoginUtility.getPhoto();
        if (!TextUtils.isEmpty(profilePicUrl) && !profilePicUrl.contains("no-image"))
            Picasso.with(this).load(profilePicUrl).placeholder(R.drawable.profile).error(R.drawable.profile).into(profile_image);
        else
            setDefaultImage();
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getAllMaterialList();
    }

    private void getAllMaterialList() {
        allMaterialAl.addAll(FlowingCourseUtils.getAllMaterials());

        allDuplicate.addAll(allMaterialAl);

        for (int i = 0; i < allMaterialAl.size(); i++) {
            for (int m = 0; m < HomeActivity.mReadResponse.size(); m++) {
                if (HomeActivity.mReadResponse.get(m).getMaterial_id().equalsIgnoreCase(allMaterialAl.get(i).getMaterial_id())) {
                    if (allDuplicate.contains(allMaterialAl.get(i))) {
                        allDuplicate.remove(allMaterialAl.get(i));
                    }
                }
            }
        }

        if (adapter == null) {
            adapter = new UnseenMaterialAdapter(allDuplicate);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(manager);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        allMaterialAl.clear();
        allDuplicate.clear();
        getAllMaterialList();
    }
}
