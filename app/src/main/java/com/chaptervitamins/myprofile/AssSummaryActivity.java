package com.chaptervitamins.myprofile;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.chaptervitamins.utility.ReadResponseUtility;

import java.util.ArrayList;

public class AssSummaryActivity extends BaseActivity {

    AssSummaryAdapter AssSummaryAdapter;
    private int TotalModules = 0;

    ArrayList<ReadResponseUtility> ReadResponseUtilityArrayList;
    ArrayList<AssSummaryModel> assSummaryModelArrayList = new ArrayList<>();
    int TotalQuiz;
    int pendingQuiz;
    int TotalQuizCompleted;
    RecyclerView recyclerView;
    TextView assTextView;
    TextView toolbar_title;
    TextView comTextView;
    TextView asstitText;
    TextView comtitText;
    ImageView back;

    ArrayList<String> ids = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ass_summary);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        assTextView = (TextView) findViewById(R.id.assTextView);
        comTextView = (TextView) findViewById(R.id.comTextView);

        asstitText = (TextView) findViewById(R.id.asstitText);
        comtitText = (TextView) findViewById(R.id.comtitText);
        back = (ImageView) findViewById(R.id.back);
        recyclerView = (RecyclerView) findViewById(R.id.listView);

        ReadResponseUtilityArrayList = new ArrayList<>();

        toolbar_title.setText(getString(R.string.ass_sum));

        asstitText.setText(getString(R.string.pen_sum));
        comtitText.setText(getString(R.string.com_sum));

        int CourseUtilSize = HomeActivity.courseUtilities.size();
        for (int i = 0; i < CourseUtilSize; i++) {
            if (HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) || HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.CourseType.SURVEY)) {
                TotalModules = TotalModules + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size();
                for (int j = 0; j < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size(); j++) {
                    // TotalMaterials = TotalMaterials + MultichoiceActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size();
                    ArrayList<String> materialids = new ArrayList<>();
                    int sizeMaterial=HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size();
                    for (int k = 0; k < sizeMaterial; k++) {
                        MeterialUtility utility = HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k);
                        if (utility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {

                            TotalQuiz = TotalQuiz + 1;
                        }
                        for (int Tk = 0; Tk < HomeActivity.mReadResponse.size(); Tk++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getMaterial_id())) {
                                if (!materialids.contains(utility.getMaterial_id())) {
                                    materialids.add(utility.getMaterial_id());
                                    //    TotalMaterialsAccessed = TotalMaterialsAccessed + 1;
                                }
                                utility.setResult(HomeActivity.mReadResponse.get(Tk).getResult());
                            }
//                            }
                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    //   TotalQuizAttempted = TotalQuizAttempted + 1;
                                    break;
                                }
//                                }
                            }
                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    System.out.println("======MultichoiceActivity.mReadResponse.get(k1).getResult()===" + HomeActivity.mReadResponse.get(k1).getResult());
//                                        if (Integer.parseInt(HomeActivity.mReadResponse.get(k1).getResult()) >= Integer.parseInt(utility.getPassing_percentage())) {
                                        TotalQuizCompleted = TotalQuizCompleted + 1;
                                    break;
//                                        }
                                }
//                                }
                            }
                        }
                    }
                }
            }

        }
        AssSummaryModel model = new AssSummaryModel();
        model.setType("1");
        assSummaryModelArrayList.add(model);
        for (int k = HomeActivity.mReadResponse.size() - 1; k >= 0; k--) {
            if (HomeActivity.mReadResponse.get(k).getResponse_type() != null)
                if (HomeActivity.mReadResponse.get(k).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                    AssSummaryModel assSummaryModel = new AssSummaryModel();
                    assSummaryModel.setMaterialId(HomeActivity.mReadResponse.get(k).getMaterial_id());
                    assSummaryModel.setAssignMaterialId(HomeActivity.mReadResponse.get(k).getAssign_material_id());
                    assSummaryModel.setName(HomeActivity.mReadResponse.get(k).getTitle());
                    assSummaryModel.setScore(HomeActivity.mReadResponse.get(k).getResult());
                    assSummaryModel.setType("0");

                    for (int l = 0; l < HomeActivity.courseUtilities.size(); l++) {
                        ArrayList<ModulesUtility> modulesUtilities = HomeActivity.courseUtilities.get(l).getModulesUtilityArrayList();
                        for (int m = 0; m < modulesUtilities.size(); m++) {
                            ArrayList<MeterialUtility> meterialUtilities = modulesUtilities.get(m).getMeterialUtilityArrayList();
                            for (int n = 0; n < meterialUtilities.size(); n++) {
                                MeterialUtility utility = meterialUtilities.get(n);
                                if (utility.getAssign_material_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k).getAssign_material_id())) {
                                    assSummaryModel.setMeterialUtility(utility);
                                }
                                assSummaryModel.setMeterialUtility(utility);
                            }
                        }
                    }
                    assSummaryModelArrayList.add(assSummaryModel);
                    ids.add(HomeActivity.mReadResponse.get(k).getAssign_material_id());
//                    ReadResponseUtilityArrayList.add(HomeActivity.mReadResponse.get(k));
                }
        }
        AssSummaryModel model1 = new AssSummaryModel();
        model1.setType("2");
        assSummaryModelArrayList.add(model1);
        int previousSize = assSummaryModelArrayList.size();
        for (int l = 0; l < HomeActivity.courseUtilities.size(); l++) {
            ArrayList<ModulesUtility> modulesUtilities = HomeActivity.courseUtilities.get(l).getModulesUtilityArrayList();
            for (int m = 0; m < modulesUtilities.size(); m++) {
                ArrayList<MeterialUtility> meterialUtilities = modulesUtilities.get(m).getMeterialUtilityArrayList();
                for (int n = 0; n < meterialUtilities.size(); n++) {
                    MeterialUtility utility = meterialUtilities.get(n);
                    if (utility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ) && !ids.contains(utility.getAssign_material_id())&& !utility.isPassed()) {
                        AssSummaryModel assSummaryModel = new AssSummaryModel();
                        assSummaryModel.setMaterialId(utility.getMaterial_id());
                        assSummaryModel.setAssignMaterialId(utility.getAssign_material_id());
                        assSummaryModel.setName(utility.getTitle());
                        assSummaryModel.setScore(utility.getResult());
                        assSummaryModel.setMeterialUtility(utility);
                        assSummaryModel.setType("3");
                        assSummaryModelArrayList.add(assSummaryModel);
                    }
                }
            }
        }

        int newSize = assSummaryModelArrayList.size();
        if (previousSize == newSize) {
            assSummaryModelArrayList.remove(newSize - 1);
        }

        assTextView.setText((TotalQuiz - TotalQuizCompleted) + "");
        comTextView.setText(TotalQuizCompleted + "");

        AssSummaryAdapter = new AssSummaryAdapter(assSummaryModelArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AssSummaryActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(AssSummaryAdapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        assSummaryModelArrayList.clear();
        TotalModules = 0;
        TotalQuiz = 0;
        TotalQuizCompleted = 0;
        ids.clear();
        int CourseUtilSize = HomeActivity.courseUtilities.size();
        for (int i = 0; i < CourseUtilSize; i++) {
            if (HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) && HomeActivity.courseUtilities.get(i).getCourse_type().equalsIgnoreCase(AppConstants.MaterialType.SURVEY)) {
                TotalModules = TotalModules + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size();
                for (int j = 0; j < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size(); j++) {
                    // TotalMaterials = TotalMaterials + MultichoiceActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size();
                    ArrayList<String> materialids = new ArrayList<>();
                    for (int k = 0; k < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size(); k++) {
                        MeterialUtility utility = HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k);
                        if (utility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {

                            TotalQuiz = TotalQuiz + 1;
                        }
                        for (int Tk = 0; Tk < HomeActivity.mReadResponse.size(); Tk++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(Tk).getMaterial_id())) {
                                if (!materialids.contains(utility.getMaterial_id())) {
                                    materialids.add(utility.getMaterial_id());
                                    //    TotalMaterialsAccessed = TotalMaterialsAccessed + 1;
                                }
                                utility.setResult(HomeActivity.mReadResponse.get(Tk).getResult());
                            }
//                            }
                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    //   TotalQuizAttempted = TotalQuizAttempted + 1;
                                    break;
                                }
//                                }
                            }
                        }
                        for (int k1 = 0; k1 < HomeActivity.mReadResponse.size(); k1++) {
//                            if (HomeActivity.courseUtilities.get(i).getAssign_course_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id()) || HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getAssign_course_id())) {
                            if (utility.getMaterial_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k1).getMaterial_id())) {
                                if (HomeActivity.mReadResponse.get(k1).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                    System.out.println("======MultichoiceActivity.mReadResponse.get(k1).getResult()===" + HomeActivity.mReadResponse.get(k1).getResult());
                                    if (!utility.getPassing_percentage().equalsIgnoreCase("") && utility.getPassing_percentage() != null && !HomeActivity.mReadResponse.get(k1).getResult().equalsIgnoreCase("") && HomeActivity.mReadResponse.get(k1).getResult() != null)
//                                        if (Integer.parseInt(HomeActivity.mReadResponse.get(k1).getResult()) >= Integer.parseInt(utility.getPassing_percentage())) {
                                        TotalQuizCompleted = TotalQuizCompleted + 1;
                                    break;
//                                        }
                                }
//                                }
                            }
                        }
                    }
                }
            }

        }
        AssSummaryModel model = new AssSummaryModel();
        model.setType("1");
        assSummaryModelArrayList.add(model);
        for (int k = HomeActivity.mReadResponse.size() - 1; k >= 0; k--) {
            if (HomeActivity.mReadResponse.get(k).getResponse_type() != null)
                if (HomeActivity.mReadResponse.get(k).getResponse_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                    AssSummaryModel assSummaryModel = new AssSummaryModel();
                    assSummaryModel.setMaterialId(HomeActivity.mReadResponse.get(k).getMaterial_id());
                    assSummaryModel.setAssignMaterialId(HomeActivity.mReadResponse.get(k).getAssign_material_id());
                    assSummaryModel.setName(HomeActivity.mReadResponse.get(k).getTitle());
                    assSummaryModel.setScore(HomeActivity.mReadResponse.get(k).getResult());
                    assSummaryModel.setType("0");

                    for (int l = 0; l < HomeActivity.courseUtilities.size(); l++) {
                        ArrayList<ModulesUtility> modulesUtilities = HomeActivity.courseUtilities.get(l).getModulesUtilityArrayList();
                        for (int m = 0; m < modulesUtilities.size(); m++) {
                            ArrayList<MeterialUtility> meterialUtilities = modulesUtilities.get(m).getMeterialUtilityArrayList();
                            for (int n = 0; n < meterialUtilities.size(); n++) {
                                MeterialUtility utility = meterialUtilities.get(n);
                                if (utility.getAssign_material_id().equalsIgnoreCase(HomeActivity.mReadResponse.get(k).getAssign_material_id())) {
                                    assSummaryModel.setMeterialUtility(utility);
                                }
                            }
                        }
                    }
                    assSummaryModelArrayList.add(assSummaryModel);
                    ids.add(HomeActivity.mReadResponse.get(k).getAssign_material_id());
//                    ReadResponseUtilityArrayList.add(HomeActivity.mReadResponse.get(k));
                }
        }
        AssSummaryModel model1 = new AssSummaryModel();
        model1.setType("2");
        assSummaryModelArrayList.add(model1);
        int previousSize = assSummaryModelArrayList.size();
        for (int l = 0; l < HomeActivity.courseUtilities.size(); l++) {
            ArrayList<ModulesUtility> modulesUtilities = HomeActivity.courseUtilities.get(l).getModulesUtilityArrayList();
            for (int m = 0; m < modulesUtilities.size(); m++) {
                ArrayList<MeterialUtility> meterialUtilities = modulesUtilities.get(m).getMeterialUtilityArrayList();
                for (int n = 0; n < meterialUtilities.size(); n++) {
                    MeterialUtility utility = meterialUtilities.get(n);
                    if (utility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ) && !ids.contains(utility.getAssign_material_id())) {
                        AssSummaryModel assSummaryModel = new AssSummaryModel();
                        assSummaryModel.setMaterialId(utility.getMaterial_id());
                        assSummaryModel.setAssignMaterialId(utility.getAssign_material_id());
                        assSummaryModel.setName(utility.getTitle());
                        assSummaryModel.setScore(utility.getResult());
                        assSummaryModel.setMeterialUtility(utility);
                        assSummaryModel.setType("3");
                        assSummaryModelArrayList.add(assSummaryModel);
                    }
                }
            }
        }
        int newSize = assSummaryModelArrayList.size();
        if (previousSize == newSize) {
            assSummaryModelArrayList.remove(newSize - 1);
        }

        assTextView.setText((TotalQuiz - TotalQuizCompleted) + "");
        comTextView.setText(TotalQuizCompleted + "");
        AssSummaryAdapter.notifyDataSetChanged();

    }
}