package com.chaptervitamins.myprofile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;

import java.util.ArrayList;

/**
 * Created by test on 23-06-2017.
 */
/*
public class AssSummaryAdapter extends BaseAdapter {
    Context context;
    ListView listView;
    int sizeOfModule;
    ArrayList<AssSummaryModel> assSummaryModelArrayList;;

    public AssSummaryAdapter(Context context, ListView listView,ArrayList<AssSummaryModel> assSummaryModelArrayList) {
        this.context = context;
        this.listView = listView;
        this.assSummaryModelArrayList=assSummaryModelArrayList;
        this.sizeOfModule=assSummaryModelArrayList.size();
    }
    */
/* public void getTeamUtilSize(ArrayList<ModulesUtility> modulesUtilityArrayList){
         this.modulesUtilityArrayList=modulesUtilityArrayList;

     }
*//*

    @Override
    public int getCount() {
        return sizeOfModule;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder(view = LayoutInflater.from(context)
                    .inflate(R.layout.ass_list_row, listView, false));
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.setupView(position);
        return view;
    }

    class ViewHolder {
        int position;
        @Bind(R.id.matTxtView)
        TextView matTxtView;
        @Bind(R.id.scoreTxtView)
        TextView scoreTxtView;

        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);

        }

        void setupView(int position) {

            matTxtView.setText(ReadResponseUtilityArrayList.get(position).getTitle());

            scoreTxtView.setText(ReadResponseUtilityArrayList.get(position).getResult());

        }


    }
}
*/

public class AssSummaryAdapter extends RecyclerView.Adapter<AssSummaryAdapter.ViewHolder> {

    private ArrayList<AssSummaryModel> assSummaryModels;
    private Context mContext;
    private final int TYPE_ITEM = 0;
    private final int TYPE_HEADER = 1;

    public AssSummaryAdapter(ArrayList<AssSummaryModel> assSummaryModels) {
        this.assSummaryModels = assSummaryModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        View view = null;
        switch (viewType) {
            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.ass_list_row, parent, false);
                break;
            case TYPE_HEADER:
                view = LayoutInflater.from(mContext).inflate(R.layout.ass_header_layout, parent, false);
                break;
        }

        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            AssSummaryModel assSummaryModel = assSummaryModels.get(position);
            if (assSummaryModel != null) {
                switch (assSummaryModel.getType()) {
                    case "0":
                        holder.matTxtView.setText(assSummaryModel.getName());
                        holder.scoreTxtView.setText(assSummaryModel.getScore());
                        break;
                    case "1":
                        holder.tvEarnMore.setText("Completed Assessment");
                        holder.tvStatus.setText("% Score");
                        break;
                    case "2":
                        holder.tvEarnMore.setText("Pending Assessment");
                        holder.tvStatus.setText("Status");
                        break;
                    case "3":
                        holder.matTxtView.setText(assSummaryModel.getName());
                        holder.scoreTxtView.setText("Pending");
                        break;
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (assSummaryModels.get(position).getType()) {
            case "0":
                return TYPE_ITEM;
            case "1":
            case "2":
                return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return assSummaryModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView matTxtView, tvStatus;
        TextView scoreTxtView, tvEarnMore;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == TYPE_HEADER) {
                tvEarnMore = (TextView) itemView.findViewById(R.id.tv_earn_more);
                tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            } else if (viewType == TYPE_ITEM) {
                matTxtView = (TextView) itemView.findViewById(R.id.matTxtView);
                scoreTxtView = (TextView) itemView.findViewById(R.id.scoreTxtView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogUtils.showDialog(mContext, "Are you sure to open this material?", new Runnable() {
                            @Override
                            public void run() {
                                FlowingCourseUtils.openMaterial(mContext, assSummaryModels.get(getAdapterPosition()).getMeterialUtility());
                            }
                        }, null);

                    }
                });

            }

        }
    }
}
