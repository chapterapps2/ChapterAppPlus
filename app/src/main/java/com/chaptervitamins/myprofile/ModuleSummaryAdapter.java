package com.chaptervitamins.myprofile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by test on 10-02-2017.
 */
public class ModuleSummaryAdapter extends BaseAdapter {
    Context context;
    ListView listView;
    int sizeOfModule;
    ArrayList<ModulesUtility> modulesUtilityListView;

    public ModuleSummaryAdapter(Context context, ListView listView, ArrayList<ModulesUtility> modulesUtilityListView) {
        this.context = context;
        this.listView = listView;
        this.modulesUtilityListView = modulesUtilityListView;
        this.sizeOfModule = modulesUtilityListView.size();
    }

    /* public void getTeamUtilSize(ArrayList<ModulesUtility> modulesUtilityArrayList){
         this.modulesUtilityArrayList=modulesUtilityArrayList;

     }
*/
    @Override
    public int getCount() {
        return sizeOfModule;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder(view = LayoutInflater.from(context)
                    .inflate(R.layout.mod_list_row_layout, listView, false));
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.setupView(position);
        return view;
    }

    class ViewHolder {
        int position;
        @BindView(R.id.moduleTxtView)
        TextView moduleTxtView;
        @BindView(R.id.satusTxtView)
        TextView satusTxtView;
        @BindView(R.id.comTxtView)
        TextView comTxtView;
        @BindView(R.id.accTxtView)
        TextView accTxtView;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);

        }

        void setupView(int position) {

            moduleTxtView.setText(modulesUtilityListView.get(position).getModule_name());
            if (modulesUtilityListView.get(position).getTotalProgress().equals("100"))
                satusTxtView.setText("Completed");
            else
                satusTxtView.setText("Assigned");
            comTxtView.setText(modulesUtilityListView.get(position).getCompletion_per());
            accTxtView.setText(Utils.getQuizEndDateNewFormat(modulesUtilityListView.get(position).getLastaccessed()));
        }


    }
}
