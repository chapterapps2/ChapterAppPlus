package com.chaptervitamins.myprofile;

/**
 * Created by Vijay Antil on 31-07-2017.
 */

public class MaterialCoinModel  {
    private String materialId,materialName,earnedCoins,totalCoins,type,headerType,status;

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getEarnedCoins() {
        return earnedCoins;
    }

    public void setEarnedCoins(String earnedCoins) {
        this.earnedCoins = earnedCoins;
    }

    public String getTotalCoins() {
        return totalCoins;
    }

    public void setTotalCoins(String totalCoins) {
        this.totalCoins = totalCoins;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHeaderType() {
        return headerType;
    }

    public void setHeaderType(String headerType) {
        this.headerType = headerType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
