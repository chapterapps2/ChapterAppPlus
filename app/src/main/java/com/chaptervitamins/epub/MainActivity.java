/*
The MIT License (MIT)

Copyright (c) 2013, V. Giacometti, M. Giuriato, B. Petrantuono

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

package com.chaptervitamins.epub;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.CustomView.NextMaterial;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.play_video.FlashCardResult;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.EpubModel;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifTextView;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class MainActivity extends BaseActivity implements WebViewListener, View.OnClickListener {
    MenuItem mSearchAction;
    private Menu menu;
    protected EpubNavigator navigator;
    protected int bookSelector = 0;
    protected int panelCount;
    private GifTextView goldGif;
    protected String[] settings;
    static NextMaterial nextMaterial;
    private boolean mSearchOpened;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private MeterialUtility mMeterialUtility;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;

    public static NextMaterial getNextMaterial() {
        return nextMaterial;
    }

    public static void setNextMaterial(NextMaterial nextMaterial) {
        MainActivity.nextMaterial = nextMaterial;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mainepub);
//		Toolbar toolbar = (Toolbar) findViewById(R.id.base_toolbar);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        coinsAllocatedModel = mMeterialUtility.getCoinsAllocatedModel();
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();
//		setSupportActionBar(toolbar);
       /* new Thread(new Runnable() {
            @Override
            public void run() {
                coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(HomeActivity.ASSIGNEDCOURSEID, getIntent().getStringExtra("id"));
                if (coinsAllocatedModel == null) {
                    coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(getIntent().getStringExtra("id"));
                }
                if (coinsAllocatedModel != null) {
                    redeem = coinsAllocatedModel.getRedeem();
                    noOfCoins = coinsAllocatedModel.getMaxCoins();
                } else
                    coinsAllocatedModel = new CoinsAllocatedModel();
            }
        }).start();*/
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(mMeterialUtility.getTitle());
        }
        setFlowingCourseData();
        getModuleData();
        setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
        navigator = new EpubNavigator(1, this);

        panelCount = 0;
        settings = new String[8];
        navigator.openBook(getIntent().getStringExtra("filename"), bookSelector, "");
        // LOADSTATE
//		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
//		loadState(preferences);
//		navigator.loadViews(preferences);
        /*if (panelCount == 0) {
            bookSelector = 0;
			Intent goToChooser = new Intent(this, FileChooser.class);
			`ForResult(goToChooser, 0);
		}*/

    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(mMeterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, mMeterialUtility.getModule_id());
    }
//	@Override
//	protected void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		if (panelCount == 0) {
//			SharedPreferences preferences = getPreferences(MODE_PRIVATE);
//			navigator.loadViews(preferences);
//			onCreate(outState);
//		}
//	}

    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(MainActivity.this);
        if (panelCount == 0) {
            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            navigator.loadViews(preferences);
        }
    }

    private void setFlowingCourseData() {
        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(mMeterialUtility.getMaterial_id(), true);
        position = FlowingCourseUtils.getPositionOfMeterial(mMeterialUtility.getMaterial_id(), meterialUtilityArrayList);
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(MainActivity.this,position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

//		new File(getIntent().getStringExtra("filename")).delete();
//		finish();
    }

    @Override
    public void onBackPressed() {
        if (mSearchOpened) closeSearchBar();
        else
            quiteDialog();
    }

    private void quiteDialog() {
        System.out.println("=====pag count===" + panelCount);
        final DataBase dataBase = DataBase.getInstance(MainActivity.this);
        new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
//				.setTitle("Open")
                .setMessage("eBook Reader will be closed. Click No to stay?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//						new File(getIntent().getStringExtra("filename")).delete();
//						int selectedPos=getIntent().getIntExtra("pos",0);
//						selectedPos++;
//						nextMaterial.nextMaterial(selectedPos);
                        String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility,100, redeem));
                        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
                            showGoldGif();
                            Toast.makeText(MainActivity.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
                        }
                        new WebServices().setProgressOfMaterial(dataBase,mMeterialUtility, "1", "1",coinsCollected);
                        new WebServices().addSubmitResponse(DataBase.getInstance(MainActivity.this),mMeterialUtility, "100", "", "",  "", coinsCollected, redeem,WebServices.mLoginUtility.getOrganization_id(),WebServices.mLoginUtility.getBranch_id());

                        if (WebServices.isNetworkAvailable(MainActivity.this))
                            new SubmitData(MainActivity.this,mMeterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, DataBase.getInstance(MainActivity.this)).execute();
                        else {
                           /* if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                                coinsAllocatedModel.setRedeem("TRUE");
                                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                                    WebServices.updateTotalCoins(MainActivity.this, coinsCollected);

                            }*/
                        }
                        dialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })

                .show();


//		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MultichoiceActivity.this);
//
//		// set title
//
//		// set dialog message
//		alertDialogBuilder.setMessage("eBook Reader will be closed. Click No to stay?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int id) {
//				new File(getIntent().getStringExtra("filename")).delete();
//				dialog.cancel();
//				finish();
//			}
//		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int id) {
//				dialog.cancel();
//			}
//		});
//		// create alert dialog
//		AlertDialog alertDialog = alertDialogBuilder.create();
//
//		// show it
//		alertDialog.show();
    }

    // load the selected book
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		if (panelCount == 0) {
//			SharedPreferences preferences = getPreferences(MODE_PRIVATE);
//			navigator.loadViews(preferences);
//		}
//
//		if (resultCode == Activity.RESULT_OK) {
//			String path = data.getStringExtra(getString(R.string.bpath));
//			navigator.openBook(path, bookSelector,"");
//		}
//	}
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 20) {
            // Extract name value from result extras
            if (data != null) {
                EpubModel epubModel = data.getParcelableExtra(EpubModel.class.getSimpleName());
                navigator.setBookPage(epubModel.topicLink, 0);
//                if(epubModel.topicLink!=null)
//                    navigator.injectJavaScript("javascript:window.location.assign(" + epubModel.topicLink + ")",0, this);

            }
        }

    }


    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }

    // ---- Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
        mSearchAction.setVisible(false);
        menu.findItem(R.id.other_group).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//		int id = item.getItemId();
//		if (id != android.R.id.home)
//			return false;

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.white_mode:
                setColor(getString(R.string.black_color));
                setBackColor(getString(R.string.white_color));
                setCSS();
                return true;
            case R.id.sepia_mode:
                setColor(getString(R.string.white_color));
                setBackColor(getString(R.string.sepia_color));
                setCSS();
                return true;
            case R.id.night_mode:
                setColor(getString(R.string.white_color));
                setBackColor(getString(R.string.black_color));
                setCSS();
                return true;
            case R.id.topics:
                ArrayList<EpubModel> epubModelArrayList = navigator.getTableOfContents(0);
                Intent intent = new Intent(this, EpubTopicsActivity.class);
                intent.putParcelableArrayListExtra(EpubModel.class.getSimpleName(), epubModelArrayList);
                intent.putExtra("title", mMeterialUtility.getTitle());
                startActivityForResult(intent, 20);
                return true;
            case R.id.share:
                String SHARE_TEXT = "<html><body>" +
                        "<p>Hey! I just read this on ChapterVitamins app.</p>" +
                        "<p>" + mMeterialUtility.getTitle() + "</p></body></html>";
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(SHARE_TEXT));
                sendIntent.setType("message/rfc822");
                startActivity(Intent.createChooser(sendIntent, "Share With"));
                return true;
            case R.id.action_search:
                if (mSearchOpened) {
                    closeSearchBar();
                } else {
                    openSearchBar();
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openSearchBar() {
        menu.findItem(R.id.brightness_group).setVisible(false);
        menu.findItem(R.id.other_group).setVisible(false);
        // Set custom view on action bar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.search_menu);

        // Search edit text field setup.
        EditText mSearchEt = (EditText) actionBar.getCustomView()
                .findViewById(R.id.etSearch);
        mSearchEt.addTextChangedListener(new SearchWatcher());
        mSearchEt.setText("");
        mSearchEt.requestFocus();

        // Change search icon accordingly.
//		mSearchAction.setIcon(mIconCloseSearch);
        mSearchOpened = true;

    }

    private void closeSearchBar() {
        menu.findItem(R.id.brightness_group).setVisible(true);
        menu.findItem(R.id.other_group).setVisible(true);
        // Remove custom view.
        getSupportActionBar().setDisplayShowCustomEnabled(false);

        // Change search icon accordingly.
//		mSearchAction.setIcon(mIconOpenSearch);
        mSearchOpened = false;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                FlowingCourseUtils.callFlowingCourseMaterial(MainActivity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                FlowingCourseUtils.callFlowingCourseMaterial(MainActivity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(MainActivity.this) &&
                        mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                  //  showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(MainActivity.this, mMeterialUtility, true);
                break;
        }
    }

    /**
     * Responsible for handling changes in search edit text.
     */
    private class SearchWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence c, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence c, int i, int i2, int i3) {
            System.out.println("=======searchtext====" + c);
//			if (c.length()>2)
//			navigator.getTextFromHtmlFile(c.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }


    }
    // ----

    // ---- Panels Manager
    public void addPanel(SplitPanel p) {

        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.add(R.id.MainLayout, p, p.getTag());
        fragmentTransaction.commit();

        panelCount++;
    }

    public void attachPanel(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.attach(p);
        fragmentTransaction.commit();

        panelCount++;
    }

    public void detachPanel(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.detach(p);
        fragmentTransaction.commit();

        panelCount--;
    }

    //
    public void removePanelWithoutClosing(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.remove(p);
        fragmentTransaction.commit();

        panelCount--;
    }

    public void removePanel(SplitPanel p) {
        FragmentTransaction fragmentTransaction = getFragmentManager()
                .beginTransaction();
        fragmentTransaction.remove(p);
        fragmentTransaction.commit();

        panelCount--;
        if (panelCount <= 0)
            finish();
    }

    // ----

    // ---- Language Selection
    public void chooseLanguage(int book) {

        String[] languages;
        languages = navigator.getLanguagesBook(book);
        if (languages.length == 2)
            refreshLanguages(book, 0, 1);
        else if (languages.length > 0) {
            Bundle bundle = new Bundle();
            bundle.putInt(getString(R.string.tome), book);
            bundle.putStringArray(getString(R.string.lang), languages);

            LanguageChooser langChooser = new LanguageChooser();
            langChooser.setArguments(bundle);
            langChooser.show(getFragmentManager(), "");
        } else {
            errorMessage(getString(R.string.error_noOtherLanguages));
        }
    }

    public void refreshLanguages(int book, int first, int second) {
        navigator.parallelText(book, first, second);
    }

    // ----

    // ---- Change Style
    public void setCSS() {
        navigator.changeCSS(bookSelector, settings);
    }

    public void setBackColor(String my_backColor) {
        settings[1] = my_backColor;
    }

    public void setColor(String my_color) {
        settings[0] = my_color;
    }

    public void setFontType(String my_fontFamily) {
        settings[2] = my_fontFamily;
    }

    public void setFontSize(String my_fontSize) {
        settings[3] = my_fontSize;
    }

    public void setLineHeight(String my_lineHeight) {
        if (my_lineHeight != null)
            settings[4] = my_lineHeight;
    }

    public void setAlign(String my_Align) {
        settings[5] = my_Align;
    }

    public void setMarginLeft(String mLeft) {
        settings[6] = mLeft;
    }

    public void setMarginRight(String mRight) {
        settings[7] = mRight;
    }

    // ----

    // change the views size, changing the weight
    protected void changeViewsSize(float weight) {
        navigator.changeViewsSize(weight);
    }

    public int getHeight() {
        LinearLayout main = (LinearLayout) findViewById(R.id.MainLayout);
        return main.getMeasuredHeight();
    }

    public int getWidth() {
        LinearLayout main = (LinearLayout) findViewById(R.id.MainLayout);
        return main.getWidth();
    }

    // Save/Load State
    protected void saveState(Editor editor) {
        navigator.saveState(editor);
    }

    protected void loadState(SharedPreferences preferences) {
        if (!navigator.loadState(preferences))
            errorMessage(getString(R.string.error_cannotLoadState));
    }

    public void errorMessage(String message) {
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void getSelectedText(String text) {

    }
}
