package com.chaptervitamins.epub;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;


import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.R;
import com.chaptervitamins.utility.EpubModel;

import java.util.ArrayList;

public class EpubTopicsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_epub_topics);
        initializeToolbar();

        ArrayList<EpubModel> epubModels = getEpubModelsFromIntent();
        if (epubModels != null && epubModels.size() > 0)
            showTopicsOnView(epubModels);
    }

    @Override
    protected void onPause() {
//        SharedPreferenceManager.getSharedInstance().savePausedActivityName(this.getClass().getSimpleName());
//        AppUtility.shootBackgroundEvent(this);

        super.onPause();
    }

    @Override
    protected void onResume() {
//        SharedPreferenceManager.getSharedInstance().saveCurrentActivityName(this.getClass().getSimpleName());
//
//        String pausedActivity = SharedPreferenceManager.getSharedInstance().getPausedActivityName();
//        if (pausedActivity != null && pausedActivity.equals(this.getClass().getSimpleName())) {
//            MixPanelManager mixPanelManager = AppUtility.getMixPanelManager(this);
//            mixPanelManager.appComesInForeGround();
//        }
//
//        ErrorManager.getSharedInstance().updateContext(this);
        super.onResume();
    }

    private void initializeToolbar() {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.base_toolbar_layout);
//        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("Topics");
        }
    }

    private void showTopicsOnView(ArrayList<EpubModel> epubModels) {
        final ListView listView = (ListView) findViewById(R.id.epub_topics_listview);
        listView.setAdapter(new EpubTopicsListAdapter(this, epubModels));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                EpubTopicsListAdapter epubTopicsListAdapter = (EpubTopicsListAdapter) listView.getAdapter();
                EpubModel epubModel = epubTopicsListAdapter.getItem(i);

                Intent data = new Intent();
                data.putExtra(EpubModel.class.getSimpleName(), epubModel);
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }

    private ArrayList<EpubModel> getEpubModelsFromIntent() {
        Intent intent = getIntent();
        return intent.getParcelableArrayListExtra(EpubModel.class.getSimpleName());
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_epub_topics, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}