package com.chaptervitamins.epub;

/**
 * Created by arun on 08/10/15.
 */
public interface WebViewListener {
    void getSelectedText(String text);
}
