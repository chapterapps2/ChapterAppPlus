package com.chaptervitamins.epub;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.chaptervitamins.R;
import com.chaptervitamins.utility.EpubModel;

import java.util.ArrayList;

/**
 * Created by arun on 01/10/15.
 */
public class EpubTopicsListAdapter extends BaseAdapter{

    private ArrayList<EpubModel> epubModels;
    private Context context;

    EpubTopicsListAdapter(Context context, ArrayList<EpubModel> epubModels)
    {
        this.context = context;
        this.epubModels = epubModels;
    }

    @Override
    public int getCount() {
        return epubModels.size();
    }

    @Override
    public EpubModel getItem(int i) {
        return epubModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EpubModelListRowHolder holder;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        EpubModel epubModel = epubModels.get(position);

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.epub_topic_list_item, null);
            holder = new EpubModelListRowHolder(convertView);
            convertView.setTag(holder);
        }else
            holder = (EpubModelListRowHolder) convertView.getTag();

        setDataOnView(holder, epubModel);

        return convertView;
    }

    private void setDataOnView(EpubModelListRowHolder holder, EpubModel epubModel) {
        holder.epubTopicName.setText(epubModel.topicName);
    }

    public static class EpubModelListRowHolder {
        TextView epubTopicName;

        public EpubModelListRowHolder(View view) {
            this.epubTopicName = (TextView) view.findViewById(R.id.topic_textview);
        }
    }
}
