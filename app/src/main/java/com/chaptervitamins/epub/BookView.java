/*
The MIT License (MIT)

Copyright (c) 2013, V. Giacometti, M. Giuriato, B. Petrantuono

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package com.chaptervitamins.epub;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.chaptervitamins.utility.JavaScriptUtilityListener;
import com.chaptervitamins.R;


// Panel specialized in visualizing EPUB pages
public class BookView extends SplitPanel implements JavaScriptUtilityListener {
	public ViewStateEnum state = ViewStateEnum.books;
	protected String viewedPage;
	protected float swipeOriginX, swipeOriginY;
	private WebView view;
	private WebViewListener webViewListener;

	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)	{
		super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate(R.layout.activity_book_view, null, false);
	}

	@Override
    public void onActivityCreated(Bundle saved) {
		super.onActivityCreated(saved);
		view = (WebView) getView().findViewById(R.id.Viewport);
		view.findNext(true);
//		view.findAll("the");
		enableWebViewSettings();

//		view.setOnScrollChangedCallback(new ObservableWebView.OnScrollChangedCallback() {
//			@Override
//			public void onScroll(int oldX, int x, int oldY, int y) {
//				if (y % 500 >= 450)
//					Log.e("ScrollState", "Scroll Event");
//			}
//		});

//		mTextSelectionSupport = TextSelectionSupport.support(getActivity(), view);
//		mTextSelectionSupport.setSelectionListener(new TextSelectionSupport.SelectionListener() {
//			@Override
//			public void startSelection() {
//			}
//
//			@Override
//			public void selectionChanged(String text) {
//				navigator.setSelectedText(text);
//			}
//
//			@Override
//			public void endSelection() {
//			}
//		});

//		// ----- SWIPE PAGE
//		view.setOnTouchListener(new OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//
//				if (state == ViewStateEnum.books)
//					swipePage(v, event, 0);
//
//				WebView view = (WebView) v;
//				return view.onTouchEvent(event);
//			}
//		});
//
		JavaScriptUtility javaScriptUtility = new JavaScriptUtility();
		javaScriptUtility.setJavaScriptUtilityListener(this);
		view.addJavascriptInterface(javaScriptUtility, "Android");
		view.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				try {
					navigator.setBookPage(url, index);
				} catch (Exception e) {
					errorMessage(getString(R.string.error_LoadPage));
				}
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				if(url.contains("#"))
//					view.loadUrl("javascript:location.hash = '#" + url.substring(url.indexOf("#")+1) + "';");
					view.loadUrl("javascript:window.location="+url);

				//This line is used to give the zooming feature for loading page in web view.
				view.loadUrl("javascript: (function() { var viewPort = document.querySelector('meta[name=viewport]'); " +
						"if(viewPort != null) " +
						"viewPort.setAttribute('content','width = 350, minimum-scale = 1, maximum-scale = 10'); " +
						"else{ " +
						"var metaTag = document.createElement('meta'); " +
						"metaTag.setAttribute('name', 'viewport'); " +
						"metaTag.setAttribute('content', 'width = 350, minimum-scale = 1, maximum-scale = 10'); " +
						"document.getElementsByTagName('head')[0].appendChild(metaTag); }" +
						"})()");


// "var style = document.createElement('style');"+
////					"style.appendChild(document.createTextNode(''));"+
////					"document.head.appendChild(style);"+
////					"return style.sheet;"+
////					"}" +
////					"function addCSSRule(selector, newRule) {" +
////					"if (getSheet().addRule) {"+
////					"getSheet().addRule(selector, newRule);"+
////					"} else {"+
////					"ruleIndex = getSheet().cssRules.length;"+
////					"getSheet().insertRule(selector + '{' + newRule + ';}', ruleIndex);"+
////					"}"+
////					"} " +
//						"var head = document.createElement('head');" +
//						"var script = document.createElement('script');" +
//						"script.type = 'text/javascript';script.src = 'file:///android_asset/WebViewUtility.js';" +
//						"document.getElementsByTagName('html').item(0).appendChild(head);" +
//						"document.getElementsByTagName('head').item(0).appendChild(script);" +
////					"var style = document.createElement('style');"+
////					"style.rel  = 'stylesheet';style.type = 'text/css';style.src = 'file:///android_asset/WebView.css';"+
////					"style.appendChild(document.createTextNode('webview'));"+
////					"document.head.appendChild(style);"+
//						"})()");

			}

		});

		loadPage(viewedPage);
	}

	private void enableWebViewSettings() {
		if (view == null)
			return;

		WebSettings webSettings = view.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDisplayZoomControls(true);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setUseWideViewPort(true);
	}

	public void loadPage(String path)
	{
		viewedPage = path;
		if(created)
			view.loadUrl(path);
	}

	public void setWebViewListener(WebViewListener webViewListener)
	{
		this.webViewListener = webViewListener;
	}

	// Change page
	protected void swipePage(View v, MotionEvent event, int book) {
		int action = MotionEventCompat.getActionMasked(event);

		switch (action) {
		case (MotionEvent.ACTION_DOWN):
			swipeOriginX = event.getX();
			swipeOriginY = event.getY();
			break;

		case (MotionEvent.ACTION_UP):
			int quarterWidth = (int) (screenWidth * 0.25);
			float diffX = swipeOriginX - event.getX();
			float diffY = swipeOriginY - event.getY();
			float absDiffX = Math.abs(diffX);
			float absDiffY = Math.abs(diffY);

			if ((diffX > quarterWidth) && (absDiffX > absDiffY)) {
				try {
					navigator.goToNextChapter(index);
				} catch (Exception e) {
					errorMessage(getString(R.string.error_cannotTurnPage));
				}
			} else if ((diffX < -quarterWidth) && (absDiffX > absDiffY)) {
				try {
					navigator.goToPrevChapter(index);
				} catch (Exception e) {
					errorMessage(getString(R.string.error_cannotTurnPage));
				}
			}
			break;
		}

	}
	
	@Override
	public void saveState(Editor editor) {
		super.saveState(editor);
		editor.putString("state"+index, state.name());
		editor.putString("page"+index, viewedPage);
	}
	
	@Override
	public void loadState(SharedPreferences preferences)
	{
		super.loadState(preferences);
		loadPage(preferences.getString("page"+index, ""));
		state = ViewStateEnum.valueOf(preferences.getString("state" + index, ViewStateEnum.books.name()));
	}

	@Override
	public void getSelectedText(String value) {

		if(webViewListener!=null)
			webViewListener.getSelectedText(value);
	}

	@Override
	public void getCompleteHtmlTextInWebview(String value) {

	}

	@Override
	public void getNoteIdOfNoteClicked(String value) {

	}

	@Override
	public void getHighlightsRemovedText(String value) {

	}
}
