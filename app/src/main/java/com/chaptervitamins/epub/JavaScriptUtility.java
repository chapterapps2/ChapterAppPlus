package com.chaptervitamins.epub;

import android.webkit.JavascriptInterface;

import com.chaptervitamins.utility.JavaScriptUtilityListener;

public class JavaScriptUtility
{
	private JavaScriptUtilityListener listener;
	
	public void setJavaScriptUtilityListener(JavaScriptUtilityListener listener)
	{
		this.listener = listener;
	}
	
	@JavascriptInterface
	public void getSeletedText(String value)
	{
		listener.getSelectedText(value);
	}
	
	@JavascriptInterface
	public void getCompleteHtmlTextInWebview(String value)
	{
		listener.getCompleteHtmlTextInWebview(value);
	}
	
	@JavascriptInterface
	public void getNoteIdOfNoteClicked(String value)
	{
		listener.getNoteIdOfNoteClicked(value);
	}
	
	@JavascriptInterface
	public void getHighlightsRemovedText(String value)
	{
		listener.getHighlightsRemovedText(value);
	}
	
}