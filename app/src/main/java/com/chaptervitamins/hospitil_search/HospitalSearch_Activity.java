package com.chaptervitamins.hospitil_search;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.Hospital_Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class HospitalSearch_Activity extends BaseActivity {
    private ImageView search_img, back;
    private LinearLayout search_ll, cross_ll;
    private EditText search_edt;
    private boolean isOpen = false;
    private RecyclerView search_listview;
    private TextView title;
    private WebServices webServices;
    private HospitalAdapter hospitalAdapter;
    private ArrayList<Hospital_Utils> hospital_utilsArrayList = new ArrayList<>();
    private Spinner provider_spinner, city_spinner, hospital_spinner;
    ArrayAdapter<String> providerAdapter, cityAdapter, hospital_Adapter;
    ArrayList<String> hospital_name = new ArrayList<>();
    String provider_Selected = "", city_Selected = "", hospital_Selected = "";
    private Button search_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_search);
        search_img = (ImageView) findViewById(R.id.search_img);
        back = (ImageView) findViewById(R.id.back);
        search_ll = (LinearLayout) findViewById(R.id.search_ll);
        cross_ll = (LinearLayout) findViewById(R.id.cross_ll);
        search_listview = (RecyclerView) findViewById(R.id.search_listview);
        title = (TextView) findViewById(R.id.title);
        title.setText(getIntent().getStringExtra("title"));
        search_edt = (EditText) findViewById(R.id.search_edt);
        provider_spinner = (Spinner) findViewById(R.id.provider_spinner);
        city_spinner = (Spinner) findViewById(R.id.city_spinner);
        hospital_spinner = (Spinner) findViewById(R.id.hospital_spinner);
        webServices = new WebServices();
        search_btn = (Button) findViewById(R.id.search_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIUtility.hideKeyboard(HospitalSearch_Activity.this);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isOpen) {
                            isOpen = false;
                            slideToTop(search_ll);

                        } else {
                            isOpen = true;
                            search_edt.setText("");
                            provider_spinner.setSelection(0);
                            city_spinner.setSelection(0);
                            hospital_spinner.setSelection(0);
                            slideToBottom(search_ll);
//                            hospitalAdapter.filter("");
                        }
                    }
                });
            }
        });
        cross_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIUtility.hideKeyboard(HospitalSearch_Activity.this);
                hospitalAdapter.filter(search_edt.getText().toString());


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isOpen) {
                            isOpen = false;
                            slideToTop(search_ll);
                        } else {
                            isOpen = true;
                            search_edt.setText("");
                            provider_spinner.setSelection(0);
                            city_spinner.setSelection(0);
                            hospital_spinner.setSelection(0);
                            slideToBottom(search_ll);
                        }
                    }
                });

            }
        });
        hospital_utilsArrayList.addAll(WebServices.hospital_utilses);
        int size = hospital_utilsArrayList.size();
        if (size > 50) {
            hospital_utilsArrayList.subList(50, size).clear();
        }
        hospitalAdapter = new HospitalAdapter(HospitalSearch_Activity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        search_listview.setLayoutManager(mLayoutManager);
        search_listview.setItemAnimator(new DefaultItemAnimator());
        search_listview.setAdapter(hospitalAdapter);
        Collections.sort(WebServices.hospital_types);
        WebServices.hospital_types.add(0, "Select Provider");

        WebServices.hospital_city.add(0, "Select City");
        hospital_name.add("Select Hospital");
        // Creating adapter for spinner
        providerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, WebServices.hospital_types);

        // Drop down layout style - list view with radio button
        providerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        provider_spinner.setAdapter(providerAdapter);
        cityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, WebServices.hospital_city);

        // Drop down layout style - list view with radio button
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        city_spinner.setAdapter(cityAdapter);
        hospital_Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, hospital_name);

        // Drop down layout style - list view with radio button
        hospital_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        hospital_spinner.setAdapter(hospital_Adapter);
        provider_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                city_spinner.setSelection(0);
                provider_Selected = WebServices.hospital_types.get(position);

                if (provider_Selected.equalsIgnoreCase("select provider")) {
                    city_spinner.setSelection(0);
                } else {
                    WebServices.hospital_city = new ArrayList<String>();
                    for (int i = 0; i < WebServices.hospital_utilses.size(); i++) {
                        if (provider_Selected.equalsIgnoreCase(WebServices.hospital_utilses.get(i).getHospital_type()))
                            if (!WebServices.hospital_city.contains(WebServices.hospital_utilses.get(i).getCity()))
                                WebServices.hospital_city.add(WebServices.hospital_utilses.get(i).getCity());
                    }
                    Collections.sort(WebServices.hospital_city);
                    WebServices.hospital_city.add(0, "Select City");
                    cityAdapter = new ArrayAdapter<String>(HospitalSearch_Activity.this, android.R.layout.simple_spinner_item, WebServices.hospital_city);

                    // Drop down layout style - list view with radio button
                    cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    city_spinner.setAdapter(cityAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                city_Selected = WebServices.hospital_city.get(position);
                if (WebServices.hospital_city.get(position).equalsIgnoreCase("select city")) {
                    hospital_spinner.setSelection(0);
                } else {
                    hospital_name = new ArrayList<String>();
                    for (int i = 0; i < WebServices.hospital_utilses.size(); i++) {
                        if (provider_Selected.equalsIgnoreCase(WebServices.hospital_utilses.get(i).getHospital_type()))
                            if (WebServices.hospital_utilses.get(i).getCity().equalsIgnoreCase(WebServices.hospital_city.get(position)))
                                if (!hospital_name.contains(WebServices.hospital_utilses.get(i).getHospital_name()))
                                    hospital_name.add(WebServices.hospital_utilses.get(i).getHospital_name());
                    }
                    Collections.sort(hospital_name);
                    hospital_name.add(0, "Select Hospital");
                    hospital_Adapter = new ArrayAdapter<String>(HospitalSearch_Activity.this, android.R.layout.simple_spinner_item, hospital_name);

                    // Drop down layout style - list view with radio button
                    hospital_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    hospital_spinner.setAdapter(hospital_Adapter);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        hospital_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hospital_Selected = hospital_name.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIUtility.hideKeyboard(HospitalSearch_Activity.this);
                if (!hospital_Selected.equalsIgnoreCase("") && !hospital_Selected.equalsIgnoreCase("Select Hospital")) {
                    hospitalAdapter.filterbyName(provider_Selected, city_Selected, hospital_Selected);
                } else if (!city_Selected.equalsIgnoreCase("") && !city_Selected.equalsIgnoreCase("Select City")) {
                    hospitalAdapter.filterbyCity(provider_Selected, city_Selected);
                } else if (!provider_Selected.equalsIgnoreCase("") && !provider_Selected.equalsIgnoreCase("Select Provider")) {
                    hospitalAdapter.filterbytype(provider_Selected);
                } else {
                    hospitalAdapter.filter("");
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isOpen) {
                            isOpen = false;
                            slideToTop(search_ll);
                        } else {
                            isOpen = true;
                            search_edt.setText("");
                            hospital_Selected = "";
                            city_Selected = "";
                            hospital_Selected = "";
                            provider_spinner.setSelection(0);
                            city_spinner.setSelection(0);
                            hospital_spinner.setSelection(0);
                            slideToBottom(search_ll);
                        }
                    }
                });
            }
        });
    }


    // To animate view slide out from top to bottom
    public void slideToBottom(View view) {
        TranslateAnimation animate = new TranslateAnimation(0, 0, 0, -view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    // To animate view slide out from bottom to top
    public void slideToTop(View view) {
        TranslateAnimation animate = new TranslateAnimation(0, 0, -view.getHeight(), 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }


    public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.MyViewHolder> {

        private Context mContext;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, count;
            public ImageView thumbnail, overflow;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.hospital_name);
                count = (TextView) view.findViewById(R.id.hospital_address);

            }
        }


        public HospitalAdapter(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.hospital_list_row, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            Hospital_Utils utils = hospital_utilsArrayList.get(position);
            holder.title.setText(utils.getHospital_name());
            holder.count.setText(utils.getAddress() + "," + utils.getCity() + "," + utils.getState() + ",Pin - " + utils.getPincode());

            // loading album cover using Glide library
//            Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

//            holder.overflow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    showPopupMenu(holder.overflow);
//                }
//            });
        }

        /**
         * Showing popup menu when tapping on 3 dots
         */
//        private void showPopupMenu(View view) {
//            // inflate menu
//            PopupMenu popup = new PopupMenu(mContext, view);
//            MenuInflater inflater = popup.getMenuInflater();
//            inflater.inflate(R.menu.menu_album, popup.getMenu());
//            popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
//            popup.show();
//        }
//
//        /**
//         * Click listener for popup menu items
//         */
//        class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
//
//            public MyMenuItemClickListener() {
//            }
//
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                switch (menuItem.getItemId()) {
//                    case R.id.action_add_favourite:
//                        Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
//                        return true;
//                    case R.id.action_play_next:
//                        Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
//                        return true;
//                    default:
//                }
//                return false;
//            }
//        }
// Filter Class
        public void filter(String charText) {
            if (!TextUtils.isEmpty(charText)) {
                charText = charText.toLowerCase(Locale.getDefault());
                hospital_utilsArrayList = new ArrayList<>();
                if (charText.length() == 0) {
                    hospital_utilsArrayList.addAll(WebServices.hospital_utilses);
                } else {
                    for (Hospital_Utils wp : WebServices.hospital_utilses) {

                        if (wp.getHospital_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                            hospital_utilsArrayList.add(wp);
                        } else if (wp.getAddress().toLowerCase(Locale.getDefault()).contains(charText)) {
                            hospital_utilsArrayList.add(wp);
                        } else if (wp.getCity().toLowerCase(Locale.getDefault()).contains(charText)) {
                            hospital_utilsArrayList.add(wp);
                        } else if (wp.getState().toLowerCase(Locale.getDefault()).contains(charText)) {
                            hospital_utilsArrayList.add(wp);
                        } else if (wp.getPincode().toLowerCase(Locale.getDefault()).contains(charText)) {
                            hospital_utilsArrayList.add(wp);
                        }
                    }
                }

                if (hospital_utilsArrayList.size() == 0)
                    Toast.makeText(HospitalSearch_Activity.this, "No result found", Toast.LENGTH_LONG).show();
                else {
                    int size = hospital_utilsArrayList.size();
                    if (size > 50) {
                        hospital_utilsArrayList.subList(50, size).clear();
                    }
                }
                notifyDataSetChanged();
            } else
                Toast.makeText(HospitalSearch_Activity.this, "Please select any search field option.", Toast.LENGTH_LONG).show();
        }

        // Filter Class
        public void filterbytype(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            hospital_utilsArrayList = new ArrayList<>();
            if (charText.length() == 0) {
                hospital_utilsArrayList.addAll(WebServices.hospital_utilses);
            } else {
                for (Hospital_Utils wp : WebServices.hospital_utilses) {


                    if (wp.getHospital_type().toLowerCase(Locale.getDefault()).contains(charText)) {
                        hospital_utilsArrayList.add(wp);
                    }
                }
            }
            if (hospital_utilsArrayList.size() == 0)
                Toast.makeText(HospitalSearch_Activity.this, "No result found", Toast.LENGTH_LONG).show();
            int size = hospital_utilsArrayList.size();
            if (size > 50) {
                hospital_utilsArrayList.subList(50, size).clear();
            }
            notifyDataSetChanged();
        }

        // Filter Class
        public void filterbyCity(String charText, String city) {
            charText = charText.toLowerCase(Locale.getDefault());
            hospital_utilsArrayList = new ArrayList<>();
            if (charText.length() == 0) {
                hospital_utilsArrayList.addAll(WebServices.hospital_utilses);
            } else {
                for (Hospital_Utils wp : WebServices.hospital_utilses) {


                    if (wp.getHospital_type().toLowerCase(Locale.getDefault()).contains(charText) && wp.getCity().toLowerCase(Locale.getDefault()).contains(city)) {
                        hospital_utilsArrayList.add(wp);
                    }
                }
            }
            if (hospital_utilsArrayList.size() == 0)
                Toast.makeText(HospitalSearch_Activity.this, "No result found", Toast.LENGTH_LONG).show();
            int size = hospital_utilsArrayList.size();
            if (size > 50) {
                hospital_utilsArrayList.subList(50, size).clear();
            }
            notifyDataSetChanged();
        }

        // Filter Class
        public void filterbyName(String charText, String city, String name) {
            charText = charText.toLowerCase(Locale.getDefault());
            hospital_utilsArrayList = new ArrayList<>();
            if (charText.length() == 0) {
                hospital_utilsArrayList.addAll(WebServices.hospital_utilses);
            } else {
                for (Hospital_Utils wp : WebServices.hospital_utilses) {


                    if (wp.getHospital_type().toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText))
                        if (wp.getCity().toLowerCase(Locale.getDefault()).equalsIgnoreCase(city))
                            if (wp.getHospital_name().toLowerCase(Locale.getDefault()).equalsIgnoreCase(name))
                                hospital_utilsArrayList.add(wp);

                }
            }
            if (hospital_utilsArrayList.size() == 0)
                Toast.makeText(HospitalSearch_Activity.this, "No result found", Toast.LENGTH_LONG).show();
            int size = hospital_utilsArrayList.size();
            if (size > 50) {
                hospital_utilsArrayList.subList(50, size).clear();
            }
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return hospital_utilsArrayList.size();
        }
    }


}
