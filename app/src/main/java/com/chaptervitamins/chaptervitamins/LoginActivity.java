package com.chaptervitamins.chaptervitamins;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.OTPListener;
import com.chaptervitamins.CustomView.OtpReader;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.home.ChangePass_Activity;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.signup.SignupActivity;
import com.chaptervitamins.utility.LoginUtility;
import com.facebook.CallbackManager;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.splunk.mint.Mint;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements OTPListener {
    @BindView(R.id.forgetpassword_txt)
    TextView forgetpassword_txt;
    @BindView(R.id.resend_txt)
    TextView resend_txt;
    @BindView(R.id.send_txt)
    TextView send_txt;
    @BindView(R.id.timer_txt)
    TextView timer_txt;
    /*  @BindView(R.id.verText)
      TextView verText;*/
    EditText email_edt, mobile_no_edt, otp_edt;
    EditText password_edt;
    //    Typeface face;
    LinearLayout send_otp_ll;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private UserMobileLoginTask mobileAuthTask = null;
    private DataBase dataBase;
    // UI references.
    private View mProgressView;
    private View mLoginFormView;
    private Handler handler;
    private WebServices webServices;
    //    private Dialog dialog;
    private ProgressDialog mDialog = null;
    private long TIMERVALUE = 15;
    private boolean TimerVal = true;
    private Handler handler2 = new Handler();
    private String ip = "";
    private String refreshedToken;
    TelephonyManager telemananger = null;
    private final int REQUEST_READ_PHONE_STATE = 1;
    MixPanelManager mixPanelManager;
    private static final String EMAIL = "email";
    private CallbackManager callbackManager;
    //  TextView orgName;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    private GoogleApiClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        // TODO: Update with your API key
        Mint.initAndStartSession(LoginActivity.this, Constants.CRASH_REPORT_KEY);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        OtpReader.bind(LoginActivity.this, "-CHAPTR");
        APIUtility.SESSION = "";

        mixPanelManager = APIUtility.getMixPanelManager(LoginActivity.this);
        FirebaseApp.initializeApp(LoginActivity.this);
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        email_edt = (EditText) findViewById(R.id.email_edit_text);
      /*  verText = (TextView) findViewById(R.id.verText);
        orgName = (TextView) findViewById(R.id.org_name_txt);*/
        password_edt = (EditText) findViewById(R.id.password_edit_text);
        dataBase = DataBase.getInstance(LoginActivity.this);
        webServices = new WebServices();
        //    Constants.getAppVersion(LoginActivity.this,verText);
        // orgName.setText("Welcome to\n " + getString(R.string.app_name));
//        face = Typeface.createFromAsset(getAssets(), "Raleway_SemiBold.ttf");
//        email_edt.setTypeface(face);
//        password_edt.setTypeface(face);
        @SuppressLint("WifiManagerLeak") WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
        ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
//        Typeface face2 = Typeface.createFromAsset(getAssets(), "Raleway_SemiBold.ttf");
//        mEmailSignInButton.setTypeface(face2);

        mobile_no_edt = (EditText) findViewById(R.id.mobile_edit_text);
        otp_edt = (EditText) findViewById(R.id.mobile_password_edit_text);
        send_otp_ll = (LinearLayout) findViewById(R.id.send_otp_ll);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (!getIntent().getStringExtra("sms_url").equalsIgnoreCase("")) {
                final Dialog dialog = new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.logout_dialog);
                TextView title_dialog = (TextView) dialog.findViewById(R.id.title_dialog);
                TextView desc_dialog = (TextView) dialog.findViewById(R.id.desc_dialog);
                title_dialog.setText("Login");
                desc_dialog.setText("To continue, Please Login to the " + getString(R.string.app_name) + ". Press OK");
                Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
                Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                ok_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dialog != null && !isFinishing())
                            dialog.dismiss();

                    }
                });
                dialog.show();
            }
        }


//        mobile_no_edt.setTypeface(face);
//        otp_edt.setTypeface(face);

//        dialog = new Dialog(LoginActivity.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.smssautofetch_dialog);
//        Button cancel_btn=(Button)dialog.findViewById(R.id.cancel_btn);
//        cancel_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TimerVal=false;
//                resend_txt.setEnabled(true);
//                dialog.dismiss();
//            }
//        });
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
//            profile_name_txt.setTextSize(12);
            //   ((TextView) findViewById(R.id.org_name_txt)).setTextSize(18);
            email_edt.setTextSize(12);
            password_edt.setTextSize(12);
            mobile_no_edt.setTextSize(12);
            otp_edt.setTextSize(12);
            ((TextView) findViewById(R.id.send_txt)).setTextSize(14);
        }

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    telemananger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    APIUtility.SESSION = "";
                    APIUtility.DEVICEID = telemananger.getDeviceId().toString();
                    if (!email_edt.getText().toString().equalsIgnoreCase("")) {
                        attemptLogin(ip);
                    } else if (!mobile_no_edt.getText().toString().equalsIgnoreCase("")) {
                        mobileattemptLogin(ip);
                    } else {
                        Toast.makeText(LoginActivity.this, "Please enter valid Email Id or Mobile No.", Toast.LENGTH_LONG).show();
                    }
                } else
                    checkPhoneStatePermission();
            }
        });
        if(Constants.ORGANIZATION_ID.equalsIgnoreCase("33")&&
                Constants.BRANCH_ID.equalsIgnoreCase("45")) {
            findViewById(R.id.signUpButton).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                    startActivity(intent);
                }
            });
            //userFBLoginTask();

        }
        changeStatusBarColor();
        email_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mobile_no_edt.getText().toString().equalsIgnoreCase("")) {
                    mobile_no_edt.setText("");
                    otp_edt.setText("");
                    send_txt.setText("Send OTP");
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        mobile_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!email_edt.getText().toString().equalsIgnoreCase("")) {
                    email_edt.setText("");
                    password_edt.setText("");

                }
                otp_edt.setText("");
                mobile_no_edt.setEnabled(true);
                send_otp_ll.setEnabled(true);
                send_txt.setEnabled(true);
                send_txt.setText("Send OTP");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //for OTP CODE

//        String first = "Not Recieved OTP ? ";
//        String next = "<font color='#c51b2b'><u>Resend OTP</u></font>";
//        resend_txt.setText(Html.fromHtml(first + next));
//        resend_txt.setEnabled(true);
        send_otp_ll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                startApplication("com..chap_tempone.chaptervitamins");
                final String email = mobile_no_edt.getText().toString();

                // Check for a valid email address.
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_email), Toast.LENGTH_LONG).show();
                    return;
                }
                if (email.length() < 10) {
                    Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_email), Toast.LENGTH_LONG).show();
                    return;
                }
                // Check for a valid email address.
                if (!isValidPhoneNumber(email)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_email), Toast.LENGTH_LONG).show();
                    return;
                }
                TIMERVALUE = 15;
                TimerVal = true;
                if (WebServices.isNetworkAvailable(LoginActivity.this)) {
                    OtpReader.bind(LoginActivity.this, "-CHAPTR");
                    APIUtility.hideKeyboard(LoginActivity.this);
                    if (send_txt.getText().toString().trim().equalsIgnoreCase("send otp"))
                        sendOTP(email, false);
                    else {

                        sendOTP(email, true);
                    }
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });
        resend_txt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mobile_no_edt.getText().toString();
                TIMERVALUE = 15;
                TimerVal = true;
                // Check for a valid email address.
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_email), Toast.LENGTH_LONG).show();
                    return;
                }
                // Check for a valid email address.
                if (!isValidPhoneNumber(email)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_email), Toast.LENGTH_LONG).show();
                    return;
                }
                if (WebServices.isNetworkAvailable(LoginActivity.this)) {
                    APIUtility.hideKeyboard(LoginActivity.this);
                    sendOTP(email, true);
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });

    }



    /* Fb Login task-----------------
    private void userFBLoginTask() {
        findViewById(R.id.login_button).setReadPermissions("email","public_profile");
        callbackManager = CallbackManager.Factory.create();
        //updateHashKey();
        findViewById(R.id.login_button).registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String userId=loginResult.getAccessToken().getUserId();
                GraphRequest graphRequest=GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        object.optString("");
                    }
                });

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                error.toString();
            }
        });


    }*/

    private void updateHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.chaptervitamins.upsccms", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.forgetpassword_txt)
    public void forgotPassword() {
        Intent intent = new Intent(LoginActivity.this, ForgetPassword_Activity.class);
        startActivity(intent);
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private void sendOTP(final String mobileno, final boolean isResend) {
        final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "", "Sending OTP...");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (dialog != null && dialog.isShowing() && !isFinishing()) dialog.dismiss();
                if (msg.what == 1)
                    Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_SHORT).show();
                else {
                    Toast.makeText(LoginActivity.this, "OTP has been sent to your mobile number.", Toast.LENGTH_SHORT).show();
                    send_txt.setEnabled(false);
                    send_otp_ll.setEnabled(false);
                    startTime();
//                    LoginActivity.this.dialog.show();
//                    mDialog = ProgressDialog.show(LoginActivity.this, "", "Please wait to Verify OTP...");
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("mobile_no", mobileno));
//            nameValuePair.add(new BasicNameValuePair("otp_code", mPassword));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                if (!Constants.ORGANIZATION_ID.equals("1") || Constants.BRANCH_ID.equals("25")) {    //---branch_id condition for panasonic
                    nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
                    if (Constants.BRANCH_ID.equals("25"))
                        nameValuePair.add(new BasicNameValuePair("branch_id", Constants.BRANCH_ID));
                }
                String resp = "";
                if (isResend)
                    resp = webServices.getLogin(nameValuePair, APIUtility.RESEND_OTP);
                else
                    resp = webServices.getLogin(nameValuePair, APIUtility.SEND_OTP);
                Log.d("Response:", resp.toString());
                boolean isSuccess = webServices.parseOTP(resp);
                if (isSuccess)
                    handler.sendEmptyMessage(0);
                else
                    handler.sendEmptyMessage(1);
            }
        }.start();
    }

    private void reset_Device(final String mobileno, final String email) {
        final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "", "Please wait...");
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (dialog != null && !isFinishing()) dialog.dismiss();

                if (msg.what == 0) {
                    final Dialog dialog = new Dialog(LoginActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.reset_device_dialog);
                    Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
                    TextView desc_txt = (TextView) dialog.findViewById(R.id.desc_dialog);
                    desc_txt.setText(WebServices.ERRORMSG);
                    ok_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (dialog != null && !isFinishing())
                                dialog.dismiss();
                        }
                    });

                    dialog.show();
                } else {
                    Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_SHORT).show();
                }

            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("mobile_no", mobileno));
                nameValuePair.add(new BasicNameValuePair("email", email));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                String resp = "";
                resp = webServices.getLogin(nameValuePair, APIUtility.RESET_DEVICE);
                Log.d(" Response:", resp.toString());
                boolean isSuccess = webServices.get_reset_msg(resp);
                if (isSuccess)
                    handler.sendEmptyMessage(0);
                else
                    handler.sendEmptyMessage(1);
            }
        }.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    telemananger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    APIUtility.SESSION = "";
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    APIUtility.DEVICEID = telemananger.getDeviceId().toString();
                } else {

                }
                break;
          /*  case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults.length > 0)
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(LoginActivity.this, "READ_PHONE_STATE Denied", Toast.LENGTH_SHORT)
                                .show();
                        finish();
                    }
            break;*/
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin(final String ip) {
        try {
            if (mAuthTask != null) {
                return;
            }
            // Reset errors.
            email_edt.setError(null);
            password_edt.setError(null);

            // Store values at the time of the login attempt.
            final String email = email_edt.getText().toString();
            final String password = password_edt.getText().toString();

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(LoginActivity.this, getString(R.string.error_invalid_email), Toast.LENGTH_LONG).show();
                return;
            }
            // Check for a valid email address.
//            if (!isValidEmaillId(email)) {
//                Toast.makeText(LoginActivity.this, getString(R.string.error_invalid_email), Toast.LENGTH_LONG).show();
//                return;
//            }
            // Check for a valid password, if the user entered one.
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(LoginActivity.this, getString(R.string.error_invalid_password), Toast.LENGTH_LONG).show();
//            password_edt.requestFocus();
                return;
            }

            if (WebServices.isNetworkAvailable(LoginActivity.this)) {
                mAuthTask = new UserLoginTask(email, password, ip, true);
                mAuthTask.execute((Void) null);

            } else if (dataBase.getLoginData() > 0) {
                if (!dataBase.getLoginData(email, password).equalsIgnoreCase("")) {
                    mDialog = new ProgressDialog(LoginActivity.this);
                    mDialog.setMessage("Please wait...");
                    mDialog.setTitle("");
                     mDialog.setCancelable(false);
                    if (mDialog != null)
                        mDialog.show();
                    handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (mDialog != null && !isFinishing()) mDialog.dismiss();
                            if (msg.what == 1) {
                                if (WebServices.ERRORMSG.equalsIgnoreCase(""))
                                    WebServices.ERRORMSG = "Some error encountered. Please try again later.";
                               // Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                                return;
                            }
                            java.text.DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            Date date = new Date();
                            System.out.println(dateFormat.format(date));

                            SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                            editor.putString("startTime", dateFormat.format(date));
                            editor.commit();
                            if (password.equalsIgnoreCase("12345")) {
//                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
//                    editor.putBoolean("isFirstTime",false);
//                    editor.commit();
                                Intent intent = new Intent(LoginActivity.this, ChangePass_Activity.class);
                                intent.putExtra("sms_url", getIntent().getStringExtra("sms_url"));
                                intent.putExtra("loginscreen", "login");
                                startActivity(intent);
                                finish();
                            } else {
//                        if (SplashActivity.mPref.getBoolean("ishint", false)) {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.putExtra("loginscreen", "login");
                                intent.putExtra("sms_url", getIntent().getStringExtra("sms_url"));
                                startActivity(intent);
                                finish();
                                mAuthTask = new UserLoginTask(email, password, ip, false);
                                mAuthTask.execute((Void) null);
                            }
//                        } else {
//                            Intent intent = new Intent(LoginActivity.this, DescriptionActivity.class);
//                            startActivity(intent);
//                            finish();
//                        }
                        }
                    };
                    new Thread() {
                        @Override
                        public void run() {
                            boolean islogin = webServices.isLogined(dataBase.getLoginData(email_edt.getText().toString(), password_edt.getText().toString()));
                            if (islogin) {
                                if (SplashActivity.mPref == null)
                                    SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                                editor.putBoolean("islogin", true);
                                editor.putString("id", email);
                                editor.putString("pass", password);
                                editor.commit();

                                userLogin(LoginActivity.this, WebServices.mLoginUtility.getUser_id(), email, WebServices.mLoginUtility.getFirstname(), ip, "email");
                                handler.sendEmptyMessage(0);
                            } else {
                                handler.sendEmptyMessage(1);
                            }
                        }
                    }.start();
                } else if (dataBase.getLoginData(email).equalsIgnoreCase("")) {
                    Toast.makeText(LoginActivity.this, "User does not exist in the system; please contact your adminstrator", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, "Incorrect password; please try again ", Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(LoginActivity.this, "Can't proceed. No internet connection.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isValidEmaillId(String email) {

        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int res = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (res != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_SMS, Manifest.permission.READ_PHONE_STATE},
                        REQUEST_READ_PHONE_STATE);
            }
            res = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (res != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                        REQUEST_READ_PHONE_STATE);
            }

        }
    }

    private void checkPhoneStatePermission() {
        if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_PHONE_STATE)) {
                ActivityCompat.requestPermissions(LoginActivity.this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        REQUEST_READ_PHONE_STATE);

            } else {
                ActivityCompat.requestPermissions(LoginActivity.this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        REQUEST_READ_PHONE_STATE);
            }
        } else {
            telemananger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            APIUtility.SESSION = "";
            APIUtility.DEVICEID = telemananger.getDeviceId().toString();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private ProgressDialog progressDialog;
        private boolean isShow = true;
        String ip = "";

        UserLoginTask(String email, String password, String ip, boolean isshow) {
            isShow = isshow;
            mEmail = email;
            this.ip = ip;
            mPassword = password;
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.getWindow().setBackgroundDrawableResource(R.drawable.custom_dialog);
            progressDialog.setTitle("");
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isShow && progressDialog != null)
                progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
// Building post parameters, key and value pair
            if(TextUtils.isEmpty(refreshedToken)){
                refreshedToken = FirebaseInstanceId.getInstance().getToken();
            }
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("email", mEmail));
            nameValuePair.add(new BasicNameValuePair("password", mPassword));
            nameValuePair.add(new BasicNameValuePair("push_token", refreshedToken));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            if(!Constants.ORGANIZATION_ID.equals("1") || Constants.BRANCH_ID.equals("25")) {    //---branch_id condition for panasonic
                nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
                if (Constants.BRANCH_ID.equals("25"))
                    nameValuePair.add(new BasicNameValuePair("branch_id", Constants.BRANCH_ID));
            }
            String resp = webServices.getLogin(nameValuePair, APIUtility.LOGIN);
            Log.d(" Response:", resp.toString());
            boolean isSuccess = webServices.isLogined(resp);
            if (isSuccess) {
                if (SplashActivity.mPref == null)
                    SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putBoolean("islogin", isSuccess);
                editor.putString("id", mEmail);
                editor.putString("pass", mPassword);
                editor.commit();
                if (dataBase.isData(mEmail, "Login"))
                    dataBase.addLoginData(mEmail, mPassword, resp);
                else {
                    dataBase.updateLoginData(mEmail, mPassword, resp);
                }

                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                nameValuePair.add(new BasicNameValuePair("push_token", refreshedToken));
                nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                resp = webServices.getLogin(nameValuePair, APIUtility.ORGANIZATION_DETAILS);
                Log.d(" Response:", resp.toString());
                if (!resp.equalsIgnoreCase("")) {
                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "Organisation_setting"))
                        dataBase.addOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                    else {
                        dataBase.updateOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                    }
                    webServices.parseOrganization(resp);
                }

//                nameValuePair = new ArrayList<NameValuePair>();
//                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
//                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
//                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
//                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
//                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
//                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
//                resp = webServices.getLogin(nameValuePair, APIUtility.GETALL_NOTIFICATION);
//                if (!resp.equalsIgnoreCase("")) {
//                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "Notifications"))
//                        dataBase.addNotificationData(WebServices.mLoginUtility.getUser_id(), resp);
//                    else {
//                        dataBase.updateNotificationData(WebServices.mLoginUtility.getUser_id(), resp);
//                    }
//                    webServices.getAllNotification(resp);
//                }

                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("push_token", refreshedToken));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUP);
                Log.d(" Response:", resp.toString());
                if (webServices.isValid(resp)) {
                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "All_Groups"))
                        dataBase.addallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                    else {
                        dataBase.updateallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                    }
                    webServices.getAllGroup(resp);
                }


                MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(LoginActivity.this);
                userLogin(LoginActivity.this, WebServices.mLoginUtility.getUser_id(), mEmail, WebServices.mLoginUtility.getFirstname(), ip, "email");
            }

            return isSuccess;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            if (SplashActivity.mPref == null) {
                SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
            }
            try {
                if (progressDialog != null && !isFinishing()) progressDialog.dismiss();
            } catch (Exception e) {
                finish();
            }
            if (success) {

                if (isShow) {
                    java.text.DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = new Date();
                    System.out.println(dateFormat.format(date));

                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putString("startTime", dateFormat.format(date));
                    editor.commit();
                    if (mPassword.equalsIgnoreCase("12345")) {
//                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
//                    editor.putBoolean("isFirstTime",false);
//                    editor.commit();
                        Intent intent = new Intent(LoginActivity.this, ChangePass_Activity.class);
                        intent.putExtra("loginscreen", "login");
                        intent.putExtra("sms_url", getIntent().getStringExtra("sms_url"));
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        intent.putExtra("loginscreen", "login");
                        intent.putExtra("sms_url", getIntent().getStringExtra("sms_url"));
                        startActivity(intent);
                        finish();
                    }
                }
//                } else {
//                    Intent intent = new Intent(LoginActivity.this, DescriptionActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
            } else {
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_DEVICE")) {
                    try {
                        final Dialog dialog = new Dialog(LoginActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.invalid_device_dialog);
                        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
                        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
                        TextView desc_txt = (TextView) dialog.findViewById(R.id.desc_dialog);
                        cancel_btn.setVisibility(View.GONE);
                        ok_btn.setText("OK");
                        desc_txt.setText(WebServices.ERRORMSG);
                        ok_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (dialog != null && !isFinishing())
                                    dialog.dismiss();
                            }
                        });

                        try {
                            dialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        finish();
                    }
                }
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("UNREGISTERED_DEVICE")) {
                    try {
                        InvalidDevice("", mEmail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        finish();
                    }
                } else if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Toast.makeText(LoginActivity.this, getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
                    dataBase.updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putBoolean("islogin", false);
                    editor.clear();
                    editor.commit();

                }
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("USER_NOT_EXIST")) {
                    dataBase.updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putBoolean("islogin", false);
                    editor.clear();
                    editor.commit();
                    if (!isShow) {
                        WebServices.mLoginUtility = new LoginUtility();
                        Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                    if (WebServices.isNetworkAvailable(LoginActivity.this)) {
                        if (WebServices.ERRORMSG.equalsIgnoreCase(""))
                            WebServices.ERRORMSG = "Some error encountered. Please try again later.";
                        Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                    } else {
                        if (WebServices.ERRORMSG.equalsIgnoreCase(""))
                            WebServices.ERRORMSG = "Internet connection unavailable. App running in offline mode.";
                        Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (WebServices.isNetworkAvailable(LoginActivity.this)) {
                        if (WebServices.ERRORMSG.equalsIgnoreCase(""))
                            WebServices.ERRORMSG = "Some error encountered. Please try again later.";
                        Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                    } else {
                        if (WebServices.ERRORMSG.equalsIgnoreCase(""))
                            WebServices.ERRORMSG = "Internet connection unavailable. App running in offline mode.";
                        Toast.makeText(LoginActivity.this, "Internet connection unavailable. App running in offline mode.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void InvalidDevice(final String mobno, final String email) {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.invalid_device_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        TextView desc_txt = (TextView) dialog.findViewById(R.id.desc_dialog);
        desc_txt.setText(WebServices.ERRORMSG);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mixPanelManager.resetDevice(LoginActivity.this, WebServices.mLoginUtility.getEmail(), true);
                if (dialog != null && !isFinishing())
                    dialog.dismiss();
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mixPanelManager.resetDevice(LoginActivity.this, WebServices.mLoginUtility.getEmail(), false);
                if (dialog != null && !isFinishing())
                    dialog.dismiss();
                reset_Device(mobno, email);
            }
        });
        try {
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    public void userLogin(Activity activity, String userID, String userEmail, String fname, String ipaddress, String type) {
        //getting unique id for device
        String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        if (userID == null)
            return;

        /*if (lname.equalsIgnoreCase("null") || lname == null)
            lname = "";*/
        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            if (!type.equalsIgnoreCase("email"))
                jsonObject.put(AppConstants.MIXPANAL_OTP, userEmail);

            jsonObject.put(AppConstants.MIXPANAL_EMAIL, WebServices.mLoginUtility.getEmail());
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, AppConstants.MIXPANAL_DEVICE_TYPE_ANDROID);
            jsonObject.put(AppConstants.MIXPANAL_LOGIN_TIME, getCurrentTimeStamp());
            jsonObject.put(AppConstants.MIXPANAL_FIRSTNAME, fname);
            jsonObject.put(AppConstants.MIXPANAL_LASTNAME, "");
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_ID, id);
            jsonObject.put(AppConstants.MIXPANAL_IPADDRESS, ipaddress);
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
            jsonObject.put(AppConstants.MIXPANAL_USER_PHONE, WebServices.mLoginUtility.getPhone());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {
        }
        AppConstants.mixpanelAPI.identify(userID);
        people.identify(userID);
        if (type.equalsIgnoreCase("email"))
            AppConstants.mixpanelAPI.track(AppConstants.USER_LOGIN_EVENT, jsonObject);
        else
            AppConstants.mixpanelAPI.track(AppConstants.USER_LOGIN_OTP_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }

    private String getCurrentTimeStamp() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(AppConstants.TIME_ZONE));
        Date date = calendar.getTime();
        return DateFormat.format(AppConstants.DATE_FORMAT, date).toString();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void mobileattemptLogin(final String ip) {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.

        // Store values at the time of the login attempt.
        final String email = mobile_no_edt.getText().toString();
        final String password = otp_edt.getText().toString();

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_email), Toast.LENGTH_LONG).show();
            return;
        }
        // Check for a valid email address.
        if (!isValidPhoneNumber(email)) {
            Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_email), Toast.LENGTH_LONG).show();
            return;
        }
        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(LoginActivity.this, getString(R.string.merror_invalid_password), Toast.LENGTH_LONG).show();
//            password_edt.requestFocus();
            return;
        }

//        if (dataBase.getLoginData(email, password).equalsIgnoreCase("")) {
        if (WebServices.isNetworkAvailable(LoginActivity.this)) {
            mobileAuthTask = new UserMobileLoginTask(email, password, ip, true);
            mobileAuthTask.execute((Void) null);
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public class UserMobileLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private ProgressDialog progressDialog;
        private boolean isShow = true;
        String ip = "";

        UserMobileLoginTask(String email, String password, String ip, boolean isshow) {
            isShow = isshow;
            mEmail = email;
            this.ip = ip;
            mPassword = password;
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setTitle("");
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isShow && progressDialog != null)
                progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

// Building post parameters, key and value pair
            if(TextUtils.isEmpty(refreshedToken)){
                refreshedToken = FirebaseInstanceId.getInstance().getToken();
            }
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("mobile_no", mEmail));
            nameValuePair.add(new BasicNameValuePair("otp_code", mPassword));
            nameValuePair.add(new BasicNameValuePair("push_token", refreshedToken));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            if (!Constants.ORGANIZATION_ID.equals("1") || Constants.BRANCH_ID.equals("25")) {    //---branch_id condition for panasonic
                nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
                if (Constants.BRANCH_ID.equals("25"))
                    nameValuePair.add(new BasicNameValuePair("branch_id", Constants.BRANCH_ID));
            }
            String resp = webServices.getLogin(nameValuePair, APIUtility.LOGIN_BY_OTP);
            Log.d(" Response:", resp.toString());
            boolean isSuccess = webServices.isLogined(resp);
            if (isSuccess) {
                if (SplashActivity.mPref == null)
                    SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putBoolean("islogin", isSuccess);
                editor.putString("id", mEmail);
                editor.putString("pass", mPassword);
                editor.commit();
                if (dataBase.isData(mEmail, "Login")) {
                    dataBase.addLoginData(mEmail, mPassword, resp);
                } else {
                    dataBase.updateLoginData(mEmail, mPassword, resp);
                }

                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
                nameValuePair.add(new BasicNameValuePair("push_token", refreshedToken));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                resp = webServices.getLogin(nameValuePair, APIUtility.ORGANIZATION_DETAILS);
                if (!resp.equalsIgnoreCase("")) {
                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "Organisation_setting"))
                        dataBase.addOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                    else {
                        dataBase.updateOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                    }
                    webServices.parseOrganization(resp);
                }

                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("push_token", refreshedToken));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                resp = webServices.getLogin(nameValuePair, APIUtility.GETALL_NOTIFICATION);
                if (!resp.equalsIgnoreCase("")) {
                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "Notifications"))
                        dataBase.addNotificationData(WebServices.mLoginUtility.getUser_id(), resp);
                    else {
                        dataBase.updateNotificationData(WebServices.mLoginUtility.getUser_id(), resp);
                    }
                    webServices.getAllNotification(resp);
                }


                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("push_token", refreshedToken));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUP);
                Log.d(" Response:", resp.toString());
                if (webServices.isValid(resp)) {
                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "All_Groups"))
                        dataBase.addallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                    else {
                        dataBase.updateallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                    }
                    webServices.getAllGroup(resp);
                }
                if (isShow) {
//                    MyApplication.getInstance().trackScreenView("Login");
                    MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(LoginActivity.this);
                    userLogin(LoginActivity.this, WebServices.mLoginUtility.getUser_id(), mEmail, WebServices.mLoginUtility.getFirstname(), ip, "otp");
                }
            }


            return isSuccess;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            if (progressDialog != null && !isFinishing()) progressDialog.dismiss();

            if (success) {
                java.text.DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                System.out.println(dateFormat.format(date));

                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putString("startTime", dateFormat.format(date));
                editor.commit();
                if (isShow) {
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    intent.putExtra("loginscreen", "login");
                    intent.putExtra("sms_url", getIntent().getStringExtra("sms_url"));
                    startActivity(intent);
                    finish();
                }
//                } else {
//                    Intent intent = new Intent(LoginActivity.this, DescriptionActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
            } else {
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_DEVICE")) {
                    try {
                        final Dialog dialog = new Dialog(LoginActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.invalid_device_dialog);
                        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
                        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
                        TextView desc_txt = (TextView) dialog.findViewById(R.id.desc_dialog);
                        cancel_btn.setVisibility(View.GONE);
                        ok_btn.setText("OK");
                        desc_txt.setText(WebServices.ERRORMSG);
                        ok_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (dialog != null && !isFinishing())
                                    dialog.dismiss();
                            }
                        });

                        try {
                            dialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        finish();
                    }
                }
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("UNREGISTERED_DEVICE")) {
                    try {
                        InvalidDevice("", mEmail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        finish();
                    }
                } else if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Toast.makeText(LoginActivity.this, getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
                    dataBase.updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putBoolean("islogin", false);
                    editor.clear();
                    editor.commit();

                }
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("USER_NOT_EXIST")) {
                    dataBase.updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putBoolean("islogin", false);
                    editor.clear();
                    editor.commit();
                    if (!isShow) {
                        WebServices.mLoginUtility = new LoginUtility();
                        Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else if (WebServices.isNetworkAvailable(LoginActivity.this)) {
                    if (WebServices.ERRORMSG.equalsIgnoreCase(""))
                        WebServices.ERRORMSG = "Some error encountered. Please try again later.";
                    Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                } else {
                    if (WebServices.ERRORMSG.equalsIgnoreCase(""))
                        WebServices.ERRORMSG = "Internet connection unavailable. App running in offline mode.";
                    Toast.makeText(LoginActivity.this, WebServices.ERRORMSG, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //    private void  launchComponent(String packageName, String name){
//        Intent launch_intent = new Intent("android.intent.action.MAIN");
//        launch_intent.addCategory("android.intent.category.LAUNCHER");
//        launch_intent.setComponent(new ComponentName(packageName, name));
//        launch_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        launch_intent.putExtra("id","demo@chap_tempone.com");//put Emailid
//        launch_intent.putExtra("pass","12345");//put Password
//        startActivity(launch_intent);
//    }
//    public void startApplication(String application_name){
//        try{
//            Intent intent = new Intent("android.intent.action.MAIN");
//            intent.addCategory("android.intent.category.LAUNCHER");
//
//            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//            List<ResolveInfo> resolveinfo_list = getPackageManager().queryIntentActivities(intent, 0);
//
//            for(ResolveInfo info:resolveinfo_list){
//                if(info.activityInfo.packageName.equalsIgnoreCase(application_name)){
//                    launchComponent(info.activityInfo.packageName, info.activityInfo.name);
//                    break;
//                }
//            }
//        }
//        catch (ActivityNotFoundException e) {
//            Toast.makeText(getApplicationContext(), "There was a problem loading the application: "+application_name,Toast.LENGTH_SHORT).show();
//        }
//    }
    @Override
    public void otpReceived(String smsText) {
        //Do whatever you want to do with the text
//        Toast.makeText(this, "Got " + smsText, Toast.LENGTH_LONG).show();
        Log.d("Otp", smsText);
        String otp = smsText.substring(0, 6);
        System.out.println("====" + otp);
//        if (LoginActivity.this.dialog!=null)LoginActivity.this.dialog.dismiss();
//        if (TIMERVALUE<=15&&TimerVal) {
//        send_txt.setEnabled(true);
        send_txt.setText(" Send OTP ");
        TimerVal = false;
        otp_edt.setText(otp);
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            timer_txt.setText("");
                        }
                    });
                }
            }
        }.start();
//            mobileattemptLogin(ip);
//        }else{
//            Toast.makeText(LoginActivity.this, "SMS not detected. Please try again" , Toast.LENGTH_LONG).show();
//        }

    }

    public static String convertSecondsToHMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        return String.format("%02d:%02d", m, s);
    }

    private void startTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (TimerVal) {
                    try {
                        Thread.sleep(1000);
                        handler2.post(new Runnable() {

                            @Override
                            public void run() {
                                if (TIMERVALUE != 0) {
                                    timer_txt.setText("Getting OTP. Please wait for: " + convertSecondsToHMmSs(--TIMERVALUE) + " sec(s)");
                                } else {
                                    send_txt.setEnabled(true);
                                    send_otp_ll.setEnabled(true);
                                    send_txt.setText("Resend OTP");
                                    TimerVal = false;
                                    timer_txt.setText("");
//                                    if (LoginActivity.this.dialog!=null)LoginActivity.this.dialog.dismiss();

                                    Toast.makeText(LoginActivity.this, "SMS not detected. Please try again", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        }).start();
    }
}

