package com.chaptervitamins.chaptervitamins;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.splunk.mint.Mint;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abcd on 5/10/2016.
 */
public class ForgetPassword_Activity extends AppCompatActivity {
    @BindView(R.id.forget_pass_email_edit_text)
    EditText forget_pass_email_edit_text;
    @BindView(R.id.reset_button)
    Button reset_button;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.go_back)
    TextView go_back;
    private UserLoginTask mAuthTask = null;
    private DataBase dataBase;
    // UI references.
    private View mProgressView;
    private View mLoginFormView;
    private Handler handler;
    private WebServices webServices;
    private ProgressDialog mDialog;
    private ImageView back;
    private MixPanelManager mixPanelManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (APIUtility.organizationModel.getEnableScreenshot().equalsIgnoreCase("no"))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        // TODO: Update with your API key
        Mint.initAndStartSession(ForgetPassword_Activity.this, Constants.CRASH_REPORT_KEY);
        setContentView(R.layout.forgot_pass);
        mixPanelManager = APIUtility.getMixPanelManager(this);

        // Check if no view has focus:
        changeStatusBarColor();
        ButterKnife.bind(this);
        toolbar_title.setText("Forgot Password");
        back = (ImageView) findViewById(R.id.back);
        dataBase = DataBase.getInstance(ForgetPassword_Activity.this);
        webServices = new WebServices();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        reset_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            this.finish();
        return true;
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        forget_pass_email_edit_text.setError(null);

        // Store values at the time of the login attempt.
        String email = forget_pass_email_edit_text.getText().toString();

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(ForgetPassword_Activity.this, getString(R.string.error_invalid_email), Toast.LENGTH_LONG).show();
            return;
        }

        if (WebServices.isNetworkAvailable(ForgetPassword_Activity.this)) {

            mAuthTask = new UserLoginTask(email);
            mAuthTask.execute((Void) null);


        } else {
            Toast.makeText(ForgetPassword_Activity.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private String mEmail;
        private String id;
        private ProgressDialog progressDialog;


        UserLoginTask(String id) {

            this.id = id;
            progressDialog = new ProgressDialog(ForgetPassword_Activity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setTitle("");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }
        @Override
        protected Boolean doInBackground(Void... params) {
// Building post parameters, key and value pair
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("username", ""));
            nameValuePair.add(new BasicNameValuePair("email", id));
            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
            if (!TextUtils.isEmpty(Constants.BRANCH_ID))
                nameValuePair.add(new BasicNameValuePair("branch_id", Constants.BRANCH_ID));
            String resp = webServices.getLogin(nameValuePair, APIUtility.FORGET_PASSWORD);
            Log.d(" Response:", resp.toString());
            boolean isSuccess = webServices.forgetPassword(resp);
            mEmail = "";
            if (isSuccess) {
                isSuccess = webServices.changePassword(resp);
                if (!isSuccess) mEmail = "notfound";
            }
            return isSuccess;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            if (progressDialog != null && progressDialog.isShowing()) progressDialog.dismiss();
            mixPanelManager.forgetPasswordEvent(ForgetPassword_Activity.this,  id);
            if (success) {
                finish();
            }
//            if (mEmail.equalsIgnoreCase("notfound")){
//                Intent intent = new Intent(ForgetPassword_Activity.this, ChangePassByID.class);
//                intent.putExtra("userid",id);
//                    startActivity(intent);
//                    finish();
//            }
            Toast.makeText(ForgetPassword_Activity.this, WebServices.ERRORMSG, Toast.LENGTH_SHORT).show();

        }
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }
}
