package com.chaptervitamins.chaptervitamins;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.util.Log;
import android.util.Patterns;
import android.view.Window;
import android.view.WindowManager;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.google.firebase.FirebaseApp;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.splunk.mint.Mint;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class SplashActivity extends AppCompatActivity {
    private boolean isBackPressed = false;
    public static final long SPLASH_SCREEN_DELAY = 2000;
    public static SharedPreferences mPref;
    //    public static SharedPreferences mPref_Data;
    private WebServices webServices;
    private DataBase dataBase;
    public static boolean isOpenLoginScreen=true;
    TelephonyManager telemananger = null;
    //    public final SIM_Docker shipit = SIM_Docker.sharedInstance();
//    public final SIM_Cfg cfg_local = SIM_Cfg.sharedInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        // Set the application environment
//        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
//        // TODO: Update with your API key
        Mint.initAndStartSession(SplashActivity.this, Constants.CRASH_REPORT_KEY);
        FirebaseApp.initializeApp(this);
        if (!isTaskRoot() && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {
            finish();
            return;
        }

        setContentView(R.layout.activity_splace_screen);


        // Obtain the shared Tracker instance.
        //ProjectID from GCM SenderID Configuration (4)

//        cfg_local.projID = "1087500666497";
//
////From shipit dashboard
//
//        cfg_local.SIM_AppKey = "AIzaSyBQ0GTF-f4M2rvGmQgvCWLoMRVZisdVMLY";
////Enable Push Notification (can also use shipit_setPush(boolean))
//
//        cfg_local.pushEnable = true;
//
////Enable RichPush Notification (can also use shipit_setRichPush(boolean))
//
//        cfg_local.richPushEnable = true;
//
////To DEV mode, default is PROD
//
//        cfg_local.buildType = "PROD";
//
//        shipit.init(this);
//
////To enable user's app movement
//
//        SIM_ActTracker.init(this.getApplication());

//        java.text.DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        Date date = new Date();
//        System.out.println(dateFormat.format(date));

        webServices = new WebServices();
        WebServices.ERRORMSG = "";
        dataBase = DataBase.getInstance(SplashActivity.this);
        mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor=mPref.edit();
//        editor.putString("startTime",dateFormat.format(date));
//        editor.commit();
//        mPref_Data = getSharedPreferences("prefpendingdata", Context.MODE_PRIVATE);
        setSplashScreenDelay(SPLASH_SCREEN_DELAY);
        changeStatusBarColor();
       // TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
      //  String number = tm.getLine1Number();
     //   System.out.println(number+"================");

    }


    private void changeStatusBarColor() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

    }
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1002;
    @Override
    protected void onStart() {
        super.onStart();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//            int res = checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
//            if (res != PackageManager.PERMISSION_GRANTED) {
//                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS, android.Manifest.permission.READ_PHONE_STATE},
//                        REQUEST_CODE_ASK_PERMISSIONS);
//            }
//            res = checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE);
//            if (res != PackageManager.PERMISSION_GRANTED) {
//                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
//                        REQUEST_CODE_ASK_PERMISSIONS);
//            }
//
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(SplashActivity.this);
    }
    private void setSplashScreenDelay(final long delay) {
        new Thread() {
            public void run() {
                try {

                    if (isBackPressed) {
                        finish();
                    }
                    Thread.sleep(delay);

                    showNextView();
                } catch (Exception e) {
                    Log.e(SplashActivity.class.getSimpleName(), "Exception Occured: ", e);
                }
            }
        }.start();
    }


    @Override
    protected void onDestroy() {
        if (AppConstants.mixpanelAPI != null)
            AppConstants.mixpanelAPI.flush();
        super.onDestroy();
    }

    private void showNextView() {
        String sms_url="";
        Intent smsintent = getIntent();
        if (smsintent.getExtras() != null)
            if (smsintent.getData()!=null) {
                if (!smsintent.getData().toString().equalsIgnoreCase("")) {

//                    String url = "http://abhkms.chaptervitamins.com/?cid=5&mid=44";
                    sms_url = smsintent.getData().toString();
//                    String str = url.split("\\?")[1];
//                    String str1[] = str.split("&");
//                    String cid = str1[0].split("=")[1];
//                    String mid = str1[1].split("=")[1];
//                    mid = mid.split(" ")[0];
//                    System.out.println("====" + cid);
//                    System.out.println("====" + mid);
                    System.out.println("====" + sms_url);
                }
            }else if (smsintent.getStringExtra("sms_url")!=null)
            if (!smsintent.getStringExtra("sms_url").equalsIgnoreCase("")){
//                String url = "http://abhkms.chaptervitamins.com/?cid=3&mid=42";
                sms_url = smsintent.getStringExtra("sms_url");
//                String str = url.split("\\?")[1];
//                String str1[] = str.split("&");
//                String cid = str1[0].split("=")[1];
//                String mid = str1[1].split("=")[1];
//                mid = mid.split(" ")[0];
//                System.out.println("====" + cid);
//                System.out.println("====" + mid);
                System.out.println("====" + sms_url);
            }
        if (mPref.getBoolean("islogin", false)) {
            if (!mPref.getString("id", "").equalsIgnoreCase("")) {
                String resp = dataBase.getLoginData(mPref.getString("id", ""), mPref.getString("pass", ""));
                if (resp.equalsIgnoreCase("")&&isOpenLoginScreen) {

                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.clear();
                    editor.commit();
//                    MyApplication.getInstance().trackScreenView("Login");
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    intent.putExtra("sms_url", sms_url);
                    startActivity(intent);
                    finish();
                } else {
                    java.text.DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = new Date();
                    System.out.println(dateFormat.format(date));

                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putString("startTime", dateFormat.format(date));
                    editor.commit();
                    telemananger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    APIUtility.DEVICEID=telemananger.getDeviceId().toString();
                    webServices.isLogined(resp);
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    intent.putExtra("loginscreen", "");
                    intent.putExtra("sms_url", sms_url);
                    startActivity(intent);
//                    MyApplication.getInstance().trackScreenView("DashBoard");
                    @SuppressLint("WifiManagerLeak") WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
                    final String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                    MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(SplashActivity.this);
                    userLogin(SplashActivity.this, WebServices.mLoginUtility.getUser_id(), mPref.getString("id",""), WebServices.mLoginUtility.getFirstname(), ip);
                    finish();
                }
            }

//            if (mPref.getBoolean("ishint", false)) {

//            } else {
//                Intent intent = new Intent(SplashActivity.this, DescriptionActivity.class);
//                startActivity(intent);
//                finish();
//            }
        } else{
//            MyApplication.getInstance().trackScreenView("Login");
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            intent.putExtra("sms_url", sms_url);
            startActivity(intent);
            finish();
        }
    }
    private boolean isValidEmaillId(String email) {

        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    public void userLogin(Activity activity, String userID, String userEmail, String fname, String ipaddress) {

//        TelephonyManager tMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//        String mPhoneNumber = tMgr.getLine1Number();
//        System.out.println(mPhoneNumber+"=====mPhoneNumber======");
        //getting unique id for device
        String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (userID == null)
            return;
//        if (lname.equalsIgnoreCase("null") || lname == null)
//            lname = "";

        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            if (isValidEmaillId(userEmail))
                jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            else
                jsonObject.put(AppConstants.MIXPANAL_OTP, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, AppConstants.MIXPANAL_DEVICE_TYPE_ANDROID);
            jsonObject.put(AppConstants.MIXPANAL_LOGIN_TIME, getCurrentTimeStamp());
            jsonObject.put(AppConstants.MIXPANAL_FIRSTNAME, fname);
            jsonObject.put(AppConstants.MIXPANAL_LASTNAME, "");
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_ID, id);
            jsonObject.put(AppConstants.MIXPANAL_IPADDRESS, ipaddress);
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(userID);
        people.identify(userID);
        AppConstants.mixpanelAPI.track(AppConstants.USER_AUTO_LOGIN_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    private String getCurrentTimeStamp() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(AppConstants.TIME_ZONE));
        Date date = calendar.getTime();
        return DateFormat.format(AppConstants.DATE_FORMAT, date).toString();
    }


}
