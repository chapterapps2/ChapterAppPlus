package com.chaptervitamins.Suggestions;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;

import java.io.File;


public class Play_Audio_Activity extends BaseActivity implements UploadListener {
    private TextView video_title;
    private ImageView play, cancel_img, ok_img, back;
    private MediaPlayer mediaPlayer;
    SeekBar seekBarProgress;
    boolean isPlay=false;
    private int mediaFileLengthInMilliseconds;
    private final Handler handler = new Handler();
    RelativeLayout rl;
    String FILENAME="";
    long size=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_play_audio);
        video_title = (TextView) findViewById(R.id.video_title);
//        video_title.setText(getIntent().getStringExtra("desc"));
        cancel_img = (ImageView) findViewById(R.id.cancel_img);
        ok_img = (ImageView) findViewById(R.id.ok_img);
        rl=(RelativeLayout)findViewById(R.id.rl);
        if (!getIntent().getStringExtra("screen").equalsIgnoreCase("upload"))rl.setVisibility(View.GONE);
        back = (ImageView) findViewById(R.id.back);
        play = (ImageView) findViewById(R.id.play);
        seekBarProgress = (SeekBar) findViewById(R.id.seekBar);
        seekBarProgress.setMax(99);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                finish();
            }
        });
        cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                finish();
            }
        });
        ok_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FILENAME.equalsIgnoreCase("")&&size==0){
                    Toast.makeText(Play_Audio_Activity.this, "Can't Upload. Please try again.", Toast.LENGTH_LONG).show();
                    return;
                }

                new File_Uploader(Play_Audio_Activity.this, getIntent().getStringExtra("file_path"), APIUtility.UPLOAD_FILE, FILENAME, size, "IMAGE", true, Play_Audio_Activity.this);
//                final ProgressDialog dialog = ProgressDialog.show(Play_Audio_Activity.this, "", "Please wait..");
//                dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
//                final Handler handler = new Handler() {
//                    @Override
//                    public void handleMessage(Message msg) {
//                        if (dialog != null) dialog.dismiss();
//                        uploadDialog("");
//                    }
//                };
//                new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            sleep(2000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        } finally {
//                            handler.sendEmptyMessage(0);
//                        }
//                    }
//                }.start();
            }
        });
        System.out.println("=======" + getIntent().getStringExtra("file_path"));
        mediaPlayer = new MediaPlayer();
        File file=new File(getIntent().getStringExtra("file_path"));
            String url=getIntent().getStringExtra("file_path");
            if (url!=null) {
                String[] r=url.split("/");
                System.out.println("===="+r[r.length-1]);
                FILENAME=r[r.length-1];
                size=file.length();
            }

        openAudioFile(getIntent().getStringExtra("file_path"));
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPlay) {
                    isPlay=true;
                    mediaPlayer.start();
                    play.setImageResource(R.drawable.button_pause);
                } else {
                    isPlay=false;
                    mediaPlayer.pause();
                    play.setImageResource(R.drawable.button_play);
                }
                primarySeekBarProgressUpdater();
            }
        });
        seekBarProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer.isPlaying()) {
                    int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * seekBar.getProgress();
                    mediaPlayer.seekTo(playPositionInMillisecconds);
                }
            }
        });
        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                seekBarProgress.setSecondaryProgress(percent);
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                play.setImageResource(R.drawable.button_play);
                seekBarProgress.setSecondaryProgress(0);
                seekBarProgress.setProgress(0);
            }
        });
    }
    private void openAudioFile(String audiofile) {

        /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
        try {
            mediaPlayer.setDataSource(audiofile); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
            mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
        } catch (Exception e) {
            e.printStackTrace();
        }

        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL




    }

    private void primarySeekBarProgressUpdater() {
        seekBarProgress.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }
    }
    private void uploadDialog(String filepath) {
        final Dialog dialog = new Dialog(Play_Audio_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_dialog);
        TextView title_dialog = (TextView) dialog.findViewById(R.id.title_dialog);
        TextView desc_dialog = (TextView) dialog.findViewById(R.id.desc_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button submit_btn = (Button) dialog.findViewById(R.id.submit_btn);
        title_dialog.setVisibility(View.GONE);
        cancel_btn.setVisibility(View.GONE);
        submit_btn.setVisibility(View.VISIBLE);
        desc_dialog.setText("Your Answer is successfully submitted to \n Admin for review");
        desc_dialog.setTextSize(10);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        ok_btn.setVisibility(View.GONE);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(getIntent().getStringExtra("file_path"));
                if (f.exists()) f.delete();
                dialog.dismiss();
            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(getIntent().getStringExtra("file_path"));
                if (f.exists()) f.delete();
                Toast.makeText(Play_Audio_Activity.this, "Successfully Uploaded!", Toast.LENGTH_LONG).show();
                finish();
            }
        });
        dialog.show();
    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void error(String error) {
        Suggestion_Main_Activity.MEDIA_ID="";
    }

    @Override
    public void complete(int media_id, String type,String response) {
        Suggestion_Main_Activity.MEDIA_ID=media_id+"";
//        Toast.makeText(this, "Image Uploaded Successfully.", Toast.LENGTH_SHORT).show();
        finish();
    }
}
