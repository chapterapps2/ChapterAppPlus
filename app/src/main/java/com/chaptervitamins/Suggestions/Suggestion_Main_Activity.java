package com.chaptervitamins.Suggestions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.Suggestion_Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
public class Suggestion_Main_Activity extends BaseActivity {
    private ImageView back;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter adapter;
    private WebServices webServices;
    private Suggestion_History tab1;
    private Quary_History tab2;
    public static ArrayList<Suggestion_Utils> suggestionUtilsArrayList=new ArrayList<>();
    public static String MEDIA_ID="";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion__main);
        back=(ImageView)findViewById(R.id.back);
        webServices=new WebServices();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tabLayout=(TabLayout)findViewById(R.id.tab_layout);
        viewPager=(ViewPager)findViewById(R.id.pager);
        tabLayout.addTab(tabLayout.newTab().setText("Query"));
        tabLayout.addTab(tabLayout.newTab().setText("Suggestion"));

        tabLayout.setBackground(createShapeByColor(Utils.getColorPrimary(),0,0,Utils.getColorPrimary()));

////        tabLayout.addTab(tabLayout.newTab().setText("DOCUMENTS"));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
////        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
//        tabLayout.setSelectedTabIndicatorColor(Color.TRANSPARENT);
//        tabLayout.setTabTextColors(Color.BLACK, Color.WHITE);
//        ((TabWidget)tabLayout.getTag()).setDividerDrawable(R.color.white);
        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (viewPager!=null)
                    viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        getSuggestions();
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(android.support.v4.app.FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            String tabname = tabLayout.getTabAt(position).getText().toString();
            if (tabname.equalsIgnoreCase("Suggestion")) {
                tab1 = new Suggestion_History();
//                tab1.notifydata();
                return tab1;
            }  else if (tabname.equalsIgnoreCase("Query")) {
                tab2 = new Quary_History();
                return tab2;
            }else return null;

        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    private void getSuggestions(){
        dialog = ProgressDialog.show(Suggestion_Main_Activity.this, "", "Please wait...");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("branch_id",WebServices.mLoginUtility.getBranch_id()));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        new GenericApiCall(this, APIUtility.GET_SUGGESTION_QUERY, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                if(dialog!=null)
                    dialog.dismiss();
                if(webServices.isValid((String)result)){
                    suggestionUtilsArrayList = webServices.getAllSuggestions((String)result);
                    if (tab1==null)tab1=new Suggestion_History();
                    if (tab2==null)tab2=new Quary_History();
                    tab1.notifyData();
                    tab2.notifyData();
                }
            }

            @Override
            public void onError(ErrorModel error) {
                if(dialog!=null)
                    dialog.dismiss();
                Toast.makeText(Suggestion_Main_Activity.this, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
            }
        }).execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dialog!=null)
            dialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getSuggestions();
    }
}
