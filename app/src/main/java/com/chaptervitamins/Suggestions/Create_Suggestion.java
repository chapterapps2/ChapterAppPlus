package com.chaptervitamins.Suggestions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 10/27/2016.
 */

public class Create_Suggestion extends BaseActivity {
    private ImageView back;
    private LinearLayout attach_ll;
    private Button btnSubmit;
    private EditText sublect_txt,suggestion_txt;
    private WebServices webServices;
    private TextView count_sub_txt,count_desc_txt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.create_suggestion);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSubmit=(Button)findViewById(R.id.btn_submit);
        attach_ll=(LinearLayout)findViewById(R.id.attach_ll);
//        attach_ll.setVisibility(View.GONE);
        sublect_txt=(EditText)findViewById(R.id.sublect_txt);
        suggestion_txt=(EditText)findViewById(R.id.suggestion_txt);
        count_sub_txt=(TextView)findViewById(R.id.count_sub_txt);
        count_desc_txt=(TextView)findViewById(R.id.count_desc_txt);
        webServices=new WebServices();
        attach_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Create_Suggestion.this, ChooseMedia_Activity.class);
                startActivityForResult(intent,200);
            }
        });
        sublect_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int remaining=80-(sublect_txt.getText().toString().length());
                count_sub_txt.setText(remaining+" Characters Remaining");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        suggestion_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int remaining=1000-(suggestion_txt.getText().toString().length());
                count_desc_txt.setText(remaining+" Characters Remaining");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String sub=sublect_txt.getText().toString().trim();
                final String sugg=suggestion_txt.getText().toString().trim();
                if (sub.equalsIgnoreCase("")){
                    Toast.makeText(Create_Suggestion.this,"Please Enter Subject..",Toast.LENGTH_LONG).show();
                    return;
                }
                if (sugg.equalsIgnoreCase("")){
                    Toast.makeText(Create_Suggestion.this,"Please Enter Suggestion..",Toast.LENGTH_LONG).show();
                    return;
                }
                if (WebServices.isNetworkAvailable(Create_Suggestion.this)) {
                    final ProgressDialog dialog = ProgressDialog.show(Create_Suggestion.this, "", "Please wait...");
                    dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                    final Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (dialog != null) dialog.dismiss();
                            if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                                Utils.callInvalidSession(Create_Suggestion.this,APIUtility.ADD_SUGGESTION_QUERY);
                            }else {
                                Toast.makeText(Create_Suggestion.this,"Suggestion submitted successfully. Thank You!",Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    };
                    new Thread() {
                        @Override
                        public void run() {
                            if (Suggestion_Main_Activity.MEDIA_ID.equalsIgnoreCase(""))Suggestion_Main_Activity.MEDIA_ID="0";
                            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                            nameValuePair.add(new BasicNameValuePair("category", sub));
                            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                            nameValuePair.add(new BasicNameValuePair("message", sugg));
                            nameValuePair.add(new BasicNameValuePair("branch_id",WebServices.mLoginUtility.getBranch_id()));
                            nameValuePair.add(new BasicNameValuePair("type", "SUGGESTION"));
                            nameValuePair.add(new BasicNameValuePair("media_id", Suggestion_Main_Activity.MEDIA_ID));
                            nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                            String resp = webServices.callServices(nameValuePair, APIUtility.ADD_SUGGESTION_QUERY);
                            Log.d(" Response:", resp.toString());
                            if (!webServices.isValid(resp)){
                                handler.sendEmptyMessage(1);
                                return;
                            }
                            handler.sendEmptyMessage(0);
                        }
                    }.start();
                } else {
                    Toast.makeText(Create_Suggestion.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("====="+Suggestion_Main_Activity.MEDIA_ID);
    }
}
