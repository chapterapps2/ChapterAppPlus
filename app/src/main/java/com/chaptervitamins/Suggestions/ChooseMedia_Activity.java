package com.chaptervitamins.Suggestions;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.BaseActivity;

public class ChooseMedia_Activity extends BaseActivity {
private ImageButton choose_photo_imgbtn,choose_video_imgbtn,choose_music_imgbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Window window = this.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.activity_choose_media);
        choose_photo_imgbtn=(ImageButton)findViewById(R.id.choose_photo_imgbtn);
        choose_video_imgbtn=(ImageButton)findViewById(R.id.choose_video_imgbtn);
        choose_music_imgbtn=(ImageButton)findViewById(R.id.choose_music_imgbtn);
        choose_photo_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseMedia_Activity.this, Upload_Image_Activity.class);
                startActivity(intent);
                finish();
            }
        });
        choose_video_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseMedia_Activity.this, Upload_Video_Activity.class);
                startActivity(intent);
                finish();
            }
        });
        choose_music_imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseMedia_Activity.this, Upload_Music_Activity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
