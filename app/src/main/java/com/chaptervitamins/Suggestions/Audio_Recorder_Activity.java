package com.chaptervitamins.Suggestions;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Audio_Recorder_Activity extends AppCompatActivity {
    public static final int RequestPermissionCode = 1;
    Button buttonStart, buttonStop;
    private TextView timer_txt;
    CountDownTimer timer;
    int cnt = 0,time=0;
    MediaRecorder mediaRecorder;
    Random random;
    File AudioSavePathInDevice = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_audio__recorder);
        timer_txt = (TextView) findViewById(R.id.timer_txt);
        Typeface myTypeface = Typeface.createFromAsset(this.getAssets(),
                "DIGITAL.ttf");
        timer_txt.setTypeface(myTypeface);
        buttonStart = (Button) findViewById(R.id.button);
        buttonStop = (Button) findViewById(R.id.button2);
        if (!checkPermission()) requestPermission();
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkPermission()) {

                    try {
                        AudioSavePathInDevice = getFileFromURL(Audio_Recorder_Activity.this, "AudioRecording.mp3");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    MediaRecorderReady();

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();
                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    buttonStart.setEnabled(false);
                    buttonStop.setEnabled(true);
                    timer.start();
                    Toast.makeText(Audio_Recorder_Activity.this, "Recording started", Toast.LENGTH_LONG).show();
                } else {

                    requestPermission();

                }

            }
        });
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mediaRecorder.stop();
                timer.cancel();
                buttonStop.setEnabled(false);
                buttonStart.setEnabled(true);
                Toast.makeText(Audio_Recorder_Activity.this, "Recording Completed", Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(Audio_Recorder_Activity.this, Play_Audio_Activity.class);
                intent1.putExtra("file_path", AudioSavePathInDevice.getAbsolutePath());
                intent1.putExtra("screen","upload");
                startActivity(intent1);
                finish();
            }
        });
        timer = new CountDownTimer(Long.MAX_VALUE, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                cnt++;
//                String time = new Integer(cnt).toString();
                time++;
                long millis = cnt;
                int seconds = (int) (millis / 60);
                seconds = seconds % 60;
                timer_txt.setText(String.format("%02d:%02d",  seconds, time));
                if (time==59)time=0;
            }

            @Override
            public void onFinish() {

            }
        };

    }

    public void MediaRecorderReady() {

        mediaRecorder = new MediaRecorder();

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);

        mediaRecorder.setOutputFile(AudioSavePathInDevice.getAbsolutePath());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        timer.cancel();
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(Audio_Recorder_Activity.this, new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {

                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {

                        Toast.makeText(Audio_Recorder_Activity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Audio_Recorder_Activity.this, "Permission Denied", Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    public static File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Downloaded/");
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        return file;
    }
}
