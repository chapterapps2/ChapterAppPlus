package com.chaptervitamins.Suggestions;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.play_video.JW_Player_Activity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class Upload_Video_Activity extends BaseActivity {
    TextView video_title;
    private LinearLayout recording_ll,gallery_ll;
    private Uri fileUri;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int RESULT_LOAD_VIDEO = 3;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    private ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_quiz);
        video_title=(TextView)findViewById(R.id.video_title);
//        video_title.setText(getIntent().getStringExtra("desc"));
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recording_ll=(LinearLayout)findViewById(R.id.recording_ll);
        gallery_ll=(LinearLayout)findViewById(R.id.gallery_ll);
        recording_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create new Intentwith with Standard Intent action that can be
                // sent to have the camera application capture an video and return it.
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                // create a file to save the video
                fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

                // set the image file name
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                // set the video image quality to high
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

                // start the Video Capture Intent
                startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
            }
        });
        gallery_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
                mediaChooser.setType("video/*");
                startActivityForResult(mediaChooser, RESULT_LOAD_VIDEO);
            }
        });
    }
    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){

        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){

        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraVideo");


        // Create the storage directory(MyCameraVideo) if it does not exist
        if (! mediaStorageDir.exists()){

            if (! mediaStorageDir.mkdirs()){

//                output.setText("Failed to create directory MyCameraVideo.");

//                Toast.makeText(this, "Failed to create directory MyCameraVideo.",Toast.LENGTH_LONG).show();

                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }


        // Create a media file name

        // For unique file name appending current timeStamp with file name
        java.util.Date date= new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());

        File mediaFile;

        if(type == MEDIA_TYPE_VIDEO) {

            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");

        } else {
            return null;
        }

        return mediaFile;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // After camera screen this code will excuted

        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                if (data==null)return;
                File file = new File(data.getData().getPath());
                System.out.println("====file====choose===" + file.getAbsolutePath());
                Intent intent=new Intent(Upload_Video_Activity.this,JW_Player_Activity.class);
                intent.putExtra("file_path",file.getAbsolutePath());
                intent.putExtra("screen","upload");
                startActivity(intent);
//                output.setText("Video File : " +data.getData());

                // Video captured and saved to fileUri specified in the Intent
//                Toast.makeText(Upload_Video_Activity.this, "Video saved to:" +data.getData(), Toast.LENGTH_LONG).show();
                    finish();
            } else if (resultCode == RESULT_CANCELED) {

//                output.setText("User cancelled the video capture.");

                // User cancelled the video capture
                Toast.makeText(Upload_Video_Activity.this, "User cancelled the video capture.",
                        Toast.LENGTH_LONG).show();

            } else {

//                output.setText("Video capture failed.");

                // Video capture failed, advise user
                Toast.makeText(Upload_Video_Activity.this, "Video capture failed.",
                        Toast.LENGTH_LONG).show();
            }
        }else if (requestCode == RESULT_LOAD_VIDEO){
            if (resultCode == RESULT_OK) {
                if (data==null)return;
//                File file = new File(data.getData().getEncodedPath());//getPath());
//                file=new File(getPath(Uri.fromFile(file)));
               String f= getRealPathFromURI(data.getData());
                System.out.println("====file====choose===" + f);
//                System.out.println("====file====choose===" + file.getAbsolutePath());
                File copy_path=null;
                if (f==null){
                    Toast.makeText(Upload_Video_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    copy_path= APIUtility.getFileFromURL(Upload_Video_Activity.this,"chapter_video.mp4");
                    APIUtility.copyDirectory(new File(f),copy_path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (copy_path!=null) {
                    Intent intent = new Intent(Upload_Video_Activity.this, JW_Player_Activity.class);
                    intent.putExtra("file_path", copy_path.getAbsolutePath());
                    intent.putExtra("screen","upload");
                    startActivity(intent);
                    finish();
//                uploadDialog(data.getData().getPath());
//                output.setText("Video File : " +data.getData());

                    // Video captured and saved to fileUri specified in the Intent
//                    Toast.makeText(Upload_Video_Activity.this, "Video selected from:" + data.getData(), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(Upload_Video_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                }

            }
        }
    }
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.VideoColumns.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result=null;
        try {
            Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return result;
    }
}
