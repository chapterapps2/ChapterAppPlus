package com.chaptervitamins.Suggestions;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.APIUtility;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Upload_Music_Activity extends AppCompatActivity {
    private LinearLayout recording_ll,gallery_ll;
    private ImageView back;
    public static final int RESULT_LOAD_VIDEO = 3;
    public static final int REQUESTCODE_RECORDING = 4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload__music);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recording_ll=(LinearLayout)findViewById(R.id.recording_ll);
        gallery_ll=(LinearLayout)findViewById(R.id.gallery_ll);
        recording_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent =
//                        new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
//                if (isAvailable(getApplicationContext(), intent)) {
//                    startActivityForResult(intent,
//                            REQUESTCODE_RECORDING);
//                }else {
                Intent intent=new Intent(Upload_Music_Activity.this,Audio_Recorder_Activity.class);
                    startActivityForResult(intent,
                            REQUESTCODE_RECORDING);
//                }
            }
        });
        gallery_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
                mediaChooser.setType("audio/*");
                startActivityForResult(mediaChooser, RESULT_LOAD_VIDEO);
            }
        });
    }
    public static boolean isAvailable(Context ctx, Intent intent) {
        final PackageManager mgr = ctx.getPackageManager();
        List<ResolveInfo> list =
                mgr.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUESTCODE_RECORDING) {
            if (resultCode == RESULT_OK) {
                Uri audioUri = intent.getData();
                // make use of this MediaStore uri
                // e.g. store it somewhere
                String f= getRealPathFromURI(intent.getData());
                System.out.println("====file====choose===" + f);
//                System.out.println("====file====choose===" + file.getAbsolutePath());
                File copy_path=null;
                if (f==null){
                    Toast.makeText(Upload_Music_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    copy_path= APIUtility.getFileFromURL(Upload_Music_Activity.this,"chapter_audio.mp3");
                    APIUtility.copyDirectory(new File(f),copy_path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (copy_path!=null) {
                    Intent intent1 = new Intent(Upload_Music_Activity.this, Play_Audio_Activity.class);
                    intent1.putExtra("file_path", copy_path.getAbsolutePath());
                    intent.putExtra("screen","upload");
                    startActivity(intent1);
                    finish();
//                uploadDialog(data.getData().getPath());
//                output.setText("Video File : " +data.getData());

                    // Video captured and saved to fileUri specified in the Intent
//                    Toast.makeText(Upload_Video_Activity.this, "Video selected from:" + data.getData(), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(Upload_Music_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                }
            }
            else {
                // react meaningful to problems
                System.out.println("======URI AUDIO= ERROR===");
            }
        }else if (requestCode == RESULT_LOAD_VIDEO){
            if (resultCode == RESULT_OK) {
                if (intent==null)return;
//                File file = new File(data.getData().getEncodedPath());//getPath());
//                file=new File(getPath(Uri.fromFile(file)));
                String f= getRealPathFromURI(intent.getData());
                System.out.println("====file====choose===" + f);
//                System.out.println("====file====choose===" + file.getAbsolutePath());
                File copy_path=null;
                if (f==null){
                    Toast.makeText(Upload_Music_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    copy_path= APIUtility.getFileFromURL(Upload_Music_Activity.this,"chapter_audio.mp3");
                    APIUtility.copyDirectory(new File(f),copy_path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (copy_path!=null) {
                    Intent intent1 = new Intent(Upload_Music_Activity.this, Play_Audio_Activity.class);
                    intent1.putExtra("file_path", copy_path.getAbsolutePath());
                    intent1.putExtra("screen","upload");
                    startActivity(intent1);
                    finish();
//                uploadDialog(data.getData().getPath());
//                output.setText("Video File : " +data.getData());

                    // Video captured and saved to fileUri specified in the Intent
//                    Toast.makeText(Upload_Video_Activity.this, "Video selected from:" + data.getData(), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(Upload_Music_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                }

            }
        }
        else {
            super.onActivityResult(requestCode,
                    resultCode, intent);
        }
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result=null;
        try {
            Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return result;
    }
}
