package com.chaptervitamins.Suggestions;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.APIUtility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Upload_Image_Activity extends AppCompatActivity {
    private LinearLayout recording_ll,gallery_ll;
    private ImageView back;
    public static final int RESULT_LOAD_VIDEO = 3;
    final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    Uri file=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload__image);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recording_ll=(LinearLayout)findViewById(R.id.recording_ll);
        gallery_ll=(LinearLayout)findViewById(R.id.gallery_ll);
        recording_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*************************** Camera Intent Start ************************/

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
             String   FILENAME = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())+".jpg";

                try {
                    file = Uri.fromFile(APIUtility.getFileFromURL(Upload_Image_Activity.this, FILENAME));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                /*************************** Camera Intent End ************************/

            }
        });
        gallery_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
                mediaChooser.setType("image/*");
                startActivityForResult(mediaChooser, RESULT_LOAD_VIDEO);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ( requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

            if ( resultCode == RESULT_OK) {

                /*********** Load Captured Image And Data Start ****************/

//                String imageId = convertImageUriToFile( imageUri,Upload_Image_Activity.this);

//                File file = new File(data.getData().getEncodedPath());//getPath());
//                file=new File(getPath(Uri.fromFile(file)));
                String f= getRealPathFromURI(file);
                System.out.println("====file====choose===" + f);
//                System.out.println("====file====choose===" + file.getAbsolutePath());
                File copy_path=null;
                if (f==null){
                    Toast.makeText(Upload_Image_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    copy_path= APIUtility.getFileFromURL(Upload_Image_Activity.this,"chapter_photo.png");
                    APIUtility.copyDirectory(new File(f),copy_path);
                    new File(f).delete();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (copy_path!=null) {
                    Intent intent = new Intent(Upload_Image_Activity.this, Show_Image_Activity.class);
                    intent.putExtra("file_path", copy_path.getAbsolutePath());
                    startActivity(intent);
                    finish();
//                uploadDialog(data.getData().getPath());
//                output.setText("Video File : " +data.getData());

                    // Video captured and saved to fileUri specified in the Intent
//                    Toast.makeText(Upload_Video_Activity.this, "Video selected from:" + data.getData(), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(Upload_Image_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                }

            } else if ( resultCode == RESULT_CANCELED) {

                Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
            }
        }else if (requestCode == RESULT_LOAD_VIDEO){
            if (resultCode == RESULT_OK) {
                if (data==null)return;
//                File file = new File(data.getData().getEncodedPath());//getPath());
//                file=new File(getPath(Uri.fromFile(file)));
                String f= getRealPathFromURI(data.getData());
                System.out.println("====file====choose===" + f);
//                System.out.println("====file====choose===" + file.getAbsolutePath());
                File copy_path=null;
                if (f==null){
                    Toast.makeText(Upload_Image_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    copy_path= APIUtility.getFileFromURL(Upload_Image_Activity.this,"chapter_photo.png");
                    APIUtility.copyDirectory(new File(f),copy_path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (copy_path!=null) {
                    Intent intent = new Intent(Upload_Image_Activity.this, Show_Image_Activity.class);
                    intent.putExtra("file_path", copy_path.getAbsolutePath());
                    startActivity(intent);
                    finish();
//                uploadDialog(data.getData().getPath());
//                output.setText("Video File : " +data.getData());

                    // Video captured and saved to fileUri specified in the Intent
//                    Toast.makeText(Upload_Video_Activity.this, "Video selected from:" + data.getData(), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(Upload_Image_Activity.this, "Please try again!", Toast.LENGTH_LONG).show();
                }

            }
        }
    }


    /************ Convert Image Uri path to physical path **************/

    public static String convertImageUriToFile ( Uri imageUri, Activity activity )  {

        Cursor cursor = null;
        int imageID = 0;

        try {

            /*********** Which columns values want to get *******/
            String [] proj={
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Thumbnails._ID,
                    MediaStore.Images.ImageColumns.ORIENTATION
            };

            cursor = activity.managedQuery(

                    imageUri,         //  Get data for specific image URI
                    proj,             //  Which columns to return
                    null,             //  WHERE clause; which rows to return (all rows)
                    null,             //  WHERE clause selection arguments (none)
                    null              //  Order-by clause (ascending by name)

            );

            //  Get Query Data

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
            int columnIndexThumb = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
            int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            //int orientation_ColumnIndex = cursor.
            //    getColumnIndexOrThrow(MediaStore.Images.ImageColumns.ORIENTATION);

            int size = cursor.getCount();

            /*******  If size is 0, there are no images on the SD Card. *****/

            if (size == 0) {


//                imageDetails.setText("No Image");
            }
            else
            {

                int thumbID = 0;
                if (cursor.moveToFirst()) {

                    /**************** Captured image details ************/

                    /*****  Used to show image on view in LoadImagesFromSDCard class ******/
                    imageID     = cursor.getInt(columnIndex);

                    thumbID     = cursor.getInt(columnIndexThumb);

                    String Path = cursor.getString(file_ColumnIndex);

                    //String orientation =  cursor.getString(orientation_ColumnIndex);

                    String CapturedImageDetails = " CapturedImageDetails : \n\n"
                            +" ImageID :"+imageID+"\n"
                            +" ThumbID :"+thumbID+"\n"
                            +" Path :"+Path+"\n";

                    // Show Captured Image detail on activity
//                    imageDetails.setText( CapturedImageDetails );

                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        // Return Captured Image ImageID ( By this ImageID Image will load from sdcard )

        return ""+imageID;
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result=null;
        try {
            Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return result;
    }
}
