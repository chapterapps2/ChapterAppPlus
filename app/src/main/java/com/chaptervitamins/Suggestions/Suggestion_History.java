package com.chaptervitamins.Suggestions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.play_video.JW_Player_Activity;
import com.chaptervitamins.utility.Suggestion_Utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Android on 10/27/2016.
 */

public class Suggestion_History extends Fragment implements UploadListener {
    LinearLayout ll_below;
    WebServices webServices;
    private ListView listview;
    private ArrayList<Suggestion_Utils> suggestionUtilsArrayList = new ArrayList<>();
    private LayoutInflater mInflater;
    private ListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.suggestion_history, container, false);
        webServices = new WebServices();
        ll_below = (LinearLayout) v.findViewById(R.id.ll_below);
        listview = (ListView) v.findViewById(R.id.listview);
        mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ll_below.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Create_Suggestion.class);
                startActivityForResult(intent,200);
            }
        });
        for (int i = 0; i < Suggestion_Main_Activity.suggestionUtilsArrayList.size(); i++) {
            if (Suggestion_Main_Activity.suggestionUtilsArrayList.get(i).getType().equalsIgnoreCase("SUGGESTION"))
                suggestionUtilsArrayList.add(Suggestion_Main_Activity.suggestionUtilsArrayList.get(i));
        }
        adapter = new ListAdapter();
        listview.setAdapter(adapter);
        return v;
    }

    public void notifyData() {
        suggestionUtilsArrayList = new ArrayList<>();
        for (int i = 0; i < Suggestion_Main_Activity.suggestionUtilsArrayList.size(); i++) {
            if (Suggestion_Main_Activity.suggestionUtilsArrayList.get(i).getType().equalsIgnoreCase("SUGGESTION"))
                suggestionUtilsArrayList.add(Suggestion_Main_Activity.suggestionUtilsArrayList.get(i));
        }
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    @Override
    public void error(String error) {
        Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
    }

    @Override
    public void complete(int media_id, String type,String resp) {
        Intent intent1 = new Intent(getActivity(), Play_Audio_Activity.class);
        intent1.putExtra("file_path",type);
        intent1.putExtra("screen","");
        startActivity(intent1);
    }


    class ListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return suggestionUtilsArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.suggestion_query_row, null);
                TextView sub_txt = (TextView) convertView.findViewById(R.id.sub_txt);
                TextView desc_txt = (TextView) convertView.findViewById(R.id.desc_txt);
                TextView resp_txt = (TextView) convertView.findViewById(R.id.resp_txt);
                TextView date_txt = (TextView) convertView.findViewById(R.id.date_txt);
                LinearLayout view_att_ll=(LinearLayout)convertView.findViewById(R.id.view_att_ll);
                if (!suggestionUtilsArrayList.get(position).getFile_url().equalsIgnoreCase(""))
                    view_att_ll.setVisibility(View.VISIBLE);
                else view_att_ll.setVisibility(View.GONE);
                String subString = "<b>" + "Subject : " + "</b> " + suggestionUtilsArrayList.get(position).getCategory();
                sub_txt.setText(Html.fromHtml(subString));
                 subString = "<b>" + "Suggestion : " + "</b> " + suggestionUtilsArrayList.get(position).getMessage();
                desc_txt.setText(Html.fromHtml(subString));
                String answer="Unanswered";
                if (!suggestionUtilsArrayList.get(position).getAnswers().equalsIgnoreCase("false"))
                    answer=suggestionUtilsArrayList.get(position).getAnswers();
                resp_txt.setText("Response : " + answer);
                date_txt.setText("Submitted on : " + suggestionUtilsArrayList.get(position).getAdded_on());

                view_att_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (suggestionUtilsArrayList.get(position).getFile_type().equalsIgnoreCase("IMAGE") ) {
                            Intent intent = new Intent(getActivity(), Image_Show_Activity.class);
                            intent.putExtra("imgurl", suggestionUtilsArrayList.get(position).getFile_url());
                            intent.putExtra("imguri", "");
                            intent.putExtra("userid",suggestionUtilsArrayList.get(position).getUser_id());
                            startActivity(intent);
                        } else if (suggestionUtilsArrayList.get(position).getFile_type().contains("VIDEO")) {
                            Intent intent = new Intent(getActivity(), JW_Player_Activity.class);
                            intent.putExtra("file_path", suggestionUtilsArrayList.get(position).getFile_url());
                            intent.putExtra("screen","");
                            startActivity(intent);
                        } else if (suggestionUtilsArrayList.get(position).getFile_type().contains("AUDIO")) {
                            File dir = new File(getActivity().getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Downloaded/");
                            if (!dir.exists())
                                dir.mkdirs();
                            String url=suggestionUtilsArrayList.get(position).getFile_url();
                            String f_name = url.substring(url.lastIndexOf("/")).substring(1);
                            File file = new File(dir, f_name);
                            if (file.exists()) {
                                Intent intent1 = new Intent(getActivity(), Play_Audio_Activity.class);
                                intent1.putExtra("file_path", url);
                                intent1.putExtra("screen", "");
                                startActivity(intent1);
                            }else{
                               new APIUtility().DownloadFIle(getActivity(),url,f_name,Suggestion_History.this);
                            }
                        }
                    }
                });

            }
            return convertView;
        }
    }
}
