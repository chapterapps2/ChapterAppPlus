package com.chaptervitamins.Suggestions;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;

import java.io.File;


public class Show_Image_Activity extends BaseActivity implements UploadListener {
    private TextView video_title;
    private ImageView show_img,cancel_img,ok_img,back;
    String FILENAME="";
    long size=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_show_image);
        video_title=(TextView)findViewById(R.id.video_title);
//        video_title.setText(getIntent().getStringExtra("desc"));
        show_img=(ImageView)findViewById(R.id.show_img);
        cancel_img=(ImageView)findViewById(R.id.cancel_img);
        ok_img=(ImageView)findViewById(R.id.ok_img);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Suggestion_Main_Activity.MEDIA_ID="";
                finish();
            }
        });
        ok_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if (FILENAME.equalsIgnoreCase("")&&size==0){
                   Toast.makeText(Show_Image_Activity.this, "Can't Upload. Please try again.", Toast.LENGTH_LONG).show();
                   return;
               }

                new File_Uploader(Show_Image_Activity.this, getIntent().getStringExtra("file_path"), APIUtility.UPLOAD_FILE, FILENAME, size, "IMAGE", true, Show_Image_Activity.this);
            }
        });
        System.out.println("======="+getIntent().getStringExtra("file_path"));
        File file=new File(getIntent().getStringExtra("file_path"));
        if (file.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

            String url=getIntent().getStringExtra("file_path");
            if (url!=null) {
                String[] r=url.split("/");
                System.out.println("===="+r[r.length-1]);
                FILENAME=r[r.length-1];
                size=file.length();
            }

            show_img.setImageBitmap(myBitmap);
        }

    }
    private void uploadDialog(String filepath){
        final Dialog dialog = new Dialog(Show_Image_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_dialog);
        TextView title_dialog=(TextView)dialog.findViewById(R.id.title_dialog);
        TextView desc_dialog=(TextView)dialog.findViewById(R.id.desc_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button submit_btn = (Button) dialog.findViewById(R.id.submit_btn);
        title_dialog.setVisibility(View.GONE);
        cancel_btn.setVisibility(View.GONE);
        submit_btn.setVisibility(View.VISIBLE);
        desc_dialog.setText("Your Answer is successfully submitted to \n Admin for review");
        desc_dialog.setTextSize(10);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        ok_btn.setVisibility(View.GONE);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f=new File(getIntent().getStringExtra("file_path"));
                if (f.exists())f.delete();
                dialog.dismiss();
            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f=new File(getIntent().getStringExtra("file_path"));
                if (f.exists())f.delete();
                Toast.makeText(Show_Image_Activity.this,"Successfully Uploaded!",Toast.LENGTH_LONG).show();
                finish();
            }
        });
        dialog.show();
    }


    @Override
    public void error(String error) {
        Suggestion_Main_Activity.MEDIA_ID="";
    }

    @Override
    public void complete(int media_id, String type,String Response) {
        Suggestion_Main_Activity.MEDIA_ID=media_id+"";
//        Toast.makeText(this, "Image Uploaded Successfully.", Toast.LENGTH_SHORT).show();
        finish();
    }
}
