package com.chaptervitamins.Suggestions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 10/27/2016.
 */

public class Create_Query extends BaseActivity {
    private ImageView back;
    private Spinner query_spinner;
    LayoutInflater mInflater;
    Spinner_Adapter adapter;
    private LinearLayout attach_ll;
    private Button mBtnSubmit;
    private EditText desc_edt;
    String Selected_Item = "";
    private WebServices webServices;
    private TextView count_desc_txt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.create_query);
        query_spinner = (Spinner) findViewById(R.id.query_spinner);
        attach_ll = (LinearLayout) findViewById(R.id.attach_ll);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
//        attach_ll.setVisibility(View.GONE);
        mInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final ArrayList<String> arrayList = new ArrayList<>();
        if(!TextUtils.isEmpty(APIUtility.organizationModel.getQueryCategory())){
            String[] categories = APIUtility.organizationModel.getQueryCategory().split(",");
            for(int i=0; i<categories.length;i++){
                arrayList.add(categories[i]);
            }
        }

        /*arrayList.add("Product");
        arrayList.add("Underwriting");
        arrayList.add("Credit");
        arrayList.add("Other");*/
        webServices = new WebServices();
        adapter = new Spinner_Adapter(arrayList);
        query_spinner.setAdapter(adapter);
        query_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Selected_Item = arrayList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        attach_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Create_Query.this, ChooseMedia_Activity.class);
                startActivityForResult(intent, 200);
            }
        });
        back = (ImageView) findViewById(R.id.back);
        desc_edt = (EditText) findViewById(R.id.desc_edt);
        count_desc_txt = (TextView) findViewById(R.id.count_desc_txt);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        desc_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int remaining = 1000 - (desc_edt.getText().toString().length());
                count_desc_txt.setText(remaining + " Characters Remaining");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String sub = Selected_Item.toString().trim();
                final String sugg = desc_edt.getText().toString().trim();
                if (sub.equalsIgnoreCase("")) {
                    Toast.makeText(Create_Query.this, "Please Enter Subject..", Toast.LENGTH_LONG).show();
                    return;
                }
                if (sugg.equalsIgnoreCase("")) {
                    Toast.makeText(Create_Query.this, "Please Enter Suggestion..", Toast.LENGTH_LONG).show();
                    return;
                }
                if (WebServices.isNetworkAvailable(Create_Query.this)) {
                    final ProgressDialog dialog = ProgressDialog.show(Create_Query.this, "", "Please wait...");
                    dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                    final Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (dialog != null) dialog.dismiss();
                            if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                                Utils.callInvalidSession(Create_Query.this,APIUtility.ADD_SUGGESTION_QUERY);
                            } else {
                                Toast.makeText(Create_Query.this, "Query submitted successfully. Thank You!", Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    };
                    new Thread() {
                        @Override
                        public void run() {
                            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                            nameValuePair.add(new BasicNameValuePair("category",sub));
                            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                            nameValuePair.add(new BasicNameValuePair("message", sugg));
                            nameValuePair.add(new BasicNameValuePair("branch_id",WebServices.mLoginUtility.getBranch_id()));
                            nameValuePair.add(new BasicNameValuePair("type", "QUERY"));
                            nameValuePair.add(new BasicNameValuePair("media_id", Suggestion_Main_Activity.MEDIA_ID));
                            nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                            String resp = webServices.callServices(nameValuePair, APIUtility.ADD_SUGGESTION_QUERY);
                            Log.d(" Response:", resp.toString());
                            if(!webServices.isValid(resp)) {
                                handler.sendEmptyMessage(1);
                                return;
                            }
                            handler.sendEmptyMessage(0);
                        }
                    }.start();
                } else {
                    Toast.makeText(Create_Query.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private class Spinner_Adapter extends BaseAdapter {
        ArrayList<String> mStrings = new ArrayList<>();

        public Spinner_Adapter(ArrayList<String> strings) {
            mStrings = strings;
        }

        @Override
        public int getCount() {
            return mStrings.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.query_spinner_row, null);
                TextView title = (TextView) convertView.findViewById(R.id.query_spinner_row_txt);
                title.setText(mStrings.get(position));
            }
            return convertView;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("=====" + Suggestion_Main_Activity.MEDIA_ID);
    }
}
