package com.chaptervitamins.chat.activities;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.chat.adapters.ChatGroupAdapter;
import com.chaptervitamins.chat.models.ChatGroupModel;
import com.chaptervitamins.newcode.utils.WrapLinearLayoutManager;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.api.JsonParsingUtils;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.newcode.utils.APIUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class ChatGroupActivity extends BaseActivity implements View.OnClickListener{
    private RecyclerView mRvChat;
    private Toolbar mToolbar;
    private ChatGroupAdapter mChatGroupAdapter;
    private ArrayList<ChatGroupModel> chatGroupModelArrayList = new ArrayList<>();
    private FloatingActionButton mFabNewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_group);

        findViews();
        setClickListeners();
        initData();
    }

    private void setClickListeners() {
        mFabNewMessage.setOnClickListener(this);
    }

    private void initData() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Groups");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRvChat.setLayoutManager(new WrapLinearLayoutManager(this));
        getGroupsFromServer();
    }

    private void getGroupsFromServer() {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "");
        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));

        new GenericApiCall(this, APIUtility.GETALLGROUP, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                if (dialog != null)
                    dialog.dismiss();
                String response = (String) result;
                chatGroupModelArrayList.addAll(JsonParsingUtils.parseGroupsData(response));
                mChatGroupAdapter = new ChatGroupAdapter(chatGroupModelArrayList);
                mRvChat.setAdapter(mChatGroupAdapter);
            }

            @Override
            public void onError(ErrorModel error) {
                if (dialog != null)
                    dialog.dismiss();
                Toast.makeText(ChatGroupActivity.this, "Something went wrong in fetching data!", Toast.LENGTH_SHORT).show();
            }
        }).execute();
    }

    private void findViews() {
        mRvChat = (RecyclerView) findViewById(R.id.rv_group);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mFabNewMessage = (FloatingActionButton) findViewById(R.id.fab_new_message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_groups, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fab_new_message:
                break;
        }
    }
}
