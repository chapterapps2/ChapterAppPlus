package com.chaptervitamins.chat.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.chat.models.ChatGroupModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 12-12-2017.
 */

public class ChatGroupAdapter extends RecyclerView.Adapter<ChatGroupAdapter.ViewHolder> {
    private ArrayList<ChatGroupModel> chatGroupModelAl;
    private Context mContext;

    public ChatGroupAdapter(ArrayList<ChatGroupModel> chatGroupModelAl) {
        this.chatGroupModelAl = chatGroupModelAl;
    }

    @Override
    public ChatGroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_chat_group, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatGroupAdapter.ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            ChatGroupModel chatGroupModel = chatGroupModelAl.get(position);
            if (chatGroupModel != null) {
                holder.tvGroupName.setText(chatGroupModel.getGroup_name());
                holder.tvGroupMembers.setText(chatGroupModel.getGroup_members()+" members");
                if(!TextUtils.isEmpty(chatGroupModel.getImage()))
                    new Picasso.Builder(mContext)
//                            .downloader(new OkHttpDownloader(mContext, Integer.MAX_VALUE))
                            .build().load(chatGroupModel.getImage()).error(R.drawable.profile).placeholder(R.drawable.profile).into(holder.ivGroupProfile);

            }
        }

    }

    @Override
    public int getItemCount() {
        return chatGroupModelAl.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivGroupProfile;
        private TextView tvGroupName, tvUnreadMessage, tvGroupMembers;

        public ViewHolder(View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            ivGroupProfile = (ImageView) itemView.findViewById(R.id.iv_group_profile);
            tvGroupName = (TextView) itemView.findViewById(R.id.tv_group_name);
            tvUnreadMessage = (TextView) itemView.findViewById(R.id.tv_unread_count);
            tvGroupMembers = (TextView) itemView.findViewById(R.id.tv_members_count);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
