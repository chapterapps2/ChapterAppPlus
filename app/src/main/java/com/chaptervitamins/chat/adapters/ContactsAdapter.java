package com.chaptervitamins.chat.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.chat.models.ContactModel;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 14-12-2017.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {
    private ArrayList<ContactModel> mContactModelArrayList;
    private Context mContext;
    public ContactsAdapter(ArrayList<ContactModel> contactModelArrayList) {
        this.mContactModelArrayList = contactModelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(mContext == null)
            mContext = parent.getContext();
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mContactModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUserName;
        private ImageView ivUser;
        public ViewHolder(View itemView) {
            super(itemView);

        }
    }
}
