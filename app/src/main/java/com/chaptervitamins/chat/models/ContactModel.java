package com.chaptervitamins.chat.models;

import java.io.Serializable;

/**
 * Created by Vijay Antil on 14-12-2017.
 */

public class ContactModel implements Serializable {
    private String imageUrl,name,userId;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
