package com.chaptervitamins;

import android.support.multidex.MultiDexApplication;

import com.chaptervitamins.newcode.utils.Constants;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.splunk.mint.Mint;

/**
 * Created by Vijay Antil on 28-08-2017.
 */

public class CentumApplication extends MultiDexApplication {

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        // Obtain the FirebaseAnalytics instanc.
        FirebaseApp.initializeApp(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //initialize session for Mint Splunk
        Mint.initAndStartSession(this, Constants.CRASH_REPORT_KEY);

        //facebook login------------
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}
