package com.chaptervitamins.Materials;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ReadResponseUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by abcd on 5/21/2016.
 */
public class SubmitData extends AsyncTask<String, Void, Boolean> {
    WebServices webServices;
    private String mUserid;
    private String courseId;
    private String mMaterial_id, assignMaterialId, moduleId, seenCount, totalCount, completionPercentage, errorMessage;
    private String mType;
    private MeterialUtility meterialUtility;
    private CoinsAllocatedModel coinsAllocatedModel;
    DataBase mDataBase;
    Context mContext;
    private boolean sendStartTime;
    private OnSubmitDataListener mListener;

    public SubmitData(Context context, MeterialUtility meterialUtility, String userid, CoinsAllocatedModel coinsAllocatedModel, DataBase dataBase) {
        webServices = new WebServices();
        mUserid = userid;
        this.meterialUtility = meterialUtility;
        this.assignMaterialId = meterialUtility.getAssign_material_id();
        this.moduleId = meterialUtility.getModule_id();
        mUserid = userid;
        this.courseId = meterialUtility.getCourse_id();
        mMaterial_id = meterialUtility.getMaterial_id();
        mType = meterialUtility.getMaterial_type();
        this.coinsAllocatedModel = coinsAllocatedModel;
        mDataBase = dataBase;
        mContext = context;
        this.seenCount = meterialUtility.getSeen_count();
        this.totalCount = meterialUtility.getTotal_count();
        this.completionPercentage = meterialUtility.getComplete_per();
    }

    public SubmitData(Context context, MeterialUtility meterialUtility, String userid, CoinsAllocatedModel coinsAllocatedModel, DataBase dataBase, boolean sendStartTime) {
        webServices = new WebServices();
        mUserid = userid;
        this.meterialUtility = meterialUtility;
        this.assignMaterialId = meterialUtility.getAssign_material_id();
        this.moduleId = meterialUtility.getModule_id();
        mUserid = userid;
        this.courseId = meterialUtility.getCourse_id();
        mMaterial_id = meterialUtility.getMaterial_id();
        mType = meterialUtility.getMaterial_type();
        this.coinsAllocatedModel = coinsAllocatedModel;
        mDataBase = dataBase;
        mContext = context;
        this.seenCount = meterialUtility.getSeen_count();
        this.totalCount = meterialUtility.getTotal_count();
        this.completionPercentage = meterialUtility.getComplete_per();
        this.sendStartTime = sendStartTime;
    }

    public SubmitData(Context context, MeterialUtility meterialUtility, String userid, CoinsAllocatedModel coinsAllocatedModel, DataBase dataBase, OnSubmitDataListener mListener) {
        webServices = new WebServices();
        mUserid = userid;
        this.meterialUtility = meterialUtility;
        this.assignMaterialId = meterialUtility.getAssign_material_id();
        this.moduleId = meterialUtility.getModule_id();
        mUserid = userid;
        this.courseId = meterialUtility.getCourse_id();
        mMaterial_id = meterialUtility.getMaterial_id();
        mType = meterialUtility.getMaterial_type();
        this.coinsAllocatedModel = coinsAllocatedModel;
        mDataBase = dataBase;
        mContext = context;
        this.seenCount = meterialUtility.getSeen_count();
        this.totalCount = meterialUtility.getTotal_count();
        this.completionPercentage = meterialUtility.getComplete_per();
        this.mListener = mListener;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        String quizresponse = "";
        String correctQues = "0";
        String incorrectQues = "0";
        String result = "100";
        if (params.length != 0) {
            correctQues = params[0];
            incorrectQues = params[1];
            try {
                byte ptext[] = params[2].getBytes();
                quizresponse = new String(ptext, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                quizresponse = params[2];
                e.printStackTrace();
            }
            result = params[3];
            System.out.println("===params====" + params[0]);

        }
        String coinsCollected = "0";
        /*if (coinsAllocatedModel != null)
            if (coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                if (params.length == 4) {
                    coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(Integer.valueOf(params[3])));
                    nameValuePair.add(new BasicNameValuePair("coins_allocated", coinsCollected));
                } else {
                    coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(100));
                    nameValuePair.add(new BasicNameValuePair("coins_allocated", coinsCollected));
                }

            } else {
                nameValuePair.add(new BasicNameValuePair("coins_allocated", coinsCollected));
            }
        else {
            nameValuePair.add(new BasicNameValuePair("coins_allocated", coinsCollected));
        }*/

        if (coinsAllocatedModel != null && !TextUtils.isEmpty(coinsAllocatedModel.getCurrentCoins()) && !coinsAllocatedModel.getCurrentCoins().equals("0")) {
            nameValuePair.add(new BasicNameValuePair("coins_allocated", coinsAllocatedModel.getCurrentCoins()));
        } else
            nameValuePair.add(new BasicNameValuePair("coins_allocated", coinsCollected));

// Building post parameters, key and value pair
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("user_id", mUserid));
//        nameValuePair.add(new BasicNameValuePair("assign_course_id", HomeActivity.ASSIGNEDCOURSEID));
        nameValuePair.add(new BasicNameValuePair("material_id", meterialUtility.getMaterial_id()));
        nameValuePair.add(new BasicNameValuePair("push_token", WebServices.mLoginUtility.getDevice().getPush_token()));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("correct_question", correctQues));
        if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
            if (!sendStartTime) {
                nameValuePair.add(new BasicNameValuePair("finish_time", meterialUtility.getMaterialEndTime()));
                nameValuePair.add(new BasicNameValuePair("start_time", ""));
                nameValuePair.add(new BasicNameValuePair("is_completed", "YES"));
                nameValuePair.add(new BasicNameValuePair("result", "0"));
                nameValuePair.add(new BasicNameValuePair("completion_per", "0"));
            } else {
                nameValuePair.add(new BasicNameValuePair("start_time", meterialUtility.getMaterialStartTime()));
                nameValuePair.add(new BasicNameValuePair("finish_time", ""));
                nameValuePair.add(new BasicNameValuePair("is_completed", "NO"));
                nameValuePair.add(new BasicNameValuePair("result", "0"));
                nameValuePair.add(new BasicNameValuePair("completion_per", "0"));

            }
        } else {
            nameValuePair.add(new BasicNameValuePair("finish_time", meterialUtility.getMaterialEndTime()));
            nameValuePair.add(new BasicNameValuePair("start_time", meterialUtility.getMaterialStartTime()));
            nameValuePair.add(new BasicNameValuePair("result", result));
            nameValuePair.add(new BasicNameValuePair("completion_per", completionPercentage));
            if (meterialUtility.getMax_result().equals("100"))
                nameValuePair.add(new BasicNameValuePair("is_completed", "YES"));
            else
                nameValuePair.add(new BasicNameValuePair("is_completed", "NO"));
        }
        nameValuePair.add(new BasicNameValuePair("incorrect_question", incorrectQues));
        nameValuePair.add(new BasicNameValuePair("question_response", quizresponse));
        nameValuePair.add(new BasicNameValuePair("response_type", meterialUtility.getMaterial_type()));

        nameValuePair.add(new BasicNameValuePair("taken_device", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("time_taken", meterialUtility.getTimeTaken()));
        nameValuePair.add(new BasicNameValuePair("assign_material_id", meterialUtility.getAssign_material_id()));
        nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
        nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
        nameValuePair.add(new BasicNameValuePair("module_id", meterialUtility.getModule_id()));
        nameValuePair.add(new BasicNameValuePair("course_id", meterialUtility.getCourse_id()));
        nameValuePair.add(new BasicNameValuePair("seen_count", seenCount));
        nameValuePair.add(new BasicNameValuePair("total_count", totalCount));

        //Log.v("submit time","submit start time: "+meterialUtility.getMaterialStartTime()+"end "+meterialUtility.getMaterialEndTime()+"time"+meterialUtility.getTimeTaken());

        String data = "";
        for (NameValuePair nameValuePair1 : nameValuePair) {
            data = data + nameValuePair1.getName() + " : " + nameValuePair1.getValue() + " ,";
        }
        Log.d("vijay", data);
        String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_RESPONSE);
        Log.d(" Response:", resp.toString());
        if (!webServices.isValid(resp)) {
            if (!coinsCollected.equals("0"))
                coinsAllocatedModel.setRedeem("FALSE");
            errorMessage = WebServices.ERRORMSG;
            String errorCode = webServices.getErrorCode(resp);
            if (!TextUtils.isEmpty(errorCode) && errorCode.equalsIgnoreCase("1005"))
                DataBase.getInstance(mContext).deleteRowGetResponse(meterialUtility.getMaterial_id(), mUserid, meterialUtility.getMaterialStartTime());
            webServices.setProgress(mDataBase, courseId, false);
            return false;
        } else {
            DataBase.getInstance(mContext).deleteRowGetResponse(meterialUtility.getMaterial_id(), mUserid, meterialUtility.getMaterialStartTime());

        }

        //------------------------------update coins in profile database--------------------
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
            WebServices.updateTotalCoins(mContext, coinsCollected);


        ArrayList<ReadResponseUtility> list = mDataBase.getResponseData();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ReadResponseUtility readResponseUtility = list.get(i);
            nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("user_id", readResponseUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("material_id", readResponseUtility.getMaterial_id()));
            nameValuePair.add(new BasicNameValuePair("push_token", WebServices.mLoginUtility.getDevice().getPush_token()));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("correct_question", readResponseUtility.getCorrect_question()));
            nameValuePair.add(new BasicNameValuePair("finish_time", readResponseUtility.getFinish_time()));
            nameValuePair.add(new BasicNameValuePair("incorrect_question", readResponseUtility.getIncorrect_question()));
            nameValuePair.add(new BasicNameValuePair("question_response", readResponseUtility.getQuiz_response()));
            nameValuePair.add(new BasicNameValuePair("response_type", readResponseUtility.getResponse_type()));
            nameValuePair.add(new BasicNameValuePair("result", readResponseUtility.getResult()));
            nameValuePair.add(new BasicNameValuePair("start_time", readResponseUtility.getStart_time()));
            nameValuePair.add(new BasicNameValuePair("taken_device", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("time_taken", readResponseUtility.getTime_taken()));
            nameValuePair.add(new BasicNameValuePair("coins_allocated", readResponseUtility.getCoins_allocated()));
            nameValuePair.add(new BasicNameValuePair("assign_material_id", readResponseUtility.getAssign_material_id()));
            nameValuePair.add(new BasicNameValuePair("organization_id", readResponseUtility.getOrganization_id()));
            nameValuePair.add(new BasicNameValuePair("branch_id", readResponseUtility.getBranch_id()));
            nameValuePair.add(new BasicNameValuePair("module_id", readResponseUtility.getModuleId()));
            nameValuePair.add(new BasicNameValuePair("course_id", readResponseUtility.getCourse_id()));
            nameValuePair.add(new BasicNameValuePair("seen_count", readResponseUtility.getSeen_count()));
            nameValuePair.add(new BasicNameValuePair("total_count", readResponseUtility.getTotal_count()));
            nameValuePair.add(new BasicNameValuePair("completion_per", readResponseUtility.getCompletion_per()));
            if (readResponseUtility.getCompletion_per().equals("100"))
                nameValuePair.add(new BasicNameValuePair("is_completed", "YES"));
            else
                nameValuePair.add(new BasicNameValuePair("is_completed", "NO"));

            data = "";
            for (NameValuePair nameValuePair1 : nameValuePair) {
                data = data + nameValuePair1.getName() + " : " + nameValuePair1.getValue() + " ,";
            }
            Log.d("vijay", data);
            String resp1 = webServices.callServices(nameValuePair, APIUtility.SUBMIT_RESPONSE);
            Log.d(" Response:", resp1.toString());
            if (webServices.isValid(resp)) {
                DataBase.getInstance(mContext).deleteRowGetResponse(readResponseUtility.getMaterial_id(), list.get(i).getUser_id(), readResponseUtility.getStart_time());
            } else {
                String errorCode = webServices.getErrorCode(resp);
                if (!TextUtils.isEmpty(errorCode) && errorCode.equalsIgnoreCase("1005"))
                    DataBase.getInstance(mContext).deleteRowGetResponse(meterialUtility.getMaterial_id(), mUserid, meterialUtility.getMaterialStartTime());

            }
        }

//        mDataBase.addGetResponseData(readResponseUtility);

        nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        resp = webServices.callServices(nameValuePair, APIUtility.GETREADRESPONSE);
        Log.d(" ReadResponse:", resp.toString());
        if (!webServices.isValid(resp)) {
            return false;
        } else {
            if (mDataBase.isData(WebServices.mLoginUtility.getUser_id(), "MyReadResponse"))
                mDataBase.addReadResponseData(WebServices.mLoginUtility.getUser_id(), resp);
            else {
                mDataBase.updateReadResponseData(WebServices.mLoginUtility.getUser_id(), resp);
            }
            HomeActivity.mReadResponse.clear();
            HomeActivity.mReadResponse.addAll(webServices.read_response_method(resp));
        }
        webServices.setProgress(mDataBase, courseId, false);
        return true;
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        if (success) {
            if (mListener != null)
                mListener.onSubmitSuccess();
        } else {
            if (mListener != null)
                mListener.onError(WebServices.ERRORMSG);
        }
        /*if (success) {
            if (coinsAllocatedModel != null && !TextUtils.isEmpty(coinsAllocatedModel.getRedeem()) && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                // entry in databasedatabase
            }
        } else {
            if (coinsAllocatedModel != null && !TextUtils.isEmpty(coinsAllocatedModel.getRedeem()) && coinsAllocatedModel.getRedeem().equalsIgnoreCase("TRUE")) {
                coinsAllocatedModel.setRedeem("FALSE");
                // entry in databasedatabase
            }
        }*/
        if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
            Utils.callInvalidSession(mContext, APIUtility.SUBMIT_RESPONSE);
            return;
        }


    }

    public static String diffDate(String dateStart, String dateStop) {
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.println(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            if (diffSeconds == 0) diffSeconds = 1;
            return diffSeconds + "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "1";
    }

    public interface OnSubmitDataListener {
        void onSubmitSuccess();

        void onError(String error);
    }
}
