/*
package com.centum.Materials;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.centum.BaseCustomView.BaseActivity;
import com.centum.CustomView.AutoLogout;
import com.centum.WebServices.WebServices;
import com.centum.R;
import com.centum.database.DataBase;
import com.centum.newcode.activities.HomeActivity;
import com.centum.utility.APIUtility;
import com.centum.utility.CoinsAllocatedModel;
import com.centum.utility.FlowingCourseUtils;
import com.centum.utility.MeterialUtility;
import com.splunk.mint.Mint;

import java.io.File;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import pl.droidsonroids.gif.GifTextView;

public class E_Learning_Activity extends BaseActivity implements View.OnClickListener {
    WebView webView;
    private ProgressBar progressBar3;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private Button btnPrevious, btnNext;
    String courseId, moduleId, materialId;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position;
    private GifTextView goldGif;
    */
/**
 * ATTENTION: This was auto-generated to implement the App Indexing API.
 * See https://g.co/AppIndexing/AndroidStudio for more information.
 *//*

    PowerManager.WakeLock fullWakeLock, partialWakeLock;
    private String redeem = "", noOfCoins = "";
    private DataBase dataBase;
    private MeterialUtility meterialUtility;
    private String isLink, url, filename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (APIUtility.TAKE_SCREENSHORT.equalsIgnoreCase("no"))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        // TODO: Update with your API key
        Mint.initAndStartSession(E_Learning_Activity.this, APIUtility.CRASH_REPORT_KEY);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_e__learning_);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_utility");
            isLink = bundle.getString("islink", "");
            url = bundle.getString("url");
            filename = bundle.getString("filename");
        }
//        materialId = getIntent().getStringExtra("meterial_id");


        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(materialId, true);
        position = FlowingCourseUtils.getPositionOfMeterial(materialId, meterialUtilityArrayList);
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course().equalsIgnoreCase("YES")) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }

        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);
        webView = (WebView) findViewById(R.id.webView);
//        WebSettings webSettings = webView.getSettings();
//
//        webView.getSettings().setAllowFileAccess(true);
//
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        HomeActivity.QUIZSTARTTIME = formattedDate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webView.getSettings().setAllowFileAccessFromFileURLs(true);
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }

        dataBase = DataBase.getInstance(E_Learning_Activity.this);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webSettings.setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setTextZoom(webView.getSettings().getTextZoom() + 50);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new MyWebViewClient());
        if (isLink.equalsIgnoreCase("false")) {
            if (url == null) {
                Toast.makeText(E_Learning_Activity.this, "Can't play the package. Could not locate the file.", Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            String[] r = url.split("/");
            System.out.println("====" + r[r.length - 1]);
            String filepath = "file://" + filename + "/" + r[r.length - 1];
            File file = new File(URI.create(filepath).getPath());
            if (!file.exists()) {
                Toast.makeText(E_Learning_Activity.this, "Can't play the package. Could not locate the file.", Toast.LENGTH_LONG).show();
                finish();
            }
            System.out.println("====uri===" + filepath);
            webView.loadUrl(filepath);
        } else {
            webView.loadUrl(filename);
//            webView.loadUrl("https://www.google.co.in");
            System.out.println("====uri=Online==" + filename);
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                FlowingCourseUtils.callFlowingCourseMaterial(E_Learning_Activity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                FlowingCourseUtils.callFlowingCourseMaterial(E_Learning_Activity.this, meterialUtilityArrayList, position, true);
                break;
        }
    }

    private class MyWebViewClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress >= 100) progressBar3.setVisibility(View.GONE);
            submitDataToServer();
            super.onProgressChanged(view, newProgress);
        }

    }

    @Override
    public void onBackPressed() {
//        partialWakeLock.acquire();
        webView.clearFormData();
        webView.destroy();
        webView.clearCache(true);
        Intent intent = new Intent();
        intent.putExtra("filename", filename);
        setResult(99, intent);
        submitDataToServer();
//        HomeActivity.QUIZSTARTTIME="";
        finish();
    }

    private void submitDataToServer() {
        coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(materialId);
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();

        String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(100, redeem));
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
            showGoldGif();
            Toast.makeText(this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
        }
        if (WebServices.isNetworkAvailable(E_Learning_Activity.this))
            new SubmitData(E_Learning_Activity.this, WebServices.mLoginUtility.getUser_id(), meterialUtility.getCourse_id(), meterialUtility.getMaterial_id(), meterialUtility.getModule_id(), meterialUtility.getAssign_material_id(), "SCROM", coinsAllocatedModel, DataBase.getInstance(E_Learning_Activity.this)).execute();
        else {

            new WebServices().addSubmitResponse(dataBase, meterialUtility.getCourse_id(), meterialUtility.getMaterial_id(), "SCROM", "100", "", "", "", filename, "", coinsCollected, redeem, meterialUtility.getModule_id(), meterialUtility.getAssign_material_id());
            if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    WebServices.updateTotalCoins(E_Learning_Activity.this, coinsCollected);

            }
        }
    }


    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }


    // Called implicitly when device is about to sleep or application is backgrounded
    protected void onPause() {
        super.onPause();
        if (webView != null)
            webView.onPause();
    }

    // Called implicitly when device is about to wake up or foregrounded
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(E_Learning_Activity.this);
        if (webView != null)
            webView.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();


    }
}
*/

package com.chaptervitamins.Materials;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import pl.droidsonroids.gif.GifTextView;

public class E_Learning_Activity extends AppCompatActivity implements View.OnClickListener,RatingListener {
    WebView webView;
    private ProgressBar progressBar3;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private Button btnPrevious, btnNext;
    String materialId;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position;
    private GifTextView goldGif;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    PowerManager.WakeLock fullWakeLock, partialWakeLock;
    private String redeem = "", noOfCoins = "";
    private DataBase dataBase;
    private MeterialUtility meterialUtility;
    private boolean isNextButtonClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_e__learning_);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
        meterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        if (meterialUtility != null) {
            meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(materialId, true);
            position = FlowingCourseUtils.getPositionOfMeterial(materialId, meterialUtilityArrayList);
        }
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(E_Learning_Activity.this,position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }

        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);
        webView = (WebView) findViewById(R.id.webView);
//        WebSettings webSettings = webView.getSettings();
//
//        webView.getSettings().setAllowFileAccess(true);
//
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        if (meterialUtility != null)
//            meterialUtility.setMaterialStartTime(df.format(c.getTime()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webView.getSettings().setAllowFileAccessFromFileURLs(true);
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }

        dataBase = DataBase.getInstance(E_Learning_Activity.this);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webSettings.setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSupportZoom(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setTextZoom(webView.getSettings().getTextZoom() + 50);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new MyWebViewClient());
        if (meterialUtility != null)
            webView.loadUrl(meterialUtility.getTincan_url());
      /*  if (isLink.equalsIgnoreCase("false")) {
            if (url == null) {
                Toast.makeText(E_Learning_Activity.this, "Can't play the package. Could not locate the file.", Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            String[] r = url.split("/");
            System.out.println("====" + r[r.length - 1]);
            String filepath = "file://" + filename + "/" + r[r.length - 1];
            File file = new File(URI.create(filepath).getPath());
            if (!file.exists()) {
                Toast.makeText(E_Learning_Activity.this, "Can't play the package. Could not locate the file.", Toast.LENGTH_LONG).show();
                finish();
            }
            System.out.println("====uri===" + filepath);
            webView.loadUrl(filepath);
        } else {
            webView.loadUrl(filename);
//            webView.loadUrl("https://www.google.co.in");
            System.out.println("====uri=Online==" + filename);
        }*/
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(E_Learning_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum()))
                    showRatingDialog();
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(E_Learning_Activity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (WebServices.isNetworkAvailable(E_Learning_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum()))
                    showRatingDialog();
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(E_Learning_Activity.this, meterialUtilityArrayList, position, true);
                break;
        }
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(E_Learning_Activity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }

    private class MyWebViewClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress >= 100) progressBar3.setVisibility(View.GONE);
//            submitDataToServer();
            super.onProgressChanged(view, newProgress);
        }

    }

    @Override
    public void onBackPressed() {
        webView.onPause();
        try {
//        partialWakeLock.acquire();
            if (WebServices.isNetworkAvailable(E_Learning_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                showRatingDialog();

            } else {
                finish();
            }
            Intent intent = new Intent();
//        intent.putExtra("filename", filename);
            setResult(99, intent);
            submitDataToServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Material_Activity.QUIZSTART_TIME="";

    }

    private void submitDataToServer() {
        if (meterialUtility == null)
            return;
        coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();

        String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility,100, redeem));
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
            showGoldGif();
            Toast.makeText(this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
        }

        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, "1", "1", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, meterialUtility, "100", "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(E_Learning_Activity.this))
            new SubmitData(E_Learning_Activity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, DataBase.getInstance(E_Learning_Activity.this)).execute();
        else {
            /*if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    WebServices.updateTotalCoins(E_Learning_Activity.this, coinsCollected);

            }*/
        }
    }


    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }


    // Called implicitly when device is about to sleep or application is backgrounded
    protected void onPause() {
        super.onPause();
        if (webView != null)
            webView.onPause();
    }

    // Called implicitly when device is about to wake up or foregrounded
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(E_Learning_Activity.this);
        if (webView != null)
            webView.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();


    }


    private void showRatingDialog() {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(E_Learning_Activity.this, meterialUtility.getMaterial_id(), true,E_Learning_Activity.this);
        custom.show(fm, "");
    }
}

