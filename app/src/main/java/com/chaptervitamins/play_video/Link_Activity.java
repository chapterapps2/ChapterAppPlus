package com.chaptervitamins.play_video;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Android on 10/4/2016.
 */
public class Link_Activity extends BaseActivity implements View.OnClickListener, RatingListener {
    private WebView webview;
    TextView title;
    private ProgressBar progressBar3;
    private String redeem = "", noOfCoins = "";
    private DataBase dataBase;
    String startTime, titleName;
    String endTime;
    MixPanelManager mixPanelManager;
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private MeterialUtility mMeterialUtility;
    String coinsCollected = "0", mGeoLocationRequestOrigin;
    private GeolocationPermissions.Callback mGeoLocationCallback;

    private ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE = 1;
    public static final int INPUT_FILE_REQUEST_CODE = 1;
    public static final String EXTRA_FROM_NOTIFICATION = "EXTRA_FROM_NOTIFICATION";

    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    String theArgumentYouWantToPass = "";
    private boolean isNextButtonClicked;
    private Uri mCapturedImageURI = null;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.link_activity);
        webview = (WebView) findViewById(R.id.webview);
        title = (TextView) findViewById(R.id.title);

        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);
        Calendar c = Calendar.getInstance();
        mixPanelManager = APIUtility.getMixPanelManager(this);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        dataBase = DataBase.getInstance(Link_Activity.this);
        progressBar3.setVisibility(View.VISIBLE);
//        if (mMeterialUtility != null)
//            mMeterialUtility.setMaterialStartTime(df.format(c.getTime()));
        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore the previous URL and history stack...........
            webview.restoreState(savedInstanceState);
        }

        if (getIntent().hasExtra("meterial_utility")) {
            mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
            theArgumentYouWantToPass = mMeterialUtility.getMaterial_media_file_url();
            titleName = mMeterialUtility.getTitle();
            if (!mMeterialUtility.isNews()) {
                meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(mMeterialUtility.getMaterial_id(), true);
                position = FlowingCourseUtils.getPositionOfMeterial(mMeterialUtility.getMaterial_id(), meterialUtilityArrayList);
                setFlowingCourse();
                getModuleData();
                setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
            } else {
                findViewById(R.id.rl_flowing_course).setVisibility(View.GONE);
                findViewById(R.id.rl_module_flowing_course).setVisibility(View.GONE);
            }
        } else {
            theArgumentYouWantToPass = getIntent().getStringExtra("url");
            titleName = (getIntent().getStringExtra("name"));
        }
        title.setText(titleName);
        boolean isImage;
        isImage = getIntent().getBooleanExtra("is_image", false);
        if (isImage)
            title.setText("Image");

        ((ImageView) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMeterialUtility != null && mMeterialUtility.isNews())
                    finish();
                else {
                    if (mMeterialUtility!=null &&!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(Link_Activity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                        showRatingDialog(true);
                    } else {
                        finish();
                    }
                }
            }
        });

//        if (theArgumentYouWantToPass.contains("watch?v="))
//            theArgumentYouWantToPass=theArgumentYouWantToPass.replace("watch?v=","embed/");
        //  String playVideo = "<html><body style=\"margin:0; padding:0px;\"><iframe  style='margin:0; padding:0px; width:100%; height:100%;' src='" + theArgumentYouWantToPass + "' frameborder='0' allowfullscreen></iframe></body></html>";
        WebSettings webSettings = webview.getSettings();

//
//        webview.getSettings().setAllowFileAccess(true);
//        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
////        webView.getSettings().setBuiltInZoomControls(false);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            webSettings.setAllowFileAccessFromFileURLs(true);
//            webSettings.setAllowUniversalAccessFromFileURLs(true);
//        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
        webSettings.setJavaScriptEnabled(true);
        if (isImage) {
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(true);
        }
        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
       /* webview.getSettings().setDisplayZoomControls(false);*/


        webview.getSettings().setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
//        webView.getSettings().setBuiltInZoomControls(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
        }
        webview.setWebChromeClient(new MyWebChromeClient());
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    MailTo mailTo = MailTo.parse(url);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mailTo.getTo()});
                    intent.putExtra(Intent.EXTRA_TEXT, mailTo.getBody());
                    intent.putExtra(Intent.EXTRA_SUBJECT, mailTo.getSubject());
                    intent.putExtra(Intent.EXTRA_CC, mailTo.getCc());
                    intent.setType("message/rfc822");
                    startActivity(intent);
                    return true;
                } else if (url.startsWith("whatsapp://") || url.startsWith("sms:")) {
                    try {
                        if (getIntent().resolveActivity(getPackageManager()) != null) {
                            view.getContext().startActivity(
                                    new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        }
                    } catch (Exception use) {
                        Log.e("", use.getMessage());
                    }
                    return true;
                } else if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL,
                            Uri.parse(url));
                    startActivity(intent);
                }else if (url.endsWith(".pdf")) {
                    view.loadUrl("https://docs.google.com/viewer?embedded=true&url=" + url);
                } else
                    view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                progressBar3.setVisibility(View.GONE);
            }
        });
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        /*webview.setWebChromeClient(new MyWebChromeClient());
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webview.setWebChromeClient(new MyWebChromeClient());
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
        webSettings.setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT); // load online by default
*/
        if (!isNetworkAvailable()) { // loading offline
            webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }

//        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=" +theArgumentYouWantToPass);
        if (mMeterialUtility != null && mMeterialUtility.isNews() && !TextUtils.isEmpty(mMeterialUtility.getMaterial_type()) && mMeterialUtility.getMaterial_type().equalsIgnoreCase("pdf"))
            theArgumentYouWantToPass = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + theArgumentYouWantToPass;
        else if (!TextUtils.isEmpty(theArgumentYouWantToPass) && theArgumentYouWantToPass.contains(getString(R.string.own_url))) {
            String ques = "?";
            if (theArgumentYouWantToPass.contains("?"))
                ques = "&";
            theArgumentYouWantToPass = theArgumentYouWantToPass + ques + "user_id=" + WebServices.mLoginUtility.getUser_id() + "&session_token=" + APIUtility.SESSION + "&organization_id=" + WebServices.mLoginUtility.getOrganization_id() + "&branch_id="
                    + WebServices.mLoginUtility.getBranch_id() + "&api_key=" + APIUtility.APIKEY;
        }
        if (theArgumentYouWantToPass.contains("watch?v="))
            webview.loadUrl(mMeterialUtility.getMaterial_media_file_url());
        else if (theArgumentYouWantToPass.endsWith(".pdf")||theArgumentYouWantToPass.endsWith(".PDF")) {
            toolbar.setVisibility(View.GONE);
            webview.getSettings().setBuiltInZoomControls(true);
            webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
            webview.loadUrl("https://docs.google.com/viewer?embedded=true&url=" + mMeterialUtility.getMaterial_media_file_url());
        }else {
            toolbar.setVisibility(View.VISIBLE);
            webview.loadUrl(theArgumentYouWantToPass);
        }



    }

    private int getScale() {
        int PIC_WIDTH= webview.getRight()-webview.getLeft();
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width)/new Double(PIC_WIDTH);
        val = val * 100d;
        return val.intValue();
    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(mMeterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, mMeterialUtility.getModule_id());
    }

    //For Android 4.1
    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
        mUploadMessage = uploadMsg;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("*/*");
        startActivityForResult(Intent.createChooser(i, "File Chooser"), FILECHOOSER_RESULTCODE);
    }

    /**
     * More info this method can be found at
     * http://developer.android.com/training/camera/photobasics.html
     *
     * @return
     * @throws IOException
     */


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }

    /**
     * Convenience method to set some generic defaults for a
     * given WebView
     *
     * @param webView
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setUpWebViewDefaults(WebView webView) {
        WebSettings settings = webView.getSettings();

        // Enable Javascript
        settings.setJavaScriptEnabled(true);

        // Use WideViewport and Zoom out if there is no viewport defined
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        // Enable pinch to zoom without the zoom buttons
        settings.setBuiltInZoomControls(true);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            // Hide the zoom controls for HONEYCOMB+
            settings.setDisplayZoomControls(false);
        }

        // Enable remote debugging via chrome://inspect
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        // We set the WebViewClient to ensure links are consumed by the WebView rather
        // than passed to a browser if it can
        webView.setWebViewClient(new WebViewClient());
    }


    private void setFlowingCourse() {
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(Link_Activity.this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }

    @Override
    protected void onStop() {

        if (mMeterialUtility != null && !mMeterialUtility.isNews()) {
            endTime = DateFormat.getDateTimeInstance().format(new Date());
            if (mixPanelManager != null)
                mixPanelManager.selectTimeTrack(Link_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                        , titleName, "Link");
            submitDataToServer();
        }
        if(webview!=null)
            webview.destroy();
        super.onStop();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(Link_Activity.this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webview.canGoBack()) {
                        webview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            if (mMeterialUtility != null && mMeterialUtility.isNews())
                finish();
            else {
                if (mMeterialUtility!=null &&!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(Link_Activity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(true);
                } else {
                    finish();
                }
            }
        }

    }

    private void submitDataToServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mMeterialUtility != null) {
                    if (mMeterialUtility.getCoinsAllocatedModel() != null) {
                        redeem = mMeterialUtility.getCoinsAllocatedModel().getRedeem();
                        noOfCoins = mMeterialUtility.getCoinsAllocatedModel().getMaxCoins();
                    }


                    if (mMeterialUtility.getCoinsAllocatedModel() != null)
                        coinsCollected = String.valueOf(mMeterialUtility.getCoinsAllocatedModel().getCoinsAccToPercentage(mMeterialUtility,100, redeem));
                    if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Link_Activity.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.you_have_been_earned), Toast.LENGTH_SHORT).show();
                            }
                        });
                    new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, "1", "1", coinsCollected);
                    new WebServices().addSubmitResponse(dataBase, mMeterialUtility, "100", "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

                    if (WebServices.isNetworkAvailable(Link_Activity.this))
                        new SubmitData(Link_Activity.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getCoinsAllocatedModel(), DataBase.getInstance(Link_Activity.this)).execute();
                    else {
                       /* if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                            coinsAllocatedModel.setRedeem("TRUE");
                            if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                                WebServices.updateTotalCoins(Link_Activity.this, coinsCollected);

                        }*/

                    }
                }
            }
        }).start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                if (mGeoLocationCallback != null) {
                    mGeoLocationCallback.invoke(mGeoLocationRequestOrigin, true, true);
                }
            } else if (mGeoLocationCallback != null) {
                mGeoLocationCallback.invoke(mGeoLocationRequestOrigin, false, false);
            }

        } else {
            /*if (requestCode == FILECHOOSER_RESULTCODE) {

                if (null == this.mUploadMessage) {
                    return;

                }

                Uri result = null;

                try {
                    if (resultCode != RESULT_OK) {

                        result = null;

                    } else {

                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "activity :" + e,
                            Toast.LENGTH_LONG).show();
                }

                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;

            } else {*/
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                if (requestCode == FILECHOOSER_RESULTCODE) {
                    if (null == mUploadMessage) return;
                    Uri result = data == null || resultCode != RESULT_OK ? null
                            : data.getData();
                    mUploadMessage.onReceiveValue(result);
                    mUploadMessage = null;
                }
            }
            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }

            Uri[] results = null;

            // Check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) {
                    // If there is not data, then we may have taken a photo
                    if (mCameraPhotoPath != null) {
                        results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                }
            }

            mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;
        }

        return;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (mMeterialUtility!=null && !TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(Link_Activity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(Link_Activity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (mMeterialUtility!=null && !TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(Link_Activity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(Link_Activity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (mMeterialUtility!=null && !TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(Link_Activity.this) &&
                        mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(Link_Activity.this, mMeterialUtility, true);
                break;
        }
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(Link_Activity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }


    private class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress >= 100) progressBar3.setVisibility(View.GONE);
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            mGeoLocationCallback = null;
            mGeoLocationRequestOrigin = null;
            askLocationPermission(callback, origin);
            super.onGeolocationPermissionsShowPrompt(origin, callback);
        }

        //For Android 4.1
//        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
//            mUploadMessage = uploadMsg;
//            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//            i.addCategory(Intent.CATEGORY_OPENABLE);
//            i.setType("*/*");
//            Link_Activity.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), Link_Activity.FILECHOOSER_RESULTCODE);
//        }

        // openFileChooser for Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {

            // Update message
            mUploadMessage = uploadMsg;

            try {

                // Create AndroidExampleFolder at sdcard

                File imageStorageDir = new File(
                        Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES)
                        , "AndroidExampleFolder");

                if (!imageStorageDir.exists()) {
                    // Create AndroidExampleFolder at sdcard
                    imageStorageDir.mkdirs();
                }

                // Create camera captured image file path and name
                File file = new File(
                        imageStorageDir + File.separator + "IMG_"
                                + String.valueOf(System.currentTimeMillis())
                                + ".jpg");

                mCapturedImageURI = Uri.fromFile(file);

                // Camera capture image intent
                final Intent captureIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");

                // Create file chooser intent
                Intent chooserIntent = Intent.createChooser(i, "Image Chooser");

                // Set camera intent to file chooser
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                        , new Parcelable[]{captureIntent});

                // On select image call onActivityResult method of activity
                startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);

            } catch (Exception e) {
                Toast.makeText(getBaseContext(), "Exception:" + e,
                        Toast.LENGTH_LONG).show();
            }

        }

        // openFileChooser for Android < 3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            openFileChooser(uploadMsg, "");
        }

        //openFileChooser for other Android versions
        public void openFileChooser(ValueCallback<Uri> uploadMsg,
                                    String acceptType,
                                    String capture) {

            openFileChooser(uploadMsg, acceptType);
        }

        public boolean onShowFileChooser(
                WebView webView, ValueCallback<Uri[]> filePathCallback,
                FileChooserParams fileChooserParams) {
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(null);
            }
            mFilePathCallback = filePathCallback;

            askCameraGalleryPermission();

            return true;
        }

    }


    public void askLocationPermission(GeolocationPermissions.Callback callback, String origin) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        AppConstants.LOCATION_PERM);

            } else {
                mGeoLocationCallback = callback;
                mGeoLocationRequestOrigin = origin;
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        AppConstants.LOCATION_PERM);
            }
        } else {
            if (!Utils.isLocationEnabled(Link_Activity.this))
                DialogUtils.showDialog(this, "Please enable your location", new Runnable() {
                    @Override
                    public void run() {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(myIntent, 101);
                    }
                }, null);
            else {
                callback.invoke(origin, true, true);
            }

        }
    }

    public void askCameraGalleryPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        AppConstants.CAMERA);

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        AppConstants.CAMERA);
            }
        } else {
            if (Constants.SHOW_GALLERY)
                showFileChooser();
            else
                openCameraThroughIntent();
        }
    }

    private void openCameraThroughIntent() {
        File photoFile = null;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            photoFile = createImageFile();
            mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
            intent.putExtra("PhotoPath", mCameraPhotoPath);
        } catch (IOException ex) {
            // Error occurred while creating the File
            Log.e("vijay", "Unable to create Image File", ex);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photoFile));
        startActivityForResult(intent, INPUT_FILE_REQUEST_CODE);
    }


    public void showFileChooser() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(Link_Activity.this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e("vijay", "Unable to create Image File", ex);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
            } else {
                takePictureIntent = null;
            }
        }
        Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
        Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
        contentSelectionIntent.setType("image/*");
        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);

        Intent[] intentArray;
        if (takePictureIntent != null) {
            intentArray = new Intent[]{takePictureIntent};
        } else {
            intentArray = new Intent[0];
        }


        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

        startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);

        if (theArgumentYouWantToPass.startsWith("mailto:")) {
            Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:someone@example.com"));
            i.putExtra(android.content.Intent.EXTRA_TEXT, URLDecoder.decode(theArgumentYouWantToPass));
            startActivity(i);
        }
    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(Link_Activity.this, mMeterialUtility.getMaterial_id(), wannaFinish, Link_Activity.this);
        custom.show(fm, "");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.LOCATION_PERM:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!Utils.isLocationEnabled(Link_Activity.this))
                        DialogUtils.showDialog(this, "Please enable your location", new Runnable() {
                            @Override
                            public void run() {
                                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(myIntent, 101);
                            }
                        }, null);
                    else {
                        if (mGeoLocationCallback != null) {
                            mGeoLocationCallback.invoke(mGeoLocationRequestOrigin, true, true);
                        }
                    }

                } else {
                    if (mGeoLocationCallback != null) {
                        mGeoLocationCallback.invoke(mGeoLocationRequestOrigin, false, false);
                    }
                    Toast.makeText(Link_Activity.this, "You cannot access this without allow your location permission!", Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Constants.SHOW_GALLERY)
                        showFileChooser();
                    else
                        openCameraThroughIntent();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
