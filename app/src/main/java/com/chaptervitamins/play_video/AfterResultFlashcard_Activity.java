package com.chaptervitamins.play_video;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.utility.FlashCardUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class AfterResultFlashcard_Activity extends BaseActivity {
    @BindView(R.id.img_view)
    ImageView img_view;
    @BindView(R.id.total_txt)
    TextView total_txt;
    @BindView(R.id.title_txt)
    TextView title_txt;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.question_txt)
    TextView question_txt;
    @BindView(R.id.revealanswer_btn)
    Button revealanswer_btn;
    @BindView(R.id.know_ll)
    LinearLayout know_ll;
    @BindView(R.id.footer)
    RelativeLayout footer;
    @BindView(R.id.remember_ll)
    LinearLayout remember_ll;
    @BindView(R.id.radioButton)
    RadioButton radioButton;
    @BindView(R.id.radioButton2)
    RadioButton radioButton2;
    @BindView(R.id.done_btn)
    Button done_btn;
    WebServices webServices;
    private ProgressDialog dialog;
    private DataBase dataBase;
    private int total = 1, seenCount;
    private boolean isoffline = false;
    private ArrayList<FlashCardUtility> list = new ArrayList<>();
    static String ImgString = "";
    boolean isQuesClick = true;
    private Bitmap imageBitmap;
    CustomPagerAdapter pagerAdapter;
    private ImageView prev_img, next_img;
    private MeterialUtility meterialUtility;
    AnimatorSet mSetRightOut, mSetLeftIn;
    private String pos = "", flashPosition = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.flashcard_activity);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ButterKnife.bind(this);
        prev_img = (ImageView) findViewById(R.id.prev_img);
        next_img = (ImageView) findViewById(R.id.next_img);
        prev_img.setVisibility(View.GONE);
        webServices = new WebServices();
        footer.setVisibility(View.VISIBLE);
        meterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        seenCount = getIntent().getIntExtra("seen_count", 1);
        total = getIntent().getIntExtra("total", 1);
        flashPosition = getIntent().getStringExtra("position");
        dataBase = DataBase.getInstance(AfterResultFlashcard_Activity.this);
        list = new ArrayList<>();
        if (getIntent().getStringExtra("position").equalsIgnoreCase("all"))
            list = FlashCardResult.flashCardUtilities;
        else {
            pos = getIntent().getStringExtra("position");
            if (Integer.parseInt(pos) < WebServices.flashcardMeterialUtilities.getFlashCardUtilities().size())
                list.add(WebServices.flashcardMeterialUtilities.getFlashCardUtilities().get(Integer.parseInt(pos)));
        }
        title_txt.setText(WebServices.flashcardMeterialUtilities.getTitle());

        if (list.size() == 0) {
            total_txt.setText("");
            done_btn.setVisibility(View.GONE);
            noFlashCardData();
        } else {
            pagerAdapter = new CustomPagerAdapter(AfterResultFlashcard_Activity.this);
            viewpager.setAdapter(pagerAdapter);
            viewpager.setCurrentItem(0);
            if (0 == list.size() - 1) {
                done_btn.setVisibility(View.VISIBLE);
            } else {
                done_btn.setVisibility(View.GONE);
            }
            total_txt.setText((Integer.parseInt(pos) + 1) + "/" + total + "");
        }
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == list.size() - 1) {
                    next_img.setVisibility(View.GONE);
                    done_btn.setVisibility(View.VISIBLE);
                } else {
                    done_btn.setVisibility(View.GONE);
                    next_img.setVisibility(View.VISIBLE);
                }
                if (viewpager.getCurrentItem() == 0) prev_img.setVisibility(View.GONE);
                else prev_img.setVisibility(View.VISIBLE);
                total_txt.setText((Integer.parseInt(pos) + 1) + "/" + total + "");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        next_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < list.size()) {
                    // move to next screen
                    viewpager.setCurrentItem(current);
                }
            }
        });
        prev_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(-1);
                if (current < list.size()) {
                    // move to next screen
                    viewpager.setCurrentItem(current);
                }
            }
        });
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FlashCardUtility> templist = WebServices.flashcardMeterialUtilities.getFlashCardUtilities();
                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j < templist.size(); j++) {
                        if (list.get(i).getFlash_card_id().equalsIgnoreCase(templist.get(j).getFlash_card_id())) {
                            templist.get(j).setUserAns(list.get(i).getUserAns());
                            break;
                        }
                    }
                }
                WebServices.flashcardMeterialUtilities.setFlashCardUtilities(templist);
                Intent intent = new Intent(AfterResultFlashcard_Activity.this, FlashCardResult.class);
                intent.putExtra("meterial_utility", meterialUtility);
                intent.putExtra("seen_count", seenCount);
                intent.putExtra("name", title_txt.getText().toString());
                intent.putExtra("time", WebServices.flashcardMeterialUtilities.getAlloted_time());
                startActivity(intent);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FlashCardUtility> templist = WebServices.flashcardMeterialUtilities.getFlashCardUtilities();
                for (int i = 0; i < list.size(); i++) {
                    for (int j = 0; j < templist.size(); j++) {
                        if (list.get(i).getFlash_card_id().equalsIgnoreCase(templist.get(j).getFlash_card_id())) {
                            templist.get(j).setUserAns(list.get(i).getUserAns());
                            break;
                        }
                    }
                }
                WebServices.flashcardMeterialUtilities.setFlashCardUtilities(templist);
                Intent intent = new Intent(AfterResultFlashcard_Activity.this, FlashCardResult.class);
                intent.putExtra("meterial_utility", meterialUtility);
                intent.putExtra("name", title_txt.getText().toString());
                intent.putExtra("seen_count", seenCount);
                intent.putExtra("time", WebServices.flashcardMeterialUtilities.getAlloted_time());
                startActivity(intent);
                finish();
            }
        });

//        revealanswer_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                revealanswer_btn.setVisibility(View.GONE);
//                know_ll.setVisibility(View.VISIBLE);
//                remember_ll.setVisibility(View.VISIBLE);
//                showAnswer(selectedPostion);
//            }
//        });
//        know_ll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                revealanswer_btn.setVisibility(View.GONE);
//                radioButton2.setChecked(false);
//                radioButton.setChecked(true);
//                list.get(selectedPostion).setUserAns("1");
//            }
//        });
//        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    radioButton2.setChecked(false);
//                    radioButton.setChecked(true);
//                    list.get(selectedPostion).setUserAns("1");
//                    final Handler handler=new Handler(){
//                        @Override
//                        public void handleMessage(Message msg) {
//                            showdata();
//                        }
//                    };
//                    new Thread(){
//                        @Override
//                        public void run() {
//                            try {
//                                sleep(500);
//                            } catch (InterruptedException e) {
//                            }finally {
//                                handler.sendEmptyMessage(0);
//                            }
//                        }
//                    }.start();
//                }
//            }
//        });
//        radioButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    radioButton2.setChecked(true);
//                    radioButton.setChecked(false);
//                    list.get(selectedPostion).setUserAns("2");
//                    final Handler handler=new Handler(){
//                        @Override
//                        public void handleMessage(Message msg) {
//                            showdata();
//                        }
//                    };
//                    new Thread(){
//                        @Override
//                        public void run() {
//                            try {
//                                sleep(500);
//                            } catch (InterruptedException e) {
//                            }finally {
//                                handler.sendEmptyMessage(0);
//                            }
//                        }
//                    }.start();
//                }
//            }
//        });
//        remember_ll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                revealanswer_btn.setVisibility(View.GONE);
//                radioButton2.setChecked(true);
//                radioButton.setChecked(false);
//                list.get(selectedPostion).setUserAns("2");
//            }
//        });
//        next_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                list.get(selectedPostion).setUserAns("");
//                showdata();
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(AfterResultFlashcard_Activity.this);
    }

    private void noFlashCardData() {
        new AlertDialog.Builder(AfterResultFlashcard_Activity.this)
                .setTitle("No FlashCard")
                .setMessage("Not Available FlashCard for you.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    private int getItem(int i) {
        return viewpager.getCurrentItem() + i;
    }

    /*private void show(final CustomPagerAdapter.MyHolder holder, final int pos) {

        if (WebServices.flashcardMeterialUtilities.getMaterial_templete() != null) {
            switch (WebServices.flashcardMeterialUtilities.getMaterial_templete()) {
                case "TEMPLATE-1":
                    showTemplate1(holder, pos);
                    break;
                default:

                    showDefaultTemplate(holder, pos);
                    break;
            }

        }


    }*/

    private void showTemplate1(final CustomPagerAdapter.MyHolder holder, final int pos) {
        holder.know_ll.setVisibility(View.GONE);
        holder.ll_template_1.setVisibility(View.VISIBLE);
        holder.ll_template_2.setVisibility(GONE);
        holder.remember_ll.setVisibility(View.GONE);
        holder.radioButton2.setChecked(false);
        holder.radioButton.setChecked(false);
        isQuesClick = true;
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getUserAns().equalsIgnoreCase("1")) holder.radioButton.setChecked(true);
        else if (list.get(pos).getUserAns().equalsIgnoreCase("2"))
            holder.radioButton2.setChecked(true);

        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.question_txt.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.question_txt.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.question_txt.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.VISIBLE);
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.img_view.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
        showAnswer(holder, pos);
    }

    private void showAnswer(final CustomPagerAdapter.MyHolder holder, final int pos) {
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.answer_txt.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.answer_txt.setText(Html.fromHtml(list.get(pos).getAnswer()));
        } else {
            holder.answer_txt.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.VISIBLE);
            holder.progress_bar.setVisibility(View.VISIBLE);
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url());
                if (imageBitmap != null) {
                    holder.img_view.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getAnswer(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getAnswer(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getAnswer_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }

    }

   /* public class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;

        public CustomPagerAdapter(Context context) {
            mContext = context;
        }

        class MyHolder {
            TextView question_txt, answer_txt,tvQuesNo,tvQuestion,tvAns;
            LinearLayout know_ll, remember_ll, rlAns,ll_template_1, ll_template_2;
            RadioButton radioButton, radioButton2;
            ImageView img_view,ivQues,ivAns;
            ProgressBar progress_bar;
            ScrollView scrollview;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            final CustomPagerAdapter.MyHolder holder = new CustomPagerAdapter.MyHolder();
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.flashcard_pager_row, collection,
                    false);
            holder.question_txt = (TextView) layout.findViewById(R.id.question_txt);
            holder.rlAns = (LinearLayout) layout.findViewById(R.id.ans_ll);
            holder.answer_txt = (TextView) layout.findViewById(R.id.answer_txt);
            holder.know_ll = (LinearLayout) layout.findViewById(R.id.know_ll);
            holder.remember_ll = (LinearLayout) layout.findViewById(R.id.remember_ll);
            holder.radioButton = (RadioButton) layout.findViewById(R.id.radioButton);
            holder.radioButton2 = (RadioButton) layout.findViewById(R.id.radioButton2);
            holder.img_view = (ImageView) layout.findViewById(R.id.img_view);
            holder.progress_bar = (ProgressBar) layout.findViewById(R.id.progress_bar);
            holder.scrollview = (ScrollView) layout.findViewById(R.id.scrollview);
            holder.ll_template_1 = (LinearLayout) layout.findViewById(R.id.ll_template_1);
            holder.ll_template_2 = (LinearLayout) layout.findViewById(R.id.ll_template_2);
            holder.tvQuesNo = (TextView) layout.findViewById(R.id.tv_ques_no);
            holder.ivQues = (ImageView) layout.findViewById(R.id.iv_ques);
            holder.ivAns = (ImageView) layout.findViewById(R.id.iv_ans);
            holder.tvQuestion = (TextView) layout.findViewById(R.id.tv_question);
            holder.tvAns = (TextView) layout.findViewById(R.id.tv_ans);
            layout.findViewById(R.id.revealanswer_btn).setVisibility(View.GONE);
            show(holder, position);
            holder.rlAns.setVisibility(View.VISIBLE);
            String template = WebServices.flashcardMeterialUtilities.getMaterial_templete();
            if (template != null) {
                switch (template) {
                    case "TEMPLATE-1":
                        holder.rlAns.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
                        break;
                   *//* case "TEMPLATE-2":
                        holder.rlAns.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.hyperspace_out));
                        break;
                    case "TEMPLATE-3":
                        holder.rlAns.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.anim_fade_out));
                        break;*//*
                   default:
                       holder.rlAns.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));

//                       showDefaultTemplate(holder, position);
                }
            }
            holder.know_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!radioButton.isChecked()) {
                        holder.radioButton2.setChecked(false);
                        holder.radioButton.setChecked(true);
                        list.get(position).setUserAns("1");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });

            holder.remember_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!radioButton2.isChecked()) {
                        holder.radioButton2.setChecked(true);
                        holder.radioButton.setChecked(false);
                        list.get(position).setUserAns("2");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });

            holder.img_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AfterResultFlashcard_Activity.this, FullScreenImageActivity.class);
                    if (isQuesClick)
                        intent.putExtra("imgurl", list.get(position).getQuestion_description_image_url());
                    else intent.putExtra("imgurl", list.get(position).getAnswer_image_url());
                    if (isQuesClick)
                        ImgString = list.get(position).getQuestion_description();
                    else ImgString = list.get(position).getAnswer();
                    startActivity(intent);
                }
            });
            ((ViewPager) collection).addView(layout);
            return layout;
        }



        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }


    }*/

    private void show(final CustomPagerAdapter.MyHolder holder, final int pos) {
        /*holder.know_ll.setVisibility(View.VISIBLE);
        holder.remember_ll.setVisibility(View.VISIBLE);*/

        if (WebServices.flashcardMeterialUtilities.getMaterial_templete() != null) {
            switch (WebServices.flashcardMeterialUtilities.getMaterial_templete()) {
                case "TEMPLATE-1":
                    showTemplate1(holder, pos);
                    break;
                case "TEMPLATE-4":
                    showTemplate4(holder, pos);
                    break;
                case "TEMPLATE-3":
                    showFlipCard(holder, pos);
                    break;
                default:
                   /* if (pos == list.size() - 1)
                        footer.setVisibility(View.VISIBLE);
                    else
                        footer.setVisibility(GONE);*/
                    showDefaultTemplate(holder, pos);
                    break;
            }
            Toast.makeText(this, "Template Id" + WebServices.flashcardMeterialUtilities.getMaterial_templete(), Toast.LENGTH_SHORT).show();
        }
    }

    private void showTemplate4(final CustomPagerAdapter.MyHolder holder, final int pos) {
        holder.ll_template_2.setVisibility(GONE);
        holder.ll_template_1.setVisibility(View.VISIBLE);
        holder.revealanswer_btn.setVisibility(GONE);
//        footer.setVisibility(GONE);
//        holder.scrollview.setVisibility(View.VISIBLE);

        holder.radioButton2.setChecked(false);
        holder.radioButton.setChecked(false);
        holder.main.setBackgroundResource(R.drawable.rect_roud_cornor_shape);

        holder.tvFlashcardCount.setText("Page " + flashPosition + "/" + total + "  ");
        holder.tvFlashcardCount.setTextSize(19.0f);
        holder.question_txt.setTextSize(19.0f);
        holder.answer_txt.setTextSize(17.5f);
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getUserAns().equalsIgnoreCase("1"))
            holder.radioButton.setChecked(true);
        else if (list.get(pos).getUserAns().equalsIgnoreCase("2"))
            holder.radioButton2.setChecked(true);

        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.question_txt.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.question_txt.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.question_txt.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.VISIBLE);
            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.img_view.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
        showAnswer(holder, pos);

        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.revealanswer_btn.setVisibility(GONE);
            holder.ans_ll.setVisibility(View.VISIBLE);
            holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
            /*holder.know_ll.setVisibility(View.VISIBLE);
            holder.remember_ll.setVisibility(View.VISIBLE);*/
        } else {
            holder.revealanswer_btn.setVisibility(GONE);
            holder.img_view1.setVisibility(View.VISIBLE);
            holder.img_view1.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
            holder.know_ll.setVisibility(View.VISIBLE);
            holder.remember_ll.setVisibility(View.VISIBLE);
        }
    }

    private void showDefaultTemplate(final FlashCard_Activity.CustomPagerAdapter.MyHolder holder, final int pos) {
//        holder.scrollview.setVisibility(GONE);
        holder.ll_template_2.setVisibility(View.VISIBLE);
        holder.ll_template_1.setVisibility(GONE);
        holder.tvQuesNo.setText(flashPosition);
//        footer.setVisibility(GONE);
        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.tvQuestion.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.tvQuestion.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.tvQuestion.setVisibility(View.GONE);
            holder.ivQues.setVisibility(View.VISIBLE);
//            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.ivQues.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.ivQues.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.ivQues.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.ivQues.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }


        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.tvAns.setVisibility(View.VISIBLE);
            holder.ivAns.setVisibility(View.GONE);
            holder.tvAns.setText(Html.fromHtml(list.get(pos).getAnswer()));
        } else {
//            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.tvAns.setVisibility(View.GONE);
            holder.ivAns.setVisibility(View.VISIBLE);
//            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url());
                if (imageBitmap != null) {
                    holder.ivAns.setImageBitmap(imageBitmap);
//                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getAnswer_image_url(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.ivAns.setImageBitmap(decodedByte);
//                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.ivAns.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getAnswer_image_url(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.ivAns.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getAnswer_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
    }

    private void showTemplate1(final FlashCard_Activity.CustomPagerAdapter.MyHolder holder, final int pos) {

        holder.ll_template_2.setVisibility(GONE);
        holder.ll_template_1.setVisibility(View.VISIBLE);
//        holder.scrollview.setVisibility(View.VISIBLE);
//        footer.setVisibility(View.VISIBLE);
        holder.radioButton2.setChecked(false);
        holder.radioButton.setChecked(false);

        holder.tvFlashcardCount.setText("Flashcard " + flashPosition + "/" + list.size() + "   ");
        holder.tvFlashcardCount.setTextSize(18.0f);
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getUserAns().equalsIgnoreCase("1"))
            holder.radioButton.setChecked(true);
        else if (list.get(pos).getUserAns().equalsIgnoreCase("2"))
            holder.radioButton2.setChecked(true);

        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.question_txt.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.question_txt.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.question_txt.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.VISIBLE);
            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.img_view.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
        showAnswer(holder, pos);
    }

    private void showAnswer(final FlashCard_Activity.CustomPagerAdapter.MyHolder holder, final int pos) {
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            // holder.answer_txt.setVisibility(View.VISIBLE);
            holder.img_view1.setVisibility(View.GONE);
            holder.answer_txt.setText(Html.fromHtml(list.get(pos).getAnswer()));
            holder.ans_ll.setVisibility(View.GONE);
            holder.know_ll.setVisibility(View.GONE);
        } else {
            // holder.answer_txt.setVisibility(View.GONE);
//            holder.img_view1.setVisibility(View.VISIBLE);
            holder.img_view1.setVisibility(View.GONE);
            holder.know_ll.setVisibility(View.GONE);
            holder.remember_ll.setVisibility(View.GONE);
//            holder.progress_bar1.setVisibility(View.VISIBLE);
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url());
                if (imageBitmap != null) {
                    holder.img_view1.setImageBitmap(imageBitmap);
                    holder.progress_bar1.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getAnswer(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view1.setImageBitmap(decodedByte);
                holder.progress_bar1.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar1.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view1.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getAnswer(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view1.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getAnswer_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }

    }

    public class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;

        public CustomPagerAdapter(Context context) {
            mContext = context;
        }

        class MyHolder {
            TextView question_txt, answer_txt, tvFlashcardCount, flipCardFrontText, flipCardBackText, flipCardFrontPos;
            LinearLayout know_ll, remember_ll;
            RadioButton radioButton, radioButton2;
            FrameLayout flipCardParent;
            ImageView img_view, img_view1, ivAns, ivQues, ivAnswer, ivQuestion;
            ProgressBar progress_bar, progress_bar1;
            LinearLayout ans_ll, ll_template_1, ll_template_2, ll_template_6;
            Button revealanswer_btn;
            RelativeLayout main;
            RelativeLayout footer;
            TextView tvQuestion, tvAns, tvQuesNo, tvTitle, tvQuestionNo, tvQuestion6, tvAnswer;
            ScrollView scrollview;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            final MyHolder holder = new MyHolder();
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.flashcard_pager_row, collection,
                    false);

            /**/
            holder.flipCardParent = (FrameLayout) layout.findViewById(R.id.flip_card_template);

            holder.flipCardBackText = (TextView) holder.flipCardParent.findViewById(R.id.text_back);
            holder.flipCardFrontText = (TextView) holder.flipCardParent.findViewById(R.id.text_front);
            holder.flipCardFrontPos = (TextView) holder.flipCardParent.findViewById(R.id.text_front_position);
            /**/
            holder.question_txt = (TextView) layout.findViewById(R.id.question_txt);
            holder.answer_txt = (TextView) layout.findViewById(R.id.answer_txt);
            holder.tvQuestion = (TextView) layout.findViewById(R.id.tv_question);
            holder.tvTitle = (TextView) layout.findViewById(R.id.tv_title);
            holder.tvQuestionNo = (TextView) layout.findViewById(R.id.tv_question_no);
            holder.tvQuestion6 = (TextView) layout.findViewById(R.id.tv_question_6);
            holder.tvAnswer = (TextView) layout.findViewById(R.id.tv_answer);
            holder.tvAns = (TextView) layout.findViewById(R.id.tv_ans);
            holder.scrollview = (ScrollView) layout.findViewById(R.id.scrollview);
            holder.tvQuesNo = (TextView) layout.findViewById(R.id.tv_ques_no_temp2);
            holder.tvFlashcardCount = (TextView) layout.findViewById(R.id.tv_no_of_flashcard);
            holder.know_ll = (LinearLayout) layout.findViewById(R.id.know_ll);
            holder.ans_ll = (LinearLayout) layout.findViewById(R.id.ans_ll);
            holder.revealanswer_btn = (Button) layout.findViewById(R.id.revealanswer_btn);
            holder.remember_ll = (LinearLayout) layout.findViewById(R.id.remember_ll);
            holder.main = (RelativeLayout) layout.findViewById(R.id.main);
            holder.footer = (RelativeLayout) layout.findViewById(R.id.footer);
            holder.radioButton = (RadioButton) layout.findViewById(R.id.radioButton);
            holder.radioButton2 = (RadioButton) layout.findViewById(R.id.radioButton2);
            holder.img_view = (ImageView) layout.findViewById(R.id.img_view);
            holder.ivQues = (ImageView) layout.findViewById(R.id.iv_ques_temp2);
            holder.ivQuestion = (ImageView) layout.findViewById(R.id.iv_question);
            holder.ivAns = (ImageView) layout.findViewById(R.id.iv_ans);
            holder.ivAnswer = (ImageView) layout.findViewById(R.id.iv_answer);
            holder.progress_bar = (ProgressBar) layout.findViewById(R.id.progress_bar);
            holder.img_view1 = (ImageView) layout.findViewById(R.id.img_view1);
            holder.progress_bar1 = (ProgressBar) layout.findViewById(R.id.progress_bar1);
            holder.ll_template_1 = (LinearLayout) layout.findViewById(R.id.ll_template_1);
            holder.ll_template_2 = (LinearLayout) layout.findViewById(R.id.ll_template_2);
            holder.ll_template_6 = (LinearLayout) layout.findViewById(R.id.ll_template_6);

            holder.ans_ll.setVisibility(View.GONE);
            holder.know_ll.setVisibility(View.GONE);
            holder.remember_ll.setVisibility(View.GONE);

            if (WebServices.flashcardMeterialUtilities.getMaterial_templete().equalsIgnoreCase("TEMPLATE-6")) {
                holder.ll_template_1.setVisibility(View.GONE);
                holder.ll_template_2.setVisibility(View.GONE);
                holder.ll_template_6.setVisibility(View.VISIBLE);
//                footer.setVisibility(GONE);
                showNewFlashcard(holder, position);
            } else {

//                footer.setVisibility(View.VISIBLE);
                holder.ll_template_6.setVisibility(View.GONE);
                show(holder, position);
            }

            if (WebServices.flashcardMeterialUtilities.getMaterial_templete() != null && WebServices.flashcardMeterialUtilities.getMaterial_templete().equalsIgnoreCase("TEMPLATE-5")) {
                holder.revealanswer_btn.setText("Tap to continue");
            }
            holder.revealanswer_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (WebServices.flashcardMeterialUtilities.getMaterial_templete() != null) {
                        switch (WebServices.flashcardMeterialUtilities.getMaterial_templete()) {
                            case "TEMPLATE-1":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }

                                break;
                            /*case "TEMPLATE-2":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.hyperspace_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.hyperspace_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;
                            case "TEMPLATE-3":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.anim_fade_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.anim_fade_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;
                            case "TEMPLATE-5":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;*/
                            case "TEMPLATE-4":
                                /*if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }*/
                                holder.ll_template_2.setVisibility(View.GONE);
                                holder.ans_ll.setVisibility(View.VISIBLE);
                                break;
                            case "TEMPLATE-6":
                                break;

                            default:
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;

                        }
                    } else {
                        if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                            holder.revealanswer_btn.setVisibility(GONE);
                            holder.ans_ll.setVisibility(View.VISIBLE);
                            holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
                            holder.know_ll.setVisibility(View.VISIBLE);
                            holder.remember_ll.setVisibility(View.VISIBLE);
                        } else {
                            holder.revealanswer_btn.setVisibility(GONE);
                            holder.img_view1.setVisibility(View.VISIBLE);
                            holder.img_view1.startAnimation(AnimationUtils.loadAnimation(AfterResultFlashcard_Activity.this, R.anim.slide_in_up));
                            holder.know_ll.setVisibility(View.VISIBLE);
                            holder.remember_ll.setVisibility(View.VISIBLE);
                        }
                    }

                }
            });

            //holder.answer_txt.setVisibility(holder.answer_txt.VISIBLE);

            holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        holder.radioButton2.setChecked(false);
                        holder.radioButton.setChecked(true);
                        list.get(position).setUserAns("1");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });
            holder.radioButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        holder.radioButton2.setChecked(true);
                        holder.radioButton.setChecked(false);
                        list.get(position).setUserAns("2");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });
            holder.img_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AfterResultFlashcard_Activity.this, FullScreenImageActivity.class);
                    if (isQuesClick)
                        intent.putExtra("imgurl", list.get(position).getQuestion_description_image_url());
                    else intent.putExtra("imgurl", list.get(position).getAnswer_image_url());
                    if (isQuesClick)
                        ImgString = list.get(position).getQuestion_description();
                    else ImgString = list.get(position).getAnswer();
                    startActivity(intent);
                }
            });
            holder.img_view1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AfterResultFlashcard_Activity.this, FullScreenImageActivity.class);
//                        intent.putExtra("imgurl", list.get(position).getQuestion_description_image_url());
                    intent.putExtra("imgurl", list.get(position).getAnswer_image_url());

                    ImgString = list.get(position).getAnswer();
                    startActivity(intent);
                }
            });
            ((ViewPager) collection).addView(layout);
            return layout;
        }

        private void showNewFlashcard(MyHolder holder, int position) {
            if (holder != null && position != -1) {
                holder.tvTitle.setText(WebServices.flashcardMeterialUtilities.getTitle());
                holder.tvQuestionNo.setText(flashPosition);
                if (list.get(position).getIs_question_image().equalsIgnoreCase("0")) {
                    holder.ivQuestion.setVisibility(View.GONE);
                    holder.tvQuestion6.setVisibility(View.VISIBLE);
                    holder.tvQuestion6.setText(Html.fromHtml(list.get(position).getQuestion_description()));
                } else {
                    holder.ivQuestion.setVisibility(View.VISIBLE);
                    holder.tvQuestion.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(list.get(position).getQuestion_description_image_url()))
                        Picasso.with(AfterResultFlashcard_Activity.this).load(list.get(position).getQuestion_description_image_url()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.ivQuestion);
                    else
                        Picasso.with(AfterResultFlashcard_Activity.this).load(R.mipmap.ic_launcher).into(holder.ivQuestion);
                }

                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                    holder.ivAnswer.setVisibility(View.GONE);
                    holder.tvAnswer.setVisibility(View.VISIBLE);
                    holder.tvAnswer.setText(Html.fromHtml(list.get(position).getAnswer()));
                } else {
                    holder.ivAnswer.setVisibility(View.VISIBLE);
                    holder.tvAnswer.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(list.get(position).getAnswer_image_url()))
                        Picasso.with(AfterResultFlashcard_Activity.this).load(list.get(position).getAnswer_image_url()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.ivAnswer);
                    else
                        Picasso.with(AfterResultFlashcard_Activity.this).load(R.mipmap.ic_launcher).into(holder.ivAnswer);
                }
            }
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }


    }

    public void showFlipCard(final AfterResultFlashcard_Activity.CustomPagerAdapter.MyHolder holder, final int pos) {
        final boolean[] isBackVisible = {false};
        final boolean[] animationRunning = {false};
        footer.setVisibility(View.VISIBLE);

        holder.flipCardParent.setVisibility(View.VISIBLE);
        holder.flipCardFrontPos.setVisibility(GONE);
        holder.ll_template_6.setVisibility(GONE);
        holder.ll_template_2.setVisibility(GONE);
        holder.ll_template_1.setVisibility(View.GONE);
        holder.revealanswer_btn.setVisibility(GONE);
        viewpager.setClipToPadding(false);
        viewpager.setPageMargin(15);
//        holder.scrollview.setVisibility(View.VISIBLE);
        loadAnimations();
        changeCameraDistance((FrameLayout) holder.flipCardParent.findViewById(R.id.card_front),
                (FrameLayout) holder.flipCardParent.findViewById(R.id.card_back));
        holder.tvFlashcardCount.setText("Page " + (pos + 1) + "/" + list.size() + "  ");

        holder.tvFlashcardCount.setTextSize(19.0f);
        holder.flipCardFrontText.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        holder.flipCardBackText.setText(Html.fromHtml(list.get(pos).getAnswer()));
        holder.flipCardFrontPos.setText((pos + 1) + "/" + list.size());

        holder.flipCardParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isBackVisible[0]) {
                    mSetRightOut.setTarget(view.findViewById(R.id.card_front));
                    mSetLeftIn.setTarget(view.findViewById(R.id.card_back));

                    mSetRightOut.start();
                    mSetRightOut.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            animationRunning[0] = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });
                    mSetLeftIn.start();
                    mSetLeftIn.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            animationRunning[0] = false;
                            isBackVisible[0] = true;
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

                } else {
                    mSetRightOut.setTarget(view.findViewById(R.id.card_back));
                    mSetLeftIn.setTarget(view.findViewById(R.id.card_front));
                    mSetRightOut.start();
                    mSetRightOut.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            animationRunning[0] = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });
                    mSetLeftIn.start();
                    mSetLeftIn.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            animationRunning[0] = false;
                            isBackVisible[0] = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

                }

            }
        });

    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(AfterResultFlashcard_Activity.this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(AfterResultFlashcard_Activity.this, R.animator.in_animation);
    }

    private void changeCameraDistance(FrameLayout viewById, FrameLayout viewById1) {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        viewById.setCameraDistance(scale);
        viewById1.setCameraDistance(scale);
    }

    private void showDefaultTemplate(final AfterResultFlashcard_Activity.CustomPagerAdapter.MyHolder holder, final int pos) {
//        holder.scrollview.setVisibility(GONE);
        holder.ll_template_2.setVisibility(View.VISIBLE);
        holder.tvQuesNo.setText(flashPosition);

        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.tvQuestion.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.tvQuestion.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.tvQuestion.setVisibility(View.GONE);
            holder.ivQues.setVisibility(View.VISIBLE);
//            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.ivQues.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.ivQues.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.ivQues.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.ivQues.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }


        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.tvAns.setVisibility(View.VISIBLE);
            holder.ivAns.setVisibility(View.GONE);
            holder.tvAns.setText(Html.fromHtml(list.get(pos).getAnswer()));
        } else {
//            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.tvAns.setVisibility(View.GONE);
            holder.ivAns.setVisibility(View.VISIBLE);
//            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(AfterResultFlashcard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url());
                if (imageBitmap != null) {
                    holder.ivAns.setImageBitmap(imageBitmap);
//                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getAnswer_image_url(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.ivAns.setImageBitmap(decodedByte);
//                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.ivAns.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getAnswer_image_url(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.ivAns.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getAnswer_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
    }
}
