package com.chaptervitamins.play_video;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.MeterialUtility;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Android on 9/1/2016.
 */

public class FullScreenImageActivity extends BaseActivity {
    ImageView back;
    TouchImageView imageView;
    Bitmap imageBitmap;
    private TextView tvTitle;
    ProgressBar progressBar;
    private MeterialUtility mMeterialUtility;
    private CoinsAllocatedModel coinsAllocatedModel;
    private String redeem = "", noOfCoins = "";
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fullscreenimage);
        back = (ImageView) findViewById(R.id.back);
        imageView = (TouchImageView) findViewById(R.id.imageView);
        tvTitle = (TextView) findViewById(R.id.toolbar_title);
        tvTitle.setText(getIntent().getStringExtra("title"));
        if (getIntent().hasExtra("meterial_utility")) {
            mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(mMeterialUtility.getCourse_id(),mMeterialUtility.getModule_id(), mMeterialUtility.getMaterial_id());
//                    if (coinsAllocatedModel == null) {
//                        coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(mMeterialUtility.getMaterial_id());
//                    }
//                    if (coinsAllocatedModel != null) {
//                        redeem = coinsAllocatedModel.getRedeem();
//                        noOfCoins = coinsAllocatedModel.getMaxCoins();
//                    } else
//                        coinsAllocatedModel = new CoinsAllocatedModel();
//
//
//                }
//            }).start();
//            meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(mMeterialUtility.getCourse_id(), mMeterialUtility.getModule_id());
//            position = FlowingCourseUtils.getPositionOfMeterial(getIntent().getStringExtra("id"), meterialUtilityArrayList);
        }
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMeterialUtility != null) {
                    DataBase dataBase = DataBase.getInstance(FullScreenImageActivity.this);
                    new WebServices().setProgressOfMaterial(dataBase,mMeterialUtility,"1", "1","0");
                    new WebServices().addSubmitResponse(dataBase, mMeterialUtility, "0", "", "", "", null, redeem,WebServices.mLoginUtility.getOrganization_id(),WebServices.mLoginUtility.getBranch_id());

                    if (WebServices.isNetworkAvailable(FullScreenImageActivity.this))
                        new SubmitData(FullScreenImageActivity.this,mMeterialUtility, WebServices.mLoginUtility.getUser_id(), null, DataBase.getInstance(FullScreenImageActivity.this)).execute();
                    else {

//                    if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
//                        coinsAllocatedModel.setRedeem("TRUE");
//                        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
//                            WebServices.updateTotalCoins(PDFViewerActivity.this, coinsCollected);
//
//                    }
                    }
                }
                finish();

            }
        });
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progressBar.setVisibility(View.GONE);
                if (imageBitmap != null) {
                    imageView.setImageBitmap(imageBitmap);
                }
            }
        };
        new Thread() {
            @Override
            public void run() {

                try {
                    imageBitmap = getBitmapFromURL(getIntent().getStringExtra("imgurl"));

                } catch (Exception e) {
                    if (!FlashCard_Activity.ImgString.equalsIgnoreCase("")) {
                        byte[] decodedString = Base64.decode(FlashCard_Activity.ImgString, Base64.DEFAULT);
                        imageBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    }
                    handler.sendEmptyMessage(0);

                }
//                imageBitmap= getBitmapFromURL(getIntent().getStringExtra("imgurl"));
                if (imageBitmap == null && !FlashCard_Activity.ImgString.equalsIgnoreCase("")) {
                    byte[] decodedString = Base64.decode(FlashCard_Activity.ImgString, Base64.DEFAULT);
                    imageBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                }
                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    public Bitmap getBitmapFromURL(String link) {
        /*--- this method downloads an Image from the given URL,
         *  then decodes and returns a Bitmap object
	     ---*/
        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("getBmpFromUrl error: ", e.getMessage().toString());
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(FullScreenImageActivity.this);
    }
//    public  Bitmap getBitmapFromURL(String link) {
//	    /*--- this method downloads an Image from the given URL,
//	     *  then decodes and returns a Bitmap object
//	     ---*/
//        try {
//            int responseCode = -1;
//            URL url = new URL(link);
//            HttpURLConnection connection = (HttpURLConnection) url
//                    .openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            responseCode = connection.getResponseCode();
//            if(responseCode == HttpURLConnection.HTTP_OK) {
//                InputStream input = connection.getInputStream();
//                Bitmap myBitmap = BitmapFactory.decodeStream(input);
//                return myBitmap;
//            }
//            return null;
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.e("getBmpFromUrl error: ", e.getMessage().toString());
//            return null;
//        }
//    }
}
