package com.chaptervitamins.play_video;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.adapters.FlashCardViewPagerAdapter;
import com.chaptervitamins.newcode.fragments.TemplateSixFragment;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CustomViewPager;
import com.chaptervitamins.utility.FlashCardUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class FlashCard_Activity extends BaseActivity {

    @BindView(R.id.total_txt)
    TextView total_txt;
    @BindView(R.id.title_txt)
    TextView title_txt;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.viewpager)
    CustomViewPager viewpager;
    @BindView(R.id.done_btn)
    Button done_btn;
    //    @BindView(R.id.pre_btn)
//    Button pre_btn;
    String resp = "";
    WebServices webServices;
    Handler handler;
    private ProgressDialog dialog;
    private DataBase dataBase;
    //    private int selectedPostion = 1;
    private boolean isoffline = false;
    private ArrayList<FlashCardUtility> list = new ArrayList<>();
    MixPanelManager mixPanelManager;
    private Bitmap imageBitmap;
    static String ImgString = "";
    boolean isQuesClick = true;
    boolean isAnsClick = true;
    CustomPagerAdapter pagerAdapter;
    private ImageView prev_img, next_img;
    String startTime;
    String endTime;
    AnimatorSet mSetRightOut, mSetLeftIn;
    FlashCardViewPagerAdapter adapter;
    ArrayList<Fragment> fragments = new ArrayList<>();
    int viewPagerPos;
    RelativeLayout footer;
    private MeterialUtility mMeterialUtility;
    private boolean isTemplate6, animationRunning = false;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashcard_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        webServices = new WebServices();
        dataBase = DataBase.getInstance(FlashCard_Activity.this);
        prev_img = (ImageView) findViewById(R.id.prev_img);
        next_img = (ImageView) findViewById(R.id.next_img);
        footer = (RelativeLayout) findViewById(R.id.footer);
        prev_img.setVisibility(View.GONE);
        list = new ArrayList<>();
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        if (mMeterialUtility != null)
//            mMeterialUtility.setMaterialStartTime(df.format(c.getTime()));
        dialog = ProgressDialog.show(FlashCard_Activity.this, "", "Please wait..");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        resp = dataBase.getFlashCardData(WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getMaterial_id());
        mixPanelManager = APIUtility.getMixPanelManager(FlashCard_Activity.this);
        pagerAdapter = new CustomPagerAdapter(FlashCard_Activity.this);
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (dialog != null) dialog.dismiss();
//                if (isoffline) new FatchBgData().execute();
                if (msg.what == 1) {
                    Toast.makeText(FlashCard_Activity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    if (WebServices.flashcardMeterialUtilities != null)
                        title_txt.setText(WebServices.flashcardMeterialUtilities.getTitle());
                    if (list.size() != 0) {
                      /*  if (WebServices.flashcardMeterialUtilities.getQuestion_sequence().equalsIgnoreCase("RANDOM"))
                            Collections.shuffle(list);*/
//                        startTime();
                        viewpager.setAdapter(pagerAdapter);
                        viewpager.setCurrentItem(0);
                        total_txt.setText((viewpager.getCurrentItem() + 1) + "/" + (list.size()) + "   ");
                        if (WebServices.flashcardMeterialUtilities.getMaterial_templete().equalsIgnoreCase("TEMPLATE-1")) {
                            footer.setVisibility(View.VISIBLE);
                        }
//                        show(selectedPostion);
                    } else {
                        total_txt.setText("");
//                        next_btn.setVisibility(View.VISIBLE);
//                        next_btn.setText("Done");
                        noFlashCardData();
                    }
                }
            }
        };
        if (mMeterialUtility.getMaterial_templete() != null) {
            if (!TextUtils.isEmpty(mMeterialUtility.getMaterial_templete())
                    && mMeterialUtility.getMaterial_templete()
                    .equalsIgnoreCase("TEMPLATE-1")) {
                viewpager.setPagingEnabled(false);
                /*  viewpager.onInterceptTouchEvent(null);*/
            } else {
                viewpager.setPagingEnabled(true);
            }
        } else {
            viewpager.setPagingEnabled(false);
        }

        new Thread() {
            public void run() {
                isoffline = true;
                if (webServices.isValid(resp)) {
                    webServices.getFlashcardData(resp);

                    list = WebServices.flashcardMeterialUtilities.getFlashCardUtilities();

                    handler.sendEmptyMessage(0);
                } else {
                    handler.sendEmptyMessage(1);
                }

            }
        }.start();

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                viewPagerPos = position;
               /* if (position == list.size() - 1) {
                    footer.setVisibility(View.VISIBLE);
                    done_btn.setVisibility(View.VISIBLE);
                } else {
                    if (!WebServices.flashcardMeterialUtilities.getMaterial_templete().equalsIgnoreCase("TEMPLATE-6"))
                        footer.setVisibility(GONE);
                }*/
                if (TextUtils.isEmpty(list.get(position).getUserAns())) {
                    list.get(0).setUserAns("1");
                    list.get(position).setUserAns("1");
                }
                if (isTemplate6) {
                    list.get(0).setUserAns("1");
                    list.get(position).setUserAns("1");
                   /* if (position == list.size() - 1) {
                        endTime = DateFormat.getDateTimeInstance().format(new Date());
                        mixPanelManager.selectTimeTrack(FlashCard_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                                , WebServices.flashcardMeterialUtilities.getTitle(), "flashcard");

                        WebServices.flashcardMeterialUtilities.setFlashCardUtilities(list);
                        Intent intent = new Intent(FlashCard_Activity.this, FlashCardResult.class);
                        intent.putExtra("meterial_utility", mMeterialUtility);
                        intent.putExtra("seen_count", viewpager.getCurrentItem() + 1);
                        startActivity(intent);
                        finish();
                    }*/
                }
                if (position == list.size() - 1) {
                    footer.setVisibility(View.VISIBLE);
                    next_img.setVisibility(View.GONE);
                    done_btn.setVisibility(View.VISIBLE);
                    prev_img.setVisibility(GONE);
                } else {
                    done_btn.setVisibility(View.GONE);
                    next_img.setVisibility(View.VISIBLE);
                    if (!mMeterialUtility.getMaterial_templete().equalsIgnoreCase("TEMPLATE-1"))
                        footer.setVisibility(GONE);
                    else
                        footer.setVisibility(View.VISIBLE);
                    if (mMeterialUtility.getMaterial_templete().equalsIgnoreCase("TEMPLATE-5")) {
                        footer.setVisibility(View.VISIBLE);
                    }
                }
                if (mMeterialUtility.getMaterial_templete().equalsIgnoreCase("TEMPLATE-1")) {
                    next_img.setVisibility(View.GONE);
                    done_btn.setVisibility(View.GONE);
                } else {
                    next_img.setVisibility(View.VISIBLE);
                    done_btn.setVisibility(View.VISIBLE);
                }
                if (viewpager.getCurrentItem() == 0) prev_img.setVisibility(View.GONE);
                else prev_img.setVisibility(View.VISIBLE);
                total_txt.setText((viewpager.getCurrentItem() + 1) + "/" + (list.size()) + "   ");

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        next_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < list.size()) {
                    // move to next screen
                    viewpager.setCurrentItem(current);

                }
            }
        });
        prev_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(-1);
                if (current < list.size()) {
                    // move to next screen
                    viewpager.setCurrentItem(current);
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime = DateFormat.getDateTimeInstance().format(new Date());

                mixPanelManager.selectTimeTrack(FlashCard_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                        , mMeterialUtility.getTitle(), "Flashcard");
                WebServices.flashcardMeterialUtilities.setFlashCardUtilities(list);
                Intent intent = new Intent(FlashCard_Activity.this, FlashCardResult.class);
                intent.putExtra("meterial_utility", mMeterialUtility);
                intent.putExtra("seen_count", viewpager.getCurrentItem() + 1);
                startActivity(intent);
//                MyApplication.getInstance().trackEvent("Materials", "Select Material", getIntent().getStringExtra("name") + " (FlashCard Pause)");
                mixPanelManager.Pausematerial(FlashCard_Activity.this, WebServices.mLoginUtility.getEmail(), mMeterialUtility.getCourse_name(), mMeterialUtility.getTitle(), "Flashcard", "0", "");
                finish();
            }
        });
//        revealanswer_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                revealanswer_btn.setVisibility(View.GONE);
//                know_ll.setVisibility(View.VISIBLE);
//                remember_ll.setVisibility(View.VISIBLE);
//                isQuesClick=false;
//                Bitmap icon = BitmapFactory.decodeResource(getResources(),
//                        R.drawable.fc_default);
//                img_view.setImageBitmap(icon);
//                showAnswer(selectedPostion);
//            }
//        });
//        know_ll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                revealanswer_btn.setVisibility(View.GONE);
//                radioButton2.setChecked(false);
//                radioButton.setChecked(true);
//                list.get(selectedPostion).setUserAns("1");
//
//            }
//        });

//        remember_ll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                revealanswer_btn.setVisibility(View.GONE);
//                radioButton2.setChecked(true);
//                radioButton.setChecked(false);
//                list.get(selectedPostion).setUserAns("2");
//            }
//        });

        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime = DateFormat.getDateTimeInstance().format(new Date());
                mixPanelManager.selectTimeTrack(FlashCard_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                        , WebServices.flashcardMeterialUtilities.getTitle(), "flashcard");

                WebServices.flashcardMeterialUtilities.setFlashCardUtilities(list);
                Intent intent = new Intent(FlashCard_Activity.this, FlashCardResult.class);
                intent.putExtra("meterial_utility", mMeterialUtility);
                intent.putExtra("seen_count", viewpager.getCurrentItem() + 1);
                startActivity(intent);
                finish();
//                list.get(selectedPostion).setUserAns("");
//                Bitmap icon = BitmapFactory.decodeResource(getResources(),
//                        R.drawable.fc_default);
//                img_view.setImageBitmap(icon);
//                showdata();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(FlashCard_Activity.this);
    }

    private int getItem(int i) {
        return viewpager.getCurrentItem() + i;
    }
//    private void showdata(final CustomPagerAdapter.MyHolder holder) {
//        if ((selectedPostion + 1) == (list.size())) {
//
//            done_btn.setVisibility(View.VISIBLE);
//
//        } else {
//            done_btn.setVisibility(View.GONE);
//            revealanswer_btn.setVisibility(View.VISIBLE);
//            holder.know_ll.setVisibility(View.VISIBLE);
//            holder.remember_ll.setVisibility(View.VISIBLE);
//            holder.radioButton2.setChecked(false);
//            holder.radioButton.setChecked(false);

//            if (selectedPostion <= (list.size() - 1)) {
//                selectedPostion++;
//                show(holder, selectedPostion);
//            }

    //        }
//    }
    private void setFragmentAdapter() {
        for (int i = 0; i < list.size(); i++)
            fragments.add(TemplateSixFragment.newInstance(list.get(i), i));
        adapter = new FlashCardViewPagerAdapter(getSupportFragmentManager(), fragments);
        viewpager.setAdapter(adapter);
    }

    private void noFlashCardData() {
        new AlertDialog.Builder(FlashCard_Activity.this)
                .setTitle("No FlashCard")
                .setMessage("Not Available FlashCard for you.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    private void show(final CustomPagerAdapter.MyHolder holder, final int pos, Context context) {
        /*holder.know_ll.setVisibility(View.VISIBLE);
        holder.remember_ll.setVisibility(View.VISIBLE);*/

        if (WebServices.flashcardMeterialUtilities.getMaterial_templete() != null) {
            switch (mMeterialUtility.getMaterial_templete()) {
                case "TEMPLATE-1":
                    showTemplate1(holder, pos);
                    break;
                case "TEMPLATE-4":
                    showTemplate4(holder, pos);
                    break;
                case "TEMPLATE-5":
                    //--------- Manthan flashcard Template-----!!
                    showTemplate5(holder, pos, context);
                    break;
                case "TEMPLATE-3":
                    showFlipCardTemplate(holder, pos, context);
                    break;

                case "TEMPLATE-2":
                    showSwipeCardTemplate();
                    break;

                default:
                    /*if (pos == list.size() - 1)
                        footer.setVisibility(View.VISIBLE);*/
                   /* else
                        footer.setVisibility(GONE);*/
                    showDefaultTemplate(holder, pos);
                    break;


            }

        }

    }

    private void showTemplate5(final CustomPagerAdapter.MyHolder holder, int pos, Context mContext) {
        showQuizData(pos, holder);
        holder.ll_template_1.setVisibility(GONE);
        holder.ll_template_2.setVisibility(GONE);
        holder.ll_template_6.setVisibility(GONE);
        holder.temp6Layout.setVisibility(View.VISIBLE);
        holder.know_ll.setVisibility(View.GONE);
        holder.remember_ll.setVisibility(View.GONE);
        Animation RightSwipe = AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.left_ani);
        holder.quizRelativeLayout.startAnimation(RightSwipe);
        holder.quesImageView.startAnimation(RightSwipe);

        holder.quizRelativeLayout.setVisibility(View.VISIBLE);

        Animation fadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
        holder.ansTempLL.startAnimation(fadeInAnimation);
        // holder.ansTempLL.startAnimation(bottomUp);
        holder.ansTempLL.setVisibility(View.VISIBLE);
        holder.revealanswer_btn.setVisibility(View.GONE);
        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.question_txt.setVisibility(View.VISIBLE);
            holder.question_txt_new.setVisibility(View.VISIBLE);
            holder.quesImageView.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.GONE);

            holder.question_txt.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
            holder.question_txt_new.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.imgViewAns.setVisibility(View.VISIBLE);
            holder.answer_txt_new.setVisibility(View.GONE);

        }
    }

    private void showQuizData(final int pos, final CustomPagerAdapter.MyHolder holder) {
        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.question_txt.setVisibility(View.VISIBLE);
            holder.question_txt_new.setVisibility(View.VISIBLE);
            holder.quesImageView.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.GONE);

            holder.question_txt.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
            holder.question_txt_new.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.question_txt.setVisibility(View.GONE);
            holder.question_txt_new.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.VISIBLE);
            holder.quesImageView.setVisibility(View.VISIBLE);
            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(FlashCard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.img_view.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    holder.quesImageView.setImageBitmap(imageBitmap);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view.setImageBitmap(imageBitmap);
                            holder.quesImageView.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view.setImageBitmap(decodedByte);
                            holder.quesImageView.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
        showAnswer(holder, pos);

        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.revealanswer_btn.setVisibility(GONE);
            holder.ans_ll.setVisibility(View.VISIBLE);
            holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
            /*holder.know_ll.setVisibility(View.VISIBLE);
            holder.remember_ll.setVisibility(View.VISIBLE);*/
        } else {
            holder.revealanswer_btn.setVisibility(GONE);
            holder.img_view1.setVisibility(View.VISIBLE);
            holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
            holder.know_ll.setVisibility(View.VISIBLE);
            holder.remember_ll.setVisibility(View.VISIBLE);
        }
    }

    private void changeCameraDistance(FrameLayout front, FrameLayout back) {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        front.setCameraDistance(scale);
        back.setCameraDistance(scale);
    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
    }

    private void showFlipCardTemplate(final CustomPagerAdapter.MyHolder holder, final int pos, Context context) {
        final boolean[] isBackVisible = {false};
        final boolean[] animationRunning = {false};
        if (pos == (list.size() - 1)) {
            footer.setVisibility(View.VISIBLE);
        } else {
            footer.setVisibility(GONE);
        }

        holder.flipCardParent.setVisibility(View.VISIBLE);
        holder.ll_template_6.setVisibility(GONE);
        holder.ll_template_2.setVisibility(GONE);
        holder.temp6Layout.setVisibility(GONE);
        holder.ll_template_1.setVisibility(View.GONE);
        holder.revealanswer_btn.setVisibility(GONE);
        viewpager.setClipToPadding(false);
        viewpager.setPageMargin(15);
//        holder.scrollview.setVisibility(View.VISIBLE);
        loadAnimations();
        changeCameraDistance((FrameLayout) holder.flipCardParent.findViewById(R.id.card_front),
                (FrameLayout) holder.flipCardParent.findViewById(R.id.card_back));
        holder.tvFlashcardCount.setText("Page " + (pos + 1) + "/" + list.size() + "  ");

        holder.tvFlashcardCount.setTextSize(19.0f);
        holder.flipCardFrontText.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        holder.flipCardBackText.setText(Html.fromHtml(list.get(pos).getAnswer()));
        holder.flipCardFrontPos.setText((pos + 1) + "/" + list.size());

        holder.flipCardParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isBackVisible[0]) {
                    mSetRightOut.setTarget(view.findViewById(R.id.card_front));
                    mSetLeftIn.setTarget(view.findViewById(R.id.card_back));

                    mSetRightOut.start();
                    mSetRightOut.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            animationRunning[0] = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });
                    mSetLeftIn.start();
                    mSetLeftIn.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            animationRunning[0] = false;
                            isBackVisible[0] = true;
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

                } else {
                    mSetRightOut.setTarget(view.findViewById(R.id.card_back));
                    mSetLeftIn.setTarget(view.findViewById(R.id.card_front));
                    mSetRightOut.start();
                    mSetRightOut.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                            animationRunning[0] = true;
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });
                    mSetLeftIn.start();
                    mSetLeftIn.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            animationRunning[0] = false;
                            isBackVisible[0] = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

                }

            }
        });
    }

    private void showTemplate4(final CustomPagerAdapter.MyHolder holder, final int pos) {
        holder.ll_template_2.setVisibility(GONE);
        holder.temp6Layout.setVisibility(GONE);
        holder.ll_template_1.setVisibility(View.VISIBLE);
        holder.revealanswer_btn.setVisibility(View.VISIBLE);
//        footer.setVisibility(GONE);
//        holder.scrollview.setVisibility(View.VISIBLE);

        holder.radioButton2.setChecked(false);
        holder.radioButton.setChecked(false);
        holder.main.setBackgroundResource(R.drawable.rect_roud_cornor_shape);

        holder.tvFlashcardCount.setText("Page " + (pos + 1) + "/" + list.size() + "  ");
        holder.tvFlashcardCount.setTextSize(19.0f);
        holder.question_txt.setTextSize(19.0f);
        holder.answer_txt.setTextSize(17.5f);
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getUserAns().equalsIgnoreCase("1"))
            holder.radioButton.setChecked(true);
        else if (list.get(pos).getUserAns().equalsIgnoreCase("2"))
            holder.radioButton2.setChecked(true);

        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.question_txt.setVisibility(View.VISIBLE);
            holder.question_txt_new.setVisibility(View.VISIBLE);
            holder.quesImageView.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.GONE);
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.question_txt.setVisibility(View.GONE);
            holder.question_txt_new.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.VISIBLE);
            holder.quesImageView.setVisibility(View.VISIBLE);
            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(FlashCard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.img_view.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    holder.quesImageView.setImageBitmap(imageBitmap);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view.setImageBitmap(imageBitmap);
                            holder.quesImageView.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view.setImageBitmap(decodedByte);
                            holder.quesImageView.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
        showAnswer(holder, pos);

        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.revealanswer_btn.setVisibility(GONE);
            holder.ans_ll.setVisibility(View.VISIBLE);
            holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
            /*holder.know_ll.setVisibility(View.VISIBLE);
            holder.remember_ll.setVisibility(View.VISIBLE);*/
        } else {
            holder.revealanswer_btn.setVisibility(GONE);
            holder.img_view1.setVisibility(View.VISIBLE);
            holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
            holder.know_ll.setVisibility(View.VISIBLE);
            holder.remember_ll.setVisibility(View.VISIBLE);
        }

    }

    private void showDefaultTemplate(final CustomPagerAdapter.MyHolder holder, final int pos) {
//        holder.scrollview.setVisibility(GONE);
        holder.ll_template_2.setVisibility(View.VISIBLE);
        holder.ll_template_1.setVisibility(GONE);
        holder.tvQuesNo.setText(pos + 1 + "");
//        footer.setVisibility(GONE);
        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.tvQuestion.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.tvQuestion.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.tvQuestion.setVisibility(View.GONE);
            holder.ivQues.setVisibility(View.VISIBLE);
//            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(FlashCard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.ivQues.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.ivQues.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.ivQues.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.ivQues.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }


        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            holder.tvAns.setVisibility(View.VISIBLE);
            holder.ivAns.setVisibility(View.GONE);
            holder.tvAns.setText(Html.fromHtml(list.get(pos).getAnswer()));
        } else {
//            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.tvAns.setVisibility(View.GONE);
            holder.ivAns.setVisibility(View.VISIBLE);
//            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(FlashCard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url());
                if (imageBitmap != null) {
                    holder.ivAns.setImageBitmap(imageBitmap);
//                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getAnswer_image_url(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.ivAns.setImageBitmap(decodedByte);
//                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.ivAns.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getAnswer_image_url(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.ivAns.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getAnswer_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
    }

    private void showTemplate1(final CustomPagerAdapter.MyHolder holder, final int pos) {

        holder.ll_template_2.setVisibility(GONE);
        holder.ll_template_6.setVisibility(GONE);
        holder.temp6Layout.setVisibility(GONE);
        holder.ll_template_1.setVisibility(View.VISIBLE);
        next_img.setVisibility(GONE);
        done_btn.setVisibility(GONE);
//        holder.scrollview.setVisibility(View.VISIBLE);
//        footer.setVisibility(View.VISIBLE);
        holder.radioButton2.setChecked(false);
        holder.radioButton.setChecked(false);

        holder.tvFlashcardCount.setText("Flashcard " + (pos + 1) + "/" + list.size() + "   ");
        holder.tvFlashcardCount.setTextSize(18.0f);
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getUserAns().equalsIgnoreCase("1"))
            holder.radioButton.setChecked(true);
        else if (list.get(pos).getUserAns().equalsIgnoreCase("2"))
            holder.radioButton2.setChecked(true);

        if (list.get(pos).getIs_question_image().equalsIgnoreCase("0")) {
            holder.question_txt.setVisibility(View.VISIBLE);
            holder.question_txt_new.setVisibility(View.VISIBLE);
            holder.img_view.setVisibility(View.GONE);
            holder.question_txt.setText(Html.fromHtml(list.get(pos).getQuestion_description()));
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);
            holder.question_txt.setVisibility(View.GONE);
            holder.question_txt_new.setVisibility(View.GONE);
            holder.img_view.setVisibility(View.VISIBLE);
            isQuesClick = true;
            if (!WebServices.isNetworkAvailable(FlashCard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url());
                if (imageBitmap != null) {
                    holder.img_view.setImageBitmap(imageBitmap);
                    holder.progress_bar.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view.setImageBitmap(decodedByte);
                holder.progress_bar.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getQuestion_description_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getQuestion_description(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getQuestion_description_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }
        showAnswer(holder, pos);
    }

    private void showAnswer(final CustomPagerAdapter.MyHolder holder, final int pos) {
        holder.progress_bar.setVisibility(View.GONE);
        if (list.get(pos).getIs_answer_image().equalsIgnoreCase("0")) {
            // holder.answer_txt.setVisibility(View.VISIBLE);
            holder.img_view1.setVisibility(View.GONE);
            holder.answer_txt.setText(Html.fromHtml(list.get(pos).getAnswer()));
            holder.answer_txt_new.setText(Html.fromHtml(list.get(pos).getAnswer()));
            holder.imgViewAns.setVisibility(View.GONE);
            holder.answer_txt_new.setVisibility(View.VISIBLE);
            holder.ans_ll.setVisibility(View.GONE);
            holder.know_ll.setVisibility(View.GONE);
        } else {
            // holder.answer_txt.setVisibility(View.GONE);
//            holder.img_view1.setVisibility(View.VISIBLE);
            holder.answer_txt_new.setVisibility(View.GONE);
            holder.imgViewAns.setVisibility(View.VISIBLE);
            holder.img_view1.setVisibility(View.GONE);
            holder.know_ll.setVisibility(View.GONE);
            holder.remember_ll.setVisibility(View.GONE);
            isAnsClick = true;
//            holder.progress_bar1.setVisibility(View.VISIBLE);
            if (!WebServices.isNetworkAvailable(FlashCard_Activity.this)) {
                imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url());
                if (imageBitmap != null) {
                    holder.img_view1.setImageBitmap(imageBitmap);
                    holder.progress_bar1.setVisibility(View.GONE);
                    return;
                }
                byte[] decodedString = Base64.decode(list.get(pos).getAnswer(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.img_view1.setImageBitmap(decodedByte);
                holder.progress_bar1.setVisibility(View.GONE);
            } else {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        holder.progress_bar1.setVisibility(View.GONE);
                        if (imageBitmap != null) {
                            holder.img_view1.setImageBitmap(imageBitmap);
                            dataBase.addImage(WebServices.mLoginUtility.getUser_id(), list.get(pos).getAnswer_image_url(), imageBitmap);
                        } else {
                            byte[] decodedString = Base64.decode(list.get(pos).getAnswer(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder.img_view1.setImageBitmap(decodedByte);
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        imageBitmap = webServices.getImage(list.get(pos).getAnswer_image_url());
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        }

    }

    private void showSwipeCardTemplate() {

        /*Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("list", list);
        bundle.putInt("type", 1);
        startActivity(new Intent(this, FlashCardActivity.class)
                .putExtras(bundle));*/
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();


    }

    public class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;

        public CustomPagerAdapter(Context context) {
            mContext = context;
        }

        class MyHolder {
            TextView question_txt, answer_txt, tvFlashcardCount, answer_txt_new, question_txt_new, flipCardFrontText, flipCardBackText, flipCardFrontPos;
            LinearLayout know_ll, remember_ll;
            FrameLayout flipCardParent, cardBack, cardFront;
            RadioButton radioButton, radioButton2;
            ImageView img_view, img_view1, ivAns, ivQues, ivAnswer, ivQuestion, quesImageView, imgViewAns;
            ProgressBar progress_bar, progress_bar1;
            LinearLayout ans_ll, ll_template_1, ll_template_2, ll_template_6, temp6Layout;
            Button revealanswer_btn;
            RelativeLayout main, ansTempLL, quizRelativeLayout, ansRelativeLayout;
            RelativeLayout footer;
            TextView tvQuestion, tvAns, tvQuesNo, tvTitle, tvQuestionNo, tvQuestion6, tvAnswer;
            ScrollView scrollview;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            final MyHolder holder = new MyHolder();

            //Fragment Layout
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.flashcard_pager_row, collection,
                    false);
            /*                                                                   */
            if (!WebServices.flashcardMeterialUtilities.getMaterial_templete().equals("")) {
                switch (WebServices.flashcardMeterialUtilities.getMaterial_templete()) {
                    case "TEMPLATE-4":
                        setFragmentAdapter();
                        break;
                }
            }
            holder.flipCardParent = (FrameLayout) layout.findViewById(R.id.flip_card_template);

            holder.flipCardBackText = (TextView) holder.flipCardParent.findViewById(R.id.text_back);
            holder.flipCardFrontText = (TextView) holder.flipCardParent.findViewById(R.id.text_front);
            holder.flipCardFrontPos = (TextView) holder.flipCardParent.findViewById(R.id.text_front_position);
            /*                                                                   */

            holder.question_txt = (TextView) layout.findViewById(R.id.question_txt);
            holder.question_txt_new = (TextView) layout.findViewById(R.id.question_txt_new);
            holder.answer_txt = (TextView) layout.findViewById(R.id.answer_txt);
            holder.answer_txt_new = (TextView) layout.findViewById(R.id.answer_txt_new);
            holder.tvQuestion = (TextView) layout.findViewById(R.id.tv_question);
            holder.tvTitle = (TextView) layout.findViewById(R.id.tv_title);
            holder.tvQuestionNo = (TextView) layout.findViewById(R.id.tv_question_no);
            holder.tvQuestion6 = (TextView) layout.findViewById(R.id.tv_question_6);
            holder.tvAnswer = (TextView) layout.findViewById(R.id.tv_answer);
            holder.tvAns = (TextView) layout.findViewById(R.id.tv_ans);
            holder.scrollview = (ScrollView) layout.findViewById(R.id.scrollview);
            holder.tvQuesNo = (TextView) layout.findViewById(R.id.tv_ques_no_temp2);
            holder.tvFlashcardCount = (TextView) layout.findViewById(R.id.tv_no_of_flashcard);
            holder.know_ll = (LinearLayout) layout.findViewById(R.id.know_ll);
            holder.ans_ll = (LinearLayout) layout.findViewById(R.id.ans_ll);
            holder.revealanswer_btn = (Button) layout.findViewById(R.id.revealanswer_btn);
            holder.remember_ll = (LinearLayout) layout.findViewById(R.id.remember_ll);
            holder.main = (RelativeLayout) layout.findViewById(R.id.main);
            holder.footer = (RelativeLayout) layout.findViewById(R.id.footer);
            holder.radioButton = (RadioButton) layout.findViewById(R.id.radioButton);
            holder.radioButton2 = (RadioButton) layout.findViewById(R.id.radioButton2);
            holder.img_view = (ImageView) layout.findViewById(R.id.img_view);
            holder.ivQues = (ImageView) layout.findViewById(R.id.iv_ques_temp2);
            holder.ivQuestion = (ImageView) layout.findViewById(R.id.iv_question);
            holder.ivAns = (ImageView) layout.findViewById(R.id.iv_ans);
            holder.ivAnswer = (ImageView) layout.findViewById(R.id.iv_answer);
            holder.progress_bar = (ProgressBar) layout.findViewById(R.id.progress_bar);
            holder.img_view1 = (ImageView) layout.findViewById(R.id.img_view1);
            holder.progress_bar1 = (ProgressBar) layout.findViewById(R.id.progress_bar1);
            holder.ll_template_1 = (LinearLayout) layout.findViewById(R.id.ll_template_1);
            holder.ll_template_2 = (LinearLayout) layout.findViewById(R.id.ll_template_2);
            holder.ll_template_6 = (LinearLayout) layout.findViewById(R.id.ll_template_6);


            holder.ansTempLL = (RelativeLayout) layout.findViewById(R.id.ansTempLL);
            holder.temp6Layout = (LinearLayout) layout.findViewById(R.id.temp6Layout);
            holder.revealanswer_btn = (Button) layout.findViewById(R.id.revealanswer_btn);
            holder.quesImageView = (ImageView) layout.findViewById(R.id.quesImageView);
            holder.imgViewAns = (ImageView) layout.findViewById(R.id.imgViewAns);

            holder.progress_bar1 = (ProgressBar) layout.findViewById(R.id.progress_bar1);

            holder.quizRelativeLayout = (RelativeLayout) layout.findViewById(R.id.quizRelativeLayout);
            holder.ansRelativeLayout = (RelativeLayout) layout.findViewById(R.id.ansRelativeLayout);
            holder.ansRelativeLayout = (RelativeLayout) layout.findViewById(R.id.ansRelativeLayout);


            holder.ans_ll.setVisibility(View.GONE);
            holder.tvFlashcardCount.setTextColor(Utils.getColorPrimary());
            holder.know_ll.setVisibility(View.GONE);
            holder.remember_ll.setVisibility(View.GONE);
            /*if (!TextUtils.isEmpty(WebServices.flashcardMeterialUtilities.getMaterial_templete())&&WebServices.flashcardMeterialUtilities.getMaterial_templete().equalsIgnoreCase
                    ("TEMPLATE-1")) {
                holder.ll_template_1.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
            }*/

            /*if (mMeterialUtility.getMaterial_templete().equalsIgnoreCase
                    ("TEMPLATE-6")) {
                isTemplate6 = true;
                holder.ll_template_1.setVisibility(View.GONE);
                holder.ll_template_2.setVisibility(View.GONE);
                holder.ll_template_6.setVisibility(View.VISIBLE);
//                footer.setVisibility(GONE);
                prev_img.setVisibility(GONE);
                showNewFlashcard(holder, position);
              *//*  if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                    holder.answer_txt_new.setVisibility(View.VISIBLE);
                    holder.imgViewAns.setVisibility(View.GONE);
                } else {
//                footer.setVisibility(View.VISIBLE);
                    holder.ll_template_6.setVisibility(View.GONE);
                    show(holder, position);
                }*//*
            } else {
                holder.ll_template_6.setVisibility(View.GONE);
                show(holder, position, mContext);
            }*/
            /*if (WebServices.flashcardMeterialUtilities.getMaterial_templete().equalsIgnoreCase
                    ("TEMPLATE-5")) {
                holder.ll_template_1.setVisibility(GONE);
                holder.temp6Layout.setVisibility(View.VISIBLE);
                holder.know_ll.setVisibility(View.GONE);
                holder.remember_ll.setVisibility(View.GONE);
                Animation RightSwipe = AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.left_ani);
                holder.quizRelativeLayout.startAnimation(RightSwipe);
                holder.quesImageView.startAnimation(RightSwipe);

                holder.quizRelativeLayout.setVisibility(View.VISIBLE);


                Animation fadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
                holder.ansTempLL.startAnimation(fadeInAnimation);
                // holder.ansTempLL.startAnimation(bottomUp);
                holder.ansTempLL.setVisibility(View.VISIBLE);
                holder.revealanswer_btn.setVisibility(View.GONE);
                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                    holder.answer_txt_new.setVisibility(View.VISIBLE);
                    holder.imgViewAns.setVisibility(View.GONE);
                } else {
                    holder.imgViewAns.setVisibility(View.VISIBLE);
                    holder.answer_txt_new.setVisibility(View.GONE);
                }
            } else {
                holder.ll_template_1.setVisibility(View.VISIBLE);
                holder.temp6Layout.setVisibility(View.GONE);
                holder.know_ll.setVisibility(View.VISIBLE);
                holder.remember_ll.setVisibility(View.VISIBLE);
                holder.revealanswer_btn.setVisibility(View.VISIBLE);
            }*/

            if (mMeterialUtility.getMaterial_templete() != null && mMeterialUtility.getMaterial_templete().equalsIgnoreCase("TEMPLATE-5")) {
                holder.revealanswer_btn.setText("Tap to continue");
            }


            holder.revealanswer_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mMeterialUtility.getMaterial_templete() != null) {
                        switch (mMeterialUtility.getMaterial_templete()) {
                            case "TEMPLATE-1":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }

                                break;
                            /*case "TEMPLATE-2":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.hyperspace_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.hyperspace_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;
                            case "TEMPLATE-3":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.anim_fade_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.anim_fade_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;
                            case "TEMPLATE-5":
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;*/
                            case "TEMPLATE-4":
                                /*if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }*/
                                holder.ll_template_2.setVisibility(View.GONE);
                                holder.ans_ll.setVisibility(View.VISIBLE);
                                break;
                            case "TEMPLATE-6":
                                break;
                            case "TEMPLATE-5":
                                showQuizData(position, holder);
                                holder.ll_template_1.setVisibility(GONE);
                                holder.ll_template_2.setVisibility(GONE);
                                holder.ll_template_6.setVisibility(GONE);
                                holder.temp6Layout.setVisibility(View.VISIBLE);
                                holder.know_ll.setVisibility(View.GONE);
                                holder.remember_ll.setVisibility(View.GONE);

                                Animation fadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
                                holder.ansTempLL.startAnimation(fadeInAnimation);
                                // holder.ansTempLL.startAnimation(bottomUp);
                                holder.ansTempLL.setVisibility(View.VISIBLE);
                                holder.revealanswer_btn.setVisibility(View.GONE);
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.answer_txt_new.setVisibility(View.VISIBLE);
                                    holder.imgViewAns.setVisibility(View.GONE);
                                } else {
                                    holder.imgViewAns.setVisibility(View.VISIBLE);
                                    holder.answer_txt_new.setVisibility(View.GONE);
                                }

                               /* if(list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.anim_fade_out));


                                }else{
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.anim_fade_out));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }*/
                                break;

                            default:
                                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.ans_ll.setVisibility(View.VISIBLE);
                                    holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                } else {
                                    holder.revealanswer_btn.setVisibility(GONE);
                                    holder.img_view1.setVisibility(View.VISIBLE);
                                    holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                                    holder.know_ll.setVisibility(View.VISIBLE);
                                    holder.remember_ll.setVisibility(View.VISIBLE);
                                }
                                break;

                        }
                    } else {
                        if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                            holder.revealanswer_btn.setVisibility(GONE);
                            holder.ans_ll.setVisibility(View.VISIBLE);
                            holder.ans_ll.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                            holder.know_ll.setVisibility(View.VISIBLE);
                            holder.remember_ll.setVisibility(View.VISIBLE);
                        } else {
                            holder.revealanswer_btn.setVisibility(GONE);
                            holder.img_view1.setVisibility(View.VISIBLE);
                            holder.img_view1.startAnimation(AnimationUtils.loadAnimation(FlashCard_Activity.this, R.anim.slide_in_up));
                            holder.know_ll.setVisibility(View.VISIBLE);
                            holder.remember_ll.setVisibility(View.VISIBLE);
                        }
                    }

                }


            });

            //holder.answer_txt.setVisibility(holder.answer_txt.VISIBLE);

           /* holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        holder.radioButton2.setChecked(false);
                        holder.radioButton.setChecked(true);
                        list.get(position).setUserAns("1");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });*/

            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.radioButton.isChecked()) {
                        holder.radioButton2.setChecked(false);
                        holder.radioButton.setChecked(true);
                        list.get(position).setUserAns("1");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                } else {
                                    done_btn.setVisibility(View.VISIBLE);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });
           /* holder.radioButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        holder.radioButton2.setChecked(true);
                        holder.radioButton.setChecked(false);
                        list.get(position).setUserAns("2");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });
*/
            holder.radioButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.radioButton2.isChecked()) {
                        holder.radioButton2.setChecked(true);
                        holder.radioButton.setChecked(false);
                        list.get(position).setUserAns("2");
                        final Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.fc_default);
                                holder.img_view.setImageBitmap(icon);
                                int current = getItem(+1);
                                if (current < list.size()) {
                                    // move to next screen
                                    viewpager.setCurrentItem(current);
                                } else {
                                    done_btn.setVisibility(View.VISIBLE);
                                }
                            }
                        };
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    sleep(500);
                                } catch (InterruptedException e) {
                                } finally {
                                    handler.sendEmptyMessage(0);
                                }
                            }
                        }.start();
                    }
                }
            });


            holder.img_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FlashCard_Activity.this, FullScreenImageActivity.class);
                    if (isQuesClick)
                        intent.putExtra("imgurl", list.get(position).getQuestion_description_image_url());
                    else intent.putExtra("imgurl", list.get(position).getAnswer_image_url());
                    if (isQuesClick)
                        ImgString = list.get(position).getQuestion_description();
                    else ImgString = list.get(position).getAnswer();
                    startActivity(intent);
                }
            });
            holder.img_view1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FlashCard_Activity.this, FullScreenImageActivity.class);
                    if (!isAnsClick)
                        intent.putExtra("imgurl", list.get(position).getQuestion_description_image_url());
                    else intent.putExtra("imgurl", list.get(position).getAnswer_image_url());
                    if (!isAnsClick)
                        ImgString = list.get(position).getQuestion_description();
                    else ImgString = list.get(position).getAnswer();
                    startActivity(intent);
                }
            });
            ((ViewPager) collection).addView(layout);
            return layout;
        }


        private void showNewFlashcard(MyHolder holder, int position) {
            if (holder != null && position != -1) {
                holder.tvTitle.setText(WebServices.flashcardMeterialUtilities.getTitle());
                holder.tvQuestionNo.setText((position + 1) + "");
                if (list.get(position).getIs_question_image().equalsIgnoreCase("0")) {
                    holder.ivQuestion.setVisibility(View.GONE);
                    holder.tvQuestion6.setVisibility(View.VISIBLE);
                    holder.tvQuestion6.setText(Html.fromHtml(list.get(position).getQuestion_description()));
                } else {
                    holder.ivQuestion.setVisibility(View.VISIBLE);
                    holder.tvQuestion.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(list.get(position).getQuestion_description_image_url()))
                        Picasso.with(FlashCard_Activity.this).load(list.get(position).getQuestion_description_image_url()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.ivQuestion);
                    else
                        Picasso.with(FlashCard_Activity.this).load(R.mipmap.ic_launcher).into(holder.ivQuestion);
                }

                if (list.get(position).getIs_answer_image().equalsIgnoreCase("0")) {
                    holder.ivAnswer.setVisibility(View.GONE);
                    holder.tvAnswer.setVisibility(View.VISIBLE);
                    holder.tvAnswer.setText(Html.fromHtml(list.get(position).getAnswer()));
                } else {
                    holder.ivAnswer.setVisibility(View.VISIBLE);
                    holder.tvAnswer.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(list.get(position).getAnswer_image_url()))
                        Picasso.with(FlashCard_Activity.this).load(list.get(position).getAnswer_image_url()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.ivAnswer);
                    else
                        Picasso.with(FlashCard_Activity.this).load(R.mipmap.ic_launcher).into(holder.ivAnswer);
                }
            }
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }


    }
}
