package com.chaptervitamins.play_video;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.Suggestions.File_Uploader;
import com.chaptervitamins.Suggestions.Suggestion_Main_Activity;
import com.chaptervitamins.WebServices.MediaPlaybackService;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


public class JW_Player_Activity extends BaseActivity implements UploadListener {
    private TextView video_title;
    //private JWPlayerView mPlayerView;
    private ImageView play_img, cancel_img, ok_img, back;
    String FILENAME = "";
    long size = 0;
    RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_jw_player);

        video_title = (TextView) findViewById(R.id.video_title);
//        video_title.setText(getIntent().getStringExtra("desc"));
        play_img = (ImageView) findViewById(R.id.youtube_img);
        cancel_img = (ImageView) findViewById(R.id.cancel_img);
        ok_img = (ImageView) findViewById(R.id.ok_img);
        rl = (RelativeLayout) findViewById(R.id.rl);
        back = (ImageView) findViewById(R.id.back);
        if (!getIntent().getStringExtra("screen").equalsIgnoreCase("upload")) {
            rl.setVisibility(View.GONE);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ok_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FILENAME.equalsIgnoreCase("") && size == 0) {
                    Toast.makeText(JW_Player_Activity.this, "Can't Upload. Please try again.", Toast.LENGTH_LONG).show();
                    return;
                }

                new File_Uploader(JW_Player_Activity.this, getIntent().getStringExtra("file_path"), APIUtility.UPLOAD_FILE, FILENAME, size, "VIDEO", true, JW_Player_Activity.this);
//                final ProgressDialog dialog = ProgressDialog.show(JW_Player_Activity.this, "", "Please wait..");
//                dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
//                final Handler handler=new Handler(){
//                    @Override
//                    public void handleMessage(Message msg) {
//                        if (dialog!=null)dialog.dismiss();
//                        if (mPlayerView!=null)mPlayerView.pause();
//                        uploadDialog("");
//                    }
//                };
//                new Thread(){
//                    @Override
//                    public void run() {
//                        try {
//                            sleep(2000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }finally {
//                            handler.sendEmptyMessage(0);
//                        }
//                    }
//                }.start();
            }
        });
        System.out.println("=======" + getIntent().getStringExtra("file_path"));
        File file = new File(getIntent().getStringExtra("file_path"));
        String url = getIntent().getStringExtra("file_path");
        if (!TextUtils.isEmpty(url)) {
            String[] r = url.split("/");
            System.out.println("====" + r[r.length - 1]);
            FILENAME = r[r.length - 1];
            size = file.length();
        }

        play_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//               String filepath=Uri.encode(getIntent().getStringExtra("file_path"),"UTF-8");
                //PlayerConfig playerConfig = new PlayerConfig.Builder()
                //      .file(getIntent().getStringExtra("file_path")).autostart(true)
                //    .build();
                //mPlayerView = new JWPlayerView(JW_Player_Activity.this, playerConfig);
                //mPlayerView.setFullscreen(false, false);
                /*mPlayerView.addOnAdErrorListener(new OnAdErrorListener() {
                    @Override
                    public void onAdError(String s, String s1) {
                    }



                });*/

                RelativeLayout container = (RelativeLayout) findViewById(R.id.container);
                /*container.addView(mPlayerView, new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));*/
            }
        });

    }

    private void uploadDialog(String filepath) {
        final Dialog dialog = new Dialog(JW_Player_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_dialog);
        TextView title_dialog = (TextView) dialog.findViewById(R.id.title_dialog);
        TextView desc_dialog = (TextView) dialog.findViewById(R.id.desc_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button submit_btn = (Button) dialog.findViewById(R.id.submit_btn);
        title_dialog.setVisibility(View.GONE);
        cancel_btn.setVisibility(View.GONE);
        submit_btn.setVisibility(View.VISIBLE);
        desc_dialog.setText("Your Answer is successfully submitted to \n Admin for review");
        desc_dialog.setTextSize(10);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        ok_btn.setVisibility(View.GONE);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(getIntent().getStringExtra("file_path"));
                if (f.exists()) f.delete();
                dialog.dismiss();
            }
        });
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(getIntent().getStringExtra("file_path"));
                if (f.exists()) f.delete();
                Toast.makeText(JW_Player_Activity.this, "Successfully Uploaded!", Toast.LENGTH_LONG).show();
                finish();
            }
        });
        dialog.show();
    }

    private static final String TAG = "VideoActivity";


    /*
     * Whether we have bound to a {@link MediaPlaybackService}.
     */
    private boolean mIsBound;

    /*
     * The {@link MediaPlaybackService} we are bound to.
     */
    private MediaPlaybackService mMediaPlaybackService;

    /*
     * The {@link ServiceConnection} serves as glue between this activity and the {@link MediaPlaybackService}.
     */
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            mMediaPlaybackService = ((MediaPlaybackService.MediaPlaybackServiceBinder) service)
                    .getService();
//            mMediaPlaybackService.setActivePlayer(mPlayerView);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            mMediaPlaybackService = null;
        }
    };

    @Override
    protected void onPause() {
        /*if (mPlayerView != null) {
            // Allow background audio playback.
            try {
                //mPlayerView.setBackgroundAudio(false);
                //mPlayerView.onPause();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        super.onPause();
    }


    /*@Override
    public void onBackPressed() {
        try {
            if (mPlayerView != null) {
                if (mPlayerView.getFullscreen()) {
                } else {
                    super.onBackPressed();
                }

            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/
/*

    @Override
    protected void onDestroy() {
//        doUnbindService();
        try {
            if (mPlayerView != null)
                mPlayerView.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
*/

    private void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
        bindService(new Intent(JW_Player_Activity.this,
                MediaPlaybackService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    private void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            unbindService(mServiceConnection);
            mIsBound = false;
        }
    }

    @Override
    public void error(String error) {
        Suggestion_Main_Activity.MEDIA_ID = "";
        Intent intent = new Intent();
        intent.putExtra("success", false);
        setResult(100, intent);
    }

    @Override
    public void complete(int media_id, String type, String response) {
        Suggestion_Main_Activity.MEDIA_ID = media_id + "";
        Intent intent = new Intent();
        intent.putExtra("success", true);
        intent.putExtra("media_id", media_id + "");
        try {
            JSONObject jsonObject = new JSONObject(response);
            jsonObject = jsonObject.optJSONObject("data");
            intent.putExtra("response", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        intent.putExtra("response", response);
        setResult(100, intent);
//        Toast.makeText(this, "Image Uploaded Successfully.", Toast.LENGTH_SHORT).show();
        finish();
    }
}
