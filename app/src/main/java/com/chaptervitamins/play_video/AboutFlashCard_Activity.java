package com.chaptervitamins.play_video;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.NewFlashCardActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.MeterialUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutFlashCard_Activity extends BaseActivity {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.start_ques_ll)
    LinearLayout start_ques_ll;
    @BindView(R.id.about_txt)
    TextView about_txt;
    @BindView(R.id.totalques_txt)
    TextView totalques_txt;
    @BindView(R.id.total_time_txt)
    TextView total_time_txt;
    @BindView(R.id.desc_ques_txt)
    TextView desc_ques_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_about_flash_card);
        ButterKnife.bind(this);
        final MeterialUtility meterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");

        title.setText(meterialUtility.getTitle());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        desc_ques_txt.setText(Html.fromHtml(meterialUtility.getInstruction()));
        about_txt.setText(Html.fromHtml(meterialUtility.getDescription()));
        if (meterialUtility.getNo_of_question().equals("0"))
            findViewById(R.id.ll_ques).setVisibility(View.GONE);
        totalques_txt.setText(meterialUtility.getNo_of_question());
        total_time_txt.setText(meterialUtility.getAlloted_time() + " min");
        start_ques_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutFlashCard_Activity.this, NewFlashCardActivity.class);
                MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(AboutFlashCard_Activity.this);
                mixPanelManager.startquizflash(AboutFlashCard_Activity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "FlashCard");
                intent.putExtra("meterial_utility", meterialUtility);
                startActivity(intent);
                finish();
            }
        });
    }
}
