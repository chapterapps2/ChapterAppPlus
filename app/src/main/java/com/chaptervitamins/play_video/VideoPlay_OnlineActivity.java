package com.chaptervitamins.play_video;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.utility.MeterialUtility;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Android on 9/26/2016.
 */

public class VideoPlay_OnlineActivity extends AppCompatActivity {
    WebView webview;
    ProgressBar progress;
    private long TIMERVALUE = 10;
    private boolean TimerVal = true;
    private Handler handler2 = new Handler();
    private DataBase dataBase;
    private MeterialUtility mMeterialUtility;
    private boolean isNextButtonClicked;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.videoplay_onlineactivity);
        dataBase = DataBase.getInstance(VideoPlay_OnlineActivity.this);
        webview = (WebView) findViewById(R.id.webView);
        progress = (ProgressBar) findViewById(R.id.progressbar);
        WebSettings webSettings = webview.getSettings();
        if(getIntent().hasExtra("meterial_utility")){
            mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        }
       /* new Thread(new Runnable() {
            @Override
            public void run() {
                coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(getIntent().getStringExtra("course_id"), getIntent().getStringExtra("meterial_id"));
                if (coinsAllocatedModel == null) {
                    FlowingCourseUtils.getCoinsAllocatedFromCourseList(getIntent().getStringExtra("meterial_id"));
                }
                if (coinsAllocatedModel != null) {
                    redeem = coinsAllocatedModel.getRedeem();
                    noOfCoins = coinsAllocatedModel.getMaxCoins();
                } else
                    coinsAllocatedModel = new CoinsAllocatedModel();
            }
        }).start();*/
        if (!isConnectedFast(this))
            Toast.makeText(this, "Your internet speed may be slow, please turn to WiFi else buffering will take time.", Toast.LENGTH_LONG).show();
//        webview.loadUrl("file:///android_asset/test.html");
//        startTime();
        webview.setWebChromeClient(new MyWebViewClient());
        String theArgumentYouWantToPass = getIntent().getStringExtra("file_url");
        if (theArgumentYouWantToPass.contains("watch?v="))
            theArgumentYouWantToPass = theArgumentYouWantToPass.replace("watch?v=", "embed/");
        String playVideo = "<html><body style=\"margin:0; padding:0px;\"><iframe  style='margin:0; padding:0px; width:100%; height:100%;' src='" + theArgumentYouWantToPass + "' frameborder='0' allowfullscreen></iframe></body></html>";
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadData(playVideo, "text/html", "utf-8");
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        if(!=null)
//            mMeterialUtility.setMaterialStartTime(df.format(c.getTime()));

//        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
////        webview.loadUrl("https://www.youtube.com/watch?v=MOnMuz9Kux8");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            webSettings.setAllowFileAccessFromFileURLs(true);
//            webSettings.setAllowUniversalAccessFromFileURLs(true);
//        }
//        webview.setWebViewClient(new WebViewClient(){
//            public void onPageFinished(WebView view, String url){
//                webview.loadUrl("javascript:init('" + theArgumentYouWantToPass + "')");
//            }
//        });

        ((ImageView) findViewById(R.id.back_img)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

                /*String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(100,redeem));

                if(!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    Toast.makeText(VideoPlay_OnlineActivity.this, "You have awarded "+coinsCollected+ " coins", Toast.LENGTH_SHORT).show();
                if (WebServices.isNetworkAvailable(VideoPlay_OnlineActivity.this))
                    new SubmitData(VideoPlay_OnlineActivity.this, WebServices.mLoginUtility.getUser_id(), getIntent().getStringExtra("course_id"), getIntent().getStringExtra("meterial_id"), "VIDEO", coinsAllocatedModel, dataBase).execute();
                else {
                    new WebServices().addSubmitResponse(dataBase, getIntent().getStringExtra("course_id"), getIntent().getStringExtra("meterial_id"), "VIDEO", "100", "", "", "", getIntent().getStringExtra("title"), "",coinsCollected , redeem);
                    if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                        coinsAllocatedModel.setRedeem("TRUE");
                        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                            WebServices.updateTotalCoins(VideoPlay_OnlineActivity.this, coinsCollected);

                    }
                }*/
//                HomeActivity.QUIZSTARTTIME="";
                finish();
            }
        });
    }

    private class MyWebViewClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }

    }

    public String getNetworkClass(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
            return "-"; //not connected
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                default:
                    return "";
            }
        } else
            return "";

    }

    //    public String getNetworkClass(Context context) {
//        TelephonyManager mTelephonyManager = (TelephonyManager)
//                context.getSystemService(Context.TELEPHONY_SERVICE);
//        int networkType = mTelephonyManager.getNetworkType();
//        switch (networkType) {
//            case TelephonyManager.NETWORK_TYPE_GPRS:
//            case TelephonyManager.NETWORK_TYPE_EDGE:
//            case TelephonyManager.NETWORK_TYPE_CDMA:
//            case TelephonyManager.NETWORK_TYPE_1xRTT:
//            case TelephonyManager.NETWORK_TYPE_IDEN:
//                return "2G";
//            case TelephonyManager.NETWORK_TYPE_UMTS:
//            case TelephonyManager.NETWORK_TYPE_EVDO_0:
//            case TelephonyManager.NETWORK_TYPE_EVDO_A:
//            case TelephonyManager.NETWORK_TYPE_HSDPA:
//            case TelephonyManager.NETWORK_TYPE_HSUPA:
//            case TelephonyManager.NETWORK_TYPE_HSPA:
//            case TelephonyManager.NETWORK_TYPE_EVDO_B:
//            case TelephonyManager.NETWORK_TYPE_EHRPD:
//            case TelephonyManager.NETWORK_TYPE_HSPAP:
//                return "3G";
//            case TelephonyManager.NETWORK_TYPE_LTE:
//                return "4G";
//            default:
//                return "Unknown";
//        }
//    }
    public void setValue(int progress) {
        this.progress.setProgress(progress);
        if (progress == 100) {
            this.progress.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
//        partialWakeLock.acquire();

        if (webview != null) {
           /* if (WebServices.isNetworkAvailable(VideoPlay_OnlineActivity.this) && mMeterialUtility!=null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                FragmentManager fm = getSupportFragmentManager();
                CustomDialog custom = new CustomDialog();
                custom.setParamCustomDialog(VideoPlay_OnlineActivity.this,getIntent().getStringExtra("id"),true);
                custom.show(fm, "");
                webview.onPause();
            }else{*/
                webview.clearFormData();
                webview.destroy();
                finish();
//            }


//            webview.clearCache(true);

        }
        /*String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(100,redeem));
        if(!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
            Toast.makeText(this, "You have awarded "+coinsCollected+ " coins", Toast.LENGTH_SHORT).show();
        if (WebServices.isNetworkAvailable(VideoPlay_OnlineActivity.this))
            new SubmitData(VideoPlay_OnlineActivity.this, WebServices.mLoginUtility.getUser_id(), getIntent().getStringExtra("course_id"), getIntent().getStringExtra("meterial_id"), MainActivity.MODULEID,MainActivity.ASSIGNMATERIALID,"VIDEO", coinsAllocatedModel, dataBase).execute();
        else {
            new WebServices().addSubmitResponse(dataBase, getIntent().getStringExtra("course_id"), getIntent().getStringExtra("meterial_id"), "VIDEO", "100", "", "", "", getIntent().getStringExtra("title"), "",coinsCollected , redeem,MainActivity.MODULEID,MainActivity.ASSIGNMATERIALID);
            if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    WebServices.updateTotalCoins(VideoPlay_OnlineActivity.this, coinsCollected);

            }
        }*/
//        HomeActivity.QUIZSTARTTIME="";
        finish();
    }


    // Called implicitly when device is about to sleep or application is backgrounded
    protected void onPause() {
        super.onPause();
    }

    // Called implicitly when device is about to wake up or foregrounded
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(VideoPlay_OnlineActivity.this);

    }

    public static String convertSecondsToHMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        return String.format("%02d:%02d", m, s);
    }

    private void startTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (TimerVal) {
                    try {
                        Thread.sleep(1000);
                        handler2.post(new Runnable() {

                            @Override
                            public void run() {
                                if (TIMERVALUE != 0) {
//                                    time_txt.setText(convertSecondsToHMmSs(--TIMERVALUE));
                                } else {

                                    TimerVal = false;
                                    final Dialog dialog = new Dialog(VideoPlay_OnlineActivity.this);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setCancelable(false);
                                    dialog.setContentView(R.layout.logout_dialog);
                                    TextView title_dialog = (TextView) dialog.findViewById(R.id.title_dialog);
                                    TextView desc_dialog = (TextView) dialog.findViewById(R.id.desc_dialog);
                                    title_dialog.setText("Slow Connection!");
                                    desc_dialog.setText("It is taken longer than expected. Do you want to continue?");
                                    Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
                                    Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
                                    final MixPanelManager mixPanelManager = MixPanelManager.getInstance();
                                    cancel_btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mixPanelManager.onVideoPlay(VideoPlay_OnlineActivity.this, WebServices.mLoginUtility.getEmail(), getIntent().getStringExtra("title"), "Cancel");
                                            finish();
                                        }
                                    });
                                    ok_btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mixPanelManager.onVideoPlay(VideoPlay_OnlineActivity.this, WebServices.mLoginUtility.getEmail(), getIntent().getStringExtra("title"), "OK");
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.show();

                                }

                            }
                        });
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        }).start();
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();


    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnectedFast(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && isConnectionFast(info.getType(), info.getSubtype()));
    }

    /**
     * Check if the connection is fast
     *
     * @param type
     * @param subType
     * @return
     */
    public static boolean isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return true;
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps
            /*
             * Above API level 7, make sure to set android:targetSdkVersion
             * to appropriate level to use these
             */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return true;
            }
        } else {
            return false;
        }
    }

}
