package com.chaptervitamins.play_video;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.CustomView.Custom_Progress;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.NewContentInShortsActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlashCardUtility;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.droidsonroids.gif.GifTextView;


public class FlashCardResult extends BaseActivity implements View.OnClickListener, RatingListener {
    @BindView(R.id.your_score_txt)
    TextView your_score_txt;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.revise_all_button)
    Button revise_all_button;
    @BindView(R.id.reviseredonly_btn)
    Button reviseredonly_btn;
    @BindView(R.id.grid_view)
    GridView grid_view;
    @BindView(R.id.donut_progress)
    Custom_Progress donutProgress;
    @BindView(R.id.tvname)
    TextView tvname;
    int TotalMarks = 0;
    int UserTotalMarks = 0, seenCount = 1;
    CustomGridAdapter gridAdapter;
    private boolean isErrorResult = false;
    DataBase dataBase;
    public static ArrayList<FlashCardUtility> flashCardUtilities = new ArrayList<>();
    int corrs_ques = 0, incorres_ques = 0;
    MixPanelManager mixPanelManager;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";
    private TextView tvAwardedCoins, tvBackToCourse;
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private LinearLayout mainLayout,reviewLayout;
    private GifTextView goldGif;
    private MeterialUtility mMeterialUtility;
    private boolean isNextButtonClicked;
    private TextView textViewTitle;

    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashcardresult);
        ButterKnife.bind(this);

        mainLayout = (LinearLayout) findViewById(R.id.main);
        reviewLayout = (LinearLayout) findViewById(R.id.reviewLayout);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        tvAwardedCoins = (TextView) findViewById(R.id.tv_coins);
        tvBackToCourse = (TextView) findViewById(R.id.tvbacktochapter);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        textViewTitle = (TextView) findViewById(R.id.toolbar_title);
        dataBase = DataBase.getInstance(FlashCardResult.this);
        tvname.setText(WebServices.mLoginUtility.getFirstname() + "!");
        TotalMarks = WebServices.flashcardMeterialUtilities.getFlashCardUtilities().size();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        flashCardUtilities = new ArrayList<>();
        mixPanelManager = APIUtility.getMixPanelManager(FlashCardResult.this);
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        seenCount = getIntent().getIntExtra("seen_count", 1);
        if (mMeterialUtility != null) {
            coinsAllocatedModel = mMeterialUtility.getCoinsAllocatedModel();
            textViewTitle.setText(mMeterialUtility.getTitle());
        }
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();
        //---------Not need review for Manthan flashcard(TEMPLATE-5)-----
        if(WebServices.flashcardMeterialUtilities.getMaterial_templete().equalsIgnoreCase("TEMPLATE-5"))
            reviewLayout.setVisibility(View.GONE);

        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(mMeterialUtility.getMaterial_id(), true);
        position = FlowingCourseUtils.getPositionOfMeterial(mMeterialUtility.getMaterial_id(), meterialUtilityArrayList);
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(FlashCardResult.this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
        getModuleData();
        setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
        mixPanelManager.completematerial(FlashCardResult.this, WebServices.mLoginUtility.getEmail(), WebServices.flashcardMeterialUtilities.getTitle(), "Flashcard", mMeterialUtility.getMaterial_id());
        for (int i = 0; i < WebServices.flashcardMeterialUtilities.getFlashCardUtilities().size(); i++) {

            if (WebServices.flashcardMeterialUtilities.getFlashCardUtilities().get(i).getUserAns() != null && !WebServices.flashcardMeterialUtilities.getFlashCardUtilities().get(i).getUserAns().equalsIgnoreCase(""))
                if (WebServices.flashcardMeterialUtilities.getFlashCardUtilities().get(i).getUserAns().equalsIgnoreCase(WebServices.flashcardMeterialUtilities.getFlashCardUtilities().get(i).getCorrect_option())) {
                    UserTotalMarks = UserTotalMarks + 1;
                    corrs_ques++;
                } else {
                    incorres_ques++;
                    flashCardUtilities.add(WebServices.flashcardMeterialUtilities.getFlashCardUtilities().get(i));
                    if (!isErrorResult) {
                        isErrorResult = true;
                    }
                }
        }
        if (!isErrorResult) {
            reviseredonly_btn.setBackgroundResource(R.drawable.rounded_gray_bg);
            reviseredonly_btn.setClickable(false);
            reviseredonly_btn.setTextColor(Color.WHITE);
        }
        gridAdapter = new CustomGridAdapter(FlashCardResult.this, WebServices.flashcardMeterialUtilities.getFlashCardUtilities());
        grid_view.setAdapter(gridAdapter);
        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (isErrorResult) {
                if (seenCount < position + 1)
                    seenCount = position + 1;
                Intent intent = new Intent(FlashCardResult.this, AfterResultFlashcard_Activity.class);
                intent.putExtra("meterial_utility", mMeterialUtility);
                intent.putExtra("seen_count", seenCount);
                intent.putExtra("position", position + "");
                intent.putExtra("total", WebServices.flashcardMeterialUtilities.getFlashCardUtilities().size());
                startActivity(intent);
                finish();
//                }
            }
        });
        int percent = (UserTotalMarks * 100) / TotalMarks;
        donutProgress.setStartingDegree(270);
        donutProgress.setProgress(percent);
        donutProgress.setFinishedStrokeWidth(10f);
        donutProgress.setUnfinishedStrokeWidth(10f);
        String coinsCollected = null;
        try {
            coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility,percent, redeem));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
            /*tvAwardedCoins.setText("You have been awarded with " + coinsCollected + " Bonus points!");
            tvAwardedCoins.setVisibility(View.VISIBLE);*/
            showGoldGif(coinsCollected);
            Toast.makeText(FlashCardResult.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
        }
        new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, seenCount + "", WebServices.flashcardMeterialUtilities.getFlashCardUtilities().size() + "", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, mMeterialUtility, percent + "", corrs_ques + "", incorres_ques + "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(FlashCardResult.this))
            new SubmitData(FlashCardResult.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, dataBase).execute();
        else {
            /*if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    WebServices.updateTotalCoins(FlashCardResult.this, coinsCollected);
            }*/
        }
        revise_all_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FlashCardResult.this, FlashCard_Activity.class);
                intent.putExtra("meterial_utility", mMeterialUtility);
                startActivity(intent);
                finish();
            }
        });

        tvBackToCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(2000);
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(FlashCardResult.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum()))
                    showRatingDialog(true);
                else
                    finish();
            }
        });
        reviseredonly_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isErrorResult) {
                    Intent intent = new Intent(FlashCardResult.this, AfterResultFlashcard_Activity.class);
                    intent.putExtra("meterial_utility", mMeterialUtility);
                    intent.putExtra("seen_count", seenCount);
                    intent.putExtra("position", "all");
                    intent.putExtra("total", WebServices.flashcardMeterialUtilities.getFlashCardUtilities().size());
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(mMeterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, mMeterialUtility.getModule_id());
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(FlashCardResult.this);
    }

    private void showGoldGif(String coinsCollected) {
        tvAwardedCoins.setText("You have been awarded with " + coinsCollected + " Bonus points!");
        tvAwardedCoins.setVisibility(View.VISIBLE);
        mainLayout.setVisibility(View.GONE);
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null) {
                    goldGif.setVisibility(View.GONE);
                    mainLayout.setVisibility(View.VISIBLE);
                }
            }
        }, 2000);
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(FlashCardResult.this, meterialUtilityArrayList, position, isNextButtonClicked);
    }


    public class CustomGridAdapter extends BaseAdapter {

        private Context context;
        ArrayList<FlashCardUtility> cardUtilities = new ArrayList<>();
        LayoutInflater inflater;

        //Constructor to initialize values
        public CustomGridAdapter(Context context, ArrayList<FlashCardUtility> utilities) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.context = context;
            cardUtilities = utilities;
        }

        @Override
        public int getCount() {

            // Number of times getView method call depends upon gridValues.length
            return cardUtilities.size();
        }

        @Override
        public Object getItem(int position) {

            return null;
        }

        @Override
        public long getItemId(int position) {

            return 0;
        }


        // Number of times getView method call depends upon gridValues.length

        public View getView(int position, View convertView, ViewGroup parent) {
            // LayoutInflator to call external grid_item.xml file
            convertView = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.grid_row, null);
                TextView textView = (TextView) convertView
                        .findViewById(R.id.count_txt);
                textView.setText((position + 1) + "");

                LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.row_ll);
                if (cardUtilities.get(position).getUserAns().equalsIgnoreCase("")) {
                    textView.setBackgroundResource(R.drawable.gray_circle);
                } else if (cardUtilities.get(position).getUserAns().equalsIgnoreCase(cardUtilities.get(position).getCorrect_option())) {
                    textView.setBackgroundResource(R.drawable.rounded_corner_green_button);
                } else {
                    textView.setBackgroundResource(R.drawable.red_circle);
                }
            }

            return convertView;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(FlashCardResult.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(FlashCardResult.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(FlashCardResult.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(FlashCardResult.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(FlashCardResult.this) &&
                        mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(FlashCardResult.this, mMeterialUtility, true);
                break;
        }
    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(FlashCardResult.this, mMeterialUtility.getMaterial_id(), wannaFinish, FlashCardResult.this);
        custom.show(fm, "");
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(FlashCardResult.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
            showRatingDialog(false);
        } else
            finish();
    }
}
