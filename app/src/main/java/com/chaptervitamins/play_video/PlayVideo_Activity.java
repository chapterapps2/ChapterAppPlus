package com.chaptervitamins.play_video;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.downloadImages.NewsFeedImageDownloader;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class PlayVideo_Activity extends BaseActivity implements View.OnClickListener, RatingListener {
    private TextView filename_txt, video_title, video_desc;
    DataBase dataBase;
    // Declare variables
    Dialog pDialog;
    ImageView youtube_img, video_img;
    // Insert your Video URL
    String VideoURL = "http://www.androidbegin.com/tutorial/AndroidCommercial.3gp";
    private View view;
    private Handler timeout;
    int count = 15;//for 40 seconds to wait for buffering after it will finish the activity
    boolean istimeout = true;
    RelativeLayout rl;
    private NewsFeedImageDownloader downloader;
    /*private JWPlayerView mPlayerView;*/
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "FALSE", noOfCoins = "";
    private RelativeLayout rlFlowingCourse;
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position;
    private TextView tvCurrentPage;
    private MixPanelManager mixPanelManager;
    private String startTime, endTime;
    private double videoPosition = 0, totalDuration = 0;
    private GifTextView goldGif;
    private MeterialUtility mMeterialUtility;
    private boolean isNextButtonClicked;
    //    private JWPlayerFragment jwPlayerFragment;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos = -1;
    private ImageView ivShare;
    private android.support.v7.widget.Toolbar toolbar;
    private LinearLayout titleLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_play_video);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        view = inflater.inflate(R.layout.activity_play_video, null);
        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        ImageView back = (ImageView) findViewById(R.id.back);
        video_img = (ImageView) findViewById(R.id.video_img);
        titleLayout = (LinearLayout) findViewById(R.id.titleLayout);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        mixPanelManager = APIUtility.getMixPanelManager(this);
//        jwPlayerFragment = (JWPlayerFragment) getFragmentManager().findFragmentById(R.id.jw_player);
//        mPlayerView = jwPlayerFragment.getPlayer();
/*
        mPlayerView = (JWPlayerView) findViewById(R.id.playerView);
*/
        ivShare = (ImageView) findViewById(R.id.iv_share);

        rlFlowingCourse = (RelativeLayout) findViewById(R.id.rl_flowing_course);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        tvCurrentPage = (TextView) findViewById(R.id.tv_current_page);
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        if (mMeterialUtility != null && !TextUtils.isEmpty(mMeterialUtility.getIsShareable()) && mMeterialUtility.getIsShareable().equalsIgnoreCase("YES"))
            ivShare.setVisibility(View.VISIBLE);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(mMeterialUtility.getMaterial_id(), true);
        position = FlowingCourseUtils.getPositionOfMeterial(mMeterialUtility.getMaterial_id(), meterialUtilityArrayList);
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && position != -1 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(PlayVideo_Activity.this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
        getModuleData();
        setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.shareMaterial(PlayVideo_Activity.this, mMeterialUtility, mixPanelManager);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(PlayVideo_Activity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    submitDataToServer();
                    //showRatingDialog(true);
                } else {
                    onBackPressed();
                }

            }
        });
        // Find your VideoView in your video_main.xml layout
        youtube_img = (ImageView) findViewById(R.id.youtube_img);
//        video_title = (TextView) findViewById(R.id.video_title);
        video_desc = (TextView) findViewById(R.id.video_desc);
//        video_title.setText(Html.fromHtml(mMeterialUtility.getTitle()));
        toolbar_title.setText(Html.fromHtml(mMeterialUtility.getTitle()));
        video_desc.setText(Html.fromHtml((mMeterialUtility.getDescription())));
        dataBase = DataBase.getInstance(PlayVideo_Activity.this);
        // Execute StreamVideo AsyncTask
        downloader = new NewsFeedImageDownloader(PlayVideo_Activity.this);
       /* if (!TextUtils.isEmpty(mMeterialUtility.getMaterial_media_file_url())) {
            video_img.setVisibility(View.GONE);
            downloader.DisplayImage(mMeterialUtility.getMaterial_image(), video_img);
        }
        else*/
        video_img.setVisibility(View.GONE);

        youtube_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                youtube_img.setVisibility(View.GONE);
                playVideo();
            }
        });

        /*rl = (RelativeLayout) findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playVideo();
            }
        });*/
    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(mMeterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, mMeterialUtility.getModule_id());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            /*if(mPlayerView.getFullscreen()) {
                mPlayerView.setFullscreen(false, true);
            }*/
            toolbar.setVisibility(View.VISIBLE);
            titleLayout.setVisibility(View.VISIBLE);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            /*if(!mPlayerView.getFullscreen()) {
                mPlayerView.setFullscreen(true, true);
            }*/
            toolbar.setVisibility(View.GONE);
            titleLayout.setVisibility(View.GONE);
        }
    }

    private void playVideo() {
        if (mMeterialUtility.getMaterial_id() != null) {
//            Calendar c = Calendar.getInstance();
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            if (mMeterialUtility != null)
//                mMeterialUtility.setMaterialStartTime(df.format(c.getTime()));
            File decodefile = new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + mMeterialUtility.getMaterial_id());
            File decodefile2 = new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + mMeterialUtility.getMaterial_id() + ".mp4");
            if (decodefile2.exists()) {
                String filepath = decodefile2.getAbsolutePath();
                if (!TextUtils.isEmpty(filepath)) {
                    File f = new File(filepath);
                    if (f.exists()) {
                        startVideoJwPlayer(filepath);
                    }
                } else {
                    if (WebServices.isNetworkAvailable(PlayVideo_Activity.this)) {
                        filepath = mMeterialUtility.getMaterial_media_file_url();
                        startVideoJwPlayer(filepath);
                    } else {
                        Toast.makeText(PlayVideo_Activity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    }
                }
            } else if (decodefile.exists()) {
                String filepath = "";
                try {
                    File sdcard = new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());
                    File from = new File(sdcard, mMeterialUtility.getMaterial_id());
                    File to = new File(sdcard, mMeterialUtility.getMaterial_id() + ".mp4");
                    boolean remane = from.renameTo(to);
                    filepath = getFileFromURL(PlayVideo_Activity.this, mMeterialUtility.getMaterial_id() + ".mp4").getAbsolutePath();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!TextUtils.isEmpty(filepath)) {
                    File f = new File(filepath);
                    if (f.exists()) {
                        startVideoJwPlayer(filepath);
                    }
                } else {
                    if (WebServices.isNetworkAvailable(PlayVideo_Activity.this)) {
                        filepath = mMeterialUtility.getMaterial_media_file_url();
                        startVideoJwPlayer(filepath);
                    } else {
                        Toast.makeText(PlayVideo_Activity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    }
                }
            } else if (!mMeterialUtility.getMaterial_media_file_type().equalsIgnoreCase("URL")) {
                if (WebServices.isNetworkAvailable(PlayVideo_Activity.this)) {
                    String filepath = mMeterialUtility.getMaterial_media_file_url();
                    startVideoJwPlayer(filepath);

                } else {
                    Toast.makeText(PlayVideo_Activity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            } else if (mMeterialUtility.getMaterial_media_file_type().equalsIgnoreCase("URL")) {
                if (WebServices.isNetworkAvailable(PlayVideo_Activity.this)) {
                    String filepath = mMeterialUtility.getMaterial_media_file_url();
                    startVideoJwPlayer(filepath);

                } else {
                    Toast.makeText(PlayVideo_Activity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            } else if (WebServices.isNetworkAvailable(PlayVideo_Activity.this)) {
                callOnlinePlayActivity();

            } else {
                Toast.makeText(PlayVideo_Activity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
            }
        }
        coinsAllocatedModel = mMeterialUtility.getCoinsAllocatedModel();
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();

        showGoldGif();
    }

    private void startVideoJwPlayer(String filePath) {
       /* PlayerConfig playerConfig = new PlayerConfig.Builder()
                .file(filePath).autostart(true)
                .build();
        mPlayerView = new JWPlayerView(PlayVideo_Activity.this, playerConfig);
        mPlayerView.setFullscreen(false, false);*/
        /*final PlaylistItem video = new PlaylistItem(filePath);
        mPlayerView.load(video);*/

        /*mPlayerView.addOnErrorListener(new OnErrorListenerV2() {
            @Override
            public void onError(ErrorEvent errorEvent) {
                if (errorEvent.getMessage().equalsIgnoreCase("Invalid HTTP response code: 404: Not Found")) {
                    mPlayerView.load(video);
                } else
                    callOnlinePlayActivity();
            }
        });*/


//        RelativeLayout container = (RelativeLayout) findViewById(R.id.rl);
//        container.addView(mPlayerView, new RelativeLayout.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        try {
            if (!TextUtils.isEmpty(mMeterialUtility.getSeen_count()) && !mMeterialUtility.getComplete_per().equals("100")) {
                //mPlayerView.seek(Long.parseLong(mMeterialUtility.getSeen_count()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callOnlinePlayActivity() {
        if (WebServices.isNetworkAvailable(PlayVideo_Activity.this)) {
            videoPosition = 1;
            totalDuration = 1;
            submitDataToServer();
            Intent intent = new Intent(PlayVideo_Activity.this, VideoPlay_OnlineActivity.class);
            if (mMeterialUtility != null)
                intent.putExtra("meterial_utility", mMeterialUtility);
            intent.putExtra("file_url", mMeterialUtility.getMaterial_media_file_url());
            intent.putExtra("isSaved", false);
            startActivity(intent);
            finish();
        }
    }


    public static File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());

        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }


    private void submitDataToServer() {
        //sendMixPanelTrackData();
        final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility, 100, redeem));
        if (videoPosition == 0) {
            if (!TextUtils.isEmpty(mMeterialUtility.getSeen_count()))
                videoPosition = (long) Double.parseDouble(mMeterialUtility.getSeen_count());
            if (!TextUtils.isEmpty(mMeterialUtility.getTotal_count()))
                totalDuration = (long) Double.parseDouble(mMeterialUtility.getTotal_count());
        }
        new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, videoPosition + "", totalDuration + "", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, mMeterialUtility, mMeterialUtility.getComplete_per(), "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(PlayVideo_Activity.this))
            new SubmitData(PlayVideo_Activity.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, dataBase).execute();
        else {
           /* if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    WebServices.updateTotalCoins(PlayVideo_Activity.this, coinsCollected);

            }*/
        }
    }

    private void showGoldGif() {
        final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility, 100, redeem));
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    goldGif.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (goldGif != null)
                                goldGif.setVisibility(View.GONE);
                        }
                    }, 2000);
                    Toast.makeText(PlayVideo_Activity.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(PlayVideo_Activity.this);
        try {
            /*if (mPlayerView != null) {
                mPlayerView.setBackgroundAudio(true);
                mPlayerView.onResume();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        /*if (mPlayerView != null) {
            // Allow background audio playback.
            try {
                mPlayerView.setBackgroundAudio(false);
                mPlayerView.onPause();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        super.onPause();
    }


   /* @Override
    public void onBackPressed() {

        submitDataToServer();
        try {*/
            /*if (mPlayerView != null) {
                if (mPlayerView.getFullscreen()) {
                    mPlayerView.setFullscreen(false, false);
                } else {
//                    submitDataToServer();
                    super.onBackPressed();
                }

            } else {
//                submitDataToServer();
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

/*
    @Override

        protected void onDestroy() {
        try {
            if (mPlayerView != null)
                mPlayerView.onDestroy();
           */
/* File sdcard = new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());
            File from = new File(sdcard, mMeterialUtility.getMaterial_id());
            File to = new File(sdcard, mMeterialUtility.getMaterial_id() + ".mp4");
            boolean remane = to.renameTo(from);*//*

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
*/

    @Override
    public void onClick(View view) {
        submitDataToServer();
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(PlayVideo_Activity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    //showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(PlayVideo_Activity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(PlayVideo_Activity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    //showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(PlayVideo_Activity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && !TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(PlayVideo_Activity.this) &&
                        mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    //showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(PlayVideo_Activity.this, mMeterialUtility, true);
                break;
        }

    }

    /*private void sendMixPanelTrackData() {
        if (mPlayerView != null) {
            if (videoPosition != 1 && totalDuration != 1) {
                videoPosition = mPlayerView.getPosition();
                totalDuration = mPlayerView.getDuration();
            }
        }
        endTime = DateFormat.getDateTimeInstance().format(new Date());
        mixPanelManager.selectPDFPageCountTrack(PlayVideo_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                , mMeterialUtility.getTitle(), "Video", String.valueOf(videoPosition), String.valueOf(totalDuration));

    }

    private void showRatingDialog(boolean wannaFinish) {
        if (mPlayerView != null)
            mPlayerView.pause();
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(PlayVideo_Activity.this, mMeterialUtility.getMaterial_id(), wannaFinish, PlayVideo_Activity.this);
        custom.show(fm, "");
    }*/

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(PlayVideo_Activity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }
}


