package com.chaptervitamins.play_video;

import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.downloadImages.NewsFeedImageDownloader;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class Audio_Player_Activity extends BaseActivity implements View.OnClickListener, RatingListener {
    private TextView audio_title, audio_desc;

    private int mediaFileLengthInMilliseconds; // this value contains the song duration in milliseconds. Look at getDuration() method in MediaPlayer class

    SeekBar seekBarProgress;
    // Declare variables
    ImageView back, play, video_img;
    // Insert your Video URL
    String AudioURL = "http://www.androidbegin.com/tutorial/AndroidCommercial.3gp";
    private final Handler handler = new Handler();

    private MediaPlayer mediaPlayer;
    private Dialog mDialog;
    private ProgressBar mBar;
    private TextView totalPer_txt, title_txt, total_file_txt, audio_name_txt, tvStartTime, tvEndTime;
    private Button abort_btn = null;
    private boolean isDownload = true;
    String MSG = "Network issues.. You have been Timed-out";
    private String FILEPATH = "";
    private int totallenghtOfFile = 0;
    private String totalVal = "-1";
    DataBase dataBase;
    boolean isPlay = false;
    private NewsFeedImageDownloader downloader;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private TextView toolbarTitle;
    private MeterialUtility meterialUtility;
    private int position = -1;
    private MixPanelManager mixPanelManager;
    private String startTime, endTime;
    public long currentTime = 0, totalAudioTime = 0;
    private GifTextView goldGif;
    private boolean isNextButtonClicked;
    private LinearLayout llDesc;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            setContentView(R.layout.activity_audio__player);
//        view = inflater.inflate(R.layout.activity_audio__player, null);
            audio_desc = (TextView) findViewById(R.id.audio_desc);
            toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
            tvStartTime = (TextView) findViewById(R.id.tv_start_time);
            llDesc = (LinearLayout) findViewById(R.id.ll_desc);
            tvEndTime = (TextView) findViewById(R.id.tv_end_time);
            goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
            ImageView back = (ImageView) findViewById(R.id.back);
            btnPrevious = (Button) findViewById(R.id.btn_prev);
            btnNext = (Button) findViewById(R.id.btn_next);
            startTime = DateFormat.getDateTimeInstance().format(new Date());
            mixPanelManager = APIUtility.getMixPanelManager(this);
            video_img = (ImageView) findViewById(R.id.video_img);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    trackAudioTime();
                    submitDataToServer();
                    if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(Audio_Player_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                        showRatingDialog(true);

                    } else {
                        finish();
                    }
                }
            });
//        filename_txt = (TextView) findViewById(R.id.filename_txt);
            audio_title = (TextView) findViewById(R.id.audio_title);
            audio_name_txt = (TextView) findViewById(R.id.audio_name_txt);
            audio_name_txt.setText("AUDIO");
            play = (ImageView) findViewById(R.id.play);
            seekBarProgress = (SeekBar) findViewById(R.id.seekBar);
            seekBarProgress.setMax(100); // It means 100% .0-99
            getDataFromBundle();

//
            setFlowingCourseData();
            getModuleData();
            setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
            mediaPlayer = new MediaPlayer();

            downloader = new NewsFeedImageDownloader(Audio_Player_Activity.this);
            if (!TextUtils.isEmpty(meterialUtility.getMaterial_media_file_url()))
                downloader.DisplayImage(meterialUtility.getMaterial_image(), video_img);
            dataBase = DataBase.getInstance(Audio_Player_Activity.this);
            mediaPlayer.reset();
            File decodefile = null;
            if (!TextUtils.isEmpty(FILEPATH))
                decodefile = new File(FILEPATH);
            if (decodefile != null && decodefile.exists()) {
                openAudioFile(decodefile.getAbsolutePath(), meterialUtility.getMaterial_id(), meterialUtility.getCourse_id());
            } else if (meterialUtility.getIs_offline_available().equalsIgnoreCase("no") && !TextUtils.isEmpty(meterialUtility.getMaterial_media_file_url()))
                openAudioFile(meterialUtility.getMaterial_media_file_url(), meterialUtility.getMaterial_id(), meterialUtility.getCourse_id());
            else
                DownloadAudioFile(meterialUtility.getMaterial_media_file_url(), meterialUtility.getMaterial_id(), meterialUtility.getCourse_id());
            seekBarProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                    seekBar.setProgress(progress);
//                    tvStartTime.setText(Utils.diffDate(mediaPlayer.getCurrentPosition() / 1000));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mediaPlayer.isPlaying()) {
                        int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * seekBar.getProgress();
                        mediaPlayer.seekTo(playPositionInMillisecconds);
                    }
                }
            });
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Calendar c = Calendar.getInstance();
//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    String formattedDate = df.format(c.getTime());
//                    meterialUtility.setMaterialStartTime(formattedDate);
                    if (!isPlay) {
                        isPlay = true;
                        mediaPlayer.start();
                        try {

                            if (currentTime < mediaPlayer.getCurrentPosition())
                                currentTime = mediaPlayer.getCurrentPosition();

                            play.setImageResource(R.drawable.button_pause);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        submitDataToServer();
                    } else {
                        if (currentTime < mediaPlayer.getCurrentPosition())
                            currentTime = mediaPlayer.getCurrentPosition();
                        isPlay = false;
                        mediaPlayer.pause();
                        play.setImageResource(R.drawable.button_play);
                    }
                    primarySeekBarProgressUpdater();
                }
            });

            mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    seekBarProgress.setSecondaryProgress(percent);
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    play.setImageResource(R.drawable.button_play);
                    seekBarProgress.setSecondaryProgress(0);
                    seekBarProgress.setProgress(0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(meterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, meterialUtility.getModule_id());
    }

    private void trackAudioTime() {
        currentTime = mediaPlayer.getCurrentPosition();
        totalAudioTime = mediaPlayer.getDuration();
        endTime = DateFormat.getDateTimeInstance().format(new Date());
        mixPanelManager.selectPDFPageCountTrack(Audio_Player_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                , meterialUtility.getDescription(), "Audio", String.valueOf(currentTime), String.valueOf(totalAudioTime));
    }

    private void getDataFromBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_utility");
            if (meterialUtility != null) {
                audio_title.setText(Html.fromHtml(meterialUtility.getDescription()));
                if (!TextUtils.isEmpty(meterialUtility.getDescription()))
                    toolbarTitle.setText(meterialUtility.getTitle());
                else
                    llDesc.setVisibility(View.GONE);
            }
            FILEPATH = bundle.getString("file_path");
        }


    }

    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }

    private void setFlowingCourseData() {
        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(meterialUtility.getCourse_id(), meterialUtility.getModule_id());
        position = FlowingCourseUtils.getPositionOfMeterial(meterialUtility.getMaterial_id(), meterialUtilityArrayList);
        if (meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && position != -1 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(Audio_Player_Activity.this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }

    private void submitDataToServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
               /* coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getCourse_id(), meterialUtility.getModule_id(), meterialUtility.getMaterial_id());
                if (coinsAllocatedModel == null) {
                    coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getMaterial_id());
                }
                if (coinsAllocatedModel != null) {
                    redeem = coinsAllocatedModel.getRedeem();
                    noOfCoins = coinsAllocatedModel.getMaxCoins();
                } else
                    coinsAllocatedModel = new CoinsAllocatedModel();*/
                coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
                if (coinsAllocatedModel != null) {
                    redeem = coinsAllocatedModel.getRedeem();
                    noOfCoins = coinsAllocatedModel.getMaxCoins();
                } else
                    coinsAllocatedModel = new CoinsAllocatedModel();

                if (!TextUtils.isEmpty(meterialUtility.getMaterialStartTime())) {
                    final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility,100, redeem));
                    if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showGoldGif();
                                Toast.makeText(Audio_Player_Activity.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    new WebServices().setProgressOfMaterial(dataBase, meterialUtility, currentTime + "", totalAudioTime + "", coinsCollected);
                    new WebServices().addSubmitResponse(dataBase, meterialUtility, "0", "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

                    if (WebServices.isNetworkAvailable(Audio_Player_Activity.this)) {
                        new SubmitData(Audio_Player_Activity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, dataBase).execute();
                    }
                }
            }
        }).start();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            trackAudioTime();
            submitDataToServer();
            this.finish();
            mediaPlayer.release();
            mediaPlayer.stop();
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();

        mediaPlayer.stop();
    }

    @Override
    public void onStop() {
        super.onStop();
        mediaPlayer.stop();
    }

    @Override
    public void onBackPressed() {
        trackAudioTime();
        submitDataToServer();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(Audio_Player_Activity.this);
    }

    private void openAudioFile(String audiofile, String material_id, String ASSIGNEDCOURSEID) {

        /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
        try {
            mediaPlayer.setDataSource(audiofile); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
            mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            if (meterialUtility != null && !TextUtils.isEmpty(meterialUtility.getSeen_count()) && !meterialUtility.getSeen_count().equalsIgnoreCase("null")) {
                Integer resumeTime = Integer.parseInt(meterialUtility.getSeen_count());
                if (resumeTime <= mediaPlayer.getDuration())
                    mediaPlayer.seekTo(resumeTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvEndTime.setText(Utils.diffDate(mediaPlayer.getDuration() / 1000));
        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL
        tvStartTime.setText(Utils.diffDate(mediaPlayer.getCurrentPosition() / 1000));

    }

    private void primarySeekBarProgressUpdater() {
        seekBarProgress.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
        tvStartTime.setText(Utils.diffDate(mediaPlayer.getCurrentPosition() / 1000));
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }
    }

    public void DownloadAudioFile(final String DownloadUrl, final String fileName, final String course_id) {

        isDownload = true;
        //        private ProgressDialog pDialog;
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mDialog.dismiss();
                if (msg.what == 0) {
                    if (FILEPATH != null) {
                        openAudioFile(FILEPATH, fileName, course_id);

                    } else {
                        try {
                            APIUtility.isDeletedFileFromURL(Audio_Player_Activity.this, fileName);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                        AppUtility.showToast(context, MSG);

                        Toast.makeText(Audio_Player_Activity.this, MSG, Toast.LENGTH_LONG).show();
                        finish();
                    }
                } else {
                    try {
                        APIUtility.isDeletedFileFromURL(Audio_Player_Activity.this, fileName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    AppUtility.showToast(context, MSG);
                    Toast.makeText(Audio_Player_Activity.this, MSG, Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        };
        mDialog = new Dialog(Audio_Player_Activity.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDownload = false;
                FILEPATH = null;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(1);
            }
        });
        mDialog.show();
        new Thread() {
            @Override
            public void run() {
                int lenghtOfFile = 0;
                File file = null;
                try {
                    file = getFileFromURL(Audio_Player_Activity.this, fileName);

                    URL url = new URL(DownloadUrl);

                    long startTime = System.currentTimeMillis();
                    Log.d("DownloadManager", "download url:" + url);
                    Log.d("DownloadManager", "download file name:" + fileName);

                    URLConnection uconn = url.openConnection();
                    uconn.setReadTimeout(50000);
                    uconn.setConnectTimeout(50000);
                    lenghtOfFile = uconn.getContentLength();
                    InputStream is = uconn.getInputStream();
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);


                    // Output stream to write file
                    //Download epub file in app private directory /sdcard/Android/data/<package-name>/files/chapters
                    OutputStream output = new FileOutputStream(file);
                    byte data[] = new byte[1024];
                    int current = 0;
                    long total = 0;
                    while ((current = input.read(data)) != -1) {
                        output.write(data, 0, current);

                        if (isDownload) {
                            total += current;
                            totalVal = "" + (int) ((total * 100) / lenghtOfFile);
                            totallenghtOfFile = lenghtOfFile;
//                            publishProgress();
//                            runnable.notify();//run();
                            // writing data to file

                            (Audio_Player_Activity.this).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    abort_btn.setEnabled(true);
                                    if (totallenghtOfFile != -1) {
                                        String total = (totallenghtOfFile / (1024 * 1000f)) + "";
                                        String[] t = total.split("\\.");
                                        total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                    } else {
                                        total_file_txt.setText("0 MB");
                                    }
                                    title_txt.setText("Fetching... Please wait!");
                                    if (!totalVal.contains("-")) {
                                        mBar.setProgress(Integer.parseInt(totalVal));
                                        totalPer_txt.setText(Integer.valueOf(totalVal) + " %");
                                    } else {
                                        isDownload = false;
                                        MSG = "Network issues.. You have been Timed-out";
                                        mBar.setProgress(0);
                                        totalPer_txt.setText("0 %");
                                        handler.sendEmptyMessage(2);
                                    }
                                }
                            });
                        }
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();
                    Log.d("DownloadManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + "sec");

                } catch (IOException e) {
                    MSG = "Network issues.. You have been Timed-out";
                    FILEPATH = null;
                    handler.sendEmptyMessage(2);
                    return;
                }
                FILEPATH = file.getAbsolutePath();
                handler.sendEmptyMessage(0);
            }
        }.start();

    }

    public static File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    @Override
    public void onClick(View view) {
        trackAudioTime();
        submitDataToServer();
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(Audio_Player_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum()))
                    showRatingDialog(false);
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(Audio_Player_Activity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(Audio_Player_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum()))
                    showRatingDialog(false);
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(Audio_Player_Activity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(Audio_Player_Activity.this) &&
                        meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(Audio_Player_Activity.this, meterialUtility, true);
                break;
        }

    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(Audio_Player_Activity.this, meterialUtility.getMaterial_id(), wannaFinish, Audio_Player_Activity.this);
        custom.show(fm, "");
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(Audio_Player_Activity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }
}
