package com.chaptervitamins.play_video;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.chaptervitamins.utility.ReadResponseUtility;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class PDFViewerActivity extends BaseActivity implements View.OnClickListener, RatingListener {
    private TextView pagecount_txt;
    String filename = "0";
    PDFView pdfViewer;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";
    private Button btnPrevious, btnNext,btnPreviousCourse,btnNextCourse;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private MixPanelManager mixPanelManager;
    private String endTime, startTime;
    private int pageRead = 1, pageCount = 1, startPage = -1, resumePage = 1;
    private GifTextView goldGif;
    private ImageView back;
    private TextView toolbar_title;
    private MeterialUtility mMeterialUtility;
    private boolean isNextButtonClicked;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdfviewer_activity);
        back = (ImageView) findViewById(R.id.back);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMeterialUtility != null)
                    submitDataToServer(true);
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(PDFViewerActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(true);
                } else {
                    finish();
                }

            }
        });
        pdfViewer = (PDFView) findViewById(R.id.pdfview);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        pagecount_txt = (TextView) findViewById(R.id.pagecount_txt);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);

        btnNextCourse = (Button) findViewById(R.id.btn_next_course);
        btnPreviousCourse = (Button) findViewById(R.id.btn_prev_course);

        startTime = DateFormat.getDateTimeInstance().format(new Date());
        btnNext = (Button) findViewById(R.id.btn_next);
        mixPanelManager = APIUtility.getMixPanelManager(this);
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        if (mMeterialUtility != null) {
            toolbar_title.setText(mMeterialUtility.getTitle());
            coinsAllocatedModel = mMeterialUtility.getCoinsAllocatedModel();
            if (coinsAllocatedModel != null) {
                redeem = coinsAllocatedModel.getRedeem();
                noOfCoins = coinsAllocatedModel.getMaxCoins();
            } else
                coinsAllocatedModel = new CoinsAllocatedModel();

            for (int i = 0; i < HomeActivity.mReadResponse.size(); i++) {
                ReadResponseUtility readResponseUtility = HomeActivity.mReadResponse.get(i);
                if (readResponseUtility != null && readResponseUtility.getMaterial_id().equals(mMeterialUtility.getMaterial_id())) {
                    if (!TextUtils.isEmpty(readResponseUtility.getSeen_count()) && !TextUtils.isEmpty(readResponseUtility.getTotal_count())) {
                        if (!readResponseUtility.getSeen_count().equals(readResponseUtility.getTotal_count()))
                            startPage = Integer.parseInt(readResponseUtility.getSeen_count());
                    }
                }
            }
            meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(mMeterialUtility.getMaterial_id(), true);
            position = FlowingCourseUtils.getPositionOfMeterial(mMeterialUtility.getMaterial_id(), meterialUtilityArrayList);

            moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(mMeterialUtility.getModule_id());
            if (moduleUtilityList != null && moduleUtilityList.size() > 0)
                modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, mMeterialUtility.getModule_id());

        }else
            toolbar_title.setText("PDF");
        try {
            pdfViewer.fromFile(new File(getIntent().getStringExtra("filename")))
//                .pages(0)
                    .defaultPage(0)
                    .enableSwipe(true)
                    .swipeHorizontal(true)
                    .enableDoubletap(true)
                    .defaultPage(startPage - 1)
//                .onDraw(onDrawListener)
//                .onLoad(onLoadCompleteListener)
                    .onPageChange(new OnPageChangeListener() {
                        @Override
                        public void onPageChanged(int page, int pageCount) {
                            if (page + 1 >= PDFViewerActivity.this.pageRead)
                                PDFViewerActivity.this.pageRead = page + 1;
                            resumePage = page + 1;
                            PDFViewerActivity.this.pageCount = pageCount;
                            pagecount_txt.setText((page + 1) + "/" + pdfViewer.getPageCount());
                            if (page + 1 == pageCount) {
                                showGoldGif();
                                setFlowingCourse();
                                setModuleFlowingCourse(PDFViewerActivity.this,position,meterialUtilityArrayList,btnNextCourse,modulePos,moduleUtilityList);

                            } else if (page + 1 < pageCount) {
                                findViewById(R.id.rl_flowing_course).setVisibility(View.GONE);
                                findViewById(R.id.rl_module_flowing_course).setVisibility(View.GONE);
                            }
                            if (page + 1 > pageCount) {
                                pdfViewer.enableSwipe(false);
                            }
                        }
                    })
                    .onError(new OnErrorListener() {
                        @Override
                        public void onError(Throwable t) {
                            File f = new File(getIntent().getStringExtra("filename"));
                            if (f.exists()) f.delete();
                            Toast.makeText(PDFViewerActivity.this, "Error in opening the file! Please try again.", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    })
                    .load();
        } catch (Exception e) {

        }
        // setTitle();
        pagecount_txt.setText((pdfViewer.getCurrentPage() + 1) + "/" + pdfViewer.getPageCount());
        pdfViewer.useBestQuality(true);


//        pdfViewer.findViewsWithText(pdfViewer.getFocusables(View.FIND_VIEWS_WITH_TEXT),"the",true);
    }

    private void showGoldGif() {
        final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility,100, redeem));
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    goldGif.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (goldGif != null)
                                goldGif.setVisibility(View.GONE);
                        }
                    }, 2000);
                    //coinsAllocatedModel.setRedeem("TRUE");
                    Toast.makeText(PDFViewerActivity.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();

                }
            });
        }

    }


    private void setFlowingCourse() {
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(PDFViewerActivity.this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }
/*    private void setModuleFlowingCourse(){
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0&&moduleUtilityList.get(modulePos).getIs_flowing_course() ) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextModuleButtonStatus(PDFViewerActivity.this, position, meterialUtilityArrayList,modulePos,moduleUtilityList);
          //  FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPreviousCourse);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNextCourse);
          //  btnPreviousCourse.setOnClickListener(this);
            btnNextCourse.setOnClickListener(this);
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(PDFViewerActivity.this);
    }

    @Override
    public void onBackPressed() {
//       super.onBackPressed();
        if (mMeterialUtility != null)
            submitDataToServer(true);
        if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(PDFViewerActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
            showRatingDialog(true);
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            if (mMeterialUtility != null)
                submitDataToServer(true);
            finish(); // close this acgetIntenttivity and return to preview activity (if there is any)

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pdfmenu, menu);
        MenuItem gotopage = menu.findItem(R.id.gotopage);
        gotopage.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PDFViewerActivity.this);
                builder.setTitle("Go To Page");

// Set up the input
                final EditText input = new EditText(PDFViewerActivity.this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setMinEms(3);
                builder.setView(input);


// Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String s = input.getText().toString();
                        if (s.toString().equalsIgnoreCase("") || s.length() == 0) return;
                        if (s.toString().equalsIgnoreCase("") || s.length() > 3) {
                            Toast.makeText(PDFViewerActivity.this, "Invalid Page number! Please try again.", Toast.LENGTH_LONG).show();
                            return;
                        }
                        try {
                            int pos = Integer.parseInt(s.toString());
                            if (pdfViewer.getPageCount() >= pos) {
                                try {
                                    pdfViewer.jumpTo(pos - 1);
                                } catch (Exception e) {

                                }
                            } else
                                Toast.makeText(PDFViewerActivity.this, "Invalid Page number! Please try again.", Toast.LENGTH_LONG).show();
                        } catch (NumberFormatException e) {
                            Toast.makeText(PDFViewerActivity.this, "Invalid Page number! Please try again.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                return false;
            }
        });
        return true;
    }

    private void submitDataToServer(boolean wannaFinish) {
        endTime = DateFormat.getDateTimeInstance().format(new Date());
        if (mMeterialUtility != null)
            mixPanelManager.selectPDFPageCountTrack(PDFViewerActivity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                    , mMeterialUtility.getTitle(), "pdf", String.valueOf(pageRead), String.valueOf(pageCount));
        DataBase dataBase = DataBase.getInstance(PDFViewerActivity.this);
        final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility,100, redeem));
        new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, pageRead + "", pageCount + "", coinsCollected);
        if (resumePage != pageRead) {
            mMeterialUtility.setSeen_count(resumePage + "");
            new WebServices().setSeenCountOfMaterialInStaticList(mMeterialUtility);
        }
        new WebServices().addSubmitResponse(dataBase, mMeterialUtility, "100", "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(PDFViewerActivity.this))
            new SubmitData(PDFViewerActivity.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, DataBase.getInstance(PDFViewerActivity.this)).execute();

    }

    @Override
    public void onClick(View view) {
        if (mMeterialUtility != null)
            submitDataToServer(true);
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(PDFViewerActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(PDFViewerActivity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(PDFViewerActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(PDFViewerActivity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_prev_course:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(PDFViewerActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(PDFViewerActivity.this, mMeterialUtility, false);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(PDFViewerActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(PDFViewerActivity.this, mMeterialUtility, true);
                break;
        }

    }

    private void showRatingDialog(boolean wannFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(PDFViewerActivity.this, mMeterialUtility.getMaterial_id(), wannFinish, PDFViewerActivity.this);
        custom.show(fm, "");
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(PDFViewerActivity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }

}
