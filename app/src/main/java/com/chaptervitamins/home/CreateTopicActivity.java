package com.chaptervitamins.home;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.discussions.AndroidMultiPartEntity;
import com.chaptervitamins.discussions.UploadFilesActivity;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.DialogUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Android on 7/26/2016.
 */

public class CreateTopicActivity extends BaseActivity implements UploadListener {
    @BindView(R.id.forum_title_edt)
    EditText forum_title_edt;
    @BindView(R.id.forum_desc_edt)
    EditText forum_desc_edt;
    @BindView(R.id.text_count)
    TextView count_txt;
    @BindView(R.id.create_topic_txt)
    Button create_topic_txt;
    WebServices webServices;
    String MEDIAID = "";
    String TYPE = "";
    private final int REQUEST_CHOOSER = 2000;
    long imageSize = 0; // kb
    String imagePath = "";
    String UPLOAD_SERVER_URL = "";
    String imageName = "";
    Context mContext;
    private Dialog mDialog;
    ProgressDialog dialog;
    private ProgressBar mBar;
    private TextView totalPer_txt, title_txt, total_file_txt, toolbar_title;
    private Button abort_btn = null;
    private boolean isUpload = true;
    String MSG = "Network issues.. You have been Timed-out";
    private String FILEPATH = null;
    String type;
    UploadListener listener;
    private ImageView back, fab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.create_topic_activity);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        ButterKnife.bind(this);
        back = (ImageView) findViewById(R.id.back);
        fab = (ImageView) findViewById(R.id.fab);
        toolbar_title.setText("CREATE A TOPIC");
        // changeStatusBarColor();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateTopicActivity.this, UploadFilesActivity.class);
                intent.putExtra("show_pdf", false);
                startActivityForResult(intent, REQUEST_CHOOSER);
            }
        });
        webServices = new WebServices();
//        forum_title_edt = (EditText) findViewById(R.id.forum_title_edt);
//        forum_desc_edt = (EditText) findViewById(R.id.forum_desc_edt);
//        count_txt = (TextView) findViewById(R.id.text_count);
        forum_title_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int mCount = 140 - s.length();
                count_txt.setText(String.valueOf(mCount));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        create_topic_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (forum_title_edt.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(CreateTopicActivity.this, "Please enter a title!", Toast.LENGTH_LONG).show();
                    return;
                }
                if (forum_desc_edt.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(CreateTopicActivity.this, "Please enter description!", Toast.LENGTH_LONG).show();
                    return;
                }
                final InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(create_topic_txt.getWindowToken(), 0);
                if (WebServices.isNetworkAvailable(CreateTopicActivity.this))
                    if (create_topic_txt.getText().toString().equalsIgnoreCase("Create Image Topic"))
                        createTopic(forum_title_edt.getText().toString().trim(), forum_desc_edt.getText().toString().trim(), TYPE, MEDIAID);
                    else
                        createTopic(forum_title_edt.getText().toString().trim(), forum_desc_edt.getText().toString().trim(), "POST", "");
                else
                    Toast.makeText(CreateTopicActivity.this, "Your Topic will be Posted once Internet is available", Toast.LENGTH_LONG).show();

            }
        });
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.attach_menu, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        if (item.getItemId() == R.id.action_addfile) {
            Intent intent = new Intent(CreateTopicActivity.this, UploadFilesActivity.class);
            startActivityForResult(intent, REQUEST_CHOOSER);
        }

        return true;
    }


   /* private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
    }*/

    /*private void uploadTypedmsg(final String title, final String massage, final String type, final String media_id) {
        final ProgressDialog dialog = ProgressDialog.show(CreateTopicActivity.this, "", "Please wait...");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (dialog != null) dialog.dismiss();
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Toast.makeText(CreateTopicActivity.this, getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
                    DataBase.getInstance(CreateTopicActivity.this).updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putBoolean("islogin", false);
                    editor.clear();
                    editor.commit();
                    Intent intent = new Intent(CreateTopicActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("sms_url", "");
                    startActivity(intent);
                    finish();
                } else
                    finish();
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getIntent().getStringExtra("groupid")));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", title));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("media_id", media_id));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)) {
                    handler.sendEmptyMessage(1);
                    return;
                }
                HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages(resp);

                handler.sendEmptyMessage(0);
            }
        }.start();
    }
*/
    private void createTopic(final String title, final String massage, final String type, final String media_id) {
        final ProgressDialog dialog = ProgressDialog.show(CreateTopicActivity.this, "", "Creating...");
        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("group_id", getIntent().getStringExtra("groupid")));
        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("forum_description", massage));
        nameValuePair.add(new BasicNameValuePair("forum_title", title));
        nameValuePair.add(new BasicNameValuePair("forum_type", type));
        if (!TextUtils.isEmpty(media_id))
            nameValuePair.add(new BasicNameValuePair("media_id", media_id));
        new GenericApiCall(CreateTopicActivity.this, APIUtility.SUBMIT_FORUM, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                if(dialog!=null)
                    dialog.dismiss();
                if (webServices.isValid((String) result)) {
                    HomeActivity.chat_messagesUtilses.addAll(webServices.getPublicchatMessages((String) result));
                    setResult(RESULT_OK);
                    finish();
                }
            }

            @Override
            public void onError(ErrorModel error) {
                if(dialog!=null)
                    dialog.dismiss();
                DialogUtils.showDialog(CreateTopicActivity.this, error.getErrorDescription());
            }
        }).execute();
    }

    /*private void createTopic(final String title, final String massage, final String type) {
        final ProgressDialog dialog = ProgressDialog.show(CreateTopicActivity.this, "", "Creating...");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (dialog != null) dialog.dismiss();
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Toast.makeText(CreateTopicActivity.this, getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
                    DataBase.getInstance(CreateTopicActivity.this).updateLoginData(SplashActivity.mPref.getString("id",""),SplashActivity.mPref.getString("pass",""),"");
                    SharedPreferences.Editor editor=SplashActivity.mPref.edit();
                    editor.putBoolean("islogin",false);
                    editor.clear();
                    editor.commit();
                    Intent intent = new Intent(CreateTopicActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("sms_url", "");
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getIntent().getStringExtra("groupid")));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", title));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
//                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)){
                    handler.sendEmptyMessage(1);
                    return;
                }
                HomeActivity.chat_messagesUtilses.addAll(webServices.getPublicchatMessages(resp));

                handler.sendEmptyMessage(0);
            }
        }.start();
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHOOSER:
                if (resultCode == REQUEST_CHOOSER) {
                    String imagePath, imageName;
                    long imageSize;
// GET IMAGE PATH
                    imagePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    imageName = imagePath.substring(imagePath.lastIndexOf("/"));

                    imageSize = this.getFileSize(imagePath);

//                    final Uri uri = data.getData();
//
                    System.out.println("==imagePath==" + imagePath);
                    System.out.println("==imageName==" + imageName);
                    System.out.println("==imageSize==" + imageSize);
//                    this.progressDialog = this.createDialog();
//                    this.progressDialog.show();
//                    imageName=imageName.substring(1);
                    if (!new File(CreateTopicActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").exists()) {
                        new File(CreateTopicActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").mkdirs();
                    }
                    String username = WebServices.mLoginUtility.getFirstname();
                   /* if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null")||!WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
                        username= username+" "+WebServices.mLoginUtility.getLastname();*/
                    try {
                        APIUtility.copyDirectory(new File(imagePath), new File(CreateTopicActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/" + username + "_" + imageName.substring(1)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ImageUploader(CreateTopicActivity.this, imagePath, APIUtility.UPLOAD_FILE, imageName, imageSize, "IMAGE", CreateTopicActivity.this);
                } else if (resultCode == 30000) {
                    String filePath, fileName;
                    long fileSize;
// GET IMAGE PATH
                    filePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    fileName = filePath.substring(filePath.lastIndexOf("/"));

                    fileSize = this.getFileSize(filePath);
                    System.out.println("==filePath==" + filePath);
                    System.out.println("==fileName==" + fileName);
                    System.out.println("==fileSize==" + fileSize);
//                    this.progressDialog = this.createDialog();
//                    this.progressDialog.show();
                    if (!new File(CreateTopicActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").exists()) {
                        new File(CreateTopicActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").mkdirs();
                    }
                    String username = WebServices.mLoginUtility.getFirstname();
                   /* if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null")||!WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
                        username= username+" "+WebServices.mLoginUtility.getLastname();
*/
                    try {
                        APIUtility.copyDirectory(new File(filePath), new File(CreateTopicActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/" + username + "_" + fileName.substring(1)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ImageUploader(CreateTopicActivity.this, filePath, APIUtility.UPLOAD_FILE, fileName, fileSize, "PDF", CreateTopicActivity.this);
                }
                break;
        }
    }

    private long getFileSize(String imagePath) {

        long length = 0;

        try {

            File file = new File(imagePath);
            length = file.length();
            length = length / 1024;

        } catch (Exception e) {

            e.printStackTrace();
        }
        return length;
    }

    @Override
    public void error(String error) {
        System.out.println("==================image Uploaded Error===============" + error);
    }

    @Override
    public void complete(int media_id, String type, String res) {
//        showInputDialog("", type, media_id);
        System.out.println("==================image Uploaded===============" + media_id);
        create_topic_txt.setText("Create Image Topic");
        MEDIAID = String.valueOf(media_id);
        TYPE = type;
    }

    public void ImageUploader(Context mContext, String imagePath, String UPLOAD_SERVER_URL, String imageName, long imageSize, String type, UploadListener listener) {
        this.imagePath = imagePath;
        this.UPLOAD_SERVER_URL = UPLOAD_SERVER_URL;
        this.imageName = imageName;
        this.mContext = mContext;
        this.imageSize = imageSize;
        this.type = type;
        this.listener = listener;
        UploadingImage();
//        uploadFile(imagePath, UPLOAD_SERVER_URL);
    }


    protected void UploadingImage() {
        isUpload = true;
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!isFinishing() && mDialog != null)
                    mDialog.dismiss();
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(mContext,UPLOAD_SERVER_URL);
                } else if (msg.what == 0) {
                    listener.error(MSG);
                } else {
                    listener.complete(msg.what, type, "");
//                    Toast.makeText(mContext, MSG, Toast.LENGTH_LONG).show();
                }
            }
        };
        mDialog = new Dialog(CreateTopicActivity.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        abort_btn.setVisibility(View.GONE);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isUpload = false;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(0);
            }
        });
        mDialog.show();

//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                mDialog.show();
//            }
//        });
        new Thread() {
            @Override
            public void run() {
                int id = 0;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(UPLOAD_SERVER_URL);
                System.out.println("====filename12==" + UPLOAD_SERVER_URL);
                System.out.println("=====file length===" + (imageSize / (1000f)));
                try {
                    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                            new AndroidMultiPartEntity.ProgressListener() {
                                public void transferred(final long num) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            abort_btn.setEnabled(true);

                                            if (imageSize != -1) {
                                                mBar.setProgress((int) ((((num / 1024))) / imageSize) * 100);
                                                totalPer_txt.setText((int) ((((num / 1024)) / imageSize) * 100) + "%");
                                                String total = ((imageSize / 1000f)) + "";
                                                String[] t = total.split("\\.");
                                                total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                            } else {
                                                total_file_txt.setText("0 MB");
                                            }
                                        }
                                    });

                                }

                                public void onProgress(int progress) {
                                    // TODO Auto-generated method stub

                                }
                            });
                    File sourceFile = new File(imagePath);
                    // boolean b= sourceFile.renameTo(new File(toFullPath));
                    // if(b){
                    // System.out.println("===imageName1"+"got the sucess");
                    // }
                    System.out.println("=====filename==" + imagePath);
                    FileBody mFileBody = new FileBody(sourceFile);

                    imageName = imageName.substring(1);
                    System.out.println("=====filename1==" + imageName);
                    FormBodyPart mFormBodyPart = new FormBodyPart(
                            "file", mFileBody) {
                        @Override
                        protected void generateContentDisp(ContentBody body) {
                            StringBuilder buffer = new StringBuilder();
                            buffer.append("form-data; name=\"");
                            buffer.append("file");
                            buffer.append("\"");
                            buffer.append("; filename=\"" + imagePath
                                    + "\"");
                            addField(MIME.CONTENT_DISPOSITION,
                                    buffer.toString());
                        }
                    };
                    entity.addPart("file_name", new StringBody(imageName));
                    entity.addPart("type", new StringBody(type));
                    entity.addPart("api_key", new StringBody(APIUtility.APIKEY));
                    entity.addPart("device_type", new StringBody(APIUtility.DEVICE));
                    entity.addPart("session_token", new StringBody(APIUtility.SESSION));
                    entity.addPart("push_tokenn", new StringBody(APIUtility.DEVICE));
                    entity.addPart("device_uuid", new StringBody(APIUtility.DEVICEID));
                    entity.addPart("uploaded_by", new StringBody(WebServices.mLoginUtility.getUser_id()));
                    entity.addPart(mFormBodyPart);
//                    entity.addPart("file",new FileBody(sourceFile));

                    // entity.addPart("uploadedFile", new FileBody(sourceFile));
//            totalSize = entity.getContentLength();
                    httppost.setEntity(entity);

                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 200) {

                        // Server response
                        MSG = EntityUtils.toString(r_entity);
//                        MSG = MSG.substring(MSG.lastIndexOf(")"));
//                        MSG = MSG.substring(1);
                        if (webServices.isValid(MSG)) {
                            handler.sendEmptyMessage(1);
                            return;
                        }
                        try {
                            JSONObject object = new JSONObject(MSG);
                            id = object.getJSONObject("data").getInt("media_id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        MSG = "Error occurred!";
                        handler.sendEmptyMessage(0);
                    }
                    System.out.println("===resp===" + MSG.toString());
                } catch (ClientProtocolException e) {
                    handler.sendEmptyMessage(0);
//                MSG = e.toString();
                } catch (IOException e) {
//                MSG = e.toString();
                    handler.sendEmptyMessage(0);
                }
                handler.sendEmptyMessage(id);
            }
        }.start();

    }

}
