package com.chaptervitamins.home;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Android on 7/21/2016.
 */
public class ShowImageActivity extends BaseActivity {
    private ImageView imageView;
    private Bitmap imageBitmap;
    private WebServices webServices;
    private TextView toolbar_title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.showimage_activity);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
//        changeStatusBarColor();
        toolbar_title.setText("Discussions");
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        webServices = new WebServices();
        imageView = (ImageView) findViewById(R.id.imageView);
        if (!getIntent().getStringExtra("imguri").equalsIgnoreCase("")) {

            File file = new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/");
            if (!file.exists()) {
                file.mkdirs();
            }
            File f = new File(file.getAbsolutePath() + "/" + getIntent().getStringExtra("imguri"));

            if (f.exists()) {
                // bimatp factory
                BitmapFactory.Options options = new BitmapFactory.Options();

                // downsizing image as it throws OutOfMemory Exception for larger
                // images
                options.inSampleSize = 8;

                final Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                        options);
//                Bitmap myBitmap = decodeSampledBitmapFromFile(f.getAbsolutePath(), 1000, 700);
                imageView.setImageBitmap(bitmap);
                return;
            }
        } else if (!getIntent().getStringExtra("imgurl").equalsIgnoreCase("")) {
            final ProgressDialog dialog = ProgressDialog.show(ShowImageActivity.this, "", "Please wait...");
            dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (dialog != null) dialog.dismiss();
                    if (imageBitmap != null) {
                        imageView.setImageBitmap(imageBitmap);
                    }
                    if (imageBitmap != null) {
//                            if (!getIntent().getStringExtra("userid").equalsIgnoreCase(WebServices.mLoginUtility.getUser_id())) {
                        String username = WebServices.mLoginUtility.getFirstname();
//                                if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null") &&!WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                                    username = username + " " + WebServices.mLoginUtility.getLastname();
                        String f_name = getIntent().getStringExtra("imgurl").substring(getIntent().getStringExtra("imgurl").lastIndexOf("/")).substring(1);
                        File filepath = null;
                        try {
                            filepath = APIUtility.getFileFromURL(ShowImageActivity.this, username + "_" + f_name);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        OutputStream output;
                        // Create a new folder in SD Card


                        try {

                            output = new FileOutputStream(filepath);

                            // Compress into png format image from 0% - 100%
                            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                            output.flush();
                            output.close();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {

                    try {

                        imageBitmap = getBitmapFromURL(getIntent().getStringExtra("imgurl"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                        imageBitmap = webServices.getImage(getIntent().getStringExtra("imgurl"));
                    handler.sendEmptyMessage(0);
                }
            }.start();

        }
    }

    public Bitmap getBitmapFromURL(String link) {


        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("getBmpFromUrl error: ", e.getMessage().toString());
            return null;
        }
    }
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                getBitmapfromUrl(getIntent().getStringExtra("imgurl"));
            }
        }).start();
//        new
//        getBitmapfromUrl(getIntent().getStringExtra("imgurl"));

        Picasso picasso = Picasso.with(this);
        picasso.setLoggingEnabled(true);*/
        /*picasso.load(getIntent().getStringExtra("imgurl"))
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
               *//* Save the bitmap or do something with it here *//*

                        // Set it in the ImageView
                        imageView.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        imageView.setImageResource(R.drawable.splash_logo);
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        imageView.setImageResource(R.drawable.splash_logo);
                    }
                });*/


    /*
   *To get a Bitmap image from the URL received
   * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            final Bitmap bitmap = BitmapFactory.decodeStream(input);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imageView.setImageBitmap(bitmap);
                }
            });

            return bitmap;

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }

        return true;
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }
}
