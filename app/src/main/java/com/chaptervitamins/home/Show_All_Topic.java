package com.chaptervitamins.home;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.discussions.ImageUploader;
import com.chaptervitamins.discussions.UploadFilesActivity;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.downloadImages.Profile_ImageLoader;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.Chat_MessagesUtils;
import com.chaptervitamins.utility.CommentsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Android on 12/20/2016.
 */

public class Show_All_Topic extends Fragment implements UploadListener {
    private WebServices webServices;
    private LayoutInflater mInflater;
    private Profile_ImageLoader imageLoader;
    private ListView chat_list;
    static ChatAdapter chatAdapter;
    private String Group_Id = "";
    private String COURSE_NAME = "";
    private final int REQUEST_CHOOSER = 2000;
    private final int REQUEST_CREATETOPIC = 1000;
    private ImageView attached_img, send_img;
    private EditText editText, search_edt;
    private LinearLayout chat_ll, chat_blank_ll,search_ll;
    ArrayList<Chat_MessagesUtils> tempchat_messagesUtilses = new ArrayList<>();

    ArrayList<Chat_MessagesUtils> chat_messagesUtilses2 = new ArrayList<>();
    public static ArrayList<CommentsUtils> commentsUtilsArrayList = new ArrayList<>();
    ProgressDialog progressDialog;
    String FILE_PATH = "";
    Toolbar toolbar;
    TextView toolbar_title;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.chat_activity, container, false);
        toolbar=(Toolbar)view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        webServices = new WebServices();
        imageLoader = new Profile_ImageLoader(getActivity());
        mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        chat_list = (ListView) view.findViewById(R.id.chat_list);

        chat_ll = (LinearLayout) view.findViewById(R.id.chat_ll);
        search_ll = (LinearLayout) view.findViewById(R.id.search_ll);

        chat_blank_ll = (LinearLayout) view.findViewById(R.id.chat_blank_ll);
        chat_blank_ll.setVisibility(View.GONE);
        search_edt = (EditText) view.findViewById(R.id.search_edt);
        chat_ll.setVisibility(View.GONE);
        chatAdapter = new ChatAdapter();
//        chat_list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chat_list.setAdapter(chatAdapter);
//        for (int i = 0; i < WebServices.groupUtilsArrayList.size(); i++) {
//            if (WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("super")) {
//                setGroup_Id(WebServices.groupUtilsArrayList.get(i).getGroup_id());
//                break;
//            }
//        }
        try {
            if (WebServices.isNetworkAvailable(getActivity())) {
                final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Please wait...");
                dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (dialog != null) dialog.dismiss();
                        if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                            Utils.callInvalidSession(getActivity(),APIUtility.GETPOSTS);
                        } else {
                            if (HomeActivity.chat_messagesUtilses.size() == 0) {
                                Toast.makeText(getActivity(), "Currently, there are no active Topics.", Toast.LENGTH_LONG).show();
                            }
                            for (int i = 0; i < HomeActivity.chat_messagesUtilses.size(); i++) {
                                tempchat_messagesUtilses.add(HomeActivity.chat_messagesUtilses.get(i));
                            }
                            HomeActivity.chat_messagesUtilses = tempchat_messagesUtilses;
                            chat_messagesUtilses2.addAll(HomeActivity.chat_messagesUtilses);
                            chatAdapter.notifyDataSetChanged();
                        }
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
                        nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                        String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
                        Log.d(" Response:", resp.toString());
                        if (!webServices.isValid(resp)) {
                            handler.sendEmptyMessage(1);
                            return;
                        }
                        HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages(resp);
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
            }
//        chat_list.setSelection(chatAdapter.getCount() - 1);

//        chatAdapter.registerDataSetObserver(new DataSetObserver() {
//            @Override
//            public void onChanged() {
//                super.onChanged();
//                chat_list.setSelection(chatAdapter.getCount() - 1);
//            }
//        });
            attached_img = (ImageView) view.findViewById(R.id.attached_img);
            send_img = (ImageView) view.findViewById(R.id.send_img);
            editText = (EditText) view.findViewById(R.id.editText);
            editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            attached_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), UploadFilesActivity.class);
                    startActivityForResult(intent, REQUEST_CHOOSER);
//                Intent getContentIntent = FileUtils.createGetContentIntent();
//
//                Intent intent = Intent.createChooser(getContentIntent, "Select a file");
//                startActivityForResult(intent, REQUEST_CHOOSER);
                }
            });
            send_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editText.getText().toString().trim().equalsIgnoreCase("")) return;
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(send_img.getWindowToken(), 0);
                    if (WebServices.isNetworkAvailable(getActivity()))
                        uploadmsg(editText.getText().toString().trim(), "POST");
                    else
                        Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();

                }
            });
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//                search_edt.setText("");
//                fetchTimelineAsync(0);
//            }
//        });
//        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);

            chat_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    commentsUtilsArrayList = HomeActivity.chat_messagesUtilses.get(position).getCommentsUtilsArrayList();
                    String user_name = "";
                    if (!HomeActivity.chat_messagesUtilses.get(position).getLastname().equalsIgnoreCase("null") && HomeActivity.chat_messagesUtilses.get(position).getLastname() != null)
                        user_name = (HomeActivity.chat_messagesUtilses.get(position).getFirstname() + " " + HomeActivity.chat_messagesUtilses.get(position).getLastname());
                    else
                        user_name = (HomeActivity.chat_messagesUtilses.get(position).getFirstname());
                    Intent intent = new Intent(getActivity(), TopicDiscussion.class);
                    intent.putExtra("name", getCOURSE_NAME());
                    intent.putExtra("user_name", user_name);
                    intent.putExtra("pos", position);
                    intent.putExtra("groupid", getGroup_Id());
                    intent.putExtra("userid", HomeActivity.chat_messagesUtilses.get(position).getUser_id());
                    intent.putExtra("title", HomeActivity.chat_messagesUtilses.get(position).getForum_title());
                    intent.putExtra("msg", HomeActivity.chat_messagesUtilses.get(position).getForum_description());
                    intent.putExtra("img", HomeActivity.chat_messagesUtilses.get(position).getPhoto());
                    intent.putExtra("time", HomeActivity.chat_messagesUtilses.get(position).getAdded_on());
                    intent.putExtra("forum_id", HomeActivity.chat_messagesUtilses.get(position).getForum_id());
                    intent.putExtra("media_url", HomeActivity.chat_messagesUtilses.get(position).getMedia());
                    startActivityForResult(intent, 9000);
                }
            });
            chat_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    if (HomeActivity.chat_messagesUtilses.get(position).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id())) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Delete")
                                .setMessage("Are you sure want to Delete?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        deleteTopic(HomeActivity.chat_messagesUtilses.get(position).getForum_id(), position);
                                        dialog.dismiss();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })

                                .show();
                    }
                    return false;
                }
            });

            search_edt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String text = search_edt.getText().toString().toLowerCase(Locale.getDefault());
                    chatAdapter.filter(text);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    public void notifyAdapter(){
        if(chatAdapter!=null)
            chatAdapter.notifyDataSetChanged();
    }
    protected void showInputDialog(String msg, final String type, final int mediaid) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.input_dialog);
        // Set dialog title

        final EditText editText = (EditText) dialog.findViewById(R.id.edit_text);
        editText.setHint("Enter description of uploaded file...");
        // setup a dialog window
        if (!msg.equalsIgnoreCase(""))
            editText.setText(msg);
        Button button = (Button) dialog.findViewById(R.id.ok_button);
        dialog.setCancelable(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIUtility.hideKeyboard(getActivity());
                uploadTypedmsg(editText.getText().toString(), type, mediaid + "");
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        if (!Discussion_Activity.isAdded)
//            fetchTimelineAsync(0);
//    }
    private void deleteTopic(final String forumid, final int pos) {
        if (WebServices.isNetworkAvailable(getActivity())) {
            final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Deleting...");
            dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (dialog != null) dialog.dismiss();
                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                        Utils.callInvalidSession(getActivity(),APIUtility.DELETE_FORUM);
                    }else
                    if (chatAdapter != null) {
                        chatAdapter.notifyDataSetChanged();
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {
                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("forum_id", forumid));
                    nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                    String resp = webServices.callServices(nameValuePair, APIUtility.DELETE_FORUM);
                    Log.d(" Response:", resp.toString());
                    if (!webServices.isValid(resp)){
                        handler.sendEmptyMessage(1);
                        return;
                    }
                    HomeActivity.chat_messagesUtilses.remove(pos);
                    chat_messagesUtilses2.remove(pos);
                    handler.sendEmptyMessage(0);
                }
            }.start();
        } else {
            Toast.makeText(getActivity(), "Forum is deleted successfully", Toast.LENGTH_LONG).show();
        }
    }

    private void fetchTimelineAsync(int i) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(getActivity(),APIUtility.GETPOSTS);
                }else
                if (chatAdapter != null) {
//                    swipeContainer.setRefreshing(false);
                    chatAdapter.notifyDataSetChanged();
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)){
                    handler.sendEmptyMessage(1);
                    return;
                }
                HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages(resp);
                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
//                tempchat_messagesUtilses = WebServices.chat_messagesUtilses;
//                for (int i = 0; i < WebServices.chat_messagesUtilses.size(); i++) {
//                    if (WebServices.chat_messagesUtilses.get(i).isSection())
//                        tempchat_messagesUtilses.remove(i);
//                }
//                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
//                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                for (int i = 0; i<HomeActivity.chat_messagesUtilses.size(); i++) {
//                    if (!WebServices.chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
//                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
//                        utils1.setDate(WebServices.chat_messagesUtilses.get(i).getDate());
//                        utils1.setSection(true);
//                        tempchat_messagesUtilses.add(utils1);
//                        sectiondate = utils1.getDate();
//                    }
                    tempchat_messagesUtilses.add(HomeActivity.chat_messagesUtilses.get(i));
                }
                HomeActivity.chat_messagesUtilses = tempchat_messagesUtilses;
                chat_messagesUtilses2.clear();
                chat_messagesUtilses2.addAll(HomeActivity.chat_messagesUtilses);
                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    private void uploadmsg(final String massage, final String type) {
        chat_ll.setVisibility(View.GONE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (progressDialog!=null)progressDialog.dismiss();
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(getActivity(),APIUtility.SUBMIT_FORUM);
                }else {
                    chatAdapter.notifyDataSetChanged();
                    editText.setText("");
                    chat_ll.setVisibility(View.VISIBLE);
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", ""));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)){
                    handler.sendEmptyMessage(1);
                    return;
                }
                HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages(resp);
                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    private void uploadTypedmsg(final String massage, final String type, final String media_id) {
        chat_ll.setVisibility(View.GONE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (progressDialog!=null)progressDialog.dismiss();
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(getActivity(),APIUtility.SUBMIT_FORUM);
                }else {
                    chatAdapter.notifyDataSetChanged();
                    editText.setText("");
                    chat_ll.setVisibility(View.VISIBLE);
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", massage));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("media_id", media_id));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)){
                    handler.sendEmptyMessage(1);
                    return;
                }
                HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages(resp);

                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 9000:
                chatAdapter.notifyDataSetChanged();
                break;
            case REQUEST_CREATETOPIC:
                tempchat_messagesUtilses=new ArrayList<>();
                for (int i = 0; i<HomeActivity.chat_messagesUtilses.size(); i++) {
                    tempchat_messagesUtilses.add(HomeActivity.chat_messagesUtilses.get(i));
                }
                HomeActivity.chat_messagesUtilses = tempchat_messagesUtilses;
                chat_messagesUtilses2.addAll(HomeActivity.chat_messagesUtilses);
                chatAdapter.notifyDataSetChanged();
                break;
            case REQUEST_CHOOSER:
                if (resultCode == REQUEST_CHOOSER) {
                    String imagePath, imageName;
                    long imageSize;
// GET IMAGE PATH
                    imagePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    imageName = imagePath.substring(imagePath.lastIndexOf("/"));

                    imageSize = this.getFileSize(imagePath);

//                    final Uri uri = data.getData();
//
                    System.out.println("==imagePath==" + imagePath);
                    System.out.println("==imageName==" + imageName);
                    System.out.println("==imageSize==" + imageSize);
//                    this.progressDialog = this.createDialog();
//                    this.progressDialog.show();
//                    imageName=imageName.substring(1);
                    if (!new File(getActivity().getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").exists()) {
                        new File(getActivity().getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() +"/Uploaded/").mkdirs();
                    }
                    String username=WebServices.mLoginUtility.getFirstname();
//                    if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null")||!WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                        username= username+" "+WebServices.mLoginUtility.getLastname();
                    try {
                        APIUtility.copyDirectory(new File(imagePath), new File(getActivity().getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() +"/Uploaded/"+username+"_" + imageName.substring(1)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new ImageUploader(getActivity(), imagePath, APIUtility.UPLOAD_FILE, imageName, imageSize, "IMAGE",false, this);
                } else if (resultCode == 30000) {
                    String filePath, fileName;
                    long fileSize;
// GET IMAGE PATH
                    filePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    fileName = filePath.substring(filePath.lastIndexOf("/"));

                    fileSize = this.getFileSize(filePath);
                    System.out.println("==filePath==" + filePath);
                    System.out.println("==fileName==" + fileName);
                    System.out.println("==fileSize==" + fileSize);
//                    this.progressDialog = this.createDialog();
//                    this.progressDialog.show();
                    if (!new File(getActivity().getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() +"/Uploaded/").exists()) {
                        new File(getActivity().getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() +"/Uploaded/").mkdirs();
                    }
                    String username=WebServices.mLoginUtility.getFirstname();
//                    if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null")||!WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                        username= username+" "+WebServices.mLoginUtility.getLastname();
                    try {
                        APIUtility.copyDirectory(new File(filePath), new File(getActivity().getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() +"/Uploaded/"+username+"_" + fileName.substring(1)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new ImageUploader(getActivity(), filePath, APIUtility.UPLOAD_FILE, fileName, fileSize, "PDF",false, this);
                }
                break;
        }
    }

    /**
     * Get the image path
     *
     * @param uri
     * @return
     */
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * Get the file size in kilobytes
     *
     * @return
     */
    private long getFileSize(String imagePath) {

        long length = 0;

        try {

            File file = new File(imagePath);
            length = file.length();
            length = length / 1024;

        } catch (Exception e) {

            e.printStackTrace();
        }
        return length;
    }

    @Override
    public void error(String error) {
        System.out.println("==================image Uploaded Error===============" + error);
    }

    @Override
    public void complete(int media_id, String type,String res) {
        showInputDialog("", type, media_id);
        System.out.println("==================image Uploaded===============" + media_id);

    }

    private class ChatAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return HomeActivity.chat_messagesUtilses.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.topic_row, null);
                ImageView profile_img = (ImageView) convertView.findViewById(R.id.profile_image);
                ImageView right_arrow_img = (ImageView) convertView.findViewById(R.id.right_arrow_img);
                final TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                final TextView msg_txt = (TextView) convertView.findViewById(R.id.msg_txt);
                TextView time_txt = (TextView) convertView.findViewById(R.id.time_txt);
                TextView total_msg_txt = (TextView) convertView.findViewById(R.id.reply_txt);

                LinearLayout member_ll=(LinearLayout)convertView.findViewById(R.id.member_ll);
                TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt);
                member_ll.setVisibility(View.VISIBLE);
                profile_img.setVisibility(View.GONE);
                member_fname_txt.setText("");
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
                    msg_txt.setTextSize(13);
                    time_txt.setTextSize(11);
                    member_name.setTextSize(11);
                }else{
                    msg_txt.setLineSpacing((float) (1),1);
                }
                if (position % 4 == 1) {
                    member_ll.setBackgroundResource(R.drawable.test_series_bg);
                } else if (position % 4 == 2) {
                    member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                } else if (position % 4 == 0) {
                    member_ll.setBackgroundResource(R.drawable.light_blue_bg);
                } else if (position % 4 == 3) {
                    member_ll.setBackgroundResource(R.drawable.audio_bg);
                }
                String fname=HomeActivity.chat_messagesUtilses.get(position).getFirstname().trim().toString().substring(0,1);
                member_fname_txt.setText(fname);
//                TextView view_reply_txt = (TextView) convertView.findViewById(R.id.view_reply_txt);
//                if (chat_messagesUtilses.get(position).getCommentsUtilsArrayList().size() != 0)
                total_msg_txt.setText((HomeActivity.chat_messagesUtilses.get(position).getCommentsUtilsArrayList().size()) + "");
                total_msg_txt.setTextColor(Color.BLACK);
//                else total_msg_txt.setBackgroundColor(Color.TRANSPARENT);
//                right_arrow_img.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        commentsUtilsArrayList = WebServices.chat_messagesUtilses.get(position).getCommentsUtilsArrayList();
//                        Intent intent = new Intent(getActivity(), ReplyChatActivity.class);
//                        intent.putExtra("name", getCOURSE_NAME());
//                        intent.putExtra("user_name", member_name.getText().toString());
//                        intent.putExtra("msg", msg_txt.getText().toString());
//                        intent.putExtra("img", WebServices.chat_messagesUtilses.get(position).getPhoto());
//                        intent.putExtra("time", WebServices.chat_messagesUtilses.get(position).getAdded_on());
//                        intent.putExtra("forum_id", WebServices.chat_messagesUtilses.get(position).getForum_id());
//                        startActivity(intent);
//                    }
//                });
//                view_reply_txt.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        commentsUtilsArrayList=WebServices.chat_messagesUtilses.get(position).getCommentsUtilsArrayList();
//                        Intent intent=new Intent(getActivity(),ReplyChatActivity.class);
//                        intent.putExtra("name",getCOURSE_NAME());
//                        intent.putExtra("user_name",member_name.getText().toString());
//                        intent.putExtra("msg",msg_txt.getText().toString());
//                        intent.putExtra("img",WebServices.chat_messagesUtilses.get(position).getPhoto());
//                        intent.putExtra("time",WebServices.chat_messagesUtilses.get(position).getAdded_on());
//                        intent.putExtra("forum_id",WebServices.chat_messagesUtilses.get(position).getForum_id());
//                        startActivity(intent);
//                    }
//                });
//                imageLoader.DisplayImage(chat_messagesUtilses.get(position).getPhoto(), profile_img);
                if (!HomeActivity.chat_messagesUtilses.get(position).getLastname().equalsIgnoreCase("null") && HomeActivity.chat_messagesUtilses.get(position).getLastname() != null)
                    member_name.setText("by "+HomeActivity.chat_messagesUtilses.get(position).getFirstname() + " " + HomeActivity.chat_messagesUtilses.get(position).getLastname());
                else
                    member_name.setText("by "+HomeActivity.chat_messagesUtilses.get(position).getFirstname());
                msg_txt.setText(Html.fromHtml(HomeActivity.chat_messagesUtilses.get(position).getForum_title()));
                time_txt.setText("Added on " +HomeActivity.chat_messagesUtilses.get(position).getDate()+" "+ HomeActivity.chat_messagesUtilses.get(position).getAdded_on());
                msg_txt.setTextColor(Color.BLACK);
            }
            return convertView;
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            HomeActivity.chat_messagesUtilses.clear();
            if (charText.length() == 0) {
                HomeActivity.chat_messagesUtilses.addAll(chat_messagesUtilses2);
            } else {
                for (Chat_MessagesUtils wp : chat_messagesUtilses2) {
                    if (wp.getForum_title().toLowerCase(Locale.getDefault()).contains(charText)) {
                        HomeActivity.chat_messagesUtilses.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    public String getGroup_Id() {
        return Group_Id;
    }

    public void setGroup_Id(String group_Id) {
        Group_Id = group_Id;
    }

    public String getCOURSE_NAME() {
        return COURSE_NAME;
    }

    public void setCOURSE_NAME(String COURSE_NAME) {
        this.COURSE_NAME = COURSE_NAME;
    }
}
