package com.chaptervitamins.home;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.Suggestions.File_Uploader;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.discussions.UploadFilesActivity;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.downloadImages.Profile_ImageLoader;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.play_video.Link_Activity;
import com.chaptervitamins.play_video.PDFViewerActivity;
import com.chaptervitamins.utility.CommentsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Android on 7/1/2016.
 */

public class TopicDiscussion extends BaseActivity implements UploadListener {
    private WebServices webServices;
    private LayoutInflater mInflater;
    private Profile_ImageLoader imageLoader;
    private ListView chat_list;
    private ChatAdapter chatAdapter;
    private String Group_Id = "";
    private final int REQUEST_CHOOSER = 2000;
    private ImageView attached_img, send_img, back, refresh;
    private LinearLayout chat_ll, send_ll;
    ArrayList<CommentsUtils> tempchat_messagesUtilses = new ArrayList<>();
    ArrayList<CommentsUtils> arraylist = new ArrayList<>();
    ProgressBar progress_bar;
    private TextView course_name_txt;
    private TextView remLimtTextView;
    private ProgressDialog dialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.reply_chat_activity);
        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setText("Topic");

        back = (ImageView) findViewById(R.id.back);
        refresh = (ImageView) findViewById(R.id.refresh);
//        changeStatusBarColor();
        webServices = new WebServices();
        setGroup_Id(getIntent().getStringExtra("forum_id"));
        imageLoader = new Profile_ImageLoader(TopicDiscussion.this);
        mInflater = (LayoutInflater) TopicDiscussion.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        chat_list = (ListView) findViewById(R.id.chat_list);
        chat_ll = (LinearLayout) findViewById(R.id.chat_ll);
        send_ll = (LinearLayout) findViewById(R.id.send_ll);
//        remLimtTextView = (TextView) findViewById(R.id.remLimtTextView);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        final EditText editText = (EditText) findViewById(R.id.editText);

        course_name_txt = (TextView) findViewById(R.id.course_name_txt);
        course_name_txt.setVisibility(View.GONE);
        chatAdapter = new ChatAdapter();
        chat_list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chat_list.setAdapter(chatAdapter);
        String sectiondate = "";
        /*final String charLimit=getIntent().getStringExtra("character_limit");
        if(!charLimit.equalsIgnoreCase("")) {
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(charLimit))});
            remLimtTextView.setText(charLimit+" Characters Remaining");
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int remaining = Integer.parseInt(charLimit) - (editText.getText().toString().length());
                    remLimtTextView.setText(remaining + " Characters Remaining");
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }*/

        if (HomeActivity.chat_messagesUtilses != null) {
            arraylist = HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).getCommentsUtilsArrayList();
            for (int i = 0; i < arraylist.size(); i++) {
                if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                    CommentsUtils utils1 = new CommentsUtils();
                    utils1.setDate(arraylist.get(i).getDate());
                    utils1.setSection(true);
                    tempchat_messagesUtilses.add(utils1);
                    sectiondate = utils1.getDate();
                }
                tempchat_messagesUtilses.add(arraylist.get(i));
            }
            arraylist = tempchat_messagesUtilses;
        }
        chat_list.setSelection(chatAdapter.getCount() - 1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                chat_list.setSelection(chatAdapter.getCount() - 1);
            }
        });
        attached_img = (ImageView) findViewById(R.id.attached_img);
        send_img = (ImageView) findViewById(R.id.send_img);


        editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editText.post(new Runnable() {
            @Override
            public void run() {
                editText.setSelection(editText.getText().toString().length());
            }
        });
//        editText.setHint("Reply...");
        attached_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TopicDiscussion.this, UploadFilesActivity.class);
                startActivityForResult(intent, REQUEST_CHOOSER);
//                Intent getContentIntent = FileUtils.createGetContentIntent();
//
//                Intent intent = Intent.createChooser(getContentIntent, "Select a file");
//                startActivityForResult(intent, REQUEST_CHOOSER);
            }
        });
        send_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = ((EditText) editText).getText().toString();
                System.out.println("===strstrstr==" + str);
                if (editText.getText().toString().equalsIgnoreCase("")) return;
                final InputMethodManager imm = (InputMethodManager) TopicDiscussion.this.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(send_img.getWindowToken(), 0);
                if (WebServices.isNetworkAvailable(TopicDiscussion.this)) {
                    uploadmsg1(editText.getText().toString(), "POST");
                    editText.getText().clear();
                } else
                    Toast.makeText(TopicDiscussion.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();


            }
        });

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                refreshChat(false);
                handler.postDelayed(this, 30000);
            }
        };

//Start
        handler.postDelayed(runnable, 30000);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshChat(true);
            }
        });

//        final View rootView = findViewById(R.id.root_view);
//        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);
//        final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
        //Will automatically set size according to the soft keyboard size
       /* popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (editText == null || emojicon == null) {
                    return;
                }

                int start = editText.getSelectionStart();
                int end = editText.getSelectionEnd();
                if (start < 0) {
                    editText.append(emojicon.getEmoji());
                } else {
                    editText.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                editText.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        editText.setFocusableInTouchMode(true);
                        editText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }
            }
        });*/
    }

    /*private void fetchTimelineAsync1() {
        final ProgressDialog dialog = ProgressDialog.show(TopicDiscussion.this, "", "Refreshing...");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));

        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("group_id", getIntent().getStringExtra("groupid")));
        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
        nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
        new GenericApiCall(TopicDiscussion.this, APIUtility.GETPOSTS, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages((String) result);
                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                if (HomeActivity.chat_messagesUtilses != null && HomeActivity.chat_messagesUtilses.size() > 0) {
                    arraylist = HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).getCommentsUtilsArrayList();
                    for (int i = 0; i < arraylist.size(); i++) {
                        if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                            CommentsUtils utils1 = new CommentsUtils();
                            utils1.setDate(arraylist.get(i).getDate());
                            utils1.setSection(true);
                            tempchat_messagesUtilses.add(utils1);
                            sectiondate = utils1.getDate();
                        }
                        tempchat_messagesUtilses.add(arraylist.get(i));
                    }
                    arraylist = tempchat_messagesUtilses;
                }
                chatAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }

            @Override
            public void onError(Object error) {

            }
        }).execute();
    }*/

    /*private void fetchTimelineAsync() {
        if (WebServices.isNetworkAvailable(TopicDiscussion.this)) {
            if (!isFinishing()) {
                final ProgressDialog dialog = ProgressDialog.show(TopicDiscussion.this, "", "Refreshing...");
                dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (dialog != null) dialog.dismiss();
                        if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                            Toast.makeText(TopicDiscussion.this, getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
                            DataBase.getInstance(TopicDiscussion.this).updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                            SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                            editor.putBoolean("islogin", false);
                            editor.clear();
                            editor.commit();
                            Intent intent = new Intent(TopicDiscussion.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("sms_url", "");
                            startActivity(intent);
                            finish();
                            return;
                        }
//                    else
//                        chatAdapter.notifyDataSetChanged();
                    }
                };
                new Thread() {
                    @Override
                    public void run() {
                        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("group_id", getIntent().getStringExtra("groupid")));
                        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
                        nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                        String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
                        Log.d(" Response:", resp.toString());
                        if (!webServices.isValid(resp)) {
                            handler.sendEmptyMessage(1);
                            return;
                        }
                        HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages(resp);
                        String sectiondate = "";
                        tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                        if (HomeActivity.chat_messagesUtilses != null && HomeActivity.chat_messagesUtilses.size() > 0) {
                            arraylist = HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).getCommentsUtilsArrayList();
                            for (int i = 0; i < arraylist.size(); i++) {
                                if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                                    CommentsUtils utils1 = new CommentsUtils();
                                    utils1.setDate(arraylist.get(i).getDate());
                                    utils1.setSection(true);
                                    tempchat_messagesUtilses.add(utils1);
                                    sectiondate = utils1.getDate();
                                }
                                tempchat_messagesUtilses.add(arraylist.get(i));
                            }
                            arraylist = tempchat_messagesUtilses;
                        }
                        handler.sendEmptyMessage(0);
                    }
                }.start();
            }
        } else {
            Toast.makeText(TopicDiscussion.this, "Unable to reach server! Please check your internet connection.", Toast.LENGTH_LONG).show();
        }
    }*/

    private void refreshChat(boolean showDialog) {
        if (WebServices.isNetworkAvailable(TopicDiscussion.this)) {
            if (!isFinishing()) {
                if (showDialog)
                    dialog = ProgressDialog.show(TopicDiscussion.this, "", "Refreshing...");

                ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getIntent().getStringExtra("groupid")));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                new GenericApiCall(TopicDiscussion.this, APIUtility.GETPOSTS, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                    @Override
                    public void onSuccess(Object result) {
                        try {
                            if (dialog != null) dialog.dismiss();
                            HomeActivity.chat_messagesUtilses = webServices.getPublicchatMessages((String) result);
                            String sectiondate = "";
                            tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                            if (HomeActivity.chat_messagesUtilses != null && HomeActivity.chat_messagesUtilses.size() > 0) {
                                arraylist.clear();
                                arraylist.addAll(HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).getCommentsUtilsArrayList());
                                for (int i = 0; i < arraylist.size(); i++) {
                                    if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                                        CommentsUtils utils1 = new CommentsUtils();
                                        utils1.setDate(arraylist.get(i).getDate());
                                        utils1.setSection(true);
                                        tempchat_messagesUtilses.add(utils1);
                                        sectiondate = utils1.getDate();
                                    }
                                    tempchat_messagesUtilses.add(arraylist.get(i));
                                }
                                arraylist.clear();
                                arraylist.addAll(tempchat_messagesUtilses);
                            }
                            if (chatAdapter != null)
                                chatAdapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ErrorModel error) {
                        if (dialog != null) dialog.dismiss();
                    }
                }).execute();
            }
        } else
            Toast.makeText(TopicDiscussion.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    private void deleteTopic1(final String comment_id, final int pos) {
        if (WebServices.isNetworkAvailable(TopicDiscussion.this)) {
            if (!isFinishing()) {
                final ProgressDialog dialog = ProgressDialog.show(TopicDiscussion.this, "", "Deleting...");

                ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("comment_id", comment_id));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                new GenericApiCall(TopicDiscussion.this, APIUtility.DELETE_COMMENT, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                    @Override
                    public void onSuccess(Object result) {
                        arraylist.remove(pos);
                        tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                        tempchat_messagesUtilses = arraylist;
                        for (int i = 0; i < arraylist.size(); i++) {
                            if (arraylist.get(i).isSection())
                                tempchat_messagesUtilses.remove(i);
                        }
                        HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).setCommentsUtilsArrayList(tempchat_messagesUtilses);
                        arraylist = tempchat_messagesUtilses;
                        tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                        String sectiondate = "";
                        for (int i = 0; i < arraylist.size(); i++) {
                            if (!arraylist.get(i).isSection()) {
                                if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                                    CommentsUtils utils1 = new CommentsUtils();
                                    utils1.setDate(arraylist.get(i).getDate());
                                    utils1.setSection(true);
                                    tempchat_messagesUtilses.add(utils1);
                                    sectiondate = utils1.getDate();
                                }
                                tempchat_messagesUtilses.add(arraylist.get(i));
                            }
                        }
                        arraylist = tempchat_messagesUtilses;
                        try {
                            chatAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(TopicDiscussion.this, "Comment is deleted successfully", Toast.LENGTH_LONG).show();
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }

                    @Override
                    public void onError(ErrorModel error) {
                        if (dialog != null) dialog.dismiss();
                    }
                }).execute();
            }
        } else
            Toast.makeText(TopicDiscussion.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }

    private void deleteTopic(final String comment_id, final int pos) {
        if (WebServices.isNetworkAvailable(TopicDiscussion.this)) {
            final ProgressDialog dialog = ProgressDialog.show(TopicDiscussion.this, "", "Deleting...");
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                        Utils.callInvalidSession(TopicDiscussion.this,APIUtility.DELETE_COMMENT);
                        return;
                    } else {
                        tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                        tempchat_messagesUtilses = arraylist;
                        for (int i = 0; i < arraylist.size(); i++) {
                            if (arraylist.get(i).isSection())
                                tempchat_messagesUtilses.remove(i);
                        }
                        HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).setCommentsUtilsArrayList(tempchat_messagesUtilses);
                        arraylist = tempchat_messagesUtilses;
                        tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                        String sectiondate = "";
                        for (int i = 0; i < arraylist.size(); i++) {
                            if (!arraylist.get(i).isSection()) {
                                if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                                    CommentsUtils utils1 = new CommentsUtils();
                                    utils1.setDate(arraylist.get(i).getDate());
                                    utils1.setSection(true);
                                    tempchat_messagesUtilses.add(utils1);
                                    sectiondate = utils1.getDate();
                                }
                                tempchat_messagesUtilses.add(arraylist.get(i));
                            }
                        }
                        arraylist = tempchat_messagesUtilses;
                        if (dialog != null) dialog.dismiss();
//                        if (chatAdapter != null) {
//                            chatAdapter.notifyDataSetChanged();
//
//                        }
                        Toast.makeText(TopicDiscussion.this, "Comment is deleted successfully", Toast.LENGTH_LONG).show();
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {
                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("comment_id", comment_id));
                    nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                    String resp = webServices.callServices(nameValuePair, APIUtility.DELETE_COMMENT);
                    Log.d(" Response:", resp.toString());
                    if (!webServices.isValid(resp)) {
                        handler.sendEmptyMessage(1);
                        return;
                    }
                    arraylist.remove(pos);
                    handler.sendEmptyMessage(0);
                }
            }.start();
        } else {
            Toast.makeText(TopicDiscussion.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }

    protected void showInputDialog(String msg, final String type, final int mediaid) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(TopicDiscussion.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.input_dialog);
        // Set dialog title

        final EditText editText = (EditText) dialog.findViewById(R.id.edit_text);
        editText.setHint("Enter description of uploaded file...");
        // setup a dialog window
        if (!msg.equalsIgnoreCase(""))
            editText.setText(msg);
        Button button = (Button) dialog.findViewById(R.id.ok_button);
        dialog.setCancelable(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                APIUtility.hideKeyboard(TopicDiscussion.this);
                uploadTypedmsg1(editText.getText().toString(), type, mediaid + "");

            }
        });
        if (!isFinishing())
            dialog.show();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }
    }

    @Override
    public void error(String error) {
        System.out.println("==================image Uploaded Error===============" + error);
    }

    @Override
    public void complete(int media_id, String type, String resp) {
        if (media_id == 0) {
            if (type.contains("pdf")) {
                Intent intent = new Intent(TopicDiscussion.this, PDFViewerActivity.class);
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(type));
                intent.putExtra("filename", type);
                //if document protected with password
                intent.putExtra("password", "encrypted PDF password");

                //if you need highlight link boxes
                intent.putExtra("linkhighlight", true);
//                    intent.putExtra("filename", filename);

                //if you don't need device sleep on reading document
                intent.putExtra("idleenabled", false);

                //set true value for horizontal page scrolling, false value for vertical page scrolling
                intent.putExtra("horizontalscrolling", true);
                try {
                    type = type.substring(type.lastIndexOf("/")).split("_")[1];
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //document name
                intent.putExtra("docname", type);

                startActivity(intent);
            } else {
                Toast.makeText(TopicDiscussion.this, "Error to upload! Please try again", Toast.LENGTH_LONG).show();
            }
        } else
            showInputDialog("", type, media_id);
        System.out.println("==================image Uploaded===============" + media_id);
    }

    private void uploadTypedmsg1(final String massage, final String type, final String media_id) {
        if (WebServices.isNetworkAvailable(TopicDiscussion.this)) {
            progress_bar.setVisibility(View.VISIBLE);
            chat_ll.setVisibility(View.GONE);

            ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("forum_id", getGroup_Id()));
            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("forum_description", massage));
            nameValuePair.add(new BasicNameValuePair("forum_title", massage));
            nameValuePair.add(new BasicNameValuePair("comment", massage));
            nameValuePair.add(new BasicNameValuePair("forum_type", type));
            nameValuePair.add(new BasicNameValuePair("media_id", media_id));
            nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
            new GenericApiCall(TopicDiscussion.this, APIUtility.SUBMIT_COMMENT, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    arraylist.addAll(webServices.getPublicchatComment((String) result));

                    String sectiondate = "";
                    tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                    tempchat_messagesUtilses = arraylist;
                    for (int i = 0; i < arraylist.size(); i++) {
                        if (arraylist.get(i).isSection())
                            tempchat_messagesUtilses.remove(i);
                    }

                    arraylist = tempchat_messagesUtilses;
                    HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).setCommentsUtilsArrayList(arraylist);
                    tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                    for (int i = 0; i < arraylist.size(); i++) {
                        if (!arraylist.get(i).isSection()) {
                            if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                                CommentsUtils utils1 = new CommentsUtils();
                                utils1.setDate(arraylist.get(i).getDate());
                                utils1.setSection(true);
                                tempchat_messagesUtilses.add(utils1);
                                sectiondate = utils1.getDate();
                            }
                            tempchat_messagesUtilses.add(arraylist.get(i));
                        }
                    }
                    arraylist = tempchat_messagesUtilses;
                    try {
                        chatAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progress_bar.setVisibility(View.GONE);
                    chat_ll.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError(ErrorModel error) {
                    progress_bar.setVisibility(View.GONE);
                    chat_ll.setVisibility(View.VISIBLE);
                }
            }).execute();
        } else
            Toast.makeText(TopicDiscussion.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }


    private void uploadTypedmsg(final String massage, final String type, final String media_id) {
        progress_bar.setVisibility(View.VISIBLE);
        chat_ll.setVisibility(View.GONE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress_bar.setVisibility(View.GONE);
                        chat_ll.setVisibility(View.VISIBLE);
                        if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                            Utils.callInvalidSession(TopicDiscussion.this,APIUtility.SUBMIT_COMMENT);
                            return;
                        } else {
                            /*try {
                                chatAdapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                        }
                    }
                });


            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("forum_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", massage));
                nameValuePair.add(new BasicNameValuePair("comment", massage));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("media_id", media_id));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_COMMENT);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)) {
                    handler.sendEmptyMessage(1);

                    return;
                }
                arraylist.addAll(webServices.getPublicchatComment(resp));

                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                tempchat_messagesUtilses = arraylist;
                for (int i = 0; i < arraylist.size(); i++) {
                    if (arraylist.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }

                arraylist = tempchat_messagesUtilses;
                HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).setCommentsUtilsArrayList(arraylist);
                tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                for (int i = 0; i < arraylist.size(); i++) {
                    if (!arraylist.get(i).isSection()) {
                        if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                            CommentsUtils utils1 = new CommentsUtils();
                            utils1.setDate(arraylist.get(i).getDate());
                            utils1.setSection(true);
                            tempchat_messagesUtilses.add(utils1);
                            sectiondate = utils1.getDate();
                        }
                        tempchat_messagesUtilses.add(arraylist.get(i));
                    }
                }
                arraylist = tempchat_messagesUtilses;
//                tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
//                for (int i = (arraylist.size() - 1); i >= 0; i--) {
//                    if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
//                        CommentsUtils utils1 = new CommentsUtils();
//                        utils1.setDate(arraylist.get(i).getDate());
//                        utils1.setSection(true);
//                        tempchat_messagesUtilses.add(utils1);
//                        sectiondate = utils1.getDate();
//                    }
//                    tempchat_messagesUtilses.add(arraylist.get(i));
//                }
//                arraylist = tempchat_messagesUtilses;
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            handler.sendEmptyMessage(0);
                        }
                    }
                }.start();

            }
        }.start();
    }

    private void uploadmsg1(final String massage, final String type) {
        if (WebServices.isNetworkAvailable(TopicDiscussion.this)) {
            chat_ll.setVisibility(View.GONE);
            progress_bar.setVisibility(View.VISIBLE);

            ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("forum_id", getIntent().getStringExtra("forum_id")));
            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("comment", massage));
            nameValuePair.add(new BasicNameValuePair("forum_title", ""));
            nameValuePair.add(new BasicNameValuePair("forum_type", type));
            nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
            new GenericApiCall(TopicDiscussion.this, APIUtility.SUBMIT_COMMENT, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    arraylist.addAll(webServices.getPublicchatComment((String) result));

                    String sectiondate = "";
                    tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                    tempchat_messagesUtilses = arraylist;
                    for (int i = 0; i < arraylist.size(); i++) {
                        if (arraylist.get(i).isSection())
                            tempchat_messagesUtilses.remove(i);
                    }

                    arraylist = tempchat_messagesUtilses;
                    HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).setCommentsUtilsArrayList(arraylist);
                    tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                    for (int i = 0; i < arraylist.size(); i++) {
                        if (!arraylist.get(i).isSection()) {
                            if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                                CommentsUtils utils1 = new CommentsUtils();
                                utils1.setDate(arraylist.get(i).getDate());
                                utils1.setSection(true);
                                tempchat_messagesUtilses.add(utils1);
                                sectiondate = utils1.getDate();
                            }
                            tempchat_messagesUtilses.add(arraylist.get(i));
                        }
                    }
                    arraylist = tempchat_messagesUtilses;
                    try {
                        chatAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progress_bar.setVisibility(View.GONE);
                    chat_ll.setVisibility(View.VISIBLE);

                }

                @Override
                public void onError(ErrorModel error) {
                    progress_bar.setVisibility(View.GONE);
                    chat_ll.setVisibility(View.VISIBLE);
                }
            }).execute();
        } else
            Toast.makeText(TopicDiscussion.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
    }


    private void uploadmsg(final String massage, final String type) {
        chat_ll.setVisibility(View.GONE);
        progress_bar.setVisibility(View.VISIBLE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress_bar.setVisibility(View.GONE);
                        chat_ll.setVisibility(View.VISIBLE);
                        if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                            Utils.callInvalidSession(TopicDiscussion.this,APIUtility.SUBMIT_COMMENT);
                            return;
                        } else {

//                            chatAdapter.notifyDataSetChanged();
                        }
                    }
                });


            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("forum_id", getIntent().getStringExtra("forum_id")));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("comment", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", ""));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_COMMENT);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)) {
                    handler.sendEmptyMessage(1);

                    return;
                }
                arraylist.addAll(webServices.getPublicchatComment(resp));

                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                tempchat_messagesUtilses = arraylist;
                for (int i = 0; i < arraylist.size(); i++) {
                    if (arraylist.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }

                arraylist = tempchat_messagesUtilses;
                HomeActivity.chat_messagesUtilses.get(getIntent().getIntExtra("pos", 0)).setCommentsUtilsArrayList(arraylist);
                tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
                for (int i = 0; i < arraylist.size(); i++) {
                    if (!arraylist.get(i).isSection()) {
                        if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                            CommentsUtils utils1 = new CommentsUtils();
                            utils1.setDate(arraylist.get(i).getDate());
                            utils1.setSection(true);
                            tempchat_messagesUtilses.add(utils1);
                            sectiondate = utils1.getDate();
                        }
                        tempchat_messagesUtilses.add(arraylist.get(i));
                    }
                }
                arraylist = tempchat_messagesUtilses;
//                tempchat_messagesUtilses = new ArrayList<CommentsUtils>();
//                for (int i = 0; i <arraylist.size(); i++) {
//                    if (!arraylist.get(i).getDate().equalsIgnoreCase(sectiondate)) {
//                        CommentsUtils utils1 = new CommentsUtils();
//                        utils1.setDate(arraylist.get(i).getDate());
//                        utils1.setSection(true);
//                        tempchat_messagesUtilses.add(utils1);
//                        sectiondate = utils1.getDate();
//                    }
//                    tempchat_messagesUtilses.add(arraylist.get(i));
//                }
//                arraylist = tempchat_messagesUtilses;
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            handler.sendEmptyMessage(0);
                        }
                    }
                }.start();
            }
        }.start();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHOOSER:
                if (resultCode == REQUEST_CHOOSER) {
                    String imagePath, imageName;
                    long imageSize;
// GET IMAGE PATH
                    imagePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    imageName = imagePath.substring(imagePath.lastIndexOf("/"));

                    imageSize = this.getFileSize(imagePath);

//                    final Uri uri = data.getData();
//
                    System.out.println("==imagePath==" + imagePath);
                    System.out.println("==imageName==" + imageName);
                    System.out.println("==imageSize==" + imageSize);
                    if (!new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").exists()) {
                        new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").mkdirs();
                    }
                    String username = WebServices.mLoginUtility.getFirstname();
//                    if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null") || !WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                        username = username + " " + WebServices.mLoginUtility.getLastname();
                    try {
                        APIUtility.copyDirectory(new File(imagePath), new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/" + username + "_" + imageName.substring(1)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new File_Uploader(TopicDiscussion.this, imagePath, APIUtility.UPLOAD_FILE, imageName, imageSize, "IMAGE", true, TopicDiscussion.this);
                } else if (resultCode == 3000) {
                    String filePath, fileName;
                    long fileSize;
// GET IMAGE PATH
                    filePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    fileName = filePath.substring(filePath.lastIndexOf("/"));

                    fileSize = this.getFileSize(filePath);
                    System.out.println("==filePath==" + filePath);
                    System.out.println("==fileName==" + fileName);
                    System.out.println("==fileSize==" + fileSize);
                    if (!new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").exists()) {
                        new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/").mkdirs();
                    }
                    String username = WebServices.mLoginUtility.getFirstname();
//                    if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null") || !WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                        username = username + " " + WebServices.mLoginUtility.getLastname();
                    try {
                        APIUtility.copyDirectory(new File(filePath), new File(getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Uploaded/" + username + "_" + fileName.substring(1)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    new File_Uploader(TopicDiscussion.this, filePath, APIUtility.UPLOAD_FILE, fileName, fileSize, "PDF", true, TopicDiscussion.this);
                }
                break;
        }
    }

    /**
     * Get the image path
     *
     * @param uri
     * @return
     */
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = TopicDiscussion.this.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * Get the file size in kilobytes
     *
     * @return
     */
    private long getFileSize(String imagePath) {

        long length = 0;

        try {

            File file = new File(imagePath);
            length = file.length();
//            length = length / 1024;

        } catch (Exception e) {

            e.printStackTrace();
        }
        return length;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(TopicDiscussion.this);
    }

    private class ChatAdapter extends BaseAdapter {

        @Override
        public int getCount() {

            return arraylist.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                if (position == 0) {
                    convertView = mInflater.inflate(R.layout.chat_row, null);
                    LinearLayout left_ll = (LinearLayout) convertView.findViewById(R.id.left_ll);
                    LinearLayout msg_ll = (LinearLayout) convertView.findViewById(R.id.msg_ll);
                    LinearLayout time_ll = (LinearLayout) convertView.findViewById(R.id.time_ll);
                    LinearLayout row_ll = (LinearLayout) convertView.findViewById(R.id.row_ll);
                    LinearLayout blank_ll = (LinearLayout) convertView.findViewById(R.id.blank_ll);
//                    ImageView profile_img = (ImageView) convertView.findViewById(R.id.profile_image);
                    TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                    TextView msg_txt = (TextView) convertView.findViewById(R.id.msg_txt);
                    TextView post_txt = (TextView) convertView.findViewById(R.id.post_txt);
                    TextView replies_txt = (TextView) convertView.findViewById(R.id.replies_txt);
                    TextView title_txt = (TextView) convertView.findViewById(R.id.title_txt);
                    left_ll.setVisibility(View.VISIBLE);
                    replies_txt.setVisibility(View.VISIBLE);
                    blank_ll.setVisibility(View.GONE);
                    title_txt.setVisibility(View.VISIBLE);
                    msg_ll.setBackgroundColor(Color.TRANSPARENT);
                    time_ll.setBackgroundColor(Color.TRANSPARENT);
                    member_name.setTextSize(14);
                    post_txt.setTextSize(10);
                    member_name.setTypeface(Typeface.DEFAULT_BOLD);
                    title_txt.setTextSize(15);
                    msg_txt.setTextSize(12);
                    TextView file_name_txt = (TextView) convertView.findViewById(R.id.file_name_txt);
                    LinearLayout member_ll = (LinearLayout) convertView.findViewById(R.id.member_ll3);
                    TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt3);
                    member_ll.setVisibility(View.GONE);
//                    profile_img.setVisibility(View.GONE);
                    member_fname_txt.setText("");
                    if (position % 4 == 1) {
                        member_ll.setBackgroundResource(R.drawable.test_series_bg);
                    } else if (position % 4 == 2) {
                        member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                    } else if (position % 4 == 0) {
                        member_ll.setBackgroundResource(R.drawable.pdf_bg);
                    } else if (position % 4 == 3) {
                        member_ll.setBackgroundResource(R.drawable.audio_bg);
                    }
                    String fname = getIntent().getStringExtra("user_name").toUpperCase().trim().toString().substring(0, 1);
                    member_fname_txt.setText(fname);
                    LinearLayout left_attached_file_ll = (LinearLayout) convertView.findViewById(R.id.left_attached_file_ll);
                    if (!getIntent().getStringExtra("media_url").equalsIgnoreCase("")) {
                        left_attached_file_ll.setVisibility(View.VISIBLE);
                        String filename = getIntent().getStringExtra("media_url").substring(getIntent().getStringExtra("media_url").lastIndexOf("/"));
                        filename = filename.substring(1);
                        file_name_txt.setText(filename);
                    }
//                    imageLoader.DisplayImage(getIntent().getStringExtra("img"), profile_img);
                    msg_txt.setText(Html.fromHtml(getIntent().getStringExtra("msg")));
                    title_txt.setText(Html.fromHtml(getIntent().getStringExtra("title")));
                    post_txt.setText("Posted a topic on " + getIntent().getStringExtra("time"));
                    member_name.setText(getIntent().getStringExtra("user_name").toUpperCase());
                    row_ll.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!getIntent().getStringExtra("media_url").equalsIgnoreCase("")) {
                                if (getIntent().getStringExtra("media_url").contains(".png") || getIntent().getStringExtra("media_url").contains(".jpg")) {
                                    Intent intent = new Intent(TopicDiscussion.this, Link_Activity.class);
                                    intent.putExtra("url", getIntent().getStringExtra("media_url"));
                                    intent.putExtra("imguri", "");
                                    intent.putExtra("is_image", true);
                                    intent.putExtra("userid", getIntent().getStringExtra("userid"));
                                    startActivity(intent);
                                } else if (getIntent().getStringExtra("media_url").contains(".pdf")) {
                                    String username = WebServices.mLoginUtility.getFirstname();
//                                    if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null") || !WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                                        username = username + " " + WebServices.mLoginUtility.getLastname();
                                    String f_name = getIntent().getStringExtra("media_url").substring(getIntent().getStringExtra("media_url").lastIndexOf("/")).substring(1);
                                    new APIUtility().DownloadFIle(TopicDiscussion.this, getIntent().getStringExtra("media_url"), username + "_" + f_name, TopicDiscussion.this);

                                } else if (getIntent().getStringExtra("media_url").contains(".txt")) {
                                    Intent intent = new Intent(TopicDiscussion.this, NewsFeedDetail_Activity.class);
                                    intent.putExtra("link", getIntent().getStringExtra("media_url"));
                                    intent.putExtra("title", "Discussions");
                                    intent.putExtra("type", "txt");
                                    startActivity(intent);
                                }
                            }
                        }
                    });
                } else {
                    if (!arraylist.get(position - 1).isSection()) {
                        convertView = mInflater.inflate(R.layout.chat_row, null);
                        LinearLayout left_ll = (LinearLayout) convertView.findViewById(R.id.left_ll);
                        left_ll.setVisibility(View.GONE);
                        LinearLayout row_ll = (LinearLayout) convertView.findViewById(R.id.row_ll);
                        LinearLayout left_new_ll = (LinearLayout) convertView.findViewById(R.id.left_new_ll);
                        ImageView left_new_img = (ImageView) convertView.findViewById(R.id.left_new_img);
                        TextView left_new_name_txt = (TextView) convertView.findViewById(R.id.left_new_name_txt);

                        TextView left_new_msg_txt = (TextView) convertView.findViewById(R.id.left_new_msg_txt);
                        TextView left_new_time_txt = (TextView) convertView.findViewById(R.id.left_new_time_txt);
                        LinearLayout left_attached_file_ll2 = (LinearLayout) convertView.findViewById(R.id.left_attached_file_ll2);
                        LinearLayout right_attached_file_ll2 = (LinearLayout) convertView.findViewById(R.id.right_attached_file_ll2);
                        TextView left_attach_filename_txt = (TextView) convertView.findViewById(R.id.left_attach_filename_txt);
                        TextView right_attach_filename_txt = (TextView) convertView.findViewById(R.id.right_attach_filename_txt);
                        LinearLayout right_new_ll = (LinearLayout) convertView.findViewById(R.id.right_new_ll);
                        ImageView right_new_img = (ImageView) convertView.findViewById(R.id.right_new_img);
                        TextView right_new_name_txt = (TextView) convertView.findViewById(R.id.right_new_name_txt);

                        TextView right_new_msg_txt = (TextView) convertView.findViewById(R.id.right_new_msg_txt);
                        TextView right_new_time_txt = (TextView) convertView.findViewById(R.id.right_new_time_txt);

//                        if ((position - 1) % 5 == 0) {
//                            left_new_name_txt.setTextColor(Color.parseColor("#6fb5ec"));
//                            right_new_name_txt.setTextColor(Color.parseColor("#6fb5ec"));
//                        } else if ((position - 1) % 5 == 1) {
//                            left_new_name_txt.setTextColor(Color.parseColor("#ed873e"));
//                            right_new_name_txt.setTextColor(Color.parseColor("#ed873e"));
//                        } else if ((position - 1) % 5 == 2) {
//                            left_new_name_txt.setTextColor(Color.parseColor("#bf564d"));
//                            right_new_name_txt.setTextColor(Color.parseColor("#bf564d"));
//                        } else if ((position - 1) % 5 == 3) {
//                            left_new_name_txt.setTextColor(Color.parseColor("#4d5fbf"));
//                            right_new_name_txt.setTextColor(Color.parseColor("#4d5fbf"));
//                        } else if ((position - 1) % 5 == 4) {
//                            left_new_name_txt.setTextColor(Color.parseColor("#4dbfb0"));
//                            right_new_name_txt.setTextColor(Color.parseColor("#4dbfb0"));
//                        }
                        row_ll.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                if (position == 0) return false;
                                if (arraylist.get(position - 1).isSection()) return false;
                                if (arraylist.get(position - 1).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id())) {
                                    new AlertDialog.Builder(TopicDiscussion.this)
                                            .setTitle("Delete")
                                            .setMessage("Are you sure want to Delete?")
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    deleteTopic1(arraylist.get(position - 1).getForum_id(), position - 1);

                                                }
                                            })
                                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            })

                                            .show();
                                }
                                return false;
                            }
                        });
                        row_ll.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (arraylist.get(position - 1).getMedia().endsWith(".")) {
                                    Intent intent = new Intent(TopicDiscussion.this, Link_Activity.class);
                                    intent.putExtra("url", arraylist.get(position - 1).getMedia());
                                    intent.putExtra("imguri", "");
                                    intent.putExtra("is_image", true);
                                    intent.putExtra("userid", arraylist.get(position - 1).getUser_id());
                                    startActivity(intent);
                                }
                                if (arraylist.get(position - 1).getMedia().contains(".png") || arraylist.get(position - 1).getMedia().contains(".jpg")) {
                                    Intent intent = new Intent(TopicDiscussion.this, Link_Activity.class);
                                    intent.putExtra("url", arraylist.get(position - 1).getMedia());
                                    intent.putExtra("imguri", "");
                                    intent.putExtra("is_image", true);
                                    intent.putExtra("userid", arraylist.get(position - 1).getUser_id());
                                    startActivity(intent);
                                } else if (arraylist.get(position - 1).getMedia().contains(".pdf")) {
                                    String username = WebServices.mLoginUtility.getFirstname();
//                                    if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null") || !WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                                        username = username + " " + WebServices.mLoginUtility.getLastname();
                                    String f_name = arraylist.get(position - 1).getMedia().substring(arraylist.get(position - 1).getMedia().lastIndexOf("/")).substring(1);
                                    new APIUtility().DownloadFIle(TopicDiscussion.this, arraylist.get(position - 1).getMedia(), username + "_" + f_name, TopicDiscussion.this);

                                } else if (arraylist.get(position - 1).getMedia().contains(".txt")) {
                                    Intent intent = new Intent(TopicDiscussion.this, NewsFeedDetail_Activity.class);
                                    intent.putExtra("link", arraylist.get(position - 1).getMedia());
                                    intent.putExtra("title", "Discussions");
                                    intent.putExtra("type", arraylist.get(position - 1).getMedia_type());
                                    startActivity(intent);
                                }

                            }
                        });
                        if (arraylist.get(position - 1).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id())) {
                            LinearLayout member_ll = (LinearLayout) convertView.findViewById(R.id.member_ll2);
                            TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt2);
                            member_ll.setVisibility(View.VISIBLE);
                            right_new_img.setVisibility(View.GONE);
                            member_fname_txt.setText("");
                            if ((position - 1) % 4 == 1) {
                                member_ll.setBackgroundResource(R.drawable.test_series_bg);
                            } else if ((position - 1) % 4 == 2) {
                                member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                            } else if ((position - 1) % 4 == 0) {
                                member_ll.setBackgroundResource(R.drawable.pdf_bg);
                            } else if ((position - 1) % 4 == 3) {
                                member_ll.setBackgroundResource(R.drawable.audio_bg);
                            }
                            String fname = arraylist.get(position - 1).getFirstname().trim().toString().substring(0, 1);
                            member_fname_txt.setText(fname);
                            left_new_ll.setVisibility(View.GONE);
                            right_new_ll.setVisibility(View.VISIBLE);
//                            if (!arraylist.get(position-1).getPhoto().equalsIgnoreCase(""))right_attached_file_ll.setVisibility(View.VISIBLE);
//                            right_user_ll.setBackgroundResource(R.drawable.r_box);
//                            imageLoader.DisplayImage(arraylist.get(position - 1).getPhoto(), right_new_img);
                            right_new_name_txt.setText("You");
//                            String msg=arraylist.get(position-1).getComment().replace("\n"," \n ");
//                            msg=arraylist.get(position-1).getComment().replace(" \n ","<br/>");
                            right_new_msg_txt.setTextColor(Color.GRAY);

                            right_new_msg_txt.setText(arraylist.get(position - 1).getComment());
                            right_new_time_txt.setText(arraylist.get(position - 1).getAdded_on());

                            if (!arraylist.get(position - 1).getMedia().equalsIgnoreCase("")) {
                                right_attached_file_ll2.setVisibility(View.VISIBLE);
                                String filename = arraylist.get(position - 1).getMedia().substring(arraylist.get(position - 1).getMedia().lastIndexOf("/"));
                                filename = filename.substring(1);
                                right_attach_filename_txt.setText(filename);
                            }


                        } else {

                            LinearLayout member_ll = (LinearLayout) convertView.findViewById(R.id.member_ll);
                            TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt);
                            member_ll.setVisibility(View.VISIBLE);
                            left_new_img.setVisibility(View.GONE);
                            member_fname_txt.setText("");
                            if ((position - 1) % 4 == 1) {
                                member_ll.setBackgroundResource(R.drawable.test_series_bg);
                            } else if ((position - 1) % 4 == 2) {
                                member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                            } else if ((position - 1) % 4 == 0) {
                                member_ll.setBackgroundResource(R.drawable.pdf_bg);
                            } else if ((position - 1) % 4 == 3) {
                                member_ll.setBackgroundResource(R.drawable.audio_bg);
                            }
                            String fname = arraylist.get(position - 1).getFirstname().trim().toString().substring(0, 1);
                            member_fname_txt.setText(fname);


                            right_new_ll.setVisibility(View.GONE);
                            left_new_ll.setVisibility(View.VISIBLE);
                            if (!arraylist.get(position - 1).getMedia().equalsIgnoreCase("")) {
                                left_attached_file_ll2.setVisibility(View.VISIBLE);
                                String filename = arraylist.get(position - 1).getMedia().substring(arraylist.get(position - 1).getMedia().lastIndexOf("/"));
                                filename = filename.substring(1);
                                left_attach_filename_txt.setText(filename);
                            }
//                            if (!arraylist.get(position-1).getPhoto().equalsIgnoreCase(""))left_attached_file_ll.setVisibility(View.VISIBLE);
//                            imageLoader.DisplayImage(arraylist.get(position - 1).getPhoto(), left_new_img);
                            if (!arraylist.get(position - 1).getLastname().equalsIgnoreCase("null") && arraylist.get(position - 1).getLastname() != null)
                                left_new_name_txt.setText(arraylist.get(position - 1).getFirstname() + " " + arraylist.get(position - 1).getLastname());
                            else
                                left_new_name_txt.setText(arraylist.get(position - 1).getFirstname());
                            String msg = arraylist.get(position - 1).getComment().replace("\n", " \n ");
                            msg = arraylist.get(position - 1).getComment().replace(" \n ", "<br/>");
                            left_new_msg_txt.setText(arraylist.get(position - 1).getComment());
                            left_new_time_txt.setText(arraylist.get(position - 1).getAdded_on());
                        }

                    } else {
                        convertView = mInflater.inflate(R.layout.date_section_row, null);
                        TextView date_txt = (TextView) convertView.findViewById(R.id.date_txt);
                        date_txt.setText(arraylist.get(position - 1).getDate());
                    }
                }
            }
            return convertView;
        }
    }

    public String getGroup_Id() {
        return Group_Id;
    }

    public void setGroup_Id(String group_Id) {
        Group_Id = group_Id;
    }
}
