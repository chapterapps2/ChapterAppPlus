package com.chaptervitamins.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.utility.ReadResponseUtility;

import java.util.ArrayList;

/**
 * Created by abcd on 5/23/2016.
 */
public class HistoryFragment extends BaseActivity {
    public static ArrayList<ReadResponseUtility> list=new ArrayList<>();
    private ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_fragment);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ListView listView= (ListView) findViewById(R.id.listview);
        list=new ArrayList<ReadResponseUtility>();

        for (int i = HomeActivity.mReadResponse.size()-1; i>=0; i--){
            if (HomeActivity.mReadResponse.get(i).getResponse_type().equalsIgnoreCase("quiz")){
                list.add(HomeActivity.mReadResponse.get(i));
            }
        }
        if (list.size()==0) Toast.makeText(HistoryFragment.this, "No Test History found!", Toast.LENGTH_SHORT).show();
//        Adapter adapter = new Adapter(HistoryFragment.this,list);
//        listView.setAdapter(adapter);
    }
}

