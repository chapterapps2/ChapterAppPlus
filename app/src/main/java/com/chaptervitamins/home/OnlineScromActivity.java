package com.chaptervitamins.home;

import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Vijay Antil on 10/4/2016.
 */

public class OnlineScromActivity extends BaseActivity implements View.OnClickListener, RatingListener {
    private WebView webview;
    TextView title;
    private ProgressBar progressBar3;
    String startTime;
    String endTime;
    private MeterialUtility meterialUtility;
    MixPanelManager mixPanelManager;
    private DataBase dataBase;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private Button btnPrevious, btnNext, btnNextCourse;
    private boolean isNextButtonClicked;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.activity_online_link_open);

        webview = (WebView) findViewById(R.id.webview);
        title = (TextView) findViewById(R.id.title);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnNextCourse = (Button) findViewById(R.id.btn_next_course);
        dataBase = DataBase.getInstance(this);
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        mixPanelManager = APIUtility.getMixPanelManager(this);
        meterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        setFlowingCourse();
        getModuleData();
        setModuleFlowingCourse(this, position, meterialUtilityArrayList, btnNextCourse, modulePos, moduleUtilityList);
        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);
        ((ImageView) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (WebServices.isNetworkAvailable(OnlineScromActivity.this) && meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM))
            new SubmitData(OnlineScromActivity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), meterialUtility.getCoinsAllocatedModel(), DataBase.getInstance(OnlineScromActivity.this), true).execute();

//        toolbar.setVisibility(View.GONE);
        //  String theArgumentYouWantToPass= getIntent().getStringExtra("url");
        String theArgumentYouWantToPass = "";
       /* if (meterialUtility != null && meterialUtility.getMaterial_type().equalsIgnoreCase("TINCAN-SCROM"))
            theArgumentYouWantToPass = meterialUtility.getTincan_url();
        else if (meterialUtility != null && meterialUtility.getMaterial_type().equalsIgnoreCase("FLASH"))
            theArgumentYouWantToPass = meterialUtility.getScrom_url();*/

        if (getIntent().hasExtra("url")) {
            theArgumentYouWantToPass = getIntent().getStringExtra("url");
        }
      /*  if (!getIntent().getStringExtra("name").equalsIgnoreCase("chat")) {
            theArgumentYouWantToPass = getIntent().getStringExtra("url") + "?user_id=" + WebServices.mLoginUtility.getUser_id() + "&session_token=" + APIUtility.SESSION;
////            toolbar.setVisibility(View.VISIBLE);
//            String playVideo= "<html><body style=\"margin:0; padding:0px;\"><iframe  style='margin:0; padding:0px; width:100%; height:100%;' src='"+theArgumentYouWantToPass+"' frameborder='0' allowfullscreen></iframe></body></html>";
//            webview.loadData(playVideo, "text/html", "utf-8");
        }*/
// else{
        webview.loadUrl(theArgumentYouWantToPass);
//        }
//        if (theArgumentYouWantToPass.contains("watch?v="))
//            theArgumentYouWantToPass=theArgumentYouWantToPass.repla ce("watch?v=","embed/");
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setDisplayZoomControls(false);
        webview.getSettings().setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
//        webView.getSettings().setBuiltInZoomControls(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSettings.setAllowFileAccessFromFileURLs(true);
            webSettings.setAllowUniversalAccessFromFileURLs(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
        webview.setWebViewClient(new MyWebClient());
        webview.setWebChromeClient(new MyWebViewClient());
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUseWideViewPort(true);


    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(meterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, meterialUtility.getModule_id());
    }


    /*private void setModuleFlowingCourse(){
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextModuleButtonStatus(this, position, meterialUtilityArrayList,modulePos,moduleUtilityList);
            // FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPreviousCourse);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNextCourse);
           // btnPreviousCourse.setOnClickListener(this);
            btnNextCourse.setOnClickListener(this);
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(OnlineScromActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webview != null)
            webview.destroy();
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.onPause();
        }
        endTime = DateFormat.getDateTimeInstance().format(new Date());
        mixPanelManager.selectTimeTrack(OnlineScromActivity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                , meterialUtility.getMaterial_id(), meterialUtility.getMaterial_type());

        submitDataToServer();
        if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(OnlineScromActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
            showRatingDialog(true);
        } else {
            if (webview != null)
                webview.destroy();
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(OnlineScromActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum()))
                    showRatingDialog(false);
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(OnlineScromActivity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && !TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(OnlineScromActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum()))
                    showRatingDialog(false);
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(OnlineScromActivity.this, meterialUtilityArrayList, position, true);
                break;
        /*    case R.id.btn_prev_course:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(OnlineScromActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(OnlineScromActivity.this, meterialUtility, false);
                break;*/
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(OnlineScromActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(OnlineScromActivity.this, meterialUtility, true);
                break;
        }
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(OnlineScromActivity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }

    private class MyWebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.endsWith("exit")) {
                onBackPressed();
            } else
                view.loadUrl(url);
            return true;
        }
    }

    private class MyWebViewClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            progressBar3.setProgress(newProgress);
            if (newProgress >= 100) progressBar3.setVisibility(View.GONE);
            super.onProgressChanged(view, newProgress);
        }


    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(OnlineScromActivity.this, meterialUtility.getMaterial_id(), wannaFinish, OnlineScromActivity.this);
        custom.show(fm, "");
    }

    private void submitDataToServer() {
        if (meterialUtility == null)
            return;
        String redeem = "", noOfCoins = "";
        CoinsAllocatedModel coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();

        String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility, 100, redeem));
        /*if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
            showGoldGif();
            Toast.makeText(this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
        }*/

        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, "1", "1", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, meterialUtility, "100", "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(OnlineScromActivity.this))
            if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM))
                new SubmitData(OnlineScromActivity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, DataBase.getInstance(OnlineScromActivity.this), false).execute();
            else
                new SubmitData(OnlineScromActivity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, DataBase.getInstance(OnlineScromActivity.this)).execute();

    }

    private void setFlowingCourse() {
        if (meterialUtility != null) {
            meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(meterialUtility.getMaterial_id(), true);
            position = FlowingCourseUtils.getPositionOfMeterial(meterialUtility.getMaterial_id(), meterialUtilityArrayList);
        }
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(OnlineScromActivity.this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }
}