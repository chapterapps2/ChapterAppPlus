package com.chaptervitamins.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.CustomView.DonutProgress;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.QuizUtils;
import com.chaptervitamins.quiz.ResultAssignment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

;

public class HistoryQuizResultActivity extends BaseActivity {

    @BindView(R.id.donut_progress)
    DonutProgress donutProgress;
    @BindView(R.id.tvduration)
    TextView tvduration;
    @BindView(R.id.tvbacktochapter)
    TextView tvbacktochapter;
    @BindView(R.id.total_marks_txt)
    TextView total_marks_txt;
    @BindView(R.id.tvname)
    TextView tvname;
    @BindView(R.id.tvviewanswer_ll)
    LinearLayout tvviewanswer_ll;
    int TotalMarks = 0;
    int UserTotalMarks = 0;
    int corr_que = 0, incorr_que = 0;
    DataBase dataBase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_history_quiz_result);
        ButterKnife.bind(this);
        ImageView back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dataBase=DataBase.getInstance(HistoryQuizResultActivity.this);
        tvname.setText(WebServices.mLoginUtility.getFirstname() + "!");
        tvduration.setText(" " + getIntent().getStringExtra("time") + " min");
       /* for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {
            TotalMarks = TotalMarks + Integer.parseInt(WebServices.questionUtility.getData_utils().get(i).getMarks());
            if(WebServices.questionUtility.getData_utils().get(i).getQuestion_type().equalsIgnoreCase("SINGLE")){
            if (WebServices.questionUtility.getData_utils().get(i).getUser_ans()!=null && WebServices.questionUtility.getData_utils().get(i).getUser_ans().size()>0 && WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0) != null)
                if (WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0).equalsIgnoreCase(WebServices.questionUtility.getData_utils().get(i).getCorrect_option())) {
                    UserTotalMarks = UserTotalMarks + Integer.parseInt(WebServices.questionUtility.getData_utils().get(i).getMarks());
                    corr_que++;
                } else {
                    incorr_que++;
                }
        }else{

        }
        }*/
        int size = WebServices.questionUtility.getData_utils().size();
        for (int j = 0; j < size; j++) {
            Data_util dataUtil = WebServices.questionUtility.getData_utils().get(j);
            TotalMarks = TotalMarks + Integer.parseInt(dataUtil.getMarks());
            if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                    ArrayList<String> correctAnswerAl = QuizUtils.convertStringToAl(dataUtil.getCorrect_option());
                    if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                        String correctOptionType = dataUtil.getCorrect_option_type();
                        switch (correctOptionType) {
                            case "ALL":
                                if (QuizUtils.checkAnsForAll(dataUtil.getUser_ans(), correctAnswerAl)) {
                                    UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                                    corr_que++;
                                } else
                                    incorr_que++;
                                break;
                            case "ANY":
                                if (QuizUtils.checkAnsForAny(dataUtil.getUser_ans(), correctAnswerAl)) {
                                    UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                                    corr_que++;
                                } else
                                    incorr_que++;
                                break;
                        }
                    }
                }
            } else {
                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                    if (dataUtil.getUser_ans().get(0).equalsIgnoreCase(dataUtil.getCorrect_option())) {
                        UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                        corr_que++;
                    } else {
                        incorr_que++;
                    }
                }
            }
        }

        int percent = (UserTotalMarks * 100) / TotalMarks;
        donutProgress.setStartingDegree(270);
//        total_marks_txt.setText(percent+" %");
        donutProgress.setProgress(percent);
        tvviewanswer_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HistoryQuizResultActivity.this, ResultAssignment.class);
                intent.putExtra("test_pattern", getIntent().getStringExtra("test_pattern"));
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });
        tvbacktochapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
