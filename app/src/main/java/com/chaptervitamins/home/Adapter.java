package com.chaptervitamins.home;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chaptervitamins.CustomView.Custom_Progress2;
import com.chaptervitamins.R;
import com.chaptervitamins.utility.Quiz_Response_Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Anjali on 19-Apr-16.
 */
public class Adapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<Quiz_Response_Util> mResponseUtilities=new ArrayList<>();

    // ArrayList<String> list=new ArrayList<String>();

    public Adapter(Context context1, ArrayList<Quiz_Response_Util> responseUtilities) {
        context = context1;

        mResponseUtilities=responseUtilities;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mResponseUtilities.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = null;
        TextView attempt_txt, result_txt, result_txt_bg,time_txt;
        if (view == null) {
            view = inflater.inflate(R.layout.history, null);
            attempt_txt = (TextView) view.findViewById(R.id.attempt_txt);
            result_txt = (TextView) view.findViewById(R.id.result_txt);
            time_txt = (TextView) view.findViewById(R.id.dates_txt);
            result_txt_bg = (TextView) view.findViewById(R.id.result_txt_bg);//2016-05-15 10:54:30
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm");
            SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd MMM yy");
            Custom_Progress2 progressBar = (Custom_Progress2) view.findViewById(R.id.donut_progress);
            progressBar.setStartingDegree(270);
            progressBar.setInnerBackgroundColor(Color.parseColor("#ed873e"));
            progressBar.setFinishedStrokeColor(Color.RED);
            progressBar.setDefault_stroke_width(13f);
            progressBar.setUnfinishedStrokeColor(Color.WHITE);
            if (mResponseUtilities.get(i).getResult().equalsIgnoreCase("")||mResponseUtilities.get(i).getResult()==null)
                mResponseUtilities.get(i).setResult("0");
            progressBar.setProgress(Integer.parseInt(mResponseUtilities.get(i).getResult()));
            attempt_txt.setText("Attempt "+(mResponseUtilities.size()-i));
            String t=mResponseUtilities.get(i).getFinish_time();
            java.util.Date d = null;
            if (mResponseUtilities.get(i).getIsPass().equalsIgnoreCase("pass")){
                result_txt.setText("Pass");
                result_txt_bg.setBackgroundResource(R.drawable.pass_bg);
            }
            if (mResponseUtilities.get(i).ishighest()){
                String high=result_txt.getText().toString();
                result_txt.setText(high+" (Highest)");
            }

            try {
                d = ((DateFormat) dateFormat).parse(t);
                String formattedDate = dateFormat2.format(d).toString();
                String formattedDate2 = dateFormat3.format(d).toString();
                time_txt.setText(formattedDate+" on "+formattedDate2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
//           if (mResponseUtilities.get(i).getResult().equalsIgnoreCase("100")) {
//               complete_txt.setText("COMPLETED");
//               complete_txt.setBackgroundResource(R.drawable.rounded_green_bg);
//           }else{
//               complete_txt.setText(mResponseUtilities.get(i).getResult()+"% RESULT");
//               complete_txt.setBackgroundResource(R.drawable.rounded_uncompleted_bg);
//           }
//            title_txt.setText(mResponseUtilities.get(i).getTitle());
//            sub_description.setText(mResponseUtilities.get(i).getTitle());
//
        }
        return view;
    }
}


