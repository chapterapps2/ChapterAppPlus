package com.chaptervitamins.home;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.BaseActivity;


/**
 * Created by Android on 7/16/2016.
 */

public class NewsFeedDetail_Activity extends BaseActivity {
    WebView webView;
    ProgressBar progress;
    TextView toolbar_title;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.newsfeeddetail_activity);
        toolbar_title=(TextView)findViewById(R.id.toolbar_title);
        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        progress=(ProgressBar)findViewById(R.id.progressbar);
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        ImageView back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //---you need this to prevent the webview from
        // launching another browser when a url
        // redirection occurs---
        webView.setWebChromeClient(new MyWebViewClient());
        if (getIntent().getStringExtra("type").equalsIgnoreCase("pdf")) {
            String pdfURL = getIntent().getStringExtra("link");
            webView.loadUrl(
                    "https://docs.google.com/viewer?url=" + pdfURL);
//            finish();
        } else {
            String pdfURL = getIntent().getStringExtra("link");
            webView.loadUrl(pdfURL);
        }

        changeStatusBarColor();
        toolbar_title.setText(getIntent().getStringExtra("title"));
    }

    private class MyWebViewClient extends WebChromeClient  {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }

    }


    public void setValue(int progress) {
        this.progress.setProgress(progress);
        if (progress==100){
            this.progress.setVisibility(View.GONE);
        }
    }
//        setContentView(R.layout.newsfeeddetail_activity);

//        webView=(WebView)findViewById(R.id.webView);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="+getIntent().getStringExtra("link"));

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            webView.clearCache(true);
            webView.clearHistory();
            this.finish();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        webView.clearCache(true);
        webView.clearHistory();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }
    }
}