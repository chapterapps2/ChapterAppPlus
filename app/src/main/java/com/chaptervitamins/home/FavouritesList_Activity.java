package com.chaptervitamins.home;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.Favourites_Utility;
import com.chaptervitamins.utility.Favourites_Utils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class FavouritesList_Activity extends BaseActivity {
    private ImageView back;
    private ListView listview;
    private LayoutInflater mInflater;
    private FavouritesAdapter adapter;
    private WebServices webServices;
    private TextView nofount_txt;
    private ArrayList<Favourites_Utility> arrayList = new ArrayList<>();
    private ArrayList<String> strings = new ArrayList<>();
    String COURSEID = "", COURSENAME = "";
    private DataBase dataBase;


    MixPanelManager mixPanelManager;
    private Dialog mDialog;
    private ProgressBar mBar;
    private TextView totalPer_txt, title_txt, total_file_txt, count_txt;
    private Button abort_btn = null;
    private boolean isDownload = true;
    String MSG = "Download Sucessfully!";
    private String FILEPATH = null;
    private int totallenghtOfFile = 0;
    private String totalVal = "-1";
    Handler timeout = null;
    int[] count = {10};//for 40 seconds to wait for buffering after it will finish the activity
    boolean istimeout = true;
    ArrayList<Favourites_Utils> favourites_utilses = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_favourites_list);
        listview = (ListView) findViewById(R.id.listview);
        back = (ImageView) findViewById(R.id.back);
        nofount_txt = (TextView) findViewById(R.id.nofount_txt);
        webServices = new WebServices();
        dataBase = DataBase.getInstance(FavouritesList_Activity.this);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        favourites_utilses = dataBase.getFavData(WebServices.mLoginUtility.getUser_id());
        for (int i = 0; i < favourites_utilses.size(); i++) {
            if (!WebServices.isFavouritesMaterial(favourites_utilses.get(i).getItem_id())) {
                boolean isRemove = false;
                for (int j = 0; j < WebServices.favourites_utilses.size(); j++) {
                    if (favourites_utilses.get(i).getItem_id().equalsIgnoreCase(WebServices.favourites_utilses.get(j).getItem_id())) {
                        WebServices.favourites_utilses.remove(j);
                        isRemove = true;
                    }
                }
                if (!isRemove) {
                    if (favourites_utilses.get(i).getIs_favourite().equalsIgnoreCase("yes"))
                        WebServices.favourites_utilses.add(favourites_utilses.get(i));
                }
            }
        }

        System.out.println("===favourites_utilses=====" + favourites_utilses.size());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LoadData();

        System.out.println("===arrayList==" + arrayList.size());
        if (arrayList.size() <= 1) {
            nofount_txt.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        } else {
            adapter = new FavouritesAdapter();
            listview.setAdapter(adapter);
        }
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!arrayList.get(position).isSection()) {
                    listClick(arrayList.get(position).getId());
                }
            }
        });

    }

    private void listClick(String mid) {
        boolean isAvailableMaterial = false;

        MeterialUtility meterialUtility = new MeterialUtility();
        for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {

            ArrayList<ModulesUtility> modulesUtilityArrayList = HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList();
            for (int j = 0; j < modulesUtilityArrayList.size(); j++) {
                ArrayList<MeterialUtility> meterialUtilities = modulesUtilityArrayList.get(j).getMeterialUtilityArrayList();
                for (int k = 0; k < meterialUtilities.size(); k++) {
                    if (meterialUtilities.get(k).getMaterial_id().equalsIgnoreCase(mid)) {
                        isAvailableMaterial = true;
                        meterialUtility = meterialUtilities.get(k);
                        COURSEID = HomeActivity.courseUtilities.get(i).getCourse_id();
                        COURSENAME = HomeActivity.courseUtilities.get(i).getCourse_name();
                        System.out.println("===COURSEID==" + COURSEID);
                        System.out.println("===COURSENAME==" + COURSENAME);
                        System.out.println("===MID==" + mid);
                        System.out.println("===midNAME==" + meterialUtility.getTitle());
                        break;
                    }
                }
                if (isAvailableMaterial) break;
            }
            if (isAvailableMaterial) break;
        }
        if (isAvailableMaterial) {
            if (WebServices.isNewMaterial(COURSEID, mid))
                for (int i = 0; i < WebServices.material_utils.size(); i++) {
                    if (WebServices.material_utils.get(i).getMaterial_id().equalsIgnoreCase(mid)) {
                        read_Notification(WebServices.material_utils.get(i).getNotification_id(), WebServices.material_utils.get(i).getNotification_user_id());
//                        break;
                    }
                }
            mixPanelManager = APIUtility.getMixPanelManager(FavouritesList_Activity.this);
            new MaterialOpenController(FavouritesList_Activity.this, mixPanelManager, dataBase).openMaterial(meterialUtility, true, false, false);
        }
    }

    @Override
    protected void onResume() {
        APIUtility.showFlowingCourse = false;
        super.onResume();
    }

    private void read_Notification(final String notifi_id, final String notifi_user_id) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(FavouritesList_Activity.this, APIUtility.READ_NOTIFICATION);
                    return;
                }
                for (int i = 0; i < WebServices.notificationUtilityArrayList.size(); i++) {
                    if (WebServices.notificationUtilityArrayList.get(i).getNotification_id().equalsIgnoreCase(notifi_id)) {
                        WebServices.notificationUtilityArrayList.get(i).setSent_status("READ");
                        break;
                    }
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("notification_user_id", notifi_user_id));
                nameValuePair.add(new BasicNameValuePair("notification_id", notifi_id));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("sent_status", "READ"));
                String resp = webServices.getLogin(nameValuePair, APIUtility.READ_NOTIFICATION);
                webServices.isValid(resp);
                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    private void LoadData() {
        arrayList = new ArrayList<>();
        strings = new ArrayList<>();
        for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
            for (int j = 0; j < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().size(); j++) {
                for (int k = 0; k < HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().size(); k++) {
                    if (WebServices.isFavouritesMaterial(HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k).getMaterial_id())) {
                        System.out.println("===name==" + HomeActivity.courseUtilities.get(i).getCourse_name());
                        if (!strings.contains(HomeActivity.courseUtilities.get(i).getCourse_name() + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getModule_name())) {
                            strings.add(HomeActivity.courseUtilities.get(i).getCourse_name() + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getModule_name());
                            Favourites_Utility utility = new Favourites_Utility();
                            utility.setName(HomeActivity.courseUtilities.get(i).getCourse_name() + " > " + HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getModule_name());
                            utility.setSection(true);
                            arrayList.add(utility);
                        }
                        Favourites_Utility utility = new Favourites_Utility();
                        utility.setName(HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k).getTitle());
                        utility.setSection(false);
                        utility.setId(HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k).getMaterial_id());
                        utility.setType(HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k).getMaterial_type());
                        utility.setImage(HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList().get(j).getMeterialUtilityArrayList().get(k).getMaterial_image());
                        arrayList.add(utility);
                    }
                }
            }
        }
    }

    class FavouritesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null)
                if (arrayList.get(position).isSection()) {
                    convertView = mInflater.inflate(R.layout.fav_section_row, null);
                    TextView textView = (TextView) convertView.findViewById(R.id.header_row_txt);
                    textView.setText(arrayList.get(position).getName());
                } else {
                    convertView = mInflater.inflate(R.layout.fav_row, null);
                    ImageView type_img = (ImageView) convertView.findViewById(R.id.type_img);
                    TextView todo_row_title = (TextView) convertView.findViewById(R.id.todo_row_title);
                    ImageView delete_img = (ImageView) convertView.findViewById(R.id.delete_img);
                    todo_row_title.setText(arrayList.get(position).getName());
                    Utils.setMaterialImage(FavouritesList_Activity.this, arrayList.get(position), type_img);
                    delete_img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RemoveFavoritesItem(arrayList.get(position).getId(), WebServices.getFavouriteid(arrayList.get(position).getId()), position);
                        }
                    });
                }
            return convertView;
        }
    }

    private void RemoveFavoritesItem(final String materialid, final String favourite_id, final int position) {

        if (WebServices.isNetworkAvailable(FavouritesList_Activity.this)) {
            final ProgressDialog dialog = ProgressDialog.show(FavouritesList_Activity.this, "", "Please wait..");
            dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    if (dialog != null) dialog.dismiss();
//                    adapter.notifyDataSetChanged();
                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                        Utils.callInvalidSession(FavouritesList_Activity.this, APIUtility.REMOVE_FAVORITES);
                        return;
                    } else if (msg.what == 1) {
                        Toast.makeText(FavouritesList_Activity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                    } else {
                        arrayList.remove(position);
                        Toast.makeText(FavouritesList_Activity.this, "Item has been removed from your Favourites List.", Toast.LENGTH_LONG).show();
                    }
                    for (int i = 0; i < WebServices.favourites_utilses.size(); i++) {
                        if (WebServices.favourites_utilses.get(i).getItem_id().equalsIgnoreCase(materialid)) {
                            WebServices.favourites_utilses.remove(i);
                        }
                    }
                    LoadData();
                    if (arrayList.size() <= 1) {
                        nofount_txt.setVisibility(View.VISIBLE);
                        listview.setVisibility(View.GONE);
                    } else {
                        adapter.notifyDataSetChanged();
                    }


                }
            };
            new Thread() {
                public void run() {
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("item_id", materialid));
                        nameValuePair.add(new BasicNameValuePair("favourite_id", favourite_id));
                        String resp = webServices.callServices(nameValuePair, APIUtility.REMOVE_FAVORITES);
                        Log.d(" Response:", resp.toString());
                        if (webServices.isValid(resp)) {
                            nameValuePair = new ArrayList<NameValuePair>();
                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                            resp = webServices.getLogin(nameValuePair, APIUtility.GET_ALL_FAVORITES);
                            System.out.println("====resp====" + resp);
                            if (!resp.equalsIgnoreCase("")) {
                                DataBase.getInstance(FavouritesList_Activity.this).addFavouritesData(WebServices.mLoginUtility.getUser_id(), resp);
                                webServices.getAllFavouritesItems(resp);
//                            System.out.println("====resp====" + dataBase.getFavouritesData(WebServices.mLoginUtility.getUser_id()));
                            }
                            handler.sendEmptyMessage(0);
                        } else {
                            handler.sendEmptyMessage(1);
                        }
                    }
                }
            }.start();
        } else {

            long id = dataBase.RemoveFav(WebServices.mLoginUtility.getUser_id(), materialid);

            if (id == 1) {

                for (int i = 0; i < WebServices.favourites_utilses.size(); i++) {
                    if (materialid.equalsIgnoreCase(WebServices.favourites_utilses.get(i).getItem_id())) {
//                        if (position>arrayList.size())
                        arrayList.remove(position);
                        WebServices.favourites_utilses.remove(i);
                    }
                }
            } else {
                dataBase.addFData(WebServices.mLoginUtility.getUser_id(), materialid, "MATERIAL", favourite_id, "no");
                for (int i = 0; i < WebServices.favourites_utilses.size(); i++) {
                    if (materialid.equalsIgnoreCase(WebServices.favourites_utilses.get(i).getItem_id())) {
//                        if (position>arrayList.size())
                        arrayList.remove(position);
                        WebServices.favourites_utilses.remove(i);
                    }
                }
            }
            LoadData();
            if (arrayList.size() <= 1) {
                nofount_txt.setVisibility(View.VISIBLE);
                listview.setVisibility(View.GONE);
            } else {
                adapter.notifyDataSetChanged();
            }
//            Toast.makeText(FavouritesList_Activity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();

        }
    }

}
