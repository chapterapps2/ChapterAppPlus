package com.chaptervitamins.home;

/**
 * Created by Android on 7/27/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.downloadImages.Profile_ImageLoader;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.Chat_MessagesUtils;
import com.chaptervitamins.utility.CommentsUtils;

import java.util.ArrayList;

/**
 * Created by Android on 7/16/2016.
 */

public class DiscussionActivity extends BaseActivity {
    //    private SwipeRefreshLayout swipeContainer;
    private WebServices webServices;
    private LayoutInflater mInflater;
    private Profile_ImageLoader imageLoader;
    private ListView chat_list;
    private String Group_Id = "";
    private String COURSE_NAME = "";
    private final int REQUEST_CHOOSER = 2000;
    private final int REQUEST_CREATETOPIC = 1000;
    private ImageView attached_img, send_img;
    private EditText editText, search_edt;
    private LinearLayout chat_ll, chat_blank_ll, search_ll;
    ArrayList<Chat_MessagesUtils> tempchat_messagesUtilses = new ArrayList<>();

    ArrayList<Chat_MessagesUtils> chat_messagesUtilses2 = new ArrayList<>();
    public static ArrayList<CommentsUtils> commentsUtilsArrayList = new ArrayList<>();
    ProgressDialog progressDialog;
    String FILE_PATH = "";
    Toolbar toolbar;
    TextView toolbar_title;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter adapter;
    private Show_All_Topic tab1;
    private Show_All_Members tab2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_suggestion__main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.title_txt);
        toolbar.setVisibility(View.VISIBLE);
        toolbar_title.setText("Discussions");
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView fab = (ImageView) findViewById(R.id.fab);
        if (WebServices.mLoginUtility.getRole_id() != null && APIUtility.ROLE_ID != null)
            fab.setVisibility(View.VISIBLE);
            /*if (WebServices.mLoginUtility.getRole_id().equalsIgnoreCase(APIUtility.ROLE_ID))
                fab.setVisibility(View.VISIBLE);
            else
                fab.setVisibility(View.GONE);*/
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DiscussionActivity.this, CreateTopicActivity.class);
                intent.putExtra("groupid", getGroup_Id());
                startActivityForResult(intent, REQUEST_CREATETOPIC);
            }
        });



        setGroup_Id(getIntent().getStringExtra("groupid"));
        webServices = new WebServices();
        imageLoader = new Profile_ImageLoader(DiscussionActivity.this);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout.addTab(tabLayout.newTab().setText("Topics"));
        tabLayout.addTab(tabLayout.newTab().setText("Members"));
        tabLayout.setBackground(createShapeByColor(Utils.getColorPrimary(),0,0,Utils.getColorPrimary()));
        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (viewPager != null)
                    viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case REQUEST_CREATETOPIC:
                    if(adapter!=null){
                        Fragment fragment = adapter.getItem(0);
                        if(fragment!=null && fragment instanceof Show_All_Topic){
                            ((Show_All_Topic) fragment).notifyAdapter();
                        }
//                        adapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        adapter.notifyDataSetChanged();
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        public PagerAdapter(android.support.v4.app.FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }
        @Override
        public Fragment getItem(int position) {
            String tabname = tabLayout.getTabAt(position).getText().toString();
            if (tabname.equalsIgnoreCase("Topics")) {
                tab1 = new Show_All_Topic();
                tab1.setCOURSE_NAME(COURSE_NAME);
                tab1.setGroup_Id(Group_Id);
//                tab1.notifydata();
                return tab1;
            } else if (tabname.equalsIgnoreCase("members")) {
                tab2 = new Show_All_Members();
                return tab2;
            } else return null;

        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            this.finish();

        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        new AutoLogout().autoLogout(DiscussionActivity.this);
    }

    public String getGroup_Id() {
        return Group_Id;
    }

    public void setGroup_Id(String group_Id) {
        Group_Id = group_Id;
    }

    public String getCOURSE_NAME() {
        return COURSE_NAME;
    }

    public void setCOURSE_NAME(String COURSE_NAME) {
        this.COURSE_NAME = COURSE_NAME;
    }
}