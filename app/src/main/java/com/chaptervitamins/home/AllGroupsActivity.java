package com.chaptervitamins.home;

/**
 * Created by Android on 8/1/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.discussions.AddGroupActivity;
import com.chaptervitamins.downloadImages.ImageLoader;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.GroupUtils;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Android on 7/1/2016.
 */

public class AllGroupsActivity extends BaseActivity implements ObservableScrollViewCallbacks {
    //    private SwipeRefreshLayout swipeContainer;
    private WebServices webServices;
    private LayoutInflater mInflater;
    private ImageLoader imageLoader;
    private GroupAdapter adapter;
    private ObservableListView lvItems;
    private String Group_Id = "";
    private String course_name = "";
    private String CourseID = "";
    private TextView add_private_group;
    ArrayList<GroupUtils> groupUtilsArrayList = new ArrayList<>();
    ArrayList<GroupUtils> tempgroupUtilsArrayList = new ArrayList<>();
    ArrayList<GroupUtils> groupUtilsArrayList2 = new ArrayList<>();
    EditText search_edt;
    AppBarLayout appbar;
    ImageView back;
    LinearLayout search_ll;
    private ImageView fab;
    private DataBase dataBase;
    Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.groups_activity);
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
////        Mint.initAndStartSession(getActivity(), APIUtility.CRASH_REPORT_KEY);
//        View v = inflater.inflate(R.layout.groups_activity, container, false);
//        setContentView(R.layout.groups_activity);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dataBase = DataBase.getInstance(AllGroupsActivity.this);
        search_ll = (LinearLayout) findViewById(R.id.search_ll);
        search_edt = (EditText) findViewById(R.id.search_edt);
        fab = (ImageView) findViewById(R.id.fab);
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getRole_id()) && WebServices.mLoginUtility.getRole_id().equalsIgnoreCase(APIUtility.ROLE_ID))
            fab.setVisibility(View.VISIBLE);
//        else
            fab.setVisibility(View.GONE);
        search_ll.setVisibility(View.VISIBLE);
        appbar.setVisibility(View.VISIBLE);
//        MyApplication.getInstance().trackScreenView("Chat");
        ((TextView) findViewById(R.id.title_txt)).setText(getIntent().getStringExtra("name"));
        webServices = new WebServices();
        imageLoader = new ImageLoader(AllGroupsActivity.this);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        add_private_group = (TextView) findViewById(R.id.add_private_group);
        lvItems = (ObservableListView) findViewById(R.id.lvItems);
        adapter = new GroupAdapter();
        lvItems.setAdapter(adapter);
        lvItems.setScrollViewCallbacks(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllGroupsActivity.this, AddGroupActivity.class);
                startActivityForResult(intent, 2000);
            }
        });
        search_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = search_edt.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });
        if (WebServices.groupUtilsArrayList.size() != 0) {
            GroupUtils groupUtils = new GroupUtils();
            tempgroupUtilsArrayList = new ArrayList<>();
            for (int i = 0; i < WebServices.groupUtilsArrayList.size(); i++) {

                if (WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("super") || WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("private")) {
                    groupUtils = WebServices.groupUtilsArrayList.get(i);
//                    groupUtilsArrayList = new ArrayList<>();
//                    groupUtilsArrayList2 = new ArrayList<>();
//            groupUtils.setGroup_name("Organization Group");
//            groupUtils.setGroup_members("All employees");
                    groupUtilsArrayList.add(groupUtils);
                    groupUtilsArrayList2.add(groupUtils);
                } /*else {
                    tempgroupUtilsArrayList.add(WebServices.groupUtilsArrayList.get(i));
                }*/

            }


//            groupUtilsArrayList.addAll(tempgroupUtilsArrayList);
//            groupUtilsArrayList2.addAll(tempgroupUtilsArrayList);
        } else if (WebServices.isNetworkAvailable(this)){
            ArrayList nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
            new GenericApiCall(this, APIUtility.GETALLGROUP, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    String resp = (String) result;
                    if (webServices.isValid(resp)) {
                        if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "All_Groups"))
                            dataBase.addallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                        else {
                            dataBase.updateallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                        }
                        webServices.getAllGroup(resp);
                        if (WebServices.groupUtilsArrayList.size() != 0) {
                            GroupUtils groupUtils = new GroupUtils();
                            tempgroupUtilsArrayList = new ArrayList<>();
                            for (int i = 0; i < WebServices.groupUtilsArrayList.size(); i++) {
                                if (WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("super") || WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("private")) {
                                    groupUtils = WebServices.groupUtilsArrayList.get(i);
                                    groupUtilsArrayList.add(groupUtils);
                                    groupUtilsArrayList2.add(groupUtils);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onError(ErrorModel error) {
                    DialogUtils.showDialog(AllGroupsActivity.this,error.getErrorDescription());
                }
            }).execute();


        }
        add_private_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AllGroupsActivity.this, AddGroupActivity.class);
                intent.putExtra("courseid", getCourseID());
                startActivityForResult(intent, 2000);
            }
        });
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                if (WebServices.isNetworkAvailable(AllGroupsActivity.this)) {

//                    if (groupUtilsArrayList.get(position).getGroup_type().equalsIgnoreCase("super")) {
//
//                                    Intent intent = new Intent(AllGroupsActivity.this, cussionActivity.class);
//                                    intent.putExtra("name", groupUtilsArrayList.get(position).getGroup_name());
//                                    intent.putExtra("courseid", groupUtilsArrayList.get(position).getCourse_id());
//                                    intent.putExtra("groupid", groupUtilsArrayList.get(position).getGroup_id());
//                                    startActivityForResult(intent,2000);
////
//                    } else {
                    final ProgressDialog dialog = ProgressDialog.show(AllGroupsActivity.this, "", "Please wait...");
                    dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                     handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (dialog != null && dialog.isShowing() && !isFinishing())
                                dialog.dismiss();
                            if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                                Utils.callInvalidSession(AllGroupsActivity.this,APIUtility.GETALLGROUPMEMBERS);
                            } else {
                                Intent intent = new Intent(AllGroupsActivity.this, DiscussionActivity.class);
                                intent.putExtra("name", groupUtilsArrayList.get(position).getGroup_name());
                                intent.putExtra("courseid", groupUtilsArrayList.get(position).getCourse_id());
                                intent.putExtra("groupid", groupUtilsArrayList.get(position).getGroup_id());
                                startActivityForResult(intent, 2000);
//                                    Intent intent = new Intent(AllGroupsActivity.this, GroupsSelectionActivity.class);
//                                    intent.putExtra("name", groupUtilsArrayList.get(position).getGroup_name());
//                                    intent.putExtra("courseid", groupUtilsArrayList.get(position).getCourse_id());
//                                    intent.putExtra("groupid", groupUtilsArrayList.get(position).getGroup_id());
//                                    intent.putExtra("userid", groupUtilsArrayList.get(position).getUser_id());
//                                    startActivityForResult(intent, 2000);
                            }
                        }
                    };
                    new Thread() {
                        @Override
                        public void run() {
//                                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
//                                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
//                                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
//                                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
//                                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
//                                nameValuePair.add(new BasicNameValuePair("group_id", groupUtilsArrayList.get(position).getGroup_id()));
//                                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
//                                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
//                                nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
//                                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
//                                String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
//                                Log.d(" Response:", resp.toString());
//                                if (!webServices.isValid(resp)){
//                                    handler.sendEmptyMessage(1);
//                                    return;
//                                }
//                                webServices.getGroupchatMessages(resp);
                            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                            nameValuePair.add(new BasicNameValuePair("group_id", groupUtilsArrayList.get(position).getGroup_id()));
                            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                            nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                            String resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUPMEMBERS);
                            Log.d(" Response:", resp.toString());
                            if (!webServices.isValid(resp)) {
                                handler.sendEmptyMessage(1);
                                return;
                            }
                            webServices.getAllGroupMembers(resp);

                            handler.sendEmptyMessage(0);
                        }
                    }.start();
//                    }
                } else {
                    Toast.makeText(AllGroupsActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });
         handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                swipeContainer.setRefreshing(false);
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(AllGroupsActivity.this,APIUtility.GETALLGROUPMEMBERS);
                } else {
                    if (WebServices.groupUtilsArrayList.size() != 0) {
                        search_edt.setText("");
                        groupUtilsArrayList.clear();
                        groupUtilsArrayList2.clear();
                        GroupUtils groupUtils = new GroupUtils();
                        tempgroupUtilsArrayList.clear();
                        for (int i = 0; i < WebServices.groupUtilsArrayList.size(); i++) {

//                            if (WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("super")||WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("private")) {
                            groupUtils = WebServices.groupUtilsArrayList.get(i);
                            groupUtilsArrayList.add(groupUtils);
                            groupUtilsArrayList2.add(groupUtils);
                            /* }else {
                            tempgroupUtilsArrayList.add(WebServices.groupUtilsArrayList.get(i));
                        }*/

                        }
//                    groupUtils.setGroup_name("Organization Group");
//                    groupUtils.setGroup_members("All employees");

//                    groupUtilsArrayList.addAll(tempgroupUtilsArrayList);
//                    groupUtilsArrayList2.addAll(tempgroupUtilsArrayList);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                String groupid = "0";
                if (groupUtilsArrayList == null) return;
                for (int i = 0; i < groupUtilsArrayList.size(); i++) {
                    if (groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("Super"))
                        groupid = groupUtilsArrayList.get(i).getGroup_id();
                    break;
                }
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", groupid));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUPMEMBERS);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)) {
                    handler.sendEmptyMessage(1);
                    return;
                }
                webServices.getAllGroupMembers(resp);
                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUP);
                Log.d(" Response:", resp.toString());
                if (webServices.isValid(resp)) {
                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "All_Groups"))
                        dataBase.addallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                    else {
                        dataBase.updateallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                    }
                    webServices.getAllGroup(resp);
                }
                handler.sendEmptyMessage(0);
            }
        }.start();
//         Setup refresh listener which triggers new data loading
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//                search_edt.setText("");
//                fetchTimelineAsync(0);
//            }
//        });
//        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);

//        return v;
    }


    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(AllGroupsActivity.this);
        if (search_edt != null)
            search_edt.setText("");
    }


    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (search_ll == null) {
            return;
        }
        if (scrollState == ScrollState.UP) {
            if (search_ll.isShown()) {
                search_ll.setVisibility(View.GONE);
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!search_ll.isShown()) {
                search_ll.setVisibility(View.VISIBLE);
            }
        }
    }

    public void fetchTimelineAsync(int page) {

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                swipeContainer.setRefreshing(false);
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(AllGroupsActivity.this,APIUtility.GETALLGROUPMEMBERS);
                } else {
                    groupUtilsArrayList.clear();
                    groupUtilsArrayList2.clear();
                    for (int i = 0; i < WebServices.groupUtilsArrayList.size(); i++) {
//            if (WebServices.groupUtilsArrayList.get(i).getCourse_id().equalsIgnoreCase(Material_Activity.ASSIGNEDCOURSEID))
                        groupUtilsArrayList.add(WebServices.groupUtilsArrayList.get(i));
                        groupUtilsArrayList2.add(WebServices.groupUtilsArrayList.get(i));
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUPMEMBERS);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)) {
                    handler.sendEmptyMessage(1);
                    return;
                }
                webServices.getAllGroupMembers(resp);
                handler.sendEmptyMessage(0);
            }
        }.start();

    }


    private class GroupAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return groupUtilsArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.member_row, null);
                ImageView profile_img = (ImageView) convertView.findViewById(R.id.profile_image);
                TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                TextView member_count_txt = (TextView) convertView.findViewById(R.id.member_count_txt);
                profile_img.setImageResource(R.drawable.group);
                profile_img.setVisibility(View.GONE);
                member_count_txt.setVisibility(View.VISIBLE);
                member_count_txt.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
//                imageLoader.DisplayImage(WebServices.membersUtilses.get(position).getPhoto(), profile_img);
                if (groupUtilsArrayList.get(position).getGroup_type().equalsIgnoreCase("Super")) {
                    member_name.setTypeface(Typeface.DEFAULT_BOLD);
                    member_count_txt.setText(groupUtilsArrayList.get(position).getGroup_members() + " member(s)");
                } else {
                    member_count_txt.setText(groupUtilsArrayList.get(position).getGroup_members() + " members");
                }
                member_name.setText(groupUtilsArrayList.get(position).getGroup_name());

                if (position % 4 == 0) {
                    member_name.setTextColor(Color.parseColor("#ed873e"));
                } else if (position % 4 == 1) {
                    member_name.setTextColor(Color.parseColor("#6fb5ec"));
                } else if (position % 4 == 2) {
                    member_name.setTextColor(Color.parseColor("#4dbfb0"));
                } else if (position % 4 == 3) {
                    member_name.setTextColor(Color.parseColor("#bf564d"));
                }
            }

            return convertView;
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            groupUtilsArrayList.clear();
            if (charText.length() == 0) {
                groupUtilsArrayList.addAll(groupUtilsArrayList2);
            } else {
                for (GroupUtils wp : groupUtilsArrayList2) {
                    if (wp.getGroup_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                        groupUtilsArrayList.add(wp);
                    }
                }
            }
            if (groupUtilsArrayList.size() == 0)
                Toast.makeText(AllGroupsActivity.this, "No result found", Toast.LENGTH_LONG).show();
            notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2000) {
            try {
                final ProgressDialog dialog = ProgressDialog.show(AllGroupsActivity.this, "", "Refreshing...");
                dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (dialog != null && dialog.isShowing() && !isFinishing()) dialog.dismiss();
                        adapter.notifyDataSetChanged();
                    }
                };
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
            new Thread() {
                @Override
                public void run() {
                    /*try {
                        sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {*/
                        groupUtilsArrayList.clear();
                        groupUtilsArrayList2.clear();
                        GroupUtils groupUtils = new GroupUtils();
                        tempgroupUtilsArrayList.clear();
                        for (int i = 0; i < WebServices.groupUtilsArrayList.size(); i++) {

                            if (WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("super") || WebServices.groupUtilsArrayList.get(i).getGroup_type().equalsIgnoreCase("private")) {
                                groupUtils = WebServices.groupUtilsArrayList.get(i);
                                groupUtilsArrayList.add(groupUtils);
                                groupUtilsArrayList2.add(groupUtils);
                            } else {
                                tempgroupUtilsArrayList.add(WebServices.groupUtilsArrayList.get(i));
                            }

                        }
//                        groupUtils.setGroup_name("Organization Group");
//                        groupUtils.setGroup_members("All employees");

//                        groupUtilsArrayList.addAll(tempgroupUtilsArrayList);
//                        groupUtilsArrayList2.addAll(tempgroupUtilsArrayList);
                        handler.sendEmptyMessage(0);
                    }
              //  }
            }.start();


        }
    }

    public String getGroup_Id() {
        return Group_Id;
    }

    public void setGroup_Id(String group_Id) {
        Group_Id = group_Id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }
}