package com.chaptervitamins.home;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.downloadImages.Profile_ImageLoader;
import com.chaptervitamins.utility.GroupMembersUtils;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Android on 12/20/2016.
 */

public class Show_All_Members extends Fragment {
    private ListView listView;
    private LayoutInflater mInflater;
    private Profile_ImageLoader imageLoader;
    private MyMemberAdapter adapter;
    private ArrayList<GroupMembersUtils> arrayList = new ArrayList<>();
    private ArrayList<GroupMembersUtils> arrayList2 = new ArrayList<>();
    EditText search_edt;
    AppBarLayout toolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.member_list_activity, container, false);
        toolbar = (AppBarLayout) view.findViewById(R.id.appbar);
        toolbar.setVisibility(View.GONE);
        search_edt = (EditText) view.findViewById(R.id.search_edt);
        arrayList = new ArrayList<>();
        arrayList2 = new ArrayList<>();

        arrayList.addAll(WebServices.membersUtilses);
        arrayList2.addAll(WebServices.membersUtilses);

        imageLoader = new Profile_ImageLoader(getActivity());
        listView = (ListView) view.findViewById(R.id.member_list);
        mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        adapter = new MyMemberAdapter();
        listView.setAdapter(adapter);
        search_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = search_edt.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }


    private class MyMemberAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.member_list_row, null);
                CircleImageView profile_img = (CircleImageView) convertView.findViewById(R.id.profile_image);
                TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                final RadioButton radioButton = (RadioButton) convertView.findViewById(R.id.radioButton);
                LinearLayout member_ll = (LinearLayout) convertView.findViewById(R.id.member_ll);
                TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt);
                member_ll.setVisibility(View.VISIBLE);
                profile_img.setVisibility(View.GONE);
                radioButton.setVisibility(View.GONE);
                member_fname_txt.setText("");
                if ((position - 1) % 4 == 1) {
                    member_ll.setBackgroundResource(R.drawable.test_series_bg);
                } else if ((position - 1) % 4 == 2) {
                    member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                } else if ((position - 1) % 4 == 0) {
                    member_ll.setBackgroundResource(R.drawable.light_blue_bg);
                } else if ((position - 1) % 4 == 3) {
                    member_ll.setBackgroundResource(R.drawable.audio_bg);
                }
                if (arrayList.get(position) != null) {
                    String fullName = arrayList.get(position).getFirstname().trim().toString();
                    if (fullName.length() > 1) {
                        String fname = fullName.substring(0, 1);
                        member_fname_txt.setText(fname);
                    }

                }
                radioButton.setChecked(arrayList.get(position).isSelected());
//                imageLoader.DisplayImage(arrayList.get(position).getPhoto(), profile_img);
//                if (!arrayList.get(position).getLastname().equalsIgnoreCase("null") && arrayList.get(position).getLastname() != null)
//                    member_name.setText(arrayList.get(position).getFirstname() + " " + arrayList.get(position).getLastname());
//                else
                member_name.setText(arrayList.get(position).getFirstname());
                if (position % 4 == 0) {
                    member_name.setTextColor(Color.parseColor("#ed873e"));
//                    profile_img.setBorderColor(Color.parseColor("#ed873e"));
                } else if (position % 4 == 1) {
                    member_name.setTextColor(Color.parseColor("#6fb5ec"));
//                    profile_img.setBorderColor(Color.parseColor("#6fb5ec"));
                } else if (position % 4 == 2) {
                    member_name.setTextColor(Color.parseColor("#4dbfb0"));
//                    profile_img.setBorderColor(Color.parseColor("#4dbfb0"));
                } else if (position % 4 == 3) {
                    member_name.setTextColor(Color.parseColor("#bf564d"));
//                    profile_img.setBorderColor(Color.parseColor("#bf564d"));
                }

            }
            return convertView;
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            arrayList.clear();
            if (charText.length() == 0) {
                arrayList.addAll(arrayList2);
            } else {
                for (GroupMembersUtils wp : arrayList2) {
                    String memberName = "";
//                    if (!wp.getLastname().equalsIgnoreCase("null") && wp.getLastname() != null) {
//                        memberName = wp.getFirstname() + " " + wp.getLastname();
//                    } else {
                    memberName = wp.getFirstname();
//                    }
                    if (memberName.toLowerCase(Locale.getDefault()).contains(charText)) {
                        arrayList.add(wp);
                    }
                }
            }
            if (arrayList.size() == 0)
                Toast.makeText(getActivity(), "No result found", Toast.LENGTH_LONG).show();
            notifyDataSetChanged();
        }
    }
}
