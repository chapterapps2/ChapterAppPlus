package com.chaptervitamins.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abcd on 5/10/2016.
 */
public class ChangePass_Activity extends AppCompatActivity {
    @BindView(R.id.old_pass_edit_text)
    EditText old_pass_edit_text;
    @BindView(R.id.new_pass_edit_text)
    EditText new_pass_edit_text;
    @BindView(R.id.reset_pass_button)
    Button reset_pass_button;
    @BindView(R.id.go_back)
    TextView go_back;
    @BindView(R.id.changepass_txt)
    TextView changepass_txt;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    private UserLoginTask mAuthTask = null;
    private DataBase dataBase;
    // UI references.
    private View mProgressView;
    private View mLoginFormView;
    private Handler handler;
    private WebServices webServices;
    private ProgressDialog mDialog;
    private ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword);
        setStatusbarColor();
        setToolbarColor();
        // Check if no view has focus:
//        changeStatusBarColor();
        ButterKnife.bind(this);
        back=(ImageView)findViewById(R.id.back);
        toolbar_title.setText("Change Password");
        dataBase = DataBase.getInstance(ChangePass_Activity.this);
        webServices = new WebServices();
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra("loginscreen").equalsIgnoreCase("login")){
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.clear();
                    editor.commit();
                }
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra("loginscreen").equalsIgnoreCase("login")){
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.clear();
                    editor.commit();
                }
                finish();
            }
        });
        reset_pass_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
        if (getIntent().getStringExtra("loginscreen").equalsIgnoreCase("login")){
            old_pass_edit_text.setText("12345");
            old_pass_edit_text.setEnabled(false);
            changepass_txt.setVisibility(View.VISIBLE);

        }
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        old_pass_edit_text.setError(null);
        new_pass_edit_text.setError(null);
        // Store values at the time of the login attempt.
        String oldpass = old_pass_edit_text.getText().toString();
        String newpass = new_pass_edit_text.getText().toString();
        // Check for a valid email address.
        if (TextUtils.isEmpty(oldpass)) {
            Toast.makeText(ChangePass_Activity.this, getString(R.string.error_invalid_email), Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(newpass)) {
            Toast.makeText(ChangePass_Activity.this, getString(R.string.error_invalid_email), Toast.LENGTH_LONG).show();
            return;
        }
        if (newpass.length()<5) {
            Toast.makeText(ChangePass_Activity.this, "The Password should be at least 5 characters long!", Toast.LENGTH_LONG).show();
            return;
        }
        if (oldpass.equalsIgnoreCase(newpass)) {
            Toast.makeText(ChangePass_Activity.this, "New Password cannot be same as the Old Password", Toast.LENGTH_LONG).show();
            return;
        }
        if (WebServices.isNetworkAvailable(ChangePass_Activity.this)) {

            mAuthTask = new UserLoginTask(oldpass, newpass);
            mAuthTask.execute((Void) null);


        } else {
            Toast.makeText(ChangePass_Activity.this, "No connectivity! Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            this.finish();
        return true;
    }
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    @Override
    protected void onResume() {
        super.onResume();
//        new AutoLogout().autoLogout(ChangePass_Activity.this);
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mOldpass;
        private final String mNewpass;
        private ProgressDialog progressDialog;


        UserLoginTask(String oldpass, String newpass) {

            mOldpass = oldpass;
            mNewpass = newpass;
            progressDialog = new ProgressDialog(ChangePass_Activity.this);
            progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
            progressDialog.setMessage("Please wait...");
            progressDialog.setTitle("");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
// Building post parameters, key and value pair
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("old_password", mOldpass));
            nameValuePair.add(new BasicNameValuePair("new_password", mNewpass));
            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid",APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            String resp = webServices.getLogin(nameValuePair, APIUtility.CHANGE_PASSWORD);
            Log.d(" Response:", resp.toString());
            boolean isSuccess = webServices.changePassword(resp);

            return isSuccess;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            if (progressDialog != null) progressDialog.dismiss();

            if (success) {
                dataBase.UpdatePassword(WebServices.mLoginUtility.getEmail(),mNewpass);
                MixPanelManager mixPanelManager= APIUtility.getMixPanelManager(ChangePass_Activity.this);
                mixPanelManager.changepassword(ChangePass_Activity.this,WebServices.mLoginUtility.getEmail(),mOldpass,mNewpass);
                if (getIntent().getStringExtra("loginscreen").equalsIgnoreCase("login")){
                    Intent intent = new Intent(ChangePass_Activity.this, HomeActivity.class);
                    intent.putExtra("loginscreen", "login");
					intent.putExtra("sms_url", "");
                    startActivity(intent);
                    SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                    editor.putString("pass", mNewpass);
                    editor.commit();
                }
                finish();

            }
            Toast.makeText(ChangePass_Activity.this, WebServices.ERRORMSG, Toast.LENGTH_SHORT).show();

        }
    }

    private void setStatusbarColor() {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                    if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1())) {
                        window.setStatusBarColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color1()));
                    } else
                        window.setStatusBarColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color1()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setToolbarColor() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        try {
            if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1())) {
                    toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(WebServices.mLoginUtility.getBranch_color1())));
                } else
                    toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(WebServices.mLoginUtility.getOrganization_color1())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
