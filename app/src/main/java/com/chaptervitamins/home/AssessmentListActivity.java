package com.chaptervitamins.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.MaterialAdapter;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssessmentListActivity extends AppCompatActivity implements View.OnClickListener{
    @BindView(R.id.toolbar_title)
    TextView toolTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ArrayList<MeterialUtility> allAssessmentMateraialaList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_list);
        ButterKnife.bind(this);
        setDataIntoViews();
        setListenerIntoViews();
    }

    private void setListenerIntoViews() {
    findViewById(R.id.back).setOnClickListener(this);

    }

    private void setDataIntoViews() {
        toolTitle.setText("Assessments");
        allAssessmentMateraialaList=FlowingCourseUtils.getAllAssessmentMaterials();
        MaterialAdapter materialAdapter=new MaterialAdapter(allAssessmentMateraialaList,true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(materialAdapter);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.back)
            finish();
    }
}
