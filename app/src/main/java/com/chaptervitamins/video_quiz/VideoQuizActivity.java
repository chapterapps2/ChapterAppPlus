package com.chaptervitamins.video_quiz;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.play_video.JW_Player_Activity;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.QuizUtils;
import com.chaptervitamins.quiz.ThankYou_Activity;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.MeterialUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoQuizActivity extends BaseActivity implements View.OnClickListener, RatingListener {
    @BindView(R.id.back)
    ImageView ivBack;
    @BindView(R.id.title_txt)
    TextView mTvTitle;
    @BindView(R.id.tv_question)
    TextView mTvQuestion;
    @BindView(R.id.count_txt)
    TextView mTvCount;
    @BindView(R.id.commentEditView)
    EditText commentEditView;
    @BindView(R.id.previous_btn)
    Button previous_btn;
    @BindView(R.id.next_btn)
    Button next_btn;
    @BindView(R.id.rl_jw_player)
    LinearLayout rlJwPlayer;
    @BindView(R.id.ll_video)
    LinearLayout llVideo;
    @BindView(R.id.iv_play)
    ImageView ivPlay;
    @BindView(R.id.tv_upload_new)
    TextView tvUploadNew;

    private MeterialUtility mMeterialutility;
    private DataBase mDatabase;
    private WebServices mWebServices;
    private ArrayList<Data_util> list;
    private int currentQuestion = 0;
    private Uri fileUri;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int RESULT_LOAD_VIDEO = 3;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    private static final int CAMERA = 101;
    private static final int GALLERY = 102;
    //private JWPlayerView jwPlayerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_video_quiz2);
        mDatabase = DataBase.getInstance(VideoQuizActivity.this);
        mWebServices = new WebServices();
        ButterKnife.bind(this);
        setListeners();
        mMeterialutility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        if (mMeterialutility != null)
//            mMeterialutility.setMaterialStartTime(df.format(c.getTime()));
        mixPanelManager.startquizflash(VideoQuizActivity.this, WebServices.mLoginUtility.getEmail(), mMeterialutility.getTitle(), "Video Quiz");
        mTvTitle.setText(mMeterialutility.getTitle());
        if (savedInstanceState != null) {
            list = (ArrayList<Data_util>) savedInstanceState.getSerializable("list");
            currentQuestion = savedInstanceState.getInt("pos", 0);
            if (list != null && list.size() > 0) {
                showQuestion(currentQuestion);
                showUploadedVideo();
            } else {
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {
            String resp = mDatabase.getQuizData(WebServices.mLoginUtility.getUser_id(), mMeterialutility.getMaterial_id());
            if (!TextUtils.isEmpty(resp)) {
                if (mWebServices.isValid(resp)) {
                    WebServices.questionUtility = mWebServices.parseQuestionData(resp);
                    list = WebServices.questionUtility.getData_utils();
                    if (list != null && list.size() > 0) {
                        showQuestion(currentQuestion);
                        showUploadedVideo();
                    } else {
                        Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("list", list);
        outState.putInt("pos", currentQuestion);
    }

    private void setListeners() {
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.ll_upload_video).setOnClickListener(this);
        findViewById(R.id.ll_record_video).setOnClickListener(this);
        findViewById(R.id.next_btn).setOnClickListener(this);
        findViewById(R.id.previous_btn).setOnClickListener(this);
        ivPlay.setOnClickListener(this);
        tvUploadNew.setOnClickListener(this);
    }

    /*private void stopPlayer() {
        if (jwPlayerView != null) {
            jwPlayerView.stop();
            jwPlayerView = null;
        }
    }*/

    private void showQuestion(int currentQuestion) {
        if (!TextUtils.isEmpty(list.get(currentQuestion).getUser_input()))
            commentEditView.setText(list.get(currentQuestion).getUser_input());
        else
            commentEditView.getText().clear();
        llVideo.setVisibility(View.VISIBLE);
        rlJwPlayer.setVisibility(View.GONE);
        //stopPlayer();
        if (list != null && currentQuestion < list.size()) {
            Data_util dataUtil = list.get(currentQuestion);
            if (currentQuestion == 0) {
                previous_btn.setVisibility(View.GONE);
                next_btn.setText("Next");
            } else if (currentQuestion == list.size() - 1) {
                previous_btn.setVisibility(View.VISIBLE);
                next_btn.setText("Done");
            } else {
                previous_btn.setVisibility(View.VISIBLE);
                next_btn.setText("Next");
            }
            if (dataUtil != null) {
                mTvQuestion.setText("Q" + (currentQuestion + 1) + ". " + Html.fromHtml(dataUtil.getQuestion_description()));
                mTvCount.setText((currentQuestion + 1) + "/" + list.size());
                showUploadedVideo();
            } else {
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (WebServices.isNetworkAvailable(VideoQuizActivity.this) && mMeterialutility != null && TextUtils.isEmpty(mMeterialutility.getRateNum())) {
            FragmentManager fm = getSupportFragmentManager();
            CustomDialog custom = new CustomDialog();
            custom.setParamCustomDialog(VideoQuizActivity.this, mMeterialutility.getMaterial_id(), true, VideoQuizActivity.this);
            custom.show(fm, "");
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mixPanelManager.quitmaterial(VideoQuizActivity.this, WebServices.mLoginUtility.getEmail(), mMeterialutility.getTitle(), "Quiz", "0");

                onBackPressed();
                break;
            case R.id.ll_record_video:
                checkPersmission(CAMERA);
                break;
            case R.id.ll_upload_video:
                checkPersmission(GALLERY);
                break;
            case R.id.next_btn:
                list.get(currentQuestion).setUser_input(commentEditView.getText().toString().trim());
                if (currentQuestion == list.size() - 1) {
                    saveQuizResponse();
                } else {
                    currentQuestion++;
                    showQuestion(currentQuestion);
                }
                break;
            case R.id.previous_btn:
                if (currentQuestion != 0) {
                    list.get(currentQuestion).setUser_input(commentEditView.getText().toString().trim());
                    currentQuestion--;
                    showQuestion(currentQuestion);
                }
                break;
            case R.id.tv_upload_new:
                /*stopPlayer();
                rlJwPlayer.setVisibility(View.GONE);
                llVideo.setVisibility(View.VISIBLE);
                if (jwPlayerView != null) {
                    jwPlayerView.destroySurface();
                }*/
                break;
            case R.id.iv_play:
                playVideo();
                break;

        }
    }

    private void playVideo() {
        String response = list.get(currentQuestion).getResponse();
        if (!TextUtils.isEmpty(response)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject != null) {
                    JSONObject dataObject = jsonObject.optJSONObject("data");
                    if (dataObject != null) {
                        String fileUrl = dataObject.optString("file_url");
                        if (!TextUtils.isEmpty(fileUrl)) {
                            ivPlay.setVisibility(View.GONE);

                           /* final PlaylistItem video = new PlaylistItem(fileUrl);
                            jwPlayerView.load(video);
                            jwPlayerView.addOnErrorListener(new VideoPlayerEvents.OnErrorListenerV2() {
                                @Override
                                public void onError(ErrorEvent errorEvent) {
                                    if (errorEvent.getMessage().equalsIgnoreCase("Invalid HTTP response code: 404: Not Found")) {
                                        mPlayerView.load(video);
                                    } else
                                        callOnlinePlayActivity();
                                }
                            });*/

/*
                            PlayerConfig playerConfig = new PlayerConfig.Builder()
                                    .file(fileUrl).autostart(true)
                                    .build();
                            jwPlayerView = new JWPlayerView(VideoQuizActivity.this, playerConfig);
*/
                          /*  jwPlayerView.setFullscreen(false, false);
//                            jwPlayerView.setControls(true);
                            jwPlayerView.addOnAdErrorListener(new AdvertisingEvents.OnAdErrorListener() {
                                @Override
                                public void onAdError(String s, String s1) {
                                    System.out.println("=======" + s);
                                }
                            });*/
                           /* jwPlayerView.addOnErrorListener(new VideoPlayerEvents.OnErrorListenerV2() {
                                @Override
                                public void onError(ErrorEvent errorEvent) {
                                    System.out.println("=======" + errorEvent.getMessage());
                                }
                            });*/
                            RelativeLayout container = (RelativeLayout) findViewById(R.id.container);
                            /*container.addView(jwPlayerView, new RelativeLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));*/
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void uploadVideo() {
//        Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
//        mediaChooser.setType("video");
//        startActivityForResult(mediaChooser, RESULT_LOAD_VIDEO);
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_VIDEO);
    }


    private void checkPersmission(int permissionType) {
        if (ContextCompat.checkSelfPermission(VideoQuizActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(VideoQuizActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(VideoQuizActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(VideoQuizActivity.this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(VideoQuizActivity.this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        permissionType);

            } else {
                ActivityCompat.requestPermissions(VideoQuizActivity.this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        permissionType);
            }
        } else {
            if (permissionType == CAMERA) {
                recordVideo();
            } else {
                uploadVideo();
            }
        }
    }

    private void recordVideo() {

        // create new Intentwith with Standard Intent action that can be
        // sent to have the camera application capture an video and return it.
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        // create a file to save the video
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        // set the image file name
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        intent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
        intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
        intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
        // set the video image quality to high
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);

        // start the Video Capture Intent
        startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
    }

    /**
     * Create a file Uri for saving an image or video
     */
    private static Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type) {

        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraVideo");


        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {

//                output.setText("Failed to create directory MyCameraVideo.");

//                Toast.makeText(this, "Failed to create directory MyCameraVideo.",Toast.LENGTH_LONG).show();

                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }


        // Create a media file name

        // For unique file name appending current timeStamp with file name
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());

        File mediaFile;

        if (type == MEDIA_TYPE_VIDEO) {

            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");

        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // After camera screen this code will excuted

        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            File file = null;
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    try {
//                        Uri imageUri = Uri.parse(mCurrentPhotoPath);
                        file = new File(fileUri.getPath());

//                        ivPreview.setImageBitmap(BitmapFactory.decodeStream(ims));
//                        Bitmap thePic = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri);
//                        Bitmap thePic = BitmapFactory.decodeStream(ims);
                        if (file != null) {
                            Intent intent = new Intent(VideoQuizActivity.this, JW_Player_Activity.class);
                            intent.putExtra("file_path", file.getAbsolutePath());
                            intent.putExtra("screen", "upload");
                            startActivityForResult(intent, 100);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    file = new File(data.getData().getPath());
                    System.out.println("====file====choose===" + file.getAbsolutePath());
                    Intent intent = new Intent(VideoQuizActivity.this, JW_Player_Activity.class);
                    intent.putExtra("file_path", file.getAbsolutePath());
                    intent.putExtra("screen", "upload");
                    startActivityForResult(intent, 100);
                }

//                output.setText("Video File : " +data.getData());

                // Video captured and saved to fileUri specified in the Intent
//                Toast.makeText(VideoQuizActivity.this, "Video saved to:" +data.getData(), Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {

//                output.setText("User cancelled the video capture.");

                // User cancelled the video capture
                Toast.makeText(VideoQuizActivity.this, "User cancelled the video capture.",
                        Toast.LENGTH_LONG).show();

            } else {

//                output.setText("Video capture failed.");

                // Video capture failed, advise user
                Toast.makeText(VideoQuizActivity.this, "Video capture failed.",
                        Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == RESULT_LOAD_VIDEO) {
            File file = null;
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    return;
                } else {
//                File file = new File(data.getData().getEncodedPath());//getPath());
//                file=new File(getPath(Uri.fromFile(file)));
                    Uri selectedImageUri = data.getData();

                    // OI FILE Manager
//                filemanagerstring = selectedImageUri.getPath();

                    // MEDIA GALLERY
                    String selectedImagePath = getPath(selectedImageUri);
                    if (selectedImagePath != null) {
                        String f = getRealPathFromURI(data.getData());
                        System.out.println("====file====choose===" + f);
//                System.out.println("====file====choose===" + file.getAbsolutePath());
                        File copy_path = null;
                        if (f == null) {
                            Toast.makeText(VideoQuizActivity.this, "Please try again!", Toast.LENGTH_LONG).show();
                            return;
                        }
                        Toast.makeText(VideoQuizActivity.this, "Success!", Toast.LENGTH_LONG).show();
                        try {
                            copy_path = APIUtility.getFileFromURL(VideoQuizActivity.this, "chapter_video.mp4");
                            APIUtility.copyDirectory(new File(f), copy_path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(VideoQuizActivity.this, JW_Player_Activity.class);
                        intent.putExtra("file_path", selectedImagePath);
                        intent.putExtra("screen", "upload");
                        startActivity(intent);
//                uploadDialog(data.getData().getPath());
//                output.setText("Video File : " +data.getData());

                        // Video captured and saved to fileUri specified in the Intent
//                    Toast.makeText(VideoQuizActivity.this, "Video selected from:" + data.getData(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(VideoQuizActivity.this, "Please try again!", Toast.LENGTH_LONG).show();
                    }

                }
            }
        } else if (requestCode == 100) {
            if (data != null) {
                if (data.getBooleanExtra("success", false)) {
                    ArrayList<String> ans = new ArrayList<>();
                    ans.add(data.getStringExtra("1"));
                    list.get(currentQuestion).setUser_ans(ans);
                    list.get(currentQuestion).setResponse(data.getStringExtra("response"));
                    list.get(currentQuestion).setOption_type("VIDEO");
//                    Toast.makeText(this, "Your video submitted successfully!", Toast.LENGTH_SHORT).show();
                    showUploadedVideo();
                    showNextQuestionDialog(true);
                } else {
                    showNextQuestionDialog(false);
//                    Toast.makeText(this, "Uploading failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    private void showUploadedVideo() {
        if (!TextUtils.isEmpty(list.get(currentQuestion).getResponse())) {
            //stopPlayer();
            rlJwPlayer.setVisibility(View.VISIBLE);
            ivPlay.setVisibility(View.VISIBLE);
            llVideo.setVisibility(View.GONE);
        }
    }

    private void showNextQuestionDialog(boolean status) {
        final Dialog dialog = new Dialog(VideoQuizActivity.this);
        dialog.setContentView(R.layout.next_question_layout);
        final Button btnNext = (Button) dialog.findViewById(R.id.btn_next);
        Button btnBackTOCourse = (Button) dialog.findViewById(R.id.btn_back_to_course);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_success_message);
        if (!status)
            tvMessage.setText(getString(R.string.your_answer_failed));
        if (currentQuestion == list.size())
            btnNext.setText("Submit");
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dialog != null)
                    dialog.dismiss();
                if (currentQuestion == list.size()) {
                    saveQuizResponse();
                } else {
                    currentQuestion++;
                    showQuestion(currentQuestion);
                    saveQuizResponse();
                }
            }
        });
        btnBackTOCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null)
                    dialog.dismiss();
                saveQuizResponse();
            }
        });

    }

    private String getRealPathFromURI(Uri contentURI) {
        String result = null;
        try {
            Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            cursor.moveToFirst();
//            int column_index = cursor
//                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);

            int columnIndex = cursor.getColumnIndex(projection[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
//            int columnIndex = cursor.getColumnIndex(projection[0]);

            return picturePath;
        } else
            return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    recordVideo();
                }
                break;
            case GALLERY:
                if (grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    uploadVideo();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void saveQuizResponse() {
        JSONArray jsonArray = new JSONArray();
        int correct = 0, skipped = 0, result = 0;
        for (int i = 0; i < list.size(); i++) {
            Data_util dataUtil = list.get(i);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("question_bank_id", dataUtil.getQuestion_bank_id());
                jsonObject.put("assign_question_id", dataUtil.getAssign_question_id());
                jsonObject.put("answer_key", "1");
//                jsonObject.put("answer", convertAlToString(dataUtil.getUser_ans()));
                jsonObject.put("marks", dataUtil.getMarks());
                jsonObject.put("question_comment", dataUtil.getUser_input());
                jsonObject.put("answer_type", "VIDEO");
                jsonObject.put("answer", dataUtil.getResponse());
                jsonObject.put("answer_type", dataUtil.getOption_type());
                if (!TextUtils.isEmpty(QuizUtils.convertAlToString(dataUtil.getUser_ans())))
                    correct++;
                else
                    skipped++;
                jsonObject.put("time_taken", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }


        result = (correct * 100) / list.size();
//        System.out.println("===jsonArray=="+jsonArray.toString());
        mixPanelManager.completematerial(VideoQuizActivity.this, WebServices.mLoginUtility.getEmail(), mMeterialutility.getTitle(), "Quiz|" + "100" + "|" + correct + "|" + result, mMeterialutility.getMaterial_id());

        CoinsAllocatedModel coinsAllocatedModel = mMeterialutility.getCoinsAllocatedModel();
        String coinsCollected = "0";
        String redeem = "false";
        if (coinsAllocatedModel != null) {
            coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialutility, 100, coinsAllocatedModel.getRedeem()));
            redeem = coinsAllocatedModel.getRedeem();
        }
        new WebServices().setProgressOfMaterial(mDatabase, mMeterialutility, (currentQuestion + 1) + "", list.size() + "", coinsCollected);
        new WebServices().addSubmitResponse(mDatabase, mMeterialutility, result + "", correct + "", skipped + "", jsonArray.toString(), null, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
            WebServices.updateTotalCoins(VideoQuizActivity.this, coinsCollected);
        new SubmitData(VideoQuizActivity.this, mMeterialutility, WebServices.mLoginUtility.getUser_id(), null, mDatabase).execute(correct + "", skipped + "", jsonArray.toString(), result + "");
//        Toast.makeText(VideoQuizActivity.this, "Congratulations! You have completed this quiz", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(VideoQuizActivity.this, ThankYou_Activity.class);
        Bundle bundle = new Bundle();
//        bundle.putString("coins", coinsCollected);
        bundle.putSerializable("meterial_Utility", mMeterialutility);
        bundle.putString("msg", "Thank you for submitting your response. Your answers will be evaluated soon.");
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    @Override
    public void onRatingBack() {

    }
}
