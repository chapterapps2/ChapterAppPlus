package com.chaptervitamins.video_quiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.MeterialUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutVideoQuizActivity extends BaseActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.start_ques_ll)
    LinearLayout start_ques_ll;
    @BindView(R.id.about_txt)
    TextView about_txt;
    @BindView(R.id.totalques_txt)
    TextView totalques_txt;
    @BindView(R.id.total_time_txt)
    TextView total_time_txt;
    @BindView(R.id.desc_ques_txt)
    TextView desc_ques_txt;
    private MeterialUtility meterialUtility;
    private String resume = "", res_data = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_about_quiz);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_utility");
            resume = bundle.getString("resume");
            res_data = bundle.getString("res_data");
            title.setText(Html.fromHtml(meterialUtility.getTitle()));
            desc_ques_txt.setText(Html.fromHtml(meterialUtility.getInstruction()));
            about_txt.setText(Html.fromHtml(meterialUtility.getDescription()));
            totalques_txt.setText(meterialUtility.getNo_of_question());
            total_time_txt.setText(meterialUtility.getAlloted_time() + " min");
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        start_ques_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*for (int j = 0; j < HomeActivity.courseUtilities.size(); j++) {
                    if (HomeActivity.courseUtilities.get(j).getCourse_id().equals(meterialUtility.getCourse_id())) {
                        ArrayList<ModulesUtility> modulesUtilityArrayList = HomeActivity.courseUtilities.get(j).getModulesUtilityArrayList();
                        for (int k = 0; k < modulesUtilityArrayList.size(); k++) {
                            if (modulesUtilityArrayList.get(k).getModule_id().equals(meterialUtility.getModule_id())) {
                                ArrayList<MeterialUtility> meterialUtilityArrayList = modulesUtilityArrayList.get(k).getMeterialUtilityArrayList();
                                for (int l = 0; l < meterialUtilityArrayList.size(); l++) {
                                    MeterialUtility utility = meterialUtilityArrayList.get(l);
                                    if (utility.getAssign_material_id().equals(meterialUtility.getAssign_material_id())) {
                                        utility.setRemainingAttempt(String.valueOf(Integer.parseInt(utility.getRemainingAttempt()) - 1));
                                    }
                                }
                            }
                        }
                    }
                }*/
                if (SplashActivity.mPref == null)
                    SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                String count = SplashActivity.mPref.getString(meterialUtility.getMaterial_id(), "0");
                editor.putString(meterialUtility.getMaterial_id(), (Integer.parseInt(count) + 1) + "");
                editor.commit();
                Intent intent = new Intent(AboutVideoQuizActivity.this, VideoQuizActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("meterial_utility", meterialUtility);
                intent.putExtras(bundle);
                startActivity(intent);
                MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(AboutVideoQuizActivity.this);
                mixPanelManager.startquizflash(AboutVideoQuizActivity.this, WebServices.mLoginUtility.getEmail(), getIntent().getStringExtra("name"), "Quiz");
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(AboutVideoQuizActivity.this);
    }
}
