package com.chaptervitamins.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.utility.Favourites_Utils;
import com.chaptervitamins.utility.Get_Survey_DataUtil;
import com.chaptervitamins.utility.Like_Utility;
import com.chaptervitamins.utility.Pending_Response_Utility;
import com.chaptervitamins.utility.ReadResponseUtility;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static com.chaptervitamins.newcode.utils.Constants.DATABASENAME;

/**
 * Created by abcd on 4/20/2016.
 */
public class DataBase {

    private final String LOGINTABLE = "Login";
    public static final String COURSETABLE = "MyCourse";
    public static final String QUIZTABLE = "MyQuiz";
    public static final String FLASHCARDTABLE = "MyFlashCard";
    public static final String SURVEYTABLE = "MySurvey";
    private final String READRESPONSETABLE = "MyReadResponse";
    private final String GETRESPONSETABLE = "MyGetResponse";
    private final String SAVEORANIZATION_SETTING = "Organisation_setting";
    private final String GETPENDING_RESPONSE = "Pending_response";
    private final String PENDING_RESPONSE = "Pending_response_Offline";
    private final String PENDING_RESPONSE_STATUS = "Pending_response_status";
    private final String SURVEY_RESPONSE_TABLE = "Survey_response";
    private final String IMAGES_TABLE = "All_Image";
    private final String ALLGROUP_TABLE = "All_Groups";
    private final String LATESTQUIZ_TABLE = "Latest_Quiz";
    private final String QUIZ_HISTORY_TABLE = "Quiz_History";
    private final String NOTIFICATION_TABLE = "Notifications";
    private final String FAVOUTITES_TABLE = "Favourites";
    private final String All_LD_TABLE = "All_LD_Table";
    private final String ALL_F_TABLE = "All_F_Table";
    private final String ALL_HOSPITALS_TABLE = "All_Hospitals_Table";
    private final String FAQ_DATA = "FAQ_DATA_TABLE";
    private final String SAVE_NOTIFICATION_TABLE = "Notifications_Data";
    private final String PROFILE_TABLE = "Profile_Data";
    private final String TRAINING_TABLE = "Training";
    private DataBaseHelper helper;
    private SQLiteDatabase database;
    private Context mContext;
    private static DataBase dataBaseObj = null;

    public static DataBase getInstance(Context context) {
        if (dataBaseObj == null) {
            dataBaseObj = new DataBase(context);
        }
        return dataBaseObj;
    }

    private DataBase(Context mContext) {
        this.mContext = mContext;
        helper = new DataBaseHelper(mContext);
        database = helper.getWritableDatabase();
    }

    public boolean isData(String userid, String table_name) {
        boolean isUpdated = true;
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(table_name, null, "user_id ='" + userid + "'", null, null, null, null);
        cursor.moveToFirst();
        try {
            if (cursor.getCount() > 0)
                while (!cursor.isAfterLast()) {
                    if (cursor.getString(cursor.getColumnIndex("user_id")).equalsIgnoreCase(userid)) {
                        isUpdated = false;
                        break;
                    }
                    cursor.moveToNext();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isUpdated;

    }

    public boolean isOfflineDataAvailable(String userId, String materialId, String tableName) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(tableName, new String[]{"data"}, "user_id ='" + userId + "' AND meterial_id ='" + materialId + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return false;
        }
        if (!TextUtils.isEmpty(resp))
            return true;
        return false;
    }

    public boolean isData(String userid, String materialid, String table_name) {
        boolean isUpdated = true;
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(table_name, null, "user_id ='" + userid + "' AND meterial_id ='" + materialid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            if (cursor.getCount() > 0)
                while (!cursor.isAfterLast()) {
                    if (cursor.getString(cursor.getColumnIndex("user_id")).equalsIgnoreCase(userid)) {
                        isUpdated = false;
                        break;
                    }
                    cursor.moveToNext();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isUpdated;

    }

    public long addDataToDb(String userId, String meterialId, String data, String tableName) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userId);
        values.put("meterial_id", meterialId);
        values.put("data", data);
        long indertdata = 0;
        try {
            if (isData(userId, meterialId, tableName))
                indertdata = database.insert(tableName, null, values);
            else
                indertdata = database.update(tableName, values, "user_id ='" + userId + "' AND meterial_id ='" + meterialId + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return indertdata;
    }

    public long addProfileData(String userid, String data) {
        long id = 0;
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        if (getProfileData(userid).equals(""))
            id = database.insert(PROFILE_TABLE, null, values);
        else
            id = database.update(PROFILE_TABLE, values, "user_id ='" + userid + "'", null);

        return id;
    }

    public String getProfileData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(PROFILE_TABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public long addTrainingData(String userid, String data) {
        long id = 0;
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        if (getTrainingData(userid).equals(""))
            id = database.insert(TRAINING_TABLE, null, values);
        else
            id = database.update(TRAINING_TABLE, values, "user_id ='" + userid + "'", null);

        return id;
    }

    public String getTrainingData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(TRAINING_TABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public long addLoginData(String userid, String pass, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("password", pass);
        values.put("data", data);
        long id = database.insert(LOGINTABLE, null, values);

        return id;
    }

    public String getLoginData(String userid, String pass) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(LOGINTABLE, new String[]{"data"}, "user_id ='" + userid + "' AND password ='" + pass + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public String getLoginData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(LOGINTABLE, new String[]{"data"}, "user_id ='" + userid + "' ", null, null, null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public long getLoginData() {
        long resp = 0;
        resp = DatabaseUtils.longForQuery(database, "SELECT COUNT(*) FROM " + LOGINTABLE, null);
//        database = helper.getWritableDatabase();
//        Cursor cursor = database.rawQuery("SELECT COUNT (*) FROM " + LOGINTABLE,null );
//        resp = cursor.getCount();
        return resp;
    }

    public void UpdatePassword(String userid, String pass) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("password", pass);
        database.update(LOGINTABLE, values, "user_id ='" + userid + "'", null);

    }

    public void updateLoginData(String userid, String pass, String data) {
        String resp = "";
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", data);
        values.put("password", pass);
        database.update(LOGINTABLE, values, "user_id ='" + userid + "'", null);

    }

    public void updateProfileImageData(String userid, String pass, Bitmap data) {
        String resp = "";
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("image", getBytes(data));
        int id = database.update(LOGINTABLE, values, "user_id ='" + userid + "' AND password ='" + pass + "'", null);
        System.out.println("=============update id====" + id);
    }

    public Bitmap getProfileImage(String userid, String pass) {
        Bitmap resp = null;
        try {
            database = helper.getWritableDatabase();
            Cursor cursor = database.query(LOGINTABLE, new String[]{"image"}, "user_id ='" + userid + "' AND password ='" + pass + "'", null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (cursor.getBlob(cursor.getColumnIndex("image")) != null)
                    resp = getImage(cursor.getBlob(cursor.getColumnIndex("image")));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return resp;
        }
        return resp;
    }


    public long addOrganizationData(String userid, String organization_id, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("organization_id", organization_id);
        values.put("data", data);
        long indertdata = database.insert(SAVEORANIZATION_SETTING, null, values);

        return indertdata;
    }

    public String getOrganizationData(String userid, String organization_id) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(SAVEORANIZATION_SETTING, new String[]{"data"}, "user_id ='" + userid + "' AND organization_id ='" + organization_id + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }

        return resp;
    }

    public void updateOrganizationData(String userid, String organization_id, String data) {
        String resp = "";
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", data);
        database.update(SAVEORANIZATION_SETTING, values, "user_id ='" + userid + "' AND organization_id ='" + organization_id + "'", null);

    }

    public long addNotificationData(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        long indertdata = database.insert(NOTIFICATION_TABLE, null, values);

        return indertdata;
    }

    public String getNotificationData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(NOTIFICATION_TABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }

        return resp;
    }

    public void updateNotificationData(String userid, String data) {
        String resp = "";
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", data);
        database.update(NOTIFICATION_TABLE, values, "user_id ='" + userid + "'", null);

    }

    public long addReadNotificationData(String userid, String notification_id, String notification_user_id) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("notification_id", notification_id);
        values.put("notification_user_id", notification_user_id);
        long indertdata = database.insert(SAVE_NOTIFICATION_TABLE, null, values);

        return indertdata;
    }

    public ArrayList<String> getReadNotificationData(String userid) {
        ArrayList<String> arrayList = new ArrayList<>();
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(SAVE_NOTIFICATION_TABLE, new String[]{"notification_id", "notification_user_id"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String resp = "";
                resp = cursor.getString(cursor.getColumnIndex("notification_id"));
                resp = resp + "," + cursor.getString(cursor.getColumnIndex("notification_user_id"));
                arrayList.add(resp);
                cursor.moveToNext();
            }

        } catch (Exception e) {

        }

        return arrayList;
    }

    public long deleteReadNotificationData(String userid, String notifi_id) {
        database = helper.getWritableDatabase();
        long indertdata = database.delete(SAVE_NOTIFICATION_TABLE, "user_id ='" + userid + "' AND notification_id ='" + notifi_id + "'", null);

        return indertdata;
    }


    public long addCourseData(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);

        values.put("data", data);
        long indertdata = database.insert(COURSETABLE, null, values);

        return indertdata;
    }

    public String getCourseData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(COURSETABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }

        return resp;
    }

    public void updateCourseData(String userid, String data) {
        String resp = "";
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", data);
        database.update(COURSETABLE, values, "user_id ='" + userid + "'", null);

    }

    public long addHospitalsData(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);

        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(ALL_HOSPITALS_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
            indertdata = -1;
        }
        if (indertdata == -1) updateHospitalsData(userid, data);
        return indertdata;
    }

    public String getHospitalsData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(ALL_HOSPITALS_TABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }

        return resp;
    }

    public void updateHospitalsData(String userid, String data) {
        String resp = "";
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", data);
        database.update(ALL_HOSPITALS_TABLE, values, "user_id ='" + userid + "'", null);

    }

    public long addReadResponseData(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);

        values.put("data", data);
        long indertdata = database.insert(READRESPONSETABLE, null, values);

        return indertdata;
    }

    public String getReadResponseData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(READRESPONSETABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public int updateReadResponseData(String userid, String data) {
        String resp = "";
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("data", data);
        int update = 0;
        try {
            update = database.update(READRESPONSETABLE, values, "user_id ='" + userid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return update;
    }

    public long addQuizData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            if (isData(userid, meterialid, QUIZTABLE))
                indertdata = database.insert(QUIZTABLE, null, values);
            else
                indertdata = database.update(QUIZTABLE, values, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return indertdata;
    }

    public String getQuizData(String userid, String meterialid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(QUIZTABLE, new String[]{"data"}, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }
        return resp;
    }

    public long addFavouritesData(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        long indertdata = 0;
        try {
            if (isData(userid, FAVOUTITES_TABLE))
                indertdata = database.insert(FAVOUTITES_TABLE, null, values);
            else
                indertdata = database.update(FAVOUTITES_TABLE, values, "user_id ='" + userid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return indertdata;
    }

    /*public long addFData(String userid, String item_id, String item_type, String favourite_id, String is_favourite) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("item_id", item_id);
        values.put("item_type", item_type);
        values.put("favourite_id", favourite_id);
        values.put("is_favourite", is_favourite);
//        long indertdata = database.insert(All_F_TABLE, null, values);
        long indertdata=0;
        try {
            indertdata = database.insert(FAVOUTITES_TABLE, null, values);
        }catch (Exception e){
            database = helper.getWritableDatabase();
            database.update(FAVOUTITES_TABLE,values,"user_id ='" + userid + "' AND item_id ='" + item_id + "'",null);
            e.printStackTrace();
            return indertdata;
        }

        return indertdata;
    }*/

    public long addLDData(String userid, String item_id, String item_type, String value, String comment) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("item_id", item_id);
        values.put("item_type", item_type);
        values.put("value", value);
        values.put("comment", comment);
        long indertdata = 0;
        try {
            indertdata = database.insert(All_LD_TABLE, null, values);
        } catch (Exception e) {
            database = helper.getWritableDatabase();
            database.update(All_LD_TABLE, values, "user_id ='" + userid + "' AND item_id ='" + item_id + "'", null);

            e.printStackTrace();
            return indertdata;
        }

        return indertdata;
    }

    public long RemoveLD(String userid, String item_id) {
        database = helper.getWritableDatabase();
        long indertdata = 0;
        try {
            indertdata = database.delete(All_LD_TABLE, "user_id ='" + userid + "' AND item_id ='" + item_id + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return indertdata;
    }

    public long RemoveLD(String userid, String item_id, String comment) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("comment", comment);
        values.put("value", "DISLIKE");
        long indertdata = 0;
        try {
            indertdata = database.update(All_LD_TABLE, values, "user_id ='" + userid + "' AND item_id ='" + item_id + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return indertdata;
    }

    public long addFData(String userid, String item_id, String item_type, String favourite_id, String is_favourite) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("item_id", item_id);
        values.put("item_type", item_type);
        values.put("favourite_id", favourite_id);
        values.put("is_favourite", is_favourite);
//        long indertdata = database.insert(All_F_TABLE, null, values);
        long indertdata = 0;
        try {
            indertdata = database.insert(ALL_F_TABLE, null, values);
        } catch (Exception e) {
            database = helper.getWritableDatabase();
            database.update(ALL_F_TABLE, values, "user_id ='" + userid + "' AND item_id ='" + item_id + "'", null);
            e.printStackTrace();
            return indertdata;
        }

        return indertdata;
    }

    public long RemoveFav(String userid, String item_id) {
        database = helper.getWritableDatabase();
        long indertdata = 0;
        try {
            indertdata = database.delete(ALL_F_TABLE, "user_id ='" + userid + "' AND item_id ='" + item_id + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public ArrayList<Like_Utility> getLDData(String userid) {
        ArrayList<Like_Utility> arrayList = new ArrayList<>();
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(All_LD_TABLE, new String[]{"item_id", "item_type", "value", "comment"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Like_Utility like_Utility = new Like_Utility();
                like_Utility.setComment(cursor.getString(cursor.getColumnIndex("comment")));
                like_Utility.setItem_id(cursor.getString(cursor.getColumnIndex("item_id")));
                like_Utility.setItem_type(cursor.getString(cursor.getColumnIndex("item_type")));
                like_Utility.setValue(cursor.getString(cursor.getColumnIndex("value")));
                arrayList.add(like_Utility);
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return arrayList;
        }

        return arrayList;
    }

    public ArrayList<Favourites_Utils> getFavData(String userid) {
        ArrayList<Favourites_Utils> arrayList = new ArrayList<>();
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(ALL_F_TABLE, new String[]{"item_id", "item_type", "favourite_id", "is_favourite","image"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Favourites_Utils like_Utility = new Favourites_Utils();
                like_Utility.setItem_id(cursor.getString(cursor.getColumnIndex("item_id")));
                like_Utility.setItem_type(cursor.getString(cursor.getColumnIndex("item_type")));
                like_Utility.setFavourite_id(cursor.getString(cursor.getColumnIndex("favourite_id")));
                like_Utility.setIs_favourite(cursor.getString(cursor.getColumnIndex("is_favourite")));
                like_Utility.setImage(cursor.getString(cursor.getColumnIndex("image")));
                arrayList.add(like_Utility);
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return arrayList;
        }

        return arrayList;
    }

    public String getFavouritesData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(FAVOUTITES_TABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }
        return resp;
    }


    public long updateQuizData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.update(QUIZTABLE, values, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public long addQuizLastData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(LATESTQUIZ_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public String getQuizLastData(String userid, String meterialid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(LATESTQUIZ_TABLE, new String[]{"data"}, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }
        return resp;
    }

    public long updateQuizLastData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.update(LATESTQUIZ_TABLE, values, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public long addSurveyData(String userid, String survey_id, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", survey_id);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(SURVEYTABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public String getSurveyData(String userid, String survey_id) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(SURVEYTABLE, new String[]{"data"}, "user_id ='" + userid + "' AND meterial_id ='" + survey_id + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }
        return resp;
    }

    public long deleteSurveyData(String userid, String survey_id) {
        database = helper.getWritableDatabase();
        long indertdata = 0;
        try {
            indertdata = database.delete(SURVEYTABLE, "user_id ='" + userid + "' AND meterial_id ='" + survey_id + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public long addSurveyresponseData(String userid, String survey_id, String course_id, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("survey_id", survey_id);
        values.put("course_id", course_id);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(SURVEY_RESPONSE_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public ArrayList<Get_Survey_DataUtil> getSurveyresponseData(String userid) {
        ArrayList<Get_Survey_DataUtil> list = new ArrayList<>();
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(SURVEY_RESPONSE_TABLE, new String[]{"data", "user_id", "survey_id", "course_id"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Get_Survey_DataUtil dataUtil = new Get_Survey_DataUtil();
                dataUtil.setSurvey_id(cursor.getString(cursor.getColumnIndex("curvey_id")));
                dataUtil.setCourse_id(cursor.getString(cursor.getColumnIndex("course_id")));
                dataUtil.setData(cursor.getString(cursor.getColumnIndex("data")));
                dataUtil.setUser_id(cursor.getString(cursor.getColumnIndex("user_id")));
                list.add(dataUtil);
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return list;
        }
        return list;
    }

    public void deletesurvey(String userid, String surveyid) {
        database = helper.getWritableDatabase();
        try {
            database.delete(SURVEY_RESPONSE_TABLE, "user_id ='" + userid + "' AND survey_id ='" + surveyid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public long addFlashCardData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(FLASHCARDTABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public String getFlashCardData(String userid, String meterialid) {
        String resp = "";
        database = helper.getWritableDatabase();
        try {
            Cursor cursor = database.query(FLASHCARDTABLE, new String[]{"data"}, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return resp;
    }

    public long updateFlashcardData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.update(FLASHCARDTABLE, values, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public long addPending_Data(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(GETPENDING_RESPONSE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public String getPending_Data(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(GETPENDING_RESPONSE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public long updatePending_Data(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.update(GETPENDING_RESPONSE, values, "user_id ='" + userid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public boolean isQuizData(String userid, String materialid, String startTime, String table_name) {
        boolean isUpdated = true;
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(table_name, null, "user_id ='" + userid + "' AND meterial_id ='" + materialid + "' AND start_time ='" + startTime + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            if (cursor.getCount() > 0)
                while (!cursor.isAfterLast()) {
                    if (cursor.getString(cursor.getColumnIndex("user_id")).equalsIgnoreCase(userid)) {
                        isUpdated = false;
                        break;
                    }
                    cursor.moveToNext();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isUpdated;

    }

    public long addGetResponseData(ReadResponseUtility utility) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", utility.getUser_id());
        values.put("title", utility.getTitle());
        values.put("test_pattern", utility.getTest_pattern());
        values.put("course_id", utility.getCourse_id());
        values.put("correct_ques", utility.getCorrect_question());
        values.put("finish_time", utility.getFinish_time());
        values.put("incorrect_ques", utility.getIncorrect_question());
        values.put("meterial_id", utility.getMaterial_id());
        values.put("response_id", utility.getResponse_id());
        values.put("material_type", utility.getResponse_type());
        values.put("result", utility.getResult());
        values.put("start_time", utility.getStart_time());
        values.put("quiz_response", utility.getQuiz_response());
        values.put("redeem", utility.getRedeem());
        values.put("coins_allocated", utility.getCoins_allocated());
        values.put("module_id", utility.getModuleId());
        values.put("assign_meterial_id", utility.getAssign_material_id());
        values.put("seen_count", utility.getSeen_count());
        values.put("total_count", utility.getTotal_count());
        values.put("completion_per", utility.getCompletion_per());
        values.put("organization_id",utility.getOrganization_id());
        values.put("branch_id",utility.getBranch_id());
        long indertdata = 0;
        try {
            if (isQuizData(utility.getUser_id(), utility.getMaterial_id(), utility.getStart_time(), GETRESPONSETABLE))
                indertdata = database.insert(GETRESPONSETABLE, null, values);
            else
                indertdata = database.update(GETRESPONSETABLE, values, "user_id ='" + utility.getUser_id() + "' AND meterial_id ='" + utility.getMaterial_id() + "' AND start_time ='" + utility.getStart_time() + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return indertdata;
    }

    public void deleteRowGetResponse(String materialid, String userId, String startTime) {
        try {
            database = helper.getWritableDatabase();
            int rowid = database.delete(GETRESPONSETABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "' AND meterial_id ='" + materialid + "' AND start_time ='" + startTime + "'", null);//execSQL("delete from "+ GETRESPONSETABLE);
            System.out.println("==rowid=====" + rowid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ReadResponseUtility> getResponseData() {
        ArrayList<ReadResponseUtility> list = new ArrayList<>();
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(GETRESPONSETABLE, new String[]{"user_id", "title", "test_pattern", "course_id", "correct_ques", "finish_time", "incorrect_ques", "meterial_id", "response_id", "material_type", "result", "start_time", "quiz_response", "redeem", "coins_allocated", "module_id", "assign_meterial_id", "seen_count", "total_count", "completion_per","organization_id","branch_id"}, null, null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ReadResponseUtility readresponse = new ReadResponseUtility();
//                cursor.getString(cursor.getColumnIndex("data"))
                readresponse.setResponse_id(cursor.getString(cursor.getColumnIndex("response_id")));
//                readresponse.setAssign_course_id(cursor.getString(cursor.getColumnIndex("course_id")));
                readresponse.setUser_id(cursor.getString(cursor.getColumnIndex("user_id")));
                readresponse.setCourse_id(cursor.getString(cursor.getColumnIndex("course_id")));
                readresponse.setMaterial_id(cursor.getString(cursor.getColumnIndex("meterial_id")));
                readresponse.setResponse_type(cursor.getString(cursor.getColumnIndex("material_type")));
                readresponse.setResult(cursor.getString(cursor.getColumnIndex("result")));
                readresponse.setStart_time(cursor.getString(cursor.getColumnIndex("start_time")));
                readresponse.setFinish_time(cursor.getString(cursor.getColumnIndex("finish_time")));
                readresponse.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                readresponse.setTest_pattern(cursor.getString(cursor.getColumnIndex("test_pattern")));
                readresponse.setQuiz_response(cursor.getString(cursor.getColumnIndex("quiz_response")));
                readresponse.setIncorrect_question(cursor.getString(cursor.getColumnIndex("incorrect_ques")));
                readresponse.setCorrect_question(cursor.getString(cursor.getColumnIndex("correct_ques")));
                readresponse.setRedeem(cursor.getString(cursor.getColumnIndex("redeem")));
                readresponse.setCoins_allocated(cursor.getString(cursor.getColumnIndex("coins_allocated")));
                readresponse.setModuleId(cursor.getString(cursor.getColumnIndex("module_id")));
                readresponse.setAssign_material_id(cursor.getString(cursor.getColumnIndex("assign_meterial_id")));
                readresponse.setSeen_count(cursor.getString(cursor.getColumnIndex("seen_count")));
                readresponse.setTotal_count(cursor.getString(cursor.getColumnIndex("total_count")));
                readresponse.setCompletion_per(cursor.getString(cursor.getColumnIndex("completion_per")));
                readresponse.setOrganization_id(cursor.getString(cursor.getColumnIndex("organization_id")));
                readresponse.setBranch_id(cursor.getString(cursor.getColumnIndex("branch_id")));

                list.add(readresponse);
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return list;
        }
        return list;
    }

    public void deleteGetResponse() {
        database = helper.getWritableDatabase();
        if (!database.isOpen()) database = helper.getWritableDatabase();
        if (!database.isOpen()) return;
        try {
            database.execSQL("delete from " + GETRESPONSETABLE);
        } catch (SQLiteCantOpenDatabaseException e) {
            e.printStackTrace();
        }

    }

    public long addPending_ResponseData(String pending_id, String meterialid, String pending_response) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("pending_response_id", pending_id);
        values.put("meterial_id", meterialid);
        values.put("pending_response", pending_response);
        long indertdata = 0;
        try {
            indertdata = database.insert(PENDING_RESPONSE_STATUS, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public String getPending_ResponseData(String meterialid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(PENDING_RESPONSE_STATUS, new String[]{"pending_response_id"}, "meterial_id ='" + meterialid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("pending_response_id"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public long updatePending_ResponseData(String pending_id, String meterialid, String pending_response) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("pending_response_id", pending_id);
        values.put("meterial_id", meterialid);
        values.put("pending_response", pending_response);
        long indertdata = 0;
        try {
            indertdata = database.update(PENDING_RESPONSE_STATUS, values, "meterial_id ='" + meterialid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public long addPending_Response(String user_id, String meterial_id, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", user_id);
        values.put("isUpdate", "0");
        values.put("meterial_id", meterial_id);
        values.put("pending_response", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(PENDING_RESPONSE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public ArrayList<Pending_Response_Utility> getPending_Response(String userid) {
        ArrayList<Pending_Response_Utility> strings = new ArrayList<>();

        database = helper.getWritableDatabase();
        Cursor cursor = database.query(PENDING_RESPONSE, new String[]{"data", "meterial_id"}, "user_id ='" + userid + "' AND isUpdate ='0'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Pending_Response_Utility utility = new Pending_Response_Utility();
                utility.setData(cursor.getString(cursor.getColumnIndex("data")));
                utility.setMeterial_id(cursor.getString(cursor.getColumnIndex("meterial_id")));
                strings.add(utility);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strings;
    }

    public boolean isPending_Response(String meterial_id) {
        boolean isUpdated = false;
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(PENDING_RESPONSE, new String[]{"data", "meterial_id"}, "meterial_id ='" + meterial_id + "' AND isUpdate ='0'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (cursor.getString(cursor.getColumnIndex("meterial_id")).equalsIgnoreCase(meterial_id)) {
                    isUpdated = true;
                    break;
                }
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isUpdated;
    }

    public void addImage(String userid, String url, Bitmap bitmap) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("url", url);
        values.put("image", getBytes(bitmap));
        try {
            long indertdata = database.insert(IMAGES_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getImage(String userid, String url) {
        Bitmap bitmap = null;
        database = helper.getWritableDatabase();
        if (!database.isOpen()) database = helper.getWritableDatabase();
        if (!database.isOpen()) return null;
        Cursor cursor = database.query(IMAGES_TABLE, new String[]{"image"}, "user_id ='" + userid + "' AND url ='" + url + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                bitmap = getImage(cursor.getBlob(cursor.getColumnIndex("image")));

                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public void deleteImage(String userid, String url) {
        database = helper.getWritableDatabase();
        try {
            int id = database.delete(IMAGES_TABLE, "user_id ='" + userid + "' AND url ='" + url + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long updatePending_Response(String meterial_id, String isupdate) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("isUpdate", isupdate);
        long indertdata = 0;
        try {
            indertdata = database.update(PENDING_RESPONSE, values, "meterial_id ='" + meterial_id + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public long addallgroupsData(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(ALLGROUP_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public String getallgroupsData(String userid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(ALLGROUP_TABLE, new String[]{"data"}, "user_id ='" + userid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public long updateallgroupsData(String userid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.update(ALLGROUP_TABLE, values, "user_id ='" + userid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public long addQuizHistoryData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.insert(QUIZ_HISTORY_TABLE, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public String getQuizHistoryData(String userid, String meterialid) {
        String resp = "";
        database = helper.getWritableDatabase();
        Cursor cursor = database.query(QUIZ_HISTORY_TABLE, new String[]{"data"}, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null, null, null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                resp = cursor.getString(cursor.getColumnIndex("data"));
                cursor.moveToNext();
            }

        } catch (Exception e) {
            return resp;
        }
        return resp;
    }

    public long updateQuizHistoryData(String userid, String meterialid, String data) {
        database = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_id", userid);
        values.put("meterial_id", meterialid);
        values.put("data", data);
        long indertdata = 0;
        try {
            indertdata = database.update(QUIZ_HISTORY_TABLE, values, "user_id ='" + userid + "' AND meterial_id ='" + meterialid + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return indertdata;
    }

    public void DeleteAllData() {
        try {
            database = helper.getWritableDatabase();
            database.delete(IMAGES_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(LOGINTABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(COURSETABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(QUIZTABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(READRESPONSETABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(GETRESPONSETABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(FLASHCARDTABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(SAVEORANIZATION_SETTING, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(GETPENDING_RESPONSE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(PENDING_RESPONSE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(SURVEYTABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(SURVEY_RESPONSE_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(ALLGROUP_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(LATESTQUIZ_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(QUIZ_HISTORY_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(NOTIFICATION_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(SAVE_NOTIFICATION_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(FAVOUTITES_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(All_LD_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(ALL_F_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(ALL_HOSPITALS_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(PROFILE_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
            database.delete(TRAINING_TABLE, "user_id ='" + WebServices.mLoginUtility.getUser_id() + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class DataBaseHelper extends SQLiteOpenHelper {
        public DataBaseHelper(Context context) {
            super(context, DATABASENAME, null, 6);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            String quary = "CREATE TABLE " + LOGINTABLE + " ( user_id TEXT, password TEXT, data TEXT, image BLOB, PRIMARY KEY(user_id));";
            db.execSQL(quary);
            String quary2 = "CREATE TABLE " + COURSETABLE + " ( user_id TEXT,  data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary2);
            String quary3 = "CREATE TABLE " + QUIZTABLE + " ( user_id TEXT, meterial_id TEXT,  data TEXT, PRIMARY KEY(user_id,meterial_id));";
            db.execSQL(quary3);
            String quary4 = "CREATE TABLE " + READRESPONSETABLE + " ( user_id TEXT, data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary4);
            String quary5 = "CREATE TABLE " + GETRESPONSETABLE + " ( user_id TEXT, material_type TEXT, meterial_id TEXT, course_id TEXT, start_time TEXT, finish_time TEXT, correct_ques TEXT, incorrect_ques TEXT, result TEXT, response_id TEXT, title TEXT, test_pattern TEXT, quiz_response TEXT, redeem TEXT, coins_allocated TEXT,module_id TEXT,assign_meterial_id TEXT,seen_count TEXT,total_count TEXT,completion_per TEXT, organization_id TEXT, branch_id TEXT);";
            db.execSQL(quary5);
            String quary6 = "CREATE TABLE " + FLASHCARDTABLE + " ( user_id TEXT, meterial_id TEXT,  data TEXT, PRIMARY KEY(user_id,meterial_id));";
            db.execSQL(quary6);
            String quary7 = "CREATE TABLE " + SAVEORANIZATION_SETTING + " ( user_id TEXT,organization_id TEXT,  data TEXT, PRIMARY KEY(user_id,organization_id));";
            db.execSQL(quary7);
            String quary8 = "CREATE TABLE " + GETPENDING_RESPONSE + " ( user_id TEXT, data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary8);
            String quary9 = "CREATE TABLE " + PENDING_RESPONSE_STATUS + " ( pending_response_id TEXT, meterial_id TEXT,  pending_response TEXT, PRIMARY KEY(meterial_id));";
            db.execSQL(quary9);
            String quary10 = "CREATE TABLE " + PENDING_RESPONSE + " ( user_id TEXT,isUpdate TEXT,meterial_id TEXT, data TEXT, PRIMARY KEY(meterial_id));";
            db.execSQL(quary10);
            String quary11 = "CREATE TABLE " + SURVEYTABLE + " ( user_id TEXT, meterial_id TEXT,  data TEXT, PRIMARY KEY(user_id,meterial_id));";
            db.execSQL(quary11);
            String quary12 = "CREATE TABLE " + SURVEY_RESPONSE_TABLE + " ( user_id TEXT, survey_id TEXT, course_id TEXT, data TEXT, PRIMARY KEY(user_id,survey_id));";
            db.execSQL(quary12);
            String quary13 = "CREATE TABLE " + IMAGES_TABLE + " ( user_id TEXT, url TEXT,image BLOB , PRIMARY KEY(user_id,url));";
            db.execSQL(quary13);
            String quary14 = "CREATE TABLE " + ALLGROUP_TABLE + " ( user_id TEXT, data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary14);
            String quary15 = "CREATE TABLE " + LATESTQUIZ_TABLE + " ( user_id TEXT, meterial_id TEXT,  data TEXT, PRIMARY KEY(user_id,meterial_id));";
            db.execSQL(quary15);
            String quary16 = "CREATE TABLE " + QUIZ_HISTORY_TABLE + " ( user_id TEXT, meterial_id TEXT,  data TEXT, PRIMARY KEY(user_id,meterial_id));";
            db.execSQL(quary16);
            String quary17 = "CREATE TABLE " + NOTIFICATION_TABLE + " ( user_id TEXT,  data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary17);

            String quary18 = "CREATE TABLE " + SAVE_NOTIFICATION_TABLE + " ( user_id TEXT, notification_id TEXT,notification_user_id TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary18);
            String quary19 = "CREATE TABLE " + FAVOUTITES_TABLE + " ( user_id TEXT,  data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary19);
            String quary20 = "CREATE TABLE " + All_LD_TABLE + " ( user_id TEXT,item_id TEXT,  item_type TEXT,value TEXT,comment TEXT, PRIMARY KEY(user_id,item_id));";
            db.execSQL(quary20);
            String quary21 = "CREATE TABLE " + ALL_F_TABLE + " ( user_id TEXT,item_id TEXT,  item_type TEXT,is_favourite TEXT,favourite_id TEXT,image TEXT, PRIMARY KEY(user_id,item_id));";
            db.execSQL(quary21);
            String quary22 = "CREATE TABLE " + ALL_HOSPITALS_TABLE + " ( user_id TEXT,  data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary22);
            String quary23 = "CREATE TABLE " + PROFILE_TABLE + " ( user_id TEXT,  data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary23);
            String quary24 = "CREATE TABLE " + TRAINING_TABLE + " ( user_id TEXT,  data TEXT, PRIMARY KEY(user_id));";
            db.execSQL(quary24);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (oldVersion != newVersion) {
                db.execSQL("DROP TABLE IF EXISTS " + LOGINTABLE);
                db.execSQL("DROP TABLE IF EXISTS " + COURSETABLE);
                db.execSQL("DROP TABLE IF EXISTS " + QUIZTABLE);
                db.execSQL("DROP TABLE IF EXISTS " + READRESPONSETABLE);
                db.execSQL("DROP TABLE IF EXISTS " + GETRESPONSETABLE);
                db.execSQL("DROP TABLE IF EXISTS " + FLASHCARDTABLE);
                db.execSQL("DROP TABLE IF EXISTS " + SAVEORANIZATION_SETTING);
                db.execSQL("DROP TABLE IF EXISTS " + GETPENDING_RESPONSE);
                db.execSQL("DROP TABLE IF EXISTS " + PENDING_RESPONSE_STATUS);
                db.execSQL("DROP TABLE IF EXISTS " + PENDING_RESPONSE);
                db.execSQL("DROP TABLE IF EXISTS " + SURVEYTABLE);
                db.execSQL("DROP TABLE IF EXISTS " + SURVEY_RESPONSE_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + IMAGES_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + ALLGROUP_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + LATESTQUIZ_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + QUIZ_HISTORY_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + NOTIFICATION_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + SAVE_NOTIFICATION_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + FAVOUTITES_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + All_LD_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + ALL_F_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + ALL_HOSPITALS_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + PROFILE_TABLE);
                db.execSQL("DROP TABLE IF EXISTS " + TRAINING_TABLE);
                onCreate(db);
            }
        }
    }

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        if (image == null) return null;
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
}
