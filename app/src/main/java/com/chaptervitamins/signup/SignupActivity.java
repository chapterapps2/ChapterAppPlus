package com.chaptervitamins.signup;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.signup.model.SignupModel;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.toolbar_title)
    TextView tooolBarTitle;
    @BindView(R.id.nameEditView)
    EditText nameEditView;
    @BindView(R.id.emailEdittext)
    EditText emailEdittext;
    @BindView(R.id.cmpyEdittext)
    EditText cmpyEdittext;
    @BindView(R.id.mobileEdittext)
    EditText mobileEdittext;
    @BindView(R.id.cityeEdittext)
    EditText cityeEdittext;
    @BindView(R.id.otherEdittext)
    EditText otherEdittext;
    @BindView(R.id.signUpButton)
    Button signUpButton;
    @BindView(R.id.radioGrp)
    RadioGroup radioGrp;
    @BindView(R.id.firstRadioBTn)
    RadioButton firstRadioBTn;
    @BindView(R.id.secondtRadioBTn)
    RadioButton secondtRadioBTn;
    private WebServices webServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        setDataIntoViews();
        setListenerIntoViews();
    }

    private void setListenerIntoViews(){
        signUpButton.setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setDataIntoViews() {
        webServices = new WebServices();
        tooolBarTitle.setText(R.string.create_account_str);
    }

    @Override
    public void onClick(View v) {
        attemptSignUp();
    }

    private void attemptSignUp() {
        String qualification="";
        String nameText = nameEditView.getText().toString();
        String emailText = emailEdittext.getText().toString();
        /*String cmpyText = cmpyEdittext.getText().toString();*/
        String mobileText = mobileEdittext.getText().toString();
        String cityText = cityeEdittext.getText().toString();
        String otherText = otherEdittext.getText().toString();

        //-------------Check Validation-----------
        if (TextUtils.isEmpty(nameText)) {
            Toast.makeText(SignupActivity.this, "Please enter a Name", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(emailText)) {
            Toast.makeText(SignupActivity.this, "Please enter Email", Toast.LENGTH_LONG).show();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailText).matches()) {
            Toast.makeText(SignupActivity.this, "Please enter right Email", Toast.LENGTH_LONG).show();
            return;
        }
        /*if (TextUtils.isEmpty(cmpyText)) {
            Toast.makeText(SignupActivity.this, "Please enter Organization name", Toast.LENGTH_LONG).show();
            return;
        }*/
        if (TextUtils.isEmpty(cityText)) {
            Toast.makeText(SignupActivity.this, "Please enter City", Toast.LENGTH_LONG).show();
            return;
        }
        /*if (TextUtils.isEmpty(countryText)) {
            Toast.makeText(SignupActivity.this, "Please enter Country", Toast.LENGTH_LONG).show();
            return;
        }*/
        RadioButton radioButton=(RadioButton)findViewById(radioGrp.getCheckedRadioButtonId());
        if(radioButton.isChecked()){
            qualification=radioButton.getText().toString();
        }
        if (WebServices.isNetworkAvailable(SignupActivity.this)) {
            final ProgressDialog progressDialog = ProgressDialog.show(SignupActivity.this,
                    "", "Please wait...");

            ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("email", emailText));
            nameValuePair.add(new BasicNameValuePair("firstname", nameText));
            nameValuePair.add(new BasicNameValuePair("company_name", "test"));
            nameValuePair.add(new BasicNameValuePair("country", "test"));
            nameValuePair.add(new BasicNameValuePair("reason", "test"));
            nameValuePair.add(new BasicNameValuePair("mobile_no", mobileText));
            nameValuePair.add(new BasicNameValuePair("city", cityText));
            nameValuePair.add(new BasicNameValuePair("qualification", qualification));
            nameValuePair.add(new BasicNameValuePair("other", otherText));
            nameValuePair.add(new BasicNameValuePair("push_token", ""));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("usersourse", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
            nameValuePair.add(new BasicNameValuePair("branch_id", Constants.BRANCH_ID));
            new GenericApiCall(SignupActivity.this, APIUtility.SIGNUP, nameValuePair,
                    new BaseApiCall.OnApiCallCompleteListener() {
                        @Override
                        public void onSuccess(Object result) {
                            String resp = (String) result;
                            SignupModel signupModel = webServices.isSignupData(resp);
                            if (webServices.isValid(resp)) {
                                if (progressDialog != null && !isFinishing())
                                    progressDialog.dismiss();
                                finish();
                                Toast.makeText(SignupActivity.this, signupModel.getMsg(), Toast.LENGTH_LONG).show();
                            } else {
                                if (progressDialog != null && !isFinishing())
                                    progressDialog.dismiss();
                                finish();
                                Toast.makeText(SignupActivity.this, signupModel.getMsg(), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ErrorModel error) {
                            Toast.makeText(SignupActivity.this, error.getErrorDescription(), Toast.LENGTH_LONG).show();
                            if (progressDialog != null)
                                progressDialog.dismiss();
                        }
                    }).execute();

        } else {
            Toast.makeText(SignupActivity.this, "Can't proceed. No internet connection.", Toast.LENGTH_SHORT).show();
        }

    }
}
