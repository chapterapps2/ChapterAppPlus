package com.chaptervitamins.CustomView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Android on 1/20/2017.
 */

public class DateUtil {

    public static boolean isValidTime(String startTime, String endTime) {
        try {
            String string1 = startTime;
            Date time1 = new SimpleDateFormat("HH:mm:ss").parse(string1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            String string2 = endTime;
            Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
//		    calendar2.add(Calendar.DATE, 1);
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = new Date();
//            System.out.println(dateFormat.format(date));
            String someRandomTime = dateFormat.format(date);
            Date d = new SimpleDateFormat("HH:mm:ss").parse(someRandomTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
//		    calendar3.add(Calendar.DATE, 1);
            Date x = calendar3.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                return (true);
            } else {
                return (false);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (true);
    }

}
