package com.chaptervitamins.CustomView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.chaptervitamins.chaptervitamins.LoginActivity;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.utils.APIUtility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Android on 10/4/2016.
 */

public class AutoLogout {
    static Context mContext;
    static DataBase dataBase;
    static MixPanelManager mixPanelManager;

    public static void autoLogout(Context context) {

        if (SplashActivity.mPref == null)
            SplashActivity.mPref = context.getSharedPreferences("prefdata", Context.MODE_PRIVATE);
        mContext = context;
        dataBase = DataBase.getInstance(context);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
//        if (!Constants.ORGANIZATION_ID.equals("2"))
//            diffDate(SplashActivity.mPref.getString("startTime", dateFormat.format(date)), dateFormat.format(date));
    }

    public static void diffDate(String dateStart, String dateStop) {
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000);
            long diffHours = diff / (60 * 60 * 1000);
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            System.out.println("=====");
            if (diffMinutes >= Long.parseLong(APIUtility.organizationModel.getDataClearTime())) {
                mixPanelManager = APIUtility.getMixPanelManager(mContext);
                mixPanelManager.AutoLogout(mContext, "Session expired from app side");
               /* SplashActivity.isOpenLoginScreen = false;
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putBoolean("islogin", false);
                editor.clear();
                editor.commit();
                dataBase.DeleteAllData();
                APIUtility.isDeletedDirectory(mContext);
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("sms_url", "");
                mContext.startActivity(intent);
                ((Activity) mContext).finish();*/
            } else if (diffMinutes >= Long.parseLong(APIUtility.organizationModel.getAutoLogoutTime())) {
                SplashActivity.isOpenLoginScreen = false;
                dataBase.updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putBoolean("islogin", false);
                editor.clear();
                editor.commit();
                mixPanelManager = APIUtility.getMixPanelManager(mContext);
                mixPanelManager.AutoLogout(mContext, "Session expired from app side");
              /*  Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("sms_url", "");
                mContext.startActivity(intent);
                ((Activity) mContext).finish();*/
            } else {
                java.text.DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putString("startTime", dateFormat.format(date));
                editor.commit();
            }
                /*else if (diffDays>=3){
                    SplashActivity.isOpenLoginScreen=false;
                    dataBase.updateLoginData(SplashActivity.mPref.getString("id",""),SplashActivity.mPref.getString("pass",""),"");
                    SharedPreferences.Editor editor=SplashActivity.mPref.edit();
                    editor.putBoolean("islogin",false);
                    editor.clear();
                    editor.commit();
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("sms_url", "");
                    mContext.startActivity(intent);
                    ((Activity) mContext).finish();
                }*/
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
