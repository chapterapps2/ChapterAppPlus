package com.chaptervitamins.CustomView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;

/**
 * Created by abcd on 4/20/2016.
 */
public class CustomTextView extends android.support.v7.widget.AppCompatTextView {
    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public CustomTextView(Context context) {
        super(context);

        applyCustomFont(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);

    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);

    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView);

        String fontName = attributeArray.getString(R.styleable.CustomTextView_font);

        /*Typeface customFont = selectTypeface(context, fontName);
        setTypeface(customFont);*/

        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, String fontName) {
        if(TextUtils.isEmpty(fontName))
            fontName = context.getString(R.string.font_name_raleway_regular);
        if (fontName.contentEquals(context.getString(R.string.font_name_raleway_bold))) {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeuBold.ttf");
        }
        else if (fontName.contentEquals(context.getString(R.string.font_name_raleway_regular))) {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueLt.ttf");
        }else if (fontName.contentEquals(context.getString(R.string.font_name_raleway_light))) {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/Raleway_Light.ttf");
        }else if (fontName.contentEquals(context.getString(R.string.font_name_raleway_medium))) {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueMedium.ttf");
        }else if (fontName.contentEquals(context.getString(R.string.font_name_raleway_semibold))) {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueMed.ttf");
        }else if (fontName.contentEquals(context.getString(R.string.font_name_raleway_thin))) {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueThin.ttf");
        }else if (fontName.contentEquals(context.getString(R.string.font_name_roboto_medium))) {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/HalveticaNeue.ttf");
        }
        else {
            return Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueLt.ttf");
        }

    }
    private void setBackground(){
        if(WebServices.mLoginUtility!=null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())){
            if(!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2())){
                setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color2()));
            }else
                setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2()));
        }
    }
}
