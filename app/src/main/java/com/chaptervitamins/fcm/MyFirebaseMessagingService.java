package com.chaptervitamins.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.chaptervitamins.R;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by test on 08-05-2017.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    Bitmap bitmap;
    String message;
    String title;
    String largeIcon;
    String url;
    String smallIcon;
    String notificationType;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

      /*  Log.d(TAG, "From: " + remoteMessage.getFrom());
        String imageUrl = "http://i.imgur.com/uIjCaLZ.jpg";
        String notificationType = "image";
//        Bitmap image = getBitmapfromUrl(imageUrl);
       Bitmap image = Utils.getBitmapFromURLForPush(imageUrl);*/

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //The message which i send will have keys named [message, image, AnotherActivity] and corresponding values.
        //You can change as per the requirement.

        //message will contain the Push Message
        message = remoteMessage.getData().get("message");
        title = remoteMessage.getData().get("title");
        //imageUri will contain URL of the image to be displayed with Notification
        largeIcon = remoteMessage.getData().get("largeIcon");
        url = remoteMessage.getData().get("url");
        smallIcon = remoteMessage.getData().get("smallIcon");
        notificationType = remoteMessage.getData().get("type");


        //To get a Bitmap image from the URL received
        if (largeIcon != null)
            bitmap = getBitmapfromUrl(largeIcon);
        sendNotification(message, bitmap, notificationType, title, url);

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
//    private void scheduleJob() {
//        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
//        // [END dispatch_job]
//    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, Bitmap imageBitmap, String notificcationType) {
        NotificationCompat.Builder notificationBuilder;
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("sms_url", url);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (!TextUtils.isEmpty(notificcationType) && notificcationType.equalsIgnoreCase("image")) {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setLargeIcon(imageBitmap)
                    .setSmallIcon(R.drawable.notification_transparent)
                    .setContentTitle(title)
                    .setColor(ContextCompat.getColor(MyFirebaseMessagingService.this, R.color.colorPrimary))
                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(imageBitmap))/*Notification with Image*/
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        } else {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.notification_transparent)
                    .setContentTitle(title)
                    .setColor(ContextCompat.getColor(MyFirebaseMessagingService.this, R.color.colorPrimary))
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     * @param title
     * @param url
     */
    private void sendNotification(String messageBody, Bitmap imageBitmap, String notificcationType, String title, String url) {
        NotificationCompat.Builder notificationBuilder = null;
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("sms_url", url);
        intent.putExtra("type", notificationType);
        intent.putExtra("title", title);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        if (!TextUtils.isEmpty(notificcationType) && notificcationType.equalsIgnoreCase("image") && TextUtils.isEmpty(url))
            pendingIntent = null;

       /* RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.image_notification);
        remoteViews.setTextViewText(R.id.tv_message, messageBody);
        remoteViews.setImageViewBitmap(R.id.iv_big_image, imageBitmap);*/
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
       /* if (!TextUtils.isEmpty(notificcationType) && notificcationType.equalsIgnoreCase("image")) {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setLargeIcon(imageBitmap)
                    .setSmallIcon(R.drawable.notify_logo)
                    .setContentTitle(this.title)
                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(imageBitmap))*//*Notification with Image*//*
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        }*/
        if (!TextUtils.isEmpty(notificcationType) && notificcationType.equalsIgnoreCase("image")) {
           /* if (imageBitmap == null) {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                        R.drawable.app_icon);
                notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notify_logo)
                        .setLargeIcon(icon)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            } else {*/
            if (imageBitmap != null) {
                notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_transparent)
                        .setLargeIcon(imageBitmap)
                        .setColor(getResources().getColor(R.color.yellow))
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(imageBitmap).setSummaryText(messageBody)) //*Notification with Image*//*
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }
        } else {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.notification_transparent)
                    .setColor(getResources().getColor(R.color.yellow))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationBuilder != null)
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /*
    *To get a Bitmap image from the URL received
    * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            input.close();
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
