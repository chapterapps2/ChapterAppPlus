package com.chaptervitamins.quiz;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.multiChoice.LeftRVAdapter;
import com.chaptervitamins.multiChoice.RightRVAdapter;
import com.chaptervitamins.multiChoice.SimpleItemTouchHelperCallback;
import com.chaptervitamins.newcode.activities.BaseQuizActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

public class Quiz_Review_Activity extends BaseQuizActivity {
    private ArrayList<Data_util> list = new ArrayList<>();
    int j = 0, position = 0;
    String resp = "";
    WebServices webServices;
    Handler handler;
    Button next_btn, previous_btn;
    ImageView back, timmer, ques_img, opt_img1, opt_img2, opt_img3, opt_img4;
    LinearLayout img_ll1, img_ll2, img_ll3, img_ll4, ll_answers, llMatchColumn;
    TextView comp_title_txt, count_txt, title_txt, time_txt, tvquestion_description, tvoption1, tvoption2, tvoption3, tvoption4, tvVertical;
    TextView question_no_txt;
    private CardView rlOption1, rlOption2, rlOption3, rlOption4;
    private String TEST_PATTERN = "";
    private String isCorrectAns = "";
    MixPanelManager mixPanelManager;
    public static String QUIZEND_TIME = "";
    private CheckBox cbOption1, cbOption2, cbOption3, cbOption4;
    private RadioButton rbOption1, rbOption2, rbOption3, rbOption4;
    private Integer matchColumnIndex;
    RightRVAdapter rightRVAdapter;
    LeftRVAdapter leftRVAdapter;
    ItemTouchHelper.Callback callback;
    private RecyclerView rvLeftQuest, rvRightAns;
    ArrayList<Data_util> answerMatchTheColumnAl = new ArrayList<>();
    private ArrayList<String> user_ans_al = new ArrayList<>();
    private ArrayList<Data_util> matchTheColumnAl = new ArrayList<>();
    private ItemTouchHelper mItemTouchHelper;
    private ScrollView svQuestion;
    private boolean isChangeAnswer;
    private long quesStartTime = 0, quesEndTime = 0;
    private EditText mEtFeedback;
    private int pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_quiz_review);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        webServices = new WebServices();
        dataBase = DataBase.getInstance(Quiz_Review_Activity.this);
        tvquestion_description = (TextView) findViewById(R.id.question_description);
        tvoption1 = (TextView) findViewById(R.id.option1);
        rvLeftQuest = (RecyclerView) findViewById(R.id.rv_left);
        rvRightAns = (RecyclerView) findViewById(R.id.rv_right);
        llMatchColumn = (LinearLayout) findViewById(R.id.ll_match_column);
        title_txt = (TextView) findViewById(R.id.title_txt);
        tvTimer = (TextView) findViewById(R.id.time_txt);
        tvVertical = (TextView) findViewById(R.id.tv_vertical);
        count_txt = (TextView) findViewById(R.id.count_txt);
        svQuestion = (ScrollView) findViewById(R.id.sv_question);
        comp_title_txt = (TextView) findViewById(R.id.comp_title_txt);
        tvoption2 = (TextView) findViewById(R.id.option2);
        question_no_txt = (TextView) findViewById(R.id.question_no_txt);
        tvoption3 = (TextView) findViewById(R.id.option3);
        tvoption4 = (TextView) findViewById(R.id.option4);
        previous_btn = (Button) findViewById(R.id.previous_btn);
        next_btn = (Button) findViewById(R.id.next_btn);
        mEtFeedback = (EditText) findViewById(R.id.et_feedback);
        back = (ImageView) findViewById(R.id.back);
        timmer = (ImageView) findViewById(R.id.timmer);
        ques_img = (ImageView) findViewById(R.id.question_img);
        opt_img1 = (ImageView) findViewById(R.id.option_img1);
        opt_img2 = (ImageView) findViewById(R.id.option_img2);
        opt_img3 = (ImageView) findViewById(R.id.option_img3);
        opt_img4 = (ImageView) findViewById(R.id.option_img4);
        img_ll1 = (LinearLayout) findViewById(R.id.img_ll1);
        img_ll2 = (LinearLayout) findViewById(R.id.img_ll2);
        img_ll3 = (LinearLayout) findViewById(R.id.img_ll3);
        img_ll4 = (LinearLayout) findViewById(R.id.img_ll4);
        ll_answers = (LinearLayout) findViewById(R.id.ll_answers);
        cbOption1 = (CheckBox) findViewById(R.id.cb_option1);
        cbOption2 = (CheckBox) findViewById(R.id.cb_option2);
        cbOption3 = (CheckBox) findViewById(R.id.cb_option3);
        cbOption4 = (CheckBox) findViewById(R.id.cb_option4);
        rbOption1 = (RadioButton) findViewById(R.id.rb_option1);
        rbOption2 = (RadioButton) findViewById(R.id.rb_option2);
        rbOption3 = (RadioButton) findViewById(R.id.rb_option3);
        rbOption4 = (RadioButton) findViewById(R.id.rb_option4);
        rlOption1 = (CardView) findViewById(R.id.rl_option1);
        rlOption2 = (CardView) findViewById(R.id.rl_option2);
        rlOption3 = (CardView) findViewById(R.id.rl_option3);
        rlOption4 = (CardView) findViewById(R.id.rl_option4);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_Utility");
            TIMERVALUE = bundle.getLong("timer_value", 0);
        }

        mixPanelManager = APIUtility.getMixPanelManager(Quiz_Review_Activity.this);
        quesStartTime = System.currentTimeMillis();


//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                WebServices.questionUtility.setData_utils(list);
//                finish();
//
//
//            }
//        });


        list = new ArrayList<>();
        list = WebServices.questionUtility.getData_utils();
        position = bundle.getInt("pos", 0);
        if (!TextUtils.isEmpty(bundle.getString("ques_id"))) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getQuestion_bank_id().equalsIgnoreCase(bundle.getString("ques_id"))) {
                    count_txt.setText(i + "/" + list.size());
                    show(i);
                    break;
                }
            }

        } else {
            count_txt.setText(bundle.getInt("pos", 0) + "/" + list.size());
            pos = bundle.getInt("pos", 0);
            j = pos;
            show(pos);
        }

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebServices.questionUtility.setData_utils(list);
                boolean shouldFinish = true;
//                list.get(j).setTime_taken(Utils.getTimeDifference(questStartTime,quesEndTime,list.get(j).getTime_taken()));
                quesEndTime = System.currentTimeMillis();
                if (next_btn.getText().toString().equalsIgnoreCase("Done")) {
                    String isCorrect = "Incorrect";
//                    savequizLastResponse();
                    if (meterialUtility.getTest_pattern().equalsIgnoreCase("SEQUENTIAL") && list.get(j).getUser_ans().size() == 0) {
                        showAlart("Required", "Please select an option");
                        shouldFinish = false;
                    } else if (matchColumnIndex != null && matchTheColumnAl != null && matchTheColumnAl.size() > 0) {
                        for (int mtc = 0; mtc < matchTheColumnAl.size(); mtc++) {
                            Data_util dU = answerMatchTheColumnAl.get(mtc);
                            Data_util dU1 = answerMatchTheColumnAl.get(0);
                            answerMatchTheColumnAl.get(0).setTime_taken(Utils.getTimeDifferenceForReview(quesStartTime, quesEndTime, dU1.getTime_taken()));
                            if (dU.getUser_ans().size() > 0 && dU.getUser_ans().get(0).equals(dU.getCorrect_option())) {
                                isCorrect = "Correct";
                                dU.setCorrect(true);
                            } else if (dU.getUser_ans() != null && dU.getUser_ans().size() > 0) {
                                isCorrect = "Incorrect";
                                dU.setCorrect(true);
                            } else
                                isCorrect = "Skipped";

                            if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                                mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), dU.getCorrect_option(), QuizUtils.getAnswerAccToOption(dU), isCorrect, dU.getQuestion_description(), isChangeAnswer);
                            else if (!isCorrect.equalsIgnoreCase("Skipped"))
                                mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), dU.getCorrect_option(), QuizUtils.getAnswerAccToOption(dU), isCorrect, dU.getQuestion_description(), isChangeAnswer);

                        }

                        list.removeAll(matchTheColumnAl);
                        list.addAll(matchColumnIndex, matchTheColumnAl);
                        matchTheColumnAl.clear();
                    } else {
                        if (list.get(j).getQuestion_type().equalsIgnoreCase("COMPREHENSIVE") && !TextUtils.isEmpty(mEtFeedback.getText())) {
                            user_ans_al.add("1");
                            list.get(j).setUser_ans(user_ans_al);
                        }
                        list.get(j).setTime_taken(Utils.getTimeDifferenceForReview(quesStartTime, quesEndTime, list.get(j).getTime_taken()));
                        if (list.get(j).getUser_ans() != null && list.get(j).getUser_ans().size() > 0) {
                            if (list.get(j).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                String correctOptionType = list.get(j).getCorrect_option_type();
                                switch (correctOptionType) {
                                    case "ALL":
                                        if (QuizUtils.checkAnsForAll(list.get(j).getUser_ans(), QuizUtils.convertStringToAl(list.get(j).getCorrect_option()))) {
                                            list.get(j).setCorrect(true);
                                            isCorrect = "Correct";
                                        } else {
                                            list.get(j).setCorrect(false);
                                            isCorrect = "Incorrect";
                                        }
                                        break;
                                    case "ANY":
                                        if (QuizUtils.checkAnsForAny(list.get(j).getUser_ans(), QuizUtils.convertStringToAl(list.get(j).getCorrect_option()))) {
                                            list.get(j).setCorrect(true);
                                            isCorrect = "Correct";
                                        } else {
                                            list.get(j).setCorrect(false);
                                            isCorrect = "Incorrect";
                                        }
                                        break;
                                }
                                if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                                    mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                                else if (!isCorrect.equalsIgnoreCase("Skipped"))
                                    mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);

                            } else if (list.get(j).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                if (list.get(j).getUser_ans().get(0).equals(list.get(j).getCorrect_option())) {
                                    isCorrect = "Correct";
                                    list.get(j).setCorrect(true);
                                    if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                                        mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                                    else if (!isCorrect.equalsIgnoreCase("Skipped"))
                                        mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                                } else {
                                    list.get(j).setCorrect(false);
                                    isCorrect = "InCorrect";
                                    if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                                        mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                                    else if (!isCorrect.equalsIgnoreCase("Skipped"))
                                        mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                                }
                            } else if (list.get(j).getQuestion_type().equalsIgnoreCase("COMPREHENSIVE")) {
                                if (!TextUtils.isEmpty(mEtFeedback.getText())) {
                                    list.get(j).setUser_input(mEtFeedback.getText().toString().trim());
                                    user_ans_al.add(0, "1");
                                    list.get(j).setOption_type("1");
                                    list.get(j).setUser_ans(user_ans_al);
                                }
                                mEtFeedback.getText().clear();
                            }
                        } else {
                            isCorrect = "Skipped";
                            if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                                mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                            else if (!isCorrect.equalsIgnoreCase("Skipped"))
                                mixPanelManager.eachQues_responce(Quiz_Review_Activity.this, WebServices.mLoginUtility.getEmail(), WebServices.questionUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);

                        }
                    }
                    if (shouldFinish) {
                        matchColumnIndex = null;
                        user_ans_al.clear();
                        TimerVal = false;
                        WebServices.questionUtility.setData_utils(list);
                        Intent intent = new Intent();
                        intent.putExtra("timer_value", TIMERVALUE);
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        setTimer(false, R.color.white);
    }

    private void showAlart(String title, String msg) {
        new AlertDialog.Builder(Quiz_Review_Activity.this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isCorrectAns = "false";
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
//        TimerVal = false;
//        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(Quiz_Review_Activity.this);
    }

    private void noQuizData() {
        new AlertDialog.Builder(Quiz_Review_Activity.this)
                .setTitle("No Quiz")
                .setMessage("Not Available Quiz for you.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                    }
                })


                .show();
    }

    private void clear() {
        tvoption1.setTextColor(Color.BLACK);
        tvoption2.setTextColor(Color.BLACK);
        tvoption3.setTextColor(Color.BLACK);
        tvoption4.setTextColor(Color.BLACK);
        rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
        rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
        rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
        rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
        cbOption1.setChecked(false);
        cbOption2.setChecked(false);
        cbOption3.setChecked(false);
        cbOption4.setChecked(false);
        rbOption1.setChecked(false);
        rbOption2.setChecked(false);
        rbOption3.setChecked(false);
        rbOption4.setChecked(false);

        cbOption1.setVisibility(View.GONE);
        cbOption2.setVisibility(View.GONE);
        cbOption3.setVisibility(View.GONE);
        cbOption4.setVisibility(View.GONE);
        rbOption1.setVisibility(View.GONE);
        rbOption2.setVisibility(View.GONE);
        rbOption3.setVisibility(View.GONE);
        rbOption4.setVisibility(View.GONE);

    }

    /**
     * This is used to set visibility and checked status of single and multiple type
     * * @param dataUtil
     *
     * @param cbOption
     * @param rbOption
     * @param cbStatus
     * @param rbStatus
     */
    private void setQuestionType(Data_util dataUtil, CheckBox cbOption, RadioButton rbOption, boolean cbStatus, boolean rbStatus, boolean shouldVisible) {
        cbOption.setChecked(cbStatus);
        rbOption.setChecked(rbStatus);
        if (shouldVisible)
            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE"))
                rbOption.setVisibility(View.VISIBLE);
            else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                cbOption.setVisibility(View.VISIBLE);
    }

    private void show(final int i) {
        final Data_util dataUtil = list.get(i);
        user_ans_al.clear();
        user_ans_al.addAll(dataUtil.getUser_ans());
        if (meterialUtility.getShow_status().equalsIgnoreCase("Yes") && !TextUtils.isEmpty(dataUtil.getVertical()) && !dataUtil.getVertical().equalsIgnoreCase("null")) {
            tvVertical.setVisibility(View.VISIBLE);
            tvVertical.setText("Section : " + dataUtil.getVertical());
        } else
            tvVertical.setVisibility(View.GONE);
        if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0)
            isChangeAnswer = true;
        if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE_ANIMATED")) {
            setMatchTheColumnData(i);
        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("COMPREHENSIVE")) {
            setQuestionData(dataUtil, i);
            mEtFeedback.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(dataUtil.getUser_input()))
                mEtFeedback.setText(dataUtil.getUser_input());
            ll_answers.setVisibility(View.GONE);
            llMatchColumn.setVisibility(View.GONE);
        } else {
            mEtFeedback.setVisibility(View.GONE);
            llMatchColumn.setVisibility(View.GONE);
            ll_answers.setVisibility(View.VISIBLE);
            tvoption1.setText("");
            tvoption2.setText("");
            tvoption3.setText("");
            tvoption4.setText("");
            if (TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                comp_title_txt.setVisibility(View.VISIBLE);
                comp_title_txt.setText(Html.fromHtml(dataUtil.getQuestion_hint()));
            }
            if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                next_btn.setVisibility(View.GONE);
            }
            setQuestionData(dataUtil, i);
            if (!dataUtil.getOption1().equalsIgnoreCase(""))
                if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0")) {
                    tvoption1.setText("A.  " + Html.fromHtml(dataUtil.getOption1()));
//                    tvoption1.setText(Html.fromHtml(dataUtil.getOption1()));
                    opt_img1.setVisibility(View.GONE);
                    tvoption1.setVisibility(View.VISIBLE);
                    setQuestionType(dataUtil, cbOption1, rbOption1, false, false, true);
                } else {
                    opt_img1.setVisibility(View.VISIBLE);
                    tvoption1.setVisibility(View.GONE);
                    /*byte[] decodedString = Base64.decode(dataUtil.getOption1(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    opt_img1.setImageBitmap(decodedByte);*/
                    if (!TextUtils.isEmpty(dataUtil.getOption1_url())) {
                        Picasso.with(this).load(dataUtil.getOption1_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img1);
                    }
                    setQuestionType(dataUtil, cbOption1, rbOption1, false, false, true);
                }
            if (!dataUtil.getOption2().equalsIgnoreCase(""))
                if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image())
                        || dataUtil.getIs_option2_image().equalsIgnoreCase("0")) {
                    tvoption2.setText("B.  " + Html.fromHtml(dataUtil.getOption2()));
//                    tvoption2.setText(Html.fromHtml(dataUtil.getOption2()));
                    opt_img2.setVisibility(View.GONE);
                    tvoption2.setVisibility(View.VISIBLE);
                    setQuestionType(dataUtil, cbOption2, rbOption2, false, false, true);
                } else {
                    opt_img2.setVisibility(View.VISIBLE);
                    tvoption2.setVisibility(View.GONE);
                    /*byte[] decodedString = Base64.decode(dataUtil.getOption2(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    opt_img2.setImageBitmap(decodedByte);*/
                    if (!TextUtils.isEmpty(dataUtil.getOption2_url())) {
                        Picasso.with(this).load(dataUtil.getOption2_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img2);
                    }
                    setQuestionType(dataUtil, cbOption2, rbOption2, false, false, true);
                }
            else {
                opt_img2.setVisibility(View.GONE);
                tvoption2.setVisibility(View.GONE);
            }
            if (!dataUtil.getOption3().equalsIgnoreCase(""))
                if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0")) {
                    tvoption3.setText("C.  " + Html.fromHtml(dataUtil.getOption3()));
//                    tvoption3.setText(Html.fromHtml(dataUtil.getOption3()));
                    opt_img3.setVisibility(View.GONE);
                    tvoption3.setVisibility(View.VISIBLE);
                    setQuestionType(dataUtil, cbOption3, rbOption3, false, false, true);
                } else {
                   /* byte[] decodedString = Base64.decode(dataUtil.getOption3(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    opt_img3.setImageBitmap(decodedByte);*/
                    if (!TextUtils.isEmpty(dataUtil.getOption3_url())) {
                        Picasso.with(this).load(dataUtil.getOption3_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img3);
                    }
                    tvoption3.setVisibility(View.GONE);
                    opt_img3.setVisibility(View.VISIBLE);
                    setQuestionType(dataUtil, cbOption3, rbOption3, false, false, true);
                }
            else {
                opt_img3.setVisibility(View.GONE);
                tvoption3.setVisibility(View.GONE);
                setQuestionType(dataUtil, cbOption3, rbOption3, false, false, false);
            }
            if (!dataUtil.getOption4().equalsIgnoreCase(""))
                if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0")) {
                    tvoption4.setText("D.  " + Html.fromHtml(dataUtil.getOption4()));
//                    tvoption4.setText(Html.fromHtml(dataUtil.getOption4()));
                    opt_img4.setVisibility(View.GONE);
                    tvoption4.setVisibility(View.VISIBLE);
                    setQuestionType(dataUtil, cbOption4, rbOption4, false, false, true);
                } else {
                    opt_img4.setVisibility(View.VISIBLE);
                    tvoption4.setVisibility(View.GONE);
                  /*  byte[] decodedString = Base64.decode(dataUtil.getOption4(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    opt_img4.setImageBitmap(decodedByte);*/
                    if (!TextUtils.isEmpty(dataUtil.getOption4_url())) {
                        Picasso.with(this).load(dataUtil.getOption4_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img4);
                    }
                    setQuestionType(dataUtil, cbOption4, rbOption4, false, false, true);
                }
            else {
                opt_img4.setVisibility(View.GONE);
                tvoption4.setVisibility(View.GONE);
                setQuestionType(dataUtil, cbOption4, rbOption4, false, false, false);
            }

            if (dataUtil.getOpt1clicked() == 1) {
                if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                    tvoption1.setTextColor(Color.WHITE);
                    tvoption2.setTextColor(Color.BLACK);
                    tvoption3.setTextColor(Color.BLACK);
                    tvoption4.setTextColor(Color.BLACK);
                    rbOption1.setVisibility(View.VISIBLE);
                    rbOption1.setChecked(true);
                } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                    tvoption1.setTextColor(Color.WHITE);
                    cbOption1.setVisibility(View.VISIBLE);
                    cbOption1.setChecked(true);
                }

                if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                    if (dataUtil.getCorrect_option().equalsIgnoreCase("1")) {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        else
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        isCorrectAns = "true";
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        else
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        showAlart("Error", "Incorrect option! Try again");
                    }
                } else {
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0")) {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    } else {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    }

                }

            }
            if (dataUtil.getOpt2clicked() == 1) {
                if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                    tvoption1.setTextColor(Color.BLACK);
                    tvoption2.setTextColor(Color.WHITE);
                    tvoption3.setTextColor(Color.BLACK);
                    tvoption4.setTextColor(Color.BLACK);
                    rbOption2.setVisibility(View.VISIBLE);
                    rbOption2.setChecked(true);
                } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                    tvoption2.setTextColor(Color.WHITE);
                    cbOption2.setVisibility(View.VISIBLE);
                    cbOption2.setChecked(true);
                }
                if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                    if (dataUtil.getCorrect_option().equalsIgnoreCase("2")) {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        else
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        isCorrectAns = "true";
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        else
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        showAlart("Error", "Incorrect option! Try again");
                    }
                } else {
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0")) {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    } else {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    }
                }
            }
            if (dataUtil.getOpt3clicked() == 1) {
                if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                    tvoption1.setTextColor(Color.BLACK);
                    tvoption2.setTextColor(Color.BLACK);
                    tvoption3.setTextColor(Color.WHITE);
                    tvoption4.setTextColor(Color.BLACK);
                    rbOption3.setVisibility(View.VISIBLE);
                    rbOption3.setChecked(true);
                } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                    tvoption3.setTextColor(Color.WHITE);
                    cbOption3.setVisibility(View.VISIBLE);
                    cbOption3.setChecked(true);
                }
                if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                    if (dataUtil.getCorrect_option().equalsIgnoreCase("3")) {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        else
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        isCorrectAns = "true";
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        else
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        showAlart("Error", "Incorrect option! Try again");
                    }
                } else {
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0")) {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    } else {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    }
                }
            }
            if (dataUtil.getOpt4clicked() == 1) {
                if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                    tvoption1.setTextColor(Color.BLACK);
                    tvoption2.setTextColor(Color.BLACK);
                    tvoption3.setTextColor(Color.BLACK);
                    tvoption4.setTextColor(Color.WHITE);
                    rbOption4.setVisibility(View.VISIBLE);
                    rbOption4.setChecked(true);
                } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                    tvoption4.setTextColor(Color.WHITE);
                    cbOption4.setVisibility(View.VISIBLE);
                    cbOption4.setChecked(true);
                }
                if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                    if (dataUtil.getCorrect_option().equalsIgnoreCase("4")) {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        else
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                        isCorrectAns = "true";
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        else
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                        showAlart("Error", "Incorrect option! Try again");
                    }
                } else {
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0")) {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    } else {
                        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                            rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                            rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                    }
                }
            }
            rlOption1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 1, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "1");
                        else
                            user_ans_al.add(0, "1");
                        dataUtil.setUser_ans(user_ans_al);
                        rbOption1.setChecked(true);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.WHITE);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(1);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt1clicked() == 0) {
                            user_ans_al.add("1");
                            tvoption1.setTextColor(Color.WHITE);
                            dataUtil.setOpt1clicked(1);
                            cbOption1.setChecked(true);
                            rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("1");
                            tvoption1.setTextColor(Color.BLACK);
                            dataUtil.setOpt1clicked(0);
                            cbOption1.setChecked(false);
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("1")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }
                }
            });
            rlOption2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 2, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "2");
                        else
                            user_ans_al.add(0, "2");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(true);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.WHITE);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(1);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt2clicked() == 0) {
                            user_ans_al.add("2");
                            tvoption2.setTextColor(Color.WHITE);
                            dataUtil.setOpt2clicked(1);
                            cbOption2.setChecked(true);
                            rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("2");
                            tvoption2.setTextColor(Color.BLACK);
                            dataUtil.setOpt2clicked(0);
                            cbOption2.setChecked(false);
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("2")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }
                }
            });
            rlOption3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 3, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "3");
                        else
                            user_ans_al.add(0, "3");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(true);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.WHITE);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(1);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt3clicked() == 0) {
                            user_ans_al.add("3");
                            tvoption3.setTextColor(Color.WHITE);
                            dataUtil.setOpt3clicked(1);
                            cbOption3.setChecked(true);
                            rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("3");
                            tvoption3.setTextColor(Color.BLACK);
                            dataUtil.setOpt3clicked(0);
                            cbOption3.setChecked(false);
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("3")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }

                }
            });
            rlOption4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 4, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "4");
                        else
                            user_ans_al.add(0, "4");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(true);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.WHITE);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(1);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt4clicked() == 0) {
                            user_ans_al.add("4");
                            tvoption4.setTextColor(Color.WHITE);
                            dataUtil.setOpt4clicked(1);
                            cbOption4.setChecked(true);
                            rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("4");
                            tvoption4.setTextColor(Color.BLACK);
                            dataUtil.setOpt4clicked(0);
                            cbOption4.setChecked(false);
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("4")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }

                }
            });
            opt_img1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 1, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "1");
                        else
                            user_ans_al.add(0, "1");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(true);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.WHITE);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(1);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt1clicked() == 0) {
                            user_ans_al.add("1");
                            tvoption1.setTextColor(Color.WHITE);
                            dataUtil.setOpt1clicked(1);
                            cbOption1.setChecked(true);
                            rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("1");
                            tvoption1.setTextColor(Color.BLACK);
                            dataUtil.setOpt1clicked(0);
                            cbOption1.setChecked(false);
                            rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("1")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }
                }
            });
            opt_img2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 2, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "2");
                        else
                            user_ans_al.add(0, "2");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(true);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.WHITE);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(1);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt2clicked() == 0) {
                            user_ans_al.add("2");
                            tvoption2.setTextColor(Color.WHITE);
                            dataUtil.setOpt2clicked(1);
                            cbOption2.setChecked(true);
                            rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("2");
                            tvoption2.setTextColor(Color.BLACK);
                            dataUtil.setOpt2clicked(0);
                            cbOption2.setChecked(false);
                            rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("2")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }
                }
            });
            opt_img3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 3, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "3");
                        else
                            user_ans_al.add(0, "3");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(true);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.WHITE);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(1);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt3clicked() == 0) {
                            user_ans_al.add("3");
                            tvoption3.setTextColor(Color.WHITE);
                            dataUtil.setOpt3clicked(1);
                            cbOption3.setChecked(true);
                            rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("3");
                            tvoption3.setTextColor(Color.BLACK);
                            dataUtil.setOpt3clicked(0);
                            cbOption3.setChecked(false);
                            rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }

                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("3")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }
                }
            });
            opt_img4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog(dataUtil, 4, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        if (user_ans_al.size() > 0)
                            user_ans_al.set(0, "4");
                        else
                            user_ans_al.add(0, "4");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(true);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.WHITE);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(1);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        rlOption1.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption2.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption3.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt4clicked() == 0) {
                            user_ans_al.add("4");
                            tvoption4.setTextColor(Color.WHITE);
                            dataUtil.setOpt4clicked(1);
                            cbOption4.setChecked(true);
                            rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            user_ans_al.remove("4");
                            tvoption4.setTextColor(Color.BLACK);
                            dataUtil.setOpt4clicked(0);
                            cbOption4.setChecked(false);
                            rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.white));
                        }
                        dataUtil.setUser_ans(user_ans_al);

                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("4")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            else
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            else
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(Quiz_Review_Activity.this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }

                }
            });

        }
    }

    /**
     * It is used to show the match the column data of a particular group_id
     *
     * @param i
     */
    private void setMatchTheColumnData(int i) {
        boolean shouldShuffle = true;
        matchColumnIndex = null;
        ll_answers.setVisibility(View.GONE);
        llMatchColumn.setVisibility(View.VISIBLE);
        matchTheColumnAl.clear();
        mEtFeedback.setVisibility(View.GONE);
//        matchTheColumnAl = new ArrayList<>();
        Data_util dataUtil = list.get(i);
        String groupId = dataUtil.getGroup_id();
        if (!TextUtils.isEmpty(groupId)) {
            int listSize = list.size();
//            if (listSize > i) {
            for (int k = 0; k < listSize; k++) {
                Data_util currentDataUtil = list.get(k);
                if (dataUtil.getGroup_id().equalsIgnoreCase(currentDataUtil.getGroup_id())) {
                    if (matchColumnIndex == null)
                        matchColumnIndex = k;
                    matchTheColumnAl.add(currentDataUtil);
                    j = k;
                } /*else
                        break;*/
            }
//            }

            count_txt.setText("");
            if (j >= list.size() - 1) {
                next_btn.setText("Done");
            }

            // set left recyclerview data
            if (leftRVAdapter == null) {
                leftRVAdapter = new LeftRVAdapter(matchTheColumnAl);
                LinearLayoutManager layoutManager1 = new LinearLayoutManager(Quiz_Review_Activity.this);
                rvLeftQuest.setLayoutManager(layoutManager1);
                rvLeftQuest.setAdapter(leftRVAdapter);
            } else
                leftRVAdapter.notifyDataSetChanged();


//            ArrayList<Data_util> answerMatchTheColumnAl = new ArrayList<>();
            answerMatchTheColumnAl.clear();
            answerMatchTheColumnAl.addAll(matchTheColumnAl);
            for (int p = 0; p < matchTheColumnAl.size(); p++) {
                Data_util dataUtil1 = matchTheColumnAl.get(p);
                if (dataUtil1.getUser_ans() != null && dataUtil1.getUser_ans().size() > 0) {
                    shouldShuffle = false;
                    break;
                }
            }
            if (shouldShuffle)
                Collections.shuffle(answerMatchTheColumnAl);
            if (rightRVAdapter == null) {
                rightRVAdapter = new RightRVAdapter(matchTheColumnAl, answerMatchTheColumnAl, shouldShuffle);
                LinearLayoutManager layoutManager2 = new LinearLayoutManager(Quiz_Review_Activity.this);
                rvRightAns.setLayoutManager(layoutManager2);
                rvRightAns.setAdapter(rightRVAdapter);
                callback = new SimpleItemTouchHelperCallback(rightRVAdapter);
                mItemTouchHelper = new ItemTouchHelper(callback);

                mItemTouchHelper.attachToRecyclerView(rvRightAns);
            } else
                rightRVAdapter.notifyDataSetChanged();
        }
    }

    private void setQuestionData(Data_util dataUtil, int i) {
        if (!TextUtils.isEmpty(dataUtil.getQuestion_hint())) {
            comp_title_txt.setVisibility(View.VISIBLE);
            comp_title_txt.setText(Html.fromHtml(dataUtil.getQuestion_hint()));
        } else {
            comp_title_txt.setVisibility(View.GONE);
        }

        count_txt.setVisibility(View.GONE);
//        count_txt.setText((i + 1) + "/" + list.size());
        question_no_txt.setText("Q" + (position + 1) + ". ");
        if (dataUtil.getIs_question_image().equalsIgnoreCase("0")) {
            tvquestion_description.setVisibility(View.VISIBLE);
            tvquestion_description.setText(Html.fromHtml(dataUtil.getQuestion_description()));
            ques_img.setVisibility(View.GONE);
        } else {
            tvquestion_description.setVisibility(View.GONE);
            ques_img.setVisibility(View.VISIBLE);
           /* byte[] decodedString = Base64.decode(dataUtil.getQuestion_description(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            ques_img.setImageBitmap(decodedByte);*/
            if (!TextUtils.isEmpty(dataUtil.getQuestion_description_url())) {
                Picasso.with(this).load(dataUtil.getQuestion_description_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(ques_img);
            }
        }
    }

    protected void showInputDialog(final Data_util dataUtil, int optionPos, final int pos) {
        String optionType = "";
        switch (optionPos) {
            case 1:
                optionType = dataUtil.getOption1_type();
                break;
            case 2:
                optionType = dataUtil.getOption2_type();
                break;
            case 3:
                optionType = dataUtil.getOption3_type();
                break;
            case 4:
                optionType = dataUtil.getOption4_type();
                break;
        }
        if (TextUtils.isEmpty(optionType) || !optionType.equalsIgnoreCase("text"))
            return;
        // Create custom dialog object
        final Dialog dialog = new Dialog(Quiz_Review_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.input_dialog);
        // Set dialog title
        dialog.setTitle("Write your answer");

        final EditText editText = (EditText) dialog.findViewById(R.id.edit_text);
        // setup a dialog window
        if (!TextUtils.isEmpty(dataUtil.getUser_input()))
            editText.setText(dataUtil.getUser_input());
        Button button = (Button) dialog.findViewById(R.id.ok_button);
//        dialog.setCancelable(false);
        final String finalOptionType = optionType;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataUtil.setUser_input(editText.getText().toString());
                dataUtil.setOption_type(finalOptionType + "");
                Toast.makeText(Quiz_Review_Activity.this, "Your response is added!", Toast.LENGTH_SHORT).show();
                editText.setText("");
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}


