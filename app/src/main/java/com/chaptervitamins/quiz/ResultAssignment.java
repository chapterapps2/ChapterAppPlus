package com.chaptervitamins.quiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ResultAssignment extends BaseActivity implements View.OnClickListener {
    private ArrayList<Data_util> list = new ArrayList<>();
    int j = 0;
    Handler handler;
    Button imgnext, imgback;
    ImageView back;
    TextView title_txt, tvCorrectOptions, tvUserAnswer, tvCorrectOptionType, tvquestion_description, tvExplanation, tvoption1, tvoption2, tvoption3, tvoption4, answer_type;//, tvoption5, tvoption6, tvoption7, tvoption8, tvoption9, tvoption10;
    ImageView img1, img2, ivArrow, img3, img4, ques_img, opt_img1, opt_img2, opt_img3, opt_img4;
    RelativeLayout rl1, rl2, rl3, rl4;
    private String MSG = "";
    TextView question_no_txt, l2, l3, l4,tv_vertical;
    private MeterialUtility meterialUtility;
    private String complete = "", showAnswer = "", TEST_PATTERN = "", isDone = "", tag = "";
    private boolean isArrowOpen = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result_assignment);
        tvquestion_description = (TextView) findViewById(R.id.question_description);
        l3 = (TextView) findViewById(R.id.l3);
        tv_vertical = (TextView) findViewById(R.id.tv_vertical);
        l2 = (TextView) findViewById(R.id.l2);
        l4 = (TextView) findViewById(R.id.l4);
        question_no_txt = (TextView) findViewById(R.id.question_no_txt);
        tvoption1 = (TextView) findViewById(R.id.option1);
        tvCorrectOptions = (TextView) findViewById(R.id.tv_correct_options);
        tvUserAnswer = (TextView) findViewById(R.id.tv_user_answer);
        title_txt = (TextView) findViewById(R.id.totalanswer_txt);
        tvoption2 = (TextView) findViewById(R.id.option2);
        tvCorrectOptionType = (TextView) findViewById(R.id.tv_correct_option_type);
        tvExplanation = (TextView) findViewById(R.id.tv_explanation);
        tvoption3 = (TextView) findViewById(R.id.option3);
        tvoption4 = (TextView) findViewById(R.id.option4);
        answer_type = (TextView) findViewById(R.id.answer_type);
        imgback = (Button) findViewById(R.id.previous_btn);
        imgnext = (Button) findViewById(R.id.next_btn);
        back = (ImageView) findViewById(R.id.back);
        img1 = (ImageView) findViewById(R.id.info_img1);
        img2 = (ImageView) findViewById(R.id.info_img2);
        ivArrow = (ImageView) findViewById(R.id.iv_arrow);
        img3 = (ImageView) findViewById(R.id.info_img3);
        img4 = (ImageView) findViewById(R.id.info_img4);
        rl1 = (RelativeLayout) findViewById(R.id.rl1);
        rl2 = (RelativeLayout) findViewById(R.id.rl2);
        rl3 = (RelativeLayout) findViewById(R.id.rl3);
        rl4 = (RelativeLayout) findViewById(R.id.rl4);
        ques_img = (ImageView) findViewById(R.id.question_img);
        opt_img1 = (ImageView) findViewById(R.id.option_img1);
        opt_img2 = (ImageView) findViewById(R.id.option_img2);
        opt_img3 = (ImageView) findViewById(R.id.option_img3);
        opt_img4 = (ImageView) findViewById(R.id.option_img4);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_Utility");
            complete = bundle.getString("time");
            showAnswer = bundle.getString("show_answer");
            TEST_PATTERN = bundle.getString("test_pattern");
            isDone = bundle.getString("isdone");
            tag = bundle.getString("tag");
        }

        ivArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setExplanation();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                if (getIntent().getStringExtra("tag").equalsIgnoreCase("")) {
                    WebServices.questionUtility.setData_utils(list);
                    if(meterialUtility.getShow_vertical().equalsIgnoreCase("Yes")&&WebServices.questionUtility.getVerticalList()!=null&& WebServices.questionUtility.getVerticalList().size()>0&&!TextUtils.isEmpty(WebServices.questionUtility.getVerticalList().get(0).getName())){
                        Intent intent = new Intent(ResultAssignment.this, VerticalQuizResultActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("meterial_Utility", meterialUtility);
                        bundle.putString("time", complete);
                        bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                        bundle.putString("test_pattern", TEST_PATTERN);
                        bundle.putString("isdone", isDone);
                        intent.putExtras(bundle);
                   /* intent.putExtra("name", getIntent().getStringExtra("name"));
                    intent.putExtra("time", getIntent().getStringExtra("time"));
                    intent.putExtra("show_answer", getIntent().getStringExtra("show_answer"));
                    intent.putExtra("meterial_id", getIntent().getStringExtra("meterial_id"));
                    intent.putExtra("test_pattern", getIntent().getStringExtra("test_pattern"));
                    intent.putExtra("isdone", getIntent().getStringExtra("isdone"));
                    intent.putExtra("show_leaderboard", getIntent().getStringExtra("show_leaderboard"));
                    intent.putExtra("pass_per", getIntent().getStringExtra("pass_per"));*/
                        startActivity(intent);
                    }else {
                        Intent intent = new Intent(ResultAssignment.this, ResultActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("meterial_Utility", meterialUtility);
                        bundle.putString("time", complete);
                        bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                        bundle.putString("test_pattern", TEST_PATTERN);
                        bundle.putString("isdone", isDone);
                        intent.putExtras(bundle);
                   /* intent.putExtra("name", getIntent().getStringExtra("name"));
                    intent.putExtra("time", getIntent().getStringExtra("time"));
                    intent.putExtra("show_answer", getIntent().getStringExtra("show_answer"));
                    intent.putExtra("meterial_id", getIntent().getStringExtra("meterial_id"));
                    intent.putExtra("test_pattern", getIntent().getStringExtra("test_pattern"));
                    intent.putExtra("isdone", getIntent().getStringExtra("isdone"));
                    intent.putExtra("show_leaderboard", getIntent().getStringExtra("show_leaderboard"));
                    intent.putExtra("pass_per", getIntent().getStringExtra("pass_per"));*/
                        startActivity(intent);
                    }
                }
                finish();
            }
        });
        list = WebServices.questionUtility.getData_utils();
        show(0);
        /*if (j > list.size() - 2) {
            imgnext.setText("Done");
        }*/
        imgnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isArrowOpen = true;
                if (imgnext.getText().toString().equalsIgnoreCase("done")) {
                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    if (tag.equalsIgnoreCase("")) {
                        WebServices.questionUtility.setData_utils(list);
                        if(meterialUtility.getShow_vertical().equalsIgnoreCase("Yes")&&WebServices.questionUtility.getVerticalList()!=null&& WebServices.questionUtility.getVerticalList().size()>0&&!TextUtils.isEmpty(WebServices.questionUtility.getVerticalList().get(0).getName())){
                            Intent intent = new Intent(ResultAssignment.this, VerticalQuizResultActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("meterial_Utility", meterialUtility);
                            bundle.putString("time", complete);
                            bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                            bundle.putString("test_pattern", TEST_PATTERN);
                            bundle.putString("isdone", isDone);
                            intent.putExtras(bundle);
                       /* intent.putExtra("name", getIntent().getStringExtra("name"));
                        intent.putExtra("time", getIntent().getStringExtra("time"));
                        intent.putExtra("show_answer", getIntent().getStringExtra("show_answer"));
                        intent.putExtra("meterial_id", getIntent().getStringExtra("meterial_id"));
                        intent.putExtra("test_pattern", getIntent().getStringExtra("test_pattern"));
                        intent.putExtra("isdone", getIntent().getStringExtra("isdone"));
                        intent.putExtra("show_leaderboard", getIntent().getStringExtra("show_leaderboard"));
                        intent.putExtra("pass_per", getIntent().getStringExtra("pass_per"));*/
                            startActivity(intent);
                        }else{
                            Intent intent = new Intent(ResultAssignment.this, ResultActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("meterial_Utility", meterialUtility);
                            bundle.putString("time", complete);
                            bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                            bundle.putString("test_pattern", TEST_PATTERN);
                            bundle.putString("isdone", isDone);
                            intent.putExtras(bundle);
                       /* intent.putExtra("name", getIntent().getStringExtra("name"));
                        intent.putExtra("time", getIntent().getStringExtra("time"));
                        intent.putExtra("show_answer", getIntent().getStringExtra("show_answer"));
                        intent.putExtra("meterial_id", getIntent().getStringExtra("meterial_id"));
                        intent.putExtra("test_pattern", getIntent().getStringExtra("test_pattern"));
                        intent.putExtra("isdone", getIntent().getStringExtra("isdone"));
                        intent.putExtra("show_leaderboard", getIntent().getStringExtra("show_leaderboard"));
                        intent.putExtra("pass_per", getIntent().getStringExtra("pass_per"));*/
                            startActivity(intent);
                        }

                    }
                    finish();
                    return;
                }

                if (list.size() > 0) {
                    imgback.setVisibility(View.VISIBLE);
                }

                if (list.size() > j) {

                    j++;
                    clear();
                    show(j);
                }


            }
        });
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isArrowOpen = true;
                j--;
                if (list.size() > 0) {
                    imgnext.setVisibility(View.VISIBLE);

                    clear();
                    show(j);
                }
                if (j == 0)
                    imgback.setVisibility(View.GONE);

            }
        });
    }

    private void setExplanation() {

        if (isArrowOpen) {
            isArrowOpen = false;
            tvExplanation.setMaxLines(2);
            tvExplanation.setEllipsize(TextUtils.TruncateAt.END);
            ivArrow.setImageResource(R.drawable.dropdown);
//            ivArrow.setRotation(180);
        } else {
            isArrowOpen = true;
            ivArrow.setImageResource(R.drawable.dropup);
            tvExplanation.setMaxLines(10);
//            ivArrow.setRotation(180);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(ResultAssignment.this);
    }

    private void clear() {
        tvoption1.setTextColor(Color.BLACK);
        tvoption2.setTextColor(Color.BLACK);
        tvoption3.setTextColor(Color.BLACK);
        tvoption4.setTextColor(Color.BLACK);
        tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
        tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
        tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
        tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
        rl1.setBackgroundColor(Color.WHITE);
        rl2.setBackgroundColor(Color.WHITE);
        rl3.setBackgroundColor(Color.WHITE);
        rl4.setBackgroundColor(Color.WHITE);

    }

    private void show(final int i) {
        setExplanation();
        tvoption1.setText("");
        tvoption2.setText("");
        tvoption3.setText("");
        tvoption4.setText("");
        if (j >= list.size() - 1) {
            imgnext.setText("Done");
        } else
            imgnext.setText("Next >");
        title_txt.setText((i + 1) + "/" + list.size() + " ANSWER");
        question_no_txt.setText("Q" + (i + 1) + ". ");

        if (i < list.size()) {
            if(!TextUtils.isEmpty(list.get(i).getVertical()) && !TextUtils.isEmpty(meterialUtility.getShow_vertical()) && meterialUtility.getShow_vertical().equalsIgnoreCase("yes")){
                tv_vertical.setText(meterialUtility.getVertical_level()+" : "+list.get(i).getVertical()+" ");
                tv_vertical.setTextColor(Color.WHITE);
                tv_vertical.setVisibility(View.VISIBLE);
            }else{
                tv_vertical.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(list.get(i).getExplanation()) && !list.get(i).getExplanation().equals("null")) {
                tvExplanation.setVisibility(View.VISIBLE);
                ivArrow.setVisibility(View.VISIBLE);
                tvExplanation.setText(Html.fromHtml("<b> Explanation : </b>"+list.get(i).getExplanation()));
            } else {
                tvExplanation.setVisibility(View.GONE);
                ivArrow.setVisibility(View.GONE);
            }
            if (list.get(i).getIs_question_image().equalsIgnoreCase("0")) {
                tvquestion_description.setVisibility(View.VISIBLE);
                tvquestion_description.setText(Html.fromHtml(list.get(i).getQuestion_description()));
                ques_img.setVisibility(View.GONE);
            } else {
                tvquestion_description.setVisibility(View.GONE);
                ques_img.setVisibility(View.VISIBLE);
               /* byte[] decodedString = Base64.decode(list.get(i).getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                ques_img.setImageBitmap(decodedByte);*/
                if (!TextUtils.isEmpty(list.get(i).getQuestion_description())) {
                    Picasso.with(this).load(list.get(i).getQuestion_description()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(ques_img);
                }

            }


            if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                if (list.get(i).getOption1_type().equalsIgnoreCase("text") && !TextUtils.isEmpty(list.get(i).getUser_input())) {
                    tvoption1.setText("A.  " + Html.fromHtml(list.get(i).getUser_input()));
                } else {
                    tvoption1.setText("A.  " + Html.fromHtml(list.get(i).getOption1()));
                }
                opt_img1.setVisibility(View.GONE);
                tvoption1.setVisibility(View.VISIBLE);
            } else {
                tvoption1.setVisibility(View.GONE);
                opt_img1.setVisibility(View.VISIBLE);
               /* byte[] decodedString = Base64.decode(list.get(i).getOption1(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                opt_img1.setImageBitmap(decodedByte);*/
                if (!TextUtils.isEmpty(list.get(i).getOption1_url())) {
                    Picasso.with(this).load(list.get(i).getOption1_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img1);
                }
            }
            if (!list.get(i).getOption2().equalsIgnoreCase("")) {

                if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                    if (list.get(i).getOption1_type().equalsIgnoreCase("text") && !TextUtils.isEmpty(list.get(i).getUser_input())) {
                        tvoption2.setText("B.  " + Html.fromHtml(list.get(i).getUser_input()));
                    } else {
                        tvoption2.setText("B.  " + Html.fromHtml(list.get(i).getOption2()));
                    }
//                    tvoption2.setText("B.  " + Html.fromHtml(list.get(i).getOption2()));
                    opt_img2.setVisibility(View.GONE);
                    tvoption2.setVisibility(View.VISIBLE);
                } else {
                    tvoption2.setVisibility(View.GONE);
                    opt_img2.setVisibility(View.VISIBLE);
                   /* byte[] decodedString = Base64.decode(list.get(i).getOption2(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    opt_img2.setImageBitmap(decodedByte);*/
                    if (!TextUtils.isEmpty(list.get(i).getOption2_url())) {
                        Picasso.with(this).load(list.get(i).getOption2_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img2);
                    }

                }
            } else {
                tvoption2.setVisibility(View.GONE);
                opt_img2.setVisibility(View.GONE);
                l2.setVisibility(View.GONE);
            }
            if (!list.get(i).getOption3().equalsIgnoreCase(""))
                if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                    if (list.get(i).getOption1_type().equalsIgnoreCase("text") && !TextUtils.isEmpty(list.get(i).getUser_input())) {
                        tvoption3.setText("C.  " + Html.fromHtml(list.get(i).getUser_input()));
                    } else {
                        tvoption3.setText("C.  " + Html.fromHtml(list.get(i).getOption3()));
                    }
//                    tvoption3.setText("C.  " + Html.fromHtml(list.get(i).getOption3()));
                    opt_img3.setVisibility(View.GONE);
                    l3.setVisibility(View.VISIBLE);
                    tvoption3.setVisibility(View.VISIBLE);
                } else {
                    tvoption3.setVisibility(View.GONE);
                 /*   byte[] decodedString = Base64.decode(list.get(i).getOption3(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    opt_img3.setImageBitmap(decodedByte);*/
                    if (!TextUtils.isEmpty(list.get(i).getOption3_url())) {
                        Picasso.with(this).load(list.get(i).getOption3_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img3);
                    }
                    opt_img3.setVisibility(View.VISIBLE);
                }
            else {
                tvoption3.setVisibility(View.GONE);
                opt_img3.setVisibility(View.GONE);
                l3.setVisibility(View.GONE);
            }
            if (!list.get(i).getOption4().equalsIgnoreCase(""))
                if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                    if (list.get(i).getOption1_type().equalsIgnoreCase("text") && !TextUtils.isEmpty(list.get(i).getUser_input())) {
                        tvoption4.setText("D.  " + Html.fromHtml(list.get(i).getUser_input()));
                    } else {
                        tvoption4.setText("D.  " + Html.fromHtml(list.get(i).getOption4()));
                    }
//                    tvoption4.setText("D.  " + Html.fromHtml(list.get(i).getOption4()));
                    opt_img4.setVisibility(View.GONE);
                    tvoption4.setVisibility(View.VISIBLE);
                } else {
                    tvoption4.setVisibility(View.GONE);
                    opt_img4.setVisibility(View.VISIBLE);
                   /* byte[] decodedString = Base64.decode(list.get(i).getOption4(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    opt_img4.setImageBitmap(decodedByte);*/
                    if (!TextUtils.isEmpty(list.get(i).getOption4_url())) {
                        Picasso.with(this).load(list.get(i).getOption4_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img4);
                    }
                }
            else {
                opt_img4.setVisibility(View.GONE);
                tvoption4.setVisibility(View.GONE);
                l4.setVisibility(View.GONE);
            }
//        String desc = "<font color='#3d9cdc'>Description: </font>";
            MSG = list.get(i).getExplanation();//(Html.fromHtml());
            String correctAns = list.get(i).getCorrect_option();
            boolean isCorrect = true;
            boolean isSkiped = false;
            try {
                if (list.get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                    if (!list.get(i).getUser_ans().get(0).equalsIgnoreCase("") && !list.get(i).getUser_ans().get(0).equalsIgnoreCase("null") && !list.get(i).getUser_ans().get(0).equalsIgnoreCase("0"))
                        if (!correctAns.equalsIgnoreCase(list.get(i).getUser_ans().get(0))) {
                            isCorrect = false;
                        }
                    if (list.get(i).getUser_ans().get(0).equalsIgnoreCase("") || list.get(i).getUser_ans().get(0).equalsIgnoreCase("null") || list.get(i).getUser_ans().get(0).equalsIgnoreCase("0")) {
                        answer_type.setText("Skiped Question");
                        answer_type.setTextColor(Color.GRAY);
                    } else if (isCorrect) {
                        answer_type.setText("Correct Answer");
                        answer_type.setTextColor(Color.GREEN);
                    } else {
                        answer_type.setText("Wrong Answer");
                        answer_type.setTextColor(Color.RED);
                    }
                } else if (list.get(i).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                    isCorrect = list.get(i).isCorrect();
                    if (list.get(i).getUser_ans() == null || list.get(i).getUser_ans().size() == 0)
                        isSkiped = true;

                    if (isSkiped) {
                        answer_type.setText("Skiped Question");
                        answer_type.setTextColor(Color.GRAY);
                    } else if (isCorrect) {
                        answer_type.setText("Correct Answer");
                        answer_type.setTextColor(Color.GREEN);
                    } else {
                        answer_type.setText("Wrong Answer");
                        answer_type.setTextColor(Color.RED);
                    }
                }else {
                    if (!TextUtils.isEmpty(list.get(i).getUser_input()))
                        isCorrect = true;
                    else
                        isSkiped = true;

                    if (isSkiped) {
                        answer_type.setText("Skiped Question");
                        answer_type.setTextColor(Color.GRAY);
                    } else if (isCorrect) {
                        answer_type.setText("Correct Answer");
                        answer_type.setTextColor(Color.GREEN);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            img1.setVisibility(View.GONE);
            img2.setVisibility(View.GONE);
            img3.setVisibility(View.GONE);
            img4.setVisibility(View.GONE);
            img1.setOnClickListener(this);
            img2.setOnClickListener(this);
            img3.setOnClickListener(this);
            img4.setOnClickListener(this);
            if (list.get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                if (list.get(i).getOpt1clicked() == 1) {
//                    img1.setVisibility(View.VISIBLE);

                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                        tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                        tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption1.setVisibility(View.VISIBLE);
                        tvoption1.setTextColor(Color.WHITE);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                    } else {
                        tvoption1.setVisibility(View.GONE);
                        rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                        rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                    }

                }
                if (list.get(i).getOpt2clicked() == 1) {
//                    img2.setVisibility(View.VISIBLE);

                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                        tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                        tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption2.setVisibility(View.VISIBLE);
                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.WHITE);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                    } else {
                        tvoption2.setVisibility(View.GONE);
                        rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                        rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                    }

                }
                if (list.get(i).getOpt3clicked() == 1) {
//                    img3.setVisibility(View.VISIBLE);

                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                        tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                        tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption3.setVisibility(View.VISIBLE);
                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.WHITE);
                        tvoption4.setTextColor(Color.BLACK);
                    } else {
                        tvoption3.setVisibility(View.GONE);
                        rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                        rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                    }

                }
                if (list.get(i).getOpt4clicked() == 1) {
//                    img4.setVisibility(View.VISIBLE);

                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                        tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                        tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                        tvoption4.setVisibility(View.VISIBLE);
                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.WHITE);
                    } else {
                        tvoption4.setVisibility(View.GONE);
                        rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                        rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                    }

                }
                if (list.get(i).getUser_ans().size() == 0)
                    isSkiped = true;

                if (!isCorrect) {
                    clear();


                    if (list.get(i).getOpt1clicked() == 1) {
//                        img1.setVisibility(View.VISIBLE);

                        if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                            tvoption1.setBackgroundResource(R.drawable.dashboard_border_red);
                            tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption1.setVisibility(View.VISIBLE);
                            tvoption1.setTextColor(Color.WHITE);
                            tvoption2.setTextColor(Color.BLACK);
                            tvoption3.setTextColor(Color.BLACK);
                            tvoption4.setTextColor(Color.BLACK);
                        } else {
                            tvoption1.setVisibility(View.GONE);
                            rl1.setBackgroundResource(R.drawable.dashboard_border_red);
                            rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                        }

                    }
                    if (list.get(i).getOpt2clicked() == 1) {
//                        img2.setVisibility(View.VISIBLE);

                        if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                            tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption2.setBackgroundResource(R.drawable.dashboard_border_red);
                            tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption2.setVisibility(View.VISIBLE);
                            tvoption1.setTextColor(Color.BLACK);
                            tvoption2.setTextColor(Color.WHITE);
                            tvoption3.setTextColor(Color.BLACK);
                            tvoption4.setTextColor(Color.BLACK);
                        } else {
                            tvoption2.setVisibility(View.GONE);
                            rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl2.setBackgroundResource(R.drawable.dashboard_border_red);
                            rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                        }

                    }
                    if (list.get(i).getOpt3clicked() == 1) {
//                        img3.setVisibility(View.VISIBLE);

                        if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                            tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption3.setBackgroundResource(R.drawable.dashboard_border_red);
                            tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption3.setVisibility(View.VISIBLE);
                            tvoption1.setTextColor(Color.BLACK);
                            tvoption2.setTextColor(Color.BLACK);
                            tvoption3.setTextColor(Color.WHITE);
                            tvoption4.setTextColor(Color.BLACK);
                        } else {
                            tvoption3.setVisibility(View.GONE);
                            rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl3.setBackgroundResource(R.drawable.dashboard_border_red);
                            rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                        }

                    }
                    if (list.get(i).getOpt4clicked() == 1) {
//                        img4.setVisibility(View.VISIBLE);

                        if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                            tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                            tvoption4.setBackgroundResource(R.drawable.dashboard_border_red);
                            tvoption4.setVisibility(View.VISIBLE);
                            tvoption1.setTextColor(Color.BLACK);
                            tvoption2.setTextColor(Color.BLACK);
                            tvoption3.setTextColor(Color.BLACK);
                            tvoption4.setTextColor(Color.WHITE);
                        } else {
                            tvoption4.setVisibility(View.GONE);
                            rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                            rl4.setBackgroundResource(R.drawable.dashboard_border_red);
                        }

                    }

                    img1.setVisibility(View.GONE);
                    img2.setVisibility(View.GONE);
                    img3.setVisibility(View.GONE);
                    img4.setVisibility(View.GONE);
                    if (correctAns.equalsIgnoreCase("1")) {
//                        img1.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                            tvoption1.setTextColor(Color.WHITE);
                            tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption1.setVisibility(View.GONE);
                            rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    } else if (correctAns.equalsIgnoreCase("2")) {
//                        img2.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                            tvoption2.setTextColor(Color.WHITE);
                            tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption2.setVisibility(View.GONE);
                            rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    } else if (correctAns.equalsIgnoreCase("3")) {
//                        img3.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                            tvoption3.setTextColor(Color.WHITE);
                            tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption3.setVisibility(View.GONE);
                            rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    } else if (correctAns.equalsIgnoreCase("4")) {
//                        img4.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                            tvoption4.setTextColor(Color.WHITE);
                            tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption4.setVisibility(View.GONE);
                            rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    }

                }


                if (isSkiped) {
                    img1.setVisibility(View.GONE);
                    img2.setVisibility(View.GONE);
                    img3.setVisibility(View.GONE);
                    img4.setVisibility(View.GONE);
                    rl1.setBackgroundColor(Color.WHITE);
                    rl2.setBackgroundColor(Color.WHITE);
                    rl3.setBackgroundColor(Color.WHITE);
                    rl4.setBackgroundColor(Color.WHITE);
                    if (correctAns.equalsIgnoreCase("1")) {
//                        img1.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                            tvoption1.setTextColor(Color.WHITE);
                            tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption1.setVisibility(View.GONE);
                            rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    } else if (correctAns.equalsIgnoreCase("2")) {
//                        img2.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                            tvoption2.setTextColor(Color.WHITE);
                            tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption2.setVisibility(View.GONE);
                            rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    } else if (correctAns.equalsIgnoreCase("3")) {
//                        img3.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                            tvoption3.setTextColor(Color.WHITE);
                            tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption3.setVisibility(View.GONE);
                            rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    } else if (correctAns.equalsIgnoreCase("4")) {
//                        img4.setVisibility(View.VISIBLE);
                        if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                            tvoption4.setTextColor(Color.WHITE);
                            tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                        } else {
                            tvoption4.setVisibility(View.GONE);
                            rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                        }
                    }

                }
            } else if (list.get(i).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                tvCorrectOptions.setVisibility(View.VISIBLE);
                tvUserAnswer.setVisibility(View.VISIBLE);
                tvCorrectOptionType.setVisibility(View.VISIBLE);
                tvCorrectOptions.setText("Correct Ans - " + list.get(i).getCorrect_option());
                if (!isSkiped) {
                    tvUserAnswer.setVisibility(View.VISIBLE);
                    tvUserAnswer.setText("User Ans - " + QuizUtils.convertAlToString(list.get(i).getUser_ans()));
                } else
                    tvUserAnswer.setVisibility(View.GONE);
                tvCorrectOptionType.setText("Correct Option Type = " + list.get(i).getCorrect_option_type());
            } else {
                if (!TextUtils.isEmpty(list.get(i).getUser_input())) {
                    tvoption1.setTextColor(Color.WHITE);
                    tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    tvoption1.setText(list.get(i).getUser_input());
                } else {
                    tvoption1.setText("N/A");
                    tvoption1.setTextColor(Color.BLACK);
                    tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                }

            }
            if (TEST_PATTERN.equalsIgnoreCase("COMPREHENSION")) {
                img1.setVisibility(View.GONE);
                img2.setVisibility(View.GONE);
                img3.setVisibility(View.GONE);
                img4.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void onClick(View v) {
        if (MSG.equalsIgnoreCase(""))
            return;
        showAlart(MSG);
    }

    private void showAlart(String msg) {
        new AlertDialog.Builder(ResultAssignment.this)
                .setTitle("Description")
                .setMessage(Html.fromHtml(msg))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}