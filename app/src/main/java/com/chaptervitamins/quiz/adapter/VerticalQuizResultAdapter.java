package com.chaptervitamins.quiz.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.model.Vertical;

import java.util.ArrayList;

/**
 * Created by Tanuj pan on 03-08-2017.
 */

public class VerticalQuizResultAdapter extends RecyclerView.Adapter<VerticalQuizResultAdapter.ViewHolder> {
    private ArrayList<Vertical> verticalArrayList;
    private Context mContext;
    int verticalSize=0;
    public VerticalQuizResultAdapter(Context context,
                                     ArrayList<Vertical> verticalArrayList) {
        this.verticalArrayList = verticalArrayList;
        this.mContext=context;
        verticalSize=verticalArrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.vertical_score_list_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        int totalQuiz=0,urScore=0;
        if (holder != null && position != -1&& position<verticalSize) {
            Vertical vertical = verticalArrayList.get(position);
            holder.verText.setText(vertical.getName().toString());
            holder.totalText.setText(vertical.getTotalNumQuiz()+"");
            holder.scoreText.setText(vertical.getCorrectScore()+"");
            if(vertical.getTotalNumQuiz()!=0)
            holder.perText.setText((vertical.getCorrectScore()*100)/vertical.getTotalNumQuiz()+" %");
        }else{
            holder.verText.setText("Total");
            for(Vertical vertical :verticalArrayList){
                totalQuiz=totalQuiz+ vertical.getTotalNumQuiz();
                urScore=urScore+ vertical.getCorrectScore();
            }
            holder.totalText.setText(totalQuiz+"");
            holder.scoreText.setText(urScore+"");
            holder.perText.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return verticalArrayList.size()+1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView verText,totalText,scoreText,perText;

        public ViewHolder(View itemView) {
            super(itemView);
            verText = (TextView) itemView.findViewById(R.id.verText);
            totalText = (TextView)itemView.findViewById(R.id.totalText);
            scoreText = (TextView) itemView.findViewById(R.id.scoreText);
            perText = (TextView) itemView.findViewById(R.id.perText);

        }
    }
}
