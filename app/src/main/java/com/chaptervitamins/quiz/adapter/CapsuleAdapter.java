package com.chaptervitamins.quiz.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.models.Capsule;

import java.util.ArrayList;

public class CapsuleAdapter extends RecyclerView.Adapter<CapsuleAdapter.CapsuleViewHolder> {
    private ArrayList<Capsule> mList;
    private OnItemsClickListener mListener;

    public CapsuleAdapter(ArrayList<Capsule> mList, OnItemsClickListener mListener) {
        this.mList = mList;
        this.mListener = mListener;
    }

    public interface OnItemsClickListener {

    }

    @Override
    public CapsuleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_capsule, parent, false);
        return new CapsuleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CapsuleViewHolder holder, int position) {
        //holder.tvQuestion.setText(mList.get(position).getmQuestions().get(position).getDescription());
        //holder.tvAnswer.setText(mList.get(position).getmQuestions().get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class CapsuleViewHolder extends RecyclerView.ViewHolder {
        TextView tvQuestion, tvAnswer;

        public CapsuleViewHolder(View itemView) {
            super(itemView);

            tvQuestion = (TextView) itemView.findViewById(R.id.question);
            tvAnswer = (TextView) itemView.findViewById(R.id.answer);
        }
    }
}
