package com.chaptervitamins.quiz.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.quiz.Data_util;

import java.util.ArrayList;

/**
 * Created by test on 03-04-2018.
 */

public class CustomGridAdapter extends BaseAdapter {
    private Context context;
    ArrayList<Data_util> cardUtilities = new ArrayList<>();
    LayoutInflater inflater;
    String verticalName;

    //Constructor to initialize values
    public CustomGridAdapter(Context context, ArrayList<Data_util> utilities) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        cardUtilities = utilities;
    }
    @Override
    public int getCount() {
        // Number of times getView method call depends upon gridValues.length
        return cardUtilities.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }


    // Number of times getView method call depends upon gridValues.length

    public View getView(int position, View convertView, ViewGroup parent) {

        // LayoutInflator to call external grid_item.xml file

        convertView = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_row, null);
            TextView textView = (TextView) convertView
                    .findViewById(R.id.count_txt);
            LinearLayout headerLinearLayout = (LinearLayout) convertView
                    .findViewById(R.id.headerLinearLayout);
            TextView verticalNameTxtView = (TextView) convertView
                    .findViewById(R.id.verticalNameTxtView);
            textView.setText((position + 1) + "");

            LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.row_ll);
           /* if(cardUtilities.get(position).getVertical()!=null&&
                    !TextUtils.isEmpty(cardUtilities.get(position).getVertical())) {
                if (position == 0) {
                    verticalName = cardUtilities.get(position).getVertical();
                    headerLinearLayout.setVisibility(View.VISIBLE);
                    verticalNameTxtView.setText(verticalName);
                } else if (!verticalName.equalsIgnoreCase(cardUtilities.get(position).getVertical())) {
                    headerLinearLayout.setVisibility(View.VISIBLE);
                    verticalName = cardUtilities.get(position).getVertical();
                } else {
                    verticalNameTxtView.setText(verticalName);
                    headerLinearLayout.setVisibility(View.GONE);
                }
            }*/
            if (cardUtilities != null && cardUtilities.size() > 0 && cardUtilities.get(position) != null && cardUtilities.get(position).getUser_ans() != null &&
                    cardUtilities.get(position).getUser_ans().size() > 0) {
                if (cardUtilities.get(position).getUser_ans().get(0).equalsIgnoreCase("")) {
                    textView.setBackgroundResource(R.drawable.skip_bg);
                } else {
                    textView.setBackgroundResource(R.drawable.rounded_corner_green_button);
                }
            }

        }

        return convertView;
    }
}
