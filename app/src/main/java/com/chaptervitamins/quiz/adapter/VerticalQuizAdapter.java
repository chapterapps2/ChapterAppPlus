package com.chaptervitamins.quiz.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.model.Vertical;

import java.util.ArrayList;

/**
 * Created by Tanuj pan on 03-08-2017.
 */

public class VerticalQuizAdapter extends RecyclerView.Adapter<VerticalQuizAdapter.ViewHolder> {
    private ArrayList<Data_util> dataUtilArrayList;
    private ArrayList<Vertical> verticalArrayList;
    private Context mContext;
    String verticalName;
    private int lastSelectedPosition = -1;

    public VerticalQuizAdapter(Context context, ArrayList<Data_util> dataUtilArrayList,
                               ArrayList<Vertical> verticalArrayList) {
        this.dataUtilArrayList = dataUtilArrayList;
        this.verticalArrayList = verticalArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.vertical_list_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (holder != null && position != -1) {
            int posNum=position;
            Vertical vertical = verticalArrayList.get(position);
            holder.noQusTextView.setText(vertical.getTotalNumQuiz()+"");
            holder.noTextView.setText((posNum+1)+"");
            holder.radioBtn.setChecked(position==lastSelectedPosition);
            holder.radioBtn.setText(vertical.getName());

        }

    }

    @Override
    public int getItemCount() {
        return verticalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView noTextView, noQusTextView;
        public AppCompatRadioButton radioBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            noTextView = (TextView) itemView.findViewById(R.id.noTextView);
            noQusTextView = (TextView) itemView.findViewById(R.id.noQusTextView);
            radioBtn = (AppCompatRadioButton) itemView.findViewById(R.id.radioButton);
            radioBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                }
            });
        }
    }
}
