package com.chaptervitamins.quiz.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.chaptervitamins.CustomView.ExpandableHeightGridView;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.model.Vertical;

import java.util.ArrayList;

/**
 * Created by Tanuj pan on 03-08-2017.
 */

public class VerticalWiseQuizAdapter extends RecyclerView.Adapter<VerticalWiseQuizAdapter.ViewHolder> {
    private ArrayList<Data_util> dataUtilArrayList;
    private ArrayList<Vertical> verticalArrayList;
    private Context mContext;
    private OnVerticalClickListener mListener;


    public static CustomGridAdapter customGridAdapter;


    public VerticalWiseQuizAdapter(Context context,
                                   ArrayList<Vertical> verticalArrayList, ArrayList<Data_util> dataUtilArrayList) {
        this.dataUtilArrayList = dataUtilArrayList;
        this.verticalArrayList = verticalArrayList;
        this.mContext = context;
        mListener = (OnVerticalClickListener) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.vertical_wise_list_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (holder != null && position != -1) {
            Vertical vertical = verticalArrayList.get(position);
            setDataIntoGridView(vertical.getName(), holder, vertical);
        }

    }

    private void setDataIntoGridView(String vertical, ViewHolder holder, Vertical verticalModel) {
        final ArrayList<Data_util> dataUtilArrayList = new ArrayList<>();
        int skipCount = 0, ansCount = 0;
        for (Data_util data_util : WebServices.questionUtility.getData_utils()) {
            if (vertical.equalsIgnoreCase(data_util.getVertical())) {
                if (data_util.getUser_ans() != null && data_util.getUser_ans().size() > 0) {
                    ansCount++;
                } else {
                    skipCount++;
                }
                dataUtilArrayList.add(data_util);
            }
        }
        verticalModel.setSkipQuiz(skipCount);
        verticalModel.setAttQuiz(ansCount);
        holder.verTxtView.setText(verticalModel.getName() + "\n  (Skipped: " + verticalModel.getSkipQuiz() + "\t Attempted: " + verticalModel.getAttQuiz() + ")");
        holder.gridView.setExpanded(true);
        customGridAdapter = new CustomGridAdapter(mContext, dataUtilArrayList);
        holder.gridView.setAdapter(customGridAdapter);
        holder.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    mListener.onVerticalClick(dataUtilArrayList.get(position).getQuestion_bank_id(), position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return verticalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView verTxtView;
        private ExpandableHeightGridView gridView;

        public ViewHolder(View itemView) {
            super(itemView);
            verTxtView = (TextView) itemView.findViewById(R.id.verTxtView);
            gridView = (ExpandableHeightGridView) itemView.findViewById(R.id.gridView);

        }
    }

    public interface OnVerticalClickListener {
        void onVerticalClick(String bank_id, int position);
    }
}
