package com.chaptervitamins.quiz;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.play_video.Link_Activity;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifTextView;


/**
 * Created by Android on 9/7/2016.
 */

public class ThankYou_Activity extends BaseActivity implements View.OnClickListener, RatingListener {
    private TextView tvtime;
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private MeterialUtility meterialUtility;
    private GifTextView goldGif;
    private boolean isNextButtonClicked;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thankyou_activity);
        TextView tvbacktochapter = (TextView) findViewById(R.id.tvbacktochapter);
        tvtime = (TextView) findViewById(R.id.tvtime);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_Utility");
            tvtime.setText(bundle.getString("msg"));
            String coinsCollected = bundle.getString("coins");
            if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
                TextView tvCOins = (TextView) findViewById(R.id.tv_coins);
                tvCOins.setText("You have been awarded with " + coinsCollected + " Bonus points!");
                tvCOins.setVisibility(View.VISIBLE);
            }
          /*  if (WebServices.isNetworkAvailable(this)) {
                FragmentManager fm = getSupportFragmentManager();
                CustomDialog custom = new CustomDialog();
                custom.setParamCustomDialog(this, meterialUtility.getMaterial_id(),false);
                custom.show(fm, "");
            }*/
           /* CoinsAllocatedModel coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getCourse_id(), meterialUtility.getMaterial_id());

            if (coinsAllocatedModel != null) {
                final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(100, coinsAllocatedModel.getRedeem()));
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
                    showGoldGif();
                    Toast.makeText(ThankYou_Activity.this, "You have awarded " + coinsCollected + " coins", Toast.LENGTH_SHORT).show();
                    if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                        coinsAllocatedModel.setRedeem("TRUE");
                        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                            WebServices.updateTotalCoins(ThankYou_Activity.this, coinsCollected);

                    }
                }
            }*/
        }

        setFlowingCourse();
        getModuleData();
        setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        tvbacktochapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(meterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, meterialUtility.getModule_id());
    }


    private void setFlowingCourse() {
        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(meterialUtility.getCourse_id(), meterialUtility.getModule_id());
        position = FlowingCourseUtils.getPositionOfMeterial(meterialUtility.getMaterial_id(), meterialUtilityArrayList);
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()
                ) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(ThankYou_Activity.this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(9999);
        if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(ThankYou_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
            showRatingDialog(true);

        } else {
            finish();
        }

    }

    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(ThankYou_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(ThankYou_Activity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating())&& meterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(ThankYou_Activity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(ThankYou_Activity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(ThankYou_Activity.this) &&
                        meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(ThankYou_Activity.this, meterialUtility, true);
                break;
        }

    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(ThankYou_Activity.this, meterialUtility.getMaterial_id(), wannaFinish, ThankYou_Activity.this);
        custom.show(fm, "");
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(ThankYou_Activity.this, meterialUtilityArrayList, position, isNextButtonClicked);
    }
}
