package com.chaptervitamins.quiz;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.CustomView.Custom_Progress;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.LeaderboardActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.server.ApiCallUtils;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.droidsonroids.gif.GifTextView;

public class ResultActivity extends BaseActivity implements View.OnClickListener, RatingListener {
    @BindView(R.id.quiz_name_txt)
    TextView quiz_name_txt;
    @BindView(R.id.your_score_txt)
    TextView your_score_txt;
    @BindView(R.id.donut_progress)
    Custom_Progress donutProgress;
    @BindView(R.id.tvtime)
    TextView tvtime;
    @BindView(R.id.tvbacktochapter)
    TextView tvbacktochapter;
    @BindView(R.id.tvname)
    TextView tvname;
    @BindView(R.id.tvremarks)
    TextView tvremarks;
    @BindView(R.id.quit_txt)
    TextView quit_txt;
    @BindView(R.id.tvviewanswer_ll)
    LinearLayout tvviewanswer_ll;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.linear4)
    LinearLayout linear4;
    @BindView(R.id.linear5)
    LinearLayout linear5;
    @BindView(R.id.tvseeleaderboard)
    TextView tvseeleaderboard;
    @BindView(R.id.show_marks_txt)
    TextView show_marks_txt;
    @BindView(R.id.nameTxtView)
    TextView nameTxtView;
    @BindView(R.id.idTxtView)
    TextView idTxtView;
    @BindView(R.id.emailTxtView)
    TextView emailTxtView;
    @BindView(R.id.subTxtView)
    TextView subTxtView;
    @BindView(R.id.timeTxtView)
    TextView timeTxtView;
    int TotalMarks = 0;
    int UserTotalMarks = 0;
    int corr_que = 0, incorr_que = 0, skip_que = 0;
    DataBase dataBase;
    WebServices webServices;
    String resp = "";
    private Dialog mDialog;
    private ProgressBar mBar;
    private TextView totalPer_txt, title_txt, total_file_txt;
    private Button abort_btn = null;
    private boolean isDownload = true;
    String MSG = "Download Sucessfully!";
    private String FILEPATH = null;
    private int totallenghtOfFile = 0;
    private String totalVal = "-1";

    Handler timeout = null;
    int[] count = {10};//for 40 seconds to wait for buffering after it will finish the activity
    boolean istimeout = true;
    private float PASS_PER = 0;
    private MeterialUtility meterialUtility;
    private String complete = "", showAnswer = "", TEST_PATTERN = "", isDone = "";
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private GifTextView goldGif;
    private boolean isNextButtonClicked;

    private Button btnNextCourse;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_Utility");
            complete = bundle.getString("time");
            showAnswer = meterialUtility.getShow_answer();
            TEST_PATTERN = bundle.getString("test_pattern");
            isDone = bundle.getString("isdone");
            if (meterialUtility != null) {
                setDataIntoUserDetailView();
            }


//            coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getCourse_id(), meterialUtility.getMaterial_id());
            coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
           /* if (coinsAllocatedModel == null) {
                coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getMaterial_id());
            }*/
            if (coinsAllocatedModel != null) {
                redeem = coinsAllocatedModel.getRedeem();
                noOfCoins = coinsAllocatedModel.getMaxCoins();
            } else {
                coinsAllocatedModel = new CoinsAllocatedModel();
            }

            meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(meterialUtility.getCourse_id(), meterialUtility.getModule_id());
            position = FlowingCourseUtils.getPositionOfMeterial(meterialUtility.getMaterial_id(), meterialUtilityArrayList);
            if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
                int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(ResultActivity.this, position, meterialUtilityArrayList);
                FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
                FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
                btnPrevious.setOnClickListener(this);
                btnNext.setOnClickListener(this);
            }
            getModuleData();
            setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
            webServices = new WebServices();
            dataBase = DataBase.getInstance(ResultActivity.this);
            quiz_name_txt.setText(meterialUtility.getTitle());
            PASS_PER = Float.parseFloat(meterialUtility.getPassing_percentage());
           /* Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            meterialUtility.setMaterialEndTime(df.format(c.getTime()));*/
//        getIntent().getStringExtra("pass_per")
            tvname.setText(WebServices.mLoginUtility.getFirstname() + "!");
            linear5.setVisibility(View.GONE);
            tvviewanswer_ll.setVisibility(View.GONE);
            /*if (!WebServices.questionUtility.getPassing_percentage().equalsIgnoreCase("") && !WebServices.questionUtility.getPassing_percentage().equalsIgnoreCase("null")) {
                if (WebServices.questionUtility.getPassing_percentage() != null)
                    PASS_PER = Integer.parseInt(WebServices.questionUtility.getPassing_percentage());
            }*/
        /*for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {
            TotalMarks = TotalMarks + Integer.parseInt(WebServices.questionUtility.getData_utils().get(i).getMarks());
            if(WebServices.questionUtility.getData_utils().get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                if (WebServices.questionUtility.getData_utils().get(i).getUser_ans() != null && !WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0).equalsIgnoreCase("")) {
                    if (WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0).equalsIgnoreCase(WebServices.questionUtility.getData_utils().get(i).getCorrect_option())) {

                        corr_que++;UserTotalMarks = UserTotalMarks + Integer.parseInt(WebServices.questionUtility.getData_utils().get(i).getMarks());
                    } else {
                        incorr_que++;
                    }
                } else {
                    skip_que++;
                }
            }else{*/
            int size = WebServices.questionUtility.getData_utils().size();
            for (int j = 0; j < size; j++) {
                Data_util dataUtil = WebServices.questionUtility.getData_utils().get(j);
                TotalMarks = TotalMarks + Integer.parseInt(dataUtil.getMarks());
                if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                    if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        ArrayList<String> correctAnswerAl = QuizUtils.convertStringToAl(dataUtil.getCorrect_option());
                        if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                            String correctOptionType = dataUtil.getCorrect_option_type();
                            switch (correctOptionType) {
                                case "ALL":
                                    if (QuizUtils.checkAnsForAll(dataUtil.getUser_ans(), correctAnswerAl)) {
                                        dataUtil.setCorrect(true);
                                        UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                                        corr_que++;
                                    } else {
                                        dataUtil.setCorrect(false);
                                        incorr_que++;
                                    }
                                    break;
                                case "ANY":
                                    if (QuizUtils.checkAnsForAny(dataUtil.getUser_ans(), correctAnswerAl)) {
                                        dataUtil.setCorrect(true);
                                        UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                                        corr_que++;
                                    } else {
                                        dataUtil.setCorrect(false);
                                        incorr_que++;
                                    }
                                    break;
                            }
                        }
                    } else {
                        skip_que++;
                    }

                } else {
                    if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        if (dataUtil.getUser_ans().get(0).equalsIgnoreCase(dataUtil.getCorrect_option())) {
                            dataUtil.setCorrect(true);
                            UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                            corr_que++;
                        } else {
                            dataUtil.setCorrect(false);
                            incorr_que++;
                        }
                    } else {
                        skip_que++;
                    }
                }
            }

            float percent = 0;
            if ((corr_que + incorr_que + skip_que) != WebServices.questionUtility.getData_utils().size()) {
                new AlertDialog.Builder(ResultActivity.this)
                        .setTitle("Error")
                        .setMessage("Some error occurred. Can not submit Quiz this time. Please try again.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })


                        .show();
            } else {
                if (TotalMarks != 0)
                    percent = (UserTotalMarks * 100) / TotalMarks;
                donutProgress.setStartingDegree(270);
//        total_marks_txt.setText(percent+" %");
                if (WebServices.isNetworkAvailable(ResultActivity.this) && meterialUtility.getShow_leaderboard().equalsIgnoreCase("yes")) {
                    linear4.setVisibility(View.VISIBLE);
                }
                if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSION")) {
//                    donutProgress.setVisibility(View.GONE);
                    tvviewanswer_ll.setVisibility(View.GONE);

                    your_score_txt.setText("");
                    linear4.setVisibility(View.GONE);
                    if (isDone.equalsIgnoreCase("yes")) {
                        if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes")
                                && percent >= Float.parseFloat(meterialUtility.getPassing_percentage())) {
                            if (meterialUtility.getIsResultPublishRequired().equalsIgnoreCase("YES")) {
                                if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes"))
                                    linear5.setVisibility(View.VISIBLE);
                                else
                                    linear5.setVisibility(View.GONE);
                            } else
                                linear5.setVisibility(View.VISIBLE);
                        } else
                            linear5.setVisibility(View.GONE);
//                        failpass_img.setImageResource(R.drawable.smily_happy);
                        String next = "<font color='#000000'>Pass</font>";
                        show_marks_txt.setText(Html.fromHtml("Marks " + UserTotalMarks + "/" + TotalMarks + "  : " + next));
                        tvtime.setText(" Congratulations! You have completed the Test in " + Utils.diffDate(Long.parseLong(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()))) + " min");
                    } else if (isDone.equalsIgnoreCase("no")) {
                        linear.setVisibility(View.GONE);
                        quit_txt.setVisibility(View.VISIBLE);
                        String next = "<font color='#FF0000'>Fail</font>";
                        show_marks_txt.setText(Html.fromHtml("Marks " + UserTotalMarks + "/" + TotalMarks + "  : " + next));
//                        failpass_img.setImageResource(R.drawable.smily_sad);
                        quit_txt.setText("You have decided to Quit the Test!");
                        tvtime.setText("  You spent " + Utils.diffDate(Long.parseLong(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()))) + " min");
                    }
                } else {
                    if (isDone.equalsIgnoreCase("yes")) {
                        if (percent >= PASS_PER) {
                            tvtime.setText(" Congratulations! You have Passed the Test in " + Utils.diffDate(Long.parseLong(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()))) + " min");
                            if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes")
                                    && percent >= Float.parseFloat(meterialUtility.getPassing_percentage())) {
                                if (meterialUtility.getIsResultPublishRequired().equalsIgnoreCase("YES")) {
                                    if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes"))
                                        linear5.setVisibility(View.VISIBLE);
                                    else
                                        linear5.setVisibility(View.GONE);
                                } else
                                    linear5.setVisibility(View.VISIBLE);
                            } else
                                linear5.setVisibility(View.GONE);
                            String next = "<font color='#000000'>Pass</font>";
                            show_marks_txt.setText(Html.fromHtml("Marks " + UserTotalMarks + "/" + TotalMarks + "  : " + next));
//                            failpass_img.setImageResource(R.drawable.smily_happy);
                        } else {
                            linear.setVisibility(View.GONE);
                            quit_txt.setVisibility(View.VISIBLE);
                            String next = "<font color='#FF0000'>Fail</font>";
                            show_marks_txt.setText(Html.fromHtml("Marks " + UserTotalMarks + "/" + TotalMarks + "  : " + next));
                            quit_txt.setText("Sorry! You could not Pass the Test this time.");
//                            failpass_img.setImageResource(R.drawable.smily_sad);
                            tvtime.setText(" You spent " + Utils.diffDate(Long.parseLong(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()))) + " min");
                        }
                    } else if (isDone.equalsIgnoreCase("no")) {
                        linear.setVisibility(View.GONE);
                        quit_txt.setVisibility(View.VISIBLE);
//                        failpass_img.setVisibility(View.GONE);
                        show_marks_txt.setVisibility(View.GONE);
                        quit_txt.setText("You have decided to Quit the Test!");
                        tvtime.setText("  You spent " + Utils.diffDate(Long.parseLong(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()))) + " min");
                    }
                }
                /*if (isDone.equalsIgnoreCase("yes")) {
                    savequizResponse(percent + "", corr_que + "", incorr_que + "");
                    MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(ResultActivity.this);
                    mixPanelManager.completematerial(ResultActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "Quiz|" + percent + "|" + UserTotalMarks + "|" + TotalMarks, meterialUtility.getMaterial_id());
                } else if (isDone.equalsIgnoreCase("no")) {
//                savequizResponse(percent + "", corr_que + "", incorr_que + "");
                }*/
                linear5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ApiCallUtils.showCertificate(ResultActivity.this, MixPanelManager.getInstance(), dataBase, meterialUtility);
                    }
                });
                final float finalPercent = percent;
                tvseeleaderboard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (WebServices.isNetworkAvailable(ResultActivity.this)) {
                            final ProgressDialog dialog = ProgressDialog.show(ResultActivity.this, "", "Please wait...");
                            dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
                            final Handler handler = new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    if (dialog != null) dialog.dismiss();
                                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                                        Utils.callInvalidSession(ResultActivity.this, APIUtility.GETSPECIFICQUIZHISTORY);
                                        return;
                                    } else {
                                        Intent intent = new Intent(ResultActivity.this, LeaderboardActivity.class);
                                        Bundle bundle1 = new Bundle();
                                        bundle1.putString("resp", resp);
                                        bundle1.putInt("marks", (int) finalPercent);
                                        bundle1.putInt("correct_ques", corr_que);
                                        bundle1.putInt("incorrect_ques", incorr_que);
                                        bundle1.putInt("skipped_ques", skip_que);
                                        bundle1.putBoolean("show_details", true);
                                        bundle1.putSerializable("meterial_utility", meterialUtility);
                                        intent.putExtras(bundle1);
                                        startActivity(intent);
                                    }
                                }
                            };
                            new Thread() {
                                @Override
                                public void run() {
                                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                                    nameValuePair.add(new BasicNameValuePair("material_id", meterialUtility.getMaterial_id()));
                                    nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                                    resp = webServices.callServices(nameValuePair, APIUtility.GETSPECIFICQUIZHISTORY);
                                    if (!webServices.isValid(resp)) {
                                        handler.sendEmptyMessage(1);
                                    } else
                                        handler.sendEmptyMessage(0);
                                }
                            }.start();
                        } else {
                            Toast.makeText(ResultActivity.this, getResources().getText(R.string.no_internet), Toast.LENGTH_LONG).show();
                        }
                    }
                });
                donutProgress.setProgress((int) percent);
                if (showAnswer.equalsIgnoreCase("yes"))
                    tvviewanswer_ll.setVisibility(View.VISIBLE);
                else if (showAnswer.equalsIgnoreCase("PASSED")) {
                    if (!WebServices.questionUtility.getPassing_percentage().equalsIgnoreCase("") && !WebServices.questionUtility.getPassing_percentage().equalsIgnoreCase("null")) {
                        if (WebServices.questionUtility.getPassing_percentage() != null)
                            if (percent >= PASS_PER)
                                tvviewanswer_ll.setVisibility(View.VISIBLE);
                    }
                }
                tvviewanswer_ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ResultActivity.this, ResultAssignment.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("meterial_Utility", meterialUtility);
                        bundle.putString("time", complete);
                        bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                        bundle.putString("test_pattern", TEST_PATTERN);
                        bundle.putString("isdone", isDone);
                        bundle.putString("tag", "");
                        intent.putExtras(bundle);
                   /* intent.putExtra("test_pattern", getIntent().getStringExtra("test_pattern"));
                    intent.putExtra("time", getIntent().getStringExtra("time"));
                    intent.putExtra("name", getIntent().getStringExtra("name"));
                    intent.putExtra("show_answer", getIntent().getStringExtra("show_answer"));
                    intent.putExtra("meterial_id", getIntent().getStringExtra("meterial_id"));
                    intent.putExtra("isdone", getIntent().getStringExtra("isdone"));
                    intent.putExtra("show_leaderboard", getIntent().getStringExtra("show_leaderboard"));
                    intent.putExtra("pass_per", getIntent().getStringExtra("pass_per"));
                    intent.putExtra("tag", "");*/
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    }
                });

                tvbacktochapter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setResult(2000);
                        if (WebServices.isNetworkAvailable(ResultActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                            showRatingDialog(true);
                        } else {
                            finish();
                        }
                    }
                });
            }
        }
    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(meterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, meterialUtility.getModule_id());

    }

    private void setDataIntoUserDetailView() {
        nameTxtView.setText(WebServices.mLoginUtility.getFirstname());
        idTxtView.setText(WebServices.mLoginUtility.getEmployee_id());
        emailTxtView.setText(WebServices.mLoginUtility.getEmail());
        subTxtView.setText(Utils.getCurrentTimeStamp());
        timeTxtView.setText(Utils.diffDate(Long.parseLong(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()))) + " min");
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(ResultActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
            showRatingDialog(true);
        } else {
            finish();
        }
    }

    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }

    private void savequizResponse(String persentage, String corr, String incorr) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("question_bank_id", WebServices.questionUtility.getData_utils().get(i).getQuestion_bank_id());
                jsonObject.put("assign_question_id", WebServices.questionUtility.getData_utils().get(i).getAssign_question_id());
                jsonObject.put("answer_key", QuizUtils.convertAlToString(WebServices.questionUtility.getData_utils().get(i).getUser_ans()));
                jsonObject.put("correct_option", WebServices.questionUtility.getData_utils().get(i).getCorrect_option());
                jsonObject.put("answer", WebServices.questionUtility.getData_utils().get(i).getUser_input());
                jsonObject.put("answer_type", WebServices.questionUtility.getData_utils().get(i).getOption_type());
                if (WebServices.questionUtility.getData_utils().get(i).isCorrect())
                    jsonObject.put("marks", WebServices.questionUtility.getData_utils().get(i).getMarks());
                else
                    jsonObject.put("marks", "0");
                jsonObject.put("time_taken", WebServices.questionUtility.getData_utils().get(i).getTime_taken());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        if (coinsAllocatedModel != null) {
            String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility, Integer.valueOf(persentage), redeem));
            if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
                TextView tvCOins = (TextView) findViewById(R.id.tv_coins);
                tvCOins.setText("You have been awarded with " + coinsCollected + " Bonus points!");
                tvCOins.setVisibility(View.VISIBLE);
                showGoldGif();
                Toast.makeText(this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();

            }
            new WebServices().setProgressOfMaterial(dataBase, meterialUtility, WebServices.questionUtility.getData_utils().size() + "", WebServices.questionUtility.getData_utils().size() + "", coinsCollected);

            new WebServices().addSubmitResponse(dataBase, meterialUtility, persentage, corr, incorr, jsonArray.toString(), coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

            if (WebServices.isNetworkAvailable(ResultActivity.this))
                new SubmitData(ResultActivity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), meterialUtility.getCoinsAllocatedModel(), dataBase).execute(corr, incorr, jsonArray.toString(), persentage);
            else {
                if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                    coinsAllocatedModel.setRedeem("TRUE");
                    if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                        WebServices.updateTotalCoins(ResultActivity.this, coinsCollected);

                }
                DialogUtils.showDialog(ResultActivity.this, "Your data is stored offline. Please get an internet connection and try to sync your data to server.", null, null);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(ResultActivity.this);
    }

    private void DownloadELearning(final Context context, final String mUrl, final String filename) {
        isDownload = false;
        timeout = null;
        count = new int[]{30};//for 40 seconds to wait for buffering after it will finish the activity
        istimeout = true;
        //        private ProgressDialog pDialog;
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();
                if (msg.what == 0) {
                    if (FILEPATH != null) {
                        Intent intent = new Intent(ResultActivity.this, ViewCertificateActivity.class);
                        intent.putExtra("filename", FILEPATH);
                        startActivity(intent);

                    } else {
                        try {
                            boolean isdelete = new File(ResultActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + filename).delete();
                            System.out.println("=isdelete===" + isdelete + "===" + filename);
                            APIUtility.isDeletedFileFromURL(context, filename + "zip");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                        AppUtility.showToast(context, MSG);
                        Toast.makeText(ResultActivity.this, MSG, Toast.LENGTH_LONG).show();
                    }
                } else if (msg.what == 5) {
                    try {
                        boolean isdelete = new File(ResultActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + filename).delete();
                        System.out.println("=isdelete===" + isdelete + "===" + filename);
                        APIUtility.isDeletedFileFromURL(context, filename + "zip");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        boolean isdelete = new File(ResultActivity.this.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + filename).delete();
                        System.out.println("=isdelete===" + isdelete + "===" + filename);
                        APIUtility.isDeletedFileFromURL(context, filename + "zip");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    AppUtility.showToast(context, MSG);
                    Toast.makeText(ResultActivity.this, MSG, Toast.LENGTH_LONG).show();
                }
            }
        };
        timeout = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                System.out.println("value of count=" + msg.getData().getLong("count"));
                if (msg.getData().getBoolean("valid")) {
                    if (msg.getData().getLong("count") == 0) {

                        if (mDialog != null && mDialog.isShowing()) {
                            if (istimeout) {
                                try {
                                    isDownload = false;
                                    FILEPATH = null;
                                    mDialog.dismiss();
                                    handler.sendEmptyMessage(5);
                                    Toast.makeText(ResultActivity.this, "Internet too slow to download", Toast.LENGTH_LONG).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }

            }
        };
        timeout.postDelayed(null, 0);
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (count[0] > 0) {
                    if (!isDownload)
                        if (istimeout) {
                            try {
                                Thread.sleep(1000);
                            } catch (Exception e) {
                            }

                            Message msg = new Message();
                            Bundle b = new Bundle();
                            b.putBoolean("valid", true);
                            b.putLong("count", --count[0]);
                            msg.setData(b);
                            timeout.sendMessage(msg);
                        } else {
                            --count[0];
                        }

                }

            }
        }).start();

        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDownload = false;
                FILEPATH = null;
                istimeout = false;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(1);
            }
        });
        mDialog.show();

        new Thread() {
            @Override
            public void run() {
                int lenghtOfFile = 0;
                int count;
                File file = null;

                try {
                    URL url = new URL(mUrl);

                    URLConnection conection = url.openConnection();
                    conection.setConnectTimeout(10000);
                    conection.setReadTimeout(10000);
                    conection.connect();
                    // getting file length
                    lenghtOfFile = conection.getContentLength();
                    System.out.println("=====file length===" + (lenghtOfFile / (1024 * 1024)));
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);

                    // Output stream to write file
                    //Download epub file in app private directory /sdcard/Android/data/<package-name>/files/chapters
                    file = getFileFromURL(context, filename);
                    OutputStream output = new FileOutputStream(file);
                    byte data[] = new byte[1024];
//                    runnable.run();
                    long total = 0;
                    isDownload = true;
                    while ((count = input.read(data)) != -1) {
                        istimeout = false;
                        if (isDownload) {
                            total += count;
                            totalVal = "" + (int) ((total * 100) / lenghtOfFile);
                            totallenghtOfFile = lenghtOfFile;
//                            publishProgress();
//                            runnable.notify();//run();
                            // writing data to file

                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    abort_btn.setEnabled(true);
                                    if (totallenghtOfFile != -1) {
                                        String total = (totallenghtOfFile / (1024 * 1000f)) + "";
                                        String[] t = total.split("\\.");
                                        total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                    } else {
                                        total_file_txt.setText("0 MB");
                                    }
                                    title_txt.setText("Fetching... Please wait!");
                                    if (!totalVal.contains("-")) {
                                        mBar.setProgress(Integer.parseInt(totalVal));
                                        totalPer_txt.setText(Integer.valueOf(totalVal) + " %");
                                    } else {
//                                        isDownload = false;
//                                        MSG = "Network issues.. You have been Timed-out";
//                                        mBar.setProgress(0);
//                                        totalPer_txt.setText("0 %");
//                                        handler.sendEmptyMessage(2);
                                    }
                                }
                            });
                            output.write(data, 0, count);
                        } else {
                            // flushing output
                            output.flush();

                            // closing streams
                            output.close();
                            input.close();
                            return;
                        }
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    MSG = "Network issues.. You have been Timed-out";
                    FILEPATH = null;
                    handler.sendEmptyMessage(2);
                    return;
                }
                FILEPATH = file.getAbsolutePath();

                handler.sendEmptyMessage(0);
            }

        }.start();


    }

    public static File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());

        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && !TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(ResultActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(ResultActivity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) && meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(ResultActivity.this) && meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(ResultActivity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(meterialUtility.getShow_rating()) &&
                        meterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(ResultActivity.this) &&
                        meterialUtility != null && TextUtils.isEmpty(meterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(ResultActivity.this, meterialUtility, true);
                break;
        }
    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(ResultActivity.this, meterialUtility.getMaterial_id(), wannaFinish, ResultActivity.this);
        custom.show(fm, "");
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(ResultActivity.this, meterialUtilityArrayList, position, isNextButtonClicked);
    }
}
