package com.chaptervitamins.quiz;

import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by Sagar on 09-07-2017.
 */

public class QuizUtils {

    public static ArrayList<String> convertStringToAl(String correct_option) {
        ArrayList<String> correctAnsAl = new ArrayList<>();
        if (!TextUtils.isEmpty(correct_option)) {
            String[] correctAnsArray = correct_option.split(",");
            if (correctAnsArray != null && correctAnsArray.length > 0) {
                for (int i = 0; i < correctAnsArray.length; i++) {
                    correctAnsAl.add(correctAnsArray[i]);
                }
            }
        }
        return correctAnsAl;
    }

    public static String convertAlToString(ArrayList<String> answerAl) {
        String answer = "";
        if (answerAl != null) {
            for (int i = 0; i < answerAl.size(); i++) {
                if (i < answerAl.size() - 1)
                    answer = answer + answerAl.get(i) + ",";
                else
                    answer = answer + answerAl.get(i);

            }
        }
        return answer;
    }

    public static boolean checkAnsForAll(ArrayList<String> userAnsAl, ArrayList<String> correctAnswerAl) {
        int userAnsSize = userAnsAl.size();
        int correctSize = correctAnswerAl.size();
        int count = 0;
        if (userAnsSize == correctSize) {
            for (int j = 0; j < userAnsSize; j++) {
                for (int k = 0; k < correctSize; k++) {
                    if (userAnsAl.get(j).equals(correctAnswerAl.get(k))) {
                        count++;
                        break;
                    }
                }
            }
        } else {
            return false;
        }
        if (count == correctSize)
            return true;
        else
            return false;
    }

    public static boolean checkAnsForAny(ArrayList<String> userAnsAl, ArrayList<String> correctAnswerAl) {
        int userAnsSize = userAnsAl.size();
        int correctSize = correctAnswerAl.size();
        for (int j = 0; j < correctSize; j++) {
            for (int k = 0; k < userAnsSize; k++) {
                if (correctAnswerAl.get(j).equals(userAnsAl.get(k))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getAnswerAccToOption(Data_util dataUtil) {
        ArrayList<String> userAnsAl = dataUtil.getUser_ans();
        String userAns = "";
        if (userAnsAl.size() > 0) {
            for (int i = 0; i < userAnsAl.size(); i++) {
                switch (userAnsAl.get(i)) {
                    case "1":
                        if (dataUtil.getIs_option1_image().equals("0"))
                            userAns = userAns + "1." + dataUtil.getOption1();
                        else
                            userAns = userAns + "1";
                        break;
                    case "2":
                        if (dataUtil.getIs_option2_image().equals("0"))
                            userAns = userAns + "2." + dataUtil.getOption2();
                        else
                            userAns = userAns + "2";
                        break;
                    case "3":
                        if (dataUtil.getIs_option3_image().equals("0"))
                            userAns = userAns + "3." + dataUtil.getOption3();
                        else
                            userAns = userAns + "3";
                        break;
                    case "4":
                        if (dataUtil.getIs_option4_image().equals("0"))
                            userAns = userAns + "4." + dataUtil.getOption4();
                        else
                            userAns = userAns + "4";
                        break;
                }
            }
        }
        return userAns;
    }

    public static String getRealCorrectOption(Data_util dataUtil) {
        String userAnswer = "";
        if (dataUtil != null) {
            if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0) {
                for (int i = 0; i < dataUtil.getUser_ans().size(); i++) {
                    switch (dataUtil.getUser_ans().get(i)) {
                        case "1":
                            if (i == dataUtil.getUser_ans().size() - 1)
                                userAnswer = userAnswer + dataUtil.getOption1_real_pos();
                            else
                                userAnswer = userAnswer + dataUtil.getOption1_real_pos() + ",";
                            break;
                        case "2":
                            if (i == dataUtil.getUser_ans().size() - 1)
                                userAnswer = userAnswer + dataUtil.getOption2_real_pos();
                            else
                                userAnswer = userAnswer + dataUtil.getOption2_real_pos() + ",";
                            break;
                        case "3":
                            if (i == dataUtil.getUser_ans().size() - 1)
                                userAnswer = userAnswer + dataUtil.getOption3_real_pos();
                            else
                                userAnswer = userAnswer + dataUtil.getOption3_real_pos() + ",";
                            break;
                        case "4":
                            if (i == dataUtil.getUser_ans().size() - 1)
                                userAnswer = userAnswer + dataUtil.getOption4_real_pos();
                            else
                                userAnswer = userAnswer + dataUtil.getOption4_real_pos() + ",";
                            break;
                    }
                }
            }

        }
        return userAnswer;
    }
}
