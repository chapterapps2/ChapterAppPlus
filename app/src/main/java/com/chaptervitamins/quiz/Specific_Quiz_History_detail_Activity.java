package com.chaptervitamins.quiz;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.utility.ReadResponseUtility;
import com.splunk.mint.Mint;

import java.util.ArrayList;

public class Specific_Quiz_History_detail_Activity extends BaseActivity {
    private LinearLayout allview_ll;
    private ImageView back;
    private LayoutInflater mInflater;
    ArrayList<Data_util> list = new ArrayList<>();
    ArrayList<ReadResponseUtility> list2 = new ArrayList<>();
    String TEST_PATTERN = "";
    private DataBase dataBase;
    private WebServices webServices;
    TextView toolbar_title;

    //    ArrayList<Data_util> correctList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Mint.initAndStartSession(Specific_Quiz_History_detail_Activity.this, Constants.CRASH_REPORT_KEY);
            setContentView(R.layout.activity_specific__quiz__history_detail);
            allview_ll = (LinearLayout) findViewById(R.id.allview_ll);
            toolbar_title = (TextView) findViewById(R.id.toolbar_title);
            toolbar_title.setText(getIntent().getStringExtra("title"));
            back = (ImageView) findViewById(R.id.back);
            dataBase = DataBase.getInstance(Specific_Quiz_History_detail_Activity.this);
            webServices = new WebServices();
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            String resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(), getIntent().getStringExtra("material_id"));
            WebServices.questionUtility = new WebServices().parseQuestionData(resp);
            list = WebServices.questionUtility.getData_utils();
            for (int j = 0; j < SpecificHistoryActivity.list.get(getIntent().getIntExtra("pos", 0)).getmQuestionResponseArraylist().size(); j++) {
                for (int k = 0; k < list.size(); k++) {
                    if (SpecificHistoryActivity.list.get(getIntent().getIntExtra("pos", 0)).getmQuestionResponseArraylist().get(j).getQuestion_bank_id().equalsIgnoreCase(list.get(k).getQuestion_bank_id())) {
                        if (list.get(k).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                            list.get(k).setUser_ans(SpecificHistoryActivity.list.get(getIntent().getIntExtra("pos", 0)).getmQuestionResponseArraylist().get(j).getAnswer_key());
                            if (list.get(k).getUser_ans().get(0).equalsIgnoreCase("1"))
                                list.get(k).setOpt1clicked(1);
                            else if (list.get(k).getUser_ans().get(0).equalsIgnoreCase("2"))
                                list.get(k).setOpt2clicked(1);
                            else if (list.get(k).getUser_ans().get(0).equalsIgnoreCase("3"))
                                list.get(k).setOpt3clicked(1);
                            else if (list.get(k).getUser_ans().get(0).equalsIgnoreCase("4"))
                                list.get(k).setOpt4clicked(1);
                        } else if (list.get(k).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                            for (int l = 0; l < SpecificHistoryActivity.list.get(getIntent().getIntExtra("pos", 0)).getmQuestionResponseArraylist().get(j).getAnswer_key().size(); l++) {
                                list.get(k).setUser_ans(SpecificHistoryActivity.list.get(getIntent().getIntExtra("pos", 0)).getmQuestionResponseArraylist().get(j).getAnswer_key());
                                if (list.get(k).getUser_ans().get(l).equalsIgnoreCase("1"))
                                    list.get(k).setOpt1clicked(1);
                                if (list.get(k).getUser_ans().get(l).equalsIgnoreCase("2"))
                                    list.get(k).setOpt2clicked(1);
                                if (list.get(k).getUser_ans().get(l).equalsIgnoreCase("3"))
                                    list.get(k).setOpt3clicked(1);
                                if (list.get(k).getUser_ans().get(l).equalsIgnoreCase("4"))
                                    list.get(k).setOpt4clicked(1);
                            }
                        }
                    }
                }
            }


//        for (int i = 0; i < com.chap_tempone.home.MultichoiceActivity.mReadResponse.size(); i++) {
//            if (com.chap_tempone.home.MultichoiceActivity.mReadResponse.get(i).getResponse_type().equalsIgnoreCase("QUIZ"))
//                if (com.chap_tempone.home.MultichoiceActivity.mReadResponse.get(i).getMaterial_id().equalsIgnoreCase(getIntent().getStringExtra("material_id")))
//                    list2.add(MultichoiceActivity.mReadResponse.get(i));
//        }
//        if (list2.size()>0) {
//            System.out.println("=====" + list2.get(getIntent().getIntExtra("pos", 0)).getQuiz_response());
//            String resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(), getIntent().getStringExtra("material_id"));
//            WebServices.questionUtility = webServices.parseQuestionData(resp);
//            list = WebServices.questionUtility.getData_utils();
//            for (int i=0;i<list.size();i++){
//                if (list.get(i).getCorrect_option().equalsIgnoreCase("1"))list.get(i).setOpt1clicked(1);
//                else if (list.get(i).getCorrect_option().equalsIgnoreCase("2"))list.get(i).setOpt2clicked(1);
//                else if (list.get(i).getCorrect_option().equalsIgnoreCase("3"))list.get(i).setOpt3clicked(1);
//                else if (list.get(i).getCorrect_option().equalsIgnoreCase("4"))list.get(i).setOpt4clicked(1);
//            }
//        }
            addViews(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void addViews(ArrayList<Data_util> courseUtilities) {
        allview_ll.removeAllViews();
        for (int i = 0; i < courseUtilities.size(); i++) {

            View v = mInflater.inflate(R.layout.specific_quiz_history_row, null);
            MyHolder myHolder = new MyHolder(v);
            show(myHolder, i);
            allview_ll.addView(v);
        }
    }

    private void show(MyHolder myHolder, final int i) {
        myHolder.tvoption1.setText("");
        myHolder.tvoption2.setText("");
        myHolder.tvoption3.setText("");
        myHolder.tvoption4.setText("");
        myHolder.question_no_txt.setText("Q" + (i + 1) + ". ");


        if (list.get(i).getIs_question_image().equalsIgnoreCase("0")) {
            myHolder.tvquestion_description.setVisibility(View.VISIBLE);
            myHolder.tvquestion_description.setText(Html.fromHtml(list.get(i).getQuestion_description()));
            myHolder.ques_img.setVisibility(View.GONE);
        } else {
            myHolder.tvquestion_description.setVisibility(View.GONE);
            myHolder.ques_img.setVisibility(View.VISIBLE);
            byte[] decodedString = Base64.decode(list.get(i).getQuestion_description(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            myHolder.ques_img.setImageBitmap(decodedByte);
        }


        if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
            myHolder.tvoption1.setText("A.  " + Html.fromHtml(list.get(i).getOption1()));
            myHolder.opt_img1.setVisibility(View.GONE);
            myHolder.tvoption1.setVisibility(View.VISIBLE);
        } else {
            myHolder.tvoption1.setVisibility(View.GONE);
            myHolder.opt_img1.setVisibility(View.VISIBLE);
            byte[] decodedString = Base64.decode(list.get(i).getOption1(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            myHolder.opt_img1.setImageBitmap(decodedByte);
        }
        if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
            myHolder.tvoption2.setText("B.  " + Html.fromHtml(list.get(i).getOption2()));
            myHolder.opt_img2.setVisibility(View.GONE);
            myHolder.tvoption2.setVisibility(View.VISIBLE);
        } else {
            myHolder.tvoption2.setVisibility(View.GONE);
            myHolder.opt_img2.setVisibility(View.VISIBLE);
            byte[] decodedString = Base64.decode(list.get(i).getOption2(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            myHolder.opt_img2.setImageBitmap(decodedByte);
        }
        if (!list.get(i).getOption3().equalsIgnoreCase(""))
            if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                myHolder.tvoption3.setText("C.  " + Html.fromHtml(list.get(i).getOption3()));
                myHolder.opt_img3.setVisibility(View.GONE);
                myHolder.tvoption3.setVisibility(View.VISIBLE);
            } else {
                myHolder.tvoption3.setVisibility(View.GONE);
                byte[] decodedString = Base64.decode(list.get(i).getOption3(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                myHolder.opt_img3.setImageBitmap(decodedByte);
                myHolder.opt_img3.setVisibility(View.VISIBLE);
            }
        else {
            myHolder.tvoption3.setVisibility(View.GONE);
            myHolder.opt_img3.setVisibility(View.GONE);
            myHolder.l3.setVisibility(View.GONE);
        }
        if (!list.get(i).getOption4().equalsIgnoreCase(""))
            if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                myHolder.tvoption4.setText("D.  " + Html.fromHtml(list.get(i).getOption4()));
                myHolder.opt_img4.setVisibility(View.GONE);
                myHolder.tvoption4.setVisibility(View.VISIBLE);
            } else {
                myHolder.tvoption4.setVisibility(View.GONE);
                myHolder.opt_img4.setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(list.get(i).getOption4(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                myHolder.opt_img4.setImageBitmap(decodedByte);
            }
        else {
            myHolder.opt_img4.setVisibility(View.GONE);
            myHolder.tvoption4.setVisibility(View.GONE);
            myHolder.l4.setVisibility(View.GONE);
        }
//        String desc = "<font color='#3d9cdc'>Description: </font>";
//            MSG = list.get(i).getExplanation();//(Html.fromHtml());
        String correctAns = list.get(i).getCorrect_option();
        boolean isCorrect = true;
        boolean isSkiped = false;
        if (list.get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
            if (list.get(i).getUser_ans() != null && list.get(i).getUser_ans().size() > 0 && !list.get(i).getUser_ans().get(0).equalsIgnoreCase("") && !list.get(i).getUser_ans().get(0).equalsIgnoreCase("null") && !list.get(i).getUser_ans().get(0).equalsIgnoreCase("0"))
                if (!correctAns.equalsIgnoreCase(list.get(i).getUser_ans().get(0))) {
                    isCorrect = false;
                }
            if (list.get(i).getUser_ans() != null && list.get(i).getUser_ans().size() == 0) {
                myHolder.answer_type.setText("Skiped Question");
                myHolder.answer_type.setTextColor(Color.GRAY);
            } else if (isCorrect) {
                myHolder.answer_type.setText("Correct Answer");
                myHolder.answer_type.setTextColor(Color.GREEN);
            } else {
                myHolder.answer_type.setText("Wrong Answer");
                myHolder.answer_type.setTextColor(Color.RED);
            }
        } else if (list.get(i).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
            ArrayList<String> correctAnswerAl = QuizUtils.convertStringToAl(list.get(i).getCorrect_option());
            for (int l = 0; l < list.get(i).getUser_ans().size(); l++) {
                if (!list.get(i).getUser_ans().get(l).equalsIgnoreCase("") && !list.get(i).getUser_ans().get(l).equalsIgnoreCase("null") && !list.get(i).getUser_ans().get(l).equalsIgnoreCase("0")) {
                    if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                        String correctOptionType = list.get(i).getCorrect_option_type();
                        switch (correctOptionType) {
                            case "ALL":
                                if (QuizUtils.checkAnsForAll(list.get(i).getUser_ans(), correctAnswerAl)) {
                                    isCorrect = true;
                                } else
                                    isCorrect = false;
                                break;
                            case "ANY":
                                if (QuizUtils.checkAnsForAny(list.get(i).getUser_ans(), correctAnswerAl)) {
                                    isCorrect = true;
                                } else
                                    isCorrect = false;
                                break;
                        }
                    }
                }
            }
            if (list.get(i).getUser_ans() != null && list.get(i).getUser_ans().size() == 0) {
                myHolder.answer_type.setText("Skiped Question");
                myHolder.answer_type.setTextColor(Color.GRAY);
            } else if (isCorrect) {
                myHolder.answer_type.setText("Correct Answer");
                myHolder.answer_type.setTextColor(Color.GREEN);
            } else {
                myHolder.answer_type.setText("Wrong Answer");
                myHolder.answer_type.setTextColor(Color.RED);
            }
        }

        if (list.get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
            if (list.get(i).getOpt1clicked() == 1) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption1.setVisibility(View.VISIBLE);
                    myHolder.tvoption1.setTextColor(Color.WHITE);
                    myHolder.tvoption2.setTextColor(Color.BLACK);
                    myHolder.tvoption3.setTextColor(Color.BLACK);
                    myHolder.tvoption4.setTextColor(Color.BLACK);
                } else {
                    myHolder.tvoption1.setVisibility(View.GONE);
                    myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                }


            } else if (list.get(i).getOpt2clicked() == 1) {

                if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption2.setVisibility(View.VISIBLE);
                    myHolder.tvoption1.setTextColor(Color.BLACK);
                    myHolder.tvoption2.setTextColor(Color.WHITE);
                    myHolder.tvoption3.setTextColor(Color.BLACK);
                    myHolder.tvoption4.setTextColor(Color.BLACK);
                } else {
                    myHolder.tvoption2.setVisibility(View.GONE);
                    myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                }

            } else if (list.get(i).getOpt3clicked() == 1) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption3.setVisibility(View.VISIBLE);
                    myHolder.tvoption1.setTextColor(Color.BLACK);
                    myHolder.tvoption2.setTextColor(Color.BLACK);
                    myHolder.tvoption3.setTextColor(Color.WHITE);
                    myHolder.tvoption4.setTextColor(Color.BLACK);
                } else {
                    myHolder.tvoption3.setVisibility(View.GONE);
                    myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                }

            } else if (list.get(i).getOpt4clicked() == 1) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption4.setVisibility(View.VISIBLE);
                    myHolder.tvoption1.setTextColor(Color.BLACK);
                    myHolder.tvoption2.setTextColor(Color.BLACK);
                    myHolder.tvoption3.setTextColor(Color.BLACK);
                    myHolder.tvoption4.setTextColor(Color.WHITE);
                } else {
                    myHolder.tvoption4.setVisibility(View.GONE);
                    myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                    myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                }

            } else {
                isSkiped = true;
            }
        } else if (list.get(i).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
            myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
            myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
            myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
            myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
            myHolder.tvoption1.setTextColor(Color.BLACK);
            myHolder.tvoption2.setTextColor(Color.BLACK);
            myHolder.tvoption3.setTextColor(Color.BLACK);
            myHolder.tvoption4.setTextColor(Color.BLACK);
            if (list.get(i).getOpt1clicked() == 1) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption1.setVisibility(View.VISIBLE);
                    myHolder.tvoption1.setTextColor(Color.WHITE);
                } else {
                    myHolder.tvoption1.setVisibility(View.GONE);
                    myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                }


            }
            if (list.get(i).getOpt2clicked() == 1) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption2.setVisibility(View.VISIBLE);
                    myHolder.tvoption2.setTextColor(Color.WHITE);

                } else {
                    myHolder.tvoption2.setVisibility(View.GONE);
                    myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                }

            }
            if (list.get(i).getOpt3clicked() == 1) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption3.setVisibility(View.VISIBLE);
                    myHolder.tvoption3.setTextColor(Color.WHITE);
                } else {
                    myHolder.tvoption3.setVisibility(View.GONE);
                    myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                }

            }
            if (list.get(i).getOpt4clicked() == 1) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                    myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                    myHolder.tvoption4.setVisibility(View.VISIBLE);
                    myHolder.tvoption4.setTextColor(Color.WHITE);
                } else {
                    myHolder.tvoption4.setVisibility(View.GONE);
                    myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                }

            }
            if (list.get(i).getOpt1clicked() == 0 && list.get(i).getOpt2clicked() == 0 && list.get(i).getOpt3clicked() == 0 && list.get(i).getOpt4clicked() == 0) {
                isSkiped = true;
            }
        }
        if (!isCorrect) {

            if (list.get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                if (list.get(i).getOpt1clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption1.setVisibility(View.VISIBLE);
                        myHolder.tvoption1.setTextColor(Color.WHITE);
                        myHolder.tvoption2.setTextColor(Color.BLACK);
                        myHolder.tvoption3.setTextColor(Color.BLACK);
                        myHolder.tvoption4.setTextColor(Color.BLACK);
                    } else {
                        myHolder.tvoption1.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                    }

                } else if (list.get(i).getOpt2clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption2.setVisibility(View.VISIBLE);
                        myHolder.tvoption1.setTextColor(Color.BLACK);
                        myHolder.tvoption2.setTextColor(Color.WHITE);
                        myHolder.tvoption3.setTextColor(Color.BLACK);
                        myHolder.tvoption4.setTextColor(Color.BLACK);
                    } else {
                        myHolder.tvoption2.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                    }

                } else if (list.get(i).getOpt3clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption3.setVisibility(View.VISIBLE);
                        myHolder.tvoption1.setTextColor(Color.BLACK);
                        myHolder.tvoption2.setTextColor(Color.BLACK);
                        myHolder.tvoption3.setTextColor(Color.WHITE);
                        myHolder.tvoption4.setTextColor(Color.BLACK);
                    } else {
                        myHolder.tvoption3.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                    }

                } else if (list.get(i).getOpt4clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.tvoption4.setVisibility(View.VISIBLE);
                        myHolder.tvoption1.setTextColor(Color.BLACK);
                        myHolder.tvoption2.setTextColor(Color.BLACK);
                        myHolder.tvoption3.setTextColor(Color.BLACK);
                        myHolder.tvoption4.setTextColor(Color.WHITE);
                    } else {
                        myHolder.tvoption4.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_red);
                    }

                }
            } else if (list.get(i).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                myHolder.tvoption1.setTextColor(Color.BLACK);
                myHolder.tvoption2.setTextColor(Color.BLACK);
                myHolder.tvoption3.setTextColor(Color.BLACK);
                myHolder.tvoption4.setTextColor(Color.BLACK);
                myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_white);
                myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_white);
                myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_white);
                myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_white);
                if (list.get(i).getOpt1clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.tvoption1.setVisibility(View.VISIBLE);
                        myHolder.tvoption1.setTextColor(Color.WHITE);
                    } else {
                        myHolder.tvoption1.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_red);
                    }

                } else if (list.get(i).getOpt2clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_red);

                        myHolder.tvoption2.setVisibility(View.VISIBLE);
                        myHolder.tvoption2.setTextColor(Color.WHITE);

                    } else {
                        myHolder.tvoption2.setVisibility(View.GONE);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_red);
                    }

                } else if (list.get(i).getOpt3clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.tvoption3.setVisibility(View.VISIBLE);
                        myHolder.tvoption3.setTextColor(Color.WHITE);
                    } else {
                        myHolder.tvoption3.setVisibility(View.GONE);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_red);
                    }

                } else if (list.get(i).getOpt4clicked() == 1) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_red);
                        myHolder.tvoption4.setVisibility(View.VISIBLE);
                        myHolder.tvoption4.setTextColor(Color.WHITE);
                    } else {
                        myHolder.tvoption4.setVisibility(View.GONE);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_red);
                    }

                }
            }
            if (list.get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                if (correctAns.equalsIgnoreCase("1")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setTextColor(Color.WHITE);
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption1.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                } else if (correctAns.equalsIgnoreCase("2")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption2.setTextColor(Color.WHITE);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption2.setVisibility(View.GONE);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                } else if (correctAns.equalsIgnoreCase("3")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption3.setTextColor(Color.WHITE);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption3.setVisibility(View.GONE);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                } else if (correctAns.equalsIgnoreCase("4")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption4.setTextColor(Color.WHITE);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption4.setVisibility(View.GONE);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
            } else if (list.get(i).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                if (correctAns.equalsIgnoreCase("1")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setTextColor(Color.WHITE);
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption1.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
                if (correctAns.equalsIgnoreCase("2")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption2.setTextColor(Color.WHITE);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption2.setVisibility(View.GONE);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
                if (correctAns.equalsIgnoreCase("3")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption3.setTextColor(Color.WHITE);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption3.setVisibility(View.GONE);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
                if (correctAns.equalsIgnoreCase("4")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption4.setTextColor(Color.WHITE);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption4.setVisibility(View.GONE);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
            }
        }


        if (isSkiped) {
            myHolder.rl1.setBackgroundColor(Color.WHITE);
            myHolder.rl2.setBackgroundColor(Color.WHITE);
            myHolder.rl3.setBackgroundColor(Color.WHITE);
            myHolder.rl4.setBackgroundColor(Color.WHITE);
            if (list.get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                if (correctAns.equalsIgnoreCase("1")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setTextColor(Color.WHITE);
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption1.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                } else if (correctAns.equalsIgnoreCase("2")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption2.setTextColor(Color.WHITE);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption2.setVisibility(View.GONE);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                } else if (correctAns.equalsIgnoreCase("3")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption3.setTextColor(Color.WHITE);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption3.setVisibility(View.GONE);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                } else if (correctAns.equalsIgnoreCase("4")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption4.setTextColor(Color.WHITE);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption4.setVisibility(View.GONE);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }

            } else if (list.get(i).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                if (correctAns.equalsIgnoreCase("1")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption1.setTextColor(Color.WHITE);
                        myHolder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption1.setVisibility(View.GONE);
                        myHolder.rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
                if (correctAns.equalsIgnoreCase("2")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption2.setTextColor(Color.WHITE);
                        myHolder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption2.setVisibility(View.GONE);
                        myHolder.rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
                if (correctAns.equalsIgnoreCase("3")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption3.setTextColor(Color.WHITE);
                        myHolder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption3.setVisibility(View.GONE);
                        myHolder.rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
                if (correctAns.equalsIgnoreCase("4")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                        myHolder.tvoption4.setTextColor(Color.WHITE);
                        myHolder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                    } else {
                        myHolder.tvoption4.setVisibility(View.GONE);
                        myHolder.rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                    }
                }
            }
        }
    }
}


    /*private void show(final MyHolder holder, final int i) {
        holder.tvoption1.setText("");
        holder.tvoption2.setText("");
        holder.tvoption3.setText("");
        holder.tvoption4.setText("");
//            if (TEST_PATTERN.equalsIgnoreCase("COMPREHENSION")) {
//                comp_title_txt.setVisibility(View.VISIBLE);
//                comp_title_txt.setText(Html.fromHtml(list.get(i).getQuestion_hint()));
//            }
        boolean isCorrectAns=false;
        if (list.get(i).getCorrect_option().equalsIgnoreCase(list.get(i).getUser_ans())){
            isCorrectAns=true;
        }
        holder.question_no_txt.setText("Q" + (i + 1) + ". ");
        if (list.get(i).getIs_question_image().equalsIgnoreCase("0")) {
            holder.tvquestion_description.setVisibility(View.VISIBLE);
            holder.tvquestion_description.setText(Html.fromHtml(list.get(i).getQuestion_description()));
            holder.ques_img.setVisibility(View.GONE);
        } else {
            holder.tvquestion_description.setVisibility(View.GONE);
            holder.ques_img.setVisibility(View.VISIBLE);
            byte[] decodedString = Base64.decode(list.get(i).getQuestion_description(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.ques_img.setImageBitmap(decodedByte);
        }


        if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
            holder.tvoption1.setText("A.  " + Html.fromHtml(list.get(i).getOption1()));
            holder.opt_img1.setVisibility(View.GONE);
            holder.tvoption1.setVisibility(View.VISIBLE);
        } else {
            holder.opt_img1.setVisibility(View.VISIBLE);
            holder.tvoption1.setVisibility(View.GONE);
            byte[] decodedString = Base64.decode(list.get(i).getOption1(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.opt_img1.setImageBitmap(decodedByte);
        }
        if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
            holder.tvoption2.setText("B.  " + Html.fromHtml(list.get(i).getOption2()));
            holder.opt_img2.setVisibility(View.GONE);
            holder.tvoption2.setVisibility(View.VISIBLE);
        } else {
            holder.opt_img2.setVisibility(View.VISIBLE);
            holder.tvoption2.setVisibility(View.GONE);
            byte[] decodedString = Base64.decode(list.get(i).getOption2(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.opt_img2.setImageBitmap(decodedByte);
        }
        if (!list.get(i).getOption3().equalsIgnoreCase(""))
            if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                holder.tvoption3.setText("C.  " + Html.fromHtml(list.get(i).getOption3()));
                holder.opt_img3.setVisibility(View.GONE);
                holder.tvoption3.setVisibility(View.VISIBLE);
                holder.l3.setVisibility(View.VISIBLE);
            } else {
                byte[] decodedString = Base64.decode(list.get(i).getOption3(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.opt_img3.setImageBitmap(decodedByte);
                holder.tvoption3.setVisibility(View.GONE);
                holder.opt_img3.setVisibility(View.VISIBLE);
                holder.l3.setVisibility(View.VISIBLE);
            }
        else {
            holder.opt_img3.setVisibility(View.GONE);
            holder.tvoption3.setVisibility(View.GONE);
            holder.l3.setVisibility(View.GONE);
        }
        if (!list.get(i).getOption4().equalsIgnoreCase(""))
            if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                holder.tvoption4.setText("D.  " + Html.fromHtml(list.get(i).getOption4()));
                holder.opt_img4.setVisibility(View.GONE);
                holder.tvoption4.setVisibility(View.VISIBLE);
                holder.l4.setVisibility(View.VISIBLE);
            } else {
                holder.opt_img4.setVisibility(View.VISIBLE);
                holder.tvoption4.setVisibility(View.GONE);
                holder.l4.setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(list.get(i).getOption4(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.opt_img4.setImageBitmap(decodedByte);
            }
        else {
            holder.opt_img4.setVisibility(View.GONE);
            holder.tvoption4.setVisibility(View.GONE);
            holder.l4.setVisibility(View.GONE);
        }

        if (list.get(i).getOpt1clicked() == 1) {
            holder.tvoption1.setTextColor(Color.WHITE);
            holder.tvoption2.setTextColor(Color.BLACK);
            holder.tvoption3.setTextColor(Color.BLACK);
            holder.tvoption4.setTextColor(Color.BLACK);


            if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSION")) {
                if (list.get(i).getCorrect_option().equalsIgnoreCase("1")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0"))
                        holder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                    else
                        holder.img_ll1.setBackgroundResource(R.drawable.dashboard_border_green);
//                        isCorrectAns = "true";
                } else {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0"))
                        holder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_red);
                    else
                        holder.img_ll1.setBackgroundResource(R.drawable.dashboard_border_red);
//                        showAlart("Error", "Incorrect option! Try again");
                }
            } else {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                    holder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_red);
                    holder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                } else {
                    holder.img_ll1.setBackgroundResource(R.drawable.dashboard_border_green);
                    holder.img_ll2.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll3.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll4.setBackgroundResource(R.drawable.dashboard_border_white);
                }

            }

        } else if (list.get(i).getOpt2clicked() == 1) {
            holder.tvoption1.setTextColor(Color.BLACK);
            holder.tvoption2.setTextColor(Color.WHITE);
            holder.tvoption3.setTextColor(Color.BLACK);
            holder.tvoption4.setTextColor(Color.BLACK);
            if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSION")) {
                if (list.get(i).getCorrect_option().equalsIgnoreCase("2")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0"))
                        holder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                    else
                        holder.img_ll2.setBackgroundResource(R.drawable.dashboard_border_green);
//                            isCorrectAns = "true";
                } else {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0"))
                        holder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_red);
                    else
                        holder.img_ll2.setBackgroundResource(R.drawable.dashboard_border_red);
//                            showAlart("Error", "Incorrect option! Try again");
                }
            } else {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                    holder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_red);
                    holder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                } else {
                    holder.img_ll1.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll2.setBackgroundResource(R.drawable.dashboard_border_green);
                    holder.img_ll3.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll4.setBackgroundResource(R.drawable.dashboard_border_white);
                }
            }
        } else if (list.get(i).getOpt3clicked() == 1) {
            holder.tvoption1.setTextColor(Color.BLACK);
            holder.tvoption2.setTextColor(Color.BLACK);
            holder.tvoption3.setTextColor(Color.WHITE);
            holder.tvoption4.setTextColor(Color.BLACK);
            if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSION")) {
                if (list.get(i).getCorrect_option().equalsIgnoreCase("3")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0"))
                        holder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                    else
                        holder.img_ll3.setBackgroundResource(R.drawable.dashboard_border_green);
//                            isCorrectAns = "true";
                } else {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0"))
                        holder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_red);
                    else
                        holder.img_ll3.setBackgroundResource(R.drawable.dashboard_border_red);
//                            showAlart("Error", "Incorrect option! Try again");
                }
            } else {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                    holder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_red);
                    holder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_white);
                } else {
                    holder.img_ll1.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll2.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll3.setBackgroundResource(R.drawable.dashboard_border_green);
                    holder.img_ll4.setBackgroundResource(R.drawable.dashboard_border_white);
                }
            }
        } else if (list.get(i).getOpt4clicked() == 1) {
            holder.tvoption1.setTextColor(Color.BLACK);
            holder.tvoption2.setTextColor(Color.BLACK);
            holder.tvoption3.setTextColor(Color.BLACK);
            holder.tvoption4.setTextColor(Color.WHITE);
            if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSION")) {
                if (list.get(i).getCorrect_option().equalsIgnoreCase("4")) {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0"))
                        holder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                    else
                        holder.img_ll4.setBackgroundResource(R.drawable.dashboard_border_green);
//                            isCorrectAns = "true";
                } else {
                    if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0"))
                        holder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_red);
                    else
                        holder.img_ll4.setBackgroundResource(R.drawable.dashboard_border_red);
//                            showAlart("Error", "Incorrect option! Try again");
                }
            } else {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                    holder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_red);
                } else {
                    holder.img_ll1.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll2.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll3.setBackgroundResource(R.drawable.dashboard_border_white);
                    holder.img_ll4.setBackgroundResource(R.drawable.dashboard_border_green);
                }
            }
        }
        if (!isCorrectAns){
            if (list.get(i).getCorrect_option().equalsIgnoreCase("1")) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option1_image()) || list.get(i).getIs_option1_image().equalsIgnoreCase("0")) {
                    holder.tvoption1.setTextColor(Color.WHITE);
                    holder.tvoption1.setBackgroundResource(R.drawable.dashboard_border_green);
                } else {
                    holder.tvoption1.setVisibility(View.GONE);
//                    rl1.setBackgroundResource(R.drawable.dashboard_border_green);
                }
            } else if (list.get(i).getCorrect_option().equalsIgnoreCase("2")) {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option2_image()) || list.get(i).getIs_option2_image().equalsIgnoreCase("0")) {
                    holder.tvoption2.setTextColor(Color.WHITE);
                    holder.tvoption2.setBackgroundResource(R.drawable.dashboard_border_green);
                } else {
                    holder.tvoption2.setVisibility(View.GONE);
//                    rl2.setBackgroundResource(R.drawable.dashboard_border_green);
                }
            } else if (list.get(i).getCorrect_option().equalsIgnoreCase("3")) {
//                img3.setVisibility(View.VISIBLE);
                if ("null".equalsIgnoreCase(list.get(i).getIs_option3_image()) || list.get(i).getIs_option3_image().equalsIgnoreCase("0")) {
                    holder.tvoption3.setTextColor(Color.WHITE);
                    holder.tvoption3.setBackgroundResource(R.drawable.dashboard_border_green);
                } else {
                    holder.tvoption3.setVisibility(View.GONE);
//                    rl3.setBackgroundResource(R.drawable.dashboard_border_green);
                }
            } else if (list.get(i).getCorrect_option().equalsIgnoreCase("4")) {
//                img4.setVisibility(View.VISIBLE);
                if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0")) {
                    holder.tvoption4.setTextColor(Color.WHITE);
                    holder.tvoption4.setBackgroundResource(R.drawable.dashboard_border_green);
                } else {
                   holder.tvoption4.setVisibility(View.GONE);
//                    rl4.setBackgroundResource(R.drawable.dashboard_border_green);
                }
            }
        }

    }*/


class MyHolder {
    ImageView ques_img, opt_img1, opt_img2, opt_img3, opt_img4, img1, img2, img3, img4;
    TextView tvquestion_description, tvoption1, tvoption2, tvoption3, tvoption4, answer_type;
    TextView l3, l4, question_no_txt;
    RelativeLayout rl1, rl2, rl3, rl4;

    public MyHolder(View v) {
        question_no_txt = (TextView) v.findViewById(R.id.question_no_txt);
        tvquestion_description = (TextView) v.findViewById(R.id.question_description);
        ques_img = (ImageView) v.findViewById(R.id.question_img);
        tvoption1 = (TextView) v.findViewById(R.id.option1);
        tvoption2 = (TextView) v.findViewById(R.id.option2);
        tvoption3 = (TextView) v.findViewById(R.id.option3);
        tvoption4 = (TextView) v.findViewById(R.id.option4);
        l3 = (TextView) v.findViewById(R.id.l3);
        l4 = (TextView) v.findViewById(R.id.l4);
        answer_type = (TextView) v.findViewById(R.id.answer_type);
        opt_img1 = (ImageView) v.findViewById(R.id.option_img1);
        opt_img2 = (ImageView) v.findViewById(R.id.option_img2);
        opt_img3 = (ImageView) v.findViewById(R.id.option_img3);
        opt_img4 = (ImageView) v.findViewById(R.id.option_img4);
        rl1 = (RelativeLayout) v.findViewById(R.id.rl1);
        rl2 = (RelativeLayout) v.findViewById(R.id.rl2);
        rl3 = (RelativeLayout) v.findViewById(R.id.rl3);
        rl4 = (RelativeLayout) v.findViewById(R.id.rl4);
        img1 = (ImageView) v.findViewById(R.id.info_img1);
        img2 = (ImageView) v.findViewById(R.id.info_img2);
        img3 = (ImageView) v.findViewById(R.id.info_img3);
        img4 = (ImageView) v.findViewById(R.id.info_img4);
        img1.setVisibility(View.GONE);
        img2.setVisibility(View.GONE);
        img3.setVisibility(View.GONE);
        img4.setVisibility(View.GONE);
    }
}







