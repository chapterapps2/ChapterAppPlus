package com.chaptervitamins.quiz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseQuizActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.quiz.adapter.VerticalWiseQuizAdapter;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.MeterialUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class QuizSummary_Activity extends BaseQuizActivity implements VerticalWiseQuizAdapter.OnVerticalClickListener, SubmitData.OnSubmitDataListener {
    private ImageView back;
    private GridView gridview;
    private RecyclerView recyclerView;
    private LinearLayout submit_ll;
    public CustomGridAdapter gridAdapter;
    int TotalMarks = 0;
    int UserTotalMarks = 0;
    MixPanelManager mixPanelManager;
    private String completed = "", startTime = "", showAnswer = "", TEST_PATTERN = "", isDone = "";
    private TextView tvHeader;
    private GifTextView goldGif;
    VerticalWiseQuizAdapter verticalWiseQuizAdapter;
    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_summary);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        back = (ImageView) findViewById(R.id.back);
        tvHeader = (TextView) findViewById(R.id.verticalNameTxtView);
        tvTimer = (TextView) findViewById(R.id.time_txt);
        gridview = (GridView) findViewById(R.id.gridview);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        submit_ll = (LinearLayout) findViewById(R.id.submit_ll);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_Utility");
            completed = bundle.getString("time");
            startTime = bundle.getString("start_time");
            showAnswer = bundle.getString("show_answer");
            TEST_PATTERN = bundle.getString("test_pattern");
            isDone = bundle.getString("isdone");
            TIMERVALUE = getIntent().getLongExtra("timer_value", 0);
        }
        setTimer(true, R.color.black);
        try {

            new Thread(new Runnable() {
                @Override
                public void run() {
//                    coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getCourse_id(), meterialUtility.getMaterial_id());
                    if (meterialUtility != null)
                        coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
                   /* if (coinsAllocatedModel == null) {
                        coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getCourse_id());
                    }*/
                    if (coinsAllocatedModel != null) {
                        redeem = coinsAllocatedModel.getRedeem();
                        noOfCoins = coinsAllocatedModel.getMaxCoins();
                    } else
                        coinsAllocatedModel = new CoinsAllocatedModel();
                }
            }).start();
            mixPanelManager = APIUtility.getMixPanelManager(QuizSummary_Activity.this);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openNextScreen();
                }
            });
            if (meterialUtility.getShow_vertical().equalsIgnoreCase("Yes") && WebServices.questionUtility.getVerticalList() != null && WebServices.questionUtility.getVerticalList().size() > 0 && !TextUtils.isEmpty(WebServices.questionUtility.getVerticalList().get(0).getName())) {
                setDataIntoViews();
            } else {
                recyclerView.setVisibility(View.GONE);
                gridview.setVisibility(View.VISIBLE);
                gridAdapter = new CustomGridAdapter(QuizSummary_Activity.this, WebServices.questionUtility.getData_utils());
                gridview.setAdapter(gridAdapter);
                getSkippedAndAnsCount();
                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        TimerVal = false;
                        Intent intent = new Intent(QuizSummary_Activity.this, Quiz_Review_Activity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("meterial_Utility", meterialUtility);
                        bundle.putLong("timer_value", TIMERVALUE);
                        bundle.putInt("pos", position);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, 200);
                    }
                });
            }

            submit_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(QuizSummary_Activity.this)
                            .setTitle("Submit")
                            .setMessage("Are you sure you want to submit your response?")
                            .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (dialogInterface != null)
                                        dialogInterface.dismiss();

                                }
                            }).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (dialogInterface != null)
                                dialogInterface.dismiss();
                            openNextScreen();
                        }
                    }).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataIntoViews() {
        verticalWiseQuizAdapter = new VerticalWiseQuizAdapter(QuizSummary_Activity.this, WebServices.questionUtility.getVerticalList(),
                WebServices.questionUtility.getData_utils());
        recyclerView.setLayoutManager(new LinearLayoutManager(QuizSummary_Activity.this));
        recyclerView.setItemViewCacheSize(WebServices.questionUtility.getVerticalList().size());
        recyclerView.setAdapter(verticalWiseQuizAdapter);
    }

    private void openNextScreen() {
        String endTime = DateFormat.getDateTimeInstance().format(new Date());
        mixPanelManager.selectTimeTrack(QuizSummary_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                , WebServices.questionUtility.getTitle(), "quiz");

        int corr_que = 0, incorr_que = 0, skip_que = 0;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        MainActivity.QUIZEND_TIME = df.format(c.getTime());
            /*for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {
                TotalMarks = TotalMarks + Integer.parseInt(WebServices.questionUtility.getData_utils().get(i).getMarks());
                if (WebServices.questionUtility.getData_utils().get(i).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                    if (WebServices.questionUtility.getData_utils().get(i).getUser_ans() != null && !WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0).equalsIgnoreCase("")) {
                        if (WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0).equalsIgnoreCase(WebServices.questionUtility.getData_utils().get(i).getCorrect_option())) {
                            UserTotalMarks = UserTotalMarks + Integer.parseInt(WebServices.questionUtility.getData_utils().get(i).getMarks());
                            corr_que++;
                        } else {
                            incorr_que++;
                        }
                    } else {
                        skip_que++;
                    }
                } else {
                    if (WebServices.questionUtility.getData_utils().get(i).getUser_ans() != null && !WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0).equalsIgnoreCase("")) {
                        if (WebServices.questionUtility.getData_utils().get(i).getUser_ans().get(0).equalsIgnoreCase(WebServices.questionUtility.getData_utils().get(i).getCorrect_option())) {
                            UserTotalMarks = UserTotalMarks + Integer.parseInt(WebServices.questionUtility.getData_utils().get(i).getMarks());
                            corr_que++;
                        } else {
                            incorr_que++;
                        }
                    } else {
                        skip_que++;
                    }
                }
            }*/

        int size = WebServices.questionUtility.getData_utils().size();
        for (int i = 0; i < size; i++) {
            Data_util dataUtil = WebServices.questionUtility.getData_utils().get(i);
            if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                    ArrayList<String> correctAnswerAl = QuizUtils.convertStringToAl(dataUtil.getCorrect_option());
                    if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                        String correctOptionType = dataUtil.getCorrect_option_type();
                        switch (correctOptionType) {
                            case "ALL":
                                if (QuizUtils.checkAnsForAll(dataUtil.getUser_ans(), correctAnswerAl)) {
                                    dataUtil.setCorrect(true);
//                                        isCorrect = "Correct";
                                    UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                                    corr_que++;
                                } else {
                                    dataUtil.setCorrect(false);
                                    incorr_que++;
                                }
                                break;
                            case "ANY":
                                if (QuizUtils.checkAnsForAny(dataUtil.getUser_ans(), correctAnswerAl)) {
//                                        isCorrect = "Correct";
                                    dataUtil.setCorrect(true);
                                    UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                                    corr_que++;
                                } else {
                                    dataUtil.setCorrect(false);
                                    incorr_que++;
                                }
                                break;
                        }
                    }
                } else {
                    skip_que++;
                }

            } else {
                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                    if (dataUtil.getUser_ans().get(0).equalsIgnoreCase(dataUtil.getCorrect_option())) {
                        UserTotalMarks = UserTotalMarks + Integer.parseInt(dataUtil.getMarks());
                        dataUtil.setCorrect(true);
                        corr_que++;
                    } else {
                        dataUtil.setCorrect(false);
                        incorr_que++;
                    }
                } else {
                    skip_que++;
                }
            }
        }
        if ((corr_que + incorr_que + skip_que) == WebServices.questionUtility.getData_utils().size()) {
            MainActivity.TimerVal = false;
            int percent = 0;
            long TIMERVALUE = Long.parseLong(completed) * 60;
            percent = ((corr_que) * 100) / WebServices.questionUtility.getData_utils().size();
            long completedTIme = ((Long.parseLong(completed) * 60) - TIMERVALUE);
            savequizResponse(percent + "", corr_que + "", incorr_que + "");
            mixPanelManager.completematerial(QuizSummary_Activity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "Quiz|" + percent + "|" + corr_que + "|" + TotalMarks, meterialUtility.getMaterial_id());
        }
    }

    private void savequizResponse(String persentage, String corr, String incorr) {
        TimerVal = false;
        DataBase dataBase = DataBase.getInstance(QuizSummary_Activity.this);
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("question_bank_id", WebServices.questionUtility.getData_utils().get(i).getQuestion_bank_id());
                jsonObject.put("assign_question_id", WebServices.questionUtility.getData_utils().get(i).getAssign_question_id());
                jsonObject.put("answer_key",QuizUtils.getRealCorrectOption(WebServices.questionUtility.getData_utils().get(i)));
                jsonObject.put("correct_option", WebServices.questionUtility.getData_utils().get(i).getRealCorrectOption());
                jsonObject.put("answer", WebServices.questionUtility.getData_utils().get(i).getUser_input());
                jsonObject.put("question_description", WebServices.questionUtility.getData_utils().get(i).getQuestion_description());
                jsonObject.put("answer_type", WebServices.questionUtility.getData_utils().get(i).getOption_type());
                if (WebServices.questionUtility.getData_utils().get(i).isCorrect())
                    jsonObject.put("marks", WebServices.questionUtility.getData_utils().get(i).getMarks());
                else
                    jsonObject.put("marks", "0");
                jsonObject.put("time_taken", WebServices.questionUtility.getData_utils().get(i).getTime_taken());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility, Integer.valueOf(persentage), redeem));
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
            showGoldGif();
            Toast.makeText(this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
        }
        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, WebServices.questionUtility.getData_utils().size() + "", WebServices.questionUtility.getData_utils().size() + "", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, meterialUtility, persentage, corr, incorr, jsonArray.toString(), "0", redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(QuizSummary_Activity.this)) {
//                DataBase.getInstance(QuizSummary_Activity.this).deleteRowGetResponse(WebServices.questionUtility.getMaterial_id());
            dialog = ProgressDialog.show(this, "", "Please wait..");
            new SubmitData(QuizSummary_Activity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, DataBase.getInstance(QuizSummary_Activity.this), QuizSummary_Activity.this).execute(corr, incorr, jsonArray.toString(), persentage);
        } else {
            DialogUtils.showDialog(QuizSummary_Activity.this, "Your data is stored offline. Please get an internet connection and try to sync your data to server.", new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
           /* if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    WebServices.updateTotalCoins(QuizSummary_Activity.this, coinsCollected);
            }*/
        }

    }

    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && data.hasExtra("timer_value")) {
            TIMERVALUE = data.getLongExtra("timer_value", 0);
        }
        setTimer(true, R.color.black);
        if (gridAdapter != null) {
            gridAdapter.notifyDataSetChanged();
            getSkippedAndAnsCount();
        }
        if (verticalWiseQuizAdapter != null)
            verticalWiseQuizAdapter.notifyDataSetChanged();
        if (VerticalWiseQuizAdapter.customGridAdapter != null)
            VerticalWiseQuizAdapter.customGridAdapter.notifyDataSetChanged();

    }

    private void getSkippedAndAnsCount() {
        int skipped = 0,answered = 0;
        for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {
            Data_util dataUtil = WebServices.questionUtility.getData_utils().get(i);
            if(dataUtil.getUser_ans()!=null && dataUtil.getUser_ans().size()>0)
                answered++;
            else
                skipped++;
        }

        tvHeader.setVisibility(View.VISIBLE);
        tvHeader.setText(WebServices.questionUtility.getTitle() + "\n  (Skipped: " + skipped + "\t Attempted: " + answered + ")");

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onVerticalClick(String bank_id, int position) {
        Intent intent = new Intent(this, Quiz_Review_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("meterial_Utility", meterialUtility);
        bundle.putLong("timer_value", TIMERVALUE);
        bundle.putInt("pos", position);
        intent.putExtra("ques_id", bank_id);
        intent.putExtras(bundle);
        // mContext.startActivity(intent);
        TimerVal = false;
        startActivityForResult(intent, 200);
    }

    @Override
    public void onSubmitSuccess() {
        if (dialog != null)
            dialog.dismiss();
        openAfterSuccess();
    }

    private void openAfterSuccess() {
        if (WebServices.questionUtility.getShow_score().equalsIgnoreCase("no")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intentThank = new Intent(QuizSummary_Activity.this, ThankYou_Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("coins", coinsCollected);
                    bundle.putSerializable("meterial_Utility", meterialUtility);
                    bundle.putString("msg", "Thank you for submitting your response. Your result will be published soon.");
                    intentThank.putExtras(bundle);
                    startActivity(intentThank);
                    finish();
                }
            }, 2000);
        } else {
            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {*/
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimerVal = false;
            /*meterialUtility.setMaterialEndTime(df.format(c.getTime()));*/
            if (meterialUtility.getShow_vertical().equalsIgnoreCase("Yes") && WebServices.questionUtility.getVerticalList() != null && WebServices.questionUtility.getVerticalList().size() > 0 && !TextUtils.isEmpty(WebServices.questionUtility.getVerticalList().get(0).getName())) {
                Intent intent = new Intent(QuizSummary_Activity.this, VerticalQuizResultActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("meterial_Utility", meterialUtility);
                bundle.putString("time", completed);
                bundle.putBoolean("show_coins", true);
                bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                bundle.putString("test_pattern", TEST_PATTERN);
                bundle.putString("isdone", "yes");
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(QuizSummary_Activity.this, ResultActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("meterial_Utility", meterialUtility);
                bundle.putString("time", completed);
                bundle.putBoolean("show_coins", true);
                bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                bundle.putString("test_pattern", TEST_PATTERN);
                bundle.putString("isdone", isDone);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }


             /*   }
            }, 2000);*/
        }
    }

    @Override
    public void onError(String error) {
        if (!TextUtils.isEmpty(error)) {
            DialogUtils.showDialog(QuizSummary_Activity.this, error, new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
        } else
            finish();
    }

    public class CustomGridAdapter extends BaseAdapter {

        private Context context;
        ArrayList<Data_util> cardUtilities = new ArrayList<>();
        LayoutInflater inflater;
        String verticalName;

        //Constructor to initialize values
        public CustomGridAdapter(Context context, ArrayList<Data_util> utilities) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.context = context;
            cardUtilities = utilities;
        }

        @Override
        public int getCount() {

            // Number of times getView method call depends upon gridValues.length
            return cardUtilities.size();
        }

        @Override
        public Object getItem(int position) {

            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        // Number of times getView method call depends upon gridValues.length

        public View getView(int position, View convertView, ViewGroup parent) {

            // LayoutInflator to call external grid_item.xml file

            convertView = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.grid_row, null);
                TextView textView = (TextView) convertView
                        .findViewById(R.id.count_txt);
//                LinearLayout headerLinearLayout = (LinearLayout) convertView
//                        .findViewById(R.id.headerLinearLayout);
                TextView verticalNameTxtView = (TextView) convertView
                        .findViewById(R.id.verticalNameTxtView);
                textView.setText((position + 1) + "");

                LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.row_ll);
                if (meterialUtility.getShow_status().equalsIgnoreCase("Yes") && cardUtilities.get(position).getVertical() != null &&
                        !TextUtils.isEmpty(cardUtilities.get(position).getVertical())) {
                    if (position == 0) {
                        verticalName = cardUtilities.get(position).getVertical();
//                        headerLinearLayout.setVisibility(View.VISIBLE);
                        verticalNameTxtView.setText(verticalName);
                    } else if (!verticalName.equalsIgnoreCase(cardUtilities.get(position).getVertical())) {
//                        headerLinearLayout.setVisibility(View.VISIBLE);
                        verticalName = cardUtilities.get(position).getVertical();
//                    } else {
//                        verticalNameTxtView.setText(verticalName);
//                        headerLinearLayout.setVisibility(View.GONE);
                    }
                }
                if (cardUtilities != null && cardUtilities.size() > 0 && cardUtilities.get(position) != null && cardUtilities.get(position).getUser_ans() != null &&
                        cardUtilities.get(position).getUser_ans().size() > 0) {
                    if (cardUtilities.get(position).getUser_ans().get(0).equalsIgnoreCase("")) {
                        textView.setBackgroundResource(R.drawable.skip_bg);
                    } else {
                        textView.setBackgroundResource(R.drawable.rounded_corner_green_button);
                    }
                }

            }

            return convertView;
        }
    }
}
