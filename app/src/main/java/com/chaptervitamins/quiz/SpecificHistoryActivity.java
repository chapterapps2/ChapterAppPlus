package com.chaptervitamins.quiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.home.Adapter;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.Quiz_Response_Util;

import java.util.ArrayList;

/**
 * Created by Android on 6/30/2016.
 */

public class SpecificHistoryActivity extends BaseActivity {
    private ListView listview;
    public static ArrayList<Quiz_Response_Util> list = new ArrayList<>();
    //    public static ArrayList<Data_util> correctList = new ArrayList<>();
    private DataBase dataBase;
    TextView noresult_txt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_specifichistory_activity);
        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        final MeterialUtility meterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        if (meterialUtility != null)
            toolbar_title.setText(meterialUtility.getTitle());
        ImageView back = (ImageView) findViewById(R.id.back);
        noresult_txt = (TextView) findViewById(R.id.noresult_txt);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        listview = (ListView) findViewById(R.id.listview);
        list = new ArrayList<Quiz_Response_Util>();
        dataBase = DataBase.getInstance(SpecificHistoryActivity.this);
//        Adapter adapter = new Adapter(SpecificHistoryActivity.this,list);
//        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes"))
                    if (list.get(position).getmQuestionResponseArraylist() != null && list.get(position).getmQuestionResponseArraylist().size() > 0) {
                        Intent intent = new Intent(SpecificHistoryActivity.this, Specific_Quiz_History_detail_Activity.class);
                        intent.putExtra("pos", position);
                        intent.putExtra("title", meterialUtility.getTitle());
                        intent.putExtra("material_id", meterialUtility.getMaterial_id());
                        startActivity(intent);
                    } else {
                        Toast.makeText(SpecificHistoryActivity.this, "No data found.Please try again later", Toast.LENGTH_SHORT).show();
                    }
                else
                    Toast.makeText(SpecificHistoryActivity.this, getString(R.string.result_not_published), Toast.LENGTH_SHORT).show();

            }
        });
        if (!getIntent().getStringExtra("resp").equalsIgnoreCase("")) {
            final ProgressDialog dialog = ProgressDialog.show(SpecificHistoryActivity.this, "", "Please Wait...");
            dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (dialog != null) dialog.dismiss();
                    if (list.size() == 0) {
                        noresult_txt.setVisibility(View.VISIBLE);
                    } else {
                        noresult_txt.setVisibility(View.GONE);
                        Adapter adapter = new Adapter(SpecificHistoryActivity.this, list);
                        listview.setAdapter(adapter);
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {
//
                    list = new WebServices().parsespecificquiz2(getIntent().getStringExtra("resp"));
                    String resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                    WebServices.questionUtility = new WebServices().parseQuestionData(resp);
                    int high = 0;
                    for (int i = 0; i < list.size(); i++) {
                        if (WebServices.questionUtility.getPassing_percentage() != null)
                            if (!WebServices.questionUtility.getPassing_percentage().equalsIgnoreCase("") && !WebServices.questionUtility.getPassing_percentage().equalsIgnoreCase("null"))
                                if (Integer.parseInt(list.get(i).getResult()) >= Integer.parseInt(WebServices.questionUtility.getPassing_percentage())) {
                                    list.get(i).setIsPass("pass");
                                } else
                                    list.get(i).setIsPass("fail");
                        if (Integer.parseInt(list.get(i).getResult()) >= high) {
                            high = Integer.parseInt(list.get(i).getResult());
                            for (int j = 0; j < list.size(); j++) {
                                list.get(j).setIshighest(false);
                            }
                            list.get(i).setIshighest(true);
                        }
                    }

// Collections.sort(list, new Comparator<ReadResponseUtility>() {
//                        public int compare(ReadResponseUtility o1, ReadResponseUtility o2) {
//                            try {
//                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                Date date = ((DateFormat) dateFormat).parse(o1.getStart_time());
//                                Date date2 = ((DateFormat) dateFormat).parse(o2.getStart_time());
//                                return date2.compareTo(date);
//                            }catch (ParseException e){
//
//                            }
//                            return 0;
//                        }
//                    });

                    handler.sendEmptyMessage(0);
                }
            }.start();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            this.finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(SpecificHistoryActivity.this);
    }
}
