package com.chaptervitamins.quiz;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;

import java.io.File;

/**
 * Created by Android on 9/22/2016.
 */

public class ViewCertificateActivity extends BaseActivity {
    ImageView back;
    PDFView pdfViewer;
    private ImageView mailImgView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.viewcertificate_activity);
        back=(ImageView)findViewById(R.id.back);
        mailImgView=(ImageView)findViewById(R.id.mailImgView);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        pdfViewer=(PDFView)findViewById(R.id.pdfview);
        System.out.println("====getIntent()======filename==="+getIntent().getStringExtra("filename"));

        mailImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
//                emailIntent.setType("text/plain");
                emailIntent.setType("application/pdf");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {""});
               /* if(!getIntent().getStringExtra("matname").isEmpty())*/
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getIntent().getStringExtra("matname"));
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                File root = Environment.getExternalStorageDirectory();
                String pathToMyAttachedFile = "temp/attachement.xml";
                File file = new File(getIntent().getStringExtra("filename"));
                if (!file.exists() || !file.canRead()) {
                    return;
                }
                Uri uri = Uri.fromFile(file);
                emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(emailIntent, "Pick an Email provider"));
            }
        });
        try {
            pdfViewer.fromFile(new File(getIntent().getStringExtra("filename")))
//                .pages(0)
                    .defaultPage(0)
                    .enableSwipe(true)
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
//                .onDraw(onDrawListener)
//                .onLoad(onLoadCompleteListener)
                    .onPageChange(new OnPageChangeListener() {
                        @Override
                        public void onPageChanged(int page, int pageCount) {
//                            pagecount_txt.setText((pdfViewer.getCurrentPage() + 1) + "/" + pdfViewer.getPageCount());
                            if (page > pageCount) {
                                pdfViewer.enableSwipe(false);
                            }
                        }
                    })
                    .onError(new OnErrorListener() {
                        @Override
                        public void onError(Throwable t) {
                            Toast.makeText(ViewCertificateActivity.this,"Error in Opening the Certificate! Please try again.",Toast.LENGTH_LONG).show();
                            finish();
                        }
                    })
                    .load();
        }catch (Exception e){

        }

    }
}
