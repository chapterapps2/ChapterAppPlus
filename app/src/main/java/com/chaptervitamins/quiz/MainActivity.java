package com.chaptervitamins.quiz;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.multiChoice.LeftRVAdapter;
import com.chaptervitamins.multiChoice.OnStartDragListener;
import com.chaptervitamins.multiChoice.RightRVAdapter;
import com.chaptervitamins.multiChoice.SimpleItemTouchHelperCallback;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.quiz.model.Vertical;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static com.chaptervitamins.quiz.QuizUtils.convertStringToAl;

public class MainActivity extends BaseActivity implements OnStartDragListener, View.OnClickListener {
    private ArrayList<Data_util> list = new ArrayList<>();
    int j = 0;
    String resp = "";
    WebServices webServices;
    String startTime, endTime;
    Handler handler;
    Button next_btn, previous_btn;
    ImageView back, timmer, ques_img, opt_img1, opt_img2, opt_img3, opt_img4, ivQuesTimer;
    LinearLayout img_ll1, img_ll2, img_ll3, img_ll4, llMatchColumn, llArrangeOrder;
    private CardView rlOption1, rlOption2, rlOption3, rlOption4;
    TextView comp_title_txt, count_txt, title_txt, time_txt, tvquestion_description, tvoption1, tvoption2, tvoption3, tvoption4, tvVerticalName, tvQuesTimer;
    TextView question_no_txt;
    private ProgressDialog dialog;
    private DataBase dataBase;
    private long TIMERVALUE = 0, totalTime = 0, questStartTime = 0, quesEndTime = 0;
    public static boolean TimerVal = true;
    private Handler handler2 = new Handler();
    private String TEST_PATTERN = "";
    private String isCorrectAns = "";
    MixPanelManager mixPanelManager;
    private ArrayList<String> user_ans_al = new ArrayList<>();
    private ArrayList<Data_util> matchTheColumnAl = new ArrayList<>();
    private Integer matchColumnIndex;
    RightRVAdapter rightRVAdapter;
    LeftRVAdapter leftRVAdapter;
    ItemTouchHelper.Callback callback;

    public static String QUIZEND_TIME = "";
    private boolean checkBackPress;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";
    private MeterialUtility meterialUtility;
    private String resume = "", res_data = "";
    private RadioButton rbOption1, rbOption2, rbOption3, rbOption4;
    private CheckBox cbOption1, cbOption2, cbOption3, cbOption4;
    private RecyclerView rvLeftQuest, rvRightAns, rvArrangeAns;
    private ScrollView svQuestion;
    private ItemTouchHelper mItemTouchHelper;
    ArrayList<Data_util> answerMatchTheColumnAl = new ArrayList<>();
    private LinearLayout llAnswer;
    private boolean isChangeAnswer;
    private String selectVertical;
    private long TIMEREACHQUES = 0, totalCompleteTime = 0;
    private Dialog dialog1;
    private Data_util dataUtil;
    private int selPosition = 0;
    private EditText mEtFeedback;
//    private Data_util dataUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_quiz_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        webServices = new WebServices();
        dataBase = DataBase.getInstance(MainActivity.this);
        tvquestion_description = (TextView) findViewById(R.id.question_description);
        rvLeftQuest = (RecyclerView) findViewById(R.id.rv_left);
        rvRightAns = (RecyclerView) findViewById(R.id.rv_right);
        rvArrangeAns = (RecyclerView) findViewById(R.id.rv_arrange_ans);
        llMatchColumn = (LinearLayout) findViewById(R.id.ll_match_column);
        llArrangeOrder = (LinearLayout) findViewById(R.id.ll_arrange_order);
        svQuestion = (ScrollView) findViewById(R.id.sv_question);
        mEtFeedback = (EditText) findViewById(R.id.et_feedback);
        llAnswer = (LinearLayout) findViewById(R.id.ll_answers);
        tvVerticalName = (TextView) findViewById(R.id.tv_vertical);
        tvoption1 = (TextView) findViewById(R.id.option1);
        title_txt = (TextView) findViewById(R.id.title_txt);
        count_txt = (TextView) findViewById(R.id.count_txt);
        comp_title_txt = (TextView) findViewById(R.id.comp_title_txt);
        time_txt = (TextView) findViewById(R.id.time_txt);
        tvoption2 = (TextView) findViewById(R.id.option2);
        tvQuesTimer = (TextView) findViewById(R.id.tv_ques_time);
        question_no_txt = (TextView) findViewById(R.id.question_no_txt);
        tvoption3 = (TextView) findViewById(R.id.option3);
        tvoption4 = (TextView) findViewById(R.id.option4);
        TimerVal = true;
        previous_btn = (Button) findViewById(R.id.previous_btn);
        next_btn = (Button) findViewById(R.id.next_btn);
        back = (ImageView) findViewById(R.id.back);
        timmer = (ImageView) findViewById(R.id.timmer);
        ivQuesTimer = (ImageView) findViewById(R.id.iv_ques_timer);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_Utility");
            resume = bundle.getString("resume");
            selectVertical = bundle.getString("sel_vertical");
            res_data = bundle.getString("res_data");
            title_txt.setText(meterialUtility.getTitle());
        }
        ques_img = (ImageView) findViewById(R.id.question_img);
        opt_img1 = (ImageView) findViewById(R.id.option_img1);
        opt_img2 = (ImageView) findViewById(R.id.option_img2);
        opt_img3 = (ImageView) findViewById(R.id.option_img3);
        opt_img4 = (ImageView) findViewById(R.id.option_img4);
        img_ll1 = (LinearLayout) findViewById(R.id.img_ll1);
        img_ll2 = (LinearLayout) findViewById(R.id.img_ll2);
        img_ll3 = (LinearLayout) findViewById(R.id.img_ll3);
        img_ll4 = (LinearLayout) findViewById(R.id.img_ll4);
        cbOption1 = (CheckBox) findViewById(R.id.cb_option1);
        cbOption2 = (CheckBox) findViewById(R.id.cb_option2);
        cbOption3 = (CheckBox) findViewById(R.id.cb_option3);
        cbOption4 = (CheckBox) findViewById(R.id.cb_option4);
        rbOption1 = (RadioButton) findViewById(R.id.rb_option1);
        rbOption2 = (RadioButton) findViewById(R.id.rb_option2);
        rbOption3 = (RadioButton) findViewById(R.id.rb_option3);
        rbOption4 = (RadioButton) findViewById(R.id.rb_option4);
        rlOption1 = (CardView) findViewById(R.id.rl_option1);
        rlOption2 = (CardView) findViewById(R.id.rl_option2);
        rlOption3 = (CardView) findViewById(R.id.rl_option3);
        rlOption4 = (CardView) findViewById(R.id.rl_option4);

        cbOption1.setOnClickListener(this);
        cbOption2.setOnClickListener(this);
        cbOption3.setOnClickListener(this);
        cbOption4.setOnClickListener(this);
        rbOption1.setOnClickListener(this);
        rbOption2.setOnClickListener(this);
        rbOption3.setOnClickListener(this);
        rbOption4.setOnClickListener(this);
//        TIMERVALUE = Long.parseLong(meterialUtility.getAlloted_time()) * 60;
        mixPanelManager = APIUtility.getMixPanelManager(MainActivity.this);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (meterialUtility != null)
            meterialUtility.setMaterialStartTime(df.format(c.getTime()));
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getCourse_id(), meterialUtility.getMaterial_id());
                if (coinsAllocatedModel == null) {
                    coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getMaterial_id());
                }
                if (coinsAllocatedModel != null) {
                    redeem = coinsAllocatedModel.getRedeem();
                    noOfCoins = coinsAllocatedModel.getMaxCoins();
                } else
                    coinsAllocatedModel = new CoinsAllocatedModel();
            }
        }).start();*/
        coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Quit")
                        .setMessage("Are you sure you want to Quit?")
                        .setNegativeButton("Yes, I want to quit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("Quiz Submit")
                                        .setMessage("Are you sure you want to submit Quiz without completing it?")
                                        .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (dialog != null && !isFinishing())
                                                    dialog.dismiss();
                                            }
                                        })
                                        .setNegativeButton("Submit", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int iint) {
                                                WebServices.questionUtility.setData_utils(list);
                                                TimerVal = false;
                                                int corr_que = 0, incorr_que = 0, skip_que = 0;
                                                Calendar c = Calendar.getInstance();
                                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                QUIZEND_TIME = df.format(c.getTime());
                                                quesEndTime = totalTime;
                                                list.get(j).setTime_taken(Utils.getTimeDifference(questStartTime, quesEndTime, list.get(j).getTime_taken()));
                                                int size = WebServices.questionUtility.getData_utils().size();
                                                for (int i = 0; i < size; i++) {
                                                    Data_util dataUtil = WebServices.questionUtility.getData_utils().get(i);
                                                    if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                                        if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                                                            ArrayList<String> correctAnswerAl = convertStringToAl(dataUtil.getCorrect_option());
                                                            if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                                                                String correctOptionType = dataUtil.getCorrect_option_type();
                                                                switch (correctOptionType) {
                                                                    case "ALL":
                                                                        if (QuizUtils.checkAnsForAll(dataUtil.getUser_ans(), correctAnswerAl)) {
                                                                            dataUtil.setCorrect(true);
                                                                            corr_que++;
                                                                        } else
                                                                            incorr_que++;
                                                                        break;
                                                                    case "ANY":
                                                                        if (QuizUtils.checkAnsForAny(dataUtil.getUser_ans(), correctAnswerAl)) {
                                                                            dataUtil.setCorrect(true);
                                                                            corr_que++;
                                                                        } else
                                                                            incorr_que++;
                                                                        break;
                                                                }
                                                            }
                                                        } else {
                                                            skip_que++;
                                                        }

                                                    } else {
                                                        if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                                                            if (dataUtil.getUser_ans().get(0).equalsIgnoreCase(dataUtil.getCorrect_option())) {
                                                                dataUtil.setCorrect(true);
                                                                corr_que++;
                                                            } else {
                                                                incorr_que++;
                                                            }
                                                        } else {
                                                            skip_que++;
                                                        }
                                                    }
                                                }

                                                if ((corr_que + incorr_que + skip_que) == WebServices.questionUtility.getData_utils().size()) {
                                                    int percent = 0;
                                                    Calendar c1 = Calendar.getInstance();
                                                    SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                    meterialUtility.setMaterialEndTime(df1.format(c1.getTime()));
                                                    percent = ((corr_que) * 100) / WebServices.questionUtility.getData_utils().size();
                                                    long completed = ((Long.parseLong(meterialUtility.getAlloted_time()) * 60) - TIMERVALUE);
//                                    if (!TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE") && !TEST_PATTERN.equalsIgnoreCase("INCREMENTAL")) {
//                                if (percent!=100){
//
//                                for (int i=1;i<com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().size();i++){
//                                    if (com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().get(i).getMaterial_id().equalsIgnoreCase(getIntent().getStringExtra("meterial_id"))){
//                                        com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().get(i).setComplete_per(percent + "");
//                                        com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().get(i).setResponse_status("PENDING");
//                                        break;
//                                    }
//                                }
//                                saveQuitquizResponse(percent + "", corr_que + "", incorr_que + "", getIntent().getStringExtra("time"));
//                                mixPanelManager.Pausematerial(MultichoiceActivity.this, WebServices.mLoginUtility.getEmail(), getIntent().getStringExtra("coursename"), getIntent().getStringExtra("name"), "Quiz", completed + "", TEST_PATTERN);
//                                }else{
                                                    savequizResponse(percent + "", corr_que + "", incorr_que + "", true);
                                                    mixPanelManager.quitmaterial(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "Quiz", percent + "");
//                                    } else {
//                                        Intent intent = new Intent(MultichoiceActivity.this, ResultActivity.class);
//                                        intent.putExtra("name", getIntent().getStringExtra("name"));
//                                        intent.putExtra("time", completed + "");
//                                        intent.putExtra("meterial_id", getIntent().getStringExtra("meterial_id"));
//                                        intent.putExtra("test_pattern", TEST_PATTERN);
//                                        intent.putExtra("isdone", "no");
//                                        intent.putExtra("show_answer", WebServices.questionUtility.getShow_answer());
//                                        intent.putExtra("show_leaderboard", getIntent().getStringExtra("show_leaderboard"));
//                                        intent.putExtra("pass_per", getIntent().getStringExtra("pass_per"));
//                                        startActivity(intent);
//                                    }
//                                savequizLastResponse();
                                                    endTime = DateFormat.getDateTimeInstance().format(new Date());
                                                    mixPanelManager.selectTimeTrack(MainActivity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                                                            , meterialUtility.getTitle(), "quiz");
                                                    if (dialog != null && !isFinishing())
                                                        dialog.dismiss();
                                                    finish();
                                                } else {
                                                    if (!isFinishing())
                                                        new AlertDialog.Builder(MainActivity.this)
                                                                .setTitle("Error")
                                                                .setMessage("Some error occurred. Can not submit Quiz this time. Please try again.")
                                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int which) {

                                                                        finish();
                                                                    }
                                                                })


                                                                .show();
                                                }
                                                finish();
                                            }
                                        });
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (dialog != null && !isFinishing())
                                    dialog.dismiss();
                            }
                        })

                        .show();

            }
        });

        count_txt.setText("");
        list = new ArrayList<>();
        if (resume.equalsIgnoreCase("no")) {
            resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
        }
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (dialog != null && !isFinishing()) dialog.dismiss();
//                if (!resp.equalsIgnoreCase("")) {
//                    new FatchDatainBG().execute();
//                }
                if (msg.what == 1) {
                    Toast.makeText(MainActivity.this, "Something went wrong!", Toast.LENGTH_LONG).show();
                    finish();
                } else {
//                    TEST_PATTERN = WebServices.questionUtility.getTest_pattern();
                    if(WebServices.questionUtility.getVerticalList()!=null && WebServices.questionUtility.getVerticalList().size()>1
                            && webServices.mLoginUtility.getOrganization_id().equals("1") && webServices.mLoginUtility.getBranch_id().equals("25")
                            && Constants.DATABASENAME.equals("PanasonicDB")){
                        meterialUtility.setShow_vertical("yes");
                        meterialUtility.setVertical_level("Section");
                    }
                    TEST_PATTERN = meterialUtility.getTest_pattern();
                    if(list!=null&&list.size()>0) {
                        if (meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                            TIMEREACHQUES = (Long.parseLong(meterialUtility.getAlloted_time()) * 60) / list.size();
                            TIMERVALUE = TIMEREACHQUES;
                        } else {
                            TIMERVALUE = Long.parseLong(meterialUtility.getAlloted_time()) * 60;
                        }
                    }else{
                        Toast.makeText(MainActivity.this, "no quiz data found..!!", Toast.LENGTH_LONG).show();
                    }

                    totalTime = Long.parseLong(meterialUtility.getAlloted_time()) * 60;
                    if (list.size() != 0) {

                        if (resume.equalsIgnoreCase("no")) {
                            if (WebServices.questionUtility.getQuestion_sequence().equalsIgnoreCase("RANDOM"))
                                Collections.shuffle(list);
                            getVerticalWiseQuizGroup(list);
                            startTime();
                            if (j >= list.size() - 1) {
                                next_btn.setText("Done");
                            }
//                            if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL")) {
//                                next_btn.setVisibility(View.GONE);
//                            }
                            if (!TextUtils.isEmpty(selectVertical)) {
                                show(selPosition);
                                j = selPosition;
                            } else {
                                show(0);
                            }
                        } else {
                            TIMERVALUE = TIMERVALUE - Long.parseLong(WebServices.questionUtility.getTimeTaken());
                            totalTime = totalTime - Long.parseLong(WebServices.questionUtility.getTimeTaken());
                            getVerticalWiseQuizGroup(list);
                            startTime();
                            int selectedpos = 0;
                            if (!WebServices.questionUtility.getQuestionIndex().equalsIgnoreCase(""))
                                selectedpos = Integer.parseInt(WebServices.questionUtility.getQuestionIndex());
                            if (list.size() >= (selectedpos + 1)) {
                                show(selectedpos);
                                previous_btn.setVisibility(View.VISIBLE);
                                j = selectedpos;
                                if (j >= list.size() - 1) {
                                    next_btn.setText("Done");
                                }
                                if (j > 0) {
                                    previous_btn.setVisibility(View.VISIBLE);
                                } else {
                                    previous_btn.setVisibility(View.GONE);
                                }
                            } else {
                                noQuizData();
                            }
                        }

                    } else {
                        timmer.setVisibility(View.GONE);
                        next_btn.setText("");
                        count_txt.setText("");
                        noQuizData();
                    }
                }
            }
        };
        dialog = ProgressDialog.show(MainActivity.this, "", "Please wait..");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        new Thread() {
            public void run() {
                if (resume.equalsIgnoreCase("no")) {
                    if (webServices.isValid(resp)) {
                        WebServices.questionUtility = webServices.parseQuestionData(resp);
                        if(WebServices.questionUtility.getData_utils()==null || WebServices.questionUtility.getData_utils().size()==0) {
                            finish();
                        }
                        list = WebServices.questionUtility.getData_utils();
                        getVerticalPostion();
                        handler.sendEmptyMessage(0);
                    } else {
                        handler.sendEmptyMessage(1);
                    }
                } else {
                    WebServices.questionUtility = webServices.parseResumeQuestionData(dataBase, res_data);
                    list = WebServices.questionUtility.getData_utils();
                    handler.sendEmptyMessage(0);
                }

            }

            private void getVerticalPostion() {
                int listSize = list.size();
                for (int i = 0; i < listSize; i++) {
                    if (!TextUtils.isEmpty(selectVertical) && selectVertical.equalsIgnoreCase(list.get(i).getVertical())) {
                        selPosition = i;
                        break;
                    }
                }
            }
        }.start();

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextButtonTask(true);
            }

        });
        previous_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quesEndTime = totalTime;
                user_ans_al.clear();
                getMatchTheColumnPosition(j, true);
                list.get(j).setTime_taken(Utils.getTimeDifference(questStartTime, quesEndTime, list.get(j).getTime_taken()));
                if (matchColumnIndex != null && matchTheColumnAl != null && matchTheColumnAl.size() > 0) {
                    list.removeAll(matchTheColumnAl);
                    list.addAll(matchColumnIndex, matchTheColumnAl);
                    matchTheColumnAl.clear();
                    j--;
                }
                if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {

                    j--;
                    if (list.size() > 0) {
                        next_btn.setText("Next >");
                        next_btn.setVisibility(View.VISIBLE);
                        time_txt.setVisibility(View.VISIBLE);
                        timmer.setVisibility(View.VISIBLE);
                        clear();
                        show(j);
                    }
                    if (j == 0)
                        previous_btn.setVisibility(View.GONE);
                } else {
                    j--;
                    if (list.size() > 0) {
                        next_btn.setText("Next >");
                        next_btn.setVisibility(View.VISIBLE);
                        time_txt.setVisibility(View.VISIBLE);
                        timmer.setVisibility(View.VISIBLE);
                        clear();
                        show(j);
                    }
                    if (j == 0)
                        previous_btn.setVisibility(View.GONE);

                }
            }
        });
    }

    private void getVerticalWiseQuizGroup(ArrayList<Data_util> lists) {
        if (meterialUtility.getShow_vertical().equalsIgnoreCase("Yes")&&WebServices.questionUtility.getVerticalList() != null && WebServices.questionUtility.getVerticalList().size() > 0 && !TextUtils.isEmpty(WebServices.questionUtility.getVerticalList().get(0).getName())) {
            ArrayList<Data_util> dataUtilArrayList = new ArrayList<>();
            for (Data_util data_util : lists) {
            for (Vertical vertical : WebServices.questionUtility.getVerticalList()) {
                    if (vertical.getName().equalsIgnoreCase(data_util.getVertical())) {
                        dataUtilArrayList.add(data_util);
                    }
                }
            }
            list.clear();
            WebServices.questionUtility.getData_utils().clear();
            list = dataUtilArrayList;
            WebServices.questionUtility.setData_utils(dataUtilArrayList);
        }

    }


    private void nextButtonTask(boolean isButtonClick) {
        quesEndTime = totalTime;
        String isCorrect = "InCorrect";
        if (matchColumnIndex != null && matchTheColumnAl != null && matchTheColumnAl.size() > 0) {
            for (int mtc = 0; mtc < matchTheColumnAl.size(); mtc++) {
                Data_util dU = answerMatchTheColumnAl.get(mtc);
                Data_util dU1 = answerMatchTheColumnAl.get(0);
                answerMatchTheColumnAl.get(0).setTime_taken(Utils.getTimeDifference(questStartTime, quesEndTime, dU1.getTime_taken()));

                if (dU.getUser_ans().size() > 0 && dU.getUser_ans().get(0).equals(dU.getCorrect_option())) {
                    dU.setCorrect(true);
                    isCorrect = "Correct";
                } else if (dU.getUser_ans() != null && dU.getUser_ans().size() > 0) {
                    isCorrect = "Incorrect";
                    dU.setCorrect(false);
                } else
                    isCorrect = "Skipped";
//                        if (isChangeAnswer)
//                            isCorrect = "Changed Answer";
                if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                    mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), dU.getCorrect_option(), QuizUtils.getAnswerAccToOption(dU), isCorrect, dU.getQuestion_description(), isChangeAnswer);
                else if (!isCorrect.equalsIgnoreCase("Skipped"))
                    mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), dU.getCorrect_option(), QuizUtils.getAnswerAccToOption(dU), isCorrect, dU.getQuestion_description(), isChangeAnswer);
            }

            list.removeAll(matchTheColumnAl);
            list.addAll(matchColumnIndex, matchTheColumnAl);
            matchTheColumnAl.clear();
        } else
            list.get(j).setTime_taken(Utils.getTimeDifference(questStartTime, quesEndTime, list.get(j).getTime_taken()));
        matchColumnIndex = null;
        user_ans_al.clear();
        if (list.get(j).getQuestion_type().equalsIgnoreCase("COMPREHENSIVE")) {
            if (!TextUtils.isEmpty(mEtFeedback.getText())) {
                list.get(j).setUser_input(mEtFeedback.getText().toString().trim());
                user_ans_al.add(0, "1");
                dataUtil.setOption_type("1");
                list.get(j).setUser_ans(user_ans_al);
            }
            mEtFeedback.getText().clear();
        }
        user_ans_al.clear();
        WebServices.questionUtility.setData_utils(list);

        int corr_que = 0, incorr_que = 0, skip_que = 0, UserTotalMarks = 0;

        int size = WebServices.questionUtility.getData_utils().size();
        for (int i = 0; i < size; i++) {
            Data_util dataUtil = WebServices.questionUtility.getData_utils().get(i);
            if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                    ArrayList<String> correctAnswerAl = convertStringToAl(dataUtil.getCorrect_option());
                    if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                        String correctOptionType = dataUtil.getCorrect_option_type();
                        switch (correctOptionType) {
                            case "ALL":
                                if (QuizUtils.checkAnsForAll(dataUtil.getUser_ans(), correctAnswerAl)) {
                                    dataUtil.setCorrect(true);
                                    isCorrect = "Correct";
                                    corr_que++;
                                } else {
                                    dataUtil.setCorrect(false);
                                    incorr_que++;
                                }
                                break;
                            case "ANY":
                                if (QuizUtils.checkAnsForAny(dataUtil.getUser_ans(), correctAnswerAl)) {
                                    dataUtil.setCorrect(true);
                                    isCorrect = "Correct";
                                    corr_que++;
                                } else {
                                    dataUtil.setCorrect(false);
                                    incorr_que++;
                                }
                                break;
                        }
                    }
                } else {
                    skip_que++;
                }

            } else {
                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                    if (dataUtil.getUser_ans().get(0).equalsIgnoreCase(dataUtil.getCorrect_option())) {
                        dataUtil.setCorrect(true);
                        corr_que++;
                    } else {
                        dataUtil.setCorrect(false);
                        incorr_que++;
                    }
                } else {
                    skip_que++;
                }
            }
        }
        int percent = 0;


        percent = ((corr_que) * 100) / WebServices.questionUtility.getData_utils().size();

        if (list.get(j).getUser_ans() != null && list.get(j).getUser_ans().size() > 0) {
            if (list.get(j).getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                String correctOptionType = list.get(j).getCorrect_option_type();
                switch (correctOptionType) {
                    case "ALL":
                        if (QuizUtils.checkAnsForAll(list.get(j).getUser_ans(), convertStringToAl(list.get(j).getCorrect_option()))) {
                            UserTotalMarks = UserTotalMarks + Integer.parseInt(list.get(j).getMarks());
                            list.get(j).setCorrect(true);
                            isCorrect = "Correct";
                        } else {
                            list.get(j).setCorrect(true);
                            isCorrect = "Incorrect";
                        }
                        break;
                    case "ANY":
                        if (QuizUtils.checkAnsForAny(list.get(j).getUser_ans(), convertStringToAl(list.get(j).getCorrect_option()))) {
                            UserTotalMarks = UserTotalMarks + Integer.parseInt(list.get(j).getMarks());
                            isCorrect = "Correct";
                            list.get(j).setCorrect(true);
                        } else {
                            isCorrect = "Incorrect";
                            list.get(j).setCorrect(false);
                        }
                        break;
                }
                if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                    mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                else if (!isCorrect.equalsIgnoreCase("Skipped"))
                    mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);

            } else if (list.get(j).getQuestion_type().equalsIgnoreCase("SINGLE")) {
                if (list.get(j).getUser_ans().get(0).equals(list.get(j).getCorrect_option())) {
                    isCorrect = "Correct";
                    list.get(j).setCorrect(true);
                    UserTotalMarks = UserTotalMarks + Integer.parseInt(list.get(j).getMarks());
                    if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                        mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                    else if (!isCorrect.equalsIgnoreCase("Skipped"))
                        mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);

                } else {
                    list.get(j).setCorrect(false);
                    isCorrect = "Incorrect";
                    if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                        mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
                    else if (!isCorrect.equalsIgnoreCase("Skipped"))
                        mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);

                }
            }
        } else {
            isCorrect = "Skipped";
            if (isChangeAnswer && !isCorrect.equalsIgnoreCase("Skipped"))
                mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);
            else if (!isCorrect.equalsIgnoreCase("Skipped"))
                mixPanelManager.eachQues_responce(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), list.get(j).getCorrect_option(), QuizUtils.getAnswerAccToOption(list.get(j)), isCorrect, list.get(j).getQuestion_description(), isChangeAnswer);

        }
        saveEachquizResponse(percent + "", corr_que + "", incorr_que + "");

        if (next_btn.getText().toString().equalsIgnoreCase("Done")) {
            TimerVal = false;
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            QUIZEND_TIME = df.format(c.getTime());
//                    if (!TEST_PATTERN.equalsIgnoreCase("INCREMENTAL")!TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
           /* if (TEST_PATTERN.equalsIgnoreCase("RANDOM") && meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                totalCompleteTime = totalCompleteTime + (TIMEREACHQUES - TIMERVALUE);
                TIMERVALUE = TIMEREACHQUES;
                savequizResponse(percent + "", corr_que + "", incorr_que + "", true);
                mixPanelManager.completematerial(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "Quiz|" + percent + "|" + corr_que + "|" + UserTotalMarks, meterialUtility.getMaterial_id());
//
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("meterial_Utility", meterialUtility);
                bundle.putString("time", totalCompleteTime + "");
                bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                bundle.putString("test_pattern", TEST_PATTERN);
                bundle.putString("isdone", "yes");
                intent.putExtras(bundle);
                startActivity(intent);
                finish();

            } else {*/
            if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL")&& list.get(j).getUser_ans().size() == 0) {
              showAlart("Required", "Please select an option");
            }else if ((corr_que + incorr_que + skip_que) == WebServices.questionUtility.getData_utils().size() && WebServices.questionUtility.getData_utils().size() != 0) {
                long completed = ((Long.parseLong(meterialUtility.getAlloted_time()) * 60) - TIMERVALUE);
                WebServices.questionUtility.setData_utils(list);
                Intent intent = new Intent(MainActivity.this, QuizSummary_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("meterial_Utility", meterialUtility);
                bundle.putString("time", completed + "");
                bundle.putLong("timer_value",totalTime);
                bundle.putString("start_time", startTime);
                bundle.putString("show_answer", WebServices.questionUtility.getShow_answer());
                bundle.putString("test_pattern", TEST_PATTERN);
                bundle.putString("isdone", "yes");
                intent.putExtras(bundle);

                startActivity(intent);

                finish();
            } else {
                if (!isFinishing())
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Error")
                            .setMessage("Some error occurred. Can not submit Quiz this time. Please try again.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dialog != null && !isFinishing()) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                }
                            })


                            .show();
            }
//            }
        } else {
            if (isButtonClick) {
                if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                    if (!isCorrectAns.equalsIgnoreCase("")) {
                        if (isCorrectAns.equalsIgnoreCase("true")) {
                            isCorrectAns = "";

                            if (meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                                previous_btn.setVisibility(View.GONE);
                                totalCompleteTime = totalCompleteTime + (TIMEREACHQUES - TIMERVALUE);
                                TIMERVALUE = TIMEREACHQUES;
                            } else {
                                if (list.size() > 0) {
                                    previous_btn.setVisibility(View.VISIBLE);
                                }
                            }
                            if (list.size() > j) {

                                j++;
                                clear();
                                show(j);
                            }
                            if (j >= list.size() - 1) {
                                next_btn.setText("Done");
                            }
                        } else {
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    } else {
                        showAlart("Required", "Please select an option");
                    }
                } else if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL")) {
                    if (list.get(j).getUser_ans().size() == 0)
                        showAlart("Required", "Please select an option");
                    else {

                        if (meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                            previous_btn.setVisibility(View.GONE);
                            totalCompleteTime = totalCompleteTime + (TIMEREACHQUES - TIMERVALUE);
                            TIMERVALUE = TIMEREACHQUES;
                        } else {
                            if (list.size() > 0) {
                                previous_btn.setVisibility(View.VISIBLE);
                            }
                        }
                        if (list.size() > j) {

                            j++;
                            clear();
                            show(j);
                        }
                        if (j >= list.size() - 1) {
                            next_btn.setText("Done");
                        }
                    }

                } else {
                    if (meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                        previous_btn.setVisibility(View.GONE);
                        totalCompleteTime = totalCompleteTime + (TIMEREACHQUES - TIMERVALUE);
                        TIMERVALUE = TIMEREACHQUES;
                    } else {
                        if (list.size() > 0) {
                            previous_btn.setVisibility(View.VISIBLE);
                        }
                    }
                    if (list.size() > j) {

                        j++;
                        clear();
                        show(j);
                    }
                    if (j >= list.size() - 1) {
                        next_btn.setText("Done");
                    }
                }
            } else {
                if (meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                    previous_btn.setVisibility(View.GONE);
                    totalCompleteTime = totalCompleteTime + (TIMEREACHQUES - TIMERVALUE);
                    TIMERVALUE = TIMEREACHQUES;

                    if (list.size() > j) {

                        j++;
                        clear();
                        show(j);
                    }
                    if (j >= list.size() - 1) {
                        next_btn.setText("Done");
                    }
                } else {

                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        if (!isCorrectAns.equalsIgnoreCase("")) {
                            if (isCorrectAns.equalsIgnoreCase("true")) {
                                isCorrectAns = "";
                                if (list.size() > 0) {
                                    previous_btn.setVisibility(View.VISIBLE);
                                }

                                if (list.size() > j) {

                                    j++;
                                    clear();
                                    show(j);
                                }
                                if (j >= list.size() - 1) {
                                    next_btn.setText("Done");
                                }
                            } else {
                                showAlart("Error", "Incorrect option! Try again");
                            }
                        } else {
                            showAlart("Required", "Please select an option");
                        }
                    } else if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL")) {
                        if (list.get(j).getUser_ans().size() == 0)
                            showAlart("Required", "Please select an option");
                        else {
                            if (list.size() > 0) {
                                previous_btn.setVisibility(View.VISIBLE);
                            }
                            if (list.size() > j) {

                                j++;
                                clear();
                                show(j);
                            }
                            if (j >= list.size() - 1) {
                                next_btn.setText("Done");
                            }
                        }

                    } else {
                        if (meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                            previous_btn.setVisibility(View.GONE);
                            totalCompleteTime = totalCompleteTime + (TIMEREACHQUES - TIMERVALUE);
                            TIMERVALUE = TIMEREACHQUES;
                        } else {
                            if (list.size() > 0) {
                                previous_btn.setVisibility(View.VISIBLE);
                            }
                        }
                        if (list.size() > j) {

                            j++;
                            clear();
                            show(j);
                        }
                        if (j >= list.size() - 1) {
                            next_btn.setText("Done");
                        }
                    }
                }
            }
        }
        user_ans_al.clear();
    }

    private void getMatchTheColumnPosition(int position, boolean isPrevious) {
        if (isPrevious) {
            Data_util currentDataUtil = list.get(position - 1);
            String groupId = currentDataUtil.getGroup_id();
            if (currentDataUtil != null && currentDataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE_ANIMATED")) {
                for (int i = position - 1; i >= 0; i--) {
                    Data_util dataUtil = list.get(i);
                    if (dataUtil != null) {
                        if (groupId.equalsIgnoreCase(dataUtil.getGroup_id())) {
                            j = i;
                            j++;
                        } else
                            break;
                    }
                }
            }
            return;
        }
    }


    private void showAlart(String title, String msg) {
        if (!isFinishing())
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(title)
                    .setMessage(msg)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            isCorrectAns = "false";
                            if (dialog != null && !isFinishing())
                                dialog.dismiss();
                        }
                    })
                    .show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!isFinishing())
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("")
                    .setMessage("You have to submit Quiz before you can exit this app.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            checkBackPress = true;
                        }


                    })
                    .show();
        return checkBackPress;

    }

    @Override
    public void onBackPressed() {
//        TimerVal = false;
//        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(MainActivity.this);
    }

    private void noQuizData() {
        if (!isFinishing())
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("No Quiz")
                    .setMessage("Quiz has no questions to attempt")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            finish();
                        }
                    })


                    .show();
    }

    private void clear() {
        tvoption1.setTextColor(Color.BLACK);
        tvoption2.setTextColor(Color.BLACK);
        tvoption3.setTextColor(Color.BLACK);
        tvoption4.setTextColor(Color.BLACK);
        rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
        rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
        rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
        rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
        cbOption1.setChecked(false);
        cbOption2.setChecked(false);
        cbOption3.setChecked(false);
        cbOption4.setChecked(false);
        rbOption1.setChecked(false);
        rbOption2.setChecked(false);
        rbOption3.setChecked(false);
        rbOption4.setChecked(false);

        cbOption1.setVisibility(View.GONE);
        cbOption2.setVisibility(View.GONE);
        cbOption3.setVisibility(View.GONE);
        cbOption4.setVisibility(View.GONE);
        rbOption1.setVisibility(View.GONE);
        rbOption2.setVisibility(View.GONE);
        rbOption3.setVisibility(View.GONE);
        rbOption4.setVisibility(View.GONE);
    }

    private void show(final int i) {
        try {
            if (dialog1 != null && dialog1.isShowing())
                dialog1.dismiss();
            questStartTime = totalTime;
            dataUtil = list.get(i);

                if (meterialUtility.getShow_vertical().equalsIgnoreCase("Yes")&&!TextUtils.isEmpty(dataUtil.getVertical()) && !dataUtil.getVertical().equalsIgnoreCase("null")) {
                    tvVerticalName.setVisibility(View.VISIBLE);
                    tvVerticalName.setText(meterialUtility.getVertical_level()+" : " + dataUtil.getVertical());
                } else
                    tvVerticalName.setVisibility(View.GONE);

            if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0)
                isChangeAnswer = true;
            if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE_ANIMATED")) {
                setMatchTheColumnData(i);
            } else if (dataUtil.getQuestion_type().equalsIgnoreCase("ARRANGEdd_ANIMATED")) {
                setArrangeTheColumnData(dataUtil, i);
            } else if (dataUtil.getQuestion_type().equalsIgnoreCase("COMPREHENSIVE")) {

                setQuestionData(i);

                mEtFeedback.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(dataUtil.getUser_input()))
                    mEtFeedback.setText(dataUtil.getUser_input());
                llAnswer.setVisibility(View.GONE);
                llMatchColumn.setVisibility(View.GONE);
            } else {
                mEtFeedback.setVisibility(View.GONE);
                llAnswer.setVisibility(View.VISIBLE);
//            question_no_txt.setVisibility(View.VISIBLE);
//            llAnswer.setVisibility(View.VISIBLE);
                llMatchColumn.setVisibility(View.GONE);
                tvoption1.setText("");
                tvoption2.setText("");
                tvoption3.setText("");
                tvoption4.setText("");
                setQuestionData(i);

                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                    next_btn.setVisibility(View.GONE);
                }
                if (!dataUtil.getOption1().equalsIgnoreCase(""))
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0")) {
                tvoption1.setText("A.  " + Html.fromHtml(dataUtil.getOption1()));
//                        tvoption1.setText(Html.fromHtml(dataUtil.getOption1()));
                        opt_img1.setVisibility(View.GONE);
                        tvoption1.setVisibility(View.VISIBLE);
                        setQuestionRbCbVisibility(dataUtil, cbOption1, rbOption1, false, false, true);

                    } else {
                        opt_img1.setVisibility(View.VISIBLE);
                        tvoption1.setVisibility(View.GONE);
                     /*   byte[] decodedString = Base64.decode(dataUtil.getOption1(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        opt_img1.setImageBitmap(decodedByte);*/
                        if (!TextUtils.isEmpty(dataUtil.getOption1_url())) {
                            Picasso.with(this).load(dataUtil.getOption1_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img1);
                        }
                        setQuestionRbCbVisibility(dataUtil, cbOption1, rbOption1, false, false, true);
                    }
                if (!dataUtil.getOption2().equalsIgnoreCase("")) {
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0")) {
                tvoption2.setText("B.  " + Html.fromHtml(dataUtil.getOption2()));
//                        tvoption2.setText(Html.fromHtml(dataUtil.getOption2()));
                        opt_img2.setVisibility(View.GONE);
                        tvoption2.setVisibility(View.VISIBLE);
                        setQuestionRbCbVisibility(dataUtil, cbOption2, rbOption2, false, false, true);
                    } else {
                        opt_img2.setVisibility(View.VISIBLE);
                        tvoption2.setVisibility(View.GONE);
                       /* byte[] decodedString = Base64.decode(dataUtil.getOption2(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        opt_img2.setImageBitmap(decodedByte);*/
                        if (!TextUtils.isEmpty(dataUtil.getOption2_url())) {
                            Picasso.with(this).load(dataUtil.getOption2_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img2);
                        }
                        setQuestionRbCbVisibility(dataUtil, cbOption2, rbOption2, false, false, true);

                    }
                } else {
                    opt_img2.setVisibility(View.GONE);
                    tvoption2.setVisibility(View.GONE);
//                setQuestionRbCbVisibility(dataUtil, cbOption3, rbOption3, false, false, false);
                }
                if (!dataUtil.getOption3().equalsIgnoreCase(""))
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0")) {
                    tvoption3.setText("C.  " + Html.fromHtml(dataUtil.getOption3()));
//                        tvoption3.setText(Html.fromHtml(dataUtil.getOption3()));
                        opt_img3.setVisibility(View.GONE);
                        tvoption3.setVisibility(View.VISIBLE);
                        setQuestionRbCbVisibility(dataUtil, cbOption3, rbOption3, false, false, true);

                    } else {
                       /* byte[] decodedString = Base64.decode(dataUtil.getOption3(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        opt_img3.setImageBitmap(decodedByte);*/

                        if (!TextUtils.isEmpty(dataUtil.getOption3_url())) {
                            Picasso.with(this).load(dataUtil.getOption3_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img3);
                        }
                        tvoption3.setVisibility(View.GONE);
                        opt_img3.setVisibility(View.VISIBLE);
                        setQuestionRbCbVisibility(dataUtil, cbOption3, rbOption3, false, false, true);
                    }
                else {
                    opt_img3.setVisibility(View.GONE);
                    tvoption3.setVisibility(View.GONE);
                    setQuestionRbCbVisibility(dataUtil, cbOption3, rbOption3, false, false, false);
                }
                if (!dataUtil.getOption4().equalsIgnoreCase(""))
                    if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0")) {
                    tvoption4.setText("D.  " + Html.fromHtml(dataUtil.getOption4()));
//                        tvoption4.setText(Html.fromHtml(dataUtil.getOption4()));
                        opt_img4.setVisibility(View.GONE);
                        tvoption4.setVisibility(View.VISIBLE);
                        setQuestionRbCbVisibility(dataUtil, cbOption4, rbOption4, false, false, true);
                    } else {
                        opt_img4.setVisibility(View.VISIBLE);
                        tvoption4.setVisibility(View.GONE);
                       /* byte[] decodedString = Base64.decode(dataUtil.getOption4(), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        opt_img4.setImageBitmap(decodedByte);*/
                        if (!TextUtils.isEmpty(dataUtil.getOption4_url())) {
                            Picasso.with(this).load(dataUtil.getOption4_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(opt_img4);
                        }
                        setQuestionRbCbVisibility(dataUtil, cbOption4, rbOption4, false, false, true);
                    }
                else {
                    opt_img4.setVisibility(View.GONE);
                    tvoption4.setVisibility(View.GONE);
                    setQuestionRbCbVisibility(dataUtil, cbOption4, rbOption4, false, false, false);
                }


                if (dataUtil.getOpt1clicked() == 1) {
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        tvoption1.setTextColor(Color.WHITE);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        rbOption1.setVisibility(View.VISIBLE);
                        rbOption1.setChecked(true);


                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        tvoption1.setTextColor(Color.WHITE);
                        cbOption1.setVisibility(View.VISIBLE);
                        cbOption1.setChecked(true);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("1")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            else
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            else
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0")) {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                            } else {
                                rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            }
                        } else {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                            } else
                                rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        }

                    }

                }


                if (dataUtil.getOpt2clicked() == 1) {
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.WHITE);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        rbOption2.setVisibility(View.VISIBLE);
                        rbOption2.setChecked(true);
                    } else {
                        tvoption2.setTextColor(Color.WHITE);
                        cbOption2.setVisibility(View.VISIBLE);
                        cbOption2.setChecked(true);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("2")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            else
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            else
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0")) {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                            } else
                                rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                            } else
                                rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        }
                    }

                }
                if (dataUtil.getOpt3clicked() == 1) {
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.WHITE);
                        tvoption4.setTextColor(Color.BLACK);
                        rbOption3.setVisibility(View.VISIBLE);
                        rbOption3.setChecked(true);
                    } else {
                        tvoption3.setTextColor(Color.WHITE);
                        cbOption3.setVisibility(View.VISIBLE);
                        cbOption3.setChecked(true);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("3")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            else
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            else
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0")) {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                            } else
                                rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                            } else
                                rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        }
                    }
                }
                if (dataUtil.getOpt4clicked() == 1) {
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.WHITE);
                        rbOption4.setVisibility(View.VISIBLE);
                        rbOption4.setChecked(true);
                    } else {
                        tvoption4.setTextColor(Color.WHITE);
                        cbOption4.setVisibility(View.VISIBLE);
                        cbOption4.setChecked(true);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("4")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            else
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            else
                                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    } else {
                        if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0")) {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            } else
                                rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        } else {
                            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                                ;
                                rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                            } else
                                rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
                        }
                    }
                }
                tvoption1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option1ClickTask(dataUtil);
                    }
                });
                tvoption2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option2ClickTask(dataUtil);
                    }
                });
                tvoption3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option3ClickTask(dataUtil);

                    }
                });
                tvoption4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option4ClickTask(dataUtil);

                    }
                });
                opt_img1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option1ClickTask(dataUtil);
                    /*showInputDialog(dataUtil, 1, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        user_ans_al.add(0, "1");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(true);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.WHITE);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(1);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
                        img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt1clicked() == 0) {
                            user_ans_al.add("1");
                            tvoption1.setTextColor(Color.WHITE);
                            dataUtil.setOpt1clicked(1);
                            cbOption1.setChecked(true);
                            img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
                        } else {
                            user_ans_al.remove("1");
                            tvoption1.setTextColor(Color.BLACK);
                            dataUtil.setOpt1clicked(0);
                            cbOption1.setChecked(false);
                            tvoption1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("1")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                tvoption1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            else
                                img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                                tvoption1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            else
                                img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }*/
                    }
                });
                opt_img2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option2ClickTask(dataUtil);
                    /*showInputDialog(dataUtil, 2, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        user_ans_al.add(0, "2");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(true);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.WHITE);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(1);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
                        img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt2clicked() == 0) {
                            user_ans_al.add("2");
                            tvoption2.setTextColor(Color.WHITE);
                            dataUtil.setOpt2clicked(1);
                            cbOption2.setChecked(true);
                            img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
                        } else {
                            user_ans_al.remove("2");
                            tvoption2.setTextColor(Color.BLACK);
                            dataUtil.setOpt2clicked(0);
                            cbOption2.setChecked(false);
                            tvoption2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("2")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                tvoption2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            else
                                img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                                tvoption2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            else
                                img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }
*/

                    }
                });
                opt_img3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option3ClickTask(dataUtil);
                    /*showInputDialog(dataUtil, 3, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        user_ans_al.add(0, "3");
                        dataUtil.setUser_ans(user_ans_al);

                        rbOption1.setChecked(false);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(true);
                        rbOption4.setChecked(false);

                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.WHITE);
                        tvoption4.setTextColor(Color.BLACK);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(1);
                        dataUtil.setOpt4clicked(0);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
                        img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt3clicked() == 0) {
                            user_ans_al.add("3");
                            tvoption3.setTextColor(Color.WHITE);
                            dataUtil.setOpt3clicked(1);
                            cbOption3.setChecked(true);
                            img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
                        } else {
                            user_ans_al.remove("3");
                            tvoption3.setTextColor(Color.BLACK);
                            dataUtil.setOpt3clicked(0);
                            cbOption3.setChecked(false);
                            tvoption3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        }
                        dataUtil.setUser_ans(user_ans_al);
                    }

                    if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                        next_btn.setVisibility(View.VISIBLE);
                    }

                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("3")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                tvoption3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            else
                                img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                                tvoption3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            else
                                img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }

*/
                    }
                });
                opt_img4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        option4ClickTask(dataUtil);
                    /*showInputDialog(dataUtil, 4, j);
                    if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                        user_ans_al.add(0, "4");
                        rbOption1.setChecked(false);
                        rbOption2.setChecked(false);
                        rbOption3.setChecked(false);
                        rbOption4.setChecked(true);

                        dataUtil.setUser_ans(user_ans_al);
                        if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                            next_btn.setVisibility(View.VISIBLE);
                        }
                        tvoption1.setTextColor(Color.BLACK);
                        tvoption2.setTextColor(Color.BLACK);
                        tvoption3.setTextColor(Color.BLACK);
                        tvoption4.setTextColor(Color.WHITE);
                        dataUtil.setOpt1clicked(0);
                        dataUtil.setOpt2clicked(0);
                        dataUtil.setOpt3clicked(0);
                        dataUtil.setOpt4clicked(1);
                        dataUtil.setOpt5clicked(0);
                        dataUtil.setOpt6clicked(0);
                        dataUtil.setOpt7clicked(0);
                        dataUtil.setOpt8clicked(0);
                        dataUtil.setOpt9clicked(0);
                        dataUtil.setOpt10clicked(0);
                        img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));

                    } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                        if (dataUtil.getOpt4clicked() == 0) {
                            user_ans_al.add("4");
                            tvoption4.setTextColor(Color.WHITE);
                            dataUtil.setOpt4clicked(1);
                            cbOption4.setChecked(true);
                            img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
                        } else {
                            user_ans_al.remove("4");
                            tvoption4.setTextColor(Color.BLACK);
                            dataUtil.setOpt4clicked(0);
                            cbOption4.setChecked(false);
                            tvoption4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
                        }
                        dataUtil.setUser_ans(user_ans_al);

                    }
                    if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
                        if (dataUtil.getCorrect_option().equalsIgnoreCase("4")) {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                tvoption4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            else
                                img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                            isCorrectAns = "true";
                        } else {
                            if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                                tvoption4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            else
                                img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                            showAlart("Error", "Incorrect option! Try again");
                        }
                    }*/

                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setQuestionData(int i) {
        if (!TextUtils.isEmpty(dataUtil.getQuestion_hint())) {
            comp_title_txt.setVisibility(View.VISIBLE);
            comp_title_txt.setText(Html.fromHtml(dataUtil.getQuestion_hint()));
        } else {
            comp_title_txt.setVisibility(View.GONE);
        }

        count_txt.setVisibility(View.VISIBLE);
        count_txt.setText((i + 1) + "/" + list.size());
        question_no_txt.setText("Q" + (i + 1) + ". ");
        if (dataUtil.getIs_question_image().equalsIgnoreCase("0")) {
            tvquestion_description.setVisibility(View.VISIBLE);
            tvquestion_description.setText(Html.fromHtml(dataUtil.getQuestion_description()));
            ques_img.setVisibility(View.GONE);
        } else {
            tvquestion_description.setVisibility(View.GONE);
            ques_img.setVisibility(View.VISIBLE);
            /*byte[] decodedString = Base64.decode(dataUtil.getQuestion_description(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            ques_img.setImageBitmap(decodedByte);*/
            if (!TextUtils.isEmpty(dataUtil.getQuestion_description_url())) {
                Picasso.with(this).load(dataUtil.getQuestion_description_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(ques_img);
            }
        }
    }

    private void setArrangeTheColumnData(Data_util dataUtil, final int i) {
        llAnswer.setVisibility(View.VISIBLE);
//            question_no_txt.setVisibility(View.VISIBLE);
//            llAnswer.setVisibility(View.VISIBLE);
        llMatchColumn.setVisibility(View.GONE);
        llArrangeOrder.setVisibility(View.VISIBLE);
        tvoption1.setText("");
        tvoption2.setText("");
        tvoption3.setText("");
        mEtFeedback.setVisibility(View.GONE);
        tvoption4.setText("");
        if (!TextUtils.isEmpty(dataUtil.getQuestion_hint())) {
            comp_title_txt.setVisibility(View.VISIBLE);
            comp_title_txt.setText(Html.fromHtml(dataUtil.getQuestion_hint()));
        } else {
            comp_title_txt.setVisibility(View.GONE);
        }

        if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
            next_btn.setVisibility(View.GONE);
        }
        count_txt.setVisibility(View.VISIBLE);
        count_txt.setText((i + 1) + "/" + list.size());
        question_no_txt.setText("Q" + (i + 1) + ". ");
        if (dataUtil.getIs_question_image().equalsIgnoreCase("0")) {
            tvquestion_description.setVisibility(View.VISIBLE);
            tvquestion_description.setText(Html.fromHtml(dataUtil.getQuestion_description()));
            ques_img.setVisibility(View.GONE);
        } else {
            tvquestion_description.setVisibility(View.GONE);
            ques_img.setVisibility(View.VISIBLE);
          /*  byte[] decodedString = Base64.decode(dataUtil.getQuestion_description(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            ques_img.setImageBitmap(decodedByte);*/
            if (!TextUtils.isEmpty(dataUtil.getQuestion_description_url())) {
                Picasso.with(this).load(dataUtil.getQuestion_description_url()).placeholder(R.drawable.default_course).error(R.drawable.splash_logo).into(ques_img);
            }
        }

    }

    /**
     * It is used to show the match the column data of a particular group_id
     *
     * @param i
     */
    private void setMatchTheColumnData(int i) {
        boolean shouldShuffle = true;
        matchColumnIndex = i;
        mEtFeedback.setVisibility(View.GONE);
        llAnswer.setVisibility(View.GONE);
        llMatchColumn.setVisibility(View.VISIBLE);
        matchTheColumnAl.clear();
//        matchTheColumnAl = new ArrayList<>();
        Data_util dataUtil = list.get(i);
        String groupId = dataUtil.getGroup_id();
        if (!TextUtils.isEmpty(groupId)) {
            int listSize = list.size();
            if (listSize > i) {
                for (int k = 0; k < listSize; k++) {
                    Data_util currentDataUtil = list.get(k);
                    if (dataUtil.getGroup_id().equalsIgnoreCase(currentDataUtil.getGroup_id())) {
                        matchTheColumnAl.add(currentDataUtil);
                        j = k;
                    }/* else
                        break;*/
                }
            }

            if (j >= list.size() - 1) {
                next_btn.setText("Done");
            }
            count_txt.setText("");
            // set left recyclerview data
            if (leftRVAdapter == null) {
                leftRVAdapter = new LeftRVAdapter(matchTheColumnAl);
                LinearLayoutManager layoutManager1 = new LinearLayoutManager(MainActivity.this);
                rvLeftQuest.setLayoutManager(layoutManager1);
                rvLeftQuest.setAdapter(leftRVAdapter);
            } else
                leftRVAdapter.notifyDataSetChanged();


//            ArrayList<Data_util> answerMatchTheColumnAl = new ArrayList<>();
            answerMatchTheColumnAl.clear();
            answerMatchTheColumnAl.addAll(matchTheColumnAl);
            for (int p = 0; p < matchTheColumnAl.size(); p++) {
                Data_util dataUtil1 = matchTheColumnAl.get(p);
                if (dataUtil1.getUser_ans() != null && dataUtil1.getUser_ans().size() > 0) {
                    shouldShuffle = false;
                    break;
                }
            }
            if (shouldShuffle)
                Collections.shuffle(answerMatchTheColumnAl);
            if (rightRVAdapter == null) {
                rightRVAdapter = new RightRVAdapter(matchTheColumnAl, answerMatchTheColumnAl, shouldShuffle);
                LinearLayoutManager layoutManager2 = new LinearLayoutManager(MainActivity.this);
                rvRightAns.setLayoutManager(layoutManager2);
                rvRightAns.setAdapter(rightRVAdapter);
                callback = new SimpleItemTouchHelperCallback(rightRVAdapter);
                mItemTouchHelper = new ItemTouchHelper(callback);

                mItemTouchHelper.attachToRecyclerView(rvRightAns);
            } else
                rightRVAdapter.notifyDataSetChanged();


        }

    }


    /**
     * This is used to set visibility and checked status of single and multiple type
     * * @param dataUtil
     *
     * @param cbOption
     * @param rbOption
     * @param cbStatus
     * @param rbStatus
     */
    private void setQuestionRbCbVisibility(Data_util dataUtil, CheckBox cbOption, RadioButton rbOption, boolean cbStatus, boolean rbStatus, boolean shouldVisible) {
        cbOption.setChecked(cbStatus);
        rbOption.setChecked(rbStatus);
        if (shouldVisible)
            if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE"))
                rbOption.setVisibility(View.VISIBLE);
            else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                cbOption.setVisibility(View.VISIBLE);
    }

    private void option1ClickTask(Data_util dataUtil) {
        showInputDialog(dataUtil, 1, j);
        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
            if (user_ans_al.size() > 0)
                user_ans_al.set(0, "1");
            else
                user_ans_al.add(0, "1");
            dataUtil.setUser_ans(user_ans_al);

            rbOption1.setChecked(true);
            rbOption2.setChecked(false);
            rbOption3.setChecked(false);
            rbOption4.setChecked(false);

            tvoption1.setTextColor(Color.WHITE);
            tvoption2.setTextColor(Color.BLACK);
            tvoption3.setTextColor(Color.BLACK);
            tvoption4.setTextColor(Color.BLACK);
            dataUtil.setOpt1clicked(1);
            dataUtil.setOpt2clicked(0);
            dataUtil.setOpt3clicked(0);
            dataUtil.setOpt4clicked(0);
            dataUtil.setOpt5clicked(0);
            dataUtil.setOpt6clicked(0);
            dataUtil.setOpt7clicked(0);
            dataUtil.setOpt8clicked(0);
            dataUtil.setOpt9clicked(0);
            dataUtil.setOpt10clicked(0);
            rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));


        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
            if (dataUtil.getOpt1clicked() == 0) {
                user_ans_al.add("1");
                tvoption1.setTextColor(Color.WHITE);
                dataUtil.setOpt1clicked(1);
                cbOption1.setChecked(true);
                rlOption1.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            } else {
                user_ans_al.remove("1");
                tvoption1.setTextColor(Color.BLACK);
                dataUtil.setOpt1clicked(0);
                cbOption1.setChecked(false);
                rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                ;
            }
            dataUtil.setUser_ans(user_ans_al);

        }
        if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
            next_btn.setVisibility(View.VISIBLE);
        }
        if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
            if (dataUtil.getCorrect_option().equalsIgnoreCase("1")) {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                    rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                else
                    rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                isCorrectAns = "true";
            } else {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option1_image()) || dataUtil.getIs_option1_image().equalsIgnoreCase("0"))
                    rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                else
                    rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                showAlart("Error", "Incorrect option! Try again");
            }
        }

    }

    private void option2ClickTask(Data_util dataUtil) {
        showInputDialog(dataUtil, 2, j);
        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
            if (user_ans_al.size() > 0)
                user_ans_al.set(0, "2");
            else
                user_ans_al.add(0, "2");
            dataUtil.setUser_ans(user_ans_al);

            rbOption1.setChecked(false);
            rbOption2.setChecked(true);
            rbOption3.setChecked(false);
            rbOption4.setChecked(false);

            tvoption1.setTextColor(Color.BLACK);
            tvoption2.setTextColor(Color.WHITE);
            tvoption3.setTextColor(Color.BLACK);
            tvoption4.setTextColor(Color.BLACK);
            dataUtil.setOpt1clicked(0);
            dataUtil.setOpt2clicked(1);
            dataUtil.setOpt3clicked(0);
            dataUtil.setOpt4clicked(0);
            dataUtil.setOpt5clicked(0);
            dataUtil.setOpt6clicked(0);
            dataUtil.setOpt7clicked(0);
            dataUtil.setOpt8clicked(0);
            dataUtil.setOpt9clicked(0);
            dataUtil.setOpt10clicked(0);
            rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));

        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
            if (dataUtil.getOpt2clicked() == 0) {
                user_ans_al.add("2");
                tvoption2.setTextColor(Color.WHITE);
                dataUtil.setOpt2clicked(1);
                cbOption2.setChecked(true);
                rlOption2.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            } else {
                user_ans_al.remove("2");
                tvoption2.setTextColor(Color.BLACK);
                dataUtil.setOpt2clicked(0);
                cbOption2.setChecked(false);
                rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                ;
            }
            dataUtil.setUser_ans(user_ans_al);
        }
        if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
            next_btn.setVisibility(View.VISIBLE);
        }
        if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
            if (dataUtil.getCorrect_option().equalsIgnoreCase("2")) {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                    rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                else
                    rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                isCorrectAns = "true";
            } else {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option2_image()) || dataUtil.getIs_option2_image().equalsIgnoreCase("0"))
                    rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                else
                    rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                showAlart("Error", "Incorrect option! Try again");
            }
        }
    }

    private void option3ClickTask(Data_util dataUtil) {
        showInputDialog(dataUtil, 3, j);
        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
            if (user_ans_al.size() > 0)
                user_ans_al.set(0, "3");
            else
                user_ans_al.add(0, "3");
            dataUtil.setUser_ans(user_ans_al);

            rbOption1.setChecked(false);
            rbOption2.setChecked(false);
            rbOption3.setChecked(true);
            rbOption4.setChecked(false);

            tvoption1.setTextColor(Color.BLACK);
            tvoption2.setTextColor(Color.BLACK);
            tvoption3.setTextColor(Color.WHITE);
            tvoption4.setTextColor(Color.BLACK);
            dataUtil.setOpt1clicked(0);
            dataUtil.setOpt2clicked(0);
            dataUtil.setOpt3clicked(1);
            dataUtil.setOpt4clicked(0);
            dataUtil.setOpt5clicked(0);
            dataUtil.setOpt6clicked(0);
            dataUtil.setOpt7clicked(0);
            dataUtil.setOpt8clicked(0);
            dataUtil.setOpt9clicked(0);
            dataUtil.setOpt10clicked(0);
            rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));

            rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));

            rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));


        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
            if (dataUtil.getOpt3clicked() == 0) {
                user_ans_al.add("3");
                tvoption3.setTextColor(Color.WHITE);
                dataUtil.setOpt3clicked(1);
                cbOption3.setChecked(true);
                rlOption3.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            } else {
                user_ans_al.remove("3");
                tvoption3.setTextColor(Color.BLACK);
                dataUtil.setOpt3clicked(0);
                cbOption3.setChecked(false);
                rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                ;
            }
            dataUtil.setUser_ans(user_ans_al);
        }
        if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
            next_btn.setVisibility(View.VISIBLE);
        }
        if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
            if (dataUtil.getCorrect_option().equalsIgnoreCase("3")) {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                    rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                else
                    rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                isCorrectAns = "true";
            } else {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option3_image()) || dataUtil.getIs_option3_image().equalsIgnoreCase("0"))
                    rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                else
                    rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                showAlart("Error", "Incorrect option! Try again");
            }
        }
    }

    private void option4ClickTask(Data_util dataUtil) {
        showInputDialog(dataUtil, 4, j);
        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE")) {
            if (user_ans_al.size() > 0)
                user_ans_al.set(0, "4");
            else
                user_ans_al.add(0, "4");
            dataUtil.setUser_ans(user_ans_al);

            rbOption1.setChecked(false);
            rbOption2.setChecked(false);
            rbOption3.setChecked(false);
            rbOption4.setChecked(true);

            tvoption1.setTextColor(Color.BLACK);
            tvoption2.setTextColor(Color.BLACK);
            tvoption3.setTextColor(Color.BLACK);
            tvoption4.setTextColor(Color.WHITE);
            dataUtil.setOpt1clicked(0);
            dataUtil.setOpt2clicked(0);
            dataUtil.setOpt3clicked(0);
            dataUtil.setOpt4clicked(1);
            dataUtil.setOpt5clicked(0);
            dataUtil.setOpt6clicked(0);
            dataUtil.setOpt7clicked(0);
            dataUtil.setOpt8clicked(0);
            dataUtil.setOpt9clicked(0);
            dataUtil.setOpt10clicked(0);
            rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            rlOption2.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            rlOption3.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            rlOption1.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));


        } else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
            if (dataUtil.getOpt4clicked() == 0) {
                user_ans_al.add("4");
                tvoption4.setTextColor(Color.WHITE);
                dataUtil.setOpt4clicked(1);
                cbOption4.setChecked(true);
                rlOption4.setCardBackgroundColor(Color.parseColor(Utils.getColorPrimaryinString()));
            } else {
                user_ans_al.remove("4");
                tvoption4.setTextColor(Color.BLACK);
                dataUtil.setOpt4clicked(0);
                cbOption4.setChecked(false);
                rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
                ;
            }
            dataUtil.setUser_ans(user_ans_al);
        }
        if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
            next_btn.setVisibility(View.VISIBLE);
        }
        if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
//                        clear();
            if (dataUtil.getCorrect_option().equalsIgnoreCase("4")) {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                    rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                else
                    rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_card));
                isCorrectAns = "true";
            } else {
                if ("null".equalsIgnoreCase(dataUtil.getIs_option4_image()) || dataUtil.getIs_option4_image().equalsIgnoreCase("0"))
                    rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                else
                    rlOption4.setCardBackgroundColor(ContextCompat.getColor(this, R.color.red_card));
                showAlart("Error", "Incorrect option! Try again");
            }
        }

    }
  /*  private void selectSIngleOptionQuiz(int quesPosition, String ansPosition) {
        TextView[] allTextViews = new TextView[]{tvoption1, tvoption2, tvoption3, tvoption4};
        LinearLayout[] allLinearLayout = new LinearLayout[]{img_ll1, img_ll2, img_ll3, img_ll4};

        list.get(quesPosition).setUser_ans(ansPosition);
        if (TEST_PATTERN.equalsIgnoreCase("SEQUENTIAL") && !list.get(quesPosition).getUser_ans().equalsIgnoreCase("")) {
            next_btn.setVisibility(View.VISIBLE);
        }
        for (int j = 0; j < allTextViews.length; j++) {
            if (j == (Integer.valueOf(ansPosition) - 1)) {
                allTextViews[j].setTextColor(Color.WHITE);
                allLinearLayout[j].setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));
            } else {
                ((TextView) allTextViews[j]).setTextColor(Color.BLACK);
                allLinearLayout[j].setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
            }
        }

       *//* tvoption1.setTextColor(Color.BLACK);
        tvoption2.setTextColor(Color.BLACK);
        tvoption3.setTextColor(Color.BLACK);
        tvoption4.setTextColor(Color.WHITE);*//*
        switch (ansPosition) {
            case "0":
                break;
            case "1":
                break;
            case "2":
                break;
            case "3":
                break;
            case "4":
                break;
            case "5":
                break;
            case "6":
                break;
            case "7":
                break;
            case "8":
                break;
            case "9":
                break;


        }

        *//*img_ll1.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
        img_ll2.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
        img_ll3.setCardBackgroundColor(ContextCompat.getColor(this,R.color.white));;
        img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_card));*//*
        if (TEST_PATTERN.equalsIgnoreCase("INCREMENTAL") || TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE")) {
            clear();
            if (list.get(quesPosition).getCorrect_option().equalsIgnoreCase(ansPosition)) {
                if ("null".equalsIgnoreCase(list.get(quesPosition).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0"))
                    tvoption4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                else
                    img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.green_card));
                isCorrectAns = "true";
            } else {
                if ("null".equalsIgnoreCase(list.get(i).getIs_option4_image()) || list.get(i).getIs_option4_image().equalsIgnoreCase("0"))
                    tvoption4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                else
                    img_ll4.setCardBackgroundColor(ContextCompat.getColor(this,R.color.red_card));
                showAlart("Error", "Incorrect option! Try again");
            }
        }


    }*/

   /* private QuestionUtility parseQuestionData(String resp) {
        QuestionUtility questionUtility = new QuestionUtility();
        try {
            JSONObject jsonObject = new JSONObject(resp);

            jsonObject = jsonObject.getJSONObject("data");
            questionUtility.setMaterial_id(jsonObject.getString("material_id"));
            questionUtility.setMaterial_media_id(jsonObject.getString("material_media_id"));
            questionUtility.setModule_id(jsonObject.getString("module_id"));
            questionUtility.setTitle(jsonObject.getString("title"));
            questionUtility.setDescription(jsonObject.getString("description"));
            questionUtility.setInstruction(jsonObject.getString("instruction"));
            questionUtility.setMaterial_type(jsonObject.getString("material_type"));
            questionUtility.setKeywords(jsonObject.getString("keywords"));
            questionUtility.setRead_more(jsonObject.getString("read_more"));
            questionUtility.setRead_more_required(jsonObject.getString("read_more_required"));
            questionUtility.setAvailability(jsonObject.getString("availability"));
            questionUtility.setNo_of_attempt(jsonObject.getString("no_of_attempt"));
            questionUtility.setIs_same_score(jsonObject.getString("is_same_score"));
            questionUtility.setNegative_marks(jsonObject.getString("negative_marks"));
            questionUtility.setAlloted_time(jsonObject.getString("alloted_time"));
            questionUtility.setTest_pattern(jsonObject.getString("test_pattern"));
            questionUtility.setQuestion_sequence(jsonObject.getString("question_sequence"));
            questionUtility.setOption_sequence(jsonObject.getString("option_sequence"));
            questionUtility.setNo_of_question(jsonObject.getString("no_of_question"));
            questionUtility.setShow_score(jsonObject.getString("show_score"));
            questionUtility.setShow_status(jsonObject.getString("show_status"));
            questionUtility.setIs_incremented(jsonObject.getString("is_incremented"));
            questionUtility.setSort_order(jsonObject.getString("sort_order"));

            JSONArray array = jsonObject.getJSONArray("question_array");
            if (array != null) {
                ArrayList<Data_util> dataUtilArrayList = new ArrayList<>();
                for (int i = 0; i < array.length(); i++) {
                    Data_util data_util = new Data_util();
                    JSONObject jsonObject5 = array.getJSONObject(i);

                    data_util.setAssign_question_id(jsonObject5.getString("assign_question_id"));
                    data_util.setQuestion_bank_id(jsonObject5.getString("question_bank_id"));
                    data_util.setCourse_id(jsonObject5.getString("course_id"));
                    data_util.setModule_id(jsonObject5.getString("module_id"));
                    data_util.setChapter_id(jsonObject5.getString("chapter_id"));
                    data_util.setQuestion_name(jsonObject5.getString("question_name"));
                    data_util.setQuestion_description(jsonObject5.getString("question_description"));
                    data_util.setQuestion_type(jsonObject5.getString("question_type"));
                    data_util.setQuestion_hint(jsonObject5.getString("question_hint"));
                    data_util.setError_message(jsonObject5.getString("error_message"));
                    data_util.setExplanation(jsonObject5.getString("explanation"));
                    data_util.setIs_question_image(jsonObject5.getString("is_question_image"));
                    data_util.setOption1(jsonObject5.getString("option1"));
                    data_util.setIs_option1_image(jsonObject5.getString("is_option1_image"));
                    data_util.setOption1_marks(jsonObject5.getString("option1_marks"));
                    data_util.setOption2(jsonObject5.getString("option2"));
                    data_util.setIs_option2_image(jsonObject5.getString("is_option2_image"));
                    data_util.setOption2_marks(jsonObject5.getString("option2_marks"));
                    data_util.setOption3(jsonObject5.getString("option3"));
                    data_util.setIs_option3_image(jsonObject5.getString("is_option3_image"));
                    data_util.setOption3_marks(jsonObject5.getString("option3_marks"));
                    data_util.setOption4(jsonObject5.getString("option4"));
                    data_util.setIs_option4_image(jsonObject5.getString("is_option4_image"));
                    data_util.setOption4_marks(jsonObject5.getString("option4_marks"));


                    data_util.setCorrect_option(jsonObject5.getString("correct_option"));
                    data_util.setMarks(jsonObject5.getString("marks"));
                    data_util.setUser_id(jsonObject5.getString("user_id"));

                    dataUtilArrayList.add(data_util);

                }
                questionUtility.setData_utils(dataUtilArrayList);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return questionUtility;
    }*/

    public static String convertSecondsToHMmSs(long seconds) {
//        long s = seconds % 60;
//        long m = (seconds / 60) % 60;
        long h = seconds / 3600;
        long m = (seconds % 3600) / 60;
        long s = seconds % 60;

        String timeString = String.format("%02d:%02d:%02d", h, m, s);
        return timeString;
    }

    private void startTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (TimerVal) {
                    try {
                        Thread.sleep(1000);
                        handler2.post(new Runnable() {

                            @Override
                            public void run() {
                                if (TimerVal) {
                                    Log.d("time", totalTime + " Main");
                                    if (totalTime != 0) {
                                        --totalTime;
                                        int warnTime = -1;
                                        if (!WebServices.questionUtility.getWarning_time().equalsIgnoreCase(""))
                                            warnTime = Integer.parseInt(WebServices.questionUtility.getWarning_time());
                                        if (totalTime == warnTime * 60) {
                                            if (dialog1 != null && dialog1.isShowing())
                                                dialog1.dismiss();
                                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                                            alertDialog.setTitle("Warning");
                                            alertDialog.setCanceledOnTouchOutside(false);
//                                        alertDialog.setMessage(getString(R.string.you_have) + " " + warnTime + " " + getString(R.string.please_review));
                                            alertDialog.setMessage(WebServices.questionUtility.getWarning_message());
                                            final int finalWarnTime = warnTime;
                                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                            mixPanelManager.warningQuizMixpanel(MainActivity.this, WebServices.mLoginUtility.getEmail(), WebServices.flashcardMeterialUtilities.getTitle(), "Quiz", finalWarnTime + " minutes");
                                                        }
                                                    });
                                            if (!isFinishing()) {
                                                alertDialog.show();
                                            }
                                        }
                                    }
                                    if (TIMERVALUE != 0) {

                                        tvQuesTimer.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
                                        if (TIMERVALUE != totalTime && !TextUtils.isEmpty(meterialUtility.getEach_ques_time()) && meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {

                                            tvQuesTimer.setVisibility(View.VISIBLE);
                                            ivQuesTimer.setVisibility(View.VISIBLE);

                                            tvQuesTimer.setText(convertSecondsToHMmSs(--TIMERVALUE));
                                            time_txt.setText(convertSecondsToHMmSs(totalTime));
                                            if (!TextUtils.isEmpty(WebServices.questionUtility.getEach_ques_warning_time()) && !WebServices.questionUtility.getEach_ques_warning_time().equals("0")) {
                                                long eachWarnTime = (TIMEREACHQUES * Long.parseLong(WebServices.questionUtility.getEach_ques_warning_time()) / 100);
                                                setWarningText(eachWarnTime);
                                            } else {
                                                long eachWarnTime = TIMEREACHQUES / 100;
                                                setWarningText(eachWarnTime);
                                            }
                                        } else {
                                            tvQuesTimer.setVisibility(View.GONE);
                                            ivQuesTimer.setVisibility(View.GONE);
                                            time_txt.setText(convertSecondsToHMmSs(--TIMERVALUE));
                                        }
                                    } else {
                                        quesEndTime = TIMERVALUE;
                                        list.get(j).setTime_taken(Utils.getTimeDifference(questStartTime, quesEndTime, list.get(j).getTime_taken()));
                                        WebServices.questionUtility.setData_utils(list);
//                                    TimerVal = false;
                                        int corr_que = 0, incorr_que = 0, skip_que = 0;
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        QUIZEND_TIME = df.format(c.getTime());
                                        int size = WebServices.questionUtility.getData_utils().size();
                                        for (int i = 0; i < size; i++) {
                                            Data_util dataUtil = WebServices.questionUtility.getData_utils().get(i);
                                            if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                                                    ArrayList<String> correctAnswerAl = convertStringToAl(dataUtil.getCorrect_option());
                                                    if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                                                        String correctOptionType = dataUtil.getCorrect_option_type();
                                                        switch (correctOptionType) {
                                                            case "ALL":
                                                                if (QuizUtils.checkAnsForAll(dataUtil.getUser_ans(), correctAnswerAl)) {
                                                                    dataUtil.setCorrect(true);
                                                                    corr_que++;
                                                                } else {
                                                                    list.get(j).setCorrect(false);
                                                                    incorr_que++;
                                                                }
                                                                break;
                                                            case "ANY":
                                                                if (QuizUtils.checkAnsForAny(dataUtil.getUser_ans(), correctAnswerAl)) {
                                                                    dataUtil.setCorrect(true);
                                                                    corr_que++;
                                                                } else {
                                                                    list.get(j).setCorrect(false);
                                                                    incorr_que++;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                } else {
                                                    skip_que++;
                                                }

                                            } else {
                                                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                                                    if (dataUtil.getUser_ans().get(0).equalsIgnoreCase(dataUtil.getCorrect_option())) {
                                                        dataUtil.setCorrect(true);
                                                        corr_que++;
                                                    } else {
                                                        list.get(j).setCorrect(false);
                                                        incorr_que++;
                                                    }
                                                } else {
                                                    skip_que++;
                                                }
                                            }
                                        }
                                        if ((corr_que + incorr_que + skip_que) == WebServices.questionUtility.getData_utils().size()) {
                                            int percent = 0;

                                            percent = ((corr_que) * 100) / WebServices.questionUtility.getData_utils().size();
                                            long completed = ((Long.parseLong(meterialUtility.getAlloted_time()) * 60) - TIMERVALUE);
//                                    if (!TEST_PATTERN.equalsIgnoreCase("COMPREHENSIVE") && !TEST_PATTERN.equalsIgnoreCase("INCREMENTAL")) {
//                                if (percent!=100){
//
//                                for (int i=1;i<com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().size();i++){
//                                    if (com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().get(i).getMaterial_id().equalsIgnoreCase(getIntent().getStringExtra("meterial_id"))){
//                                        com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().get(i).setComplete_per(percent + "");
//                                        com.soilapp.home.MultichoiceActivity.modulesUtilityArrayList.get(com.soilapp.home.MultichoiceActivity.SELECTEDPOS).getMeterialUtilityArrayList().get(i).setResponse_status("PENDING");
//                                        break;
//                                    }
//                                }
//                                saveQuitquizResponse(percent + "", corr_que + "", incorr_que + "", getIntent().getStringExtra("time"));
//                                mixPanelManager.Pausematerial(MultichoiceActivity.this, WebServices.mLoginUtility.getEmail(), getIntent().getStringExtra("coursename"), getIntent().getStringExtra("name"), "Quiz", completed + "", TEST_PATTERN);
//                                }else{
                                            if (meterialUtility.getEach_ques_time().equalsIgnoreCase("yes")) {
                                                if (j == list.size() - 1) {
                                                    TimerVal = false;
                                                    if (dialog1 != null && dialog1.isShowing())
                                                        dialog1.dismiss();
                                                    savequizResponse(percent + "", corr_que + "", incorr_que + "", false);
                                                    mixPanelManager.timeoutmaterial(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "Quiz", percent + "");
                                                    finish();
                                                } else {
                                                    if (dialog1 != null && dialog1.isShowing())
                                                        dialog1.dismiss();
                                                    nextButtonTask(false);
                                                }
                                            } else {
                                                TimerVal = false;
                                                if (dialog1 != null && dialog1.isShowing())
                                                    dialog1.dismiss();
                                                savequizResponse(percent + "", corr_que + "", incorr_que + "", false);
                                                mixPanelManager.timeoutmaterial(MainActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "Quiz", percent + "");
                                                finish();
                                            }

                                        } else {
                                            TimerVal = false;
                                            if (!isFinishing()) {
                                                new AlertDialog.Builder(MainActivity.this)
                                                        .setTitle("Error")
                                                        .setMessage("Some error occurred. Can not submit Quiz this time. Please try again.")
                                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {

                                                                finish();
                                                            }
                                                        })

                                                        .show();
                                            }
                                        }

                                    }

                                }
                            }
                        });
                    } catch (Exception e) {
                    }
                }

            }
        }).start();
    }


    private void setWarningText(float eachWarnTime) {
        if (TIMERVALUE < eachWarnTime)
            tvQuesTimer.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.warning_color));
        else
            tvQuesTimer.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);

    }


            /*case R.id.cb_option1:
            case R.id.rb_option1:
                option1ClickTask(dataUtil);
                break;
            case R.id.cb_option2:
            case R.id.rb_option2:
                option2ClickTask(dataUtil);
                break;
            case R.id.cb_option3:
            case R.id.rb_option3:
                option3ClickTask(dataUtil);
                break;
            case R.id.cb_option4:
            case R.id.rb_option4:
                option4ClickTask(dataUtil);
                break;*/


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cb_option1:
            case R.id.rb_option1:
                option1ClickTask(dataUtil);

                break;
            case R.id.cb_option2:
            case R.id.rb_option2:
                option2ClickTask(dataUtil);
                break;
            case R.id.cb_option3:
            case R.id.rb_option3:
                option3ClickTask(dataUtil);
                break;
            case R.id.cb_option4:
            case R.id.rb_option4:
                option4ClickTask(dataUtil);
                break;

        }
    }

    private void savequizResponse(String persentage, String corr, String incorr, boolean isBack) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("question_bank_id", WebServices.questionUtility.getData_utils().get(i).getQuestion_bank_id());
                jsonObject.put("assign_question_id", WebServices.questionUtility.getData_utils().get(i).getAssign_question_id());
                jsonObject.put("answer_key",QuizUtils.getRealCorrectOption(WebServices.questionUtility.getData_utils().get(i)));
                jsonObject.put("correct_option", WebServices.questionUtility.getData_utils().get(i).getRealCorrectOption());
                jsonObject.put("answer", WebServices.questionUtility.getData_utils().get(i).getUser_input());
                jsonObject.put("question_description", WebServices.questionUtility.getData_utils().get(i).getQuestion_description());
                jsonObject.put("answer_type", WebServices.questionUtility.getData_utils().get(i).getOption_type());
                if (WebServices.questionUtility.getData_utils().get(i).isCorrect())
                    jsonObject.put("marks", WebServices.questionUtility.getData_utils().get(i).getMarks());
                else
                    jsonObject.put("marks", "0");
                jsonObject.put("time_taken", WebServices.questionUtility.getData_utils().get(i).getTime_taken());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        String coinsCollected = "0";
        Toast.makeText(this, "Your response is submitted", Toast.LENGTH_SHORT).show();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        meterialUtility.setMaterialEndTime(df.format(c.getTime()));
        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, j + "", list.size() + "", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, meterialUtility, persentage, corr, incorr, jsonArray.toString(), coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());
        if (WebServices.isNetworkAvailable(MainActivity.this)) {
//            dataBase.deleteRowGetResponse(WebServices.questionUtility.getMaterial_id());
            if (!isBack) {
                coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility,Integer.valueOf(persentage), redeem));
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    Toast.makeText(this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
            }
            new SubmitData(MainActivity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, dataBase).execute(corr, incorr, jsonArray.toString(), persentage);
        } else {
            if (!isBack) {
                /*coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(Integer.valueOf(persentage), redeem));
                if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                    coinsAllocatedModel.setRedeem("TRUE");
                    if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                        WebServices.updateTotalCoins(MainActivity.this, coinsCollected);

                }*/
            }
//            new WebServices().addSubmitResponse(dataBase, meterialUtility.getCourse_id(), WebServices.questionUtility.getMaterial_id(), "QUIZ", persentage, corr, incorr, WebServices.questionUtility.getTest_pattern(), WebServices.questionUtility.getTitle(), jsonArray.toString(), coinsCollected, redeem, com.chap_tempone.home.MainActivity.MODULEID, com.chap_tempone.home.MainActivity.ASSIGNMATERIALID);


        }
    }

    private void saveEachquizResponse(String persentage, String corr, String incorr) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {
            Data_util dataUtil = WebServices.questionUtility.getData_utils().get(i);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("question_bank_id", dataUtil.getQuestion_bank_id());
                jsonObject.put("assign_question_id", dataUtil.getAssign_question_id());
                jsonObject.put("answer_key",QuizUtils.getRealCorrectOption(WebServices.questionUtility.getData_utils().get(i)));
                jsonObject.put("correct_option", WebServices.questionUtility.getData_utils().get(i).getRealCorrectOption());
                jsonObject.put("answer", WebServices.questionUtility.getData_utils().get(i).getUser_input());
                jsonObject.put("question_description", WebServices.questionUtility.getData_utils().get(i).getQuestion_description());
                jsonObject.put("answer_type", dataUtil.getOption_type());
                if (dataUtil.isCorrect())
                    jsonObject.put("marks", dataUtil.getMarks());
                else
                    jsonObject.put("marks", "0");
                jsonObject.put("time_taken", dataUtil.getTime_taken());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
//        System.out.println("===jsonArray=="+jsonArray.toString());

//        String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(Integer.valueOf(persentage), redeem));
//        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, j + "", list.size() + "", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, meterialUtility, persentage, corr, incorr, jsonArray.toString(), "0", redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());
//        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
//            WebServices.updateTotalCoins(MainActivity.this, coinsCollected);

    }
   /* private void savequizLastResponse() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", WebServices.questionUtility.getData_utils().get(i).getUser_id());
                jsonObject.put("question_bank_id", WebServices.questionUtility.getData_utils().get(i).getQuestion_bank_id());
                jsonObject.put("assign_question_id", WebServices.questionUtility.getData_utils().get(i).getAssign_question_id());
                jsonObject.put("correct_option", WebServices.questionUtility.getData_utils().get(i).getCorrect_option());
                jsonObject.put("course_id", WebServices.questionUtility.getData_utils().get(i).getCourse_id());
                jsonObject.put("error_message", WebServices.questionUtility.getData_utils().get(i).getError_message());
                jsonObject.put("explanation", WebServices.questionUtility.getData_utils().get(i).getExplanation());
                jsonObject.put("marks", WebServices.questionUtility.getData_utils().get(i).getMarks());
                jsonObject.put("chapter_id", WebServices.questionUtility.getData_utils().get(i).getChapter_id());
                jsonObject.put("module_id", WebServices.questionUtility.getData_utils().get(i).getModule_id());
                jsonObject.put("user_ans", WebServices.questionUtility.getData_utils().get(i).getUser_ans());
                jsonObject.put("question_description", WebServices.questionUtility.getData_utils().get(i).getQuestion_description());
                jsonObject.put("question_hint", WebServices.questionUtility.getData_utils().get(i).getQuestion_hint());
                jsonObject.put("question_name", WebServices.questionUtility.getData_utils().get(i).getQuestion_name());
                jsonObject.put("question_type", WebServices.questionUtility.getData_utils().get(i).getQuestion_type());
                jsonObject.put("is_question_image", WebServices.questionUtility.getData_utils().get(i).getIs_question_image());
                jsonObject.put("is_option1_image", WebServices.questionUtility.getData_utils().get(i).getIs_option1_image());
                jsonObject.put("option1", WebServices.questionUtility.getData_utils().get(i).getOption1());
                jsonObject.put("option1_marks", WebServices.questionUtility.getData_utils().get(i).getOption1_marks());
                jsonObject.put("is_option2_image", WebServices.questionUtility.getData_utils().get(i).getIs_option2_image());
                jsonObject.put("option2", WebServices.questionUtility.getData_utils().get(i).getOption2());
                jsonObject.put("option2_marks", WebServices.questionUtility.getData_utils().get(i).getOption2_marks());
                jsonObject.put("is_option3_image", WebServices.questionUtility.getData_utils().get(i).getIs_option3_image());
                jsonObject.put("option3", WebServices.questionUtility.getData_utils().get(i).getOption3());
                jsonObject.put("option3_marks", WebServices.questionUtility.getData_utils().get(i).getOption3_marks());
                jsonObject.put("is_option4_image", WebServices.questionUtility.getData_utils().get(i).getIs_option4_image());
                jsonObject.put("option4", WebServices.questionUtility.getData_utils().get(i).getOption4());
                jsonObject.put("option4_marks", WebServices.questionUtility.getData_utils().get(i).getOption4_marks());
                jsonObject.put("option1_click", WebServices.questionUtility.getData_utils().get(i).getOpt1clicked());
                jsonObject.put("option2_click", WebServices.questionUtility.getData_utils().get(i).getOpt2clicked());
                jsonObject.put("option3_click", WebServices.questionUtility.getData_utils().get(i).getOpt3clicked());
                jsonObject.put("option4_click", WebServices.questionUtility.getData_utils().get(i).getOpt4clicked());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), WebServices.questionUtility.getMaterial_id()))
            dataBase.addQuizLastData(WebServices.mLoginUtility.getUser_id(), WebServices.questionUtility.getMaterial_id(), jsonArray.toString());
        else
            dataBase.updateQuizLastData(WebServices.mLoginUtility.getUser_id(), WebServices.questionUtility.getMaterial_id(), jsonArray.toString());
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void showInputDialog(final Data_util dataUtil, int optionPos, final int pos) {
        String optionType = "";
        boolean isChecked = false;
        switch (optionPos) {
            case 1:
                optionType = dataUtil.getOption1_type();
                isChecked = checkedDialogStatus(dataUtil, cbOption1, rbOption1);
                break;
            case 2:
                optionType = dataUtil.getOption2_type();
                isChecked = checkedDialogStatus(dataUtil, cbOption2, rbOption2);
                break;
            case 3:
                optionType = dataUtil.getOption3_type();
                isChecked = checkedDialogStatus(dataUtil, cbOption3, rbOption3);
                break;
            case 4:
                optionType = dataUtil.getOption4_type();
                isChecked = checkedDialogStatus(dataUtil, cbOption4, rbOption4);
                break;
        }
        if (TextUtils.isEmpty(optionType) || !optionType.equalsIgnoreCase("text"))
            return;
        // Create custom dialog object
        dialog1 = new Dialog(MainActivity.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog1.setContentView(R.layout.input_dialog);
        // Set dialog title
        dialog1.setTitle("Write your answer");

        final EditText editText = (EditText) dialog1.findViewById(R.id.edit_text);
        // setup a dialog window
        if (!TextUtils.isEmpty(dataUtil.getUser_input()))
            editText.setText(dataUtil.getUser_input());
        Button button = (Button) dialog1.findViewById(R.id.ok_button);
//        dialog.setCancelable(false);
        final String finalOptionType = optionType;
        if (button != null)
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataUtil.setUser_input(editText.getText().toString());
                    dataUtil.setOption_type(finalOptionType + "");
                    Toast.makeText(MainActivity.this, "Your response is added!", Toast.LENGTH_SHORT).show();
                    editText.setText("");
                    dialog1.dismiss();
                }
            });
//        if (!isChecked)
        dialog1.show();
    }

    private boolean checkedDialogStatus(Data_util dataUtil, CheckBox checkBox, RadioButton radioButton) {
        if (dataUtil.getQuestion_type().equalsIgnoreCase("SINGLE"))
            if (radioButton.isChecked())
                return true;
            else if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                if (checkBox.isChecked())
                    return true;
        return false;
    }
}
