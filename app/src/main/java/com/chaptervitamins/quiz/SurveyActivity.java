package com.chaptervitamins.quiz;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.play_video.FlashCardResult;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SurveyActivity extends BaseActivity {
    TextView count_txt, title_txt, tvquestion_description, tvoption1, tvoption2, tvoption3, tvoption4, tvoption5, tvEmoji1, tvEmoji2, tvEmoji3, tvEmoji4, tvEmoji5;
    TextView l1, l2, l3, l4, question_no_txt, l5;
    Button next_btn, prev_btn;
    ImageView back, option_emo_img1, option_emo_img2, option_emo_img3, option_emo_img4, option_emo_img5;
    private ArrayList<SurveyDataUtil> list = new ArrayList<>();
    int j = 0;
    String resp = "";
    WebServices webServices;
    private LinearLayout llOption1Text, llOption2Text, llOption3Text, llOption4Text, llOption5Text;
    Handler handler;
    private RelativeLayout rlFeedback, llOption1, llOption2, llOption3, llOption4, llOption5;
    private ProgressDialog dialog;
    private DataBase dataBase;
    private MixPanelManager mixPanelManager;
    private String endTime, startTime;
    private MeterialUtility mMeterialUtility;
    private EditText etOption1, etOption2, etOption3, etOption4, etOption5;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_survey);
        tvquestion_description = (TextView) findViewById(R.id.question_description);
        tvoption1 = (TextView) findViewById(R.id.option1);
        title_txt = (TextView) findViewById(R.id.title_txt);
        count_txt = (TextView) findViewById(R.id.count_txt);
        tvoption2 = (TextView) findViewById(R.id.option2);
        rlFeedback = (RelativeLayout) findViewById(R.id.rl_feedback);
        l2 = (TextView) findViewById(R.id.l2);
        l1 = (TextView) findViewById(R.id.l1);
        l3 = (TextView) findViewById(R.id.l3);
        l4 = (TextView) findViewById(R.id.l4);
        l5 = (TextView) findViewById(R.id.l5);
        llOption1 = (RelativeLayout) findViewById(R.id.ll_option1);
        llOption2 = (RelativeLayout) findViewById(R.id.ll_option2);
        llOption3 = (RelativeLayout) findViewById(R.id.ll_option3);
        llOption4 = (RelativeLayout) findViewById(R.id.ll_option4);
        llOption5 = (RelativeLayout) findViewById(R.id.ll_option5);
        llOption1Text = (LinearLayout) findViewById(R.id.ll_option1_text);
        llOption2Text = (LinearLayout) findViewById(R.id.ll_option2_text);
        llOption3Text = (LinearLayout) findViewById(R.id.ll_option3_text);
        llOption4Text = (LinearLayout) findViewById(R.id.ll_option4_text);
        llOption5Text = (LinearLayout) findViewById(R.id.ll_option5_text);
        tvEmoji1 = (TextView) findViewById(R.id.tv_emoji1);
        tvEmoji2 = (TextView) findViewById(R.id.tv_emoji2);
        tvEmoji3 = (TextView) findViewById(R.id.tv_emoji3);
        tvEmoji4 = (TextView) findViewById(R.id.tv_emoji4);
        tvEmoji5 = (TextView) findViewById(R.id.tv_emoji5);
        question_no_txt = (TextView) findViewById(R.id.question_no_txt);
        tvoption3 = (TextView) findViewById(R.id.option3);
        tvoption4 = (TextView) findViewById(R.id.option4);
        tvoption5 = (TextView) findViewById(R.id.option5);
        next_btn = (Button) findViewById(R.id.next_btn);
        prev_btn = (Button) findViewById(R.id.previous_btn);
        back = (ImageView) findViewById(R.id.back);
        etOption1 = (EditText) findViewById(R.id.et_option1);
        etOption2 = (EditText) findViewById(R.id.et_option2);
        etOption3 = (EditText) findViewById(R.id.et_option3);
        etOption4 = (EditText) findViewById(R.id.et_option4);
        etOption5 = (EditText) findViewById(R.id.et_option5);
//        back.setVisibility(View.GONE);
        option_emo_img1 = (ImageView) findViewById(R.id.option_emo_img1);
        option_emo_img2 = (ImageView) findViewById(R.id.option_emo_img2);
        option_emo_img3 = (ImageView) findViewById(R.id.option_emo_img3);
        option_emo_img4 = (ImageView) findViewById(R.id.option_emo_img4);
        option_emo_img5 = (ImageView) findViewById(R.id.option_emo_img5);
        webServices = new WebServices();
        mixPanelManager = APIUtility.getMixPanelManager(this);
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        dataBase = DataBase.getInstance(SurveyActivity.this);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTime = DateFormat.getDateTimeInstance().format(new Date());
                if (mMeterialUtility.getTitle() != null)
                    mixPanelManager.selectTimeTrack(SurveyActivity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                            , mMeterialUtility.getTitle(), "Survey");

                SubmitData();
                if (mMeterialUtility != null) {
                    Intent intent = new Intent(SurveyActivity.this, SurveyResultActivity.class);
                    intent.putExtra("meterial_utility", mMeterialUtility);
                    intent.putExtra("course_id", mMeterialUtility.getCourse_id());
                    intent.putExtra("survey_id", mMeterialUtility.getMaterial_id());
                    startActivity(intent);
                }
                finish();
            }
        });
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        title_txt.setText(mMeterialUtility.getTitle());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        coinsAllocatedModel = mMeterialUtility.getCoinsAllocatedModel();
           /* if (coinsAllocatedModel == null) {
                coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getMaterial_id());
            }*/
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else {
            coinsAllocatedModel = new CoinsAllocatedModel();
        }

        calculateNoOfAttempt();
//        if (mMeterialUtility != null)
//            mMeterialUtility.setMaterialStartTime(df.format(c.getTime()));
        count_txt.setText("");
        list = new ArrayList<>();
        resp = dataBase.getSurveyData(WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getMaterial_id());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (dialog != null) dialog.dismiss();
                if (msg.what == 1) {
                    if (WebServices.ERRORMSG.equalsIgnoreCase("")) {
                        dataBase.deleteSurveyData(WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getMaterial_id());
                        dataBase.addSurveyData(WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getMaterial_id(), "");
                        Toast.makeText(SurveyActivity.this, "Some error occurred. unable to load survey", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(SurveyActivity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    if (list.size() != 0) {
                        int selectedpos = 0;
                        if (!WebServices.questionUtility.getQuestionIndex().equalsIgnoreCase(""))
                            selectedpos = Integer.parseInt(WebServices.questionUtility.getQuestionIndex());
                        if (list.size() >= (selectedpos + 1)) {
                            show(selectedpos);
                            j = selectedpos;
                            if (j >= list.size() - 1) {
                                next_btn.setText("Done");
                            }

                        } else {
                            noQuizData();
                        }


                    } else {
                        next_btn.setText("");
                        count_txt.setText("");
                        noQuizData();
                    }
                }
            }
        };

        dialog = ProgressDialog.show(SurveyActivity.this, "", "Please wait..");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        new Thread() {
            public void run() {
                if (webServices.isValid(resp)) {
                    list = webServices.parseSurveyData(resp);
                    handler.sendEmptyMessage(0);
                } else {
                    handler.sendEmptyMessage(1);
                }
            }
        }.start();

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserInputData();
                if (!TextUtils.isEmpty(mMeterialUtility.getTest_pattern()) && mMeterialUtility.getTest_pattern().equalsIgnoreCase("SEQUENTIAL") && TextUtils.isEmpty(list.get(j).getUser_ans())) {
                    showAlart("Required", "Please select an option");
                } else {
                    if (next_btn.getText().toString().equalsIgnoreCase("Done")) {
                        SubmitData();
                        Intent intent = new Intent(SurveyActivity.this, SurveyResultActivity.class);
                        intent.putExtra("meterial_utility", mMeterialUtility);
                        intent.putExtra("course_id", mMeterialUtility.getCourse_id());
                        intent.putExtra("survey_id", mMeterialUtility.getMaterial_id());
                        startActivity(intent);
                        finish();
                        return;
                    }
                    if (list.size() > j) {

                        j++;
                        clear();
                        show(j);
                    }
                }


            }

            private void showAlart(String title, String msg) {
                if (!isFinishing())
                    new AlertDialog.Builder(SurveyActivity.this)
                            .setTitle(title)
                            .setMessage(msg)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                   /* isCorrectAns = "false";*/
                                    if (dialog != null && !isFinishing())
                                        dialog.dismiss();
                                }
                            })
                            .show();
            }
        });

        prev_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.size() > j) {
                    setUserInputData();
                    j--;
                    clear();
                    show(j);
                }
               /* if (j >= list.size() - 1) {
                    next_btn.setText("Done");
                }*/
            }
        });
    }

    private void calculateNoOfAttempt() {
        for (int j = 0; j < HomeActivity.courseUtilities.size(); j++) {
            if (HomeActivity.courseUtilities.get(j).getCourse_id().equals(mMeterialUtility.getCourse_id())) {
                ArrayList<ModulesUtility> modulesUtilityArrayList = HomeActivity.courseUtilities.get(j).getModulesUtilityArrayList();
                for (int k = 0; k < modulesUtilityArrayList.size(); k++) {
                    if (modulesUtilityArrayList.get(k).getModule_id().equals(mMeterialUtility.getModule_id())) {
                        ArrayList<MeterialUtility> meterialUtilityArrayList = modulesUtilityArrayList.get(k).getMeterialUtilityArrayList();
                        for (int l = 0; l < meterialUtilityArrayList.size(); l++) {
                            MeterialUtility utility = meterialUtilityArrayList.get(l);
                            if (utility.getAssign_material_id().equals(mMeterialUtility.getAssign_material_id())) {
                                utility.setRemainingAttempt(String.valueOf(Integer.parseInt(utility.getRemainingAttempt()) - 1));
                            }
                        }
                    }
                }
            }
        }
    }

    private void setTextIntoEditOptions(int i) {
        SurveyDataUtil surveyDataUtil = list.get(i);
        if (!TextUtils.isEmpty(surveyDataUtil.getOption1()))
            etOption1.setHint(surveyDataUtil.getOption1());
        else
            etOption1.setHint("Please enter your feedback...");

        if (!TextUtils.isEmpty(surveyDataUtil.getOption2()))
            etOption2.setHint(surveyDataUtil.getOption2());
        else
            etOption2.setHint("Please enter your feedback...");

        if (!TextUtils.isEmpty(surveyDataUtil.getOption3()))
            etOption3.setHint(surveyDataUtil.getOption3());
        else
            etOption3.setHint("Please enter your feedback...");

        if (!TextUtils.isEmpty(surveyDataUtil.getOption4()))
            etOption4.setHint(surveyDataUtil.getOption4());
        else
            etOption4.setHint("Please enter your feedback...");

        if (!TextUtils.isEmpty(surveyDataUtil.getOption5()))
            etOption5.setHint(surveyDataUtil.getOption5());
        else
            etOption5.setHint("Please enter your feedback...");
    }

    private void setUserInputData() {
        String option1Input = etOption1.getText().toString();
        String option2Input = etOption2.getText().toString();
        String option3Input = etOption3.getText().toString();
        String option4Input = etOption4.getText().toString();
        String option5Input = etOption5.getText().toString();
        if (!TextUtils.isEmpty(option1Input)) {
            list.get(j).setUser_input(option1Input);
            list.get(j).setUser_ans("1");
        }
        if (!TextUtils.isEmpty(option2Input)) {
            list.get(j).setUser_input(option2Input);
            list.get(j).setUser_ans("2");
        }
        if (!TextUtils.isEmpty(option3Input)) {
            list.get(j).setUser_input(option3Input);
            list.get(j).setUser_ans("3");
        }
        if (!TextUtils.isEmpty(option4Input)) {
            list.get(j).setUser_input(option4Input);
            list.get(j).setUser_ans("4");
        }
        if (!TextUtils.isEmpty(option5Input)) {
            list.get(j).setUser_input(option5Input);
            list.get(j).setUser_ans("5");
        }
    }

    private void noQuizData() {
        new AlertDialog.Builder(SurveyActivity.this)
                .setTitle("No Survey")
                .setMessage("Not Available Survey for you.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })


                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(SurveyActivity.this);
    }

    private void clear() {
        tvoption1.setTextColor(Color.BLACK);
        tvoption2.setTextColor(Color.BLACK);
        tvoption3.setTextColor(Color.BLACK);
        tvoption4.setTextColor(Color.BLACK);
        tvoption5.setTextColor(Color.BLACK);
        llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
        llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
        llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
        llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
        llOption5.setBackgroundResource(R.drawable.dashboard_border_white);

        etOption1.getText().clear();
        etOption2.getText().clear();
        etOption3.getText().clear();
        etOption4.getText().clear();
        etOption5.getText().clear();

    }

    private void visibleEmoji(boolean isVisible) {
        SurveyDataUtil surveyDataUtil = list.get(j);
        if (isVisible) {
            option_emo_img1.setVisibility(View.VISIBLE);
            option_emo_img2.setVisibility(View.VISIBLE);
            option_emo_img3.setVisibility(View.VISIBLE);
            option_emo_img4.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(surveyDataUtil.getOption5()))
                option_emo_img5.setVisibility(View.VISIBLE);
            setEmoji(surveyDataUtil);
        } else {
            option_emo_img1.setVisibility(View.GONE);
            option_emo_img2.setVisibility(View.GONE);
            option_emo_img3.setVisibility(View.GONE);
            option_emo_img4.setVisibility(View.GONE);
            option_emo_img5.setVisibility(View.GONE);
        }
    }

    private void setEmoji(SurveyDataUtil surveyDataUtil) {
        if (surveyDataUtil.getOption1_emoji() != 0) {
            option_emo_img1.setVisibility(View.GONE);
            tvEmoji1.setText(getEmojiByUnicode(surveyDataUtil.getOption2_emoji()));
        } else {
            setOfflineEmoji(option_emo_img1, surveyDataUtil.getOption1());
        }
        if (surveyDataUtil.getOption2_emoji() != 0) {
            option_emo_img2.setVisibility(View.GONE);
            tvEmoji2.setText(getEmojiByUnicode(surveyDataUtil.getOption2_emoji()));
        } else {
            setOfflineEmoji(option_emo_img2, surveyDataUtil.getOption2());
        }
        if (surveyDataUtil.getOption3_emoji() != 0) {
            option_emo_img3.setVisibility(View.GONE);
            tvEmoji3.setText(getEmojiByUnicode(surveyDataUtil.getOption3_emoji()));
        } else {
            setOfflineEmoji(option_emo_img3, surveyDataUtil.getOption3());
        }
        if (surveyDataUtil.getOption4_emoji() != 0) {
            option_emo_img4.setVisibility(View.GONE);
            tvEmoji4.setText(getEmojiByUnicode(surveyDataUtil.getOption4_emoji()));
        } else {
            setOfflineEmoji(option_emo_img4, surveyDataUtil.getOption4());
        }
        if (surveyDataUtil.getOption5_emoji() != 0) {
            option_emo_img5.setVisibility(View.GONE);
            tvEmoji5.setText(getEmojiByUnicode(surveyDataUtil.getOption5_emoji()));
        } else {
            setOfflineEmoji(option_emo_img5, surveyDataUtil.getOption4());
        }
    }

    private void setOfflineEmoji(ImageView imageView, String text) {
        switch (text.toLowerCase()) {
            case "poor":
                imageView.setImageResource(R.drawable.poor_icon);
                break;
            case "Satisfactory":
                imageView.setImageResource(R.drawable.normal_icon);
                break;
            case "Good":
                imageView.setImageResource(R.drawable.good_icon);
                break;
            case "Very Good":
                imageView.setImageResource(R.drawable.very_good_icon);
                break;
            case "Excellent":
                imageView.setImageResource(R.drawable.excellent);
                break;
        }
    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    private void show(final int i) {
        setTextIntoEditOptions(i);
        if (i == 0)
            prev_btn.setVisibility(View.GONE);
        else
            prev_btn.setVisibility(View.VISIBLE);
        rlFeedback.setVisibility(View.GONE);
        tvoption1.setText("");
        tvoption2.setText("");
        tvoption3.setText("");
        tvoption4.setText("");
        tvoption5.setText("");
        if (j >= list.size() - 1) {
            next_btn.setText("Done");
        } else
            next_btn.setText("NEXT >");
        count_txt.setText((i + 1) + "/" + list.size());
        question_no_txt.setText("Q" + (i + 1) + ". ");
        tvquestion_description.setVisibility(View.VISIBLE);
        tvquestion_description.setText(Html.fromHtml(list.get(i).getQuestion_description()));
        if (!list.get(i).getOption1().equalsIgnoreCase("")) {
            if (!list.get(i).getOption1_type().equalsIgnoreCase("2")) {
                tvoption1.setText(Html.fromHtml(list.get(i).getOption1()));
                tvoption1.setVisibility(View.VISIBLE);
                llOption1Text.setVisibility(View.VISIBLE);
                etOption1.setVisibility(View.GONE);
            } else {
                llOption1Text.setVisibility(View.GONE);
                etOption1.setVisibility(View.VISIBLE);
                setPrefilledInput(list.get(i), etOption1, "1");
            }
            l1.setVisibility(View.VISIBLE);

        }
        if (list.get(i).getQuestion_type().equalsIgnoreCase("EMOJI")) {
            visibleEmoji(true);
        } else {
            visibleEmoji(false);
        }
        if (!list.get(i).getOption2().equalsIgnoreCase("")) {
            if (!list.get(i).getOption2_type().equalsIgnoreCase("2")) {
                tvoption2.setText(Html.fromHtml(list.get(i).getOption2()));
                tvoption2.setVisibility(View.VISIBLE);
                llOption2Text.setVisibility(View.VISIBLE);
                etOption2.setVisibility(View.GONE);
            } else {
                llOption2Text.setVisibility(View.GONE);
                etOption2.setVisibility(View.VISIBLE);
                setPrefilledInput(list.get(i), etOption2, "2");
            }
            l2.setVisibility(View.VISIBLE);

        } else {
            tvoption2.setVisibility(View.GONE);
            l2.setVisibility(View.GONE);
        }
        if (!list.get(i).getOption3().equalsIgnoreCase("")) {
            if (!list.get(i).getOption3_type().equalsIgnoreCase("2")) {
                tvoption3.setText(Html.fromHtml(list.get(i).getOption3()));
                tvoption3.setVisibility(View.VISIBLE);
                llOption3Text.setVisibility(View.VISIBLE);
                etOption3.setVisibility(View.GONE);
            } else {
                llOption3Text.setVisibility(View.GONE);
                etOption3.setVisibility(View.VISIBLE);
                setPrefilledInput(list.get(i), etOption3, "3");
            }
            l3.setVisibility(View.VISIBLE);
        } else {
            l3.setVisibility(View.GONE);
            tvoption3.setVisibility(View.GONE);
        }
        if (!list.get(i).getOption4().equalsIgnoreCase("")) {
            if (!list.get(i).getOption4_type().equalsIgnoreCase("2")) {
                tvoption4.setText(Html.fromHtml(list.get(i).getOption4()));
                tvoption4.setVisibility(View.VISIBLE);
                llOption4Text.setVisibility(View.VISIBLE);
                etOption4.setVisibility(View.GONE);
            } else {
                llOption4Text.setVisibility(View.GONE);
                etOption4.setVisibility(View.VISIBLE);
                setPrefilledInput(list.get(i), etOption4, "4");
            }
            l4.setVisibility(View.VISIBLE);
        } else {
            tvoption4.setVisibility(View.GONE);
            l4.setVisibility(View.GONE);
        }
        if (!list.get(i).getOption5().equalsIgnoreCase("")) {
            if (!list.get(i).getOption5_type().equalsIgnoreCase("2")) {
                tvoption5.setText(Html.fromHtml(list.get(i).getOption5()));
                tvoption5.setVisibility(View.VISIBLE);
                llOption5Text.setVisibility(View.VISIBLE);
                etOption5.setVisibility(View.GONE);
            } else {
                llOption5Text.setVisibility(View.GONE);
                etOption5.setVisibility(View.VISIBLE);
                setPrefilledInput(list.get(i), etOption5, "5");
            }
            l5.setVisibility(View.VISIBLE);
        } else {
            tvoption5.setVisibility(View.GONE);
            l5.setVisibility(View.GONE);
        }
        if (list.get(i).getUser_ans().equalsIgnoreCase("1")) {
            tvoption1.setTextColor(Color.WHITE);
            tvoption2.setTextColor(Color.BLACK);
            tvoption3.setTextColor(Color.BLACK);
            tvoption4.setTextColor(Color.BLACK);
            tvoption5.setTextColor(Color.BLACK);
            llOption1.setBackgroundResource(R.drawable.survey_selected_option);
            llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
        } else if (list.get(i).getUser_ans().equalsIgnoreCase("2")) {
            tvoption1.setTextColor(Color.BLACK);
            tvoption2.setTextColor(Color.WHITE);
            tvoption3.setTextColor(Color.BLACK);
            tvoption4.setTextColor(Color.BLACK);
            tvoption5.setTextColor(Color.BLACK);
            llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption2.setBackgroundResource(R.drawable.survey_selected_option);
            llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
        } else if (list.get(i).getUser_ans().equalsIgnoreCase("3")) {
            tvoption1.setTextColor(Color.BLACK);
            tvoption2.setTextColor(Color.BLACK);
            tvoption3.setTextColor(Color.WHITE);
            tvoption4.setTextColor(Color.BLACK);
            tvoption5.setTextColor(Color.BLACK);
            llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption3.setBackgroundResource(R.drawable.survey_selected_option);
            llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
        } else if (list.get(i).getUser_ans().equalsIgnoreCase("4")) {
            tvoption1.setTextColor(Color.BLACK);
            tvoption2.setTextColor(Color.BLACK);
            tvoption3.setTextColor(Color.BLACK);
            tvoption4.setTextColor(Color.WHITE);
            llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption4.setBackgroundResource(R.drawable.survey_selected_option);
            llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
        } else if (list.get(i).getUser_ans().equalsIgnoreCase("5")) {
            tvoption1.setTextColor(Color.BLACK);
            tvoption2.setTextColor(Color.BLACK);
            tvoption3.setTextColor(Color.BLACK);
            tvoption4.setTextColor(Color.BLACK);
            tvoption5.setTextColor(Color.WHITE);
            llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
            llOption5.setBackgroundResource(R.drawable.survey_selected_option);
        }
        option_emo_img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("1");

                tvoption1.setTextColor(Color.WHITE);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.BLACK);
                llOption1.setBackgroundResource(R.drawable.survey_selected_option);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
                /*if (list.get(i).getOption1_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/
            }
        });
        tvoption1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("1");

                tvoption1.setTextColor(Color.WHITE);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.BLACK);
                llOption1.setBackgroundResource(R.drawable.survey_selected_option);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
                /*if (list.get(i).getOption1_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/
            }
        });
        option_emo_img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("2");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.WHITE);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.BLACK);
                llOption2.setBackgroundResource(R.drawable.survey_selected_option);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
               /* if (list.get(i).getOption2_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/
            }
        });
        tvoption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("2");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.WHITE);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.BLACK);
                llOption2.setBackgroundResource(R.drawable.survey_selected_option);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
                /*if (list.get(i).getOption2_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/
            }
        });
        option_emo_img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.get(i).setUser_ans("3");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.WHITE);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.BLACK);
                llOption3.setBackgroundResource(R.drawable.survey_selected_option);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
                /*if (list.get(i).getOption3_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/
            }
        });
        tvoption3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.get(i).setUser_ans("3");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.WHITE);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.BLACK);
                llOption3.setBackgroundResource(R.drawable.survey_selected_option);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
                /*if (list.get(i).getOption3_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/
            }
        });
        option_emo_img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("4");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.WHITE);
                tvoption5.setTextColor(Color.BLACK);
                llOption4.setBackgroundResource(R.drawable.survey_selected_option);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
                /*if (list.get(i).getOption4_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/

            }
        });
        option_emo_img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("5");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.WHITE);
                llOption4.setBackgroundResource(R.drawable.survey_selected_option);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.survey_selected_option);
                /*if (list.get(i).getOption4_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/

            }
        });
        tvoption4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("4");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.WHITE);
                tvoption5.setTextColor(Color.BLACK);
                llOption4.setBackgroundResource(R.drawable.survey_selected_option);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.dashboard_border_white);
                /*if (list.get(i).getOption4_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/

            }
        });
        tvoption5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                list.get(i).setUser_ans("5");
                tvoption1.setTextColor(Color.BLACK);
                tvoption2.setTextColor(Color.BLACK);
                tvoption3.setTextColor(Color.BLACK);
                tvoption4.setTextColor(Color.BLACK);
                tvoption5.setTextColor(Color.WHITE);
                llOption4.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption2.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption3.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption1.setBackgroundResource(R.drawable.dashboard_border_white);
                llOption5.setBackgroundResource(R.drawable.survey_selected_option);
                /*if (list.get(i).getOption4_type().equalsIgnoreCase("2")) {
                    showInputDialog(list.get(i).getUser_input(), i);
                }*/

            }
        });

    }

    private void setPrefilledInput(SurveyDataUtil surveyDataUtil, EditText etInput, String pos) {
        if (!TextUtils.isEmpty(surveyDataUtil.getUser_ans()) && surveyDataUtil.getUser_ans().equals(pos) && !TextUtils.isEmpty(surveyDataUtil.getUser_input())) {
            etInput.setText(surveyDataUtil.getUser_input());
        } else
            etInput.getText().clear();
    }

    private void SubmitData() {
        JSONArray jsonArray = new JSONArray();
        int correct = 0;
        for (int i = 0; i < list.size(); i++) {
            JSONObject object = new JSONObject();
            try {
                object.put("question_id", list.get(i).getQuestion_id());
                object.put("answer_key", list.get(i).getUser_ans());
                object.put("answer", list.get(i).getUser_input());
                object.put("time_taken", "1");
                if (!list.get(i).getUser_ans().equalsIgnoreCase("") || !list.get(i).getUser_input().equalsIgnoreCase(""))
                    correct++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(object);
        }
        if (WebServices.isNetworkAvailable(SurveyActivity.this)) {
            MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(SurveyActivity.this);
            mixPanelManager.completeSurvey(SurveyActivity.this, WebServices.mLoginUtility.getEmail(), mMeterialUtility.getTitle(), jsonArray.toString(), mMeterialUtility.getMaterial_id());

            String coinsCollected = null;
            try {
                coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility, 100, redeem));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
            /*tvAwardedCoins.setText("You have been awarded with " + coinsCollected + " Bonus points!");
            tvAwardedCoins.setVisibility(View.VISIBLE);*/
                /*showGoldGif(coinsCollected);*/
                Toast.makeText(SurveyActivity.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
            }

            new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, list.size() + "", list.size() + "", coinsCollected);

            new WebServices().addSubmitResponse(dataBase, mMeterialUtility, "0", correct + "", "", jsonArray.toString(), null, "", WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());
            if (WebServices.isNetworkAvailable(SurveyActivity.this))
                new SubmitData(SurveyActivity.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, dataBase).execute(correct + "", "0", jsonArray.toString(), "100");

//            new SubmitData(mMeterialUtility.getCourse_id(), mMeterialUtility.getMaterial_id()).execute(jsonArray.toString(), correct + "");
        } else {
//            dataBase.addSurveyresponseData(WebServices.mLoginUtility.getUser_id(), getIntent().getStringExtra("survey_id"),getIntent().getStringExtra("course_id"),jsonArray.toString());
            Toast.makeText(SurveyActivity.this, "Please connect to the internet for submit to the response.", Toast.LENGTH_LONG).show();
        }
    }

    private void showGoldGif(String coinsCollected) {
    }

    protected void showInputDialog(String msg, final int pos) {
        // Create custom dialog object
       /* final Dialog dialog = new Dialog(SurveyActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.input_dialog);*/
        // Set dialog title

//        final EditText editText = (EditText) dialog.findViewById(R.id.edit_text);
        rlFeedback.setVisibility(View.VISIBLE);
        final EditText editText = (EditText) findViewById(R.id.edit_text);
        if (!TextUtils.isEmpty(list.get(pos).getUser_input()))
            editText.setText(list.get(pos).getUser_input());

        // setup a dialog window
        if (!msg.equalsIgnoreCase(""))
            editText.setText(msg);
        Button button = (Button) findViewById(R.id.ok_button);
//        dialog.setCancelable(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.get(pos).setUser_input(editText.getText().toString());
                Toast.makeText(SurveyActivity.this, "Your response is submitted!", Toast.LENGTH_SHORT).show();
                editText.setText("");
                rlFeedback.setVisibility(View.GONE);
                if (next_btn.getText().toString().equalsIgnoreCase("Done")) {
                    Intent intent = new Intent(SurveyActivity.this, SurveyResultActivity.class);
                    intent.putExtra("meterial_utility", mMeterialUtility);
                    intent.putExtra("course_id", mMeterialUtility.getCourse_id());
                    intent.putExtra("survey_id", mMeterialUtility.getMaterial_id());
                    startActivity(intent);

                    SubmitData();
                    finish();
                    return;
                }
                if (list.size() > j) {

                    j++;
                    clear();
                    show(j);
                }
               /* dialog.dismiss();*/
            }
        });
//        dialog.show();
    }

    @Override
    public void onBackPressed() {

    }

    /**
     * Created by abcd on 5/21/2016.
     */
   /* public class SubmitData extends AsyncTask<String, Void, Boolean> {
        WebServices webServices;
        private String mAssign_course_id;
        private String mSurvey_id;

        public SubmitData(String assign_course_id, String survey_id) {
            webServices = new WebServices();
            mAssign_course_id = assign_course_id;
            mSurvey_id = survey_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String quizresponse = "";

            if (params.length != 0) {
                try {
                    byte ptext[] = params[0].getBytes();
                    quizresponse = new String(ptext, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    quizresponse = params[0];
                    e.printStackTrace();
                }
                System.out.println("===params====" + params[0]);
            }
            int result = (Integer.parseInt(params[1]) * 100) / list.size();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String formattedDate = df.format(c.getTime());
// Building post parameters, key and value pair
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("assign_course_id", com.centum.home.HomeActivity.ASSIGNEDCOURSEID));
            nameValuePair.add(new BasicNameValuePair("material_id", mSurvey_id));
            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("finish_time", formattedDate));
            nameValuePair.add(new BasicNameValuePair("correct_question", params[1]));
            nameValuePair.add(new BasicNameValuePair("incorrect_question", "0"));
            nameValuePair.add(new BasicNameValuePair("question_response", quizresponse));
            nameValuePair.add(new BasicNameValuePair("response_type", "Survey"));
            nameValuePair.add(new BasicNameValuePair("result", result + ""));
            nameValuePair.add(new BasicNameValuePair("question_response", quizresponse));
            nameValuePair.add(new BasicNameValuePair("start_time", HomeActivity.QUIZSTARTTIME));
            nameValuePair.add(new BasicNameValuePair("taken_device", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("time_taken", diffDate(HomeActivity.QUIZSTARTTIME, formattedDate)));


            String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_RESPONSE);
            if (!webServices.isValid(resp)) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                Toast.makeText(SurveyActivity.this, getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
                DataBase.getInstance(SurveyActivity.this).updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putBoolean("islogin", false);
                editor.clear();
                editor.commit();
                Intent intent = new Intent(SurveyActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("sms_url", "");
                startActivity(intent);
                finish();
                return;
            }

        }
    }*/
    public static String diffDate(String dateStart, String dateStop) {
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.println(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            if (diffSeconds == 0) diffSeconds = 1;
            return diffSeconds + "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "1";
    }
}
