package com.chaptervitamins.quiz.model;

/**
 * Created by test on 02-04-2018.
 */

public class Vertical {
    String name="";
    int totalNumQuiz =0;
    String selectedVertical="";
    int correctScore=0;
    int skipQuiz=0,attQuiz=0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalNumQuiz() {
        return totalNumQuiz;
    }

    public void setTotalNumQuiz(int totalNumQuiz) {
        this.totalNumQuiz = totalNumQuiz;
    }

    public String getSelectedVertical() {
        return selectedVertical;
    }

    public void setSelectedVertical(String selectedVertical) {
        this.selectedVertical = selectedVertical;
    }

    public int getCorrectScore() {
        return correctScore;
    }

    public void setCorrectScore(int correctScore) {
        this.correctScore = correctScore;
    }

    public int getSkipQuiz() {
        return skipQuiz;
    }

    public void setSkipQuiz(int skipQuiz) {
        this.skipQuiz = skipQuiz;
    }

    public int getAttQuiz() {
        return attQuiz;
    }

    public void setAttQuiz(int attQuiz) {
        this.attQuiz = attQuiz;
    }
}
