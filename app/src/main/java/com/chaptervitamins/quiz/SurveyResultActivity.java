package com.chaptervitamins.quiz;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.play_video.FlashCardResult;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifTextView;

public class SurveyResultActivity extends BaseActivity implements View.OnClickListener,RatingListener {
    private Button btnPrevious, btnNext;
    private String courseId = "", surveyId = "";
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private GifTextView goldGif;
    private MeterialUtility mMeterialUtility;
    private boolean isNextButtonClicked;
    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_survey_result);
        courseId = getIntent().getStringExtra("course_id");
        surveyId = getIntent().getStringExtra("survey_id");
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        toolbar_title.setText("Survey Completed");
        ImageView back = (ImageView) findViewById(R.id.back);

      /*  if (WebServices.isNetworkAvailable(this)) {
            FragmentManager fm = getSupportFragmentManager();
            CustomDialog custom = new CustomDialog();
            custom.setParamCustomDialog(this, surveyId,false);
            custom.show(fm, "");
        } else {
            Toast.makeText(this,getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }*/
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String redeem = "", noOfCoins = "";
                    CoinsAllocatedModel coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(courseId, surveyId);
                    if (coinsAllocatedModel == null) {
                        coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(surveyId);
                    }
                    if (coinsAllocatedModel != null) {
                        redeem = coinsAllocatedModel.getRedeem();
                    } else
                        coinsAllocatedModel = new CoinsAllocatedModel();

                    final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility,100, redeem));
                    if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showGoldGif();
                                Toast.makeText(SurveyResultActivity.this, "You have awarded " + coinsCollected + " coins", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
            setFlowingCourse();
            getModuleData();
            setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.tv_back_to_survey).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(SurveyResultActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                   showRatingDialog(true);
                } else {
                    finish();
                }
            }
        });
    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(mMeterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, mMeterialUtility.getModule_id());
    }

    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }

    private void setFlowingCourse() {
        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(surveyId, true);
        position = FlowingCourseUtils.getPositionOfMeterial(surveyId, meterialUtilityArrayList);
        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(SurveyResultActivity.this,position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(SurveyResultActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                showRatingDialog(true);
            } else {
                finish();
            }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(SurveyResultActivity.this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(SurveyResultActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(SurveyResultActivity.this, meterialUtilityArrayList, position, false);
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating())&& mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes")&&WebServices.isNetworkAvailable(SurveyResultActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseMaterial(SurveyResultActivity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(SurveyResultActivity.this) &&
                        mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(SurveyResultActivity.this, mMeterialUtility, true);
                break;
        }
    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(SurveyResultActivity.this, mMeterialUtility.getMaterial_id(), wannaFinish,SurveyResultActivity.this);
        custom.show(fm, "");
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(SurveyResultActivity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }
}
