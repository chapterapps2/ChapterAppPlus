package com.chaptervitamins.quiz;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Deejay on 4/25/2016.
 */
public class Data_util implements Serializable {
    int skipQuiz=0,attQuiz=0;
    String assign_question_id = "0",option1_real_pos,option2_real_pos,option3_real_pos,option4_real_pos,
    realCorrectOption;
    String question_bank_id = "0";
    String course_id = "0";
    String module_id = "0";
    String chapter_id = "0";
    String question_name = "";
    String question_description = "";
    String question_description_url = "";
    String question_type = "";
    String question_hint = "";
    String error_message = "";
    String explanation = "";
    String is_question_image = "0";
    String option1 = "";
    String option1_url = "";
    String option2_url = "";
    String option3_url = "";
    String option4_url = "";
    String is_option1_image = "0";
    String option1_marks = "0";
    String option2 = "";
    String is_option2_image = "0";
    String option2_marks = "0";
    String option3 = "";
    String is_option3_image = "0";
    String option3_marks = "0";
    String option4 = "";
    String is_option4_image = "0";
    String option4_marks = "0";
    String option5 = "";
    String is_option5_image = "0";
    String option5_marks = "0";
    String option6 = "";
    String is_option6_image = "0";
    String option6_marks = "0";
    String option7 = "";
    String is_option7_image = "0";
    String option7_marks = "0";
    String option8 = "";
    String is_option8_image = "0";
    String option8_marks = "0";
    String option9 = "";
    String is_option9_image = "0";
    String option9_marks = "0";
    String option10 = "";
    String is_option10_image = "0";
    String option10_marks = "0";
    String correct_option = "0";
    String marks = "0";
    String user_id = "0",response,vertical="";
    private boolean isCorrect;
    String group_id,time_taken,user_input,option1_type,option2_type,option3_type,option4_type,option_type;
    int  height;
    ArrayList<String> user_ans = new ArrayList<>();
    //    String user_ans="";
    String correct_option_type;
    int opt1clicked = 0;
    int opt2clicked = 0;
    int opt3clicked = 0;
    int opt4clicked = 0;
    int opt5clicked = 0;
    int opt6clicked = 0;
    int opt7clicked = 0;
    int opt8clicked = 0;
    int opt9clicked = 0;
    int opt10clicked = 0;

    public int getOpt10clicked() {
        return opt10clicked;
    }

    public void setOpt10clicked(int opt10clicked) {
        this.opt10clicked = opt10clicked;
    }

    public int getOpt2clicked() {
        return opt2clicked;
    }

    public void setOpt2clicked(int opt2clicked) {
        this.opt2clicked = opt2clicked;
    }

    public int getOpt1clicked() {
        return opt1clicked;
    }

    public void setOpt1clicked(int opt1clicked) {
        this.opt1clicked = opt1clicked;
    }

    public int getOpt3clicked() {
        return opt3clicked;
    }

    public void setOpt3clicked(int opt3clicked) {
        this.opt3clicked = opt3clicked;
    }

    public int getOpt4clicked() {
        return opt4clicked;
    }

    public void setOpt4clicked(int opt4clicked) {
        this.opt4clicked = opt4clicked;
    }

    public int getOpt5clicked() {
        return opt5clicked;
    }

    public void setOpt5clicked(int opt5clicked) {
        this.opt5clicked = opt5clicked;
    }

    public int getOpt6clicked() {
        return opt6clicked;
    }

    public void setOpt6clicked(int opt6clicked) {
        this.opt6clicked = opt6clicked;
    }

    public int getOpt7clicked() {
        return opt7clicked;
    }

    public void setOpt7clicked(int opt7clicked) {
        this.opt7clicked = opt7clicked;
    }

    public int getOpt8clicked() {
        return opt8clicked;
    }

    public void setOpt8clicked(int opt8clicked) {
        this.opt8clicked = opt8clicked;
    }

    public int getOpt9clicked() {
        return opt9clicked;
    }

    public void setOpt9clicked(int opt9clicked) {
        this.opt9clicked = opt9clicked;
    }
//
//    public boolean isOpt1clicked() {
//        return opt1clicked;
//    }
//
//    public void setOpt1clicked(boolean opt1clicked) {
//        this.opt1clicked = opt1clicked;
//    }
//
//    public boolean isOpt2clicked() {
//        return opt2clicked;
//    }
//
//    public void setOpt2clicked(boolean opt2clicked) {
//        this.opt2clicked = opt2clicked;
//    }
//
//    public boolean isOpt3clicked() {
//        return opt3clicked;
//    }
//
//    public void setOpt3clicked(boolean opt3clicked) {
//        this.opt3clicked = opt3clicked;
//    }
//
//    public boolean isOpt4clicked() {
//        return opt4clicked;
//    }
//
//    public void setOpt4clicked(boolean opt4clicked) {
//        this.opt4clicked = opt4clicked;
//    }
//
//    public boolean isOpt5clicked() {
//        return opt5clicked;
//    }
//
//    public void setOpt5clicked(boolean opt5clicked) {
//        this.opt5clicked = opt5clicked;
//    }
//
//    public boolean isOpt6clicked() {
//        return opt6clicked;
//    }
//
//    public void setOpt6clicked(boolean opt6clicked) {
//        this.opt6clicked = opt6clicked;
//    }
//
//    public boolean isOpt7clicked() {
//        return opt7clicked;
//    }
//
//    public void setOpt7clicked(boolean opt7clicked) {
//        this.opt7clicked = opt7clicked;
//    }
//
//    public boolean isOpt8clicked() {
//        return opt8clicked;
//    }
//
//    public void setOpt8clicked(boolean opt8clicked) {
//        this.opt8clicked = opt8clicked;
//    }
//
//    public boolean isOpt9clicked() {
//        return opt9clicked;
//    }
//
//    public void setOpt9clicked(boolean opt9clicked) {
//        this.opt9clicked = opt9clicked;
//    }
//
//    public boolean isOpt10clicked() {
//        return opt10clicked;
//    }
//
//    public void setOpt10clicked(boolean opt10clicked) {
//        this.opt10clicked = opt10clicked;
//    }
//
//    boolean opt10clicked = false;


    public String getAssign_question_id() {
        return assign_question_id;
    }

    public void setAssign_question_id(String assign_question_id) {
        this.assign_question_id = assign_question_id;
    }

    public String getQuestion_bank_id() {
        return question_bank_id;
    }

    public void setQuestion_bank_id(String question_bank_id) {
        this.question_bank_id = question_bank_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }

    public String getQuestion_type() {
        if (!TextUtils.isEmpty(question_type))
            return question_type;
        else
            return "SINGLE";
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public String getQuestion_description() {
        return question_description;
    }

    public void setQuestion_description(String question_description) {
        this.question_description = question_description;
    }

    public String getQuestion_hint() {
        return question_hint;
    }

    public void setQuestion_hint(String question_hint) {
        this.question_hint = question_hint;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getIs_question_image() {
        return is_question_image;
    }

    public void setIs_question_image(String is_question_image) {
        this.is_question_image = is_question_image;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getIs_option1_image() {
        return is_option1_image;
    }

    public void setIs_option1_image(String is_option1_image) {
        this.is_option1_image = is_option1_image;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption1_marks() {
        return option1_marks;
    }

    public void setOption1_marks(String option1_marks) {
        this.option1_marks = option1_marks;
    }

    public String getIs_option2_image() {
        return is_option2_image;
    }

    public void setIs_option2_image(String is_option2_image) {
        this.is_option2_image = is_option2_image;
    }

    public String getOption2_marks() {
        return option2_marks;
    }

    public void setOption2_marks(String option2_marks) {
        this.option2_marks = option2_marks;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getIs_option3_image() {
        return is_option3_image;
    }

    public void setIs_option3_image(String is_option3_image) {
        this.is_option3_image = is_option3_image;
    }

    public String getOption3_marks() {
        return option3_marks;
    }

    public void setOption3_marks(String option3_marks) {
        this.option3_marks = option3_marks;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getOption4_marks() {
        return option4_marks;
    }

    public void setOption4_marks(String option4_marks) {
        this.option4_marks = option4_marks;
    }

    public String getIs_option4_image() {
        return is_option4_image;
    }

    public void setIs_option4_image(String is_option4_image) {
        this.is_option4_image = is_option4_image;
    }

    public String getOption5() {
        return option5;
    }

    public void setOption5(String option5) {
        this.option5 = option5;
    }

    public String getIs_option5_image() {
        return is_option5_image;
    }

    public void setIs_option5_image(String is_option5_image) {
        this.is_option5_image = is_option5_image;
    }

    public String getOption5_marks() {
        return option5_marks;
    }

    public void setOption5_marks(String option5_marks) {
        this.option5_marks = option5_marks;
    }

    public String getOption6() {
        return option6;
    }

    public void setOption6(String option6) {
        this.option6 = option6;
    }

    public String getIs_option6_image() {
        return is_option6_image;
    }

    public void setIs_option6_image(String is_option6_image) {
        this.is_option6_image = is_option6_image;
    }

    public String getOption6_marks() {
        return option6_marks;
    }

    public void setOption6_marks(String option6_marks) {
        this.option6_marks = option6_marks;
    }

    public String getOption7() {
        return option7;
    }

    public void setOption7(String option7) {
        this.option7 = option7;
    }

    public String getIs_option7_image() {
        return is_option7_image;
    }

    public void setIs_option7_image(String is_option7_image) {
        this.is_option7_image = is_option7_image;
    }

    public String getOption7_marks() {
        return option7_marks;
    }

    public void setOption7_marks(String option7_marks) {
        this.option7_marks = option7_marks;
    }

    public String getOption8() {
        return option8;
    }

    public void setOption8(String option8) {
        this.option8 = option8;
    }

    public String getIs_option8_image() {
        return is_option8_image;
    }

    public void setIs_option8_image(String is_option8_image) {
        this.is_option8_image = is_option8_image;
    }

    public String getOption8_marks() {
        return option8_marks;
    }

    public void setOption8_marks(String option8_marks) {
        this.option8_marks = option8_marks;
    }

    public String getOption9() {
        return option9;
    }

    public void setOption9(String option9) {
        this.option9 = option9;
    }

    public String getIs_option9_image() {
        return is_option9_image;
    }

    public void setIs_option9_image(String is_option9_image) {
        this.is_option9_image = is_option9_image;
    }

    public String getOption9_marks() {
        return option9_marks;
    }

    public void setOption9_marks(String option9_marks) {
        this.option9_marks = option9_marks;
    }

    public String getOption10() {
        return option10;
    }

    public void setOption10(String option10) {
        this.option10 = option10;
    }

    public String getIs_option10_image() {
        return is_option10_image;
    }

    public void setIs_option10_image(String is_option10_image) {
        this.is_option10_image = is_option10_image;
    }

    public String getOption10_marks() {
        return option10_marks;
    }

    public void setOption10_marks(String option10_marks) {
        this.option10_marks = option10_marks;
    }

    public String getCorrect_option() {
        return correct_option;
    }

    public void setCorrect_option(String correct_option) {
        this.correct_option = correct_option;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ArrayList<String> getUser_ans() {
        return user_ans;
    }

    public void setUser_ans(ArrayList<String> user_ans) {
        this.user_ans.clear();
        this.user_ans.addAll(user_ans);
    }

    public String getCorrect_option_type() {
        return correct_option_type;
    }

    public void setCorrect_option_type(String correct_option_type) {
        this.correct_option_type = correct_option_type;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getTime_taken() {
        return time_taken;
    }

    public void setTime_taken(String time_taken) {
        this.time_taken = time_taken;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public String getUser_input() {
        return user_input;
    }

    public void setUser_input(String user_input) {
        this.user_input = user_input;
    }

    public String getOption1_type() {
        return option1_type;
    }

    public void setOption1_type(String option1_type) {
        this.option1_type = option1_type;
    }

    public String getOption2_type() {
        return option2_type;
    }

    public void setOption2_type(String option2_type) {
        this.option2_type = option2_type;
    }

    public String getOption3_type() {
        return option3_type;
    }

    public void setOption3_type(String option3_type) {
        this.option3_type = option3_type;
    }

    public String getOption4_type() {
        return option4_type;
    }

    public void setOption4_type(String option4_type) {
        this.option4_type = option4_type;
    }

    public String getOption_type() {
        return option_type;
    }

    public void setOption_type(String option_type) {
        this.option_type = option_type;
    }

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    public int getSkipQuiz() {
        return skipQuiz;
    }

    public void setSkipQuiz(int skipQuiz) {
        this.skipQuiz = skipQuiz;
    }

    public int getAttQuiz() {
        return attQuiz;
    }

    public void setAttQuiz(int attQuiz) {
        this.attQuiz = attQuiz;
    }

    public String getOption1_url() {
        return option1_url;
    }

    public void setOption1_url(String option1_url) {
        this.option1_url = option1_url;
    }

    public String getOption2_url() {
        return option2_url;
    }

    public void setOption2_url(String option2_url) {
        this.option2_url = option2_url;
    }

    public String getOption3_url() {
        return option3_url;
    }

    public void setOption3_url(String option3_url) {
        this.option3_url = option3_url;
    }

    public String getOption4_url() {
        return option4_url;
    }

    public void setOption4_url(String option4_url) {
        this.option4_url = option4_url;
    }

    public String getQuestion_description_url() {
        return question_description_url;
    }

    public void setQuestion_description_url(String question_description_url) {
        this.question_description_url = question_description_url;
    }

    public String getOption1_real_pos() {
        return option1_real_pos;
    }

    public void setOption1_real_pos(String option1_real_pos) {
        this.option1_real_pos = option1_real_pos;
    }

    public String getOption2_real_pos() {
        return option2_real_pos;
    }

    public void setOption2_real_pos(String option2_real_pos) {
        this.option2_real_pos = option2_real_pos;
    }

    public String getOption3_real_pos() {
        return option3_real_pos;
    }

    public void setOption3_real_pos(String option3_real_pos) {
        this.option3_real_pos = option3_real_pos;
    }

    public String getOption4_real_pos() {
        return option4_real_pos;
    }

    public void setOption4_real_pos(String option4_real_pos) {
        this.option4_real_pos = option4_real_pos;
    }

    public String getRealCorrectOption() {
        return realCorrectOption;
    }

    public void setRealCorrectOption(String realCorrectOption) {
        this.realCorrectOption = realCorrectOption;
    }
}
