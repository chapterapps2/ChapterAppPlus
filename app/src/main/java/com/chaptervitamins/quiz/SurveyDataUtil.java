package com.chaptervitamins.quiz;

/**
 * Created by Android on 6/17/2016.
 */
public class SurveyDataUtil {
    String question_description;
    String question_hint;
    String question_type;
    String error_message;
    String explanation;
    String added_on;
    String question_description_type;
    String question_id;
    String option1;
    String option1_type;
    String option2;
    String option2_type;
    String option3;
    String option3_type;
    String option4;
    String option5;
    String option4_type;
    String option5_type;
    String user_ans="";
    String user_input="";
    int option1_emoji,option2_emoji,option3_emoji,option4_emoji,option5_emoji;
    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption1_type() {
        return option1_type;
    }

    public void setOption1_type(String option1_type) {
        this.option1_type = option1_type;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption2_type() {
        return option2_type;
    }

    public void setOption2_type(String option2_type) {
        this.option2_type = option2_type;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption3_type() {
        return option3_type;
    }

    public void setOption3_type(String option3_type) {
        this.option3_type = option3_type;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getOption4_type() {
        return option4_type;
    }

    public void setOption4_type(String option4_type) {
        this.option4_type = option4_type;
    }

    public String getQuestion_description() {
        return question_description;
    }

    public void setQuestion_description(String question_description) {
        this.question_description = question_description;
    }

    public String getQuestion_hint() {
        return question_hint;
    }

    public void setQuestion_hint(String question_hint) {
        this.question_hint = question_hint;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getQuestion_description_type() {
        return question_description_type;
    }

    public void setQuestion_description_type(String question_description_type) {
        this.question_description_type = question_description_type;
    }

    public String getUser_ans() {
        return user_ans;
    }

    public void setUser_ans(String user_ans) {
        this.user_ans = user_ans;
    }

    public String getUser_input() {
        return user_input;
    }

    public void setUser_input(String user_input) {
        this.user_input = user_input;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public Integer getOption1_emoji() {
        return option1_emoji;
    }

    public void setOption1_emoji(Integer option1_emoji) {
        this.option1_emoji = option1_emoji;
    }

    public Integer getOption2_emoji() {
        return option2_emoji;
    }

    public void setOption2_emoji(Integer option2_emoji) {
        this.option2_emoji = option2_emoji;
    }

    public Integer getOption3_emoji() {
        return option3_emoji;
    }

    public void setOption3_emoji(Integer option3_emoji) {
        this.option3_emoji = option3_emoji;
    }

    public Integer getOption4_emoji() {
        return option4_emoji;
    }

    public void setOption4_emoji(Integer option4_emoji) {
        this.option4_emoji = option4_emoji;
    }

    public String getOption5() {
        return option5;
    }

    public void setOption5(String option5) {
        this.option5 = option5;
    }

    public String getOption5_type() {
        return option5_type;
    }

    public void setOption5_type(String option5_type) {
        this.option5_type = option5_type;
    }

    public void setOption1_emoji(int option1_emoji) {
        this.option1_emoji = option1_emoji;
    }

    public void setOption2_emoji(int option2_emoji) {
        this.option2_emoji = option2_emoji;
    }

    public void setOption3_emoji(int option3_emoji) {
        this.option3_emoji = option3_emoji;
    }

    public void setOption4_emoji(int option4_emoji) {
        this.option4_emoji = option4_emoji;
    }

    public int getOption5_emoji() {
        return option5_emoji;
    }

    public void setOption5_emoji(int option5_emoji) {
        this.option5_emoji = option5_emoji;
    }
}
