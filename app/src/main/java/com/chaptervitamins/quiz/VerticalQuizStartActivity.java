package com.chaptervitamins.quiz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.quiz.adapter.VerticalQuizAdapter;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VerticalQuizStartActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar_title)
    TextView toolBarTitle;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.start_ques_ll)
    LinearLayout startQuesLinearLayout;
    VerticalQuizAdapter verticalQuizAdapter;
    DataBase dataBase;
    String resp;
    WebServices webServices;
    Bundle bundle;
    ArrayList<Data_util> list;
    Handler handler = null;
    ProgressDialog dialog;
    private MeterialUtility meterialUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_quiz_start);
        ButterKnife.bind(this);
        initData();
        setDataIntoViews();
    }

    private void initData() {
        bundle = getIntent().getExtras();
        dataBase = DataBase.getInstance(this);
        webServices = new WebServices();
        list = new ArrayList<>();
    }

    private void setDataIntoViews() {
        if (bundle != null) {
             meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_Utility");
            final String resume = bundle.getString("resume");
            final String res_data = bundle.getString("res_data");
            toolBarTitle.setText(meterialUtility.getTitle());
            if (resume.equalsIgnoreCase("no")) {
                resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
            }
            dialog = ProgressDialog.show(this, "", "Please wait..");
            dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
            new Thread() {
                public void run() {
                    if (resume.equalsIgnoreCase("no")) {
                        if (webServices.isValid(resp)) {
                            WebServices.questionUtility = webServices.parseQuestionData(resp);
                            list = WebServices.questionUtility.getData_utils();
                            setVerticalCount();
                            handler.sendEmptyMessage(0);
                        } else {
                            handler.sendEmptyMessage(1);
                        }
                    } else {
                        WebServices.questionUtility = webServices.parseResumeQuestionData(dataBase, res_data);
                        list = WebServices.questionUtility.getData_utils();
                        handler.sendEmptyMessage(0);
                    }

                }
            }.start();

            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    if (dialog != null && !isFinishing()) dialog.dismiss();
                    if (msg.what == 1) {
                        Toast.makeText(VerticalQuizStartActivity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                        finish();
                    } else {

                        verticalQuizAdapter = new VerticalQuizAdapter(VerticalQuizStartActivity.this,
                                list,WebServices.questionUtility.getVerticalList());
                        recyclerView.setLayoutManager(new LinearLayoutManager(VerticalQuizStartActivity.this));
                        recyclerView.setItemViewCacheSize(list.size());
                        recyclerView.setAdapter(verticalQuizAdapter);
                    }
                }
            };
            startQuesLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(VerticalQuizStartActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("meterial_Utility", meterialUtility);
                   /* bundle.putString("sel_vertical", "");*/
                    bundle.putString("res_data", res_data);
                    bundle.putString("resume", resume);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(VerticalQuizStartActivity.this);
                    mixPanelManager.startquizflash(VerticalQuizStartActivity.this, WebServices.mLoginUtility.getEmail(), getIntent().getStringExtra("name"), "Quiz");
                    finish();
                }
            });

        }


    }

    private void setVerticalCount() {
        for(int i=0;i<WebServices.questionUtility.getVerticalList().size();i++){
            int verticalCount=0;
            for(int j=0;j<list.size();j++){
                if(WebServices.questionUtility.getVerticalList().get(i).getName().
                        equalsIgnoreCase(list.get(j).getVertical())){
                    verticalCount++;
                }
            }
            WebServices.questionUtility.getVerticalList().get(i).setTotalNumQuiz(verticalCount);
        }

    }
}
