package com.chaptervitamins.nomination;

/**
 * Created by Vijay Antil on 11-08-2017.
 */

public interface ActivityInterface {

    /**
     * Method that control the initialization of the view.
     */
    void inItUi();

    /**
     * Method that control the listener of the view.
     */
    void setListener();

    /**
     * Method that control the set data of the view.
     */
    void setData();

}
