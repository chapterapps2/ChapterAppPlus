package com.chaptervitamins.nomination.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chaptervitamins.R;
import com.chaptervitamins.nomination.adapters.TrainingAdapter;
import com.chaptervitamins.nomination.models.TrainingModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainingListFragment extends BaseFragment {

    private static final String TRAINING_LIST = "training_list";
    private ArrayList<TrainingModel> trainingModelArrayList;
    private RecyclerView rvTrainingList;
    private TrainingListFragment trainingListFragment;


    public TrainingListFragment getInstance(ArrayList<TrainingModel> trainingModels) {
        if (trainingListFragment == null) {
            trainingListFragment = new TrainingListFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(TRAINING_LIST, trainingModels);
            trainingListFragment.setArguments(bundle);

        }
        return trainingListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_training_list, container, false);
    }

    @Override
    public void inItUi() {
        rvTrainingList = (RecyclerView) getView().findViewById(R.id.rv_trinaing_list);
    }

    @Override
    public void setListener() {
    }

    @Override
    public void setData() {
        parseArguments();
    }

    private void parseArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            trainingModelArrayList = (ArrayList<TrainingModel>) bundle.getSerializable(TRAINING_LIST);
        }
        setDataToAdapter();
    }

    private void setDataToAdapter() {
        if (trainingModelArrayList != null) {
            TrainingAdapter trainingAdapter = new TrainingAdapter(trainingModelArrayList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            rvTrainingList.setLayoutManager(layoutManager);
            rvTrainingList.setAdapter(trainingAdapter);
        }
    }
}
