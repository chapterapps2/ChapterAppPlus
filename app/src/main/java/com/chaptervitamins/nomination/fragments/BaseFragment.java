package com.chaptervitamins.nomination.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.chaptervitamins.R;
import com.chaptervitamins.nomination.ActivityInterface;


/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment implements ActivityInterface {
    private View rlProgress, mainLayout;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inItUi();
        mainLayout = getActivity().findViewById(R.id.main_layout);
        rlProgress = getActivity().findViewById(R.id.rl_progress);
        setData();
        setListener();

    }


    /**
     * it is used to set visible status of main layout and progress layout
     *
     * @param mainViewStatus
     * @param progressStatus
     */
    public void setVisibilityOfProgress(int mainViewStatus, int progressStatus) {
        if (mainLayout != null && rlProgress != null) {
            mainLayout.setVisibility(mainViewStatus);
            rlProgress.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
            setVisibilityOfProgress(progressStatus);
        }
    }

    /**
     * it is used to set visibility status of progress layout..
     *
     * @param visibleStatus
     * @return
     */
    public void setVisibilityOfProgress(int visibleStatus) {
        if (rlProgress != null)
            rlProgress.setVisibility(visibleStatus);
    }

}
