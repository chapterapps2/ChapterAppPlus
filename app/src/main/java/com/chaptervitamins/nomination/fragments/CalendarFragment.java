package com.chaptervitamins.nomination.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chaptervitamins.R;
import com.chaptervitamins.nomination.models.TrainingModel;
import com.riontech.calendar.CustomCalendar;
import com.riontech.calendar.dao.EventData;
import com.riontech.calendar.dao.dataAboutDate;
import com.riontech.calendar.utils.CalendarUtils;

import java.util.ArrayList;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends BaseFragment {

    private static final String TRAINING_LIST = "training_list";
    private CustomCalendar mCustomCalendar;
    private CalendarFragment calendarFragment;

    public CalendarFragment getInstance(ArrayList<TrainingModel> trainingModels) {
        if (calendarFragment == null) {
            calendarFragment = new CalendarFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(TRAINING_LIST, trainingModels);
            calendarFragment.setArguments(bundle);

        }
        return calendarFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void inItUi() {
        mCustomCalendar = (CustomCalendar) getView().findViewById(R.id.custom_calendar);
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setData() {
        String[] arr = {"2017-08-11", "2017-08-13", "2017-08-15", "2017-08-16", "2017-08-25"};
        for (int i = 0; i < 5; i++) {
            int eventCount = 3;
            mCustomCalendar.addAnEvent(arr[i], eventCount, getEventDataList(eventCount));
        }
    }

    public ArrayList<EventData> getEventDataList(int count) {
        ArrayList<EventData> eventDataList = new ArrayList();

        for (int i = 0; i < count; i++) {
            EventData dateData = new EventData();
            ArrayList<dataAboutDate> dataAboutDates = new ArrayList();

            dateData.setSection(CalendarUtils.getNAMES()[new Random().nextInt(CalendarUtils.getNAMES().length)]);
            dataAboutDate dataAboutDate = new dataAboutDate();

            int index = new Random().nextInt(CalendarUtils.getEVENTS().length);

            dataAboutDate.setTitle("Events");
            dataAboutDate.setSubject("Git Training");
            dataAboutDates.add(dataAboutDate);

            dateData.setData(dataAboutDates);
            eventDataList.add(dateData);
        }

        return eventDataList;
    }
}
