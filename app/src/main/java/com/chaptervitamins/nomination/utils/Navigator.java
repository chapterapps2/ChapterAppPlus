package com.chaptervitamins.nomination.utils;

import android.content.Context;

import com.chaptervitamins.nomination.activities.TrainingActivity;
import com.chaptervitamins.nomination.activities.TrainingDetailActivity;
import com.chaptervitamins.nomination.models.TrainingModel;

/**
 * Created by Vijay Antil on 11-08-2017.
 */

public class Navigator {

    /**
     * It navigates to Training Screen
     * @param context
     */
    public static void navigateToTraining(Context context){
        context.startActivity(TrainingActivity.getStartIntent(context));
    }

    /**
     * It navigates to Training Screen
     * @param context
     */
    public static void navigateToTrainingDetail(Context context, TrainingModel trainingModel){
        context.startActivity(TrainingDetailActivity.getStartIntent(context,trainingModel));
    }
}

