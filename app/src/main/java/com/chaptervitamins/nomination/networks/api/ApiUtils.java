package com.chaptervitamins.nomination.networks.api;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vijay Antil on 21-07-2017.
 */

public class ApiUtils {

    public static ErrorModel getErrorObject(String resp) {
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    JSONObject obj = jsonObject.optJSONObject("status");
                    if (obj != null)
                        if (obj.getString("success").equalsIgnoreCase("false")) {
                            return new ErrorModel(obj.optString("errorkey"), obj.optString("errormsg"));
                        }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
