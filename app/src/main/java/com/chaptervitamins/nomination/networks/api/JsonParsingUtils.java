package com.chaptervitamins.nomination.networks.api;

import android.text.TextUtils;

import com.chaptervitamins.chat.models.ChatGroupModel;
import com.chaptervitamins.nomination.models.TrainingModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 14-08-2017.
 */

public class JsonParsingUtils {

    public static ArrayList<TrainingModel> getAssignCompetency(String response) {
        ArrayList<TrainingModel> trainingModelArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject != null) {
                JSONArray dataArray = jsonObject.getJSONArray("data");
                if (dataArray != null) {
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject trainingObject = (JSONObject) dataArray.get(i);
                        if (trainingObject != null) {
                            String competencyUserId = trainingObject.optString("competency_user_id");
                            String competencyName = trainingObject.optString("competency_name");
                            String proficiencyId = trainingObject.optString("proficiency_id");
                            String proficiencyName = trainingObject.optString("proficiency_name");
                            String moduleId = trainingObject.optString("module_id");
                            String moduleName = trainingObject.optString("module_name");
                            String trainingId = trainingObject.optString("training_id");
                            String trainingName = trainingObject.optString("training_name");
                            String trainingDesc = trainingObject.optString("training_desc");
                            String trainingAgenda = trainingObject.optString("training_agenda");
                            String startDate = trainingObject.optString("start_date");
                            String endDate = trainingObject.optString("end_date");
                            String startTime = trainingObject.optString("start_time");
                            String endTime = trainingObject.optString("end_time");
                            String venue = trainingObject.optString("venue");
                            String zone = trainingObject.optString("zone");
                            String location = trainingObject.optString("location");
                            String city = trainingObject.optString("city");
                            String region = trainingObject.optString("region");
                            String trainingType = trainingObject.optString("training_type");
                            String trainingStatus = trainingObject.optString("training_status");
                            String isMandatory = trainingObject.optString("is_mandatory");
                            String noOfDays = trainingObject.optString("no__of_days");
                            String attendStatus = trainingObject.optString("attend_status");

                            TrainingModel trainingModel = new TrainingModel();
                            trainingModel.setCompetencyUserId(competencyUserId);
                            trainingModel.setCompetencyName(competencyName);
                            trainingModel.setProficiencyId(proficiencyId);
                            trainingModel.setProficiencyName(proficiencyName);
                            trainingModel.setModuleId(moduleId);
                            trainingModel.setModuleName(moduleName);
                            trainingModel.setTrainingId(trainingId);
                            trainingModel.setTrainingName(trainingName);
                            trainingModel.setTrainingDesc(trainingDesc);
                            trainingModel.setTrainingAgenda(trainingAgenda);
                            trainingModel.setStartDate(startDate);
                            trainingModel.setEndDate(endDate);
                            trainingModel.setStartTime(startTime);
                            trainingModel.setEndTime(endTime);
                            trainingModel.setVenue(venue);
                            trainingModel.setZone(zone);
                            trainingModel.setLocation(location);
                            trainingModel.setCity(city);
                            trainingModel.setRegion(region);
                            trainingModel.setTrainingType(trainingType);
                            trainingModel.setTrainingStatus(trainingStatus);
                            trainingModel.setIsMandatory(isMandatory);
                            trainingModel.setNoOfDays(noOfDays);

                            trainingModelArrayList.add(trainingModel);


                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return trainingModelArrayList;
    }

    public static ArrayList<ChatGroupModel> parseGroupsData(String resp) {
        ArrayList<ChatGroupModel> chatGroupModelAl = null;
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                JSONArray jsonArray = jsonObject.optJSONArray("data");
                if (jsonArray != null) {
                    chatGroupModelAl = new ArrayList<ChatGroupModel>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ChatGroupModel groupUtils = new ChatGroupModel();
                        JSONObject object = jsonArray.optJSONObject(i);
                        groupUtils.setCourse_id(object.optString("course_id", ""));
                        groupUtils.setGroup_id(object.optString("group_id", ""));
                        groupUtils.setGroup_name(object.optString("group_name", ""));
                        groupUtils.setGroup_type(object.optString("group_type", ""));
                        groupUtils.setImage(object.optString("image", ""));
                        groupUtils.setUser_id(object.optString("user_id", ""));
                        groupUtils.setGroup_members(object.optString("group_members", "0"));
                        chatGroupModelAl.add(groupUtils);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return chatGroupModelAl;
    }
}
