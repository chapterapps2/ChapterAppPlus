package com.chaptervitamins.nomination.networks.api;

import android.os.AsyncTask;

/**
 * Created by Vijay Antil on 21-07-2017.
 */

public abstract class BaseApiCall extends AsyncTask<Void,Void,Boolean>{

    public interface OnApiCallCompleteListener {
        void onSuccess(Object result);
        void onError(ErrorModel error);
    }

    public Object getResult(){
        return null;
    }

    public ErrorModel parseError(String resp){
        return ApiUtils.getErrorObject(resp);
    }


}
