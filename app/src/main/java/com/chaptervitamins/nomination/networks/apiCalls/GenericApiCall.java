package com.chaptervitamins.nomination.networks.apiCalls;

import android.content.Context;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.api.JsonParsingUtils;
import com.chaptervitamins.newcode.utils.APIUtility;
import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * Created by Tanuj on 21-07-2017.
 */

public class GenericApiCall extends BaseApiCall {
    private OnApiCallCompleteListener listener;
    private final WebServices webServices;
    private String baseUrl;
    private ErrorModel errorModel;
    private Context context;
    private ArrayList<NameValuePair> nameValuePairArrayList;
    private String response;

    public GenericApiCall(Context context, String baseUrl, ArrayList<NameValuePair> nameValuePairs, OnApiCallCompleteListener listener) {
        this.nameValuePairArrayList = nameValuePairs;
        this.listener = listener;
        this.context = context;
        this.baseUrl = baseUrl;
        this.webServices = new WebServices();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        response = new WebServices().callServices(nameValuePairArrayList, baseUrl);
        ErrorModel errorModel = parseError(response);
        if (errorModel == null) {
            if (baseUrl.equalsIgnoreCase(APIUtility.GET_ASSIGNED_COMPETENCY))
                DataBase.getInstance(context).addTrainingData(WebServices.mLoginUtility.getUser_id(), response);
            return true;
        } else
            this.errorModel = errorModel;

        return false;
    }
    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if (status) {
            switch (baseUrl) {
                case APIUtility.GET_ASSIGNED_COMPETENCY:
                    listener.onSuccess(JsonParsingUtils.getAssignCompetency(response));
                    break;
                default:
                    listener.onSuccess(response);
                    break;
            }
        } else
            listener.onError(errorModel);

    }
}
