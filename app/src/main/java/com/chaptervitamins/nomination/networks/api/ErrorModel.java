package com.chaptervitamins.nomination.networks.api;

/**
 * Created by Vijay Antil on 21-07-2017.
 */

public class ErrorModel {
    private String errorCode,errorDescription;

    public ErrorModel(String errorCode, String errorDescription) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
