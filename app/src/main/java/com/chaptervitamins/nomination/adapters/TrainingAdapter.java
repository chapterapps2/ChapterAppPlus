package com.chaptervitamins.nomination.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.nomination.models.TrainingModel;
import com.chaptervitamins.nomination.utils.Navigator;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 14-08-2017.
 */

public class TrainingAdapter extends RecyclerView.Adapter<TrainingAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<TrainingModel> trainingModelArrayList;

    public TrainingAdapter(ArrayList<TrainingModel> trainingModelArrayList) {
        this.trainingModelArrayList = trainingModelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (this.mContext == null)
            this.mContext = parent.getContext();

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_trainer_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position != -1) {
            TrainingModel trainingModel = trainingModelArrayList.get(position);
            if (trainingModel != null) {
                holder.tvName.setText(trainingModel.getTrainingName());
                holder.tvDay.setText("Days : " + trainingModel.getNoOfDays());
                holder.tvVenue.setText("Venue : " + trainingModel.getVenue());
                if (trainingModel.getIsMandatory().equalsIgnoreCase("YES"))
                    holder.rlMandatory.setVisibility(View.VISIBLE);
                else
                    holder.rlMandatory.setVisibility(View.VISIBLE);
                /*if (trainingModel.get().equals("YES")) {
                    holder.tvStart.setVisibility(View.GONE);
                    if (trainingModel.getStatus().equalsIgnoreCase("ATTENDED")) {
                        holder.rlStatus.setVisibility(View.VISIBLE);
                        holder.tvStatus.setText("ATTENDED");
                    } else if (trainingModel.getStatus().equalsIgnoreCase("MISSED")) {
                        holder.tvStatus.setText("MISSED");
                        holder.ivDownload.setVisibility(View.VISIBLE);
                    }

                } else {
                    holder.tvStart.setText(trainingModel.getStartDate());
                    holder.tvStart.setVisibility(View.VISIBLE);
                    holder.rlStatus.setVisibility(View.GONE);
                }*/
            }
        }

    }

    @Override
    public int getItemCount() {
        return 20;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvMandatory, tvName, tvDay, tvVenue, tvStart, tvStatus;
        private View rlStatus, rlMandatory;
        private ImageView ivDownload;

        public ViewHolder(View itemView) {
            super(itemView);
            findVIews(itemView);
            ivDownload.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        private void findVIews(View itemView) {
            tvMandatory = (TextView) itemView.findViewById(R.id.tv_mandatory);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvDay = (TextView) itemView.findViewById(R.id.tv_days);
            tvVenue = (TextView) itemView.findViewById(R.id.tv_venue);
            tvStart = (TextView) itemView.findViewById(R.id.tv_start_date);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            rlStatus = itemView.findViewById(R.id.rl_status);
            rlMandatory = itemView.findViewById(R.id.rl_mandatory);
            ivDownload = (ImageView) itemView.findViewById(R.id.iv_download);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.iv_download) {

            } else {
                Navigator.navigateToTrainingDetail(mContext,null);
            }
        }
    }
}
