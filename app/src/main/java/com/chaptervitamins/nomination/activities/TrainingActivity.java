package com.chaptervitamins.nomination.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.nomination.adapters.ViewPagerAdapter;
import com.chaptervitamins.nomination.fragments.CalendarFragment;
import com.chaptervitamins.nomination.fragments.TrainingListFragment;
import com.chaptervitamins.nomination.models.TrainingModel;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.newcode.utils.APIUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class TrainingActivity extends BaseActivity {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    private  ArrayList<TrainingModel> trainingModelArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);


    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void inItUi() {
        mTitle = "Training";
        isHomeAsUpEnabled = true;
        mTabLayout = (TabLayout) findViewById(R.id.tl_training);
        mViewPager = (ViewPager) findViewById(R.id.vp_training);
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setData() {
        getCompetencyFromServer();
    }


    /**
     * It used to return the intent of TrainingActivity class
     * @param context
     * @return
     */
    public static Intent getStartIntent(Context context){
        Intent intent = new Intent(context,TrainingActivity.class);
        return intent;

    }

    private void getCompetencyFromServer() {
        if(WebServices.isNetworkAvailable(TrainingActivity.this)){
            setVisibilityOfProgress(View.GONE,View.VISIBLE);
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePairs.add(new BasicNameValuePair("user_id",WebServices.mLoginUtility.getUser_id()));
            nameValuePairs.add(new BasicNameValuePair("api_key",APIUtility.APIKEY));
            nameValuePairs.add(new BasicNameValuePair("device_type",APIUtility.DEVICE));
            nameValuePairs.add(new BasicNameValuePair("push_token",""));
            nameValuePairs.add(new BasicNameValuePair("session_token",APIUtility.SESSION));

            new GenericApiCall(TrainingActivity.this, APIUtility.GET_ASSIGNED_COMPETENCY, nameValuePairs, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    setVisibilityOfProgress(View.VISIBLE,View.GONE);
                    trainingModelArrayList = (ArrayList<TrainingModel>) result;
                    setLayoutData(trainingModelArrayList);

                }

                @Override
                public void onError(ErrorModel error) {
                    showNoDataFound();

                }
            }).execute();
        }
    }

    private void setLayoutData(ArrayList<TrainingModel> trainingModelArrayList) {
        if(trainingModelArrayList!=null && trainingModelArrayList.size()>0){
            setTabLayout();
        }else
            showNoDataFound();
    }

    /**
     * TO set tab layout
     */
    public void setTabLayout() {
        if (mViewPager == null || mTabLayout == null)
            return;
        setUpViewPager(mViewPager);

        mTabLayout.addTab(mTabLayout.newTab().setText("CALENDAR"));
        mTabLayout.addTab(mTabLayout.newTab().setText("LIST"));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (mViewPagerAdapter != null && mViewPager != null) {
                    mViewPager.setCurrentItem(tab.getPosition());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (mViewPagerAdapter == null || mViewPager == null)
                    return;

                int tabPosition = tab.getPosition();
                Fragment fragment = ((ViewPagerAdapter) mViewPager.getAdapter()).getFragmentAtPosition(tabPosition);
                if (fragment != null) {
                    switch (tabPosition) {
                        case '1':

                            break;
                        case '2':
                            break;

                    }
                }

            }
        });

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

    }


    /**
     * To set Viewpager
     *
     * @param mViewPager
     */
    public void setUpViewPager(final ViewPager mViewPager) {
        try {
            mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            mViewPagerAdapter.addFragment(new CalendarFragment().getInstance(trainingModelArrayList), "Calendar");
            mViewPagerAdapter.addFragment(new TrainingListFragment().getInstance(trainingModelArrayList), "List");

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    if (mViewPagerAdapter != null && mViewPager != null) {
                        Fragment fragment = mViewPagerAdapter.getFragmentAtPosition(position);
                        if (fragment != null) {
                            fragment.onResume();
                        }

                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            mViewPager.setAdapter(mViewPagerAdapter);
            mViewPager.setOffscreenPageLimit(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
