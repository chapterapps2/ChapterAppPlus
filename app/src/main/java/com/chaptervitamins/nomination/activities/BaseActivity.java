package com.chaptervitamins.nomination.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.nomination.ActivityInterface;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements ActivityInterface, View.OnClickListener {

    private static final String TAG = ">>>>>BaseActivity";
    protected String mTitle = "";
    protected boolean isHomeAsUpEnabled;
    protected Toolbar mToolbar;
    protected RelativeLayout rlProgress;
    protected TextView tvNoDataFound;
    private View mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        inItUi();

        mainLayout = findViewById(R.id.root_layout);
        tvNoDataFound = (TextView) findViewById(R.id.tv_no_data_found);

        rlProgress = (RelativeLayout) findViewById(R.id.rl_progress);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setUpToolbar();
        }
        setListener();
        setData();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * It is used to set up the toolbar
     */
    private void setUpToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isHomeAsUpEnabled);
        getSupportActionBar().setTitle(mTitle);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    /**
     * it is used to set visible status of main layout and progress layout
     *
     * @param mainViewStatus
     * @param progressStatus
     */
    public void setVisibilityOfProgress(int mainViewStatus, int progressStatus) {
        if (mainLayout != null && rlProgress != null) {
            mainLayout.setVisibility(mainViewStatus);
            rlProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            setVisibilityOfProgress(progressStatus);
        }
        if(tvNoDataFound!=null)
            tvNoDataFound.setVisibility(View.GONE);
    }

    /**
     * it is used to set visibility status of progress layout..
     *
     * @param visibleStatus
     * @return
     */
    public void setVisibilityOfProgress(int visibleStatus) {
        if (rlProgress != null)
            rlProgress.setVisibility(visibleStatus);
        if(tvNoDataFound!=null)
            tvNoDataFound.setVisibility(View.GONE);
    }

    public void showNoDataFound(){
        setVisibilityOfProgress(View.GONE,View.GONE);
        if(tvNoDataFound!=null)
            tvNoDataFound.setVisibility(View.VISIBLE);
    }

}
