package com.chaptervitamins.nomination.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.nomination.models.TrainingModel;

public class TrainingDetailActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    private static final String TRAINING_MODEL = "training_model";
    private RadioGroup rgNomination;
    private RadioButton rbSelfNomination,rbRequestManager;
    private LinearLayout rlRequestManager;
    private TextView tvProgramName,tvTrainingType,tvVenue,tvStartDate,tvStartTime,tvEndDate,tvEndTime,tvDays,tvTrainerName,tvNominationType;
    private EditText edProgramDescription, edRequestNote;
    private Button btnNominate;
    private TrainingModel trainingModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_detail);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void inItUi() {
        mTitle = "Training Nomination";
        isHomeAsUpEnabled = true;

        rgNomination = (RadioGroup) findViewById(R.id.rg_nomination);
        rlRequestManager = (LinearLayout) findViewById(R.id.rl_request_manager);
        rbSelfNomination = (RadioButton) findViewById(R.id.rb_self_nomination);
        rbRequestManager = (RadioButton) findViewById(R.id.rb_request_manager);
        tvProgramName = (TextView) findViewById(R.id.tv_program_name);
        tvVenue = (TextView) findViewById(R.id.tv_venue);
        tvTrainingType = (TextView) findViewById(R.id.tv_training_type);
        tvStartDate = (TextView) findViewById(R.id.tv_start_date);
        tvStartTime = (TextView) findViewById(R.id.tv_start_time);
        tvEndDate = (TextView) findViewById(R.id.tv_end_date);
        tvEndTime = (TextView) findViewById(R.id.tv_end_time);
        tvDays = (TextView) findViewById(R.id.tv_days);
        tvTrainerName = (TextView) findViewById(R.id.tv_trainer_name);
        tvNominationType = (TextView) findViewById(R.id.tv_nomination_type);
        edProgramDescription = (EditText) findViewById(R.id.et_desciption);
        edRequestNote = (EditText) findViewById(R.id.et_request_note);
        btnNominate = (Button) findViewById(R.id.btn_nominate);

    }

    @Override
    public void setListener() {
        rgNomination.setOnCheckedChangeListener(this);
        btnNominate.setOnClickListener(this);
    }

    @Override
    public void setData() {
        getDataFromBundle();
        setDataToViews();
    }

    private void getDataFromBundle() {
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            trainingModel = (TrainingModel) bundle.getSerializable(TRAINING_MODEL);
        }
    }

    private void setDataToViews(){
        if(trainingModel!=null){
            tvProgramName.setText(trainingModel.getTrainingName());
            tvVenue.setText(trainingModel.getVenue());
            tvTrainingType.setText(trainingModel.getTrainingType());
            tvStartTime.setText(trainingModel.getStartDate());
            tvStartTime.setText(trainingModel.getStartTime());
            tvEndDate.setText(trainingModel.getEndDate());
            tvEndTime.setText(trainingModel.getEndTime());
            tvDays.setText(trainingModel.getNoOfDays());
            tvTrainerName.setText("");
            tvNominationType.setText("");
        }else{
            showNoDataFound();
        }
    }

    /**
     * It used to return the intent of TrainingDetailActivity class
     *
     * @param context
     * @return
     */
    public static Intent getStartIntent(Context context, TrainingModel trainingModel) {
        Intent intent = new Intent(context, TrainingDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(TRAINING_MODEL, trainingModel);
        intent.putExtras(bundle);
        return intent;

    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        if(checkedId == R.id.rb_self_nomination){
            rbRequestManager.setChecked(false);
            rlRequestManager.setVisibility(View.GONE);
        }else{
            rbSelfNomination.setChecked(false);
            rlRequestManager.setVisibility(View.VISIBLE);
        }
    }
}
