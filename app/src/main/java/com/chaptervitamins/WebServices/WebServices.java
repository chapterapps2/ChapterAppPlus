package com.chaptervitamins.WebServices;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.discussions.TopicChatFragment;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.models.AnswerModel;
import com.chaptervitamins.newcode.models.ContentInShortsModel;
import com.chaptervitamins.newcode.models.EventModel;
import com.chaptervitamins.newcode.models.TrainerModel;
import com.chaptervitamins.newcode.models.VideoCommentModel;
import com.chaptervitamins.newcode.models.VideoQuizAllResponseModel;
import com.chaptervitamins.newcode.models.VideoQuizResponse;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.QuizUtils;
import com.chaptervitamins.quiz.SurveyDataUtil;
import com.chaptervitamins.quiz.model.Vertical;
import com.chaptervitamins.signup.model.SignupModel;
import com.chaptervitamins.utility.Chat_MessagesUtils;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CommentsUtils;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.Favourites_Utils;
import com.chaptervitamins.utility.FlashCardUtility;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.GetPendingRes_Utility;
import com.chaptervitamins.utility.GroupMembersUtils;
import com.chaptervitamins.utility.GroupUtils;
import com.chaptervitamins.utility.Hospital_Utils;
import com.chaptervitamins.utility.Like_Utility;
import com.chaptervitamins.utility.LoginUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.chaptervitamins.utility.NewsUtility;
import com.chaptervitamins.utility.Notification_Material_Util;
import com.chaptervitamins.utility.Notification_Utility;
import com.chaptervitamins.utility.Pending_Response_Utility;
import com.chaptervitamins.utility.QuestionUtility;
import com.chaptervitamins.utility.Question_Response_util;
import com.chaptervitamins.utility.Quiz_Response_Util;
import com.chaptervitamins.utility.ReadResponseUtility;
import com.chaptervitamins.utility.Suggestion_Utils;
import com.chaptervitamins.utility.SurveyUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by abcd on 4/19/2016.
 */
public class WebServices {
    public static String ERRORMSG = "";
    public static String DEVICE_ERRORMSG = "";
    public static LoginUtility mLoginUtility = new LoginUtility();
    public static QuestionUtility questionUtility = new QuestionUtility();
    public static MeterialUtility flashcardMeterialUtilities = new MeterialUtility();
    public static MeterialUtility imageFlashcardMeterialUtilities = new MeterialUtility();
    public static ArrayList<GetPendingRes_Utility> mGetPendingData = new ArrayList<>();
    public static ArrayList<GroupUtils> groupUtilsArrayList = new ArrayList<>();
    public static ArrayList<GroupMembersUtils> membersUtilses = new ArrayList<>();
    public static ArrayList<GroupMembersUtils> groupmembersUtilses = new ArrayList<>();
    public static ArrayList<Chat_MessagesUtils> chat_messagesUtilses = new ArrayList<>();
    public static ArrayList<Chat_MessagesUtils> group_chat_messagesUtilses = new ArrayList<>();
    public static ArrayList<Notification_Utility> notificationUtilityArrayList = new ArrayList<>();
    public static ArrayList<Notification_Material_Util> material_utils = new ArrayList<>();
    public static ArrayList<Like_Utility> like_utilities = new ArrayList<>();
    public static ArrayList<Favourites_Utils> favourites_utilses = new ArrayList<>();
    public static List<Hospital_Utils> hospital_utilses = new ArrayList<>();
    public static List<String> hospital_types = new ArrayList<>();
    public static List<String> hospital_city = new ArrayList<>();

    public String notificationService(List<NameValuePair> nameValuePair, String url) {
        String resp = "";
        try {
            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
// Set the default socket timeout (SO_TIMEOUT)
// in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 10000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            System.setProperty("http.keepAlive", "false");
            httpPost.setHeader(HTTP.CONTENT_TYPE,
                    "application/x-www-form-urlencoded;charset=UTF-8");
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            resp = readResponse(httpResponse);
            System.out.println(resp + "===");
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            return resp;
        }
    }

    /* public String callServices(List<NameValuePair> nameValuePair, String url) {
         String resp = "";
         try {
             HttpParams httpParameters = new BasicHttpParams();
 // Set the timeout in milliseconds until a connection is established.
 // The default value is zero, that means the timeout is not used.
             int timeoutConnection = 10000;
             HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
 // Set the default socket timeout (SO_TIMEOUT)
 // in milliseconds which is the timeout for waiting for data.
             int timeoutSocket = 10000;
             HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

             HttpClient httpClient = new DefaultHttpClient(httpParameters);
             HttpPost httpPost = new HttpPost(url);
             httpPost.setHeader(HTTP.CONTENT_TYPE,
                     "application/x-www-form-urlencoded;charset=UTF-8");
             httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
             System.setProperty("http.keepAlive", "false");
             HttpResponse httpResponse = httpClient.execute(httpPost);
             HttpEntity httpEntity = httpResponse.getEntity();
             resp = readResponse(httpResponse);
             System.out.println(resp + "===");
         } catch (Exception exception) {
             exception.printStackTrace();
         } finally {
             return resp;
         }
     }
 */
    public void setProgress(DataBase mDatabase, String courseId, boolean isAllCourse) {
        HomeActivity.mSubmitResponse = mDatabase.getResponseData();
        for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
            CourseUtility courseUtility = HomeActivity.courseUtilities.get(i);
            if (isAllCourse) {
                setProgress(courseUtility, HomeActivity.mReadResponse, HomeActivity.mSubmitResponse);
            } else {
                if (courseUtility.getCourse_id().equals(courseId)) {
                    setProgress(courseUtility, HomeActivity.mReadResponse, HomeActivity.mSubmitResponse);
                    break;
                }
            }
        }
    }

    /**
     * It is used ot set Progress of courses
     *
     * @param mReadResponse
     */
    public void setProgress(CourseUtility courseUtility, ArrayList<ReadResponseUtility> mReadResponse, ArrayList<ReadResponseUtility> mSubmitResponse) {
        int noOfCompletedModules = 0, moduleCompletionPer = 0;
        ArrayList<ModulesUtility> modulesUtilities = courseUtility.getModulesUtilityArrayList();
        for (int i = 0; i < modulesUtilities.size(); i++) {
            ModulesUtility modulesUtility = modulesUtilities.get(i);
            ArrayList<MeterialUtility> meterialUtilities = modulesUtility.getMeterialUtilityArrayList();
            int meterialSize = meterialUtilities.size();
            if (meterialSize == 0) {
                modulesUtility.setTotalProgress(String.valueOf(100));
            }
            for (int ii = 0; ii < meterialSize; ii++) {
                MeterialUtility meterialUtility = meterialUtilities.get(ii);
                if (mReadResponse != null && mReadResponse.size() > 0)
                    for (int j = 0; j < mReadResponse.size(); j++) {
                        ReadResponseUtility readResponseUtility = mReadResponse.get(j);
                        if (meterialUtility.getMaterial_id().equals(readResponseUtility.getMaterial_id()) &&
                                meterialUtility.getAssign_material_id().equals(readResponseUtility.getAssign_material_id())) {
                            meterialUtility.setLastplayed(readResponseUtility.getFinish_time());
                            meterialUtility.setSeen(true);
                            meterialUtility.setEarned_coins(readResponseUtility.getCoins_allocated());
                            meterialUtility.setTotal_count(readResponseUtility.getTotal_count());
                            meterialUtility.setMax_result(readResponseUtility.getMax_score());
                            meterialUtility.setSeen_count(readResponseUtility.getSeen_count());
                            if (!TextUtils.isEmpty(readResponseUtility.getCompletion_per())) {
                                if (Integer.parseInt(readResponseUtility.getCompletion_per()) > 100)
                                    meterialUtility.setComplete_per("100");
                                else if(Integer.parseInt(meterialUtility.getComplete_per())<Integer.parseInt(readResponseUtility.getCompletion_per()))
                                    meterialUtility.setComplete_per(readResponseUtility.getCompletion_per());
                            } else
                                meterialUtility.setComplete_per("0");
                            meterialUtility.setIs_accessed(true);
                            meterialUtility.setScrom_status(readResponseUtility.getScrom_status());
                            meterialUtility.setAverage_rating(readResponseUtility.getAvg_rating());
                            meterialUtility.setRateNum(readResponseUtility.getMy_rating());
                            meterialUtility.setResponse_id(readResponseUtility.getResponse_id());
                            if (!TextUtils.isEmpty(meterialUtility.getMax_result()) && !TextUtils.isEmpty(meterialUtility.getPassing_percentage()) && Integer.parseInt(meterialUtility.getMax_result()) >= Integer.parseInt(meterialUtility.getPassing_percentage()))
                                meterialUtility.setPassed(true);

                            if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                if (meterialUtility.isPassed()) {
                                    meterialUtility.setComplete_per("100");
                                    meterialUtility.setIsComplete(true);
                                } else {
                                    meterialUtility.setComplete_per("0");
                                    meterialUtility.setIsComplete(false);
                                }
                            } else if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
                                if (!TextUtils.isEmpty(meterialUtility.getScrom_status()) && meterialUtility.getScrom_status().equalsIgnoreCase("COMPLETED") && meterialUtility.isPassed()) {
                                    meterialUtility.setComplete_per("100");
                                    meterialUtility.setIsComplete(true);
                                } else {
                                    meterialUtility.setComplete_per(meterialUtility.getComplete_per());
                                    meterialUtility.setIsComplete(false);
                                }
                            } else {
                                if (!TextUtils.isEmpty(meterialUtility.getComplete_per()) && meterialUtility.getComplete_per().equals("100"))
                                    meterialUtility.setIsComplete(true);
                                else
                                    meterialUtility.setIsComplete(false);
                            }
                            String attemptTaken = readResponseUtility.getAttempt_taken();
                            if (!TextUtils.isEmpty(attemptTaken))
                                meterialUtility.setRemainingAttempt(Integer.parseInt(meterialUtility.getNo_of_attempt()) - Integer.parseInt(attemptTaken) + "");
                            else
                                meterialUtility.setRemainingAttempt(meterialUtility.getNo_of_attempt());
                            break;
                        } else {
                            meterialUtility.setRemainingAttempt(meterialUtility.getNo_of_attempt());

                        }
                    }
                else {
                    meterialUtility.setRemainingAttempt(meterialUtility.getNo_of_attempt());
                }

                if (mSubmitResponse != null && mSubmitResponse.size() > 0) {
                    int responseSize = mSubmitResponse.size();
                    for (int k = 0; k < responseSize; k++) {
                        ReadResponseUtility readResponseUtility = mSubmitResponse.get(k);
                        if (meterialUtility.getMaterial_id().equalsIgnoreCase(readResponseUtility.getMaterial_id()) &&
                                meterialUtility.getAssign_material_id().equals(readResponseUtility.getAssign_material_id())) {
                            meterialUtility.setLastplayed(readResponseUtility.getFinish_time());
                            meterialUtility.setSeen(true);
                            meterialUtility.setMax_result(readResponseUtility.getMax_score());
                            meterialUtility.setEarned_coins(readResponseUtility.getCoins_allocated());
                            meterialUtility.setTotal_count(readResponseUtility.getTotal_count());
                            meterialUtility.setSeen_count(readResponseUtility.getSeen_count());
                            if (!TextUtils.isEmpty(meterialUtility.getComplete_per()) && !TextUtils.isEmpty(readResponseUtility.getCompletion_per()) && Integer.parseInt(readResponseUtility.getCompletion_per()) > Integer.parseInt(meterialUtility.getComplete_per()))
                                meterialUtility.setComplete_per(readResponseUtility.getCompletion_per());
                            if (!TextUtils.isEmpty(meterialUtility.getMax_result()) && !TextUtils.isEmpty(meterialUtility.getPassing_percentage()) && Integer.parseInt(meterialUtility.getMax_result()) >= Integer.parseInt(meterialUtility.getPassing_percentage()))
                                meterialUtility.setPassed(true);
                            meterialUtility.setIs_accessed(true);
                            if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                                if (meterialUtility.isPassed()) {
                                    meterialUtility.setComplete_per("100");
                                    meterialUtility.setIsComplete(true);
                                } else {
                                    meterialUtility.setComplete_per("0");
                                    meterialUtility.setIsComplete(false);
                                }
                            } else {
                                if (!TextUtils.isEmpty(meterialUtility.getComplete_per()) && meterialUtility.getComplete_per().equals("100"))
                                    meterialUtility.setIsComplete(true);
                                else
                                    meterialUtility.setIsComplete(false);
                            }
                            meterialUtility.setRemainingAttempt((Integer.parseInt(meterialUtility.getRemainingAttempt()) - 1) + "");
                        }
                    }
                }
                if (!TextUtils.isEmpty(meterialUtility.getRemainingAttempt()) && meterialUtility.getRemainingAttempt().equals("-1"))
                    meterialUtility.setRemainingAttempt("0");
                if (meterialUtility.getNo_of_attempt().equals("0"))
                    meterialUtility.setRemainingAttempt("1000");
            }
            int moduleCompletionPerTotal = 0, noOfCompleteMeterials = 0;
            for (int l = 0; l < meterialSize; l++) {
                MeterialUtility meterialUtility1 = meterialUtilities.get(l);
                if (meterialUtility1.getIsComplete()) {
                    noOfCompleteMeterials++;
                    moduleCompletionPerTotal = moduleCompletionPerTotal + Integer.parseInt(meterialUtility1.getComplete_per());
                }
            }
            int moduleCompletionPercent = Math.round(moduleCompletionPerTotal / meterialSize);
            moduleCompletionPer = moduleCompletionPer + moduleCompletionPercent;
            modulesUtility.setCompletion_per(moduleCompletionPercent + "");
            modulesUtility.setCompletedMaterials(noOfCompleteMeterials);
            if (meterialSize == noOfCompleteMeterials) {
                noOfCompletedModules++;
                modulesUtility.setCompleted(true);
            } else {
                modulesUtility.setCompleted(false);
            }
        }
        courseUtility.setCompletedModules(noOfCompletedModules);

        if (modulesUtilities != null && modulesUtilities.size() > 0)
            courseUtility.setCompletion_per(Math.round(noOfCompletedModules * 100 / courseUtility.getModulesUtilityArrayList().size()) + "");
        else
            courseUtility.setCompletion_per("100");
    }

    public void setProgressOfMaterial(DataBase dataBase, MeterialUtility mMeterialUtility, String seenCount, String totalCount, String coinsCollected) {
        if (mMeterialUtility.getCoinsAllocatedModel() != null && mMeterialUtility.getCoinsAllocatedModel().getRedeem().equalsIgnoreCase("FALSE") && !coinsCollected.equals("0")) {
            mMeterialUtility.getCoinsAllocatedModel().setRedeem("TRUE");
            mMeterialUtility.getCoinsAllocatedModel().setCurrentCoins(coinsCollected);
            mMeterialUtility.getCoinsAllocatedModel().setEarnCoins(coinsCollected);
            if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                WebServices.updateTotalCoins(dataBase, coinsCollected);
        }
        mMeterialUtility.setSeen_count(String.valueOf(seenCount));
        mMeterialUtility.setTotal_count(String.valueOf(totalCount));
        int currentProgress;
        if (Double.parseDouble(totalCount) != 0) {
            currentProgress = Math.round(((int)Double.parseDouble(seenCount) * 100) / (int)Double.parseDouble(totalCount));
        } else {
            currentProgress = 0;
        }
//        if (!TextUtils.isEmpty(mMeterialUtility.getComplete_per())) {
//            if (currentProgress > Integer.parseInt(mMeterialUtility.getComplete_per()))
        if (currentProgress == 98 || currentProgress == 99) {
//            mMeterialUtility.setComplete_per(currentProgress + "");
            currentProgress = 100;
            mMeterialUtility.setSeen_count(mMeterialUtility.getTotal_count());
        }
        if (mMeterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
            if (!TextUtils.isEmpty(mMeterialUtility.getScrom_status()) && mMeterialUtility.getScrom_status().equalsIgnoreCase("COMPLETED") && mMeterialUtility.isPassed()) {
                mMeterialUtility.setComplete_per("100");
                mMeterialUtility.setIsComplete(true);
            } else {
                mMeterialUtility.setComplete_per("0");
                mMeterialUtility.setIsComplete(false);
            }
        } else {
            if (!TextUtils.isEmpty(mMeterialUtility.getComplete_per())) {
                if (currentProgress > Integer.parseInt(mMeterialUtility.getComplete_per()))
                    mMeterialUtility.setComplete_per(currentProgress + "");
                else
                    mMeterialUtility.setComplete_per(mMeterialUtility.getComplete_per());
            } else
                mMeterialUtility.setComplete_per(currentProgress + "");
        }
//        } else

//        mMeterialUtility.setComplete_per(currentProgress + "");

        setProgressOfMaterialInStaticList(mMeterialUtility);
    }

    public void setProgressOfMaterialInStaticList(MeterialUtility meterialUtility) {
        MeterialUtility mMeterialUtility = FlowingCourseUtils.getMaterialFromCourse(meterialUtility);
        if (mMeterialUtility != null) {
            mMeterialUtility.setCoinsAllocatedModel(meterialUtility.getCoinsAllocatedModel());
            mMeterialUtility.setSeen_count(meterialUtility.getSeen_count());
            mMeterialUtility.setTotal_count(meterialUtility.getTotal_count());
            if (!TextUtils.isEmpty(mMeterialUtility.getComplete_per())) {
                if (Integer.parseInt(meterialUtility.getComplete_per()) > Integer.parseInt(mMeterialUtility.getComplete_per()))
                    mMeterialUtility.setComplete_per(meterialUtility.getComplete_per());
            }
        }

    }

    public void setSeenCountOfMaterialInStaticList(MeterialUtility meterialUtility) {
        MeterialUtility mMeterialUtility = FlowingCourseUtils.getMaterialFromCourse(meterialUtility);
        if (mMeterialUtility != null) {
            mMeterialUtility.setSeen_count(meterialUtility.getSeen_count());
        }

    }
    /*  public void setProgress(CourseUtility courseUtility, String course_id, ArrayList<ModulesUtility> modulesUtilities, ArrayList<ReadResponseUtility> mReadResponse, ArrayList<ReadResponseUtility> mSubmitResponse) {

          for (int i = 0; i < modulesUtilities.size(); i++) {
              ArrayList<MeterialUtility> meterialUtilities = modulesUtilities.get(i).getMeterialUtilityArrayList();
              if (meterialUtilities.size() == 0) {
                  modulesUtilities.get(i).setTotalProgress(String.valueOf(100));
              }
              int progress = 0, completedMaterials = 0, completedModules = 0;

              for (int ii = 0; ii < meterialUtilities.size(); ii++) {
                  MeterialUtility meterialUtility = meterialUtilities.get(ii);
                  for (int j = 0; j < mReadResponse.size(); j++) {
                      ReadResponseUtility readResponseUtility = mReadResponse.get(j);
                      if (meterialUtility.getMaterial_id().equals(readResponseUtility.getMaterial_id()) &&
                              meterialUtility.getAssign_material_id().equals(readResponseUtility.getAssign_material_id())) {
                          meterialUtilities.get(ii).setLastplayed(readResponseUtility.getFinish_time());
                          meterialUtilities.get(ii).setSeen(true);

                      }
                  }
                  for (int j = 0; j < mReadResponse.size(); j++) {
                      if (meterialUtilities.get(ii).getMaterial_id().equalsIgnoreCase(mReadResponse.get(j).getMaterial_id())) {
                          meterialUtilities.get(ii).setIsComplete("true");
                          completedMaterials++;
                          System.out.println("=====course_id==" + course_id + "===Material_id===" + meterialUtilities.get(ii).getMaterial_id());
                          progress++;
                          break;
                      }
                  }

                  int totalAttempt = 0;
                  for (int j = 0; j < mReadResponse.size(); j++) {

  //                    if (mReadResponse.get(j).getAssign_course_id().equalsIgnoreCase(course_id)) {
  //                    if (meterialUtilities.get(ii).getMaterial_id().equalsIgnoreCase(mReadResponse.get(j).getMaterial_id()) &&
  //                            meterialUtilities.get(ii).getAssign_material_id().equalsIgnoreCase(mReadResponse.get(j).getAssign_material_id())) {
  //                        totalAttempt++;
  //                    }
                      if (meterialUtilities.get(ii).getMaterial_id().equalsIgnoreCase(mReadResponse.get(j).getMaterial_id())) {
                          totalAttempt++;
                      }
  //                    }
                  }
                  Log.d("vijay", totalAttempt + "     " + meterialUtilities.get(ii).getTitle());
                  if (mSubmitResponse != null && mSubmitResponse.size() > 0) {
                      int responseSize = mSubmitResponse.size();
                      for (int k = 0; k < responseSize; k++) {
  //                        if (meterialUtilities.get(ii).getMaterial_id().equalsIgnoreCase(mSubmitResponse.get(k).getMaterial_id()) &&
  //                                meterialUtilities.get(ii).getAssign_material_id().equalsIgnoreCase(mSubmitResponse.get(k).getAssign_material_id()))
  //                            totalAttempt++;
                          if (meterialUtilities.get(ii).getMaterial_id().equalsIgnoreCase(mSubmitResponse.get(k).getMaterial_id()))
                              totalAttempt++;
                      }
                  }
                  int remainingAtt = 0;
                  if (!meterialUtilities.get(ii).getNo_of_attempt().equalsIgnoreCase("") && !meterialUtilities.get(ii).getNo_of_attempt().equalsIgnoreCase("null") && meterialUtilities.get(ii).getNo_of_attempt() != null) {
                      if (meterialUtilities.get(ii).getNo_of_attempt().equals("0")) {
                          remainingAtt = 100;
                          meterialUtilities.get(ii).setRemainingAttempt(String.valueOf(remainingAtt));
                      } else {
                          remainingAtt = Integer.parseInt(meterialUtilities.get(ii).getNo_of_attempt()) - totalAttempt;
                          if (remainingAtt >= 0)
                              meterialUtilities.get(ii).setRemainingAttempt(String.valueOf(remainingAtt));
                          else
                              meterialUtilities.get(ii).setRemainingAttempt(String.valueOf(0));
                      }
                  }
              }

              modulesUtilities.get(i).setCompletedMaterials(completedMaterials);
              if (completedMaterials == meterialUtilities.size())
                  modulesUtilities.get(i).setCompleted(true);
  //            int totalmaterial = Integer.parseInt(modulesUtilities.get(i).getTotalPDF()) + Integer.parseInt(modulesUtilities.get(i).getTotalQuiz()) + Integer.parseInt(modulesUtilities.get(i).getTotalFlashCard()) + Integer.parseInt(modulesUtilities.get(i).getTotalVideo()) + Integer.parseInt(modulesUtilities.get(i).getTotalAudio()) + Integer.parseInt(modulesUtilities.get(i).getTotalElearning());
              int totalmaterial = meterialUtilities.size();
              if (progress != 0 && totalmaterial != 0) {
                  int val = Math.round((progress * 100) / totalmaterial);
                  modulesUtilities.get(i).setTotalProgress(String.valueOf(val));
              } else {
                  modulesUtilities.get(i).setTotalProgress(String.valueOf(0));
              }

              for (int moduleIndex = 0; moduleIndex < modulesUtilities.size(); moduleIndex++) {
                  if (modulesUtilities.get(moduleIndex).isCompleted())
                      completedModules++;
              }
              courseUtility.setCompletedModules(completedModules);
          }

      }
  */
   /* public int isCompletedMaterials(MeterialUtility meterialUtility) {
        for (int j = 0; j < HomeActivity.mReadResponse.size(); j++) {
            if (meterialUtility.getMaterial_id().equals(HomeActivity.mReadResponse.get(j).getMaterial_id())
                    && meterialUtility.getAssign_material_id().equals(HomeActivity.mReadResponse.get(j).getAssign_material_id())) {
                return
                break;
            }
        }
        return iscompleted;
    }*/

    public static final MediaType JSON
            = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .build();

    public String getLogin(List<NameValuePair> nameValuePair, String url) {
        try {
            FormBody.Builder builder = new FormBody.Builder();
            if (nameValuePair != null && nameValuePair.size() > 0 && !TextUtils.isEmpty(url) && builder != null) {
                for (NameValuePair pair : nameValuePair) {
                    if (pair != null)
                        if (pair.getValue() != null)
                            builder.add(pair.getName(), pair.getValue());
                        else
                            builder.add(pair.getName(), "");
                }
                RequestBody body = builder.build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();

                Response response = null;
                try {
                    response = client.newCall(request).execute();
                    return response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (response != null) {
                        response.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public String callServices(List<NameValuePair> nameValuePair, String url) {
        try {
            FormBody.Builder builder = new FormBody.Builder();
            if (nameValuePair != null && nameValuePair.size() > 0 && !TextUtils.isEmpty(url) && builder != null) {
                for (NameValuePair pair : nameValuePair) {
                    if (pair != null) {
                        if (pair.getValue() != null)
                            builder.add(pair.getName(), pair.getValue());
                        else
                            builder.add(pair.getName(), "");
                    }

                }
                RequestBody body = builder.build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();

                Response response = null;
                try {
                    response = client.newCall(request).execute();
                    return response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (response != null) {
                        response.close();
                    }
                }
            }
        } catch (NullPointerException ec) {
            ec.printStackTrace();
        }
        return "";
    }


    public ArrayList<Suggestion_Utils> getAllSuggestions(String resp) {
        ArrayList<Suggestion_Utils> arrayList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.optJSONObject(i);
                    Suggestion_Utils utils = new Suggestion_Utils();

                    utils.setCategory(jsonObject.optString("category", ""));
                    utils.setEmail(jsonObject.optString("email", ""));
                    utils.setFirstname(jsonObject.optString("firstname", ""));
                    utils.setMessage(jsonObject.optString("message", ""));
                    utils.setPsid(jsonObject.optString("psid", ""));
                    utils.setSuggestion_query_id(jsonObject.optString("suggestion_query_id", ""));
                    utils.setType(jsonObject.optString("type", ""));
                    utils.setUser_id(jsonObject.optString("user_id", ""));

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");

                    JSONObject mediaObject = jsonObject.optJSONObject("media");
                    if (mediaObject != null) {
                        utils.setFile_url(mediaObject.optString("file_url", ""));
                        utils.setFile_type(mediaObject.optString("type", ""));
                        utils.setFile_size(mediaObject.optString("file_size", ""));
                    }
                    String date = "";
                    if (!jsonObject.optString("added_on", "").equalsIgnoreCase("")) {
                        try {
                            Date d = df.parse(jsonObject.optString("added_on", ""));
                            utils.setAdded_on(df3.format(d));
                        } catch (Exception e) {

                        }
                    }
                    JSONArray ansArray = jsonObject.optJSONArray("answers");
                    if (ansArray == null)
                        utils.setAnswers(jsonObject.optString("answers", ""));
                    else
                        for (int j = 0; j < ansArray.length(); j++) {
                            jsonObject = ansArray.optJSONObject(j);
                            utils.setAnswers(jsonObject.optString("answer", ""));
                            break;
                        }
                    arrayList.add(utils);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    /**
     * It is used to remove material locally
     *
     * @param materialId
     */
    public static void removeFavoriteMaterial(String materialId) {
        if (favourites_utilses != null) {
            for (int i = 0; i < favourites_utilses.size(); i++) {
                if (favourites_utilses.get(i).getItem_id().equals(materialId)) {
                    favourites_utilses.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * It is used to add favourite locally before api call
     *
     * @param meterialUtility
     */
    public static void addFavoriteMaterial(MeterialUtility meterialUtility) {
        if (favourites_utilses != null) {
            Favourites_Utils favourites_utils = new Favourites_Utils();
            favourites_utils.setFavourite_id("1");
            favourites_utils.setIs_favourite("yes");
            favourites_utils.setItem_id(meterialUtility.getMaterial_id());
            favourites_utils.setItem_type(meterialUtility.getMaterial_type());
            favourites_utils.setImage(meterialUtility.getMaterial_image());
            favourites_utilses.add(favourites_utils);
        }
    }

    /**
     * It is used to add favourite locally before api call
     *
     * @param meterialUtility
     */
    public static void removeFavoriteMaterial(MeterialUtility meterialUtility) {
        if (favourites_utilses != null) {
            for (Favourites_Utils favourites_utils : favourites_utilses)
                if (!TextUtils.isEmpty(favourites_utils.getItem_id()) &&
                        favourites_utils.getItem_id().equalsIgnoreCase(meterialUtility.getMaterial_id())) {
                    favourites_utilses.remove(favourites_utils);
                }
        }
    }

    /**
     * It is used to add mateial in favorite after api call
     *
     * @param response
     */
    public static void addFavoriteMaterial(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    if (favourites_utilses != null && favourites_utilses.size() > 0) {
                        favourites_utilses.get(favourites_utilses.size() - 1).setFavourite_id(object.optString("favourite_id", "0"));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public boolean get_reset_msg(String resp) {
        boolean isLogin = false;
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj == null || obj.optString("success").equalsIgnoreCase("false")) {
                isLogin = false;
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                isLogin = true;
                jsonObject = jsonObject.optJSONObject("data");
                ERRORMSG = jsonObject.optString("successmsg");
            }
        } catch (Exception e) {
            e.printStackTrace();
            isLogin = false;
        }
        return isLogin;
    }

    public void getAllHospitals(String resp) {
        try {
            JSONObject jsonObject = new JSONObject(resp);

//        jsonObject = jsonObject.optJSONObject("data");
            JSONArray array = jsonObject.optJSONArray("data");
            if (array != null) {
                hospital_utilses = new ArrayList<>();

                hospital_city = new ArrayList<>();
                hospital_types = new ArrayList<>();

                for (int i = 0; i < array.length(); i++) {
                    Hospital_Utils util = new Hospital_Utils();
                    jsonObject = array.optJSONObject(i);
                    util.setAdded_on(jsonObject.optString("added_on", ""));
                    util.setAddress(jsonObject.optString("address", ""));
                    util.setCity(jsonObject.optString("city", ""));
                    util.setHospital_name(jsonObject.optString("hospital_name", ""));
                    util.setHospital_type(jsonObject.optString("hospital_type", ""));
                    util.setId(jsonObject.optString("id", ""));
                    util.setLocality(jsonObject.optString("locality", ""));
                    util.setPincode(jsonObject.optString("pincode", ""));
                    util.setState(jsonObject.optString("state", ""));

                    if (!hospital_types.contains(util.getHospital_type()))
                        hospital_types.add(util.getHospital_type());

                    hospital_utilses.add(util);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean parseOTP(String resp) {
        boolean isLogin = false;
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj == null || obj.optString("success").equalsIgnoreCase("false")) {
                isLogin = false;
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                isLogin = true;
                jsonObject = jsonObject.optJSONObject("data");
                SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putString("otp", jsonObject.optString("otp_code"));
                editor.putString("mob_no", jsonObject.optString("mobile_no"));
                editor.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            isLogin = false;
        }
        return isLogin;
    }

    public void parseOrganization(String resp) {
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj == null || obj.optString("success").equalsIgnoreCase("false")) {
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                obj = jsonObject.optJSONObject("data");
                APIUtility.organizationModel.setAutoLogoutTime(obj.optString("session_expiry_time", "72000"));
                APIUtility.organizationModel.setDataClearTime(obj.optString("data_clear_time", "72000"));
                APIUtility.organizationModel.setShowModuleAddedOn(obj.optString("show_module_added_on", "no"));
                APIUtility.organizationModel.setAppVersion(obj.optString("android_app_version", ""));
                APIUtility.organizationModel.setBranch_app_version(obj.optString("branch_app_version", ""));
                APIUtility.organizationModel.setModuleAddedOnLabel(obj.optString("module_added_on_label", "Added On"));
                APIUtility.organizationModel.setModuleCompleteByLabel(obj.optString("module_complete_by_label", "Complete By"));
                APIUtility.organizationModel.setAdminEmail(obj.optString("admin_email", ""));
                APIUtility.organizationModel.setHome_temp(obj.optString("home_temp", ""));
                APIUtility.organizationModel.setLanguagePreference(obj.optString("language_preference", ""));
                APIUtility.organizationModel.setShowModuleEndDate(obj.optString("show_module_end_date", ""));
                APIUtility.organizationModel.setNotificationStartTime(obj.optString("notification_start_time", "8:00:00"));
                APIUtility.organizationModel.setNotificationEndTime(obj.optString("notification_end_time", "8:00:00"));
                APIUtility.organizationModel.setAdminEmail(obj.optString("admin_email", "20:00:00"));
                APIUtility.organizationModel.setEnableScreenshot(obj.optString("enable_screen_shot", "yes"));
                APIUtility.organizationModel.setQueryCategory(obj.optString("query_category"));
                APIUtility.organizationModel.setSuggestionQuery(obj.optString("suggestion_category"));
                APIUtility.organizationModel.setSessionExtend(obj.optString("session_extend", "yes"));
                APIUtility.organizationModel.setIsForceUpdateOptionally(obj.optString("force_update_optionally", "no"));
                APIUtility.organizationModel.setNotificationTimer(Long.parseLong(obj.optString("notification_timer", "21600")));

                /*APIUtility.AUTOLOGOUT_TIME = obj.optString("session_expiry_time", "72");
                APIUtility.AUTOLOGOUT_TIME_WITH_REMOVE_DATA = obj.optString("data_clear_time", "72");
                APIUtility.SHOW_MODULE_ADDES_ON = obj.optString("show_module_added_on", "no");
                APIUtility.SHOW_MATERIAL_ADDED_ON = obj.optString("show_material_added_on", "YES");
                APIUtility.ANDROID_APP_VERSION = obj.optString("android_app_version", "");
                APIUtility.MODULE_ADDED_ON_LABEL = obj.optString("module_added_on_label", "Added On");
                APIUtility.MODULE_COMPLETE_BY_LABEL = obj.optString("module_complete_by_label", "Complete By");
                APIUtility.CONTACT_ADMIN = obj.optString("admin_email", "");
                APIUtility.LANGUAGE_PREFERENCE = obj.optString("language_preference", "");
                APIUtility.SHOW_MODULE_END_DATE = obj.optString("show_module_end_date", "");
                APIUtility.NOTIFICATION_START_TIME = obj.optString("notification_start_time", "8:00:00");
                APIUtility.NOTIFICATION_END_TIME = obj.optString("notification_end_time", "20:00:00");
                APIUtility.TAKE_SCREENSHORT = obj.optString("enable_screen_shot", "yes");
                APIUtility.QUERY_CATEGORIES = obj.optString("query_category");
                APIUtility.SUGGESTION_CATEGORIES = obj.optString("suggestion_category");
                APIUtility.SESSION_EXTENED = obj.optString("session_extend");
                APIUtility.SESSION_EXTENED = obj.optString("session_extend");
                try {
                    APIUtility.NOTIFICATION_TIMER = Long.parseLong(obj.optString("notification_timer", "21600")) * 60;
                } catch (Exception e) {
                    APIUtility.NOTIFICATION_TIMER = 21600;
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ContentInShortsModel> parseContentInShortsData(String resp) {
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    ArrayList<ContentInShortsModel> contentInShortsModelArrayList = new ArrayList<>();
                    jsonObject = jsonObject.getJSONObject("data");
                    if (jsonObject != null) {
                        String template = jsonObject.optString("material_templete");
                        JSONArray contentArray = jsonObject.optJSONArray("content_inshort_array");
                        if (contentArray != null) {
                            for (int i = 0; i < contentArray.length(); i++) {
                                JSONObject contentObject = (JSONObject) contentArray.get(i);
                                if (contentObject != null) {
                                    contentInShortsModelArrayList.add(new ContentInShortsModel(contentObject.optString("content_inshort_id"), contentObject.optString("title"), contentObject.optString("description"),
                                            contentObject.optString("content_type"), contentObject.optString("media"), template));
                                }
                            }
                            return contentInShortsModelArrayList;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public boolean isLogined(String resp) {
        boolean isLogin = false;
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj == null || obj.optString("success").equalsIgnoreCase("false")) {
                isLogin = false;
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                isLogin = true;
                jsonObject = jsonObject.optJSONObject("data");
                mLoginUtility.setBranch_id(jsonObject.optString("branch_id"));
                mLoginUtility.setWelcome_msg(jsonObject.optString("welcome_message"));
                mLoginUtility.setBranch_name(jsonObject.optString("branch_name"));
                mLoginUtility.setBranch_image(jsonObject.optString("branch_image"));
                mLoginUtility.setCobrand_msg(jsonObject.optString("cobrand_msg"));
                mLoginUtility.setCircle(jsonObject.optString("circle"));
                mLoginUtility.setDesignation(jsonObject.optString("designation"));
                mLoginUtility.setEmail(jsonObject.optString("email"));
                mLoginUtility.setEmployee_id(jsonObject.optString("employee_id"));
                mLoginUtility.setFirstname(jsonObject.optString("firstname"));
//                mLoginUtility.setLastname(jsonObject.optString("lastname"));
                mLoginUtility.setLoggedin(jsonObject.optString("loggedin"));
                mLoginUtility.setOrganization_id(jsonObject.optString("organization_id"));
                mLoginUtility.setOrganization_name(jsonObject.optString("organization_name"));
                mLoginUtility.setParticipation_type(jsonObject.optString("participation_type"));
                mLoginUtility.setPassword(jsonObject.optString("password"));
                mLoginUtility.setPhone(jsonObject.optString("mobile_no"));
                mLoginUtility.setPhoto(jsonObject.optString("photo"));
                mLoginUtility.setRegion(jsonObject.optString("region"));
                mLoginUtility.setRole_id(jsonObject.optString("role_id"));
//                mLoginUtility.setUnit(jsonObject.optString("unit"));
//                mLoginUtility.setUrn(jsonObject.optString("urn"));
                mLoginUtility.setUser_id(jsonObject.optString("user_id"));
                mLoginUtility.setBranch_mixpanel_token(jsonObject.optString("branch_mixpanel_token"));

                mLoginUtility.setBranch_color1(jsonObject.optString("branch_color1"));
                mLoginUtility.setBranch_color2(jsonObject.optString("branch_color2"));
                mLoginUtility.setOrganization_color1(jsonObject.optString("organization_color1"));
                mLoginUtility.setOrganization_color2(jsonObject.optString("organization_color2"));

                mLoginUtility.setZone(jsonObject.optString("zone"));
                String coinsCollected = jsonObject.optString("coins_allocated");
                if (TextUtils.isEmpty(coinsCollected))
                    mLoginUtility.setCoins_collected("0");
                else
                    mLoginUtility.setCoins_collected(coinsCollected);

                APIUtility.SECOND_LANGUAGE_PREFERENCE = jsonObject.optString("second_language_preference", "");
                JSONObject deviceJson = jsonObject.optJSONObject("device");
                if (deviceJson != null) {
                    LoginUtility.Device device = mLoginUtility.new Device();
                    device.setAdded_on(deviceJson.optString("added_on"));
                    device.setApp_id(deviceJson.optString("app_id"));
                    device.setDevice_id(deviceJson.optString("device_id"));
                    device.setDevice_type(deviceJson.optString("device_type"));
                    device.setPush_token(deviceJson.optString("push_token"));
                    mLoginUtility.setDevice(device);
                }
                JSONObject sessionJson = jsonObject.optJSONObject("session");
                if (sessionJson != null) {
                    LoginUtility.Session session = mLoginUtility.new Session();
                    session.setSession_id(sessionJson.optString("session_id"));
                    session.setLoggedin_time(sessionJson.optString("loggedin_time"));
                    session.setSession_token(sessionJson.optString("session_token"));
                    APIUtility.SESSION = sessionJson.optString("session_token");
                    mLoginUtility.setSession(session);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isLogin = false;
        }
//        APIUtility.ROLE_ID=mLoginUtility.getRole_id();
        return isLogin;
    }

    public SignupModel isSignupData(String resp) {
        boolean isSignup = false;
        DEVICE_ERRORMSG = "";
        SignupModel signupModel = new SignupModel();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj == null || obj.optString("success").equalsIgnoreCase("false")) {
                isSignup = false;
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                signupModel.setMsg(obj.optString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return signupModel;
    }

    public boolean isProfileData(Context context, String resp) {

        boolean isLogin = false;
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj == null || obj.optString("success").equalsIgnoreCase("false")) {
                isLogin = false;
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                isLogin = true;
                jsonObject = jsonObject.optJSONObject("data");
                mLoginUtility.setDesignation(jsonObject.optString("designation"));
                mLoginUtility.setEmail(jsonObject.optString("email"));
                mLoginUtility.setWelcome_msg(jsonObject.optString("welcome_message"));
                mLoginUtility.setEmployee_id(jsonObject.optString("employee_id"));
                mLoginUtility.setFirstname(jsonObject.optString("firstname"));
                mLoginUtility.setOrganization_id(jsonObject.optString("organization_id"));
//                mLoginUtility.setBranch_id(jsonObject.optString("branch_id"));
                mLoginUtility.setOrganization_name(jsonObject.optString("organization_name"));
                mLoginUtility.setParticipation_type(jsonObject.optString("participation_type"));
                mLoginUtility.setPhone(jsonObject.optString("mobile_no"));
                mLoginUtility.setPhoto(jsonObject.optString("photo"));
                mLoginUtility.setRole_id(jsonObject.optString("role_id"));
                mLoginUtility.setUser_id(jsonObject.optString("user_id"));
                mLoginUtility.setTotal_users(jsonObject.optString("total_users"));
                String coinsCollected = jsonObject.optString("coins_allocated");
                mLoginUtility.setRank(jsonObject.optString("rank"));
                mLoginUtility.setRankMessage(jsonObject.optString("rank_message"));
                mLoginUtility.setShowMessage(jsonObject.optString("show_message"));
                if (TextUtils.isEmpty(coinsCollected))
                    mLoginUtility.setCoins_collected("0");
                else
                    mLoginUtility.setCoins_collected(coinsCollected);

                /*SharedPreferences.Editor editor = SplashActivity.mPref.edit();
                editor.putString("rank", mLoginUtility.getRank());
                editor.putString("coins_allocated", mLoginUtility.getCoins_collected());
                editor.commit();*/
                APIUtility.SECOND_LANGUAGE_PREFERENCE = jsonObject.optString("second_language_preference", "");

                DataBase dataBase = DataBase.getInstance(context);
                dataBase.addProfileData(WebServices.mLoginUtility.getUser_id(), resp);

            }
        } catch (Exception e) {
            e.printStackTrace();
            isLogin = false;
        }
        return isLogin;
    }

    public static void updateTotalCoins(Context context, String coins) {
        DataBase dataBase = DataBase.getInstance(context);
        if (SplashActivity.mPref != null) {
            String id = SplashActivity.mPref.getString("id", "");
            String pass = SplashActivity.mPref.getString("pass", "");
            String resp = dataBase.getLoginData(id, pass);
            boolean isLogin = false;

            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    JSONObject dataObject = jsonObject.optJSONObject("data");
                    if (dataObject != null) {
                        String coinsCollected = dataObject.optString("coins_allocated");
                        if (!TextUtils.isEmpty(coinsCollected)) {
                            int totalCoins = Integer.parseInt(coinsCollected) + Integer.parseInt(coins);
                            mLoginUtility.setCoins_collected(String.valueOf(totalCoins));
                            dataObject.remove("coins_allocated");
                            dataObject.put("coins_allocated", totalCoins);

                            dataBase.updateLoginData(id, pass, jsonObject.toString());
                        }

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void updateTotalCoins(DataBase dataBase, String coins) {
        if (SplashActivity.mPref != null) {
            String id = SplashActivity.mPref.getString("id", "");
            String pass = SplashActivity.mPref.getString("pass", "");
            String resp = dataBase.getLoginData(id, pass);
            boolean isLogin = false;

            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    JSONObject dataObject = jsonObject.optJSONObject("data");
                    if (dataObject != null) {
                        String coinsCollected = dataObject.optString("coins_allocated");
                        if (!TextUtils.isEmpty(coinsCollected)) {
                            int totalCoins = Integer.parseInt(coinsCollected) + Integer.parseInt(coins);
                            mLoginUtility.setCoins_collected(String.valueOf(totalCoins));
                            dataObject.remove("coins_allocated");
                            dataObject.put("coins_allocated", totalCoins);

                            dataBase.updateLoginData(id, pass, jsonObject.toString());
                        }

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Notification_Utility> getAllNotification(String resp) {
        notificationUtilityArrayList = new ArrayList<>();
        material_utils = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    jsonObject = jsonArray.optJSONObject(i);
                    Notification_Utility utility = new Notification_Utility();
                    utility.setTitle(jsonObject.optString("title", ""));
                    String msg = jsonObject.optString("message", "");
                    if (msg.contains("http://")) {
                        msg = msg.split("http://")[0];
                    }
                    utility.setMessage(msg);
                    utility.setNotification_id(jsonObject.optString("notification_id", ""));
                    utility.setNotification_type(jsonObject.optString("notification_type", ""));
                    utility.setNotification_user_id(jsonObject.optString("notification_user_id", ""));
                    utility.setSent_status(jsonObject.optString("sent_status", ""));
                    utility.setMaterial_type(jsonObject.optString("material_type", ""));
                    utility.setUrl(jsonObject.optString("url", ""));
                   /* SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");*/
                    String URL = jsonObject.optString("url", "");
                    if (URL != null && !utility.getSent_status().equalsIgnoreCase("READ"))
                        if (!URL.equalsIgnoreCase("") && URL.contains("?")) {
                            String cid = "", mid = "";
                            try {
                                Notification_Material_Util util = new Notification_Material_Util();
                                String str = URL.split("\\?")[1];
                                String str1[] = str.split("&");
                                cid = str1[0].split("=")[1];
                                String mid1 = str1[1].split("=")[1];
                                mid = mid1.split(" ")[0];
                                System.out.println("====" + cid);
                                System.out.println("====" + mid);
                                util.setCourse_id(cid);
                                util.setMaterial_id(mid);
                                util.setNotification_id(jsonObject.optString("notification_id", ""));
                                util.setNotification_user_id(jsonObject.optString("notification_user_id", ""));
                                material_utils.add(util);
                            } catch (Exception e) {

                            }
                        }

                    utility.setSent_time(jsonObject.optString("sent_time", ""));
                    notificationUtilityArrayList.add(utility);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return notificationUtilityArrayList;
    }

    public static boolean isNewMaterial(String courseid, String materialid) {
        boolean isnew = false;
        for (int i = 0; i < material_utils.size(); i++) {
            if (material_utils.get(i).getCourse_id().equalsIgnoreCase(courseid)) {
                if (material_utils.get(i).getMaterial_id().equalsIgnoreCase(materialid)) {
                    return true;
                }
            }
        }


        return isnew;

    }

    public static boolean isFavouritesMaterial(String materialid) {
        for (int i = 0; i < favourites_utilses.size(); i++) {
            if (favourites_utilses.get(i).getItem_id().equalsIgnoreCase(materialid)) {
                return true;
            }
        }
        return false;
    }

    public static String getFavouriteid(String materialid) {
        for (int i = 0; i < favourites_utilses.size(); i++) {
            if (favourites_utilses.get(i).getItem_id().equalsIgnoreCase(materialid)) {
                return favourites_utilses.get(i).getFavourite_id();
            }
        }
        return "";
    }

    public static boolean isNewModule(String courseid) {
        boolean isnew = false;
        for (int i = 0; i < material_utils.size(); i++) {
            if (material_utils.get(i).getCourse_id().equalsIgnoreCase(courseid)) {
                return true;
            }
        }


        return isnew;

    }

    public static int totalNewModule(String courseid) {

        int total = 0;
        for (int i = 0; i < material_utils.size(); i++) {
            if (material_utils.get(i).getCourse_id().equalsIgnoreCase(courseid)) {

                total++;
            }
        }


        return total;

    }

    public static boolean isLikeDislike(String id, String val, String type) {
        boolean isLikeDislike = false;
        for (int i = 0; i < like_utilities.size(); i++) {
            if (like_utilities.get(i).getItem_id().equalsIgnoreCase(id))
                if (like_utilities.get(i).getValue().equalsIgnoreCase(val))
                    if (like_utilities.get(i).getItem_type().equalsIgnoreCase(type)) {
                        isLikeDislike = true;
                        break;
                    }
        }

        return isLikeDislike;
    }

    public void getAllLikedItems(String resp) {
        like_utilities = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    Like_Utility like_utility = new Like_Utility();
                    like_utility.setItem_id(object.optString("item_id", ""));
                    like_utility.setItem_type(object.optString("item_type", ""));
                    like_utility.setValue(object.optString("value", ""));
                    like_utility.setComment(object.optString("comment", ""));
                    like_utilities.add(like_utility);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void getAllFavouritesItems(String resp) {
        favourites_utilses = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    Favourites_Utils like_utility = new Favourites_Utils();
                    like_utility.setItem_id(object.optString("item_id", "0"));
                    like_utility.setItem_type(object.optString("item_type", ""));
                    like_utility.setFavourite_id(object.optString("favourite_id", "0"));
                    favourites_utilses.add(like_utility);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public MeterialUtility getImageFlashcardData(String resp) {
        imageFlashcardMeterialUtilities = new MeterialUtility();

        try {
            JSONObject jsonObject = new JSONObject(resp);
            jsonObject = jsonObject.getJSONObject("data");
            imageFlashcardMeterialUtilities.setAlloted_time(jsonObject.optString("alloted_time", "0"));
            imageFlashcardMeterialUtilities.setMaterial_id(jsonObject.optString("material_id", "0"));
            imageFlashcardMeterialUtilities.setMaterial_media_id(jsonObject.optString("material_media_id", "0"));
            imageFlashcardMeterialUtilities.setModule_id(jsonObject.optString("module_id", "0"));
            imageFlashcardMeterialUtilities.setMaterial_type(jsonObject.optString("material_type", ""));
            imageFlashcardMeterialUtilities.setRead_more(jsonObject.optString("read_more", ""));
            imageFlashcardMeterialUtilities.setRead_more_required(jsonObject.optString("read_more_required", ""));
            imageFlashcardMeterialUtilities.setAvailability(jsonObject.optString("availability", ""));
            imageFlashcardMeterialUtilities.setDescription(jsonObject.optString("description", ""));
            imageFlashcardMeterialUtilities.setInstruction(jsonObject.optString("instruction", ""));
            imageFlashcardMeterialUtilities.setTitle(jsonObject.optString("title", ""));
            imageFlashcardMeterialUtilities.setKeywords(jsonObject.optString("keywords", ""));
            imageFlashcardMeterialUtilities.setNegative_marks(jsonObject.optString("negative_marks", "0"));
            imageFlashcardMeterialUtilities.setNo_of_question(jsonObject.optString("no_of_question", "0"));
            imageFlashcardMeterialUtilities.setNo_of_attempt(jsonObject.optString("no_of_attempt", "0"));
            imageFlashcardMeterialUtilities.setIs_incremented(jsonObject.optString("is_incremented", ""));
            imageFlashcardMeterialUtilities.setIs_same_score(jsonObject.optString("is_same_score", ""));
            imageFlashcardMeterialUtilities.setShow_score(jsonObject.optString("show_score", ""));
            imageFlashcardMeterialUtilities.setSort_order(jsonObject.optString("sort_order", ""));
            imageFlashcardMeterialUtilities.setTest_pattern(jsonObject.optString("test_pattern", ""));
            imageFlashcardMeterialUtilities.setOption_sequence(jsonObject.optString("option_sequence", ""));
            imageFlashcardMeterialUtilities.setQuestion_sequence(jsonObject.optString("question_sequence", ""));
            JSONArray jsonArray = jsonObject.optJSONArray("image_card_array");
            ArrayList<FlashCardUtility> utilities = new ArrayList<>();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    FlashCardUtility cardUtility = new FlashCardUtility();
                    JSONObject object = jsonArray.getJSONObject(i);
                    cardUtility.setAnswer(object.optString("answer", "0"));
                    cardUtility.setCorrect_option(object.optString("correct_option", ""));
                    cardUtility.setFlash_card_id(object.optString("image_card_id", "0"));
                    cardUtility.setIs_answer_image(object.optString("is_answer_image", ""));
                    cardUtility.setQuestion_description(object.optString("question_description", "0"));
                    cardUtility.setQuestion_name(object.optString("question_name", ""));
                    cardUtility.setIs_question_image(object.optString("is_question_image", ""));
                    cardUtility.setAnswer_image_url(object.optString("answer_image_url", ""));
                    cardUtility.setQuestion_description_image_url(object.optString("question_description_image_url", ""));
                    cardUtility.setTime(object.optString("time", "30"));
                    cardUtility.setOpen(false);
                    utilities.add(cardUtility);

                }
                imageFlashcardMeterialUtilities.setFlashCardUtilities(utilities);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return imageFlashcardMeterialUtilities;
    }

    public ArrayList<CourseUtility> getAssignCourseData(String resp) {
        ArrayList<CourseUtility> courseUtilities = new ArrayList<>();
        String courseName;
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.optJSONObject(i);
                CourseUtility courseUtility = new CourseUtility();
                courseUtility.setAbout_course(object.optString("about_course", ""));  //e
                courseUtility.setLink(object.optString("link", "")); //e
                courseUtility.setCourse_type(object.optString("course_type", ""));   //e
                courseUtility.setAssign_course_id(object.optString("course_id", ""));
                courseUtility.setCourse_color(object.optString("gadget_color", ""));
                /*String assignCourseModuleId = object.optString("assign_course_module_id", "");
                courseUtility.setAssign_course_module_id(assignCourseModuleId);*/
                String courseId = object.optString("course_id", "");  //e
                String isFlowingCourseInCourse = object.optString("is_flowing_course");
                courseUtility.setCourse_id(courseId);
                courseName = object.optString("course_name", "");  //e
                courseUtility.setCourse_name(courseName);
                courseUtility.setCurrency(object.optString("currency", ""));
                courseUtility.setEnd_date(object.optString("end_date", ""));
                courseUtility.setImage(object.optString("image", "")); //e
//                courseUtility.setIs_featured(object.optString("is_featured", ""));
//                courseUtility.setIs_trending(object.optString("is_trending", ""));
                courseUtility.setPrice(object.optString("price", ""));
                courseUtility.setShow_status(object.optString("show_status", "")); //e
                courseUtility.setSort_order(object.optString("sort_order", ""));  //e
                courseUtility.setStart_date(object.optString("start_date", ""));
                String size = "0";
                if (!object.optString("course_size", "0").equalsIgnoreCase("0")) {
                    double ts = (double) Long.parseLong(object.optString("course_size", "0")) / (1024 * 1024);
                    String tss = String.valueOf(ts);
                    String[] sp = tss.split("\\.");
                    size = sp[0] + "." + sp[1].charAt(0);
                }
                if (courseUtility.getCourse_type().equalsIgnoreCase("PRODUCT")) {
                    JSONArray modulesArraylist = object.optJSONArray("modules");
                    ArrayList<ModulesUtility> modulesUtilities = new ArrayList<>();

                    if (modulesArraylist != null) {
                        for (int j = 0; j < modulesArraylist.length(); j++) {
                            ModulesUtility modulesUtility = new ModulesUtility();
                            JSONObject object1 = modulesArraylist.optJSONObject(j);
                            if (object1 != null) {
                                modulesUtility.setAvailability(object1.optString("availability", "")); //e
                                modulesUtility.setCourse_id(courseId);
                                String isFlowingCourse = object1.optString("is_flowing_course");

                                if (!TextUtils.isEmpty(isFlowingCourseInCourse) && isFlowingCourseInCourse.equalsIgnoreCase("yes"))
                                    isFlowingCourse = isFlowingCourseInCourse;
                                modulesUtility.setIs_flowing_course(isFlowingCourse);

                                modulesUtility.setInfo(object1.optString("info", "")); //e
                                modulesUtility.setCourse_name(courseName);
                                modulesUtility.setIs_incremented(object1.optString("is_incremented", "")); //e
                                modulesUtility.setModule_id(object1.optString("module_id", ""));  //e
                                modulesUtility.setModule_image(object1.optString("module_image", "")); //e
                                String moduleName = object1.optString("module_name", "");
                                modulesUtility.setModule_name(moduleName);  //e
                                modulesUtility.setSort_order(object1.optString("sort_order", ""));  //e
                                modulesUtility.setShow_status(object1.optString("show_status", "")); //e
                                modulesUtility.setCourse_size(size);
                                JSONArray materialArraylist = object1.optJSONArray("material");
                                ArrayList<MeterialUtility> meterialUtilities = new ArrayList<>();
                                int totalquiz = 0, totalpdf = 0, totalvideo = 0, totalflashcard = 0, totalaudio = 0, totalelearning = 0;
                                if (materialArraylist != null) {
                                    for (int k = 0; k < materialArraylist.length(); k++) {
                                        MeterialUtility meterialUtility = new MeterialUtility();
                                        int maxCoins = 0;
                                        JSONObject object2 = materialArraylist.optJSONObject(k);
                                        JSONObject coinsObject = object2.optJSONObject("coins_allocated"); //e
                                        if (coinsObject != null) {

                                            ArrayList fromAl = new ArrayList<>();
                                            ArrayList toAl = new ArrayList<>();
                                            ArrayList coinsAl = new ArrayList<>();
                                            JSONArray fromArray = coinsObject.optJSONArray("range_from");
                                            JSONArray toArray = coinsObject.optJSONArray("range_to");
                                            JSONArray coinsArray = coinsObject.optJSONArray("coins");
                                            String rule_type = coinsObject.optString("rule_type");
                                            String redeem = coinsObject.optString("redeem");
                                            String earnCoins = coinsObject.optString("earn_coins");
                                            for (int a = 0; a < fromArray.length(); a++) {
                                                fromAl.add(Integer.parseInt(fromArray.optString(a)));
                                            }
                                            for (int b = 0; b < toArray.length(); b++) {
                                                toAl.add(Integer.parseInt(toArray.optString(b)));
                                            }

                                            for (int c = 0; c < coinsArray.length(); c++) {
                                                int coins = Integer.parseInt(coinsArray.optString(c));
                                                coinsAl.add(coins);
                                                if (maxCoins < coins)
                                                    maxCoins = coins;
                                            }

                                            CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
                                            coinsAllocatedModel.setCoinsAl(coinsAl);
                                            coinsAllocatedModel.setToAl(toAl);
                                            coinsAllocatedModel.setFromAl(fromAl);
                                            coinsAllocatedModel.setRule_type(rule_type);
                                            coinsAllocatedModel.setRedeem(redeem);
                                            coinsAllocatedModel.setMaxCoins(String.valueOf(maxCoins));
                                            coinsAllocatedModel.setEarnCoins(earnCoins);
                                            meterialUtility.setCoinsAllocatedModel(coinsAllocatedModel);

                                        }
                                        meterialUtility.setCourse_id(courseId);
                                        meterialUtility.setCourse_name(courseName);
                                        meterialUtility.setTotal_coins(String.valueOf(maxCoins));
                                        meterialUtility.setModuleName(moduleName);
                                        // new
                                        meterialUtility.setAssign_material_id(object2.optString("assign_material_id"));
                                        meterialUtility.setIs_result_published(object2.optString("result_publish"));
                                        meterialUtility.setStart_date(object2.optString("start_date"));
                                        meterialUtility.setEnd_date(object2.optString("end_date"));
                                        meterialUtility.setShow_rating(object2.optString("show_rating"));
                                        meterialUtility.setAccess_status(object2.optString("access_status"));
                                        meterialUtility.setIsResultPublishRequired(object.optString("required_result_publish"));
                                        meterialUtility.setAccess_device(object2.optString("access_device"));
                                        meterialUtility.setEach_ques_time(object2.optString("each_ques_time"));
                                        meterialUtility.setIsShareable(object2.optString("is_shareable"));
                                        meterialUtility.setLinkOpenOutside(object2.optString("show_outside"));
                                        meterialUtility.setMaterial_templete(object2.optString("material_templete", "")); //e
//                                meterialUtility.setAvailability(object2.optString("availability", ""));
                                        meterialUtility.setAlloted_time(object2.optString("alloted_time", "")); //e
//                                        meterialUtility.setIs_accessed(object2.optString("is_accessed", "")); //e
                                        meterialUtility.setDescription(object2.optString("description", "")); //e
//                                meterialUtility.setKeywords(object2.optString("keywords", ""));
                                        meterialUtility.setTitle(object2.optString("title", "")); //e
                                        meterialUtility.setAdded_on(object2.optString("added_on", "0000-00-00 00:00:00")); //e

                                        meterialUtility.setTest_pattern(object2.optString("test_pattern", "")); //e
                                        meterialUtility.setInstruction(object2.optString("instruction", "")); //e
                                        meterialUtility.setIs_incremented(object2.optString("is_incremented", "")); //e
                                        meterialUtility.setIs_same_score(object2.optString("is_same_score", "")); //e
                                        meterialUtility.setModule_id(object2.optString("module_id", "")); //e
                                        meterialUtility.setMaterial_id(object2.optString("material_id", ""));  //e
                                        meterialUtility.setMaterial_image(object2.optString("icon", "")); //e
                                        meterialUtility.setRateNum(object2.optString("rate", "")); //e
                                        meterialUtility.setIs_flowing_course(isFlowingCourse);
                                        meterialUtility.setMedia_type(object2.optString("media_type")); //e
                                        meterialUtility.setShow_vertical(object2.optString("show_vertical")); //e
                                        meterialUtility.setVertical_level(object2.optString("vertical_level")); //e

                                        String sequence = object2.optString("material_sequence"); //e

                                        if (!TextUtils.isEmpty(sequence)) {
                                            meterialUtility.setMaterialSequence(QuizUtils.convertStringToAl(sequence));
                                        }
                                        if (object2.optString("total_like", "").equalsIgnoreCase("null")) //e
                                            meterialUtility.setTotal_like("0");
                                        else
                                            meterialUtility.setTotal_like(object2.optString("total_like", ""));
                                        meterialUtility.setIs_offline_available(object2.optString("is_offline_available", "")); //e
                                        if (object2.optJSONObject("material_media") != null) {
                                            meterialUtility.setMaterial_media_file_url(object2.optJSONObject("material_media").optString("file_url", ""));
                                            meterialUtility.setMaterial_media_file_type(object2.optJSONObject("material_media").optString("type", ""));
                                            meterialUtility.setScrom_url(object2.optJSONObject("material_media").optString("file_url2", ""));
                                            String size1 = "0";
                                            if (!object2.optJSONObject("material_media").optString("file_size", "0").equalsIgnoreCase("0") && !object2.optJSONObject("material_media").optString("file_size", "0").equalsIgnoreCase("null")) {
                                                double ts = (double) Long.parseLong(object2.optJSONObject("material_media").optString("file_size", "0")) / (1024 * 1024);
                                                String tss = String.valueOf(ts);
                                                String[] sp = tss.split("\\.");
                                                size1 = sp[0] + "." + sp[1].charAt(0);
                                            }

                                            meterialUtility.setFile_size(size1);
                                        } else {
                                            meterialUtility.setScrom_url("");
                                            meterialUtility.setMaterial_media_file_url("");

                                        }
//                                meterialUtility.setMaterial_media_id(object2.optString("material_media_id", ""));
                                        meterialUtility.setMaterial_type(object2.optString("material_type", "")); //e
                                        meterialUtility.setNegative_marks(object2.optString("negative_marks", "")); //e
                                        meterialUtility.setNo_of_attempt(object2.optString("no_of_attempt", "")); //e
                                        meterialUtility.setNo_of_question(object2.optString("no_of_question", "")); //e
                                        meterialUtility.setOption_sequence(object2.optString("option_sequence", "")); //e
                                        meterialUtility.setQuestion_sequence(object2.optString("question_sequence", "")); //e
//                                meterialUtility.setRead_more(object2.optString("read_more", ""));
//                                meterialUtility.setRead_more_required(object2.optString("read_more_required", ""));
                                        meterialUtility.setShow_status(object2.optString("show_status", "")); //e
                                        meterialUtility.setSort_order(object2.optString("sort_order", "")); //e
                                        meterialUtility.setShow_score(object2.optString("show_score", "")); //e
                                        meterialUtility.setShow_answer(object2.optString("show_answer", "")); //e
                                        meterialUtility.setShow_leaderboard(object2.optString("show_leaderboard", "")); //e
                                        meterialUtility.setPassing_percentage(object2.optString("passing_percentage", "")); //e
                                        meterialUtility.setLast_updated_time(object2.optString("modified_on", "")); //e

                                        meterialUtility.setEndpoint(object2.optString("endpoint"));
                                        meterialUtility.setShow_vertical(object2.optString("show_vertical"));
                                        meterialUtility.setVertical_level(object2.optString("vertical_level"));
                                        meterialUtility.setRegistration(object2.optString("registration"));
                                        meterialUtility.setAuth(object2.optString("auth"));
                                        meterialUtility.setTincan_flag(object2.optString("tincan_flag"));
                                        meterialUtility.setShow_certificate(object2.optString("show_certificate", ""));
                                        meterialUtility.setTincan_url(object2.optString("tincan_url", ""));
                                        String submoduleId = object2.optString("sub_module_id", "");
                                        JSONArray subModuleNameArray = object2.optJSONArray("all_submodule_names");
                                        JSONArray subModuleIdsArray = object2.optJSONArray("all_submodule_ids");
                                        if (subModuleNameArray != null && subModuleIdsArray != null && subModuleIdsArray.length() == subModuleNameArray.length()) {
                                            ArrayList<ModulesUtility> subModuleArraylist = new ArrayList<>();
                                            for (int p = 0; p < subModuleIdsArray.length(); p++) {
                                                ModulesUtility subMoudle = new ModulesUtility();
                                                subMoudle.setModule_id((String) subModuleIdsArray.get(p));
                                                subMoudle.setModule_name((String) subModuleNameArray.get(p));
                                                if (modulesUtility.getSubModuleArraylist() != null && modulesUtility.getSubModuleArraylist().size() > 0) {
                                                    for (int i1 = 0; i1 < modulesUtility.getSubModuleArraylist().size(); i1++) {
                                                        if (!subMoudle.getModule_id().equals(modulesUtility.getSubModuleArraylist().get(i1).getModule_id()))
                                                            subModuleArraylist.add(subMoudle);
                                                    }
                                                } else
                                                    subModuleArraylist.add(subMoudle);

                                            }
                                        }

                                        JSONObject object3 = object2.optJSONObject("rating");
                                        if (object3 != null) {
                                            meterialUtility.setAverage_rating(object3.optString("average_rating", "0.0"));
                                            meterialUtility.setPeople_rated(object3.optString("people_rated", "0.0"));
                                        } else {
                                            meterialUtility.setAverage_rating("0");
                                            meterialUtility.setPeople_rated("0");
                                        }

                                        if (!TextUtils.isEmpty(submoduleId)) {
                                            if (modulesUtility.getSubModuleArraylist() != null && modulesUtility.getSubModuleArraylist().size() > 0) {
                                                for (int i1 = 0; i1 < modulesUtility.getSubModuleArraylist().size(); i1++) {
                                                    if (!submoduleId.equals(modulesUtility.getSubModuleArraylist().get(i1).getModule_id()))
                                                        modulesUtility.getSubModuleArraylist().get(i1).getMeterialUtilityArrayList().add(meterialUtility);
                                                }
                                            }
                                        } else
                                            meterialUtilities.add(meterialUtility);
                                        if (meterialUtility.getMaterial_type().equalsIgnoreCase("PDF") || meterialUtility.getMaterial_type().equalsIgnoreCase("Full Text")) {
                                            totalpdf++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("quiz")) {
                                            totalquiz++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("video")) {
                                            totalvideo++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("Flashcard")) {
                                            totalflashcard++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("Audio")) {
                                            totalaudio++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("flash")) {
                                            totalelearning++;
                                        }

                                    }
                                }
                                modulesUtility.setTotalPDF("" + totalpdf);
                                modulesUtility.setTotalQuiz("" + totalquiz);
                                modulesUtility.setTotalFlashCard("" + totalflashcard);
                                modulesUtility.setTotalVideo("" + totalvideo);
                                modulesUtility.setTotalAudio("" + totalaudio);
                                modulesUtility.setTotalElearning("" + totalelearning);
                                modulesUtility.setMeterialUtilityArrayList(meterialUtilities);
                                if (meterialUtilities.size() > 0)
                                    modulesUtilities.add(modulesUtility);
                            }
                        }
                    }

                    courseUtility.setModulesUtilityArrayList(modulesUtilities);
                } else {
                    JSONArray modulesArraylist = object.optJSONArray("modules");
//                JSONObject object31=object.optJSONObject("rating");
//                if (object31!=null){
//                    courseUtility.setAverage_rating(object31.optString("average_rating", "0.0"));
//                    courseUtility.setPeople_rated(object31.optString("people_rated", "0.0"));
//                }
                    ArrayList<ModulesUtility> modulesUtilities = new ArrayList<>();

                    if (modulesArraylist != null) {
                        for (int j = 0; j < modulesArraylist.length(); j++) {
                            ModulesUtility modulesUtility = new ModulesUtility();
                            JSONObject object1 = modulesArraylist.optJSONObject(j);
//                        modulesUtility.setAuthor_name(object1.optString("author_name", ""));
                            if (object1 != null) {
                                modulesUtility.setAvailability(object1.optString("availability", "")); //e
//                        String courseId = object1.optString("course_id", "");
                                modulesUtility.setCourse_id(courseId);
                                String isFlowingCourse = object1.optString("is_flowing_course");

                                if (!TextUtils.isEmpty(isFlowingCourseInCourse) && isFlowingCourseInCourse.equalsIgnoreCase("yes"))
                                    isFlowingCourse = isFlowingCourseInCourse;
                                modulesUtility.setIs_flowing_course(isFlowingCourse);
//                        modulesUtility.setCurrency(object1.optString("currency", ""));
//                        modulesUtility.setKeywords(object1.optString("keywords", ""));
                                modulesUtility.setInfo(object1.optString("info", "")); //e
                                modulesUtility.setCourse_name(courseName);
//                        modulesUtility.setInfo_title(object1.optString("info_title", ""));
                                modulesUtility.setIs_incremented(object1.optString("is_incremented", "")); //e
                                modulesUtility.setModule_id(object1.optString("module_id", ""));  //e
                                modulesUtility.setModule_image(object1.optString("module_image", "")); //e
//                        modulesUtility.setModule_media_id(object1.optString("module_media_id", ""));
                                String moduleName = object1.optString("module_name", "");
                                modulesUtility.setModule_name(moduleName);  //e
//                        modulesUtility.setPrice(object1.optString("price", ""));
                                modulesUtility.setSort_order(object1.optString("sort_order", ""));  //e
                                modulesUtility.setShow_status(object1.optString("show_status", "")); //e
                                modulesUtility.setCourse_size(size);
                                JSONArray materialArraylist = object1.optJSONArray("material");
                                ArrayList<MeterialUtility> meterialUtilities = new ArrayList<>();

                                int totalquiz = 0, totalpdf = 0, totalvideo = 0, totalflashcard = 0, totalaudio = 0, totalelearning = 0;
                                if (materialArraylist != null) {
                                    for (int k = 0; k < materialArraylist.length(); k++) {
                                        MeterialUtility meterialUtility = new MeterialUtility();
                                        int maxCoins = 0;
                                        JSONObject object2 = materialArraylist.optJSONObject(k);
                                        JSONObject coinsObject = object2.optJSONObject("coins_allocated"); //e
                                        if (coinsObject != null) {

                                            ArrayList fromAl = new ArrayList<>();
                                            ArrayList toAl = new ArrayList<>();
                                            ArrayList coinsAl = new ArrayList<>();
                                            JSONArray fromArray = coinsObject.optJSONArray("range_from");
                                            JSONArray toArray = coinsObject.optJSONArray("range_to");
                                            JSONArray coinsArray = coinsObject.optJSONArray("coins");
                                            String rule_type = coinsObject.optString("rule_type");
                                            String redeem = coinsObject.optString("redeem");
                                            String earnCoins = coinsObject.optString("earn_coins");
                                            for (int a = 0; a < fromArray.length(); a++) {
                                                fromAl.add(Integer.parseInt(fromArray.optString(a)));
                                            }
                                            for (int b = 0; b < toArray.length(); b++) {
                                                toAl.add(Integer.parseInt(toArray.optString(b)));
                                            }

                                            for (int c = 0; c < coinsArray.length(); c++) {
                                                int coins = Integer.parseInt(coinsArray.optString(c));
                                                coinsAl.add(coins);
                                                if (maxCoins < coins)
                                                    maxCoins = coins;
                                            }

                                            CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
                                            coinsAllocatedModel.setCoinsAl(coinsAl);
                                            coinsAllocatedModel.setToAl(toAl);
                                            coinsAllocatedModel.setFromAl(fromAl);
                                            coinsAllocatedModel.setRule_type(rule_type);
                                            coinsAllocatedModel.setRedeem(redeem);
                                            coinsAllocatedModel.setMaxCoins(String.valueOf(maxCoins));
                                            coinsAllocatedModel.setEarnCoins(earnCoins);
                                            meterialUtility.setCoinsAllocatedModel(coinsAllocatedModel);

                                        }
                                        meterialUtility.setCourse_id(courseId);
                                        meterialUtility.setCourse_name(courseName);
                                        meterialUtility.setTotal_coins(String.valueOf(maxCoins));
                                        meterialUtility.setModuleName(moduleName);
                                        // new
                                        meterialUtility.setAssign_material_id(object2.optString("assign_material_id"));
                                        meterialUtility.setIs_result_published(object2.optString("result_publish"));
                                        meterialUtility.setStart_date(object2.optString("start_date"));
                                        meterialUtility.setEnd_date(object2.optString("end_date"));
                                        meterialUtility.setAccess_status(object2.optString("access_status"));
                                        meterialUtility.setAccess_device(object2.optString("access_device"));
                                        meterialUtility.setEach_ques_time(object2.optString("each_ques_time"));

                                        meterialUtility.setMaterial_templete(object2.optString("material_templete", "")); //e
//                                meterialUtility.setAvailability(object2.optString("availability", ""));
                                        meterialUtility.setAlloted_time(object2.optString("alloted_time", "")); //e
                                        meterialUtility.setDescription(object2.optString("description", "")); //e
//                                meterialUtility.setKeywords(object2.optString("keywords", ""));
                                        meterialUtility.setTitle(object2.optString("title", "")); //e
                                        meterialUtility.setAdded_on(object2.optString("added_on", "0000-00-00 00:00:00")); //e

                                        meterialUtility.setTest_pattern(object2.optString("test_pattern", "")); //e
                                        meterialUtility.setInstruction(object2.optString("instruction", "")); //e
                                        meterialUtility.setIs_incremented(object2.optString("is_incremented", "")); //e
                                        meterialUtility.setIs_same_score(object2.optString("is_same_score", "")); //e
                                        meterialUtility.setModule_id(object2.optString("module_id", "")); //e
                                        meterialUtility.setMaterial_id(object2.optString("material_id", ""));  //e
                                        meterialUtility.setMaterial_image(object2.optString("icon", "")); //e
                                        meterialUtility.setRateNum(object2.optString("rate", "")); //e
                                        meterialUtility.setIs_flowing_course(isFlowingCourse);
                                        meterialUtility.setMedia_type(object2.optString("media_type")); //e
                                        String sequence = object2.optString("material_sequence"); //e
                                        if (!TextUtils.isEmpty(sequence)) {
                                            meterialUtility.setMaterialSequence(QuizUtils.convertStringToAl(sequence));
                                        }
                                        if (object2.optString("total_like", "").equalsIgnoreCase("null")) //e
                                            meterialUtility.setTotal_like("0");
                                        else
                                            meterialUtility.setTotal_like(object2.optString("total_like", ""));
                                        meterialUtility.setIs_offline_available(object2.optString("is_offline_available", "")); //e
                                        if (object2.optJSONObject("material_media") != null) {
                                            meterialUtility.setMaterial_media_file_url(object2.optJSONObject("material_media").optString("file_url", ""));
                                            meterialUtility.setMaterial_media_file_type(object2.optJSONObject("material_media").optString("type", ""));
                                            meterialUtility.setScrom_url(object2.optJSONObject("material_media").optString("file_url2", ""));
                                            String size1 = "0";
                                            if (!object2.optJSONObject("material_media").optString("file_size", "0").equalsIgnoreCase("0") && !object2.optJSONObject("material_media").optString("file_size", "0").equalsIgnoreCase("null")) {
                                                double ts = (double) Long.parseLong(object2.optJSONObject("material_media").optString("file_size", "0")) / (1024 * 1024);
                                                String tss = String.valueOf(ts);
                                                String[] sp = tss.split("\\.");
                                                size1 = sp[0] + "." + sp[1].substring(0, 2);
                                            }

                                            meterialUtility.setFile_size(size1);
                                        } else {
                                            meterialUtility.setScrom_url("");
                                            meterialUtility.setMaterial_media_file_url("");

                                        }
//                                meterialUtility.setMaterial_media_id(object2.optString("material_media_id", ""));
                                        meterialUtility.setMaterial_type(object2.optString("material_type", "")); //e
                                        meterialUtility.setNegative_marks(object2.optString("negative_marks", "")); //e
                                        meterialUtility.setNo_of_attempt(object2.optString("no_of_attempt", "")); //e
                                        meterialUtility.setNo_of_question(object2.optString("no_of_question", "")); //e
                                        meterialUtility.setOption_sequence(object2.optString("option_sequence", "")); //e
                                        meterialUtility.setQuestion_sequence(object2.optString("question_sequence", "")); //e
//                                meterialUtility.setRead_more(object2.optString("read_more", ""));
//                                meterialUtility.setRead_more_required(object2.optString("read_more_required", ""));
                                        meterialUtility.setShow_status(object2.optString("show_status", "")); //e
                                        meterialUtility.setSort_order(object2.optString("sort_order", "")); //e
                                        meterialUtility.setShow_score(object2.optString("show_score", "")); //e
                                        meterialUtility.setShow_answer(object2.optString("show_answer", "")); //e
                                        meterialUtility.setShow_leaderboard(object2.optString("show_leaderboard", "")); //e
                                        meterialUtility.setPassing_percentage(object2.optString("passing_percentage", "")); //e
                                        meterialUtility.setLast_updated_time(object2.optString("modified_on", "")); //e

                                        meterialUtility.setEndpoint(object2.optString("endpoint"));
                                        meterialUtility.setShow_vertical(object2.optString("show_vertical"));
                                        meterialUtility.setVertical_level(object2.optString("vertical_level"));
                                        meterialUtility.setRegistration(object2.optString("registration"));
                                        meterialUtility.setAuth(object2.optString("auth"));
                                        meterialUtility.setTincan_flag(object2.optString("tincan_flag"));
                                        meterialUtility.setShow_certificate(object2.optString("show_certificate", ""));
                                        meterialUtility.setTincan_url(object2.optString("tincan_url", ""));

                                        JSONObject object3 = object2.optJSONObject("rating");
                                        if (object3 != null) {
                                            meterialUtility.setAverage_rating(object3.optString("average_rating", "0.0"));
                                            meterialUtility.setPeople_rated(object3.optString("people_rated", "0.0"));
                                        } else {
                                            meterialUtility.setAverage_rating("0");
                                            meterialUtility.setPeople_rated("0");
                                        }
                                        meterialUtilities.add(meterialUtility);
                                        if (meterialUtility.getMaterial_type().equalsIgnoreCase("PDF") || meterialUtility.getMaterial_type().equalsIgnoreCase("Full Text")) {
                                            totalpdf++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("quiz")) {
                                            totalquiz++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("video")) {
                                            totalvideo++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("Flashcard")) {
                                            totalflashcard++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("Audio")) {
                                            totalaudio++;
                                        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("flash")) {
                                            totalelearning++;
                                        }

                                    }
                                }
                                modulesUtility.setTotalPDF("" + totalpdf);
                                modulesUtility.setTotalQuiz("" + totalquiz);
                                modulesUtility.setTotalFlashCard("" + totalflashcard);
                                modulesUtility.setTotalVideo("" + totalvideo);
                                modulesUtility.setTotalAudio("" + totalaudio);
                                modulesUtility.setTotalElearning("" + totalelearning);
                                modulesUtility.setMeterialUtilityArrayList(meterialUtilities);
                                if (meterialUtilities.size() > 0)
                                    modulesUtilities.add(modulesUtility);
                            }
                        }
                    }

                    courseUtility.setModulesUtilityArrayList(modulesUtilities);

                    JSONArray surveyArraylist = object.optJSONArray("survey");
                    ArrayList<SurveyUtils> surveyUtilses = new ArrayList<>();
                    if (surveyArraylist != null) {
                        for (int j = 0; j < surveyArraylist.length(); j++) {
                            JSONObject object1 = surveyArraylist.optJSONObject(j);
                            SurveyUtils surveyUtils = new SurveyUtils();
                            surveyUtils.setAlloted_time(object1.optString("alloted_time", "0"));
                            surveyUtils.setDescription(object1.optString("description", ""));
                            surveyUtils.setLast_updated_time(object1.optString("last_updated_time", ""));
                            surveyUtils.setSort_order(object1.optString("sort_order", ""));
                            surveyUtils.setSurvey_id(object1.optString("survey_id", ""));
                            surveyUtils.setTitle(object1.optString("title", ""));
                            if (object1.optString("title").equalsIgnoreCase("survey")) {
//                            if (SplashActivity.mPref.getBoolean("takesurvey", true))
                                surveyUtilses.add(surveyUtils);
                            } else {
//                            if (SplashActivity.mPref.getBoolean("takefeedback", true))
                                surveyUtilses.add(surveyUtils);
                            }
                        }
                    }
                    courseUtility.setSurveyUtilses(surveyUtilses);

                    if (courseUtility.getModulesUtilityArrayList() != null && courseUtility.getModulesUtilityArrayList().size() > 0)
                        courseUtilities.add(courseUtility);
                    else {
                        if (courseUtility.getCourse_type().equalsIgnoreCase("DISCUSSION") || courseUtility.getCourse_type().equalsIgnoreCase("SUGGESTION_QUERY") || courseUtility.getCourse_type().equalsIgnoreCase("link"))
                            courseUtilities.add(courseUtility);
                    }
//                CourseUtility course = new CourseUtility();
//                courseUtility.setAbout_course("");
//                courseUtility.setAssign_course_id("");
//                courseUtility.setCourse_id("");
//                courseUtility.setCourse_name("Discussion");
//                courseUtility.setCurrency("");
//                courseUtility.setEnd_date("");
//                courseUtility.setImage("");
//                courseUtility.setIs_featured("");
//                courseUtility.setIs_trending("");
//                courseUtility.setPrice("");
//                courseUtility.setShow_status("");
//                courseUtility.setSort_order("");
//                courseUtility.setStart_date("");
//                courseUtilities.add(course);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return courseUtilities;
    }


    public ArrayList<Quiz_Response_Util> quiz_response_method(String response) {

        ArrayList<Quiz_Response_Util> mQuiz_response_arraylist = new ArrayList<Quiz_Response_Util>();
        try {
            JSONObject jsonObject;
            jsonObject = new JSONObject(response);
            JSONObject object = jsonObject.optJSONObject("status");
            response = object.optString("success");
            if (response.equalsIgnoreCase("true")) {
                Log.d(" Response:", "Successfull");
            } else {
                Log.d(" Response:", "sorry");
            }

            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object3 = jsonArray.optJSONObject(i);
                    Quiz_Response_Util Quiz_data_util = new Quiz_Response_Util();
                    Quiz_data_util.setResponse_id(object3.optString("response_id", ""));
                    Quiz_data_util.setUser_id(object3.optString("user_id", ""));
                    Quiz_data_util.setMaterial_id(object3.optString("material_id", ""));
                    Quiz_data_util.setTitle(object3.optString("title", ""));
                    Quiz_data_util.setTest_pattern(object3.optString("test_pattern", ""));
                    Quiz_data_util.setCourse_name(object3.optString("course_name", ""));
                    Quiz_data_util.setResponse_type(object3.optString("response_type", ""));
                    Quiz_data_util.setResult(object3.optString("result", ""));
                    Quiz_data_util.setCorrect_question(object3.optString("correct_question", ""));
                    Quiz_data_util.setIncorrect_question(object3.optString("incorrect_question", ""));
                    Quiz_data_util.setTaken_device(object3.optString("taken_device", ""));
                    Quiz_data_util.setTime_taken(object3.optString("time_taken", ""));
                    Quiz_data_util.setStart_time(object3.optString("start_time", ""));
                    Quiz_data_util.setFinish_time(object3.optString("finish_time", ""));
                    Quiz_data_util.setAdded_on(object3.optString("added_on", ""));

                    ArrayList<Question_Response_util> mQuestion_response_arraylist = new ArrayList<Question_Response_util>();
                    Question_Response_util mQuestionUtil = new Question_Response_util();
                    JSONArray jsonArray1 = object3.optJSONArray("question_response");
                    if (jsonArray1 != null) {
                        for (int j = 0; j < jsonArray1.length(); j++) {
                            JSONObject object4 = jsonArray1.optJSONObject(i);
                            mQuestionUtil.setQuestion_response_id(object4.optString("question_response_id", ""));
                            mQuestionUtil.setResponse_id(object4.optString("response_id", ""));
                            mQuestionUtil.setQuestion_bank_id(object4.optString("question_bank_id", ""));
                            mQuestionUtil.setAssign_question_id(object4.optString("assign_question_id", ""));
                            ArrayList<String> answerAl = new ArrayList<>();
                            answerAl.add(object4.optString("answer_key", ""));
                            mQuestionUtil.setAnswer_key(answerAl);
                            mQuestionUtil.setTime_taken(object4.optString("time_taken", ""));
                            mQuestionUtil.setMarks(object4.optString("marks", ""));
                            mQuestion_response_arraylist.add(mQuestionUtil);
                            Quiz_data_util.setmQuestionResponseArraylist(mQuestion_response_arraylist);
                            mQuiz_response_arraylist.add(Quiz_data_util);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return mQuiz_response_arraylist;
    }

    public ArrayList<ReadResponseUtility> read_response_method(String response) {
        ArrayList<ReadResponseUtility> list = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject jsonObject2 = jsonObject.optJSONObject("status");
            jsonObject2.optString("success");
            JSONArray array = jsonObject.optJSONArray("data");

            if (array != null) {
                //exclude passing_percentage in parsing
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject3 = array.optJSONObject(i);
                    ReadResponseUtility readresponse = new ReadResponseUtility();
                    readresponse.setResponse_id(jsonObject3.optString("response_id", ""));
                    readresponse.setAssign_material_id(jsonObject3.optString("assign_material_id", ""));
                    readresponse.setUser_id(jsonObject3.optString("user_id", ""));
                    readresponse.setMaterial_id(jsonObject3.optString("material_id", ""));
                    readresponse.setResponse_type(jsonObject3.optString("response_type", ""));
                    readresponse.setResult(jsonObject3.optString("result", ""));
                    readresponse.setCorrect_question(jsonObject3.optString("correct_question", ""));
                    readresponse.setIncorrect_question(jsonObject3.optString("incorrect_question", ""));
                    readresponse.setUnanswered_question(jsonObject3.optString("unanswered_question", ""));
                    readresponse.setTaken_device(jsonObject3.optString("taken_device", ""));
                    readresponse.setTime_taken(jsonObject3.optString("time_taken", ""));
                    readresponse.setStart_time(jsonObject3.optString("start_time", ""));
                    readresponse.setFinish_time(jsonObject3.optString("finish_time", ""));
                    readresponse.setAdded_on(jsonObject3.optString("added_on", ""));
                    readresponse.setTitle(jsonObject3.optString("title", ""));
                    readresponse.setTest_pattern(jsonObject3.optString("test_pattern", ""));
                    readresponse.setCourse_name(jsonObject3.optString("course_name", ""));
                    readresponse.setCoins_allocated(jsonObject3.optString("coins_allocated", ""));
                    readresponse.setOrganization_id(jsonObject3.optString("organization_id", ""));
                    readresponse.setBranch_id(jsonObject3.optString("branch_id", ""));
                    readresponse.setModuleId(jsonObject3.optString("module_id", ""));
                    readresponse.setSeen_count(jsonObject3.optString("seen_count", ""));
                    readresponse.setTotal_count(jsonObject3.optString("total_count", ""));
                    readresponse.setCompletion_per(jsonObject3.optString("completion_per", ""));
                    readresponse.setAttempt_taken(jsonObject3.optString("attempt_completed", ""));
                    readresponse.setMax_score(jsonObject3.optString("max_result", ""));
                    readresponse.setMy_rating(jsonObject3.optString("my_rating", ""));
                    readresponse.setAvg_rating(jsonObject3.optString("average_rating", ""));
                    readresponse.setScrom_status(jsonObject3.optString("scorm_status"));
                    list.add(readresponse);

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ReadResponseUtility parseSubmitResponse(String resp) {
        ReadResponseUtility readresponse = new ReadResponseUtility();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            jsonObject = jsonObject.optJSONObject("data");

            readresponse.setResponse_id(jsonObject.optString("response_id", ""));
//            readresponse.setAssign_course_id(jsonObject.optString("assign_course_id", ""));
            readresponse.setAssign_material_id(jsonObject.optString("assign_material_id", ""));
            readresponse.setUser_id(jsonObject.optString("user_id", ""));
            readresponse.setMaterial_id(jsonObject.optString("material_id", ""));
            readresponse.setResponse_type(jsonObject.optString("response_type", ""));
            readresponse.setResult(jsonObject.optString("result", ""));
            readresponse.setTaken_device(jsonObject.optString("taken_device", ""));
            readresponse.setTime_taken(jsonObject.optString("time_taken", ""));
            readresponse.setStart_time(jsonObject.optString("start_time", ""));
            readresponse.setCorrect_question(jsonObject.optString("correct_question", ""));
            readresponse.setIncorrect_question(jsonObject.optString("incorrect_question", ""));
            readresponse.setTest_pattern(jsonObject.optString("test_pattern", ""));
            readresponse.setTitle(jsonObject.optString("title", ""));
            readresponse.setFinish_time(jsonObject.optString("finish_time", ""));
            readresponse.setAdded_on(jsonObject.optString("added_on", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return readresponse;

    }

    public ArrayList<ReadResponseUtility> parsespecificquiz(String response) {
        ArrayList<ReadResponseUtility> list = new ArrayList<ReadResponseUtility>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            jsonObject = jsonObject.optJSONObject("data");
            JSONArray array = jsonObject.optJSONArray("response");

            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject3 = array.optJSONObject(i);
                    ReadResponseUtility readresponse = new ReadResponseUtility();
                    readresponse.setFirstname(jsonObject3.optString("firstname", ""));
                    readresponse.setLastname(jsonObject3.optString("lastname", ""));
                    readresponse.setUser_id(jsonObject3.optString("user_id", ""));
                    readresponse.setMaterial_id(jsonObject3.optString("material_id", ""));
                    readresponse.setResponse_type(jsonObject3.optString("response_type", ""));
                    readresponse.setResult(jsonObject3.optString("result", ""));
                    readresponse.setTaken_device(jsonObject3.optString("taken_device", ""));
                    if (jsonObject3.optString("time_taken", "").equalsIgnoreCase("1"))
                        readresponse.setTime_taken("120");
                    else
                        readresponse.setTime_taken(jsonObject3.optString("time_taken", ""));
                    readresponse.setStart_time(jsonObject3.optString("start_time", ""));
                    readresponse.setFinish_time(jsonObject3.optString("finish_time", ""));
                    readresponse.setAdded_on(jsonObject3.optString("added_on", ""));
                    readresponse.setCorrect_question(jsonObject3.optString("correct_question", ""));
                    readresponse.setIncorrect_question(jsonObject3.optString("incorrect_question", ""));
                    readresponse.setUnanswered_question(jsonObject3.optString("unanswered_question", ""));
//
//                        if (readresponse.getStart_time().equalsIgnoreCase(readresponse.getFinish_time()))
//                            readresponse.setTotal_Time(900);
//                        else
//                            readresponse.setTotal_Time(diffDate(readresponse.getStart_time(),readresponse.getFinish_time()));

                    list.add(readresponse);

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<Quiz_Response_Util> parsespecificquiz2(String response) {
        ArrayList<Quiz_Response_Util> mQuiz_response_arraylist = new ArrayList<Quiz_Response_Util>();
        try {
            JSONObject jsonObject = new JSONObject(response);
//            jsonObject = jsonObject.optJSONObject("data");

            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {

                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    JSONObject object3 = jsonArray.optJSONObject(i);
                    Quiz_Response_Util Quiz_data_util = new Quiz_Response_Util();
                    Quiz_data_util.setResponse_id(object3.optString("response_id", ""));
                    Quiz_data_util.setUser_id(object3.optString("user_id", ""));
                    Quiz_data_util.setMaterial_id(object3.optString("material_id", ""));
                    Quiz_data_util.setTitle(object3.optString("title", ""));
                    Quiz_data_util.setTest_pattern(object3.optString("test_pattern", ""));
                    Quiz_data_util.setCourse_name(object3.optString("course_name", ""));
                    Quiz_data_util.setResponse_type(object3.optString("response_type", ""));
                    Quiz_data_util.setResult(object3.optString("result", ""));
                    Quiz_data_util.setCorrect_question(object3.optString("correct_question", ""));
                    Quiz_data_util.setIncorrect_question(object3.optString("incorrect_question", ""));
                    Quiz_data_util.setTaken_device(object3.optString("taken_device", ""));
                    Quiz_data_util.setTime_taken(object3.optString("time_taken", ""));
                    Quiz_data_util.setStart_time(object3.optString("start_time", ""));
                    Quiz_data_util.setFinish_time(object3.optString("finish_time", ""));
                    Quiz_data_util.setAdded_on(object3.optString("added_on", ""));

                    ArrayList<Question_Response_util> mQuestion_response_arraylist = new ArrayList<Question_Response_util>();

                    JSONArray jsonArray1 = object3.optJSONArray("question_response");
                    if (jsonArray1 != null) {
                        for (int j = 0; j < jsonArray1.length(); j++) {
                            Question_Response_util mQuestionUtil = new Question_Response_util();
                            JSONObject object4 = jsonArray1.optJSONObject(j);
                            mQuestionUtil.setQuestion_response_id(object4.optString("question_response_id", ""));
                            mQuestionUtil.setResponse_id(object4.optString("response_id", ""));
                            mQuestionUtil.setQuestion_bank_id(object4.optString("question_bank_id", ""));
                            mQuestionUtil.setAssign_question_id(object4.optString("assign_question_id", ""));
                            ArrayList<String> answerAl = new ArrayList<>();
                            answerAl.add(object4.optString("answer_key", ""));
                            mQuestionUtil.setAnswer_key(answerAl);
                            mQuestionUtil.setTime_taken(object4.optString("time_taken", ""));
                            mQuestionUtil.setMarks(object4.optString("marks", ""));
                            mQuestion_response_arraylist.add(mQuestionUtil);


                        }
                    }
                    Quiz_data_util.setmQuestionResponseArraylist(mQuestion_response_arraylist);
                    mQuiz_response_arraylist.add(Quiz_data_util);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mQuiz_response_arraylist;
    }

    public static long diffDate(String dateStart, String dateStop) {
        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.println(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            return diffSeconds;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public MeterialUtility getFlashcardData(String resp) {
        flashcardMeterialUtilities = new MeterialUtility();

        try {
            JSONObject jsonObject = new JSONObject(resp);
            jsonObject = jsonObject.optJSONObject("data");
            flashcardMeterialUtilities.setAlloted_time(jsonObject.optString("alloted_time", "0"));
            flashcardMeterialUtilities.setMaterial_id(jsonObject.optString("material_id", "0"));
//            flashcardMeterialUtilities.setMaterial_media_id(jsonObject.optString("material_media_id", "0"));
            flashcardMeterialUtilities.setModule_id(jsonObject.optString("module_id", "0"));
            flashcardMeterialUtilities.setMaterial_type(jsonObject.optString("material_type", ""));
            flashcardMeterialUtilities.setMaterial_templete(jsonObject.optString("material_templete", ""));
//            flashcardMeterialUtilities.setRead_more(jsonObject.optString("read_more", ""));
//            flashcardMeterialUtilities.setRead_more_required(jsonObject.optString("read_more_required", ""));
//            flashcardMeterialUtilities.setAvailability(jsonObject.optString("availability", ""));
            flashcardMeterialUtilities.setDescription(jsonObject.optString("description", ""));
            flashcardMeterialUtilities.setInstruction(jsonObject.optString("instruction", ""));
            flashcardMeterialUtilities.setTitle(jsonObject.optString("title", ""));
//            flashcardMeterialUtilities.setKeywords(jsonObject.optString("keywords", ""));
            flashcardMeterialUtilities.setNegative_marks(jsonObject.optString("negative_marks", "0"));
            flashcardMeterialUtilities.setNo_of_question(jsonObject.optString("no_of_question", "0"));
            flashcardMeterialUtilities.setNo_of_attempt(jsonObject.optString("no_of_attempt", "0"));
            flashcardMeterialUtilities.setIs_incremented(jsonObject.optString("is_incremented", ""));
            flashcardMeterialUtilities.setIs_same_score(jsonObject.optString("is_same_score", ""));
            flashcardMeterialUtilities.setShow_score(jsonObject.optString("show_score", ""));
            flashcardMeterialUtilities.setSort_order(jsonObject.optString("sort_order", ""));
            flashcardMeterialUtilities.setTest_pattern(jsonObject.optString("test_pattern", ""));
            flashcardMeterialUtilities.setOption_sequence(jsonObject.optString("option_sequence", ""));
            flashcardMeterialUtilities.setQuestion_sequence(jsonObject.optString("question_sequence", ""));
            JSONArray jsonArray = jsonObject.optJSONArray("flash_card_array");
            ArrayList<FlashCardUtility> utilities = new ArrayList<>();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    FlashCardUtility cardUtility = new FlashCardUtility();
                    JSONObject object = jsonArray.optJSONObject(i);
                    cardUtility.setAnswer(object.optString("answer", "0"));

                    cardUtility.setQuestionNo((i + 1) + "");
                    String correctOption = object.optString("correct_option", "");
                    if (TextUtils.isEmpty(correctOption))
                        cardUtility.setCorrect_option("1");
                    else
                        cardUtility.setCorrect_option(correctOption);
                    cardUtility.setFlash_card_id(object.optString("flash_card_id", "0"));
                    cardUtility.setIs_answer_image(object.optString("is_answer_image", ""));
                    cardUtility.setQuestion_description(object.optString("question_description", "0"));
                    cardUtility.setQuestion_name(object.optString("question_name", ""));
                    cardUtility.setIs_question_image(object.optString("is_question_image", ""));
                    cardUtility.setAnswer_image_url(object.optString("answer_image_url", ""));
                    cardUtility.setQuestion_description_image_url(object.optString("question_description_image_url", ""));
                    utilities.add(cardUtility);

                }
                flashcardMeterialUtilities.setFlashCardUtilities(utilities);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return flashcardMeterialUtilities;
    }
    public ArrayList<FlashCardUtility> getNewFlashcardData(String resp) {
        ArrayList<FlashCardUtility> utilities = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            jsonObject = jsonObject.optJSONObject("data");
            JSONArray jsonArray = jsonObject.optJSONArray("flash_card_array");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    FlashCardUtility cardUtility = new FlashCardUtility();
                    JSONObject object = jsonArray.optJSONObject(i);
                    cardUtility.setAnswer(object.optString("answer", "0"));

                    cardUtility.setQuestionNo((i + 1) + "");
                    String correctOption = object.optString("correct_option", "");
                    if (TextUtils.isEmpty(correctOption))
                        cardUtility.setCorrect_option("1");
                    else
                        cardUtility.setCorrect_option(correctOption);
                    cardUtility.setFlash_card_id(object.optString("flash_card_id", "0"));
                    cardUtility.setIs_answer_image(object.optString("is_answer_image", ""));
                    cardUtility.setQuestion_description(object.optString("question_description", "0"));
                    cardUtility.setQuestion_name(object.optString("question_name", ""));
                    cardUtility.setIs_question_image(object.optString("is_question_image", ""));
                    cardUtility.setAnswer_image_url(object.optString("answer_image_url", ""));
                    cardUtility.setQuestion_description_image_url(object.optString("question_description_image_url", ""));
                    utilities.add(cardUtility);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return utilities;
    }
    public boolean isValid(String resp) {
        boolean isValid = false;
        DEVICE_ERRORMSG = "";
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                JSONObject obj = jsonObject.optJSONObject("status");
                if (obj.optString("success").equalsIgnoreCase("false")) {
                    isValid = false;
                    ERRORMSG = obj.optString("errormsg");
                    DEVICE_ERRORMSG = obj.optString("errorkey", "");
                } else {
                    isValid = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
                isValid = false;
                ERRORMSG = "";
            }
        }
        return isValid;
    }

    public String getErrorCode(String resp) {
        String errorcode = "";
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                JSONObject obj = jsonObject.optJSONObject("status");
                if (obj.optString("success").equalsIgnoreCase("false")) {
                    ERRORMSG = obj.optString("errormsg");
                    errorcode = obj.optString("errormsg_code", "");
                }
            } catch (Exception e) {
                e.printStackTrace();
                ERRORMSG = "";
            }
        }
        return errorcode;
    }

    public String organization_img(String resp) {
        String resp2 = "";
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj.optString("success").equalsIgnoreCase("false")) {
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                obj = jsonObject.optJSONObject("data");
                resp2 = obj.optString("organization_logo_base_64");
            }

        } catch (Exception e) {
            e.printStackTrace();
            ERRORMSG = "";
        }
        return resp2;
    }

    public boolean changePassword(String resp) {
        boolean isValid = false;
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj.optString("success").equalsIgnoreCase("false")) {
                isValid = false;
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                obj = jsonObject.optJSONObject("data");
                ERRORMSG = obj.optString("msg");
                isValid = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            isValid = false;
            ERRORMSG = "Encountered some error! Please try again.";
        }
        return isValid;
    }

    public boolean forgetPassword(String resp) {
        boolean isValid = false;
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj.optString("success").equalsIgnoreCase("false")) {
                isValid = false;
                ERRORMSG = obj.optString("errormsg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                isValid = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            isValid = false;
            ERRORMSG = "Encountered some error! Please try again.";
        }
        return isValid;
    }

    public boolean isEmailFound(String resp) {
        boolean isValid = false;
        DEVICE_ERRORMSG = "";
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONObject obj = jsonObject.optJSONObject("data");
            if (obj.optString("email_found").equalsIgnoreCase("true")) {
                isValid = true;
                ERRORMSG = obj.optString("msg");
                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else isValid = false;
        } catch (Exception e) {
            e.printStackTrace();
            isValid = false;
            ERRORMSG = "Encountered some error! Please try again.";
        }
        return isValid;
    }

    public String readResponse(HttpResponse res) {
        InputStream is = null;
        String return_text = "";
        try {
            is = res.getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line = "";
            StringBuffer sb = new StringBuffer();
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return_text = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return return_text;

    }

    public void addSubmitResponse(DataBase dataBase, MeterialUtility meterialUtility, String result, String corr_ques, String incorr_ques, String quiz_resp, String coins_allocated, String redeem, String organization_id, String branch_id) {
        ReadResponseUtility readresponse = new ReadResponseUtility();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        meterialUtility.setMaterialEndTime(df.format(c.getTime()));
        readresponse.setResponse_id("");
        readresponse.setCourse_id(meterialUtility.getCourse_id());
        readresponse.setUser_id(WebServices.mLoginUtility.getUser_id());
        readresponse.setMaterial_id(meterialUtility.getMaterial_id());
        readresponse.setResponse_type(meterialUtility.getMaterial_type());
        readresponse.setResult(result);
        readresponse.setTaken_device(APIUtility.DEVICE);
        readresponse.setTime_taken(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()));
        meterialUtility.setTimeTaken(Utils.diffDate(meterialUtility.getMaterialStartTime(), meterialUtility.getMaterialEndTime()));
        readresponse.setStart_time(meterialUtility.getMaterialStartTime());
        readresponse.setCorrect_question(corr_ques);
        readresponse.setIncorrect_question(incorr_ques);
        readresponse.setTest_pattern(meterialUtility.getTest_pattern());
        readresponse.setTitle(meterialUtility.getTitle());
        readresponse.setFinish_time(meterialUtility.getMaterialEndTime());
        readresponse.setQuiz_response(quiz_resp);
        readresponse.setCoins_allocated(coins_allocated);
        readresponse.setRedeem(redeem);
        readresponse.setModuleId(meterialUtility.getModule_id());
        readresponse.setAssign_material_id(meterialUtility.getAssign_material_id());
        readresponse.setSeen_count(meterialUtility.getSeen_count());
        readresponse.setTotal_count(meterialUtility.getTotal_count());
        readresponse.setCompletion_per(meterialUtility.getComplete_per());
        readresponse.setMax_score(meterialUtility.getMax_result());
        readresponse.setOrganization_id(organization_id);
        readresponse.setBranch_id(branch_id);
        dataBase.addGetResponseData(readresponse);
        //call set progress in offline mode for particular course
        setProgress(dataBase, meterialUtility.getCourse_id(), false);
    }

    public ArrayList<SurveyDataUtil> parseSurveyData(String resp) {
        ArrayList<SurveyDataUtil> arrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);

            jsonObject = jsonObject.optJSONObject("data");
            JSONArray array = jsonObject.optJSONArray("question_array");
            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    SurveyDataUtil util = new SurveyDataUtil();
                    jsonObject = array.optJSONObject(i);
                    util.setAdded_on(jsonObject.optString("added_on", ""));
                    util.setError_message(jsonObject.optString("error_message", ""));
                    util.setExplanation(jsonObject.optString("explanation", ""));
                    util.setOption1(jsonObject.optString("option1", ""));
                    util.setOption1_type(jsonObject.optString("option1_type", ""));
                    util.setOption1_emoji(jsonObject.optInt("option1_emoji"));

                    util.setOption2(jsonObject.optString("option2", ""));
                    util.setOption2_type(jsonObject.optString("option2_type", ""));
                    util.setOption2_emoji(jsonObject.optInt("option2_emoji"));

                    util.setOption3(jsonObject.optString("option3", ""));
                    util.setOption3_type(jsonObject.optString("option3_type", ""));
                    util.setOption3_emoji(jsonObject.optInt("option3_emoji"));


                    util.setOption4(jsonObject.optString("option4", ""));
                    util.setOption4_type(jsonObject.optString("option4_type", ""));
                    util.setOption4_emoji(jsonObject.optInt("option4_emoji"));

                    util.setOption5(jsonObject.optString("option5", ""));
                    util.setOption5_type(jsonObject.optString("option5_type", ""));
                    util.setOption5_emoji(jsonObject.optInt("option5_emoji"));

                    util.setQuestion_description(jsonObject.optString("question_description", ""));
                    util.setQuestion_description_type(jsonObject.optString("question_description_type", ""));
                    util.setQuestion_hint(jsonObject.optString("question_hint", ""));
                    util.setQuestion_id(jsonObject.optString("question_id", ""));
                    util.setQuestion_type(jsonObject.optString("question_type", ""));
                    arrayList.add(util);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public QuestionUtility parseQuestionData(String resp) {
        QuestionUtility questionUtility = new QuestionUtility();
        try {
            JSONObject jsonObject = new JSONObject(resp);

            jsonObject = jsonObject.optJSONObject("data");
            questionUtility.setMaterial_id(jsonObject.optString("material_id"));
            questionUtility.setMaterial_media_id(jsonObject.optString("material_media_id"));
            questionUtility.setModule_id(jsonObject.optString("module_id"));
            questionUtility.setTitle(jsonObject.optString("title"));
            questionUtility.setDescription(jsonObject.optString("description"));
            questionUtility.setInstruction(jsonObject.optString("instruction"));
            questionUtility.setMaterial_type(jsonObject.optString("material_type"));
            questionUtility.setKeywords(jsonObject.optString("keywords"));
            questionUtility.setRead_more(jsonObject.optString("read_more"));
            questionUtility.setRead_more_required(jsonObject.optString("read_more_required"));
            questionUtility.setAvailability(jsonObject.optString("availability"));
            questionUtility.setNo_of_attempt(jsonObject.optString("no_of_attempt"));
            questionUtility.setIs_same_score(jsonObject.optString("is_same_score"));
            questionUtility.setNegative_marks(jsonObject.optString("negative_marks"));
            questionUtility.setAlloted_time(jsonObject.optString("alloted_time"));
            questionUtility.setTest_pattern(jsonObject.optString("test_pattern"));
            questionUtility.setQuestion_sequence(jsonObject.optString("question_sequence"));
            questionUtility.setOption_sequence(jsonObject.optString("option_sequence"));
            questionUtility.setNo_of_question(jsonObject.optString("no_of_question"));
            questionUtility.setShow_score(jsonObject.optString("show_score"));
            questionUtility.setShow_answer(jsonObject.optString("show_answer", "no"));
            questionUtility.setShow_status(jsonObject.optString("show_status"));
            questionUtility.setIs_incremented(jsonObject.optString("is_incremented"));
            questionUtility.setSort_order(jsonObject.optString("sort_order"));
            questionUtility.setPassing_percentage(jsonObject.optString("passing_percentage"));
            questionUtility.setWarning_time(jsonObject.optString("warning_time"));
            questionUtility.setWarning_message(jsonObject.optString("warning_message"));
            questionUtility.setEach_ques_warning_time(jsonObject.optString("each_que_warning_message"));
            JSONArray array = jsonObject.optJSONArray("question_array");
            JSONArray arrayVertical = jsonObject.optJSONArray("vertical");
            ArrayList<Vertical> verticalArrayList = new ArrayList<>();
            if (arrayVertical != null && arrayVertical.length() > 0) {
                for (int i = 0; i < arrayVertical.length(); i++) {
                    Vertical vertical = new Vertical();
                    vertical.setName(arrayVertical.opt(i) + "");
                    verticalArrayList.add(vertical);
                }
            }
            questionUtility.setVerticalList(verticalArrayList);

            if (array != null) {
                ArrayList<Data_util> dataUtilArrayList = new ArrayList<>();
                for (int i = 0; i < array.length(); i++) {
                    Data_util data_util = new Data_util();
                    JSONObject jsonObject5 = array.optJSONObject(i);
                    data_util.setAssign_question_id(jsonObject5.optString("assign_question_id"));
                    data_util.setQuestion_bank_id(jsonObject5.optString("question_bank_id"));
                    data_util.setCourse_id(jsonObject5.optString("course_id"));
                    data_util.setModule_id(jsonObject5.optString("module_id"));
                    data_util.setVertical(jsonObject5.optString("vertical"));
                    data_util.setChapter_id(jsonObject5.optString("chapter_id"));
                    String optionSequence = jsonObject.optString("option_sequence");
                    data_util.setQuestion_name(jsonObject5.optString("question_name"));
                    data_util.setQuestion_description(jsonObject5.optString("question_description"));
                    data_util.setQuestion_description_url(jsonObject5.optString("question_description_url"));
                    data_util.setQuestion_type(jsonObject5.optString("question_type"));
                    data_util.setGroup_id(jsonObject5.optString("group_id"));
                    data_util.setQuestion_hint(jsonObject5.optString("question_hint"));
                    data_util.setError_message(jsonObject5.optString("error_message"));
                    data_util.setExplanation(jsonObject5.optString("explanation"));
                    data_util.setIs_question_image(jsonObject5.optString("is_question_image"));

                    String oldCorrectOption = jsonObject5.optString("correct_option");
                    data_util.setRealCorrectOption(oldCorrectOption);
                    data_util.setCorrect_option_type(jsonObject5.optString("correct_option_type"));
                    data_util.setMarks(jsonObject5.optString("marks"));
                    data_util.setUser_id(jsonObject5.optString("user_id"));

                    ArrayList<AnswerModel> answerModelArrayList = new ArrayList<>();
                    if (!TextUtils.isEmpty(jsonObject5.optString("option1"))) {
                        AnswerModel answerModel = new AnswerModel();
                        answerModel.setId("1");
                        answerModel.setOption(jsonObject5.optString("option1"));
                        answerModel.setIsOptionImage(jsonObject5.optString("is_option1_image"));
                        answerModel.setOption_url(jsonObject5.optString("option1_url"));
                        answerModel.setOptionMarks(jsonObject5.optString("option1_marks"));
//                        answerModel.setOptionType("text");
                        answerModel.setOptionType(jsonObject5.optString("option1_type"));
                        answerModelArrayList.add(answerModel);
                    }
                    if (!TextUtils.isEmpty(jsonObject5.optString("option2"))) {
                        AnswerModel answerModel = new AnswerModel();
                        answerModel.setId("2");
                        answerModel.setOption(jsonObject5.optString("option2"));
                        answerModel.setOption_url(jsonObject5.optString("option2_url"));
                        answerModel.setIsOptionImage(jsonObject5.optString("is_option2_image"));
                        answerModel.setOptionMarks(jsonObject5.optString("option2_marks"));
                        answerModel.setOptionType(jsonObject5.optString("option2_type"));
                        answerModelArrayList.add(answerModel);
                    }
                    if (!TextUtils.isEmpty(jsonObject5.optString("option3"))) {
                        AnswerModel answerModel = new AnswerModel();
                        answerModel.setId("3");
                        answerModel.setOption(jsonObject5.optString("option3"));
                        answerModel.setOption_url(jsonObject5.optString("option3_url"));
                        answerModel.setIsOptionImage(jsonObject5.optString("is_option3_image"));
                        answerModel.setOptionMarks(jsonObject5.optString("option3_marks"));
                        answerModel.setOptionType(jsonObject5.optString("option3_type"));
                        answerModelArrayList.add(answerModel);
                    }
                    if (!TextUtils.isEmpty(jsonObject5.optString("option4"))) {
                        AnswerModel answerModel = new AnswerModel();
                        answerModel.setId("4");
                        answerModel.setOption(jsonObject5.optString("option4"));
                        answerModel.setOption_url(jsonObject5.optString("option4_url"));
                        answerModel.setIsOptionImage(jsonObject5.optString("is_option4_image"));
                        answerModel.setOptionMarks(jsonObject5.optString("option4_marks"));
                        answerModel.setOptionType(jsonObject5.optString("option4_type"));
                        answerModelArrayList.add(answerModel);
                    }

                    if (!TextUtils.isEmpty(optionSequence) && optionSequence.equalsIgnoreCase("RANDOM")) {
                        Collections.shuffle(answerModelArrayList);
                    }
                    String tempAnswer = "";
                    for (int k = 0; k < answerModelArrayList.size(); k++) {
                        AnswerModel answerModel = answerModelArrayList.get(k);
                        if (answerModel != null) {
                            if (k == 0) {
                                data_util.setOption1(answerModel.getOption());
                                data_util.setOption1_url(answerModel.getOption_url());
                                data_util.setOption1(answerModel.getOption());
                                data_util.setIs_option1_image(answerModel.getIsOptionImage());
                                data_util.setOption1_marks(answerModel.getOptionMarks());
                                data_util.setOption1_type(answerModel.getOptionType());
                                data_util.setOption1_real_pos(answerModel.getId());
                                if (data_util.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                    if (oldCorrectOption.equals(answerModel.getId()))
                                        data_util.setCorrect_option("1");
                                } else if (data_util.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                    if (oldCorrectOption.contains(answerModel.getId())) {
                                        if (tempAnswer.contains(","))
                                            tempAnswer = tempAnswer + "1";
                                        else
                                            tempAnswer = tempAnswer + "1,";
                                    }

                                }
                            }
                            if (k == 1) {
                                data_util.setOption2(answerModel.getOption());
                                data_util.setOption2_url(answerModel.getOption_url());
                                data_util.setIs_option2_image(answerModel.getIsOptionImage());
                                data_util.setOption2_marks(answerModel.getOptionMarks());
                                data_util.setOption2_type(answerModel.getOptionType());
                                data_util.setOption2_real_pos(answerModel.getId());
                                if (data_util.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                    if (oldCorrectOption.equals(answerModel.getId()))
                                        data_util.setCorrect_option("2");
                                } else if (data_util.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                    if (oldCorrectOption.contains(answerModel.getId())) {
                                        if (tempAnswer.contains(","))
                                            tempAnswer = tempAnswer + "2";
                                        else
                                            tempAnswer = tempAnswer + "2,";
                                    }

                                }
                            }
                            if (k == 2) {
                                data_util.setOption3(answerModel.getOption());
                                data_util.setOption3_url(answerModel.getOption_url());
                                data_util.setIs_option3_image(answerModel.getIsOptionImage());
                                data_util.setOption3_marks(answerModel.getOptionMarks());
                                data_util.setOption3_type(answerModel.getOptionType());
                                data_util.setOption3_real_pos(answerModel.getId());
                                if (data_util.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                    if (oldCorrectOption.equals(answerModel.getId()))
                                        data_util.setCorrect_option("3");
                                } else if (data_util.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                    if (oldCorrectOption.contains(answerModel.getId())) {
                                        if (tempAnswer.contains(","))
                                            tempAnswer = tempAnswer + "3";
                                        else
                                            tempAnswer = tempAnswer + "3,";
                                    }

                                }
                            }
                            if (k == 3) {
                                data_util.setOption4(answerModel.getOption());
                                data_util.setOption4_url(answerModel.getOption_url());
                                data_util.setIs_option4_image(answerModel.getIsOptionImage());
                                data_util.setOption4_marks(answerModel.getOptionMarks());
                                data_util.setOption4_type(answerModel.getOptionType());
                                data_util.setOption4_real_pos(answerModel.getId());
                                if (data_util.getQuestion_type().equalsIgnoreCase("SINGLE")) {
                                    if (oldCorrectOption.equals(answerModel.getId()))
                                        data_util.setCorrect_option("4");
                                } else if (data_util.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                    if (oldCorrectOption.contains(answerModel.getId())) {
                                        if (tempAnswer.contains(","))
                                            tempAnswer = tempAnswer + "4";
                                        else
                                            tempAnswer = tempAnswer + "4,";
                                    }

                                }
                            }

                        }

                    }
                    if (data_util.getQuestion_type().equalsIgnoreCase("MULTIPLE"))
                        data_util.setCorrect_option(tempAnswer);
                    dataUtilArrayList.add(data_util);

                }
                if (!TextUtils.isEmpty(questionUtility.getQuestion_sequence()) && questionUtility.getQuestion_sequence().equalsIgnoreCase("RANDOM"))
                    Collections.shuffle(dataUtilArrayList);

                questionUtility.setData_utils(dataUtilArrayList);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return questionUtility;
    }


    public boolean isPassQuiz(String resp, String result) {
        boolean isPass = false;
        if (result.equalsIgnoreCase("")) return false;
        if (resp.equalsIgnoreCase("") || resp.equalsIgnoreCase("null")) return false;
        try {
            if (Integer.parseInt(result) >= Integer.parseInt(resp)) {
                isPass = true;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return isPass;
    }


    public ArrayList<Data_util> getLatestData(String resp) {
        ArrayList<Data_util> dataUtilArrayList = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(resp);
            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    Data_util data_util = new Data_util();
                    JSONObject jsonObject5 = array.optJSONObject(i);
                    data_util.setAssign_question_id(jsonObject5.optString("assign_question_id"));
                    data_util.setQuestion_bank_id(jsonObject5.optString("question_bank_id"));
                    data_util.setCourse_id(jsonObject5.optString("course_id"));
                    data_util.setModule_id(jsonObject5.optString("module_id"));
                    data_util.setChapter_id(jsonObject5.optString("chapter_id"));
                    data_util.setQuestion_name(jsonObject5.optString("question_name"));
                    data_util.setQuestion_description(jsonObject5.optString("question_description"));
                    data_util.setQuestion_description_url(jsonObject5.optString("question_description_url"));
                    data_util.setQuestion_type(jsonObject5.optString("question_type"));
                    data_util.setQuestion_hint(jsonObject5.optString("question_hint"));
                    data_util.setError_message(jsonObject5.optString("error_message"));
                    data_util.setExplanation(jsonObject5.optString("explanation"));
                    data_util.setIs_question_image(jsonObject5.optString("is_question_image"));
                    data_util.setOption1(jsonObject5.optString("option1"));
                    data_util.setIs_option1_image(jsonObject5.optString("is_option1_image"));
                    data_util.setOption1_marks(jsonObject5.optString("option1_marks"));
                    data_util.setOption2(jsonObject5.optString("option2"));
                    data_util.setIs_option2_image(jsonObject5.optString("is_option2_image"));
                    data_util.setOption2_marks(jsonObject5.optString("option2_marks"));
                    data_util.setOption3(jsonObject5.optString("option3"));
                    data_util.setIs_option3_image(jsonObject5.optString("is_option3_image"));
                    data_util.setOption3_marks(jsonObject5.optString("option3_marks"));
                    data_util.setOption4(jsonObject5.optString("option4"));
                    data_util.setIs_option4_image(jsonObject5.optString("is_option4_image"));
                    data_util.setOption4_marks(jsonObject5.optString("option4_marks"));
                    data_util.setCorrect_option(jsonObject5.optString("correct_option"));
                    data_util.setMarks(jsonObject5.optString("marks"));
                    data_util.setUser_id(jsonObject5.optString("user_id"));
                    data_util.setOpt1clicked(jsonObject5.optInt("option1_click", 0));
                    data_util.setOpt2clicked(jsonObject5.optInt("option2_click", 0));
                    data_util.setOpt3clicked(jsonObject5.optInt("option3_click", 0));
                    data_util.setOpt4clicked(jsonObject5.optInt("option4_click", 0));
                    dataUtilArrayList.add(data_util);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dataUtilArrayList;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public QuestionUtility parseResumeQuestionData(DataBase dataBase, String resp) {
        QuestionUtility questionUtility = new QuestionUtility();
        try {
            JSONArray array2 = new JSONObject(dataBase.getPending_Data(WebServices.mLoginUtility.getUser_id())).optJSONArray("data");
            JSONObject object2 = array2.optJSONObject(Integer.parseInt(resp));
            JSONArray jsonArray = new JSONArray(object2.optString("response_data"));
            if (jsonArray != null)
                for (int k = 0; k < jsonArray.length(); k++) {
                    JSONObject jsonObject = jsonArray.optJSONObject(k);
                    questionUtility.setCorrectResponse(jsonObject.optString("correctResponse"));
                    questionUtility.setIncorrectResponse(jsonObject.optString("incorrectResponse"));
                    questionUtility.setResult(jsonObject.optString("result"));
                    questionUtility.setTimeTaken(jsonObject.optString("timeTaken"));
                    questionUtility.setQuestionIndex(jsonObject.optString("questionIndex"));
                    jsonObject = jsonObject.optJSONObject("attemptQuiz");

                    questionUtility.setMaterial_id(jsonObject.optString("material_id"));
                    questionUtility.setMaterial_media_id(jsonObject.optString("material_media_id"));
                    questionUtility.setModule_id(jsonObject.optString("module_id"));
                    questionUtility.setTitle(jsonObject.optString("title"));
                    questionUtility.setDescription(jsonObject.optString("description"));
                    questionUtility.setInstruction(jsonObject.optString("instruction"));
                    questionUtility.setMaterial_type(jsonObject.optString("material_type"));
                    questionUtility.setKeywords(jsonObject.optString("keywords"));
                    questionUtility.setRead_more(jsonObject.optString("read_more"));
                    questionUtility.setRead_more_required(jsonObject.optString("read_more_required"));
                    questionUtility.setAvailability(jsonObject.optString("availability"));
                    questionUtility.setNo_of_attempt(jsonObject.optString("no_of_attempt"));
                    questionUtility.setIs_same_score(jsonObject.optString("is_same_score"));
                    questionUtility.setNegative_marks(jsonObject.optString("negative_marks"));
                    questionUtility.setAlloted_time(jsonObject.optString("alloted_time"));
                    questionUtility.setTest_pattern(jsonObject.optString("test_pattern"));
                    questionUtility.setQuestion_sequence(jsonObject.optString("question_sequence"));
                    questionUtility.setOption_sequence(jsonObject.optString("option_sequence"));
                    questionUtility.setNo_of_question(jsonObject.optString("no_of_question"));
                    questionUtility.setShow_score(jsonObject.optString("show_score"));
                    questionUtility.setShow_status(jsonObject.optString("show_status"));
                    questionUtility.setIs_incremented(jsonObject.optString("is_incremented"));
                    questionUtility.setSort_order(jsonObject.optString("sort_order"));

                    JSONArray array = jsonObject.optJSONArray("question_array");
                    if (array != null) {
                        ArrayList<Data_util> dataUtilArrayList = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            Data_util data_util = new Data_util();
                            JSONObject jsonObject5 = array.optJSONObject(i);

                            data_util.setAssign_question_id(jsonObject5.optString("assign_question_id"));
                            data_util.setQuestion_bank_id(jsonObject5.optString("question_bank_id"));
                            data_util.setCourse_id(jsonObject5.optString("course_id"));
                            data_util.setModule_id(jsonObject5.optString("module_id"));
                            data_util.setChapter_id(jsonObject5.optString("chapter_id"));
                            data_util.setQuestion_name(jsonObject5.optString("question_name"));
                            data_util.setQuestion_description(jsonObject5.optString("question_description"));
                            data_util.setQuestion_type(jsonObject5.optString("question_type"));
                            data_util.setQuestion_hint(jsonObject5.optString("question_hint"));
                            data_util.setError_message(jsonObject5.optString("error_message"));
                            data_util.setExplanation(jsonObject5.optString("explanation"));
                            data_util.setIs_question_image(jsonObject5.optString("is_question_image"));
                            data_util.setOption1(jsonObject5.optString("option1"));
                            data_util.setIs_option1_image(jsonObject5.optString("is_option1_image"));
                            data_util.setOption1_marks(jsonObject5.optString("option1_marks"));
                            data_util.setOption2(jsonObject5.optString("option2"));
                            data_util.setIs_option2_image(jsonObject5.optString("is_option2_image"));
                            data_util.setOption2_marks(jsonObject5.optString("option2_marks"));
                            data_util.setOption3(jsonObject5.optString("option3"));
                            data_util.setIs_option3_image(jsonObject5.optString("is_option3_image"));
                            data_util.setOption3_marks(jsonObject5.optString("option3_marks"));
                            data_util.setOption4(jsonObject5.optString("option4"));
                            data_util.setIs_option4_image(jsonObject5.optString("is_option4_image"));
                            data_util.setOption4_marks(jsonObject5.optString("option4_marks"));
                            ArrayList<String> answerAl = new ArrayList<>();
                            answerAl.add(jsonObject5.optString("option4_marks"));
                            data_util.setUser_ans(answerAl);

                            data_util.setCorrect_option(jsonObject5.optString("my_selected_option", ""));
                            if (jsonObject5.optString("my_selected_option", "").equalsIgnoreCase("1"))
                                data_util.setOpt1clicked(1);
                            else if (jsonObject5.optString("my_selected_option", "").equalsIgnoreCase("2"))
                                data_util.setOpt2clicked(1);
                            else if (jsonObject5.optString("my_selected_option", "").equalsIgnoreCase("3"))
                                data_util.setOpt3clicked(1);
                            else if (jsonObject5.optString("my_selected_option", "").equalsIgnoreCase("4"))
                                data_util.setOpt4clicked(1);
                            data_util.setMarks(jsonObject5.optString("marks"));
                            data_util.setUser_id(jsonObject5.optString("user_id"));

                            dataUtilArrayList.add(data_util);

                        }
                        questionUtility.setData_utils(dataUtilArrayList);
                    }
                }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return questionUtility;
    }

    public void getpendingData(DataBase dataBase, String resp) {
        mGetPendingData = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    GetPendingRes_Utility utility = new GetPendingRes_Utility();
                    utility.setAdd_by(object.optString("added_by", ""));
                    utility.setMaterial_id(object.optString("material_id", ""));
                    utility.setPending_response_id(object.optString("pending_response_id", ""));
                    utility.setResponse_data(object.optString("response_data", ""));
                    utility.setResponse_status(object.optString("response_status", ""));
                    utility.setUser_id(object.optString("user_id", ""));
                    String responsedata = object.optString("response_data", "");
                    if (!responsedata.equalsIgnoreCase("")) {
                        JSONArray array = new JSONArray(responsedata);
                        if (array != null)
                            for (int j = 0; j < array.length(); j++) {
                                JSONObject object1 = array.optJSONObject(j);
                                utility.setComplete_per(object1.optString("quiz_complete_percentage", "0"));
                            }
                    }
                    mGetPendingData.add(utility);
                }
            }
            System.out.println("====mGetPendingDatamGetPendingData===" + mGetPendingData.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Pending_Response_Utility> data = dataBase.getPending_Response(WebServices.mLoginUtility.getUser_id());
        for (int i1 = 0; i1 < data.size(); i1++) {
            try {
                JSONObject jsonObject = new JSONObject(data.get(i1).getData());
                JSONArray jsonArray = jsonObject.optJSONArray("data");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.optJSONObject(i);
                        GetPendingRes_Utility utility = new GetPendingRes_Utility();
                        utility.setAdd_by(object.optString("added_by", ""));
                        utility.setMaterial_id(object.optString("material_id", ""));
                        utility.setPending_response_id(object.optString("pending_response_id", ""));
                        utility.setResponse_data(object.optString("response_data", ""));
                        utility.setResponse_status(object.optString("response_status", ""));
                        utility.setUser_id(object.optString("user_id", ""));
                        JSONArray array = object.optJSONArray("response_data");
                        if (array != null)
                            for (int j = 0; j < array.length(); j++) {
                                JSONObject object1 = array.optJSONObject(j);
                                utility.setComplete_per(object1.optString("quiz_complete_percentage", "0"));
                            }
                        mGetPendingData.add(utility);
                    }
                }
                System.out.println("====mGetPendingDatamGetPendingData===" + mGetPendingData.size());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<GroupUtils> getAllGroup(String resp) {
        groupUtilsArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    GroupUtils groupUtils = new GroupUtils();
                    JSONObject object = jsonArray.optJSONObject(i);
                    groupUtils.setCourse_id(object.optString("course_id", ""));
                    groupUtils.setGroup_id(object.optString("group_id", ""));
                    groupUtils.setGroup_name(object.optString("group_name", ""));
                    groupUtils.setGroup_type(object.optString("group_type", ""));
                    groupUtils.setImage(object.optString("image", ""));
                    groupUtils.setUser_id(object.optString("user_id", ""));
                    groupUtils.setGroup_members(object.optString("group_members", "0"));
                    groupUtilsArrayList.add(groupUtils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return groupUtilsArrayList;
    }

    public ArrayList<GroupUtils> getGroup(String resp) {
        ArrayList<GroupUtils> arrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    GroupUtils groupUtils = new GroupUtils();
                    JSONObject object = jsonArray.optJSONObject(i);
                    groupUtils.setCourse_id(object.optString("course_id", ""));
                    groupUtils.setGroup_id(object.optString("group_id", ""));
                    groupUtils.setGroup_name(object.optString("group_name", ""));
                    groupUtils.setGroup_type(object.optString("group_type", ""));
                    groupUtils.setImage(object.optString("image", ""));
                    groupUtils.setUser_id(object.optString("user_id", ""));
                    arrayList.add(groupUtils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrayList;
    }

    public ArrayList<GroupMembersUtils> getGroupMembers(String resp) {
        groupmembersUtilses = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    GroupMembersUtils utils = new GroupMembersUtils();
                    JSONObject object = jsonArray.optJSONObject(i);
                    utils.setUser_id(object.optString("user_id", ""));
                    utils.setGroup_id(object.optString("group_id", ""));
                    utils.setEmail(object.optString("email", ""));
                    utils.setFirstname(object.optString("firstname", ""));
                    utils.setGroup_member_id(object.optString("group_member_id", ""));
//                    utils.setLastname(object.optString("lastname", ""));
                    utils.setPhoto(object.optString("photo", ""));
                    groupmembersUtilses.add(utils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GroupMembersUtils utils = new GroupMembersUtils();
        utils.setUser_id(WebServices.mLoginUtility.getUser_id());
        utils.setGroup_id("");
        utils.setEmail(WebServices.mLoginUtility.getEmail());
        utils.setFirstname(WebServices.mLoginUtility.getFirstname());
//        utils.setLastname(WebServices.mLoginUtility.getLastname());
        utils.setPhoto(WebServices.mLoginUtility.getPhoto());
        groupmembersUtilses.add(utils);
        return groupmembersUtilses;
    }

    public ArrayList<GroupMembersUtils> getAllGroupMembers(String resp) {
        membersUtilses = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    GroupMembersUtils utils = new GroupMembersUtils();
                    JSONObject object = jsonArray.optJSONObject(i);
                    utils.setUser_id(object.optString("user_id", ""));
                    utils.setGroup_id(object.optString("group_id", ""));
                    utils.setEmail(object.optString("email", ""));
                    utils.setFirstname(object.optString("firstname", ""));
                    utils.setGroup_member_id(object.optString("group_member_id", ""));
//                    utils.setLastname(object.optString("lastname", ""));
                    utils.setPhoto(object.optString("photo", ""));
                    utils.setSelected(false);
                    membersUtilses.add(utils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GroupMembersUtils utils = new GroupMembersUtils();
        utils.setUser_id(WebServices.mLoginUtility.getUser_id());
        utils.setGroup_id("");
        utils.setEmail(WebServices.mLoginUtility.getEmail());
        utils.setFirstname(WebServices.mLoginUtility.getFirstname());
//        utils.setLastname(WebServices.mLoginUtility.getLastname());
        utils.setPhoto(WebServices.mLoginUtility.getPhoto());
        membersUtilses.add(utils);
        return membersUtilses;
    }

    public ArrayList<Chat_MessagesUtils> getchatMessages(String resp) {
        chat_messagesUtilses = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    Chat_MessagesUtils utils = new Chat_MessagesUtils();
                    utils.setFirstname(object.optString("firstname", ""));
                    utils.setPhoto(object.optString("photo", ""));
                    utils.setGroup_id(object.optString("group_id", ""));
                    utils.setUser_id(object.optString("user_id", ""));
                    utils.setLastname(object.optString("lastname", ""));
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat df2 = new SimpleDateFormat("hh:mm aa");
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");


                    String date = "";
                    if (!object.optString("added_on", "").equalsIgnoreCase("")) {
                        try {
                            Date d = df.parse(object.optString("added_on", ""));
                            date = df2.format(d);
                            utils.setDate(df3.format(d));
                        } catch (Exception e) {

                        }
                    }
                    utils.setAdded_on(date);
                    utils.setEmail(object.optString("email", ""));
                    utils.setForum_description(object.optString("forum_description", ""));
                    utils.setForum_title(object.optString("forum_title", ""));
                    utils.setForum_id(object.optString("forum_id", ""));
                    utils.setForum_type(object.optString("forum_type", ""));
                    JSONObject mediaObject = object.optJSONObject("media");
                    if (mediaObject != null) {
                        utils.setMedia(mediaObject.optString("file_url", ""));
                        utils.setMedia_type(mediaObject.optString("type", ""));
                    }
                    JSONArray array = object.optJSONArray("comments");
                    ArrayList<CommentsUtils> commentsUtilsArrayList = new ArrayList<>();
                    if (array != null) {
                        for (int ii = 0; ii < array.length(); ii++) {
                            JSONObject object1 = array.optJSONObject(ii);
                            CommentsUtils commentsUtils = new CommentsUtils();
                            commentsUtils.setEmail(object1.optString("email", ""));
                            SimpleDateFormat df4 = new SimpleDateFormat("hh:mm aa");
                            if (!object1.optString("added_on", "").equalsIgnoreCase("")) {
                                try {
                                    Date d = df.parse(object1.optString("added_on", ""));
                                    commentsUtils.setDate(df3.format(d));
                                    date = df4.format(d);
                                } catch (Exception e) {

                                }
                            }
                            commentsUtils.setAdded_on(date);
                            commentsUtils.setFirstname(object1.optString("firstname", ""));
                            commentsUtils.setForum_id(object1.optString("forum_id", ""));
                            commentsUtils.setLastname(object1.optString("lastname", ""));
                            commentsUtils.setComment(object1.optString("comment", ""));
                            commentsUtils.setComment_id(object1.optString("comment_id", ""));
                            commentsUtils.setPhoto(object1.optString("photo", ""));
                            commentsUtils.setUser_id(object1.optString("user_id", ""));
                            JSONObject mediaObject1 = object1.optJSONObject("media");
                            if (mediaObject1 != null) {
                                commentsUtils.setMedia(mediaObject1.optString("file_url", ""));
                                commentsUtils.setMedia_type(mediaObject1.optString("type", ""));
                            }
                            commentsUtilsArrayList.add(commentsUtils);

                        }
                    }
                    utils.setCommentsUtilsArrayList(commentsUtilsArrayList);

                    chat_messagesUtilses.add(utils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return chat_messagesUtilses;
    }

    public ArrayList<Chat_MessagesUtils> getGroupchatMessages(String resp) {
        group_chat_messagesUtilses = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    Chat_MessagesUtils utils = new Chat_MessagesUtils();
                    utils.setFirstname(object.optString("firstname", ""));
                    utils.setPhoto(object.optString("photo", ""));
                    utils.setGroup_id(object.optString("group_id", ""));
                    utils.setUser_id(object.optString("user_id", ""));
                    utils.setLastname(object.optString("lastname", ""));
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat df2 = new SimpleDateFormat("hh:mm aa");
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");


                    String date = "";
                    if (!object.optString("added_on", "").equalsIgnoreCase("")) {
                        try {
                            Date d = df.parse(object.optString("added_on", ""));
                            date = df2.format(d);
                            utils.setDate(df3.format(d));
                        } catch (Exception e) {

                        }
                    }
                    utils.setAdded_on(date);
                    utils.setEmail(object.optString("email", ""));
                    utils.setForum_description(object.optString("forum_description", ""));
                    utils.setForum_title(object.optString("forum_title", ""));
                    utils.setForum_id(object.optString("forum_id", ""));
                    utils.setForum_type(object.optString("forum_type", ""));
//                    utils.setMedia(object.optString("media",""));
                    JSONObject mediaObject = object.optJSONObject("media");
                    if (mediaObject != null) {
                        utils.setMedia(mediaObject.optString("file_url", ""));
                        utils.setMedia_type(mediaObject.optString("type", ""));
                    }
                    JSONArray array = object.optJSONArray("comments");
                    ArrayList<CommentsUtils> commentsUtilsArrayList = new ArrayList<>();
                    if (array != null) {
                        for (int ii = 0; ii < array.length(); ii++) {
                            JSONObject object1 = array.optJSONObject(ii);
                            CommentsUtils commentsUtils = new CommentsUtils();
                            commentsUtils.setEmail(object1.optString("email", ""));
                            SimpleDateFormat df4 = new SimpleDateFormat("hh:mm aa");
                            if (!object1.optString("added_on", "").equalsIgnoreCase("")) {
                                try {
                                    Date d = df.parse(object1.optString("added_on", ""));
                                    commentsUtils.setDate(df3.format(d));
                                    date = df4.format(d);
                                } catch (Exception e) {

                                }
                            }
                            commentsUtils.setAdded_on(date);
                            commentsUtils.setFirstname(object1.optString("firstname", ""));
                            commentsUtils.setForum_id(object1.optString("forum_id", ""));
                            commentsUtils.setLastname(object1.optString("lastname", ""));
                            commentsUtils.setComment(object1.optString("comment", ""));
                            commentsUtils.setComment_id(object1.optString("comment_id", ""));
                            commentsUtils.setPhoto(object1.optString("photo", ""));
                            commentsUtils.setUser_id(object1.optString("user_id", ""));
                            JSONObject mediaObject1 = object1.optJSONObject("media");
                            if (mediaObject1 != null) {
                                commentsUtils.setMedia(mediaObject1.optString("file_url", ""));
                                commentsUtils.setMedia_type(mediaObject1.optString("type", ""));
                            }
                            commentsUtilsArrayList.add(commentsUtils);

                        }
                    }
                    utils.setCommentsUtilsArrayList(commentsUtilsArrayList);

                    group_chat_messagesUtilses.add(utils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return group_chat_messagesUtilses;
    }

    public void getchatMessage(String resp) {
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    Chat_MessagesUtils utils = new Chat_MessagesUtils();
                    utils.setFirstname(object.optString("firstname", ""));
                    utils.setPhoto(object.optString("photo", ""));
                    utils.setGroup_id(object.optString("group_id", ""));
                    utils.setUser_id(object.optString("user_id", ""));
                    utils.setLastname(object.optString("lastname", ""));
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat df2 = new SimpleDateFormat("hh:mm aa");
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");
                    String date = "";
                    if (!object.optString("added_on", "").equalsIgnoreCase("")) {
                        try {
                            Date d = df.parse(object.optString("added_on", ""));
                            date = df2.format(d);
                            utils.setDate(df3.format(d));
                        } catch (Exception e) {

                        }
                    }
                    utils.setAdded_on(date);
                    utils.setEmail(object.optString("email", ""));
                    utils.setForum_description(object.optString("forum_description", ""));
                    utils.setForum_title(object.optString("forum_title", ""));
                    utils.setForum_id(object.optString("forum_id", ""));
                    utils.setForum_type(object.optString("forum_type", ""));
                    JSONObject mediaObject = object.optJSONObject("media");
                    if (mediaObject != null) {
                        utils.setMedia(mediaObject.optString("file_url", ""));
                        utils.setMedia_type(mediaObject.optString("type", ""));
                    }
                    chat_messagesUtilses.add(utils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void getchatComment(String resp) {
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray array = jsonObject.optJSONArray("data");
            if (array != null) {
                for (int ii = 0; ii < array.length(); ii++) {
                    JSONObject object1 = array.optJSONObject(ii);
                    CommentsUtils commentsUtils = new CommentsUtils();
                    commentsUtils.setEmail(object1.optString("email", ""));
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date = "";
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");
                    SimpleDateFormat df4 = new SimpleDateFormat("hh:mm aa");
                    if (!object1.optString("added_on", "").equalsIgnoreCase("")) {
                        try {
                            Date d = df.parse(object1.optString("added_on", ""));
                            commentsUtils.setDate(df3.format(d));
                            date = df4.format(d);
                        } catch (Exception e) {

                        }
                    }
                    commentsUtils.setAdded_on(date);
                    commentsUtils.setFirstname(object1.optString("firstname", ""));
                    commentsUtils.setForum_id(object1.optString("forum_id", ""));
                    commentsUtils.setLastname(object1.optString("lastname", ""));
                    commentsUtils.setComment(object1.optString("comment", ""));
                    commentsUtils.setComment_id(object1.optString("comment_id", ""));
                    commentsUtils.setPhoto(object1.optString("photo", ""));
                    commentsUtils.setUser_id(object1.optString("user_id", ""));
                    JSONObject mediaObject = object1.optJSONObject("media");
                    if (mediaObject != null) {
                        commentsUtils.setMedia(mediaObject.optString("file_url", ""));
                        commentsUtils.setMedia_type(mediaObject.optString("type", ""));
                    }
                    TopicChatFragment.commentsUtilsArrayList.add(commentsUtils);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public ArrayList<CommentsUtils> getPublicchatComment(String resp) {
        ArrayList<CommentsUtils> list = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray array = jsonObject.optJSONArray("data");
            if (array != null) {
                for (int ii = 0; ii < array.length(); ii++) {
                    JSONObject object1 = array.optJSONObject(ii);
                    CommentsUtils commentsUtils = new CommentsUtils();
                    commentsUtils.setEmail(object1.optString("email", ""));
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date = "";
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");
                    SimpleDateFormat df4 = new SimpleDateFormat("hh:mm aa");
                    if (!object1.optString("added_on", "").equalsIgnoreCase("")) {
                        try {
                            Date d = df.parse(object1.optString("added_on", ""));
                            commentsUtils.setDate(df3.format(d));
                            date = df4.format(d);
                        } catch (Exception e) {

                        }
                    }
                    commentsUtils.setAdded_on(date);
                    commentsUtils.setFirstname(object1.optString("firstname", ""));
                    commentsUtils.setForum_id(object1.optString("forum_id", ""));
                    commentsUtils.setLastname(object1.optString("lastname", ""));
                    commentsUtils.setComment(object1.optString("comment", ""));
                    commentsUtils.setComment_id(object1.optString("comment_id", ""));
                    commentsUtils.setPhoto(object1.optString("photo", ""));
                    commentsUtils.setUser_id(object1.optString("user_id", ""));
                    JSONObject mediaObject = object1.optJSONObject("media");
                    if (mediaObject != null) {
                        commentsUtils.setMedia(mediaObject.optString("file_url", ""));
                        commentsUtils.setMedia_type(mediaObject.optString("type", ""));
                    }
                    list.add(commentsUtils);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;

    }

    public ArrayList<Chat_MessagesUtils> getPublicchatMessages(String resp) {
        ArrayList<Chat_MessagesUtils> chat_messagesUtilses = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.optJSONObject(i);
                    Chat_MessagesUtils utils = new Chat_MessagesUtils();
                    utils.setFirstname(object.optString("firstname", ""));
                    utils.setPhoto(object.optString("photo", ""));
                    utils.setGroup_id(object.optString("group_id", ""));
                    utils.setUser_id(object.optString("user_id", ""));
                    utils.setLastname(object.optString("lastname", ""));
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat df2 = new SimpleDateFormat("hh:mm aa");
                    SimpleDateFormat df3 = new SimpleDateFormat("dd MMM, yyyy");


                    String date = "";
                    if (!object.optString("added_on", "").equalsIgnoreCase("")) {
                        try {
                            Date d = df.parse(object.optString("added_on", ""));
                            date = df2.format(d);
                            utils.setDate(df3.format(d));
                        } catch (Exception e) {

                        }
                    }
                    utils.setAdded_on(date);
                    utils.setEmail(object.optString("email", ""));
                    utils.setForum_description(object.optString("forum_description", ""));
                    utils.setForum_title(object.optString("forum_title", ""));
                    utils.setForum_id(object.optString("forum_id", ""));
                    utils.setForum_type(object.optString("forum_type", ""));
                    JSONObject mediaObject = object.optJSONObject("media");
                    if (mediaObject != null) {
                        utils.setMedia(mediaObject.optString("file_url", ""));
                        utils.setMedia_type(mediaObject.optString("type", ""));
                    }
                    JSONArray array = object.optJSONArray("comments");
                    ArrayList<CommentsUtils> commentsUtilsArrayList = new ArrayList<>();
                    if (array != null) {
                        for (int ii = 0; ii < array.length(); ii++) {
                            JSONObject object1 = array.optJSONObject(ii);
                            CommentsUtils commentsUtils = new CommentsUtils();
                            commentsUtils.setEmail(object1.optString("email", ""));
                            SimpleDateFormat df4 = new SimpleDateFormat("hh:mm aa");
                            if (!object1.optString("added_on", "").equalsIgnoreCase("")) {
                                try {
                                    Date d = df.parse(object1.optString("added_on", ""));
                                    commentsUtils.setDate(df3.format(d));
                                    date = df4.format(d);
                                } catch (Exception e) {

                                }
                            }
                            commentsUtils.setAdded_on(date);
                            commentsUtils.setFirstname(object1.optString("firstname", ""));
                            commentsUtils.setForum_id(object1.optString("forum_id", ""));
                            commentsUtils.setLastname(object1.optString("lastname", ""));
                            commentsUtils.setComment(object1.optString("comment", ""));
                            commentsUtils.setComment_id(object1.optString("comment_id", ""));
                            commentsUtils.setPhoto(object1.optString("photo", ""));
                            commentsUtils.setUser_id(object1.optString("user_id", ""));
                            JSONObject mediaObject1 = object1.optJSONObject("media");
                            if (mediaObject1 != null) {
                                commentsUtils.setMedia(mediaObject1.optString("file_url", ""));
                                commentsUtils.setMedia_type(mediaObject1.optString("type", ""));
                            }
                            commentsUtilsArrayList.add(commentsUtils);

                        }
                    }
                    utils.setCommentsUtilsArrayList(commentsUtilsArrayList);

                    chat_messagesUtilses.add(utils);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return chat_messagesUtilses;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Downloaded/");

        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    public Bitmap getImage(String imgUrl) {
        URL url = null;
        Bitmap image = null;

        String urlToImage = imgUrl;
        try {
            url = new URL(urlToImage);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    public ArrayList<NewsUtility> getData(String resp, String type) {
        ArrayList<NewsUtility> utilities = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray array = jsonObject.optJSONArray("data");
            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    NewsUtility utility = new NewsUtility();
                    JSONObject object = array.optJSONObject(i);
                    if (type.equalsIgnoreCase("view"))
                        utility.setId(object.optString("view_id"));
                    else
                        utility.setId(object.optString("like_id"));
                    utility.setUser_id(object.optString("user_id"));
                    utilities.add(utility);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return utilities;
    }

    public ArrayList<MeterialUtility> getNewsFeedData(String resp) {
        ArrayList<MeterialUtility> newsFeedUtilities = new ArrayList<>();

        JSONArray arrayDes;
        try {
            JSONObject jsonObject = new JSONObject(resp);
            JSONArray array = jsonObject.optJSONArray("data");
            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    MeterialUtility meterialUtility = new MeterialUtility();
                    meterialUtility.setMaterial_id(object.optString("notice_board_id", "0"));
                    meterialUtility.setTitle(object.optString("title", ""));
                    meterialUtility.setMaterial_image(object.optString("image", ""));
                    String newsType = object.optString("news_type");
                    if (newsType.equalsIgnoreCase("news"))
                        meterialUtility.setNews(true);
                    meterialUtility.setMaterial_type(object.optString("type", ""));
                    meterialUtility.setShow_vertical(object.optString("show_vertical"));
                    meterialUtility.setVertical_level(object.optString("vertical_level"));
                    if (!meterialUtility.isNews()) {
                        meterialUtility.setStart_date(object.optString("start_date", ""));
                        meterialUtility.setEnd_date(object.optString("end_date", ""));
                    } else {
                        meterialUtility.setDescription(object.optString("description", ""));
                        /*if(!TextUtils.isEmpty(meterialUtility.getMaterial_type())&&meterialUtility.getMaterial_type().equalsIgnoreCase("WISHES")){
                            arrayDes= object.optJSONArray("description");
                            if(arrayDes!=null){
                                int desLenght=arrayDes.length();
                                ArrayList<NewsDescriptionModel> newsDescriptionModelArrayList=new ArrayList<>();
                                for(int j=0;j<desLenght;j++){
                                    NewsDescriptionModel newsDescriptionModel= new NewsDescriptionModel();
                                    JSONObject objectDes = array.getJSONObject(j);
                                    if(objectDes!=null){
                                        newsDescriptionModel.setUser_id(objectDes.optString("user_id"));
                                        newsDescriptionModel.setUser_name(objectDes.optString("user_name"));
                                    }
                                    newsDescriptionModelArrayList.add(newsDescriptionModel);
                                }
                                meterialUtility.setNewsDescriptionModel(newsDescriptionModelArrayList);
                            }
                        }else{

                        }*/

                        meterialUtility.setThumbnailUrl(object.optString("image"));
                        JSONObject mediaObject = object.optJSONObject("media");
                        if (mediaObject != null) {
                            meterialUtility.setMaterial_media_file_url(mediaObject.optString("file_url"));
                            meterialUtility.setMaterial_type(mediaObject.optString("type"));
                            meterialUtility.setFile_size(mediaObject.optString("file_size"));
                        } else {
                            meterialUtility.setMaterial_media_file_url(object.optString("url"));
                            meterialUtility.setMaterial_type(object.optString("type"));
                            meterialUtility.setFile_size(object.optString("file_size"));
                        }
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yy hh:mm aa");
                        String date = "";
                        if (!object.optString("added_on", "").equalsIgnoreCase("")) {
                            try {
                                Date d = df.parse(object.optString("added_on", ""));
                                date = df2.format(d);
                            } catch (Exception e) {

                            }
                        }
                        meterialUtility.setAdded_on(date);
                    }

                    JSONArray like = object.optJSONArray("like");
                    if (like == null) {
                        meterialUtility.setTotal_like("0");
                    } else {
                        ArrayList<NewsUtility> utilities = new ArrayList<>();
                        for (int i1 = 0; i1 < like.length(); i1++) {
                            NewsUtility utility1 = new NewsUtility();
                            JSONObject objectLike = like.getJSONObject(i1);
                            utility1.setId(objectLike.optString("like_id"));
                            utility1.setUser_id(objectLike.optString("user_id"));
                            utilities.add(utility1);
                        }
                        meterialUtility.setTotal_fav(utilities.size() + "");
                        meterialUtility.setAlLike(utilities);
                    }
                    JSONArray view = object.optJSONArray("view");
                    if (view == null) {
                        meterialUtility.setTotal_view("0");
                    } else {
                        ArrayList<NewsUtility> utilities = new ArrayList<>();
                        for (int i1 = 0; i1 < view.length(); i1++) {
                            NewsUtility utility1 = new NewsUtility();
                            JSONObject objectLike = view.getJSONObject(i1);
                            utility1.setId(objectLike.optString("view_id"));
                            utility1.setUser_id(objectLike.optString("user_id"));
                            utilities.add(utility1);
                        }
                        meterialUtility.setTotal_view(utilities.size() + "");
                        meterialUtility.setAlView(utilities);
                    }






                /*NewsFeedUtility utility = new NewsFeedUtility();
                    utility.setNotice_board_id(object.optString("notice_board_id", "0"));
                    utility.setTitle(object.optString("title", ""));
                    utility.setDesc(object.optString("description", ""));
                    utility.setFile_type(object.optString("type", ""));
                    if (utility.getFile_type().equalsIgnoreCase("Quiz")) {

                        utility.setStart_date(object.optString("start_date", ""));
                        utility.setEnd_date(object.optString("start_date", ""));
                    } else {
                        utility.setImage_url(object.optString("image", ""));
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yy hh:mm aa");
                        String date = "";
                        if (!object.optString("added_on", "").equalsIgnoreCase("")) {
                            try {
                                Date d = df.parse(object.optString("added_on", ""));
                                date = df2.format(d);
                            } catch (Exception e) {

                            }
                        }
                        utility.setTime(date);
                        JSONObject object1 = object.optJSONObject("media");
                        if (object1 != null) {
                            utility.setFile_type(object1.optString("type", ""));
                            if (object1.getString("type").equalsIgnoreCase("IMAGE")) {
                                utility.setIsImage(true);
                                utility.setFile_url(object1.getString("file_url"));
                            } else if (object1.getString("type").equalsIgnoreCase("VIDEO")) {
                                utility.setIsVideo(true);
                                utility.setFile_url(object1.getString("file_url"));
                            } else {
                                utility.setFile_url(object1.getString("file_url"));
                            }
                        }
                        JSONArray like = object.optJSONArray("like");
                        if (like == null) {
                            utility.setTotal_fev("0");
                        } else {
                            ArrayList<NewsUtility> utilities = new ArrayList<>();
                            for (int i1 = 0; i1 < like.length(); i1++) {
                                NewsUtility utility1 = new NewsUtility();
                                JSONObject objectLike = like.getJSONObject(i1);
                                utility1.setId(objectLike.optString("like_id"));
                                utility1.setUser_id(objectLike.optString("user_id"));
                                utilities.add(utility1);
                            }
                            utility.setTotal_fev(utilities.size() + "");
                            utility.setLikeArrayList(utilities);
                        }
                        JSONArray view = object.optJSONArray("view");
                        if (view == null) {
                            utility.setTotal_view("0");
                        } else {
                            ArrayList<NewsUtility> utilities = new ArrayList<>();
                            for (int i1 = 0; i1 < view.length(); i1++) {
                                NewsUtility utility1 = new NewsUtility();
                                JSONObject objectLike = view.getJSONObject(i1);
                                utility1.setId(objectLike.optString("view_id"));
                                utility1.setUser_id(objectLike.optString("user_id"));
                                utilities.add(utility1);
                            }
                            utility.setTotal_view(utilities.size() + "");
                            utility.setViewArrayList(utilities);
                        }
                    }*/

                    newsFeedUtilities.add(meterialUtility);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newsFeedUtilities;
    }

    public static String updateRankAndCoinsInLogin(String loginData, String resp) {
        String respData = loginData;
        try {
            JSONObject jsonObject = new JSONObject(loginData);
            JSONObject resObject1 = new JSONObject(resp);
            JSONObject dataResObject = resObject1.optJSONObject("data");
            JSONObject obj = jsonObject.optJSONObject("status");
            if (obj == null || obj.optString("success").equalsIgnoreCase("false")) {
//                ERRORMSG = obj.optString("errormsg");
//                DEVICE_ERRORMSG = obj.optString("errorkey", "");
            } else {
                jsonObject = jsonObject.optJSONObject("data");
//                String coinsAllocated = jsonObject.optString("coins_allocated");

                String coinsCollected = dataResObject.optString("coins_allocated");
                if (!TextUtils.isEmpty(coinsCollected)) {
                    jsonObject.remove("coins_allocated");
                    jsonObject.put("coins_allocated", coinsCollected);
                    mLoginUtility.setCoins_collected(coinsCollected);
                    String rank = dataResObject.optString("rank");
                    if (TextUtils.isEmpty(rank)) {
                        if (jsonObject.has("rank")) {
                            jsonObject.remove("rank");
                            jsonObject.put("rank", rank);
                        } else {
                            jsonObject.put("rank", rank);
                        }
                        mLoginUtility.setRank("rank");
                        respData = jsonObject.toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respData;

    }

    public static String getCompletedDate(MeterialUtility meterialUtility) {
        String completedDate = "";
        if (meterialUtility != null && HomeActivity.mReadResponse != null) {
            for (int i = HomeActivity.mReadResponse.size() - 1; i >= 0; i--) {
                if (meterialUtility.getMaterial_id().equals(HomeActivity.mReadResponse.get(i).getMaterial_id())) {
                    completedDate = HomeActivity.mReadResponse.get(i).getFinish_time();
                    if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
                        if (TextUtils.isEmpty(completedDate) || completedDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                            completedDate = HomeActivity.mReadResponse.get(i).getStart_time();
                            if (!TextUtils.isEmpty(completedDate) && completedDate.equalsIgnoreCase("0000-00-00 00:00:00"))
                                completedDate = "";
                        }
                    }
                    return completedDate;
                }
            }
        }
        return completedDate;
    }

    public void updateRating(String matId, String avgRate, String myRating) {
        MeterialUtility meterialUtility = FlowingCourseUtils.getMaterialByMaterialId(matId);
        if (meterialUtility != null) {
            meterialUtility.setAverage_rating(avgRate);
            meterialUtility.setRateNum(myRating);
        }
    }

    public ArrayList<VideoQuizAllResponseModel> parseVideoQuizAllResponse(String resp) {
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    JSONArray jsonArray1 = jsonObject.optJSONArray("data");
                    if (jsonArray1 != null) {
                        ArrayList<VideoQuizAllResponseModel> videoQuizAllResponseArrayList = new ArrayList<>();
                        for (int j = 0; j < jsonArray1.length(); j++) {
                            JSONObject jsonObject2 = (JSONObject) jsonArray1.get(j);
                            if (jsonObject2 != null) {
                                VideoQuizAllResponseModel videoQuizAllResponseModel = new VideoQuizAllResponseModel();
                                videoQuizAllResponseModel.setFinishDate(jsonObject2.optString("finish_time"));
                                videoQuizAllResponseModel.setResponseId(jsonObject2.optString("response_id"));
                                JSONArray jsonArray = jsonObject2.optJSONArray("question_response");
                                if (jsonArray != null) {
                                    ArrayList<VideoQuizResponse> videoQuizResponseArrayList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        VideoQuizResponse videoQuizResponse = new VideoQuizResponse();
                                        VideoCommentModel videoCommentModel = new VideoCommentModel();
                                        JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                        if (jsonObject1 != null) {
                                            videoQuizResponse.setResponse_id(jsonObject1.optString("response_id"));
                                            videoQuizResponse.setQuestion_response_id(jsonObject1.optString("question_response_id"));
                                            videoQuizResponse.setQuestion_bank_id(jsonObject1.optString("question_bank_id"));
                                            videoQuizResponse.setAnswer_key(jsonObject1.optString("answer_key"));
                                            videoQuizResponse.setTime_taken(jsonObject1.optString("time_taken"));
                                            videoQuizResponse.setAnswer_type(jsonObject1.optString("answer_type"));
                                            videoQuizResponse.setQuestion_comment(jsonObject1.optString("question_comment"));
                                            videoQuizResponse.setQuestion_description(jsonObject1.optString("question_description"));
                                            String answerString = jsonObject1.optString("answer");
                                            if (!TextUtils.isEmpty(answerString)) {
                                                JSONObject answerJson = new JSONObject(jsonObject1.optString("answer"));
                                                if (answerJson != null) {
                                                    if (answerJson.optJSONObject("status").optString("success").equalsIgnoreCase("true")) {
                                                        JSONObject jsonObject3 = answerJson.optJSONObject("data");
                                                        videoCommentModel.setMedia_id(jsonObject3.optString("media_id"));
                                                        videoCommentModel.setFile_name(jsonObject3.optString("file_name"));
                                                        videoCommentModel.setFile_url(jsonObject3.optString("file_url"));
                                                        videoCommentModel.setAdded_on(jsonObject3.optString("added_on"));
                                                    }
                                                }
                                            }
                                        }

                                        videoQuizResponse.setAnswer(videoCommentModel);
                                        JSONArray trainerArray = jsonObject1.optJSONArray("trainer_review");
                                        if (trainerArray != null && trainerArray.length() > 0) {
                                            videoQuizAllResponseModel.setEvaluated(true);
                                            ArrayList<TrainerModel> trainerModelArrayList = new ArrayList<>();
                                            for (int k = 0; k < trainerArray.length(); k++) {
                                                JSONObject trainerObject = trainerArray.optJSONObject(k);
                                                if (trainerObject != null) {
                                                    trainerModelArrayList.add(new TrainerModel(trainerObject.optString("trainer_id"),
                                                            trainerObject.optString("trainer_name"), trainerObject.optString("trainer_emp_id"),
                                                            trainerObject.optString("rating"), trainerObject.optString("comment")));
                                                }
                                            }
                                            videoQuizResponse.setTrainerModelArrayList(trainerModelArrayList);
                                        } else
                                            videoQuizAllResponseModel.setEvaluated(false);
                                        videoQuizResponseArrayList.add(videoQuizResponse);
                                    }
                                    videoQuizAllResponseModel.setAlVideoQuizResponse(videoQuizResponseArrayList);
                                }
                                videoQuizAllResponseArrayList.add(videoQuizAllResponseModel);
                            }

                        }
                        return videoQuizAllResponseArrayList;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public ArrayList<VideoQuizResponse> parseVideoQuizResponse(String resp) {
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    JSONArray jsonArray = jsonObject.optJSONArray("data");
                    if (jsonArray != null) {
                        ArrayList<VideoQuizResponse> videoQuizResponseArrayList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            VideoQuizResponse videoQuizResponse = new VideoQuizResponse();
                            VideoCommentModel videoCommentModel = new VideoCommentModel();
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                            if (jsonObject1 != null) {
                                videoQuizResponse.setResponse_id(jsonObject1.optString("response_id"));
                                videoQuizResponse.setQuestion_response_id(jsonObject1.optString("question_response_id"));
                                videoQuizResponse.setQuestion_bank_id(jsonObject1.optString("question_bank_id"));
                                videoQuizResponse.setAnswer_key(jsonObject1.optString("answer_key"));
                                videoQuizResponse.setTime_taken(jsonObject1.optString("time_taken"));
                                videoQuizResponse.setAnswer_type(jsonObject1.optString("answer_type"));
                                videoQuizResponse.setQuestion_comment(jsonObject1.optString("question_comment"));
                                videoQuizResponse.setQuestion_description(jsonObject1.optString("question_description"));

                                JSONObject answerJson = new JSONObject(jsonObject1.optString("answer"));
                                if (answerJson != null) {
                                    if (answerJson.optJSONObject("status").optString("success").equalsIgnoreCase("true")) {
                                        JSONObject jsonObject3 = answerJson.optJSONObject("data");
                                        videoCommentModel.setMedia_id(jsonObject3.optString("media_id"));
                                        videoCommentModel.setFile_name(jsonObject3.optString("file_name"));
                                        videoCommentModel.setFile_url(jsonObject3.optString("file_url"));
                                        videoCommentModel.setAdded_on(jsonObject3.optString("added_on"));
                                    }
                                }
                            }

                            videoQuizResponse.setAnswer(videoCommentModel);
                            JSONArray trainerArray = jsonObject1.optJSONArray("trainer_review");
                            if (trainerArray != null && trainerArray.length() > 0) {
                                ArrayList<TrainerModel> trainerModelArrayList = new ArrayList<>();
                                for (int j = 0; j < trainerArray.length(); j++) {
                                    JSONObject trainerObject = trainerArray.optJSONObject(j);
                                    if (trainerObject != null) {
                                        trainerModelArrayList.add(new TrainerModel(trainerObject.optString("trainer_id"),
                                                trainerObject.optString("trainer_name"), trainerObject.optString("trainer_emp_id"),
                                                trainerObject.optString("rating"), trainerObject.optString("comment")));
                                    }
                                }
                                videoQuizResponse.setTrainerModelArrayList(trainerModelArrayList);
                            }
                            videoQuizResponseArrayList.add(videoQuizResponse);
                        }
                        return videoQuizResponseArrayList;

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    //    {"status":{"success":true,"errormsg":""},"data":{"trainings":[false]}}
    public ArrayList<EventModel> getTrainingEventList(String resp) {
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                if (jsonObject != null) {
                    ArrayList<EventModel> eventModelArrayList = new ArrayList<>();
                    JSONObject dataObject = jsonObject.optJSONObject("data");
                    if (dataObject != null) {
                        JSONArray trainingArray = dataObject.optJSONArray("trainings");
                        if (trainingArray != null && trainingArray.length() > 0) {
                            for (int i = 0; i < trainingArray.length(); i++) {
                                JSONObject jsonObject1 = (JSONObject) trainingArray.get(i);
                                if (jsonObject1 != null) {
                                    EventModel eventModel = new EventModel();
                                    eventModel.setId(jsonObject1.optString("training_id"));
                                    eventModel.setName(jsonObject1.optString("training_name"));
                                    eventModel.setDescription(jsonObject1.optString("training_desc"));
                                    eventModel.setTraining(jsonObject1.optString("training"));
                                    eventModel.setStartDate(jsonObject1.optString("start_date"));
                                    eventModel.setEndDate(jsonObject1.optString("end_date"));
                                    eventModel.setStartTime(jsonObject1.optString("start_time"));
                                    eventModel.setEndTime(jsonObject1.optString("end_time"));
                                    eventModel.setZone(jsonObject1.optString("zone"));
                                    eventModel.setLoaction(jsonObject1.optString("location"));
                                    eventModel.setVenue(jsonObject1.optString("venue"));
                                    eventModel.setCity(jsonObject1.optString("city"));
                                    eventModel.setRegion(jsonObject1.optString("region"));
                                    eventModel.setType(jsonObject1.optString("training_type"));
                                    eventModel.setStatus(jsonObject1.optString("training_status"));
                                    eventModel.setModuleId(jsonObject1.optString("module_id"));
                                    eventModel.setTrainerName(jsonObject1.optString("triner_name"));
                                    eventModelArrayList.add(eventModel);
                                }
                            }
                            return eventModelArrayList;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    public String parseQuiz(String resp) {
        try {
            JSONObject jsonObject1 = new JSONObject(resp);
            JSONObject jsonObject = jsonObject1.optJSONObject("data");
            JSONArray array = jsonObject.optJSONArray("question_array");

            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject5 = array.optJSONObject(i);
                    if (!TextUtils.isEmpty(jsonObject5.optString("is_question_image")) && jsonObject5.optString("is_question_image").equals("1")) {
                        jsonObject5.remove("question_description");
                        jsonObject5.put("question_description", "A");
                    }
                    if (!TextUtils.isEmpty(jsonObject5.optString("is_option1_image")) && jsonObject5.optString("is_option1_image").equals("1")) {
                        jsonObject5.remove("option1");
                        jsonObject5.put("option1", "A");
                    }
                    if (!TextUtils.isEmpty(jsonObject5.optString("is_option2_image")) && jsonObject5.optString("is_option2_image").equals("1")) {
                        jsonObject5.remove("option2");
                        jsonObject5.put("option2", "A");
                    }
                    if (!TextUtils.isEmpty(jsonObject5.optString("is_option3_image")) && jsonObject5.optString("is_option3_image").equals("1")) {
                        jsonObject5.remove("option3");
                        jsonObject5.put("option3", "A");
                    }
                    if (!TextUtils.isEmpty(jsonObject5.optString("is_option4_image")) && jsonObject5.optString("is_option4_image").equals("1")) {
                        jsonObject5.remove("option4");
                        jsonObject5.put("option4", "A");
                    }
                }
            }
            JSONArray verticalArray = jsonObject.optJSONArray("vertical_question_array");
            if (verticalArray != null) {
                jsonObject.remove("vertical_question_array");
            }
            if (jsonObject != null)
                return jsonObject1.toString();


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public JSONArray isResultData(String resp) {
        if (!TextUtils.isEmpty(resp)) {
            try {
                JSONObject jsonObject = new JSONObject(resp);
                JSONObject obj = jsonObject.optJSONObject("status");
                JSONArray objArray = jsonObject.optJSONArray("data");
                if (obj.optString("success").equalsIgnoreCase("false")) {

                    ERRORMSG = obj.optString("errormsg");
                    DEVICE_ERRORMSG = obj.optString("errorkey", "");
                } else {
                    return objArray;
                }

            } catch (Exception e) {
                e.printStackTrace();
                ERRORMSG = "";
            }
        }
        return null;
    }
}