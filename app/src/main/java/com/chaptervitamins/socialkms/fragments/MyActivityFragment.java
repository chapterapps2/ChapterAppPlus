package com.chaptervitamins.socialkms.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chaptervitamins.R;

import java.util.LinkedHashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyGroupsFragment.MyGroupFragementInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyGroupsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyActivityFragment extends BaseFragment {


    private MyActivityFragmentInteractionListener mListener;

    public MyActivityFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MyActivityFragment.
     */
    public static MyActivityFragment newInstance(){
        return new MyActivityFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.myaction_fragment_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
      //  setData();

    }

    private void setData() {
        LinkedHashMap<String,BaseFragment> fragmentHashMap = new LinkedHashMap<>();
        fragmentHashMap.put("My Groups",FeedsFragment.newInstance());
        fragmentHashMap.put("My Content",FeedsFragment.newInstance());
        fragmentHashMap.put("My Favourites",FeedsFragment.newInstance());
        fragmentHashMap.put("Tutorial",MyGroupsFragment.newInstance());
        setTabLayout(fragmentHashMap,false);
    }

    private void findViews(View view) {
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MyActivityFragmentInteractionListener) {
            mListener = (MyActivityFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    protected String getTitle() {
        return null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface MyActivityFragmentInteractionListener{

    }
}
