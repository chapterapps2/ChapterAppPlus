package com.chaptervitamins.socialkms.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.socialkms.activity.SocialActivity;
import com.chaptervitamins.socialkms.adapters.ViewPagerAdapter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * Created by Vijay Antil on 14-11-2017.
 */

public abstract class BaseFragment extends Fragment {
    private RelativeLayout rlProgress;
    private View mainLayout;
    private ViewPagerAdapter mAdapter;
    protected ViewPager mViewPager;
    protected TabLayout mTabLayout;

    protected abstract String getTitle();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainLayout = getActivity().findViewById(R.id.main_layout);
        rlProgress = (RelativeLayout) getActivity().findViewById(R.id.rl_progress);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (getTitle() != null) {
            setTitle(getTitle());
        }
//        menu.clear();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    protected String getText(final TextInputLayout editText) {
        if (editText != null) {
            return editText.getEditText().getText().toString().trim();
        } else {
            return "";
        }
    }

    protected boolean isEmpty(String string) {
        return TextUtils.isEmpty(string);
    }


    private void setTitle(String title) {
        if (title != null && (getActivity() instanceof SocialActivity)) {
            (getActivity()).setTitle(title);
        }
    }

    protected boolean isNetworkAvailable() {
        if (WebServices.isNetworkAvailable(getActivity())) {
            return true;
        } else {
           Toast.makeText(getActivity(), getString(R.string.no_internet_connection),Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    /**
     * it is used to set visible status of main layout and progress layout
     *
     * @param mainViewStatus
     * @param progressStatus
     */
    public void setVisibilityOfProgress(int mainViewStatus, int progressStatus) {
        if (mainLayout != null && rlProgress != null) {
            mainLayout.setVisibility(mainViewStatus);
            rlProgress.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
            setVisibilityOfProgress(progressStatus);
        }
    }

    /**
     * it is used to set visibility status of progress layout..
     *
     * @param visibleStatus
     * @return
     */
    protected void setVisibilityOfProgress(int visibleStatus) {
        if (rlProgress != null)
            rlProgress.setVisibility(visibleStatus);
    }

    /**
     * To set tab layout
     */
    public void setTabLayout(LinkedHashMap<String, BaseFragment> fragmentsHashMap, boolean isFixed) {
        if (mViewPager == null || mTabLayout == null)
            return;
        if (isFixed || fragmentsHashMap.size() == 2)
            mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        else
            mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        setUpViewPager(mViewPager, fragmentsHashMap);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (mAdapter != null && mViewPager != null) {
                    mViewPager.setCurrentItem(tab.getPosition());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (mAdapter == null || mViewPager == null)
                    return;

                int tabPosition = tab.getPosition();
                Fragment fragment = ((ViewPagerAdapter) mViewPager.getAdapter()).getFragmentAtPosition(tabPosition);
                if (fragment != null) {
                    switch (tabPosition) {
                        case '1':

                            break;
                        case '2':
                            break;

                    }
                }

            }
        });

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

    }


    /**
     * To set Viewpager
     *
     * @param mViewPager
     * @param fragmentsHashMap
     */
    public void setUpViewPager(final ViewPager mViewPager, HashMap<String, BaseFragment> fragmentsHashMap) {
        try {
            mAdapter = new ViewPagerAdapter(getChildFragmentManager());
            Iterator<String> fragmentsIterator = fragmentsHashMap.keySet().iterator();
            while (fragmentsIterator.hasNext()) {
                String key = fragmentsIterator.next();
                BaseFragment fragment = fragmentsHashMap.get(key);
                mAdapter.addFragment(fragment, key);
                mTabLayout.addTab(mTabLayout.newTab().setText(key));
            }


            /*Display display = getActivity().getWindowManager().getDefaultDisplay();
            if (display.getWidth() < 1100) {
                if (fragmentsHashMap.size() == 4)
                    mTabLayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                if (fragmentsHashMap.size() == 4)
                    mTabLayout.setTabMode(TabLayout.MODE_FIXED);
            }*/


            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                   /* if (mAdapter != null && mViewPager != null) {
                        Fragment fragment = mAdapter.getFragmentAtPosition(position);
                        if (fragment != null) {
                            fragment.onResume();
                        }

                    }*/
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            mViewPager.setAdapter(mAdapter);
            mViewPager.setOffscreenPageLimit(fragmentsHashMap.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
