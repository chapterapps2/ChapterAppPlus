package com.chaptervitamins.socialkms.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.chaptervitamins.R;
import com.chaptervitamins.socialkms.fragments.FeedsFragment;
import com.chaptervitamins.socialkms.fragments.HomeFragment;
import com.chaptervitamins.socialkms.fragments.MyActivityFragment;
import com.chaptervitamins.socialkms.fragments.MyFeedsFragment;
import com.chaptervitamins.socialkms.fragments.MyGroupsFragment;


public class SocialActivity extends BaseActivity implements HomeFragment.HomeFragmentInteractionListener,MyFeedsFragment.FeedsFragmentInteractionListener,
        MyActivityFragment.MyActivityFragmentInteractionListener,MyGroupsFragment.MyGroupFragementInteractionListener,FeedsFragment.FeedsFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addFragment(HomeFragment.newInstance(),false);
    }
}
