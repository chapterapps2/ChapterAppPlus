package com.chaptervitamins.socialkms.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;

/**
 * Created by Vijay Antil on 14-11-2017.
 */

public class BaseActivity extends AppCompatActivity {
    private RelativeLayout rlProgress;
    private View mainLayout;

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        mainLayout = findViewById(R.id.fl_fragment_container);
        rlProgress = (RelativeLayout) findViewById(R.id.rl_progress);
    }

    protected void addFragment(Fragment fragment, boolean isAddToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fl_fragment_container, fragment);
        if (isAddToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    protected boolean isNetworkAvailable() {
        if (WebServices.isNetworkAvailable(this)) {
            return true;
        } else {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    /**
     * it is used to set visible status of main layout and progress layout
     *
     * @param mainViewStatus
     * @param progressStatus
     */
    public void setVisibilityOfProgress(int mainViewStatus, int progressStatus) {
        if (mainLayout != null && rlProgress != null) {
            mainLayout.setVisibility(mainViewStatus);
            rlProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            setVisibilityOfProgress(progressStatus);
        }
    }

    /**
     * it is used to set visibility status of progress layout..
     *
     * @param visibleStatus
     * @return
     */
    private void setVisibilityOfProgress(int visibleStatus) {
        if (rlProgress != null)
            rlProgress.setVisibility(visibleStatus);
    }

}
