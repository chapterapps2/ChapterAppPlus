package com.chaptervitamins.mixpanalManager;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateFormat;


import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * Created by Mohit Chauhan on October 16, 2015.
 **/
public class MixPanelManager {

    private static MixPanelManager mixPanelManager;
//    private static String mDeviceId;

    public static MixPanelManager getInstance() {
        if (mixPanelManager == null)
            mixPanelManager = new MixPanelManager();
        return mixPanelManager;
    }

    //    public void setDeviceId(Context context) {
//        mDeviceId = APIUtility.getDeviceID(context);
//    }
//
//    public String getDeviceId() {
//        return mDeviceId;
//    }
    public void sendError(Activity activity, String message) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_ERROR, message);
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_GET_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }

    public void sendUpdate(Activity activity, String message) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_UPDATE, message);
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_UPDATE_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }

    private JSONObject getJsonObject(HashMap<String, String> hashMap) {
        return new JSONObject(hashMap);
    }

    public void selectCourse(Activity activity, String userEmail, String coursename) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_COURSE_NAME, coursename);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_SELECT_COURSE_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }

    public void selectDrawerItem(Activity activity, String itemName) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, WebServices.mLoginUtility.getEmail());
            jsonObject.put(AppConstants.MIXPANAL_DRAWER_ITEM_NAME, itemName);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            jsonObject.put(AppConstants.USER_NAME, WebServices.mLoginUtility.getUser_id());
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.MIXPANAL_DRAWER_ITEM, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }

    public void sendEvent(Activity activity, String eventName) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, WebServices.mLoginUtility.getEmail());
            jsonObject.put(AppConstants.MIXPANAL_FIRSTNAME, WebServices.mLoginUtility.getFirstname());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(eventName, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }

    public void resetDevice(Activity activity, String userEmail, boolean isReset) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_FIRSTNAME, WebServices.mLoginUtility.getFirstname());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            if (isReset)
                jsonObject.put(AppConstants.RESET, "YES");
            else
                jsonObject.put(AppConstants.RESET, "NO");
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.MIXPANAL_RESET, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }
    public void forgetPasswordEvent(Activity activity, String userEmail) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
           /* jsonObject.put(AppConstants.MIXPANAL_FIRSTNAME, WebServices.mLoginUtility.getFirstname());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());*/

        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.MIXPANAL_FORGETPWD, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }


    public void sendConnectionMixpanel(Activity activity, String userEmail, String msg) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_EMPLOYEE_ID, WebServices.mLoginUtility.getEmployee_id());
            jsonObject.put(AppConstants.MIXPANAL_MSG, msg);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_Login_MSG, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }


    public void selectTimeTrack(Activity activity, String startTime, String endTime, String userEmail, String materialname, String materialtype) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_EMPLOYEE_ID, WebServices.mLoginUtility.getEmployee_id());
           /* jsonObject.put(Constants.MIXPANAL_COURSE_NAME, coursename);*/
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_START_TIME, startTime);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            ;
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_END_TIME, endTime);

        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_SELECT_MATERIAL_TIME_TRACK, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }


    public void warningQuizMixpanel(Activity activity, String userEmail, String materialname, String materialtype, String warningTime) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_WAARNING_TIME, warningTime);
            jsonObject.put(AppConstants.MIXPANAL_EMPOLYEEE_ID, WebServices.mLoginUtility.getEmployee_id());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.QUIZ_WARNING, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }
    public void AutoLogout(Context context,String apiUrl) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;

        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, WebServices.mLoginUtility.getEmail());
            jsonObject.put(AppConstants.USER_NAME, WebServices.mLoginUtility.getUser_id());
            jsonObject.put(AppConstants.REASON, apiUrl);
            jsonObject.put(AppConstants.SESSION_TOKEN, WebServices.mLoginUtility.getSession().getSession_token());

            jsonObject.put(AppConstants.MIXPANAL_EMPOLYEEE_ID, WebServices.mLoginUtility.getEmployee_id());
        } catch (Exception e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.MIXPANAL_AUTO_LOGOUT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable((Activity) context);
        people.set(jsonObject);
    }

    public void selectPDFPageCountTrack(Activity activity, String startTime, String endTime, String userEmail, String materialname, String materialtype, String read, String total) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_EMPLOYEE_ID, WebServices.mLoginUtility.getEmployee_id());
           /* jsonObject.put(Constants.MIXPANAL_COURSE_NAME, coursename);*/
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_START_TIME, startTime);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_END_TIME, endTime);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            switch (materialtype.toUpperCase()) {
                case "PDF":
                    jsonObject.put(AppConstants.MIXPANAL_PDF_PAGE_READ, read);
                    jsonObject.put(AppConstants.MIXPANAL_PDF_PAGE_Total, total);
                    break;
                case "VIDEO":
                    jsonObject.put(AppConstants.MIXPANAL_VIDEO_SEEN_TIME, read);
                    jsonObject.put(AppConstants.MIXPANAL_VIDEO_TOTAL_TIME, total);
                    break;
                case "AUDIO":
                    jsonObject.put(AppConstants.MIXPANAL_AUDIO_LISTEN_TIME, read);
                    jsonObject.put(AppConstants.MIXPANAL_AUDIO_TOTAL_TIME, total);
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_SELECT_MATERIAL_TIME_TRACK, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }


    public void sendNotification(Context activity, String notifi_no) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, WebServices.mLoginUtility.getEmail());
            jsonObject.put(AppConstants.MIXPANAL_NOTIFICATION_NUMBER, notifi_no);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_NOTIFICATION_EVENT, jsonObject);
        try {
            AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable((Activity) activity);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        people.set(jsonObject);
    }

    public void startquizflash(Activity activity, String userEmail, String materialname, String materialtype) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, APIUtility.DEVICE);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
            jsonObject.put(AppConstants.MIXPANAL_START_TIME, getCurrentTimeStamp());
        } catch (JSONException e) {

        }
//        MyApplication.getInstance().trackEvent("Materials","Select Material",materialname+" ("+materialtype+")");
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        if (materialtype.equalsIgnoreCase("quiz")) {
            AppConstants.mixpanelAPI.track(AppConstants.USER_START_QUIZ_EVENT, jsonObject);
        } else if (materialtype.equalsIgnoreCase("Video Quiz")) {
            AppConstants.mixpanelAPI.track(AppConstants.USER_START_VIDEOQUIZ_EVENT, jsonObject);
        } else {
            AppConstants.mixpanelAPI.track(AppConstants.USER_START_FLASHCARD_EVENT, jsonObject);
        }
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);

    }

    public void selectmaterial(Activity activity, String userEmail, String coursename, String materialname, String materialtype, boolean isFlowingCourse) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;

        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_COURSE_NAME, coursename);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        if (isFlowingCourse)
            AppConstants.mixpanelAPI.track(AppConstants.USER_FLOWING_COURSE, jsonObject);
        else
            AppConstants.mixpanelAPI.track(AppConstants.USER_SELECT_MATERIAL_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void selectModule(Activity activity, String userEmail, String coursename, String modulename) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_COURSE_NAME, coursename);
            jsonObject.put(AppConstants.MIXPANAL_MODULE_NAME, modulename);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_SELECT_MODULE_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void selectDownloadmaterial(Activity activity, String userEmail, String coursename, String materialname, String materialtype) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_COURSE_NAME, coursename);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            jsonObject.put(AppConstants.MIXPANAL_EMPOLYEEE_ID, WebServices.mLoginUtility.getEmployee_id());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_DOWNLOAD_MATERIAL_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void selectmaterialNoAttempts(Activity activity, String userEmail, String coursename, String materialname, String materialtype) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_COURSE_NAME, coursename);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            jsonObject.put(AppConstants.MIXPANAL_EMPOLYEEE_ID, WebServices.mLoginUtility.getEmployee_id());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_SELECT_MATERIAL_NO_ATTEMPT_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void eachQues_responce(Activity activity, String userEmail, String materialname, String corrAns, String userAns, String isCorr, String ques, boolean isChangeAnswer) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_EMPOLYEEE_ID, WebServices.mLoginUtility.getEmployee_id());
            jsonObject.put(AppConstants.MIXPANAL_CORRECTANSWER, corrAns);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_USERANSWER, userAns);
            jsonObject.put(AppConstants.MIXPANAL_ISCORRECT, isCorr);
            jsonObject.put(AppConstants.MIXPANAL_QUESTION, ques);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        if (isChangeAnswer)
            AppConstants.mixpanelAPI.track(AppConstants.USER_QUESTION_REVIEWED, jsonObject);
        else
            AppConstants.mixpanelAPI.track(AppConstants.USER_QUIZ_RESPONSE_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void quitmaterial(Activity activity, String userEmail, String materialname, String materialtype, String persentage) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            if (materialtype.equalsIgnoreCase("quiz"))
                jsonObject.put(AppConstants.MIXPANAL_TEST_RESULT, persentage);
            jsonObject.put(AppConstants.MIXPANAL_EMPOLYEEE_ID, WebServices.mLoginUtility.getEmployee_id());
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, APIUtility.DEVICE);
            jsonObject.put(AppConstants.MIXPANAL_FINISH_TIME, getCurrentTimeStamp());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        if (materialtype.equalsIgnoreCase("quiz")) {
            AppConstants.mixpanelAPI.track(AppConstants.USER_QUIT_QUIZ_EVENT, jsonObject);
        } else {
            AppConstants.mixpanelAPI.track(AppConstants.USER_COMPLATE_FLASHCARD_EVENT, jsonObject);
        }
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void timeoutmaterial(Activity activity, String userEmail, String materialname, String materialtype, String persentage) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_EMPOLYEEE_ID, WebServices.mLoginUtility.getEmployee_id());
            if (materialtype.equalsIgnoreCase("quiz"))
                jsonObject.put(AppConstants.MIXPANAL_TEST_RESULT, persentage);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, APIUtility.DEVICE);
            jsonObject.put(AppConstants.MIXPANAL_FINISH_TIME, getCurrentTimeStamp());
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        if (materialtype.equalsIgnoreCase("quiz")) {
            AppConstants.mixpanelAPI.track(AppConstants.USER_TIMEOUT_QUIZ_EVENT, jsonObject);
        } else {
            AppConstants.mixpanelAPI.track(AppConstants.USER_COMPLATE_FLASHCARD_EVENT, jsonObject);
        }
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void onVideoPlay(Activity activity, String userEmail, String materialname, String type) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
            if (type.equalsIgnoreCase("ok"))
                jsonObject.put(AppConstants.MIXPANAL_STREAM_OK, "OK");
            else
                jsonObject.put(AppConstants.MIXPANAL_STREAM_CANCEL, "CANCEL");
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.USER_SELECT_MATERIAL_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void Pausematerial(Activity activity, String userEmail, String coursename, String materialname, String materialtype, String timetaken, String testpattern) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, APIUtility.DEVICE);
            jsonObject.put(AppConstants.MIXPANAL_TIME_TAKEN, timetaken);
            jsonObject.put(AppConstants.MIXPANAL_TEST_PATTERN, testpattern);
            jsonObject.put(AppConstants.MIXPANAL_COURSE_NAME, coursename);
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        if (materialtype.equalsIgnoreCase("quiz")) {
            AppConstants.mixpanelAPI.track(AppConstants.USER_PAUSE_QUIZ_EVENT, jsonObject);
//            MyApplication.getInstance().trackEvent("Materials","Select Material",materialname+" ("+materialtype+" Pause)");
        } else {
            AppConstants.mixpanelAPI.track(AppConstants.USER_PAUSE_FLASHCARD_EVENT, jsonObject);
        }
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void completematerial(Activity activity, String userEmail, String materialname, String materialtype, String materialid) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            if (materialtype.toUpperCase().contains("QUIZ")) {
                String persent = materialtype.split("\\|")[1];
                String corr = materialtype.split("\\|")[2];
                String total = materialtype.split("\\|")[3];
                materialtype = materialtype.split("\\|")[0];
                jsonObject.put(AppConstants.MIXPANAL_MATERIAL_MARKS, persent);
                jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TOTAL_QUES, total);
                jsonObject.put(AppConstants.MIXPANAL_MATERIAL_CORRECT_QUES, corr);
                jsonObject.put(AppConstants.MIXPANAL_TOTAL_QUIZ_ATTEMPTS, SplashActivity.mPref.getString(materialid, "0"));
            }
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialtype);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, APIUtility.DEVICE);
            jsonObject.put(AppConstants.MIXPANAL_FINISH_TIME, getCurrentTimeStamp());
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
//        MyApplication.getInstance().trackEvent("Materials","Select Material",materialname+" ("+materialtype+" Completed)");
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        if (materialtype.equalsIgnoreCase("quiz")) {
            AppConstants.mixpanelAPI.track(AppConstants.USER_COMPLATE_QUIZ_EVENT, jsonObject);
        } else {
            AppConstants.mixpanelAPI.track(AppConstants.USER_COMPLATE_FLASHCARD_EVENT, jsonObject);
        }
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void completeSurvey(Activity activity, String userEmail, String materialname, String result, String materialid) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialname);
            jsonObject.put(AppConstants.MIXPANAL_MATERIAL_TYPE, "Survey");
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, APIUtility.DEVICE);
            jsonObject.put(AppConstants.MIXPANAL_FINISH_TIME, getCurrentTimeStamp());
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
//        MyApplication.getInstance().trackEvent("Materials","Select Material",materialname+" ("+materialtype+" Completed)");
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());

        AppConstants.mixpanelAPI.track(AppConstants.USER_COMPLETE_SURVEY, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    public void changepassword(Activity activity, String userEmail, String oldpass, String newpass) {
        if (WebServices.mLoginUtility.getUser_id() == null)
            return;


        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
//            jsonObject.put(AppConstants.MIXPANAL_OLD_PASS, oldpass);
//            jsonObject.put(AppConstants.MIXPANAL_NEW_PASS, newpass);
            jsonObject.put(AppConstants.MIXPANAL_PASSWORD_CHANGE_TIME, getCurrentTimeStamp());
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
            jsonObject.put(AppConstants.BRANCH_NAME, WebServices.mLoginUtility.getBranch_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
        people.identify(WebServices.mLoginUtility.getUser_id());
        AppConstants.mixpanelAPI.track(AppConstants.CHANGE_PASSWORD_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }
    /*public void userLogin(Activity activity, String userEmail, String fname, String lname, String ipaddress) {
        if (userEmail == null)
            return;
        MixpanelAPI.People people = Constants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
//        HashMap<String, String> userModelHashmap = new HashMap<>();
            jsonObject.put(Constants.MIXPANAL_EMAIL, userEmail);
            jsonObject.put(Constants.MIXPANAL_DEVICE_TYPE, Constants.MIXPANAL_DEVICE_TYPE_ANDROID);
            jsonObject.put(Constants.MIXPANAL_LOGIN_TIME, getCurrentTimeStamp());
            jsonObject.put(Constants.MIXPANAL_FIRSTNAME, fname);
            jsonObject.put(Constants.MIXPANAL_LASTNAME, lname);
            jsonObject.put(Constants.MIXPANAL_DEVICE_ID, android.os.Build.SERIAL);
            jsonObject.put(Constants.MIXPANAL_IPADDRESS, ipaddress);
            jsonObject.put(Constants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
        } catch (JSONException e) {

        }
        Constants.mixpanelAPI.identify(userEmail);
        people.identify(userEmail);

        // to get push notification.
//            String pushToken = SharedPreferenceManager.getSharedInstance().getDeviceTokenFromPreferences();
//            if (login && pushToken != null && !pushToken.trim().isEmpty()) {
//                people.identify(uniqueId);
//                people.setPushRegistrationId(pushToken);
//                people.initPushHandling(Constants.GOOGLE_SENDER_ID);
//            }

        Constants.mixpanelAPI.track(Constants.USER_LOGIN_EVENT, jsonObject);
        Constants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);

//        shootEventWithUser(Constants.USER_LOGIN_EVENT, jsonObject, true,activity);
    }*/

   /* *//**
     * Facebook login
     * @param userModel
     *//*
    public void userFacebookLogin(UserModel userModel) {
        if (userModel == null)
            return;

        HashMap<String, String> userModelHashmap = new HashMap<>();
        userModelHashmap.put(Constants.MIXPANAL_LOGIN_TYPE, Constants.MIXPANAL_FACEBOOK);
        userModelHashmap.put(Constants.MIXPANAL_DEVICE_TYPE, Constants.MIXPANAL_DEVICE_TYPE_ANDROID);
        userModelHashmap.put(Constants.MIXPANAL_LOGIN_TIME, getCurrentTimeStamp());

        HashMap<String, String> superUserModelHashmap = new HashMap<>();
        superUserModelHashmap.put(Constants.MIXPANAL_USER_EMAIL, userModel.userEmail);
        SharedPreferenceManager.getSharedInstance().saveMixPanelUniqueId(userModel.userEmail);

        shootEventWithUser(Constants.FACEBOOK_LOGIN_EVENT, userModelHashmap, superUserModelHashmap, true);
    }*/

    /**
     * gmail Login.
     *
     * @param
     *//*
    public void googlePlusLogin(UserModel userModel) {
        if (userModel == null)
            return;

        HashMap<String, String> userModelHashmap = new HashMap<>();
        userModelHashmap.put(Constants.MIXPANAL_LOGIN_TYPE, Constants.MIXPANAL_GMAIL);
        userModelHashmap.put(Constants.MIXPANAL_DEVICE_TYPE, Constants.MIXPANAL_DEVICE_TYPE_ANDROID);
        userModelHashmap.put(Constants.MIXPANAL_LOGIN_TIME, getCurrentTimeStamp());

        HashMap<String, String> superUserModelHashmap = new HashMap<>();
        superUserModelHashmap.put(Constants.MIXPANAL_USER_EMAIL, userModel.userEmail);
        SharedPreferenceManager.getSharedInstance().saveMixPanelUniqueId(userModel.userEmail);

        shootEventWithUser(Constants.G_PLUS_LOGIN_EVENT, userModelHashmap, superUserModelHashmap, true);
    }*/
    public void ePUBDownload(MeterialUtility materialModel) {
        if (materialModel == null)
            return;

        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialModel.getTitle());
        props.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialModel.getMaterial_type());
        shootEvent(AppConstants.EPUB_DOWNLOADED_EVENT, props);
    }

    public void ePUBAccess(MeterialUtility materialModel) {
        if (materialModel == null)
            return;

        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialModel.getTitle());
        props.put(AppConstants.MIXPANAL_MATERIAL_TYPE, materialModel.getMaterial_type());
        shootEvent(AppConstants.EPUB_ACCESS_EVENT, props);
    }


    public void passwordChanged(String email) {
        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_USER_EMAIL, email);
        shootEvent(AppConstants.CHANGE_PASSWORD_EVENT, props);
    }

    public void termsAndConditionOpened() {
        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.TERM_AND_CONDITION_OPEN_EVENT, AppConstants.TERM_AND_CONDITION_OPEN_DESCRIPTION);
        shootEvent(AppConstants.TERM_AND_CONDITION_OPEN_EVENT, props);
    }

    public void privacyPolicyOpened() {
        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.PRIVACY_POLICY_OPENED_EVENT, AppConstants.PRIVACY_POLICY_OPENED_DESCRIPTION);
        shootEvent(AppConstants.PRIVACY_POLICY_OPENED_EVENT, props);
    }

    public void userLogout() {
        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_LOGOUT_TIMESTAMP, getCurrentTimeStamp());
        shootEvent(AppConstants.USER_LOGGED_OUT_EVENT, props);
    }


    public void searchKeyWordTyped(String keyWord) {
        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_SEARCH_KEYWORD, keyWord);
        shootEvent(AppConstants.MIXPANEL_SEARCHKEYWORD_TYPED, props);
    }


    public void shootEvent(String eventName, HashMap<String, String> hashMap) {
        shootEvent(eventName, getJsonObject(hashMap));
    }

    public void shootEvent(String eventName, JSONObject jsonObject) {
        if (eventName != null && eventName.length() > 0) {
            String uniqueId = SplashActivity.mPref.getString("id", "");
            if (uniqueId == null || uniqueId.trim().isEmpty())
                AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
            else
                AppConstants.mixpanelAPI.identify(uniqueId);

            AppConstants.mixpanelAPI.track(eventName, jsonObject);
        }
    }

    public void shootEventWithUser(String eventName, HashMap<String, String> eventData, boolean login, Activity context) {
        if (eventName != null && eventName.length() > 0) {
            MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
            String uniqueId = SplashActivity.mPref.getString("id", "");

            if (uniqueId == null || uniqueId.trim().isEmpty()) {
                AppConstants.mixpanelAPI.identify(WebServices.mLoginUtility.getUser_id());
                people.identify(WebServices.mLoginUtility.getUser_id());
            } else {
                AppConstants.mixpanelAPI.identify(uniqueId);
                people.identify(uniqueId);
            }
            // to get push notification.
//            String pushToken = SharedPreferenceManager.getSharedInstance().getDeviceTokenFromPreferences();
//            if (login && pushToken != null && !pushToken.trim().isEmpty()) {
//                people.identify(uniqueId);
//                people.setPushRegistrationId(pushToken);
//                people.initPushHandling(Constants.GOOGLE_SENDER_ID);
//            }

            AppConstants.mixpanelAPI.track(eventName, getJsonObject(eventData));
            AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(context);
        }
    }


    public void ePubColorChanged(String materialName, String colorName) {
        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_MATERIAL_NAME, materialName);
        props.put(AppConstants.MIXPANAL_COLOR_NAME, colorName);
        shootEvent(AppConstants.EPUB_COLOR_CHANGED, props);
    }

    public void appComesInForeGround() {
        shootEvent(AppConstants.MIXPANEL_FOREGROUND, (JSONObject) null);
    }

    public void appIsInBackground() {
        shootEvent(AppConstants.MIXPANEL_BACKGROUND, (JSONObject) null);
    }

    public void selectedInterests(String interests) {
        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_SUBJECTS, interests);
        shootEvent(AppConstants.MIXPANAL_USER_INTERESTS, props);
    }


    public void sideMenuSelected() {

        HashMap<String, String> props = new HashMap<>();
        props.put(AppConstants.MIXPANAL_USER_EMAIL, SplashActivity.mPref.getString("id", ""));

        shootEvent(AppConstants.SLIDE_MENU_SELECTED_EVENT, props);
    }

//    public void materialInfoTabClicked(UserModel userModel, BookModel bookModel) {
//        HashMap<String, String> props = new HashMap<>();
//        props.put(Constants.MIXPANAL_USER_EMAIL, userModel.userEmail);
//        props.put(Constants.MIXPANAL_BOOK_NAME, bookModel.bookName.trim());
//        shootEvent(Constants.MODULE_INFO_CLICKED_EVENT, props);
//    }

    /**
     * generate current time stamp.
     */
    private String getCurrentTimeStamp() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(AppConstants.TIME_ZONE));
        Date date = calendar.getTime();
        return DateFormat.format(AppConstants.DATE_FORMAT, date).toString();
    }


}