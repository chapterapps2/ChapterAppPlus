package com.chaptervitamins.mixpanalManager;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

/**
 * Created by abcd on 6/4/2016.
 */
public class AppConstants {

    public static final int LOCATION_PERM = 1001;
    public static final int CAMERA = 1002;
    // Gadget type
    public interface CourseType{
        String COURSE = "COURSE";
        String LINK = "LINK";
        String NEWSFEED = "NEWSFEED";
        String SUGGESTION_QUERY = "SUGGESTION_QUERY";
        String DISCUSSION = "DISCUSSION";
        String SURVEY = "SURVEY";
        String GEO_AUDIT = "GEO_AUDIT";
        String ZOOM = "ZOOM";
        String REGISTER = "REGISTER";
        String SNAPSHOT = "SNAPSHOT";
        String ASSESSMENT = "ASSESSMENT";
        String EVENT = "EVENT";
        String LEADERBOARD = "LEADERBOARD";
    }

    public interface MaterialType{
        String MULTIMEDIAQUIZ = "MULTIMEDIAQUIZ";
        String QUIZ = "QUIZ";
        String CERTIFICATE = "CERTIFICATE";
        String HOSPITAL = "HOSPITAL";
        String SURVEY = "SURVEY";
        String CONTENTINSHORT = "CONTENTINSHORT";
        String IMAGECARD = "IMAGECARD";
        String FLASHCARD = "FLASHCARD";
        String VIDEO = "VIDEO";
        String FLASH = "FLASH";
        String CONTENT = "CONTENT";
        String AUDIO = "AUDIO";
        String PDF = "PDF";
        String EPUB = "EPUB";
        String FULL_TEXT = "FULL TEXT";
        String TINCAN_SCROM = "TINCAN-SCROM";
        String IMAGE = "IMAGE";
        String LINK = "LINK";
    }

    private static final String CONNECTIVITY_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    public static MixpanelAPI mixpanelAPI;

    public static final long SPLASH_SCREEN_DELAY = 2000;
    public static final long SLOW_NETWORK_ALERT_DURATION = 8000;
    public static final int VIEW_PAGER_LENGTH = 4;
    public static final int ERROR_CODE_FOR_INVALID_SESSION = 401;
    public static final int ERROR_CODE_FOR_INVALID_API_KEY = 402;
    public static final String API_KEY = "df07120fdb13c62777351771054b53b6";

    public static final String GOOGLE_SENDER_ID = "372707486661";
//    public static final String GOOGLE_API_KEY = "AIzaSyCqgVqrrBrTZFhUtpwTVTamnaWvUBfT4M4";
// Test below
//   public static final String GOOGLE_SENDER_ID = "535622152702";
//    public static final String GOOGLE_API_KEY = "AIzaSyAmBM3Ka9b7l87p0FtK2v8PTxXiqewSyZg";

    public static final String PUSH_REGISTRATION_ID = "pushRegistrationId";
    public static final String SELECE_VIDEO_PLAYER = "Select Viewo Player";

    /**
     * Time Constants
     */
    public static final String DATE_FORMAT ="yyyy-MM-dd hh:mm:ss";
    public static final String DATE_FORMAT_NEW ="MM-dd-yyyy hh:mm:ss";
    public static final String TIME_ZONE ="GMT";

    /**
     * URLs
     */
    public static final String BASE_URL = "http://52.11.56.53/ChapterVitamins/public/webservice/";    //Test
//    public static final String BASE_URL = "http://52.11.56.53/ChapterAppStaging/public/webservice/";    //AppStaging
//    public static final String BASE_URL = "http://52.11.183.46/ChapterVitamins/public/webservice/"; //Pre-Production
//    public static final String BASE_URL = "http://52.11.183.46/TheChapterApp/public/webservice/"; //production

    public static final String LOGIN_WEBSERVICE = "login";
    public static final String CREATE_ACCOUNT_WEBSERVICE = "create-account";
    public static final String GET_LEVELS_WEBSERVICE = "get-levels";
    public static final String GET_ALL_BOOKS_WEBSERVICE = "get-all-books";
    public static final String GET_MY_STUFF_WEBSERVICE = "get-my-stuff";
    public static final String SAVE_TRANSACTION = "save-transaction";
    public static final String SUBMIT_QUIZ = "submit-quiz";
    public static final String GET_MY_ATTEMPTED_QUIZ = "get-my-attempted-quizzes";
    public static final String EDIT_USER_PROFILE_WEBSERVICE = "edit-user-profile";
    public static final String GET_ENTITY_DETAILS_WEBSERVICE = "get-entity-details";
    public static final String DATABASE_NAME = "ChapterVitaminsDB";
    public static final String DATABASE_EXTENTION = ".sqlite";
    public static final String EMPTY_STRING = "";
    public static final String ANDROID = "Android";
    public static final String USER_DETAIL_KEY = "userDetail";
    public static final String FORGOT_PASSWORD_WEBSERVICE = "forget-password";
    public static final String PUSH_TOKEN = "1";
    public static final String SUBMIT_FEEDBACK_WEBSERVICE = "submit-feedback";
    public static final String GET_SUBJECTS_WEBSERVICE = "get-subjects";
    public static final String LOGOUT_WEBSERVICE = "logout";
    public static final String WEB_SERVICE_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
    public static final String CROPPED_IMAGE_FILE_NAME = "CroppedImage.jpg";
    public static final String TEMP_CAPTURED_IMAGE_FILE_NAME = "TemporaryImage.jpg";
    public static final String NULL_STRING = "null";
    public static final String USER_ID_SP_KEY = "userID";
    public static final String EMAIL_SP_KEY = "emailID";
    public static final String FB_ID_SP_KEY = "fbID";
    public static final String G_PLUS_ID_SP_KEY = "oAuthToken";
    public static final String PASSWORD_SP_KEY = "password";
    public static final String F_NAME_SP_KEY = "firstName";
    public static final String L_NAME_SP_KEY = "lastName";
    public static final String AGE_SP_KEY = "age";
    public static final String GENDER_SP_KEY = "gender";
    public static final String PHONE_SP_KEY = "phone";
    public static final String PROFILE_IMAGE_URL_SP_KEY = "userProfilePic";
    public static final String IS_INACTIVE_SP_KEY = "isInactive";
    public static final String IS_DELETED_SP_KEY = "isDeleted";
    public static final String MEDIA_ID_SP_KEY = "mediaID";
    public static final String MEDIA_NAME_SP_KEY = "mediaName";
    public static final String MEDIA_URL_SP_KEY = "mediaURL";
    public static final String MEDIA_TYPE_SP_KEY = "mediaType";
    public static final String MEDIA_ADDED_ON_SP_KEY = "addedOn";
    public static final String MEDIA_MODIFIED_ON_SP_KEY = "modifiedOn";
    public static final String MEDIA_IS_INACTIVE_SP_KEY = "mediaIsInactive";
    public static final String MEDIA_IS_DELETED_SP_KEY = "mediaIsDeleted";
    public static final String MEDIA_UPLOADED_BY_SP_KEY = "uploadedBy";
    public static final String ADDRESS_1_SP_KEY = "address1";
    public static final String ADDRESS_2_SP_KEY = "address2";
    public static final String CITY_SP_KEY = "city";
    public static final String GROUP_SP_KEY = "group";
    public static final String STATE_SP_KEY = "state";
    public static final String PINCODE_SP_KEY = "pincode";
    public static final String COUNTRY_SP_KEY = "country";
    public static final String COLLEGE_SP_KEY = "college";
    public static final String DEVICE_SP_KEY = "device";
    public static final String LEVEL_NAME_SP_KEY = "levelName";
    public static final String ORDER_SP_KEY = "order";
    public static final String LEVEL_ID_SP_KEY = "levelID";
    public static final String IS_LEVEL_INACTIVE_SP_KEY = "levelIsInactive";
    public static final String IS_LEVEL_DELETED_SP_KEY = "levelIsDeleted";
    public static final String MODULE_LIST_SP_KEY = "moduleList";
    public static final String STATUS_SP_KEY = "status";
    public static final String SESSION_TOKEN_SP_KEY = "token";
    public static final String IS_COLLEGE_SELECTED_SP_KEY = "isCollegeSelected";
    public static final String SUBJECT_IDS_SP_KEY = "userSubjects";
    public static final String COMMA_SEPRATED_BOOK_NAME_KEY = "commaSeparatedBookName";
    public static final String PAUSED_ACTIVITY = "pausedActivityName";
    public static final String BACKGROUND_ACTIVITY = "backgroundActivity";
    public static final String MIXPANEL_UNIQUE_ID = "mixPanelUniqueId";
    public static final String SUBJECT_NAME_SP_KEY = "userSubjectsName";
    public static final String GET_QUIZ = "get-quiz-details";
    public static final String IS_SOCIAL_LOGIN = "isSocialLogin";
    public static final String IS_SIGNUP = "isSignup";
    public static final String IS_QUIZ = "isQuiz";

    public static final String PROFILE_PIC_DIR = "/The ChapterApp";
    //0=normal, 1=facebook, 2=Gplus
    //0=login, 1=signup
//    public static final String MIXPANEL_PROJECT_TOKEN="3cf4c391a0c84534e3fffa24aaf1b4ce";a63fd4b1b17fc3f767d923329bbe61ce


//    public static final String MIXPANEL_PROJECT_TOKEN="fbd0c53148f0888077ce2fd2c7ff76c2";


    // Mixpannel event string Constants.
    public static final String MIXPANAL_ORGANIZATION_NAME = "aa_Organization Name";
    public static final String MIXPANAL_EMPLOYEE_ID = "employee_id";
    public static final String MIXPANAL_FIRSTNAME = "$first_name";
    public static final String MIXPANAL_LASTNAME = "$last_name";
    public static final String MIXPANAL_USER_EMAIL = "$email";
    public static final String MIXPANAL_USER_PHONE = "$user_phone";
    public static final String USER_SELECT_MATERIAL_TIME_TRACK = "aa_Exit Material";
    public static final String MIXPANAL_DEVICE_TYPE  = "aa_Device Type";
    public static final String MIXPANAL_TIME_TAKEN  = "aa_Time Taken";
    public static final String MIXPANAL_MATERIAL_START_TIME = "aa_Material StartTime";
    public static final String MIXPANAL_MATERIAL_END_TIME = "aa_Material EndTime";
    public static final String MIXPANAL_PDF_PAGE_READ = "Pdf_Page_Read";
    public static final String MIXPANAL_PDF_PAGE_Total = "Pdf_Page_Total";
    public static final String MIXPANAL_VIDEO_SEEN_TIME = "Material_Video_Seen_Time";
    public static final String MIXPANAL_VIDEO_TOTAL_TIME= "Material_Video_Total_Time";
    public static final String MIXPANAL_AUDIO_LISTEN_TIME = "Material_Audio_Listen_Time";
    public static final String MIXPANAL_AUDIO_TOTAL_TIME= "Material_Audio_Total_Time";
    public static final String MIXPANAL_FINISH_TIME  = "aa_Finish Time";
    public static final String MIXPANAL_TEST_PATTERN  = "aa_Test Pattern";
    public static final String MIXPANAL_MSG = "aa_Login_Msg";
    public static final String MIXPANAL_IPADDRESS = "aa_IP Address";
    public static final String MIXPANAL_DEVICE_ID = "aa_Device ID";
    public static final String MIXPANAL_MATERIAL_NAME = "aa_Material Name";
    public static final String MIXPANAL_USER_INTERESTS = "User Interests";
    public static final String MIXPANAL_SUBJECTS = "Subjects";
    public static final String USER_Login_MSG = "aa_Login_Msg";
    public static final String MIXPANAL_COURSE_NAME = "aa_Gadget Name";
    public static final String MIXPANAL_DRAWER_ITEM = "aa_Drawer_Item";
    public static final String MIXPANAL_DRAWER_ITEM_NAME = "Item Name";
    public static final String BRANCH_NAME = "Branch Name";
    public static final String USER_NAME = "aa_User Id";
    public static final String RESET = "Reset";
    public static final String REASON = "Reason";
    public static final String SESSION_TOKEN = "Session Token";
    public static final String MIXPANAL_RESET = "aa_Reset";
    public static final String MIXPANAL_FORGETPWD = "aa_ForgetPass";
    public static final String MIXPANAL_NOTIFICATION_NUMBER = "aa_Total_Notification";
    public static final String MIXPANAL_STREAM_OK = "aa_vStream OK";
    public static final String MIXPANAL_STREAM_CANCEL = "aa_vStream Cancel";
    public static final String MIXPANAL_MATERIAL_TYPE = "aa_Material Type";
    public static final String MIXPANAL_WAARNING_TIME = "Warning Time";
    public static final String QUIZ_WARNING = "aa_Quiz Warning";
    public static final String MIXPANAL_COLOR_NAME = "Color Name";
    public static final String MIXPANAL_MATERIAL_MARKS = "aa_Material Marks pct";
    public static final String MIXPANAL_MATERIAL_TOTAL_QUES = "aa_Material Total Ques";
    public static final String MIXPANAL_MATERIAL_CORRECT_QUES = "aa_Material Correct Ques";
    public static final String MIXPANAL_TOTAL_QUIZ_ATTEMPTS = "aa_Quiz Attempts";
    public static final String MIXPANAL_LEVEL_NAME = "LevelName";
    public static final String MIXPANAL_LEVEL = "Level";
    public static final String MIXPANAL_MODULE_NAME = "aa_Module Name";
    public static final String USER_TIMEOUT_QUIZ_EVENT = "aaTimeout Quiz";
    public static final String MIXPANAL_TEST_RESULT = "aatest result";
    public static final String MIXPANAL_CORRECTANSWER = "Correct Answer";
    public static final String MIXPANAL_USERANSWER = "User Answer";
    public static final String MIXPANAL_ISCORRECT = "IsCorrect";
    public static final String MIXPANAL_QUESTION = "Question";
    public static final String USER_QUIT_QUIZ_EVENT = "Quit Quiz";
    public static final String MIXPANAL_EMPOLYEEE_ID = "aa_employee_id";
    public static final String USER_SELECT_MATERIAL_NO_ATTEMPT_EVENT = "Attempts Expired";
    public static final String USER_QUIZ_RESPONSE_EVENT = "Quiz Response";
    public static final String USER_QUESTION_REVIEWED = "aa_Question Reviewed";
    public static final String USER_DOWNLOAD_MATERIAL_EVENT = "Download Material";
    public static final String MIXPANAL_READER_COLOR = "Selected Color";
    public static final String MIXPANAL_FEEDBACK_DETAIL = "Feedback Detail";
    public static final String MIXPANAL_SEARCH_KEYWORD = "KeyWords";
    public static final String MIXPANAL_SEARCH_CLICK= "Search";
    public static final String MIXPANAL_COLLEGE_NAME = "CollegeName";
    public static final String MIXPANAL_INTEREST_NAME = "Subject Name";
    public static final String MIXPANAL_CREATED_TIMESTAMP = "Created On";
    public static final String MIXPANAL_ACCESS_TIMESTAMP = "Access Time";
    public static final String MIXPANAL_LOGOUT_TIMESTAMP = "aa_Logout Time";
    public static final String MIXPANAL_AUTO_LOGOUT = "aa_Auto_Logout";
    public static final String MIXPANAL_REGISTER_TYPE = "Register Type";
    public static final String MIXPANAL_LOGIN_TYPE  = "aa_Login Type";
    public static final String MIXPANAL_LOGIN_TIME  = "aa_Loggin Time";
    public static final String MIXPANAL_OLD_PASS  = "aa_Old Password";
    public static final String MIXPANAL_NEW_PASS  = "aa_New Password";
    public static final String MIXPANAL_PASSWORD_CHANGE_TIME  = "Password Change Time";
    public static final String MIXPANAL_BOOK_NAME  = "Book Name";
    public static final String MIXPANAL_DOWNLOADED_BOOK  = "Downloaded Book";

    public static final String MIXPANEL_MODULE_SELECTED = "Module Selected";
    public static final String MIXPANEL_LEVEL_SELECTED = "Level Selected";
    public static final String MIXPANEL_SUBJECT_SELECTED = "Subject Selected";
    public static final String MIXPANEL_PROFILE_UPDATED = "Profile Updates";
    public static final String MIXPANEL_COLLEGE_CHANGED = "College Changed";
    public static final String MIXPANEL_INTEREST_SELECTED = "Interest Selected";
    public static final String MIXPANEL_SEARCHKEYWORD_TYPED = "Search Keyword Typed";


    public static final String MIXPANEL_MATERIAL_SELECTED = "Material Selected";
    public static final String MIXPANEL_INFO_SCREEN_SELECTED = "Infor Screen Selected";
    public static final String MIXPANEL_MODULE_DOWNLOADED = "Module Downloaded";
    public static final String MIXPANEL_FULL_TEXT_OPENED = "Full Text Opened ";
    public static final String MIXPANEL_QUIZ_OPENED = "Quiz Accessed";
    public static final String MIXPANEL_PDF_ACCESSED = "PDF Accessed";
    public static final String MIXPANEL_QUIZ_TAKEN = "Quiz taken";
    public static final String MIXPANEL_APP_LAUNCHED = "aa_App Launched";
    public static final String MIXPANEL_FOREGROUND = "aa_App Foreground";
    public static final String MIXPANEL_BACKGROUND = "aa_App Background";
    public static final String MIXPANEL_VIDEO_LINK_SELECTED = "aa_Video Accessed";
    public static final String MIXPANEL_QUIZ_COMPLETED = "aa_Quiz Completed";
    public static final String MIXPANEL_LEADERBOARD_SCREEN_SELECTED = "Leader Screen Selected";
    public static final String MIXPANEL_QUIZ_SKIPPED = "aa_Quiz Skipped";
    public static final String MIXPANEL_QUIZ_QUIT = "aa_Quiz Quit";
    public static final String MIXPANEL_QUIZ_TIME_OUT = "aa_Quiz Timed out";
    public static final String MIXPANEL_COLOR_SELECTED_ON_READER_SCREEN = "Color Selected On Reader Screen";
    public static final String MIXPANEL_FEEDBACK_SENT = "Feedback sent from within app";
    public static final String MIXPANEL_BOOK_DOWNLOAD = "Book Downloaded";
    public static final String MIXPANEL_BOOK_SELECTED = "Book Selected";
    public static final String MIXPANAL_QUIZ_NAME = "Quiz Name";
    public static final String MIXPANAL_TOTAL_QUESTION = "aa_Total Question";
    public static final String MIXPANAL_ALLOTTED_TIME = "aa_Allotted Time";
    public static final String MIXPANAL_START_TIME = "aa_Start Time";

    // Mixpannel Value string
    public static final String MIXPANAL_GMAIL = "Google";
    public static final String MIXPANAL_FACEBOOK = "Facebook";
    public static final String MIXPANAL_EMAIL = "$aa_email";
    public static final String MIXPANAL_OTP = "$aa_phone";
    public static final String MIXPANAL_ERROR = "aa_error";
    public static final String MIXPANAL_UPDATE = "aa_app_update";
    public static final String MIXPANAL_DEVICE_TYPE_ANDROID = "aa_Android";
    public static final String SLIDE_MENU_SELECTED_EVENT = "aa_Side Menu Selected";
    public static final String NOTE_DELETED_EVENT = "aa_Note Deleted";
    public static final String EPUB_DOWNLOADED_EVENT = "aa_EPUB Downloaded";
    public static final String EPUB_ACCESS_EVENT = "aa_EPUB Accessed";
    public static final String EPUB_COLOR_CHANGED = "aa_EPUB Color Changed";
    public static final String EPUB_PURCHASE_EVENT = "aa_EPUB Purchase";
    public static final String CHAPTER_SELECTED_EVENT = "Chapter Selected";
    public static final String NOTE_ADDED_EVENT = "aa_Note Added";
    public static final String CREATE_ACCOUNT_EVENT = "aa_Create Account";
    public static final String USER_CREATED_ACCOUNT_EVENT = "aa_User Profile";
    public static final String USER_REGISTERED_EVENT = "aa_User Registered";
    public static final String CHANGE_PASSWORD_EVENT = "aa_Change Password";
    public static final String USER_CLICK_FORGOT_PASSWORD_EVENT = "aa_Forgot Password";
    public static final String USER_LOGIN_EVENT = "aa_User Login";
    public static final String USER_AUTO_LOGIN_EVENT = "aa_Auto Login";
    public static final String USER_LOGIN_OTP_EVENT = "aa_User Login OTP";
    public static final String USER_LOGOUT_EVENT = "aa_User Logout";
    public static final String USER_SELECT_COURSE_EVENT = "aa_Select Gadget";
    public static final String SYNC = "aa_Sync";
    public static final String NOTIFICATION_CLICK_EVENT = "aa_Notification_Click";
    public static final String USER_NOTIFICATION_EVENT = "aa_Sent_Notification";
    public static final String USER_GET_EVENT = "aa_error";
    public static final String USER_UPDATE_EVENT = "aa_app_update";
    public static final String USER_SELECT_MATERIAL_EVENT = "aa_Select Material";
    public static final String USER_FLOWING_COURSE = "aa_Flowing Course";
    public static final String USER_SELECT_MODULE_EVENT = "aa_Select Module";
    public static final String USER_START_QUIZ_EVENT = "aa_Start Quiz";
    public static final String USER_START_VIDEOQUIZ_EVENT = "aa_Start Video Quiz";
    public static final String USER_START_FLASHCARD_EVENT = "aa_Start Flashcard";
    public static final String USER_PAUSE_QUIZ_EVENT = "aa_Pause Quiz";
    public static final String USER_PAUSE_FLASHCARD_EVENT = "aa_Pause Flashcard";
    public static final String USER_COMPLATE_QUIZ_EVENT = "aa_Complete Quiz";
    public static final String USER_COMPLETE_SURVEY = "aa_Complete Survey";
    public static final String USER_COMPLATE_FLASHCARD_EVENT = "aa_Complete Flashcard";
    public static final String FACEBOOK_LOGIN_EVENT = "Facebook Login";
    public static final String G_PLUS_LOGIN_EVENT = "Google Plus Login";
    public static final String USER_REGISTER_SUCCESS_FB_EVENT = "Facebook User Registered";
    public static final String USER_REGISTER_SUCCESS_GPLUS_EVENT = "Google Plus User Registered";
    public static final String TERM_AND_CONDITION_OPEN_EVENT = "Terms and condition opened";
    public static final String PRIVACY_POLICY_OPENED_EVENT = "Privacy Policy opened";
    public static final String USER_LOGGED_OUT_EVENT = "aa_User Logout";
    public static final String USER_SELECTED_TITLE_EVENT = "aa_Title Selected";
    public static final String USER_ACCESS_TABLE_CONTENT_EVENT = "Table of Content";
    public static final String FORUM_POST_ACCESSED_EVENT = "Forum Detail";
    public static final String FORUM_REPORTED_BY_USER_EVENT = "Forum Reported";
    public static final String FORUM_POST_INITIATED_EVENT = "Forum Post Initiated";
    public static final String FORUM_POST_CREATED_EVENT = "Forum Post Created";
    public static final String FORUM_EDITED_EVENT = "Forum Edited";
    public static final String EPUB_FORUM_POST_CREATED_EVENT = "Epub Forum Post Created";
    public static final String EPUB_FORUM_POST_INITIATED_EVENT = "Epub Forum Post Initiated";
    public static final String FORUM_REPLY_EVENT = "Forum Reply";
    public static final String ABOUT_BOOK_CLICKED_EVENT = "About Book Clicked";
    public static final String MODULE_INFO_CLICKED_EVENT = "Module Info Clicked";
    public static final String MATERIAL_ACCESS_EVENT = "Material Access";
    public static final String START_QUIZ_EVENT = "aa_Start Quiz";
    public static final String FINISH_QUIZ_EVENT = "aa_Finish Quiz";
    public static final String START_FLASH_CARD_EVENT = "Start Flash card";
    public static final String FINISH_FLASH_CARD_EVENT = "Finish Flash card";
    public static final String PROFILE_UPDATED_EVENT = "Profile Updates";


    // Mixpannel description string

    public static final String NOTE_DELETED_DESCRIPTION = "Note has been deleted";
    public static final String EPUB_DOWNLOADED_DESCRIPTION = "ePub downloaded after purchase (if data gets wiped from device about chapter is linked to user and he downloads it again)";
    public static final String EPUB_ACCESS_DESCRIPTION = "ePub accessed";
    public static final String EPUB_PURCHASE_DESCRIPTION = "First time user clicks on a chapter to download";
    public static final String CHAPTER_SELECTED_DESCRIPTION = "Chapter detail screen accessed";
    public static final String NOTE_ADDED_DESCRIPTION = "Note has been added by a user";
    public static final String CREATE_ACCOUNT_DESCRIPTION = "User clicked on create account via email";
    public static final String USER_CREATED_ACCOUNT_DESCRIPTION = "Account for user has been created via email";
    public static final String CHANGE_PASSWORD_DESCRIPTION = " changed passsword";
    public static final String USER_CLICK_FORGOT_PASSWORD_DESCRIPTION = " clicked on forgot password";
    public static final String USER_LOGIN_DESCRIPTION = "User logged in";
    public static final String FACEBOOK_LOGIN_DESCRIPTION = "User logged in through facebook";
    public static final String G_PLUS_LOGIN_DESCRIPTION = "User logged in through Google Plus";
    public static final String USER_REGISTER_SUCCESS_FB_DESCRIPTION = "User has registered successfully using Facebook";
    public static final String USER_REGISTER_SUCCESS_GPLUS_DESCRIPTION = "User has registered successfully using Google Plus";
    public static final String TERM_AND_CONDITION_OPEN_DESCRIPTION = "Terms and conditions page accessed";
    public static final String PRIVACY_POLICY_OPENED_DESCRIPTION = "Privacy policy opened";
    public static final String USER_LOGGED_OUT_DESCRIPTION = "User logged out";
    public static final String USER_SELECTED_TITLE_DESCRIPTION = "User selected title";
    public static final String USER_ACCESS_TABLE_CONTENT_DESCRIPTION = "User accessed table of content";
    public static final String FORUM_POST_ACCESSED_DESCRIPTION = "Forum post accessed";
    public static final String FORUM_REPORTED_BY_USER_DESCRIPTION = "Forum reported by user";
    public static final String FORUM_POST_INITIATED_DESCRIPTION = "Forum post initiated";
    public static final String FORUM_POST_CREATED_DESCRIPTION = "Forum post created";
    public static final String FORUM_EDITED_DESCRIPTION = "Forum post edited";
    public static final String EPUB_FORUM_POST_CREATED_DESCRIPTION = "Forum post from epub created";
    public static final String EPUB_FORUM_POST_INITIATED_DESCRIPTION = "Forum post form epub initiated";
    public static final String FORUM_REPLY_DESCRIPTION = "Reply to a forum post";

    public static final String UNABLE_TO_LOAD_QUIZ = "Unable to load Quiz.";
    public static final String QUIZ_NOT_AVAILABLE_OFFLINE = "This quiz has not been downloaded.  Please try with a better data connection";
    public static final String NO_QUESTION_EXIST_IN_QUIZ = "No question exist corresponding to this quiz";

    public static final String UNABLE_TO_LOAD_FLASH_CARD = "Unable to load Flash Card.";
    public static final String FLASH_CARD_NOT_AVAILABLE_OFFLINE = "This Flash card has not been downloaded.  Please try with a better data connection";
    public static final String NO_QUESTION_EXIST_IN_FLASH_CARD = "No card exist corresponding to this Flash Card";

    public static final int LIST_POSITION = 0x10;


    /**
     * Mixpanel Event Key from Web
     */
    public static final String TAKE_QUIZ = "takeQuiz";
    public static final String START_QUIZ = "startQuiz";
    public static final String QUIT_QUIZ = "quitQuiz";
    public static final String COMPLETE_QUIZ = "completeQuiz";
    public static final String FINISH_QUIZ = "finishQuiz";
    public static final String START_FLASH_CARD = "startFlashCard";
    public static final String QUIT_FLASH_CARD = "quitFlashCard";
    public static final String FINISH_FLASH_CARD = "finishFlashCard";


}
