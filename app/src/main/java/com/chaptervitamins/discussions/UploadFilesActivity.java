package com.chaptervitamins.discussions;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.splunk.mint.Mint;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Android on 7/5/2016.
 */

public class UploadFilesActivity extends BaseActivity {
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private int PICK_IMAGE_REQUEST = 1;
    private static final int PICKFILE_RESULT_CODE = 2;
    private TextView upload_note_txt, upload_pdf_txt, upload_img_txt, post_ques_txt;
    private String userChoosenTask;
    public static final int MEDIA_TYPE_IMAGE = 1;
    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";
    private final int REQUEST_PER_CAMERA = 3;
    private final int REQUEST_EXTERNAL_STORAGE_NOTE = 4;
    private final int REQUEST_EXTERNAL_STORAGE_PDF = 5;

    private Uri fileUri; // file url to store image/video

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        // TODO: Update with your API key
        Mint.initAndStartSession(UploadFilesActivity.this, Constants.CRASH_REPORT_KEY);
        setContentView(R.layout.uploadfiles_activity);
//        changeStatusBarColor();
        boolean showPdf = true;
        if(getIntent().hasExtra("show_pdf"))
         showPdf = false;
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        upload_note_txt = (TextView) findViewById(R.id.upload_note_txt);
        upload_pdf_txt = (TextView) findViewById(R.id.upload_pdf_txt);
        upload_img_txt = (TextView) findViewById(R.id.upload_img_txt);
        if(!showPdf)
            upload_pdf_txt.setVisibility(View.GONE);
        post_ques_txt = (TextView) findViewById(R.id.post_ques_txt);
        upload_note_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPermission(REQUEST_EXTERNAL_STORAGE_NOTE);

            }
        });
        upload_pdf_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPermission(REQUEST_EXTERNAL_STORAGE_PDF);

            }
        });
        upload_img_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPermission(REQUEST_PER_CAMERA);
            }
        });
        post_ques_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void showPermission(int permissionType) {
        if (permissionType == REQUEST_PER_CAMERA) {
            if (ContextCompat.checkSelfPermission(UploadFilesActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(UploadFilesActivity.this, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions(UploadFilesActivity.this,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            permissionType);

                } else {
                    ActivityCompat.requestPermissions(UploadFilesActivity.this,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            permissionType);
                }
            } else {
                selectImage();
            }
        } else {
            if (ContextCompat.checkSelfPermission(UploadFilesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(UploadFilesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(UploadFilesActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            permissionType);

                } else {
                    ActivityCompat.requestPermissions(UploadFilesActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            permissionType);
                }
            } else {
                if (permissionType == REQUEST_EXTERNAL_STORAGE_NOTE)
                    uploadNote();
                else
                    uploadPDF();
            }
        }
    }


    private void uploadNote() {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{".txt", ".doc"};
        FilePickerDialog dialog = new FilePickerDialog(UploadFilesActivity.this, properties);
        dialog.show();
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                if (files.length > 0) {
                    System.out.println("====file====choose===" + files[0].toString());
                    Intent intent = new Intent();
                    intent.putExtra("filename", files[0].toString());
                    setResult(3000, intent);
                    finish();
                }
            }

        });
    }

    private void uploadPDF() {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{".pdf"};
        FilePickerDialog dialog = new FilePickerDialog(UploadFilesActivity.this, properties);
        dialog.show();
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                if (files.length > 0) {
                    System.out.println("====file====choose===" + files[0].toString());
                    Intent intent = new Intent();
                    intent.putExtra("filename", files[0].toString());
                    setResult(3000, intent);
                    finish();
                }
            }

        });
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(UploadFilesActivity.this);
        builder.setTitle("Select Option");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(UploadFilesActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                    dialog.dismiss();
                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();
                    dialog.dismiss();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PER_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    Toast.makeText(this, "Sorry! you cannot use this feature", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_EXTERNAL_STORAGE_NOTE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    uploadNote();
                } else {
                    Toast.makeText(this, "Sorry! you cannot use this feature", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_EXTERNAL_STORAGE_PDF:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    uploadPDF();
                } else {
                    Toast.makeText(this, "Sorry! you cannot use this feature", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);


//            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if (userChoosenTask.equals("Take Photo"))
//                        cameraIntent();
//                    else if (userChoosenTask.equals("Choose from Gallery"))
//                        galleryIntent();
//                } else {
//                    //code for deny
//                }
//                break;
        }
    }

    private void galleryIntent() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);//
//        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_FILE);


    }

    private void cameraIntent() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//        // start the image capture Intent
//        startActivityForResult(intent, REQUEST_CAMERA);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String FILENAME = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        try {
            fileUri = Uri.fromFile(APIUtility.getFileFromURL(UploadFilesActivity.this, FILENAME + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        startActivityForResult(intent, REQUEST_CAMERA);
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }

        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            String filepath = fileUri.getPath();
            if (filepath == null) {
                Toast.makeText(UploadFilesActivity.this, "Error FIle!!", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent();
            intent.putExtra("filename", filepath);
            setResult(2000, intent);
            finish();
        } else if (resultCode == RESULT_OK) {
            String FilePath = "";
            try {

                if (requestCode == SELECT_FILE && resultCode == RESULT_OK
                        && null != data) {


                    Uri URI = data.getData();
                    String[] FILE = {MediaStore.Images.Media.DATA};


                    Cursor cursor = getContentResolver().query(URI,
                            FILE, null, null, null);

                    cursor.moveToFirst();
//
                    int columnIndex = cursor.getColumnIndex(FILE[0]);
                    FilePath = cursor.getString(columnIndex);
                    cursor.close();
//
//                    imageViewLoad.setImageBitmap(BitmapFactory
//                            .decodeFile(ImageDecode));
                }
            } catch (Exception e) {
                Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                        .show();
                return;
            }


            System.out.println("=FilePath===" + FilePath);
            // Get the File path from the Uri

            // Alternatively, use FileUtils.getFile(Context, Uri)
            if (FilePath != null && !FilePath.equalsIgnoreCase("")) {
                File file = new File(FilePath);
                System.out.println("====file====choose===" + file.getAbsolutePath());
                Intent intent = new Intent();
                intent.putExtra("filename", file.getAbsolutePath());
                setResult(2000, intent);
                finish();
            }
        }
    }/*else
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            System.out.println("=uriuri===" + uri.getPath());
            // Get the File path from the Uri
            String path = getPath( uri);

            // Alternatively, use FileUtils.getFile(Context, Uri)
            if (path != null ) {
                File file = new File(path);
                System.out.println("====file====choose==="+file.getAbsolutePath());
                Intent intent=new Intent();
                intent.putExtra("filename",file.getAbsolutePath());
                setResult(2000,intent);
                finish();
            }
        }else*/ /*{
            if (resultCode == RESULT_OK) {

                if (resultCode == RESULT_OK) {

                    final Uri uri = data.getData();

                    // Get the File path from the Uri
                    String path = FileUtils.getPath(this, uri);

                    // Alternatively, use FileUtils.getFile(Context, Uri)
                    if (path != null && FileUtils.isLocal(path)) {
                        File file = new File(path);
                    }
                }
                String FilePath = data.getData().getPath();
                System.out.println("=FilePath===" + FilePath);
                // Get the File path from the Uri

                // Alternatively, use FileUtils.getFile(Context, Uri)
                if (FilePath != null) {
                    File file = new File(FilePath);
                    System.out.println("====file====choose===" + file.getAbsolutePath());
                    Intent intent = new Intent();
                    intent.putExtra("filename", file.getAbsolutePath());
                    setResult(3000, intent);
                    finish();
                }
            }
        }*/

//    }

    private String onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".png");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return destination.getAbsolutePath();
    }


    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    /*
     * Creating file uri to store image/video
	 */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } /*else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } */ else {
            return null;
        }

        return mediaFile;
    }
}
