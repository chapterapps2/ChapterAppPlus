package com.chaptervitamins.discussions;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.downloadImages.ImageLoader;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.utility.GroupMembersUtils;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Android on 7/5/2016.
 */

public class MemberListActivity extends BaseActivity {
    private ListView listView;
    private LayoutInflater mInflater;
    private ImageLoader imageLoader;
    private MyMemberAdapter adapter;
    private ArrayList<GroupMembersUtils> arrayList = new ArrayList<>();
    private ArrayList<GroupMembersUtils> arrayList2 = new ArrayList<>();
    EditText search_edt;
    private TextView add_txt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.member_list_activity);
//        changeStatusBarColor();
        ImageView back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        arrayList = WebServices.membersUtilses;

         search_edt = (EditText) findViewById(R.id.search_edt);
        add_txt = (TextView) findViewById(R.id.add_txt);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String[] userids = getIntent().getStringExtra("userid").toString().split("\\|");
        arrayList=new ArrayList<>();
        arrayList2=new ArrayList<>();
        for (int ii = 0; ii < WebServices.membersUtilses.size(); ii++) {
            boolean isAval = false;

            for (int i = 0; i < userids.length; i++) {

                if (userids[i].equalsIgnoreCase(WebServices.membersUtilses.get(ii).getUser_id())||WebServices.membersUtilses.get(ii).isSelected()) {
                    isAval = true;
                    break;
                }
            }
            if (!isAval) {
                if (!WebServices.membersUtilses.get(ii).isSelected()) {
                    arrayList.add(WebServices.membersUtilses.get(ii));
                    arrayList2.add(WebServices.membersUtilses.get(ii));
                }
            }
        }

        search_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = search_edt.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        add_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String allIds = "";
                for (int i = 0; i < arrayList.size(); i++) {
                    if (arrayList.get(i).isSelected())
                        allIds = allIds + arrayList.get(i).getUser_id() + "|";
                }
                for (int i=0;i<WebServices.membersUtilses.size();i++){
                    for (int j=0;j<arrayList.size();j++){
                        if (arrayList.get(j).isSelected()) {
                            if (WebServices.membersUtilses.get(i).getUser_id().equalsIgnoreCase(arrayList.get(j).getUser_id()))
                            WebServices.membersUtilses.get(i).setSelected(true);
                            break;
                        }
                    }
                }
                Intent intent = new Intent();
                intent.putExtra("id", allIds);
                setResult(99, intent);
//                WebServices.membersUtilses=arrayList;
                finish();
            }
        });
        imageLoader = new ImageLoader(MemberListActivity.this);
        listView = (ListView) findViewById(R.id.member_list);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        adapter = new MyMemberAdapter();
        listView.setAdapter(adapter);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.primary_dark));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            for (int i=0;i<WebServices.membersUtilses.size();i++){
                for (int j=0;j<arrayList.size();j++){
                    if (arrayList.get(j).isSelected()) {
                        if (WebServices.membersUtilses.get(i).getUser_id().equalsIgnoreCase(arrayList.get(i).getUser_id()))
                        WebServices.membersUtilses.get(i).setSelected(true);
                        break;
                    }
                }
            }
//            WebServices.membersUtilses=arrayList;
            this.finish();
        }


        return true;
    }

    @Override
    public void onBackPressed() {
        for (int i=0;i<WebServices.membersUtilses.size();i++){
            for (int j=0;j<arrayList.size();j++){
                if (arrayList.get(j).isSelected()) {
                    if (WebServices.membersUtilses.get(i).getUser_id().equalsIgnoreCase(arrayList.get(i).getUser_id()))
                    WebServices.membersUtilses.get(i).setSelected(true);
                    break;
                }
            }
        }
//        WebServices.membersUtilses=arrayList;
        this.finish();
    }

    private class MyMemberAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.member_list_row, null);
                CircleImageView profile_img = (CircleImageView) convertView.findViewById(R.id.profile_image);
                TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                final RadioButton radioButton = (RadioButton) convertView.findViewById(R.id.radioButton);
                TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt);


                String fname=arrayList.get(position).getFirstname().trim().toString().substring(0,1);
                member_fname_txt.setText(fname);
                radioButton.setChecked(arrayList.get(position).isSelected());
                imageLoader.DisplayImage(arrayList.get(position).getPhoto(), profile_img);
//                if (!arrayList.get(position).getLastname().equalsIgnoreCase("null") && arrayList.get(position).getLastname() != null)
//                    member_name.setText(arrayList.get(position).getFirstname() + " " + arrayList.get(position).getLastname());
//                else
                    member_name.setText(arrayList.get(position).getFirstname());
                if (position % 4 == 0) {
                    member_name.setTextColor(Color.parseColor("#ed873e"));
                    profile_img.setBorderColor(Color.parseColor("#ed873e"));
                } else if (position % 4 == 1) {
                    member_name.setTextColor(Color.parseColor("#6fb5ec"));
                    profile_img.setBorderColor(Color.parseColor("#6fb5ec"));
                } else if (position % 4 == 2) {
                    member_name.setTextColor(Color.parseColor("#4dbfb0"));
                    profile_img.setBorderColor(Color.parseColor("#4dbfb0"));
                } else if (position % 4 == 3) {
                    member_name.setTextColor(Color.parseColor("#bf564d"));
                    profile_img.setBorderColor(Color.parseColor("#bf564d"));
                }
                radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (arrayList.get(position).isSelected()) {
                            arrayList.get(position).setSelected(false);
                            radioButton.setChecked(false);
                        } else {
                            arrayList.get(position).setSelected(true);
                            radioButton.setChecked(true);
                        }
                        adapter.notifyDataSetChanged();
                    }

                });
            }
            return convertView;
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            arrayList.clear();
            if (charText.length() == 0) {
                arrayList.addAll(arrayList2);
            } else {
                for (GroupMembersUtils wp : arrayList2) {
                    String memberName = "";
//                    if (!wp.getLastname().equalsIgnoreCase("null") && wp.getLastname() != null) {
//                        memberName = wp.getFirstname() + " " + wp.getLastname();
//                    } else {
                        memberName = wp.getFirstname();
//                    }
                    if (memberName.toLowerCase(Locale.getDefault()).contains(charText)) {
                        arrayList.add(wp);
                    }
                }
            }
            if (arrayList.size() == 0)
                Toast.makeText(MemberListActivity.this, "No result found", Toast.LENGTH_LONG).show();
            notifyDataSetChanged();
        }
    }
}
