package com.chaptervitamins.discussions;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.downloadImages.Profile_ImageLoader;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.GroupMembersUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Android on 7/1/2016.
 */

public class GroupsFragment extends Fragment {
    //    private SwipeRefreshLayout swipeContainer;
    private WebServices webServices;
    private LayoutInflater mInflater;
    private Profile_ImageLoader imageLoader;
    private GroupAdapter adapter;
    private ListView lvItems;
    private String Group_Id = "";
    private String course_name = "";
    private String CourseID = "";
    private TextView add_private_group;
    //    ArrayList<GroupUtils> groupUtilsArrayList=new ArrayList<>();
    ArrayList<GroupMembersUtils> arrayList = new ArrayList<>();
    ArrayList<GroupMembersUtils> arrayList2 = new ArrayList<>();
    private ImageView addmember;
    LinearLayout search_ll;
    EditText search_edt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: Update with your API key
//        Mint.initAndStartSession(getActivity(), APIUtility.CRASH_REPORT_KEY);
        View v = inflater.inflate(R.layout.groups_activity, container, false);
        webServices = new WebServices();
        imageLoader = new Profile_ImageLoader(getActivity());
        mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        add_private_group = (TextView) v.findViewById(R.id.add_private_group);
        lvItems = (ListView) v.findViewById(R.id.lvItems);
        addmember = (ImageView) v.findViewById(R.id.addmember);
        search_ll = (LinearLayout) v.findViewById(R.id.search_ll);
        search_edt = (EditText) v.findViewById(R.id.search_edt);
        search_ll.setVisibility(View.VISIBLE);
        addmember.setVisibility(View.VISIBLE);
        adapter = new GroupAdapter();
        lvItems.setAdapter(adapter);
        addmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String allIds = "";
                for (int i = 0; i < WebServices.groupmembersUtilses.size(); i++) {
                    allIds = allIds + WebServices.groupmembersUtilses.get(i).getUser_id() + "|";
                }
                Intent intent = new Intent(getActivity(), AddMemberActivity.class);
                intent.putExtra("userid", allIds);
                startActivityForResult(intent, 9999);
            }
        });
        add_private_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddGroupActivity.class);
                intent.putExtra("courseid", getCourseID());
                startActivity(intent);
            }
        });
        search_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = search_edt.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });
//        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//
//                if (WebServices.isNetworkAvailable(getActivity())) {
//                    final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Please wait...");
//                    final Handler handler = new Handler() {
//                        @Override
//                        public void handleMessage(Message msg) {
//                            if (dialog != null) dialog.dismiss();
//                            Intent intent=new Intent(getActivity(),GroupsSelectionActivity.class);
//                            intent.putExtra("name",getCourse_name());
//                            intent.putExtra("courseid",getCourseID());
//                            intent.putExtra("groupid",arrayList.get(position).getGroup_id());
//                            startActivity(intent);
//                        }
//                    };
//                    new Thread() {
//                        @Override
//                        public void run() {
//                            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
//                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
//                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
//                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
//                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
//                            nameValuePair.add(new BasicNameValuePair("group_id", groupUtilsArrayList.get(position).getGroup_id()));
//                            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
//                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICE));
//                            nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
//                            nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
//                            String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
//                            Log.d(" Response:", resp.toString());
//                            webServices.getGroupchatMessages(resp);
//                            handler.sendEmptyMessage(0);
//                        }
//                    }.start();
//                }else{
//                    Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
//                }
//            }
//        });
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                swipeContainer.setRefreshing(false);
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(getActivity(),APIUtility.GETALLGROUPMEMBERS);
                } else
                    adapter.notifyDataSetChanged();
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUPMEMBERS);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)) handler.sendEmptyMessage(1);
                arrayList = webServices.getAllGroupMembers(resp);
                arrayList2.addAll(arrayList);
                handler.sendEmptyMessage(0);
            }
        }.start();
//         Setup refresh listener which triggers new data loading
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//                fetchTimelineAsync(0);
//            }
//        });
//        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);
        return v;
    }

    public void fetchTimelineAsync(int page) {

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                swipeContainer.setRefreshing(false);
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(getActivity(),APIUtility.GETALLGROUPMEMBERS);
                }
                adapter.notifyDataSetChanged();
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUPMEMBERS);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)) handler.sendEmptyMessage(1);
                arrayList = webServices.getAllGroupMembers(resp);
                arrayList2 = arrayList;
                handler.sendEmptyMessage(0);
            }
        }.start();

    }

    private class GroupAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.member_row, null);
                CircleImageView profile_img = (CircleImageView) convertView.findViewById(R.id.profile_image);
                TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                ImageView arrow_img = (ImageView) convertView.findViewById(R.id.arrow_img);
                TextView member_count_txt = (TextView) convertView.findViewById(R.id.member_count_txt);
                member_count_txt.setVisibility(View.GONE);
                arrow_img.setVisibility(View.GONE);
                member_name.setTextSize(16);
                profile_img.setImageResource(R.drawable.group);
                LinearLayout member_ll = (LinearLayout) convertView.findViewById(R.id.member_ll);
                TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt);
                member_ll.setVisibility(View.VISIBLE);
                profile_img.setVisibility(View.GONE);
                member_fname_txt.setText("");
                if (position % 4 == 1) {
                    member_ll.setBackgroundResource(R.drawable.test_series_bg);
                } else if (position % 4 == 2) {
                    member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                } else if (position % 4 == 0) {
                    member_ll.setBackgroundResource(R.drawable.light_blue_bg);
                } else if (position % 4 == 3) {
                    member_ll.setBackgroundResource(R.drawable.audio_bg);
                }
                String fname = arrayList.get(position).getFirstname().trim().toString().substring(0, 1);
                member_fname_txt.setText(fname);
                if (position % 4 == 0) {
                    member_name.setTextColor(Color.parseColor("#ed873e"));
//                    profile_img.setBorderColor(Color.parseColor("#ed873e"));
                } else if (position % 4 == 1) {
                    member_name.setTextColor(Color.parseColor("#6fb5ec"));
//                    profile_img.setBorderColor(Color.parseColor("#6fb5ec"));
                } else if (position % 4 == 2) {
                    member_name.setTextColor(Color.parseColor("#4dbfb0"));
//                    profile_img.setBorderColor(Color.parseColor("#4dbfb0"));
                } else if (position % 4 == 3) {
                    member_name.setTextColor(Color.parseColor("#bf564d"));
//                    profile_img.setBorderColor(Color.parseColor("#bf564d"));
                }
                imageLoader.DisplayImage(arrayList.get(position).getPhoto(), profile_img);
               /* if (!arrayList.get(position).getLastname().equalsIgnoreCase("null") && arrayList.get(position).getLastname() != null) {
                    if (arrayList.get(position).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id()))
                        member_name.setText(arrayList.get(position).getFirstname() + " " + arrayList.get(position).getLastname()+" (You)");
                    else
                        member_name.setText(arrayList.get(position).getFirstname() + " " + arrayList.get(position).getLastname());
                }else {*/
                if (arrayList.get(position).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id()))
                    member_name.setText(arrayList.get(position).getFirstname() + " (You)");
                else
                    member_name.setText(arrayList.get(position).getFirstname());
//                }
            }

            return convertView;
        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            arrayList.clear();
            if (charText.length() == 0) {
                arrayList.addAll(arrayList2);
            } else {
                for (GroupMembersUtils wp : arrayList2) {
                    String memberName = "";
                    /*if (!wp.getLastname().equalsIgnoreCase("null") && wp.getLastname() != null) {
                        memberName = wp.getFirstname() + " " + wp.getLastname();
                    } else {*/
                        memberName = wp.getFirstname();
//                    }
                    if (memberName.toLowerCase(Locale.getDefault()).contains(charText)) {
                        arrayList.add(wp);
                    }
                }
            }
            if (arrayList.size() == 0)
                Toast.makeText(getActivity(), "No result found", Toast.LENGTH_LONG).show();
            notifyDataSetChanged();
        }
    }

    public String getGroup_Id() {
        return Group_Id;
    }

    public void setGroup_Id(String group_Id) {
        Group_Id = group_Id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!data.getStringExtra("id").equalsIgnoreCase("")) {
            String[] userids = data.getStringExtra("id").toString().split("\\|");
            if (userids.length > 0)
                AddMember(userids);
        }
    }

    private void AddMember(final String[] userids) {
        if (WebServices.isNetworkAvailable(getActivity())) {
            final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Please wait...");
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (dialog != null) dialog.dismiss();
                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                        Utils.callInvalidSession(getActivity(),APIUtility.ADD_GROUP_MEMBER);
                    } else {
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), "New Member(s) added successfully!", Toast.LENGTH_LONG).show();
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {
                    for (int i = 0; i < userids.length; i++) {
                        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("user_id", userids[i]));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                        nameValuePair.add(new BasicNameValuePair("user_role", "USER"));
                        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("course_id", WebServices.mLoginUtility.getOrganization_id()));
                        String resp = webServices.callServices(nameValuePair, APIUtility.ADD_GROUP_MEMBER);
                        Log.d(" Response:", resp.toString());
                        if (!webServices.isValid(resp)) {
                            handler.sendEmptyMessage(1);
                            break;
                        }

                    }
                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                    nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                    String resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUPMEMBERS);
                    Log.d(" Response:", resp.toString());

                    webServices.getGroupMembers(resp);
                    handler.sendEmptyMessage(0);
                }
            }.start();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }
}
