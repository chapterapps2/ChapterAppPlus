package com.chaptervitamins.discussions;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.APIUtility;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.ContentValues.TAG;
//Created by Android on 7/6/2016.
public class ImageUploader {
    long imageSize = 0; // kb
    String imagePath = "";
    String UPLOAD_SERVER_URL = "";
    String imageName = "";
    Context mContext;
    private Dialog mDialog;
    ProgressDialog dialog;
    private ProgressBar mBar;
    private TextView totalPer_txt, title_txt, total_file_txt;
    private Button abort_btn = null;
    private boolean isUpload = true;
    String MSG = "Network issues.. You have been Timed-out";
    private String FILEPATH = null;
    String type;
    UploadListener listener;
    boolean isShowSize;
    public ImageUploader(Context mContext, String imagePath, String UPLOAD_SERVER_URL, String imageName, long imageSize, String type,boolean isShowSize, UploadListener listener) {
        this.imagePath = imagePath;
        this.UPLOAD_SERVER_URL = UPLOAD_SERVER_URL;
        this.imageName = imageName;
        this.mContext = mContext;
        this.imageSize = imageSize;
        this.type = type;
        this.listener = listener;
        this.isShowSize=isShowSize;
        UploadingImage();
//        uploadFile(imagePath, UPLOAD_SERVER_URL);
    }


    protected void UploadingImage() {
        isUpload = true;
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (!((Activity) mContext).isFinishing())
                    mDialog.dismiss();
                if (msg.what == 1) {
                    listener.error(MSG);
                } else {
                    listener.complete(msg.what,type,"");
//                    Toast.makeText(mContext, MSG, Toast.LENGTH_LONG).show();
                }
            }
        };
        mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        if (isShowSize)total_file_txt.setVisibility(View.GONE);
        abort_btn.setVisibility(View.GONE);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isUpload = false;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(1);
            }
        });


        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog.show();
            }
        });
        new Thread() {
            @Override
            public void run() {
                int id = 1;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(UPLOAD_SERVER_URL);
                System.out.println("====filename12==" + UPLOAD_SERVER_URL);
                System.out.println("=====file length===" + (imageSize / (1000f)));
                try {
                    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                            new AndroidMultiPartEntity.ProgressListener() {
                                public void transferred(final long num) {
                                    ((Activity) mContext).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            abort_btn.setEnabled(true);

                                            if (imageSize > 0) {
                                                mBar.setProgress((int) ((((num / 1024)) * 100) / imageSize));
                                                totalPer_txt.setText((int) ((((num / 1024)) * 100) / imageSize) + "%");
                                                if (!isShowSize) {
                                                    String total = ((imageSize / 1000f)) + "";
                                                    try {
                                                        String[] t = total.split("\\.");
                                                        if (t.length > 1)
                                                            total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                                        else total_file_txt.setText(t[0] + " MB");
                                                    } catch (Exception e) {
                                                        total_file_txt.setText(total + " MB");
                                                    }
                                                    int progress = (int) ((num / (float) imageSize) * 100);
                                                    totalPer_txt.setText(progress+" %");
                                                    mBar.setProgress(progress);
                                                } else {
                                                    total_file_txt.setText("0 MB");
                                                }
                                            }
                                        }
                                    });

                                }

                                public void onProgress(int progress) {
                                    // TODO Auto-generated method stub

                                }
                            });
                    File sourceFile = new File(imagePath);
                    // boolean b= sourceFile.renameTo(new File(toFullPath));
                    // if(b){
                    // System.out.println("===imageName1"+"got the sucess");
                    // }
                    System.out.println("=====filename==" + imagePath);
                    FileBody mFileBody = new FileBody(sourceFile);
                    if (!imageName.endsWith("."))
                        imageName=imageName+"jpg";
                    if (imageName.equalsIgnoreCase(""))
                        imageName="/profile.jpg";//+new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date())+".png";
                    imageName = imageName.substring(1);
                    System.out.println("=====filename1==" + imageName);
                    FormBodyPart mFormBodyPart = new FormBodyPart(
                            "file", mFileBody) {
                        @Override
                        protected void generateContentDisp(ContentBody body) {
                            StringBuilder buffer = new StringBuilder();
                            buffer.append("form-data; name=\"");
                            buffer.append("file");
                            buffer.append("\"");
                            buffer.append("; filename=\"" + imagePath
                                    + "\"");
                            addField(MIME.CONTENT_DISPOSITION,
                                    buffer.toString());
                        }
                    };
                    entity.addPart("file_name", new StringBody(imageName));
                    entity.addPart("type", new StringBody(type));
                    entity.addPart("api_key", new StringBody(APIUtility.APIKEY));
                    entity.addPart("device_type", new StringBody(APIUtility.DEVICE));
                    entity.addPart("session_token", new StringBody(APIUtility.SESSION));
                    entity.addPart("push_tokenn", new StringBody(APIUtility.DEVICE));
                    entity.addPart("device_uuid", new StringBody(APIUtility.DEVICEID));
                    entity.addPart("uploaded_by", new StringBody(WebServices.mLoginUtility.getUser_id()));
                    entity.addPart(mFormBodyPart);
//                    entity.addPart("file",new FileBody(sourceFile));

                    // entity.addPart("uploadedFile", new FileBody(sourceFile));
//            totalSize = entity.getContentLength();
                    httppost.setEntity(entity);

                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 200) {
                        // Server response
                        MSG = EntityUtils.toString(r_entity);
//                        MSG = MSG.substring(MSG.lastIndexOf(")"));
//                        MSG = MSG.substring(1);

                        try {
                            JSONObject object = new JSONObject(MSG);
                            id = object.getJSONObject("data").getInt("media_id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        MSG = "Error occurred!";
                    }
                    System.out.println("===resp===" + MSG.toString());
                } catch (ClientProtocolException e) {
//                MSG = e.toString();
                } catch (IOException e) {
//                MSG = e.toString();
                    e.printStackTrace();
                    System.out.println("===resp===" + e.toString());
                }
                handler.sendEmptyMessage(id);
            }
        }.start();

    }


    //android upload file to server
    public void uploadFile(final String selectedFilePath, final String SERVER_URL) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = ProgressDialog.show(mContext, "", "Uploading...Please wait!!");
            }
        });

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (dialog != null) dialog.dismiss();
                if (msg.what == 0) {

                } else if (msg.what == 1) {
                    Toast.makeText(mContext, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
                } else if (msg.what == 2) {
                    Toast.makeText(mContext, "URL error!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "File Not Found", Toast.LENGTH_SHORT).show();
                }
            }
        };
        new Thread() {
            @Override
            public void run() {


                int serverResponseCode = 0;

                HttpURLConnection connection;
                DataOutputStream dataOutputStream;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";


                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                File selectedFile = new File(selectedFilePath);


                String[] parts = selectedFilePath.split("/");
                final String fileName = parts[parts.length - 1];

                if (!selectedFile.isFile()) {
//                    dialog.dismiss();


                    handler.sendEmptyMessage(3);
                } else {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(selectedFile);
                        URL url = new URL(SERVER_URL);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);//Allow Inputs
                        connection.setDoOutput(true);//Allow Outputs
                        connection.setUseCaches(false);//Don't use a cached Copy
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("api_key", APIUtility.APIKEY);
                        connection.setRequestProperty("file_name", fileName);
                        connection.setRequestProperty("device_type", APIUtility.DEVICE);
                        connection.setRequestProperty("session_token", APIUtility.SESSION);
                        connection.setRequestProperty("push_tokenn", APIUtility.DEVICE);
                        connection.setRequestProperty("type", type);
                        connection.setRequestProperty("device_uuid", APIUtility.DEVICEID);
                        connection.setRequestProperty("uploaded_by", WebServices.mLoginUtility.getUser_id());
                        connection.setRequestProperty("xyz", "xyz");
                        connection.setRequestProperty("Connection", "Keep-Alive");
                        connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                        connection.setRequestProperty("file", selectedFilePath);

                        //creating new dataoutputstream
                        dataOutputStream = new DataOutputStream(connection.getOutputStream());

                        String key = "api_key";
                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        dataOutputStream.writeBytes("Content-Disposition: form-data;  name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(APIUtility.APIKEY);
                        dataOutputStream.writeBytes(lineEnd);
                        key = "";
                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        dataOutputStream.writeBytes("Content-Disposition: form-data;  name=\"api_key\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(APIUtility.APIKEY);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "file_name";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(fileName);
                        dataOutputStream.writeBytes(lineEnd);

                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "device_type";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(APIUtility.DEVICE);
                        dataOutputStream.writeBytes(lineEnd);

                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "session_token";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(APIUtility.SESSION);
                        dataOutputStream.writeBytes(lineEnd);

                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "uploaded_by";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(WebServices.mLoginUtility.getUser_id());
                        dataOutputStream.writeBytes(lineEnd);

                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "push_token";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(APIUtility.DEVICE);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "type";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(type);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "device_uuid";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(APIUtility.DEVICE);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        key = "xyz";
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes("xyz");
                        dataOutputStream.writeBytes(lineEnd);
                        //writing bytes to data outputstream
                        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
                                + selectedFilePath + "\"" + lineEnd);

                        dataOutputStream.writeBytes(lineEnd);
//

                        //returns no. of bytes present in fileInputStream
                        bytesAvailable = fileInputStream.available();
                        //selecting the buffer size as minimum of available bytes or 1 MB
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        //setting the buffer as byte array of size of bufferSize
                        buffer = new byte[bufferSize];

                        //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                        while (bytesRead > 0) {
                            //write the bytes read from inputstream
                            dataOutputStream.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }

                        dataOutputStream.writeBytes(lineEnd);
                        dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                        serverResponseCode = connection.getResponseCode();
                        String serverResponseMessage = connection.getResponseMessage();
                        BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
                        StringBuilder sb = new StringBuilder();
                        String output;
                        while ((output = br.readLine()) != null)
                            sb.append(output);
//                            return sb.toString();
                        Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode + "===" + sb.toString());

                        //response code of 200 indicates the server status OK
                        if (serverResponseCode == 200) {
                        }

                        //closing the input and output streams
                        fileInputStream.close();
                        dataOutputStream.flush();
                        dataOutputStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        handler.sendEmptyMessage(3);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        handler.sendEmptyMessage(2);


                    } catch (IOException e) {
                        e.printStackTrace();
                        handler.sendEmptyMessage(1);

                    }
                    handler.sendEmptyMessage(0);
                }
            }
        }.start();

    }
}
