package com.chaptervitamins.discussions;

/**
 * Created by Android on 7/8/2016.
 */

public interface UploadListener {
//    void transferred(long num);
    void error(String error);
    void complete(int media_id, String type,String response);

}

