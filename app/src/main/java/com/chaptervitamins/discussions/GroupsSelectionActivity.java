package com.chaptervitamins.discussions;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 7/4/2016.
 */

public class GroupsSelectionActivity extends BaseActivity {
    TextView course_name_txt;
    public static boolean isAdded = true;
    ImageView refresh;
    WebServices webServices;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.discussion_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbar_title=(TextView)findViewById(R.id.toolbar_title);
        toolbar_title.setText(getIntent().getStringExtra("name"));
        webServices=new WebServices();
        refresh = (ImageView) findViewById(R.id.refresh);
        ImageView back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        course_name_txt = (TextView) findViewById(R.id.course_name_txt);
        course_name_txt.setVisibility(View.GONE);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Chats"));
        tabLayout.addTab(tabLayout.newTab().setText("MEMBERS"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(Color.TRANSPARENT);
        tabLayout.setTabTextColors(Color.WHITE,Color.BLACK);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==1)refresh.setVisibility(View.GONE);
                else refresh.setVisibility(View.VISIBLE);
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTimelineAsync();
            }
        });
    }
    private void fetchTimelineAsync() {
        if (WebServices.isNetworkAvailable(GroupsSelectionActivity.this)) {
            final ProgressDialog dialog = ProgressDialog.show(GroupsSelectionActivity.this, "", "Refreshing...");
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (dialog != null) dialog.dismiss();
                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                        Utils.callInvalidSession(GroupsSelectionActivity.this,APIUtility.GETPOSTS);
                    }else
                    new ChatFragment().clickOnRefresh();
                }
            };
            new Thread() {
                @Override
                public void run() {
                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("group_id", getIntent().getStringExtra("groupid")));
                    nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                    String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
                    if (!webServices.isValid(resp))handler.sendEmptyMessage(1);
                    webServices.getGroupchatMessages(resp);
                    handler.sendEmptyMessage(0);
                }
            }.start();
        } else {
            Toast.makeText(GroupsSelectionActivity.this, "Unable to reach server! Please check your internet connection.", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            this.finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    ChatFragment tab1 = new ChatFragment();
                    tab1.setGroup_Id(getIntent().getStringExtra("groupid"));
                    tab1.setUserid(getIntent().getStringExtra("userid"));
                    return tab1;
                case 1:
                    GroupMembersFragment tab2 = new GroupMembersFragment();
                    tab2.setGroup_Id(getIntent().getStringExtra("groupid"));
                    return tab2;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}
