package com.chaptervitamins.discussions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.GroupMembersUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Android on 7/4/2016.
 */

public class AddGroupActivity extends BaseActivity {
    private TextView done_txt;
    private EditText group_title_text, group_desc_text, add_member_text;
    private Button addmember_btn;
    private String GroupID = "", Courseid = "", membersIds = "";
    private WebServices webServices;
    private DataBase dataBase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_group_activity);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dataBase = DataBase.getInstance(AddGroupActivity.this);
        group_title_text = (EditText) findViewById(R.id.group_title_text);
        group_desc_text = (EditText) findViewById(R.id.group_desc_text);
        add_member_text = (EditText) findViewById(R.id.add_member_text);
        addmember_btn = (Button) findViewById(R.id.addmember_btn);
//        changeStatusBarColor();
        done_txt = (TextView) findViewById(R.id.done);
        webServices = new WebServices();
        done_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (group_title_text.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(AddGroupActivity.this, "Please Enter Group Title", Toast.LENGTH_LONG).show();
                    return;
                }
                if (WebServices.isNetworkAvailable(AddGroupActivity.this)) {
                    createGroup();
                } else {
                    Toast.makeText(AddGroupActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
            }
        });
        addmember_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String allIds = "";
                for (int i = 0; i < WebServices.groupmembersUtilses.size(); i++) {
                    allIds = allIds + WebServices.groupmembersUtilses.get(i).getUser_id() + "|";
                }

                Intent intent = new Intent(AddGroupActivity.this, MemberListActivity.class);
                intent.putExtra("userid", allIds);
                startActivityForResult(intent, 200);
            }
        });
    }

    private void createGroup() {
        final ProgressDialog dialog = ProgressDialog.show(AddGroupActivity.this, "", "Please wait...");
        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("group_name", group_title_text.getText().toString().trim()));
        nameValuePair.add(new BasicNameValuePair("group_type", "SUPER"));
        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("course_id", WebServices.mLoginUtility.getOrganization_id()));
        new GenericApiCall(AddGroupActivity.this, APIUtility.CREATE_GROUP, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                String resp = (String) result;
                if (webServices.isValid(resp)) {
                    try {
                        JSONObject jsonObject = new JSONObject(resp);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        if (jsonArray != null) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                GroupMembersUtils utils = new GroupMembersUtils();
                                JSONObject object = jsonArray.getJSONObject(i);
                                GroupID = (object.optString("group_id", ""));
                                Courseid = (object.optString("course_id", ""));
                                WebServices.groupUtilsArrayList.addAll(webServices.getGroup(resp));
                            }
                        }

                        if (!TextUtils.isEmpty(membersIds)) {
                            ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                            nameValuePair.add(new BasicNameValuePair("member_id", membersIds));
                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                            nameValuePair.add(new BasicNameValuePair("group_id", GroupID));
                            nameValuePair.add(new BasicNameValuePair("user_role", "USER"));
                            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                            nameValuePair.add(new BasicNameValuePair("course_id", Courseid));
                            new GenericApiCall(AddGroupActivity.this, APIUtility.ADD_GROUP_MEMBER, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                                @Override
                                public void onSuccess(Object result) {

                                }

                                @Override
                                public void onError(ErrorModel error) {
                                    Toast.makeText(AddGroupActivity.this, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
                                }
                            }).execute();
                        }

                        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));

                        new GenericApiCall(AddGroupActivity.this, APIUtility.GETALLGROUP, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                            @Override
                            public void onSuccess(Object result) {
                                String resp = (String) result;
                                if (webServices.isValid(resp)) {
                                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "All_Groups"))
                                        dataBase.addallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                                    else {
                                        dataBase.updateallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                                    }
                                    webServices.getAllGroup(resp);
                                }
                                if (dialog != null)
                                    dialog.dismiss();
                                setResult(RESULT_OK);
                                finish();
                            }

                            @Override
                            public void onError(ErrorModel error) {
                                if (dialog != null)
                                    dialog.dismiss();
                                Toast.makeText(AddGroupActivity.this, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
                            }
                        }).execute();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(ErrorModel error) {
                if (dialog != null)
                    dialog.dismiss();
                Toast.makeText(AddGroupActivity.this, error.getErrorDescription(), Toast.LENGTH_SHORT).show();

            }
        }).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String id = "";
        if (requestCode == 200) {
            for (int i = 0; i < WebServices.membersUtilses.size(); i++) {
                if (WebServices.membersUtilses.get(i).isSelected()) {
//                    if (!WebServices.membersUtilses.get(i).getLastname().equalsIgnoreCase("null") && WebServices.membersUtilses.get(i).getLastname() != null)
//                        id = id + (WebServices.membersUtilses.get(i).getFirstname() + " " + WebServices.membersUtilses.get(i).getLastname()) + ",";
//                    else

                    if (i == WebServices.membersUtilses.size() - 1) {
                        membersIds = membersIds + membersIds;
                        id = id + (WebServices.membersUtilses.get(i).getFirstname());
                    } else {
                        membersIds = membersIds + membersIds + ",";
                        id = id + (WebServices.membersUtilses.get(i).getFirstname()) + ",";
//                    UserID = WebServices.membersUtilses.get(i).getUser_id();
                    }
                }
            }
            if (!"".equalsIgnoreCase(id))
                id = id.substring(0, (id.length() - 1));
            add_member_text.setText(id);
        }
    }
}
