package com.chaptervitamins.discussions;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.downloadImages.ImageLoader;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.Chat_MessagesUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 7/12/2016.
 */

public class PrivateChatActivity extends AppCompatActivity implements UploadListener{
//    private SwipeRefreshLayout swipeContainer;
    private WebServices webServices;
    private LayoutInflater mInflater;
    private ImageLoader imageLoader;
    private ListView chat_list;
    private String Group_Id = "";
    private PrivateChatAdapter chatAdapter;
    private final int REQUEST_CHOOSER = 2000;
    private ImageView attached_img, send_img;
    private EditText editText;
    private LinearLayout chat_ll,chat_blank_ll;
    ArrayList<Chat_MessagesUtils> tempchat_messagesUtilses = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chat_activity);
        webServices = new WebServices();
        imageLoader = new ImageLoader(PrivateChatActivity.this);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        chat_list = (ListView) findViewById(R.id.chat_list);
        chat_ll = (LinearLayout) findViewById(R.id.chat_ll);
        chat_blank_ll = (LinearLayout) findViewById(R.id.chat_blank_ll);
        chat_blank_ll.setVisibility(View.GONE);
        chatAdapter = new PrivateChatAdapter();
        chat_list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chat_list.setAdapter(chatAdapter);
        if (GroupsSelectionActivity.isAdded) {
            String sectiondate = "";
            if (WebServices.group_chat_messagesUtilses != null) {
                for (int i = (WebServices.group_chat_messagesUtilses.size()-1); i >=0; i--) {
                    if (!WebServices.group_chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.group_chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.group_chat_messagesUtilses.get(i));
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
            }
            Discussion_Activity.isAdded = false;
        }
        chat_list.setSelection(chatAdapter.getCount() - 1);

        chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                chat_list.setSelection(chatAdapter.getCount() - 1);
            }
        });
        attached_img = (ImageView) findViewById(R.id.attached_img);
        send_img = (ImageView) findViewById(R.id.send_img);
        editText = (EditText) findViewById(R.id.editText);
        editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        attached_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PrivateChatActivity.this, UploadFilesActivity.class);
                startActivityForResult(intent, REQUEST_CHOOSER);
//                Intent getContentIntent = FileUtils.createGetContentIntent();
//
//                Intent intent = Intent.createChooser(getContentIntent, "Select a file");
//                startActivityForResult(intent, REQUEST_CHOOSER);
            }
        });
        send_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editText.getText().toString().trim().equalsIgnoreCase("")) return;
                final InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(send_img.getWindowToken(), 0);
                if (WebServices.isNetworkAvailable(PrivateChatActivity.this))
                    uploadmsg(editText.getText().toString().trim(), "POST");
                else
                    Toast.makeText(PrivateChatActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();

            }
        });
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//                fetchTimelineAsync(0);
//            }
//        });
//        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);

    }
    protected void showInputDialog(String msg,final String type, final int mediaid) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(PrivateChatActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.input_dialog);
        // Set dialog title

        final EditText editText = (EditText) dialog.findViewById(R.id.edit_text);
        editText.setHint("Enter description of uploaded file...");
        // setup a dialog window
        if (!msg.equalsIgnoreCase(""))
            editText.setText(msg);
        Button button = (Button) dialog.findViewById(R.id.ok_button);
        dialog.setCancelable(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadTypedmsg(editText.getText().toString(),type,mediaid+"");
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void uploadTypedmsg(final String massage, final String type,final String media_id) {
        chat_ll.setVisibility(View.GONE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                chatAdapter.notifyDataSetChanged();
                editText.setText("");
                chat_ll.setVisibility(View.VISIBLE);
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", massage));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("media_id", media_id));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                Log.d(" Response:", resp.toString());
                webServices.getchatMessage(resp);

                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                tempchat_messagesUtilses = WebServices.group_chat_messagesUtilses;
                for (int i = 0; i < WebServices.group_chat_messagesUtilses.size(); i++) {
                    if (WebServices.group_chat_messagesUtilses.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                for (int i = (WebServices.group_chat_messagesUtilses.size()-1); i >=0; i--) {
                    if (!WebServices.group_chat_messagesUtilses.get(i).isSection()) {
                        if (!WebServices.group_chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                            Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                            utils1.setDate(WebServices.group_chat_messagesUtilses.get(i).getDate());
                            utils1.setSection(true);
                            tempchat_messagesUtilses.add(utils1);
                            sectiondate = utils1.getDate();
                        }
                        tempchat_messagesUtilses.add(WebServices.group_chat_messagesUtilses.get(i));
                    }
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses=new ArrayList<Chat_MessagesUtils>();
                for (int i = (WebServices.group_chat_messagesUtilses.size()-1); i >=0; i--) {
                    if (!WebServices.group_chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.group_chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.group_chat_messagesUtilses.get(i));
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                handler.sendEmptyMessage(0);
            }
        }.start();
    }
    private void fetchTimelineAsync(int i) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                swipeContainer.setRefreshing(false);
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(PrivateChatActivity.this,APIUtility.GETPOSTS);
                }
                chatAdapter.notifyDataSetChanged();
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp))handler.sendEmptyMessage(1);
                webServices.getchatMessages(resp);
                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                tempchat_messagesUtilses = WebServices.group_chat_messagesUtilses;
                for (int i = 0; i < WebServices.group_chat_messagesUtilses.size(); i++) {
                    if (WebServices.group_chat_messagesUtilses.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                for (int i = (WebServices.group_chat_messagesUtilses.size()-1); i >=0; i--) {
                    if (!WebServices.group_chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.group_chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.group_chat_messagesUtilses.get(i));
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                handler.sendEmptyMessage(0);
            }
        }.start();
    }
    @Override
    public void error(String error) {
        System.out.println("==================image Uploaded Error==============="+error);
    }

    @Override
    public void complete(int media_id,String type,String res) {
        showInputDialog("",type,media_id);
        System.out.println("==================image Uploaded==============="+media_id);
    }
    private void uploadmsg(final String massage, final String type) {
        chat_ll.setVisibility(View.GONE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(PrivateChatActivity.this,APIUtility.SUBMIT_FORUM);
                }
                chatAdapter.notifyDataSetChanged();
                editText.setText("");
                chat_ll.setVisibility(View.VISIBLE);
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", ""));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp))handler.sendEmptyMessage(1);
                webServices.getchatMessage(resp);

                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                tempchat_messagesUtilses = WebServices.group_chat_messagesUtilses;
                for (int i = 0; i < WebServices.group_chat_messagesUtilses.size(); i++) {
                    if (WebServices.group_chat_messagesUtilses.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                for (int i = (WebServices.group_chat_messagesUtilses.size()-1); i >=0; i--) {
                    if (!WebServices.group_chat_messagesUtilses.get(i).isSection()) {
                        if (!WebServices.group_chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                            Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                            utils1.setDate(WebServices.group_chat_messagesUtilses.get(i).getDate());
                            utils1.setSection(true);
                            tempchat_messagesUtilses.add(utils1);
                            sectiondate = utils1.getDate();
                        }
                        tempchat_messagesUtilses.add(WebServices.group_chat_messagesUtilses.get(i));
                    }
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses=new ArrayList<Chat_MessagesUtils>();
                for (int i = (WebServices.group_chat_messagesUtilses.size()-1); i >=0; i--) {
                    if (!WebServices.group_chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.group_chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.group_chat_messagesUtilses.get(i));
                }
                WebServices.group_chat_messagesUtilses = tempchat_messagesUtilses;
                handler.sendEmptyMessage(0);
            }
        }.start();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHOOSER:
                if (resultCode == REQUEST_CHOOSER) {
                    String imagePath, imageName;
                    long imageSize;
// GET IMAGE PATH
                    imagePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    imageName = imagePath.substring(imagePath.lastIndexOf("/"));

                    imageSize = this.getFileSize(imagePath);

//                    final Uri uri = data.getData();
//
                    System.out.println("==imagePath==" + imagePath);
                    System.out.println("==imageName==" + imageName);
                    System.out.println("==imageSize==" + imageSize);
                    new ImageUploader(PrivateChatActivity.this, imagePath, APIUtility.UPLOAD_FILE, imageName, imageSize,"IMAGE",false,this);
                } else if (resultCode == 30000) {
                    String filePath, fileName;
                    long fileSize;
// GET IMAGE PATH
                    filePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    fileName = filePath.substring(filePath.lastIndexOf("/"));

                    fileSize = this.getFileSize(filePath);
                    System.out.println("==filePath==" + filePath);
                    System.out.println("==fileName==" + fileName);
                    System.out.println("==fileSize==" + fileSize);
                    new ImageUploader(PrivateChatActivity.this, filePath, APIUtility.UPLOAD_FILE, fileName, fileSize,"PDF",false,this);
                }
                break;
        }
    }

    /**
     * Get the image path
     *
     * @param uri
     * @return
     */
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * Get the file size in kilobytes
     *
     * @return
     */
    private long getFileSize(String imagePath) {

        long length = 0;

        try {

            File file = new File(imagePath);
            length = file.length();
            length = length / 1024;

        } catch (Exception e) {

            e.printStackTrace();
        }
        return length;
    }

    private class PrivateChatAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return WebServices.group_chat_messagesUtilses.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {
                if (!WebServices.group_chat_messagesUtilses.get(position).isSection()) {
                    convertView = mInflater.inflate(R.layout.chat_row, null);
                    LinearLayout left_ll=(LinearLayout)convertView.findViewById(R.id.left_ll);
                    LinearLayout right_ll=(LinearLayout)convertView.findViewById(R.id.right_ll);
                    ImageView profile_img = (ImageView) convertView.findViewById(R.id.profile_image);
                    TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                    TextView msg_txt = (TextView) convertView.findViewById(R.id.msg_txt);
                    TextView time_txt = (TextView) convertView.findViewById(R.id.time_txt);
                    ImageView profile_img2 = (ImageView) convertView.findViewById(R.id.profile_image2);
                    TextView member_name2 = (TextView) convertView.findViewById(R.id.member_name_txt2);
                    TextView msg_txt2 = (TextView) convertView.findViewById(R.id.msg_txt2);
                    TextView time_txt2 = (TextView) convertView.findViewById(R.id.time_txt2);

                    if (WebServices.group_chat_messagesUtilses.get(position).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id())){
                        left_ll.setVisibility(View.GONE);
                        right_ll.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(WebServices.group_chat_messagesUtilses.get(position).getPhoto(), profile_img2);
                        if (!WebServices.group_chat_messagesUtilses.get(position).getLastname().equalsIgnoreCase("null") && WebServices.group_chat_messagesUtilses.get(position).getLastname() != null)
                            member_name2.setText(WebServices.group_chat_messagesUtilses.get(position).getFirstname() + " " + WebServices.group_chat_messagesUtilses.get(position).getLastname());
                        else
                            member_name2.setText(WebServices.group_chat_messagesUtilses.get(position).getFirstname());
                        msg_txt2.setText(Html.fromHtml(WebServices.group_chat_messagesUtilses.get(position).getForum_description()));
                        time_txt2.setText(WebServices.group_chat_messagesUtilses.get(position).getAdded_on());
                    }else {
                        right_ll.setVisibility(View.GONE);
                        left_ll.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(WebServices.group_chat_messagesUtilses.get(position).getPhoto(), profile_img);
                        if (!WebServices.group_chat_messagesUtilses.get(position).getLastname().equalsIgnoreCase("null") && WebServices.group_chat_messagesUtilses.get(position).getLastname() != null)
                            member_name.setText(WebServices.group_chat_messagesUtilses.get(position).getFirstname() + " " + WebServices.group_chat_messagesUtilses.get(position).getLastname());
                        else
                            member_name.setText(WebServices.group_chat_messagesUtilses.get(position).getFirstname());
                        msg_txt.setText(Html.fromHtml(WebServices.group_chat_messagesUtilses.get(position).getForum_description()));
                        time_txt.setText(WebServices.group_chat_messagesUtilses.get(position).getAdded_on());
                    }
                } else {
                    convertView = mInflater.inflate(R.layout.date_section_row, null);
                    TextView date_txt = (TextView) convertView.findViewById(R.id.date_txt);
                    date_txt.setText(WebServices.group_chat_messagesUtilses.get(position).getDate());
                }
            }
            return convertView;
        }
    }

    public String getGroup_Id() {
        return Group_Id;
    }

    public void setGroup_Id(String group_Id) {
        Group_Id = group_Id;
    }
}