package com.chaptervitamins.discussions;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.OnRefresh;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.downloadImages.Profile_ImageLoader;
import com.chaptervitamins.home.NewsFeedDetail_Activity;
import com.chaptervitamins.home.ShowImageActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.play_video.PDFViewerActivity;
import com.chaptervitamins.utility.Chat_MessagesUtils;
import com.chaptervitamins.utility.CommentsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 7/1/2016.
 */

public class TopicChatFragment extends Fragment implements UploadListener, OnRefresh {
//    private SwipeRefreshLayout swipeContainer;
    private WebServices webServices;
    private LayoutInflater mInflater;
    private Profile_ImageLoader imageLoader;
    private ListView chat_list;
    private ChatAdapter chatAdapter;
    private String Group_Id = "";
    private String COURSE_NAME = "";
    private final int REQUEST_CHOOSER = 2000;
    private ImageView attached_img, send_img;
    private EditText editText;

    private LinearLayout chat_ll,chat_blank_ll;
    ArrayList<Chat_MessagesUtils> tempchat_messagesUtilses = new ArrayList<>();
    public static ArrayList<CommentsUtils> commentsUtilsArrayList = new ArrayList<>();
    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: Update with your API key
        View v = inflater.inflate(R.layout.chat_activity, container, false);
        webServices = new WebServices();
        imageLoader = new Profile_ImageLoader(getActivity());
        mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        swipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        chat_list = (ListView) v.findViewById(R.id.chat_list);
        chat_ll = (LinearLayout) v.findViewById(R.id.chat_ll);
        chat_blank_ll = (LinearLayout) v.findViewById(R.id.chat_blank_ll);
        chat_blank_ll.setVisibility(View.GONE);
        chat_list.setPadding(0,30,0,0);
        chatAdapter = new ChatAdapter();
        chat_list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chat_list.setAdapter(chatAdapter);
        if (Discussion_Activity.isAdded) {
            String sectiondate = "";
            if (WebServices.chat_messagesUtilses != null) {
                for (int i = (WebServices.chat_messagesUtilses.size() - 1); i >= 0; i--) {
                    if (!WebServices.chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.chat_messagesUtilses.get(i));
                }
                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
            }
            Discussion_Activity.isAdded = false;
        }
        chat_list.setSelection(chatAdapter.getCount() - 1);

        chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                chat_list.setSelection(chatAdapter.getCount() - 1);
            }
        });
        attached_img = (ImageView) v.findViewById(R.id.attached_img);
        send_img = (ImageView) v.findViewById(R.id.send_img);
        editText = (EditText) v.findViewById(R.id.editText);
        editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editText.post(new Runnable() {
            @Override
            public void run() {
                editText.setFocusable(true);
                editText.setSelection(editText.getText().toString().length());
            }
        });
        attached_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UploadFilesActivity.class);
                startActivityForResult(intent, REQUEST_CHOOSER);
//                Intent getContentIntent = FileUtils.createGetContentIntent();
//
//                Intent intent = Intent.createChooser(getContentIntent, "Select a file");
//                startActivityForResult(intent, REQUEST_CHOOSER);
            }
        });
        send_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editText.getText().toString().trim().equalsIgnoreCase("")) return;
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(send_img.getWindowToken(), 0);
                if (WebServices.isNetworkAvailable(getActivity()))
                    uploadmsg(editText.getText().toString().trim(), "POST");
                else
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();

            }
        });


        return v;
    }

    private void deleteTopic(final String comment_id, final int pos) {
        if (WebServices.isNetworkAvailable(getActivity())) {
            final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Deleting...");
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (dialog != null) dialog.dismiss();
                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                        Utils.callInvalidSession(getActivity(),APIUtility.DELETE_COMMENT);
                    }else {
                        if (chatAdapter != null) {
                            chatAdapter.notifyDataSetChanged();

                        }
                        Toast.makeText(getActivity(), "Comment is deleted successfully", Toast.LENGTH_LONG).show();
                    }
                }
            };
            new Thread() {
                @Override
                public void run() {
                    List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("comment_id", comment_id));
                    nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                    String resp = webServices.callServices(nameValuePair, APIUtility.DELETE_COMMENT);
                    Log.d(" Response:", resp.toString());
                    if (!webServices.isValid(resp)){
                        handler.sendEmptyMessage(1);
                        return;
                    }
                    WebServices.chat_messagesUtilses.remove(pos);
                    handler.sendEmptyMessage(0);
                }
            }.start();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void clickOnRefresh() {
        if (chatAdapter == null) chatAdapter = new ChatAdapter();
        String sectiondate = "";
        tempchat_messagesUtilses=new ArrayList<>();
        if (WebServices.chat_messagesUtilses != null) {
            for (int i = (WebServices.chat_messagesUtilses.size() - 1); i >= 0; i--) {
                if (!WebServices.chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                    Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                    utils1.setDate(WebServices.chat_messagesUtilses.get(i).getDate());
                    utils1.setSection(true);
                    tempchat_messagesUtilses.add(utils1);
                    sectiondate = utils1.getDate();
                }
                tempchat_messagesUtilses.add(WebServices.chat_messagesUtilses.get(i));
            }
            WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
        }
        chatAdapter.notifyDataSetChanged();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
    protected void showInputDialog(String msg,final String type, final int mediaid) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.input_dialog);
        // Set dialog title

        final EditText editText = (EditText) dialog.findViewById(R.id.edit_text);
        editText.setHint("Enter description of uploaded file...");
        // setup a dialog window
        if (!msg.equalsIgnoreCase(""))
            editText.setText(msg);
        Button button = (Button) dialog.findViewById(R.id.ok_button);
        dialog.setCancelable(false);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadTypedmsg(editText.getText().toString(),type,mediaid+"");
                dialog.dismiss();
            }
        });
        dialog.show();
    }
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        if (!Discussion_Activity.isAdded)
//            fetchTimelineAsync(0);
//    }

    private void fetchTimelineAsync(int i) {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(getActivity(),APIUtility.GETPOSTS);
                }else
                if (chatAdapter != null) {
//                    swipeContainer.setRefreshing(false);
                    chatAdapter.notifyDataSetChanged();
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("no_of_post", "10"));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.GETPOSTS);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)){
                    handler.sendEmptyMessage(1);
                    return;
                }
                webServices.getchatMessages(resp);
                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                tempchat_messagesUtilses = WebServices.chat_messagesUtilses;
                for (int i = 0; i < WebServices.chat_messagesUtilses.size(); i++) {
                    if (WebServices.chat_messagesUtilses.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }
                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                for (int i = (WebServices.chat_messagesUtilses.size() - 1); i >= 0; i--) {
                    if (!WebServices.chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.chat_messagesUtilses.get(i));
                }
                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    private void uploadmsg(final String massage, final String type) {
        chat_ll.setVisibility(View.GONE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(getActivity(),APIUtility.SUBMIT_FORUM);
                }else {
                    chatAdapter.notifyDataSetChanged();
                    editText.setText("");
                    chat_ll.setVisibility(View.VISIBLE);
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", ""));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)){
                    handler.sendEmptyMessage(1);
                    return;
                }
                webServices.getchatMessage(resp);
                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                tempchat_messagesUtilses = WebServices.chat_messagesUtilses;
                for (int i = 0; i < WebServices.chat_messagesUtilses.size(); i++) {
                    if (WebServices.chat_messagesUtilses.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }
                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                for (int i = 0; i<WebServices.chat_messagesUtilses.size(); i++) {
                    if (!WebServices.chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.chat_messagesUtilses.get(i));
                }
                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
                handler.sendEmptyMessage(0);
            }
        }.start();
    }
    private void uploadTypedmsg(final String massage, final String type,final String media_id) {
        chat_ll.setVisibility(View.GONE);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")){
                    Utils.callInvalidSession(getActivity(),APIUtility.SUBMIT_FORUM);
                }else {
                    chatAdapter.notifyDataSetChanged();
                    editText.setText("");
                    chat_ll.setVisibility(View.VISIBLE);
                }
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("group_id", getGroup_Id()));
                nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("forum_description", massage));
                nameValuePair.add(new BasicNameValuePair("forum_title", massage));
                nameValuePair.add(new BasicNameValuePair("forum_type", type));
                nameValuePair.add(new BasicNameValuePair("media_id", media_id));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                String resp = webServices.callServices(nameValuePair, APIUtility.SUBMIT_FORUM);
                Log.d(" Response:", resp.toString());
                if (!webServices.isValid(resp)){
                    handler.sendEmptyMessage(1);
                    return;
                }
                webServices.getchatMessage(resp);
                String sectiondate = "";
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                tempchat_messagesUtilses = WebServices.chat_messagesUtilses;
                for (int i = 0; i < WebServices.chat_messagesUtilses.size(); i++) {
                    if (WebServices.chat_messagesUtilses.get(i).isSection())
                        tempchat_messagesUtilses.remove(i);
                }
                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
                tempchat_messagesUtilses = new ArrayList<Chat_MessagesUtils>();
                for (int i = 0; i<WebServices.chat_messagesUtilses.size(); i++) {
                    if (!WebServices.chat_messagesUtilses.get(i).getDate().equalsIgnoreCase(sectiondate)) {
                        Chat_MessagesUtils utils1 = new Chat_MessagesUtils();
                        utils1.setDate(WebServices.chat_messagesUtilses.get(i).getDate());
                        utils1.setSection(true);
                        tempchat_messagesUtilses.add(utils1);
                        sectiondate = utils1.getDate();
                    }
                    tempchat_messagesUtilses.add(WebServices.chat_messagesUtilses.get(i));
                }
                WebServices.chat_messagesUtilses = tempchat_messagesUtilses;
                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHOOSER:
                if (resultCode == REQUEST_CHOOSER) {
                    String imagePath, imageName;
                    long imageSize;
// GET IMAGE PATH
                    imagePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    imageName = imagePath.substring(imagePath.lastIndexOf("/"));

                    imageSize = this.getFileSize(imagePath);

//                    final Uri uri = data.getData();
//
                    System.out.println("==imagePath==" + imagePath);
                    System.out.println("==imageName==" + imageName);
                    System.out.println("==imageSize==" + imageSize);
//                    this.progressDialog = this.createDialog();
//                    this.progressDialog.show();
                    new ImageUploader(getActivity(), imagePath, APIUtility.UPLOAD_FILE, imageName, imageSize, "IMAGE",false,this);
                } else if (resultCode == 3000) {
                    String filePath, fileName;
                    long fileSize;
// GET IMAGE PATH
                    filePath = data.getStringExtra("filename");


                    // IMAGE NAME
                    fileName = filePath.substring(filePath.lastIndexOf("/"));

                    fileSize = this.getFileSize(filePath);
                    System.out.println("==filePath==" + filePath);
                    System.out.println("==fileName==" + fileName);
                    System.out.println("==fileSize==" + fileSize);
//                    this.progressDialog = this.createDialog();
//                    this.progressDialog.show();
                    new ImageUploader(getActivity(), filePath, APIUtility.UPLOAD_FILE, fileName, fileSize, "PDF",false,this);
                }
                break;
        }
    }

    /**
     * Get the image path
     *
     * @param uri
     * @return
     */
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * Get the file size in kilobytes
     *
     * @return
     */
    private long getFileSize(String imagePath) {

        long length = 0;

        try {

            File file = new File(imagePath);
            length = file.length();
            length = length / 1024;

        } catch (Exception e) {

            e.printStackTrace();
        }
        return length;
    }

    @Override
    public void error(String error) {
        System.out.println("==================image Uploaded Error==============="+error);
    }

    @Override
    public void complete(int media_id,String type,String resp) {
        if (media_id==0){

            Intent intent = new Intent(getActivity(), PDFViewerActivity.class);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(type));
            intent.putExtra("filename", type);
            //if document protected with password
            intent.putExtra("password", "encrypted PDF password");

            //if you need highlight link boxes
            intent.putExtra("linkhighlight", true);
//                    intent.putExtra("filename", filename);

            //if you don't need device sleep on reading document
            intent.putExtra("idleenabled", false);

            //set true value for horizontal page scrolling, false value for vertical page scrolling
            intent.putExtra("horizontalscrolling", true);
            type=type.substring(type.lastIndexOf("/")).split("_")[1];
            //document name
            intent.putExtra("docname", type);

            startActivity(intent);
        }else
        showInputDialog("",type,media_id);
System.out.println("==================image Uploaded==============="+media_id);

    }

    private class ChatAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return WebServices.chat_messagesUtilses.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = null;
            if (convertView == null) {

                if (!WebServices.chat_messagesUtilses.get(position).isSection()) {
                    convertView = mInflater.inflate(R.layout.chat_row, null);
                    LinearLayout left_new_ll = (LinearLayout) convertView.findViewById(R.id.left_new_ll);
                    ImageView left_new_img = (ImageView) convertView.findViewById(R.id.left_new_img);
                    TextView left_new_name_txt = (TextView) convertView.findViewById(R.id.left_new_name_txt);
                    TextView left_new_msg_txt = (TextView) convertView.findViewById(R.id.left_new_msg_txt);
                    TextView left_new_time_txt = (TextView) convertView.findViewById(R.id.left_new_time_txt);
                    LinearLayout row_ll = (LinearLayout) convertView.findViewById(R.id.row_ll);
                    LinearLayout right_new_ll = (LinearLayout) convertView.findViewById(R.id.right_new_ll);
                    ImageView right_new_img = (ImageView) convertView.findViewById(R.id.right_new_img);
                    TextView right_new_name_txt = (TextView) convertView.findViewById(R.id.right_new_name_txt);
                    TextView right_new_msg_txt = (TextView) convertView.findViewById(R.id.right_new_msg_txt);
                    TextView right_new_time_txt = (TextView) convertView.findViewById(R.id.right_new_time_txt);
                    LinearLayout left_attached_file_ll2 = (LinearLayout) convertView.findViewById(R.id.left_attached_file_ll2);
                    LinearLayout right_attached_file_ll2 = (LinearLayout) convertView.findViewById(R.id.right_attached_file_ll2);
                    TextView left_attach_filename_txt = (TextView) convertView.findViewById(R.id.left_attach_filename_txt);
                    TextView right_attach_filename_txt = (TextView) convertView.findViewById(R.id.right_attach_filename_txt);
                    LinearLayout left_ll = (LinearLayout) convertView.findViewById(R.id.left_ll);
                    left_ll.setVisibility(View.GONE);
                    LinearLayout right_ll = (LinearLayout) convertView.findViewById(R.id.right_ll);
                    right_ll.setVisibility(View.GONE);
                    ImageView profile_img = (ImageView) convertView.findViewById(R.id.profile_image);
                    TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
                    TextView msg_txt = (TextView) convertView.findViewById(R.id.msg_txt);
                    TextView time_txt = (TextView) convertView.findViewById(R.id.time_txt);
                    ImageView profile_img2 = (ImageView) convertView.findViewById(R.id.profile_image2);
                    TextView member_name2 = (TextView) convertView.findViewById(R.id.member_name_txt2);
                    TextView msg_txt2 = (TextView) convertView.findViewById(R.id.msg_txt2);
                    TextView time_txt2 = (TextView) convertView.findViewById(R.id.time_txt2);
                    row_ll.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            if (WebServices.chat_messagesUtilses.get(position).isSection()) return false;
                            if (WebServices.chat_messagesUtilses.get(position).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id())) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Delete")
                                        .setMessage("Are you sure want to Delete?")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                deleteTopic(WebServices.chat_messagesUtilses.get(position).getForum_id(), position );
                                                dialog.dismiss();
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })

                                        .show();
                            }
                            return false;
                        }
                    });
                    row_ll.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!WebServices.chat_messagesUtilses.get(position).isSection()) {

                                if (WebServices.chat_messagesUtilses.get(position).getMedia().contains(".png") || WebServices.chat_messagesUtilses.get(position).getMedia().contains(".jpg")) {
                                    Intent intent = new Intent(getActivity(), ShowImageActivity.class);
                                    intent.putExtra("imgurl", WebServices.chat_messagesUtilses.get(position).getMedia());
                                    intent.putExtra("imguri", "");
                                    intent.putExtra("userid",WebServices.chat_messagesUtilses.get(position).getUser_id());
                                    startActivity(intent);
                                } else if (WebServices.chat_messagesUtilses.get(position).getMedia().contains(".pdf")) {
                                    String username=WebServices.mLoginUtility.getFirstname();
//                                    if (!WebServices.mLoginUtility.getLastname().equalsIgnoreCase("null")||!WebServices.mLoginUtility.getLastname().equalsIgnoreCase(""))
//                                        username= username+" "+WebServices.mLoginUtility.getLastname();
                                    String f_name=WebServices.chat_messagesUtilses.get(position).getMedia().substring(WebServices.chat_messagesUtilses.get(position).getMedia().lastIndexOf("/")).substring(1);
                                    new APIUtility().DownloadFIle(getActivity(),WebServices.chat_messagesUtilses.get(position).getMedia(),username+"_"+f_name,TopicChatFragment.this);

                                } else if (WebServices.chat_messagesUtilses.get(position).getMedia().contains(".txt")) {
                                    Intent intent = new Intent(getActivity(), NewsFeedDetail_Activity.class);
                                    intent.putExtra("link", WebServices.chat_messagesUtilses.get(position).getMedia());
                                    intent.putExtra("title", "Discussions");
                                    intent.putExtra("type", WebServices.chat_messagesUtilses.get(position).getMedia_type());
                                    startActivity(intent);
                                }

//                                if (WebServices.chat_messagesUtilses.get(position).getMedia().contains(".png") || WebServices.chat_messagesUtilses.get(position).getMedia().contains(".jpg")) {
//                                    Intent intent = new Intent(getActivity(), ShowImageActivity.class);
//                                    intent.putExtra("imgurl", WebServices.chat_messagesUtilses.get(position).getMedia());
//                                    intent.putExtra("imguri", "");
//                                    startActivity(intent);
//                                } else if (WebServices.chat_messagesUtilses.get(position).getMedia().contains(".pdf")) {
//                                    Intent intent = new Intent(getActivity(), NewsFeedDetail_Activity.class);
//                                    intent.putExtra("link", WebServices.chat_messagesUtilses.get(position).getMedia());
//                                    intent.putExtra("title", "Discussions");
//                                    intent.putExtra("type", WebServices.chat_messagesUtilses.get(position).getMedia_type());
//                                    startActivity(intent);
//                                } else if (WebServices.chat_messagesUtilses.get(position).getMedia().contains(".txt")) {
//                                    Intent intent = new Intent(getActivity(), NewsFeedDetail_Activity.class);
//                                    intent.putExtra("link", WebServices.chat_messagesUtilses.get(position).getMedia());
//                                    intent.putExtra("title", "Discussions");
//                                    intent.putExtra("type", WebServices.chat_messagesUtilses.get(position).getMedia_type());
//                                    startActivity(intent);
//                                }
                            }
                        }
                    });

                    if (WebServices.chat_messagesUtilses.get(position).getUser_id().equalsIgnoreCase(WebServices.mLoginUtility.getUser_id())) {


                        LinearLayout member_ll=(LinearLayout)convertView.findViewById(R.id.member_ll2);
                        TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt2);
                        member_ll.setVisibility(View.VISIBLE);
                        right_new_img.setVisibility(View.GONE);
                        right_new_msg_txt.setTextColor(Color.GRAY);
                        member_fname_txt.setText("");
                        if (position % 4 == 1) {
                            member_ll.setBackgroundResource(R.drawable.test_series_bg);
                        } else if (position % 4 == 2) {
                            member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                        } else if (position % 4 == 0) {
                            member_ll.setBackgroundResource(R.drawable.light_blue_bg);
                        } else if (position % 4 == 3) {
                            member_ll.setBackgroundResource(R.drawable.audio_bg);
                        }
                        String fname=WebServices.chat_messagesUtilses.get(position ).getFirstname().trim().toString().substring(0,1);
                        member_fname_txt.setText(fname);


                        left_new_ll.setVisibility(View.GONE);
                        right_new_ll.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(WebServices.chat_messagesUtilses.get(position).getPhoto(), right_new_img);
//                        if (!WebServices.chat_messagesUtilses.get(position).getLastname().equalsIgnoreCase("null") && WebServices.chat_messagesUtilses.get(position).getLastname() != null)
//                            right_new_name_txt.setText(WebServices.chat_messagesUtilses.get(position).getFirstname() + " " + WebServices.chat_messagesUtilses.get(position).getLastname());
//                        else
                            right_new_name_txt.setText(WebServices.chat_messagesUtilses.get(position).getFirstname());
                        right_new_msg_txt.setText(Html.fromHtml(WebServices.chat_messagesUtilses.get(position).getForum_description()));
                        right_new_time_txt.setText(WebServices.chat_messagesUtilses.get(position).getAdded_on());
                        if (!WebServices.chat_messagesUtilses.get(position ).getMedia().equalsIgnoreCase("")){
                            right_attached_file_ll2.setVisibility(View.VISIBLE);
                            String filename = WebServices.chat_messagesUtilses.get(position).getMedia().substring(WebServices.chat_messagesUtilses.get(position ).getMedia().lastIndexOf("/"));
                            filename=filename.substring(1);
                            right_attach_filename_txt.setText(filename);
                        }

                    } else {

                        LinearLayout member_ll=(LinearLayout)convertView.findViewById(R.id.member_ll);
                        TextView member_fname_txt = (TextView) convertView.findViewById(R.id.member_fname_txt);
                        member_ll.setVisibility(View.VISIBLE);
                        right_new_img.setVisibility(View.GONE);
                        right_new_msg_txt.setTextColor(Color.BLACK);
                        member_fname_txt.setText("");
                        if (position % 4 == 1) {
                            member_ll.setBackgroundResource(R.drawable.test_series_bg);
                        } else if (position % 4 == 2) {
                            member_ll.setBackgroundResource(R.drawable.flashcard_bg);
                        } else if (position % 4 == 0) {
                            member_ll.setBackgroundResource(R.drawable.light_blue_bg);
                        } else if (position % 4 == 3) {
                            member_ll.setBackgroundResource(R.drawable.audio_bg);
                        }
                        String fname=WebServices.chat_messagesUtilses.get(position ).getFirstname().trim().toString().substring(0,1);
                        member_fname_txt.setText(fname);



                        right_new_ll.setVisibility(View.GONE);
                        left_new_ll.setVisibility(View.VISIBLE);
                        if (!WebServices.chat_messagesUtilses.get(position ).getMedia().equalsIgnoreCase("")){
                            left_attached_file_ll2.setVisibility(View.VISIBLE);
                            String filename = WebServices.chat_messagesUtilses.get(position).getMedia().substring(WebServices.chat_messagesUtilses.get(position ).getMedia().lastIndexOf("/"));
                            filename=filename.substring(1);
                            left_attach_filename_txt.setText(filename);
                        }
                        right_new_ll.setVisibility(View.GONE);
                        left_new_ll.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(WebServices.chat_messagesUtilses.get(position).getPhoto(), left_new_img);
//                        if (!WebServices.chat_messagesUtilses.get(position).getLastname().equalsIgnoreCase("null") && WebServices.chat_messagesUtilses.get(position).getLastname() != null)
//                            left_new_name_txt.setText(WebServices.chat_messagesUtilses.get(position).getFirstname() + " " + WebServices.chat_messagesUtilses.get(position).getLastname());
//                        else
                            left_new_name_txt.setText(WebServices.chat_messagesUtilses.get(position).getFirstname());
                        left_new_msg_txt.setText(Html.fromHtml(WebServices.chat_messagesUtilses.get(position).getForum_description()));
                        left_new_time_txt.setText(WebServices.chat_messagesUtilses.get(position).getAdded_on());
                    }

                } else {
                    convertView = mInflater.inflate(R.layout.date_section_row, null);
                    TextView date_txt = (TextView) convertView.findViewById(R.id.date_txt);
                    date_txt.setText(WebServices.chat_messagesUtilses.get(position).getDate());
                }
//                convertView = mInflater.inflate(R.layout.topic_row, null);
//                ImageView profile_img = (ImageView) convertView.findViewById(R.id.profile_image);
//                ImageView right_arrow_img = (ImageView) convertView.findViewById(R.id.right_arrow_img);
//                final TextView member_name = (TextView) convertView.findViewById(R.id.member_name_txt);
//                final TextView msg_txt = (TextView) convertView.findViewById(R.id.msg_txt);
//                TextView time_txt = (TextView) convertView.findViewById(R.id.time_txt);
//                TextView total_msg_txt = (TextView) convertView.findViewById(R.id.reply_txt);
////                TextView view_reply_txt = (TextView) convertView.findViewById(R.id.view_reply_txt);
//                if (WebServices.chat_messagesUtilses.get(position).getCommentsUtilsArrayList().size() != 0)
//                    total_msg_txt.setText((WebServices.chat_messagesUtilses.get(position).getCommentsUtilsArrayList().size())+"");
//                else total_msg_txt.setBackgroundColor(Color.TRANSPARENT);
////                right_arrow_img.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        commentsUtilsArrayList = WebServices.chat_messagesUtilses.get(position).getCommentsUtilsArrayList();
////                        Intent intent = new Intent(getActivity(), ReplyChatActivity.class);
////                        intent.putExtra("name", getCOURSE_NAME());
////                        intent.putExtra("user_name", member_name.getText().toString());
////                        intent.putExtra("msg", msg_txt.getText().toString());
////                        intent.putExtra("img", WebServices.chat_messagesUtilses.get(position).getPhoto());
////                        intent.putExtra("time", WebServices.chat_messagesUtilses.get(position).getAdded_on());
////                        intent.putExtra("forum_id", WebServices.chat_messagesUtilses.get(position).getForum_id());
////                        startActivity(intent);
////                    }
////                });
////                view_reply_txt.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        commentsUtilsArrayList=WebServices.chat_messagesUtilses.get(position).getCommentsUtilsArrayList();
////                        Intent intent=new Intent(getActivity(),ReplyChatActivity.class);
////                        intent.putExtra("name",getCOURSE_NAME());
////                        intent.putExtra("user_name",member_name.getText().toString());
////                        intent.putExtra("msg",msg_txt.getText().toString());
////                        intent.putExtra("img",WebServices.chat_messagesUtilses.get(position).getPhoto());
////                        intent.putExtra("time",WebServices.chat_messagesUtilses.get(position).getAdded_on());
////                        intent.putExtra("forum_id",WebServices.chat_messagesUtilses.get(position).getForum_id());
////                        startActivity(intent);
////                    }
////                });
//                imageLoader.DisplayImage(WebServices.chat_messagesUtilses.get(position).getPhoto(), profile_img);
//                if (!WebServices.chat_messagesUtilses.get(position).getLastname().equalsIgnoreCase("null") && WebServices.chat_messagesUtilses.get(position).getLastname() != null)
//                    member_name.setText(WebServices.chat_messagesUtilses.get(position).getFirstname() + " " + WebServices.chat_messagesUtilses.get(position).getLastname());
//                else
//                    member_name.setText(WebServices.chat_messagesUtilses.get(position).getFirstname());
//                msg_txt.setText(Html.fromHtml(WebServices.chat_messagesUtilses.get(position).getForum_description()));
//                time_txt.setText("Added on "+WebServices.chat_messagesUtilses.get(position).getAdded_on());

            }
            return convertView;
        }
    }

    public String getGroup_Id() {
        return Group_Id;
    }

    public void setGroup_Id(String group_Id) {
        Group_Id = group_Id;
    }

    public String getCOURSE_NAME() {
        return COURSE_NAME;
    }

    public void setCOURSE_NAME(String COURSE_NAME) {
        this.COURSE_NAME = COURSE_NAME;
    }



}
