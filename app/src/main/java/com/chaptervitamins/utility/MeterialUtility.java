package com.chaptervitamins.utility;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.chaptervitamins.newcode.models.NewsDescriptionModel;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by abcd on 4/25/2016.
 */
public class MeterialUtility implements Serializable, Comparable<MeterialUtility> {
    String course_id, thumbnailUrl, materialStartTime, materialEndTime, timeTaken, response_id, moduleName, earned_coins;
    boolean isOpen, isSelected;
    boolean isPassed, isNews,is_accessed;
    String material_id, max_result, isShareable, scrom_status, linkOpenOutside, isResultPublishRequired;
    String material_media_id;
    String module_id;
    String title;
    String description;
    ArrayList<NewsDescriptionModel> newsDescriptionModelArrayList;
    String instruction;
    String material_type;
    String material_templete;
    String assignCourseModuleId;
    String keywords;
    String read_more;
    String show_rating = "";
    String material_media_file_url;
    String material_media_file_type;
    String file_size;
    String read_more_required;
    String availability;
    String no_of_attempt = "0";
    String is_same_score;
    String negative_marks, media_type;
    String alloted_time;
    String test_pattern = "";
    String question_sequence;
    String option_sequence;
    String no_of_question;
    String show_score;
    String show_status;
    String is_incremented;
    String sort_order;
    String lastplayed = "0000-00-00 00:00:00";
    String remainingAttempt = "0";
    ArrayList<FlashCardUtility> flashCardUtilities = new ArrayList<>();
    String read = "1";
    String test = "1";
    String flashcard = "1";
    String video = "1";
    String audio = "1";
    String elearning = "1";
    String is_offline_available = "YES";
    String complete_per = "0";
    String response_status = "";
    String average_rating;
    String people_rated;
    String show_leaderboard;
    String passing_percentage = "0";
    String show_answer = "no";
    String scrom_url = "";
    boolean isComplete;
    String added_on = "0000-00-00 00:00:00";
    String material_image = "";
    String total_like = "", total_fav, total_view;
    String last_updated_time = "";
    String total_coins = "0", result = "",capsuleStartIndex,capsuleEndIndex;
    CoinsAllocatedModel coinsAllocatedModel;
    String course_name;
    String is_flowing_course;
    String is_result_published, assign_material_id, start_date, end_date, access_status, access_device, seen_count, total_count;
    String endpoint, registration, auth, tincan_flag, show_certificate, tincan_url, each_ques_time;
    ArrayList<String> materialSequence = new ArrayList<>();
    ArrayList<NewsUtility> AlLike, AlView = new ArrayList<>();
    String tincan_vendor = "";
    private boolean isSeen;
    private String rateNum;
    private String show_vertical = "";
    private String vertical_level = "";

    public String getTincan_vendor() {
        return tincan_vendor;
    }

    public void setTincan_vendor(String tincan_vendor) {
        this.tincan_vendor = tincan_vendor;
    }

    public String getLast_updated_time() {
        return last_updated_time;
    }

    public void setLast_updated_time(String last_updated_time) {
        this.last_updated_time = last_updated_time;
    }

    public String getTotal_like() {
        return total_like;
    }

    public void setTotal_like(String total_like) {
        this.total_like = total_like;
    }

    public String getMaterial_image() {
        return material_image;
    }

    public void setMaterial_image(String material_image) {
        this.material_image = material_image;
    }

    ArrayList<MeterialUtility> utilityArrayList = new ArrayList<>();

    public ArrayList<MeterialUtility> getUtilityArrayList() {
        return utilityArrayList;
    }

    public void setUtilityArrayList(ArrayList<MeterialUtility> utilityArrayList) {
        this.utilityArrayList = utilityArrayList;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public boolean getIsComplete() {
        return isComplete;
    }

    public void setIsComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public String getMaterial_media_id() {
        return material_media_id;
    }

    public void setMaterial_media_id(String material_media_id) {
        this.material_media_id = material_media_id;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getMaterial_type() {
        return material_type;
    }

    public void setMaterial_type(String material_type) {
        this.material_type = material_type;
    }

    /* public String getKeywords() {
         return keywords;
     }

     public void setKeywords(String keywords) {
         this.keywords = keywords;
     }

     public String getRead_more() {
         return read_more;
     }

     public void setRead_more(String read_more) {
         this.read_more = read_more;
     }
 */
    public String getMaterial_media_file_url() {
        return material_media_file_url;
    }

    public void setMaterial_media_file_url(String material_media_file_url) {
        this.material_media_file_url = material_media_file_url;
    }

//    public String getRead_more_required() {
//        return read_more_required;
//    }
//
//    public void setRead_more_required(String read_more_required) {
//        this.read_more_required = read_more_required;
//    }

//    public String getAvailability() {
//        return availability;
//    }
//
//    public void setAvailability(String availability) {
//        this.availability = availability;
//    }

    public String getNo_of_attempt() {
        return no_of_attempt;
    }

    public void setNo_of_attempt(String no_of_attempt) {
        this.no_of_attempt = no_of_attempt;
    }

    public String getIs_same_score() {
        return is_same_score;
    }

    public void setIs_same_score(String is_same_score) {
        this.is_same_score = is_same_score;
    }

    public String getNegative_marks() {
        return negative_marks;
    }

    public void setNegative_marks(String negative_marks) {
        this.negative_marks = negative_marks;
    }

    public String getAlloted_time() {
        return alloted_time;
    }

    public void setAlloted_time(String alloted_time) {
        this.alloted_time = alloted_time;
    }

    public String getTest_pattern() {
        return test_pattern;
    }

    public void setTest_pattern(String test_pattern) {
        this.test_pattern = test_pattern;
    }

    public String getQuestion_sequence() {
        return question_sequence;
    }

    public void setQuestion_sequence(String question_sequence) {
        this.question_sequence = question_sequence;
    }

    public String getOption_sequence() {
        return option_sequence;
    }

    public void setOption_sequence(String option_sequence) {
        this.option_sequence = option_sequence;
    }

    public String getNo_of_question() {
        return no_of_question;
    }

    public void setNo_of_question(String no_of_question) {
        this.no_of_question = no_of_question;
    }

    public String getShow_score() {
        return show_score;
    }

    public void setShow_score(String show_score) {
        this.show_score = show_score;
    }

    public String getShow_status() {
        return show_status;
    }

    public void setShow_status(String show_status) {
        this.show_status = show_status;
    }

    public String getIs_incremented() {
        return is_incremented;
    }

    public void setIs_incremented(String is_incremented) {
        this.is_incremented = is_incremented;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getLastplayed() {
        return lastplayed;
    }

    public void setLastplayed(String lastplayed) {
        this.lastplayed = lastplayed;
    }

    public String getRemainingAttempt() {
        return remainingAttempt;
    }

    public void setRemainingAttempt(String remainingAttempt) {
        this.remainingAttempt = remainingAttempt;
    }

    public ArrayList<FlashCardUtility> getFlashCardUtilities() {
        return flashCardUtilities;
    }

    public void setFlashCardUtilities(ArrayList<FlashCardUtility> flashCardUtilities) {
        this.flashCardUtilities = flashCardUtilities;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getFlashcard() {
        return flashcard;
    }

    public void setFlashcard(String flashcard) {
        this.flashcard = flashcard;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getElearning() {
        return elearning;
    }

    public void setElearning(String elearning) {
        this.elearning = elearning;
    }

    public String getIs_offline_available() {
        return is_offline_available;
    }

    public void setIs_offline_available(String is_offline_available) {
        this.is_offline_available = is_offline_available;
    }

    public String getComplete_per() {
        return complete_per;
    }

    public void setComplete_per(String complete_per) {
        this.complete_per = complete_per;
    }

    public String getResponse_status() {
        return response_status;
    }

    public void setResponse_status(String response_status) {
        this.response_status = response_status;
    }

    public String getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(String average_rating) {
        this.average_rating = average_rating;
    }

    public String getPeople_rated() {
        return people_rated;
    }

    public void setPeople_rated(String people_rated) {
        this.people_rated = people_rated;
    }

    public String getShow_leaderboard() {
        if (TextUtils.isEmpty(show_leaderboard))
            return "NO";
        return show_leaderboard;
    }

    public void setShow_leaderboard(String show_leaderboard) {
        this.show_leaderboard = show_leaderboard;
    }

    public String getPassing_percentage() {
        return passing_percentage;
    }

    public void setPassing_percentage(String passing_percentage) {
        this.passing_percentage = passing_percentage;
    }

    public String getFile_size() {
        return file_size;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public String getMaterial_media_file_type() {
        return material_media_file_type;
    }

    public void setMaterial_media_file_type(String material_media_file_type) {
        this.material_media_file_type = material_media_file_type;
    }

    public String getShow_answer() {
        return show_answer;
    }

    public void setShow_answer(String show_answar) {
        this.show_answer = show_answar;
    }

    public String getScrom_url() {
        return scrom_url;
    }

    public void setScrom_url(String scrom_url) {
        this.scrom_url = scrom_url;
    }

    public String getTotal_coins() {
        return total_coins;
    }

    public void setTotal_coins(String total_coins) {
        this.total_coins = total_coins;
    }


    public CoinsAllocatedModel getCoinsAllocatedModel() {
        return coinsAllocatedModel;
    }

    public void setCoinsAllocatedModel(CoinsAllocatedModel coinsAllocatedModel) {
        this.coinsAllocatedModel = coinsAllocatedModel;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public boolean getIs_flowing_course() {
        if (!TextUtils.isEmpty(is_flowing_course) && is_flowing_course.equalsIgnoreCase("yes"))
            return true;
        return false;
    }

    public void setIs_flowing_course(String is_flowing_course) {
        this.is_flowing_course = is_flowing_course;
    }

    public String getMaterial_templete() {
        return material_templete;
    }

    public void setMaterial_templete(String material_templete) {
        this.material_templete = material_templete;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public ArrayList<String> getMaterialSequence() {
        return materialSequence;
    }

    public void setMaterialSequence(ArrayList<String> materialSequence) {
        this.materialSequence = materialSequence;
    }

    public String getAssign_material_id() {
        return assign_material_id;
    }

    public void setAssign_material_id(String assign_material_id) {
        this.assign_material_id = assign_material_id;
    }

    public String getIs_result_published() {
        if (TextUtils.isEmpty(is_result_published))
            return "no";
        return is_result_published;
    }

    public void setIs_result_published(String is_result_published) {
        this.is_result_published = is_result_published;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAccess_status() {
        return access_status;
    }

    public void setAccess_status(String access_status) {
        this.access_status = access_status;
    }

    public String getAccess_device() {
        return access_device;
    }

    public void setAccess_device(String access_device) {
        this.access_device = access_device;
    }

    public boolean getIs_accessed() {
        return is_accessed;
    }

    public void setIs_accessed(boolean is_accessed) {
        this.is_accessed = is_accessed;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getTincan_flag() {
        return tincan_flag;
    }

    public void setTincan_flag(String tincan_flag) {
        this.tincan_flag = tincan_flag;
    }

    public String getShow_certificate() {
        if (TextUtils.isEmpty(show_certificate))
            return "no";
        return show_certificate;
    }

    public void setShow_certificate(String show_certificate) {
        this.show_certificate = show_certificate;
    }

    public String getTincan_url() {
        return tincan_url;
    }

    public void setTincan_url(String tincan_url) {
        this.tincan_url = tincan_url;
    }

    public boolean isSeen() {
        return isSeen;
    }

    public void setSeen(boolean seen) {
        isSeen = seen;
    }

    @Override
    public int compareTo(@NonNull MeterialUtility o) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return df.parse(this.getAdded_on()).compareTo(df.parse(o.getAdded_on()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getTotal_fav() {
        return total_fav;
    }

    public void setTotal_fav(String total_fav) {
        this.total_fav = total_fav;
    }

    public ArrayList<NewsUtility> getAlLike() {
        return AlLike;
    }

    public void setAlLike(ArrayList<NewsUtility> alLike) {
        AlLike = alLike;
    }

    public String getTotal_view() {
        return total_view;
    }

    public void setTotal_view(String total_view) {
        this.total_view = total_view;
    }

    public ArrayList<NewsUtility> getAlView() {
        return AlView;
    }

    public void setAlView(ArrayList<NewsUtility> alView) {
        AlView = alView;
    }

    public String getSeen_count() {
        return seen_count;
    }

    public void setSeen_count(String seen_count) {
        this.seen_count = seen_count;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getRateNum() {
        if (!TextUtils.isEmpty(rateNum) && !rateNum.equalsIgnoreCase("null"))
            return rateNum;
        else
            return "";
    }

    public void setRateNum(String rateNum) {
        this.rateNum = rateNum;
    }

    public String getMax_result() {
        if (!TextUtils.isEmpty(max_result))
            return max_result;
        return "0";
    }

    public void setMax_result(String max_result) {
        this.max_result = max_result;
    }

    public boolean isPassed() {
        return isPassed;
    }

    public void setPassed(boolean passed) {
        isPassed = passed;
    }

    public String getEach_ques_time() {
        if (!TextUtils.isEmpty(each_ques_time))
            return each_ques_time;
        return "no";
    }

    public void setEach_ques_time(String each_ques_time) {
        this.each_ques_time = each_ques_time;
    }

    public boolean isNews() {
        return isNews;
    }

    public void setNews(boolean news) {
        isNews = news;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getRead_more() {
        return read_more;
    }

    public void setRead_more(String read_more) {
        this.read_more = read_more;
    }

    public String getRead_more_required() {
        return read_more_required;
    }

    public void setRead_more_required(String read_more_required) {
        this.read_more_required = read_more_required;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getMaterialStartTime() {
        return materialStartTime;
    }

    public void setMaterialStartTime(String materialStartTime) {
        this.materialStartTime = materialStartTime;
    }

    public String getMaterialEndTime() {
        return materialEndTime;
    }

    public void setMaterialEndTime(String materialEndTime) {
        this.materialEndTime = materialEndTime;
    }

    public String getTimeTaken() {
        if (!TextUtils.isEmpty(timeTaken))
            return timeTaken;
        else
            return "1";
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getResponse_id() {
        return response_id;
    }

    public void setResponse_id(String response_id) {
        this.response_id = response_id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }


    public ArrayList<NewsDescriptionModel> getNewsDescriptionModel() {
        return newsDescriptionModelArrayList;
    }

    public void setNewsDescriptionModel(ArrayList<NewsDescriptionModel> newsDescriptionModelArrayList) {
        this.newsDescriptionModelArrayList = newsDescriptionModelArrayList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getIsShareable() {
        return isShareable;
    }

    public void setIsShareable(String isShareable) {
        this.isShareable = isShareable;
    }

    public String getScrom_status() {
        return scrom_status;
    }

    public void setScrom_status(String scrom_status) {
        this.scrom_status = scrom_status;
    }

    public String getLinkOpenOutside() {
        return linkOpenOutside;
    }

    public void setLinkOpenOutside(String linkOpenOutside) {
        this.linkOpenOutside = linkOpenOutside;
    }

    public String getEarned_coins() {
        return earned_coins;
    }

    public void setEarned_coins(String earned_coins) {
        this.earned_coins = earned_coins;
    }

    public String getShow_vertical() {
        return show_vertical;
    }

    public void setShow_vertical(String show_vertical) {
        this.show_vertical = show_vertical;
    }

    public String getVertical_level() {
        return vertical_level;
    }

    public void setVertical_level(String vertical_level) {
        this.vertical_level = vertical_level;
    }

    public String getShow_rating() {
        return show_rating;
    }

    public void setShow_rating(String show_rating) {
        this.show_rating = show_rating;
    }

    public String getIsResultPublishRequired() {
        if (!TextUtils.isEmpty(isResultPublishRequired))
            return isResultPublishRequired;
        else
            return "NO";
    }

    public void setIsResultPublishRequired(String isResultPublishRequired) {
        this.isResultPublishRequired = isResultPublishRequired;
    }

    public String getCapsuleStartIndex() {
        return capsuleStartIndex;
    }

    public void setCapsuleStartIndex(String capsuleStartIndex) {
        this.capsuleStartIndex = capsuleStartIndex;
    }

    public String getCapsuleEndIndex() {
        return capsuleEndIndex;
    }

    public void setCapsuleEndIndex(String capsuleEndIndex) {
        this.capsuleEndIndex = capsuleEndIndex;
    }
}
