package com.chaptervitamins.utility;

import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.model.Vertical;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abcd on 4/27/2016.
 */
public class QuestionUtility {
    String material_id;
    ArrayList<Vertical> verticalList;
    String material_media_id;
    String module_id;
    String title;
    String description;
    String instruction;
    String material_type;
    String keywords;
    String read_more;
    String read_more_required;
    String availability;
    String no_of_attempt;
    String is_same_score;
    String negative_marks;
    String alloted_time;
    String test_pattern;
    String question_sequence;
    String option_sequence;
    String no_of_question;
    String show_score;
    String show_answer;
    String show_status;
    String is_incremented;
    String sort_order;
    ArrayList<Data_util> data_utils = new ArrayList<>();
    String timeTaken = "0";
    String correctResponse;
    String incorrectResponse;
    String result;
    String questionIndex = "0";
    String quiz_complete_percentage = "0";
    String passing_percentage = "";
    private String show_vertical = "";
    private String vertical_level = "";
    String warning_time = "", warning_message, each_ques_warning_time;

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public String getMaterial_media_id() {
        return material_media_id;
    }

    public void setMaterial_media_id(String material_media_id) {
        this.material_media_id = material_media_id;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getMaterial_type() {
        return material_type;
    }

    public void setMaterial_type(String material_type) {
        this.material_type = material_type;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getRead_more() {
        return read_more;
    }

    public void setRead_more(String read_more) {
        this.read_more = read_more;
    }

    public String getRead_more_required() {
        return read_more_required;
    }

    public void setRead_more_required(String read_more_required) {
        this.read_more_required = read_more_required;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getNo_of_attempt() {
        return no_of_attempt;
    }

    public void setNo_of_attempt(String no_of_attempt) {
        this.no_of_attempt = no_of_attempt;
    }

    public String getIs_same_score() {
        return is_same_score;
    }

    public void setIs_same_score(String is_same_score) {
        this.is_same_score = is_same_score;
    }

    public String getNegative_marks() {
        return negative_marks;
    }

    public void setNegative_marks(String negative_marks) {
        this.negative_marks = negative_marks;
    }

    public String getAlloted_time() {
        return alloted_time;
    }

    public void setAlloted_time(String alloted_time) {
        this.alloted_time = alloted_time;
    }

    public String getTest_pattern() {
        return test_pattern;
    }

    public void setTest_pattern(String test_pattern) {
        this.test_pattern = test_pattern;
    }

    public String getQuestion_sequence() {
        return question_sequence;
    }

    public void setQuestion_sequence(String question_sequence) {
        this.question_sequence = question_sequence;
    }

    public String getOption_sequence() {
        return option_sequence;
    }

    public void setOption_sequence(String option_sequence) {
        this.option_sequence = option_sequence;
    }

    public String getNo_of_question() {
        return no_of_question;
    }

    public void setNo_of_question(String no_of_question) {
        this.no_of_question = no_of_question;
    }

    public String getShow_score() {
        return show_score;
    }

    public void setShow_score(String show_score) {
        this.show_score = show_score;
    }

    public String getShow_status() {
        return show_status;
    }

    public void setShow_status(String show_status) {
        this.show_status = show_status;
    }

    public String getIs_incremented() {
        return is_incremented;
    }

    public void setIs_incremented(String is_incremented) {
        this.is_incremented = is_incremented;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public ArrayList<Data_util> getData_utils() {
        return data_utils;
    }

    public void setData_utils(ArrayList<Data_util> data_utils) {
        this.data_utils = data_utils;
    }

    public String getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getCorrectResponse() {
        return correctResponse;
    }

    public void setCorrectResponse(String correctResponse) {
        this.correctResponse = correctResponse;
    }

    public String getIncorrectResponse() {
        return incorrectResponse;
    }

    public void setIncorrectResponse(String incorrectResponse) {
        this.incorrectResponse = incorrectResponse;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getQuestionIndex() {
        return questionIndex;
    }

    public void setQuestionIndex(String questionIndex) {
        this.questionIndex = questionIndex;
    }

    public String getQuiz_complete_percentage() {
        return quiz_complete_percentage;
    }

    public void setQuiz_complete_percentage(String quiz_complete_percentage) {
        this.quiz_complete_percentage = quiz_complete_percentage;
    }

    public String getShow_answer() {
        return show_answer;
    }

    public void setShow_answer(String show_answer) {
        this.show_answer = show_answer;
    }

    public String getPassing_percentage() {
        return passing_percentage;
    }

    public void setPassing_percentage(String passing_percentage) {
        this.passing_percentage = passing_percentage;
    }

    public String getWarning_time() {
        return warning_time;
    }

    public void setWarning_time(String warning_time) {
        this.warning_time = warning_time;
    }

    public String getWarning_message() {
        return warning_message;
    }

    public void setWarning_message(String warning_message) {
        this.warning_message = warning_message;
    }

    public String getEach_ques_warning_time() {
        return each_ques_warning_time;
    }

    public void setEach_ques_warning_time(String each_ques_warning_time) {
        this.each_ques_warning_time = each_ques_warning_time;
    }

    public ArrayList<Vertical> getVerticalList() {
        return verticalList;
    }

    public void setVerticalList(ArrayList<Vertical> verticalList) {
        this.verticalList = verticalList;
    }

    public String getShow_vertical() {
        return show_vertical;
    }

    public void setShow_vertical(String show_vertical) {
        this.show_vertical = show_vertical;
    }

    public String getVertical_level() {
        return vertical_level;
    }

    public void setVertical_level(String vertical_level) {
        this.vertical_level = vertical_level;
    }
}
