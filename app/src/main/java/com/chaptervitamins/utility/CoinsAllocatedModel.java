package com.chaptervitamins.utility;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Sagar on 29-06-2017.
 */

public class CoinsAllocatedModel implements Serializable {
    private String redeem = "", rule_type = "", maxCoins = "", earnCoins,currentCoins="";
    private ArrayList fromAl = new ArrayList(), toAl = new ArrayList(), coinsAl = new ArrayList();

    public String getRedeem() {
        return redeem;
    }

    public void setRedeem(String redeem) {
        this.redeem = redeem;
    }

    public String getRule_type() {
        return rule_type;
    }

    public void setRule_type(String rule_type) {
        this.rule_type = rule_type;
    }

    public ArrayList getFromAl() {
        return fromAl;
    }

    public void setFromAl(ArrayList fromAl) {
        this.fromAl = fromAl;
    }

    public ArrayList getToAl() {
        return toAl;
    }

    public void setToAl(ArrayList toAl) {
        this.toAl = toAl;
    }

    public ArrayList getCoinsAl() {
        return coinsAl;
    }

    public void setCoinsAl(ArrayList coinsAl) {
        this.coinsAl = coinsAl;
    }

    public String getMaxCoins() {
        return maxCoins;
    }

    public void setMaxCoins(String maxCoins) {
        this.maxCoins = maxCoins;
    }

    public int getCoinsAccToPercentage(MeterialUtility mMeterialUtility, int percentage, String isRedeem) {
        if (isRedeem.equalsIgnoreCase("TRUE")) {
            return 0;
        }
        int position = -1, coins = 0;
        int size = toAl.size();
        switch (rule_type) {
            case "CONDITIONAL":
                if (toAl.size() > 0 && fromAl.size() > 0 && coinsAl.size() > 0) {
                    for (int i = 0; i < size; i++) {
                        if (percentage >= (int) fromAl.get(i) && percentage <= (int) toAl.get(i)) {
                            position = i;
                            break;
                        }
                    }
                    if (position != -1 && toAl.size() >= position)
                        coins = (int) coinsAl.get(position);
                    else
                        coins = 0;
                }
                break;
            case "FLAT":
                if (toAl.size() > 0 && fromAl.size() > 0 && coinsAl.size() > 0) {
                    if (percentage >= (int) fromAl.get(0) && percentage <= (int) toAl.get(0)) {
                        coins = (int) coinsAl.get(0);
                    }
                }
                break;
            default:
                coins = 0;
        }
        if (!TextUtils.isEmpty(mMeterialUtility.getEarned_coins()) && coins == Integer.parseInt(mMeterialUtility.getEarned_coins()))
            coins = 0;
        return coins;
    }

    public String getEarnCoins() {
        return earnCoins;
    }

    public void setEarnCoins(String earnCoins) {
        this.earnCoins = earnCoins;
    }

    public String getCurrentCoins() {
        return currentCoins;
    }

    public void setCurrentCoins(String currentCoins) {
        this.currentCoins = currentCoins;
    }
}
