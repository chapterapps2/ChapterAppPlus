package com.chaptervitamins.utility;

/**
 * Created by Android on 11/24/2016.
 */

public class Certificate_Utils {
    String Course_id="";
    String Material_id="";
    String Material_name="";

    public String getCourse_id() {
        return Course_id;
    }

    public void setCourse_id(String course_id) {
        Course_id = course_id;
    }

    public String getMaterial_id() {
        return Material_id;
    }

    public void setMaterial_id(String material_id) {
        Material_id = material_id;
    }

    public String getMaterial_name() {
        return Material_name;
    }

    public void setMaterial_name(String material_name) {
        Material_name = material_name;
    }
}
