package com.chaptervitamins.utility;

/**
 * Created by Android on 11/22/2016.
 */

public class Offline_Materials_Utils {
    String Material_id="";
    String Material_name="";
    String Material_size="";
    String Material_type="";
    boolean isSelected=false;

    public String getMaterial_id() {
        return Material_id;
    }

    public void setMaterial_id(String material_id) {
        Material_id = material_id;
    }

    public String getMaterial_name() {
        return Material_name;
    }

    public void setMaterial_name(String material_name) {
        Material_name = material_name;
    }

    public String getMaterial_size() {
        return Material_size;
    }

    public void setMaterial_size(String material_size) {
        Material_size = material_size;
    }

    public String getMaterial_type() {
        return Material_type;
    }

    public void setMaterial_type(String material_type) {
        Material_type = material_type;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
