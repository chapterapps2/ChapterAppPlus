package com.chaptervitamins.utility;

/**
 * Created by Android on 7/1/2016.
 */

public class GroupMembersUtils {
    String group_member_id;
    String group_id;
    String user_id;
    String firstname;
//    String lastname;
    String email;
    String photo;
    boolean isSelected=false;
    public String getGroup_member_id() {
        return group_member_id;
    }

    public void setGroup_member_id(String group_member_id) {
        this.group_member_id = group_member_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

  /*  public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }*/

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
