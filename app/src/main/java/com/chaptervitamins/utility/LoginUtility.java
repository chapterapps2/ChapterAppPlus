package com.chaptervitamins.utility;

/**
 * Created by abcd on 4/20/2016.
 */
public class LoginUtility {
    String user_id;
    String email;
    String cobrand_msg;
    String branch_app_version;
    //    String urn;
    String employee_id;
    String firstname = "";
    //    String lastname="";
    String password;
    String loggedin;
    String participation_type;
    String designation;
    String role_id,branch_mixpanel_token;
    //    String unit;
    String branch_id;
    String branch_name;
    String branch_image;
    String organization_id;
    String circle;
    String region;
    String zone;
    String organization_name;
    String photo = "url";
    String phone;
    String coins_collected,rank = "0",total_users,rankMessage,showMessage;
    Session session = new Session();
    Device device = new Device();
    String organization_color2="";
    String organization_color1="";
    String api_key="";
    String welcome_msg="";

    public String getBranch_color1() {
        return branch_color1;
    }

    public void setBranch_color1(String branch_color1) {
        this.branch_color1 = branch_color1;
    }

    public String getOrganization_color1() {
        return organization_color1;
    }

    public void setOrganization_color1(String organization_color1) {
        this.organization_color1 = organization_color1;
    }

    String branch_color1="";

    public String getBranch_color2() {
        return branch_color2;
    }

    public void setBranch_color2(String branch_color2) {
        this.branch_color2 = branch_color2;
    }

    public String getOrganization_color2() {
        return organization_color2;
    }

    public void setOrganization_color2(String organization_color2) {
        this.organization_color2 = organization_color2;
    }

    String branch_color2="";

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /*public String getUrn() {
        return urn;
    }

    public void setUrn(String urn) {
        this.urn = urn;
    }*/

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

  /*  public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }*/

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoggedin() {
        return loggedin;
    }

    public void setLoggedin(String loggedin) {
        this.loggedin = loggedin;
    }

    public String getParticipation_type() {
        return participation_type;
    }

    public void setParticipation_type(String participation_type) {
        this.participation_type = participation_type;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    /*public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }*/

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getOrganization_name() {
        return organization_name;
    }

    public void setOrganization_name(String organization_name) {
        this.organization_name = organization_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getCoins_collected() {
        return coins_collected;
    }

    public void setCoins_collected(String coins_collected) {
        this.coins_collected = coins_collected;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getBranch_image() {
        return branch_image;
    }

    public void setBranch_image(String branch_image) {
        this.branch_image = branch_image;
    }

    public String getTotal_users() {
        return total_users;
    }

    public void setTotal_users(String total_users) {
        this.total_users = total_users;
    }

    public String getRankMessage() {
        return rankMessage;
    }

    public void setRankMessage(String rankMessage) {
        this.rankMessage = rankMessage;
    }

    public String getShowMessage() {
        return showMessage;
    }

    public void setShowMessage(String showMessage) {
        this.showMessage = showMessage;
    }

    public String getBranch_mixpanel_token() {
        return branch_mixpanel_token;
    }

    public void setBranch_mixpanel_token(String branch_mixpanel_token) {
        this.branch_mixpanel_token = branch_mixpanel_token;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getBranch_app_version() {
        return branch_app_version;
    }

    public void setBranch_app_version(String branch_app_version) {
        this.branch_app_version = branch_app_version;
    }

    public String getWelcome_msg() {
        return welcome_msg;
    }

    public void setWelcome_msg(String welcome_msg) {
        this.welcome_msg = welcome_msg;
    }

    public String getCobrand_msg() {
        return cobrand_msg;
    }

    public void setCobrand_msg(String cobrand_msg) {
        this.cobrand_msg = cobrand_msg;
    }

    public class Session {
        String session_id;
        String session_token;
        String loggedin_time;

        public String getSession_id() {
            return session_id;
        }

        public void setSession_id(String session_id) {
            this.session_id = session_id;
        }

        public String getSession_token() {
            return session_token;
        }

        public void setSession_token(String session_token) {
            this.session_token = session_token;
        }

        public String getLoggedin_time() {
            return loggedin_time;
        }

        public void setLoggedin_time(String loggedin_time) {
            this.loggedin_time = loggedin_time;
        }
    }

    public class Device {
        String device_type;
        String push_token;
        String app_id;
        String added_on;
        String device_id;

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getPush_token() {
            return push_token;
        }

        public void setPush_token(String push_token) {
            this.push_token = push_token;
        }

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public String getAdded_on() {
            return added_on;
        }

        public void setAdded_on(String added_on) {
            this.added_on = added_on;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }
    }
}
