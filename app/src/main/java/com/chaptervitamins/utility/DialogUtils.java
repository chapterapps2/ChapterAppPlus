package com.chaptervitamins.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.chaptervitamins.R;

/**
 * Created by Sagar on 02-06-2017.
 */

public class DialogUtils {
 public static Dialog dialog=null;
    public static void showDialog(final Context context, String message) {
         AlertDialog.Builder dialogBuilder= new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.app_name)).setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        });
        if(!((Activity)context).isFinishing())
            dialog=dialogBuilder.show();



    }
    public static void showDialog(final Context context, String message, final Runnable handler1) {
        AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.app_name)).setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                    dialogInterface.dismiss();
                if(handler1!=null)
                    handler1.run();
            }
        });
        if(!((Activity)context).isFinishing())
            dialog=dialogBuilder.show();
    }

    public static void showDialog(final Context context, String title, String message) {
        AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(context)
                .setTitle(title).setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                    dialogInterface.dismiss();
            }
        });
        if(!((Activity)context).isFinishing())
            dialog=dialogBuilder.show();
    }

    public static void showDialog(final Context context, String message, final Runnable handler1, final Runnable handler2) {
        AlertDialog.Builder dialogBuilder=new AlertDialog.Builder(context)
                .setCancelable(false)
                .setTitle(context.getString(R.string.app_name)).setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                dialogInterface.dismiss();
                if (handler1 != null)
                    handler1.run();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                    dialogInterface.dismiss();
                if (handler2 != null)
                    handler2.run();

            }
        });
        if(!((Activity)context).isFinishing())
            dialog=dialogBuilder.show();
    }

}
