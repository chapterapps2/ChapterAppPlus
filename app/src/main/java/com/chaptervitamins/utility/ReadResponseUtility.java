package com.chaptervitamins.utility;

/**
 * Created by abcd on 5/10/2016.
 */
public class ReadResponseUtility {
    String firstname;
    String lastname;
    String moduleId, assign_material_id,avg_rating,my_rating,assign_material_trainer_id,trainer_name,trainer_emp_id,trainer_id,
            is_evaluated,scrom_status;
    String response_id;
    String user_id;
    String material_id;
    String response_type;
    String result;
    String taken_device;
    String time_taken;
    String start_time;
    String finish_time;
    String added_on;
    String course_name;
    String correct_question;
    String incorrect_question,unanswered_question;
    String title;
    String test_pattern;
    String quiz_response,coins_allocated,redeem;
    String rank="0";
    long total_Time=0;
    String course_id,max_score;
    String total_count,seen_count,completion_per,organization_id,branch_id,attempt_taken;

    public long getTotal_Time() {
        return total_Time;
    }

    public void setTotal_Time(long total_Time) {
        this.total_Time = total_Time;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getResponse_id() {
        return response_id;
    }

    public void setResponse_id(String response_id) {
        this.response_id = response_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getTime_taken() {
        return time_taken;
    }

    public void setTime_taken(String time_taken) {
        this.time_taken = time_taken;
    }

    public String getTaken_device() {
        return taken_device;
    }

    public void setTaken_device(String taken_device) {
        this.taken_device = taken_device;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResponse_type() {
        return response_type;
    }

    public void setResponse_type(String response_type) {
        this.response_type = response_type;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCorrect_question() {
        return correct_question;
    }

    public void setCorrect_question(String correct_question) {
        this.correct_question = correct_question;
    }

    public String getIncorrect_question() {
        return incorrect_question;
    }

    public void setIncorrect_question(String incorrect_question) {
        this.incorrect_question = incorrect_question;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTest_pattern() {
        return test_pattern;
    }

    public void setTest_pattern(String test_pattern) {
        this.test_pattern = test_pattern;
    }

    public String getQuiz_response() {
        return quiz_response;
    }

    public void setQuiz_response(String quiz_response) {
        this.quiz_response = quiz_response;
    }

    public String getCoins_allocated() {
        return coins_allocated;
    }

    public void setCoins_allocated(String coins_allocated) {
        this.coins_allocated = coins_allocated;
    }

    public String getRedeem() {
        return redeem;
    }

    public void setRedeem(String redeem) {
        this.redeem = redeem;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getAssign_material_id() {
        return assign_material_id;
    }

    public void setAssign_material_id(String assign_material_id) {
        this.assign_material_id = assign_material_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    public String getSeen_count() {
        return seen_count;
    }

    public void setSeen_count(String seen_count) {
        this.seen_count = seen_count;
    }

    public String getCompletion_per() {
        return completion_per;
    }

    public void setCompletion_per(String completion_per) {
        this.completion_per = completion_per;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getAttempt_taken() {
        return attempt_taken;
    }

    public void setAttempt_taken(String attempt_taken) {
        this.attempt_taken = attempt_taken;
    }

    public String getUnanswered_question() {
        return unanswered_question;
    }

    public void setUnanswered_question(String unanswered_question) {
        this.unanswered_question = unanswered_question;
    }

    public String getMax_score() {
        return max_score;
    }

    public void setMax_score(String max_score) {
        this.max_score = max_score;
    }

    public String getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(String avg_rating) {
        this.avg_rating = avg_rating;
    }

    public String getMy_rating() {
        return my_rating;
    }

    public void setMy_rating(String my_rating) {
        this.my_rating = my_rating;
    }

    public String getAssign_material_trainer_id() {
        return assign_material_trainer_id;
    }

    public void setAssign_material_trainer_id(String assign_material_trainer_id) {
        this.assign_material_trainer_id = assign_material_trainer_id;
    }

    public String getTrainer_name() {
        return trainer_name;
    }

    public void setTrainer_name(String trainer_name) {
        this.trainer_name = trainer_name;
    }

    public String getTrainer_emp_id() {
        return trainer_emp_id;
    }

    public void setTrainer_emp_id(String trainer_emp_id) {
        this.trainer_emp_id = trainer_emp_id;
    }

    public String getTrainer_id() {
        return trainer_id;
    }

    public void setTrainer_id(String trainer_id) {
        this.trainer_id = trainer_id;
    }

    public String getIs_evaluated() {
        return is_evaluated;
    }

    public void setIs_evaluated(String is_evaluated) {
        this.is_evaluated = is_evaluated;
    }

    public String getScrom_status() {
        return scrom_status;
    }

    public void setScrom_status(String scrom_status) {
        this.scrom_status = scrom_status;
    }
}

