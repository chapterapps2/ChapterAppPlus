package com.chaptervitamins.utility;

public interface JavaScriptUtilityListener {

	public void getSelectedText(String value);
	public void getCompleteHtmlTextInWebview(String value);
	public void getNoteIdOfNoteClicked(String value);
	public void getHighlightsRemovedText(String value);
}
