package com.chaptervitamins.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.activities.StartModuleFlowingCourse;
import com.chaptervitamins.newcode.activities.ModuleActivity;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.APIUtility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vijay Antil on 04-07-2017.
 */

public class FlowingCourseUtils {

    public FlowingCourseUtils(Context context) {
    }

    /**
     * This method is used to get Total Coins Count from static coursesUtilityArraylist according to meterialId and courseId
     *
     * @param meterialId,courseId
     * @return coinsAllocatedModel
     */
    public static CoinsAllocatedModel getCoinsAllocatedFromCourseList(String courseId, String meterialId) {
        CoinsAllocatedModel coinsAllocatedModel = null;
        if (!TextUtils.isEmpty(courseId) && !TextUtils.isEmpty(meterialId)) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            if (courseUtilities != null && courseUtilities.size() > 0) {
                int courseSize = courseUtilities.size();
                for (int i = 0; i < courseSize; i++) {
                    CourseUtility courseUtility = courseUtilities.get(i);
                    if (courseUtility != null) {
                        if (courseUtility.getCourse_id().equals(courseId)) {
                            ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtility.getModulesUtilityArrayList();
                            if (modulesUtilityArrayList != null && modulesUtilityArrayList.size() > 0) {
                                int moduleSize = modulesUtilityArrayList.size();
                                for (int j = 0; j < moduleSize; j++) {
                                    ModulesUtility modulesUtility = modulesUtilityArrayList.get(j);
                                    if (modulesUtility != null) {
                                        ArrayList<MeterialUtility> meterialUtilityArrayList = modulesUtility.getMeterialUtilityArrayList();
                                        if (meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0) {
                                            int materialSize = meterialUtilityArrayList.size();
                                            for (int k = 0; k < materialSize; k++) {
                                                MeterialUtility meterialUtility = meterialUtilityArrayList.get(k);
                                                if (meterialUtility != null && meterialUtility.getMaterial_id().equals(meterialId)) {
                                                    return meterialUtility.getCoinsAllocatedModel();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return coinsAllocatedModel;
    }

    /**
     * This method is used to get Total Coins Count from static coursesUtilityArraylist according to meterialId
     *
     * @param meterialId
     * @return coinsAllocatedModel
     */
    public static CoinsAllocatedModel getCoinsAllocatedFromCourseList(String meterialId) {
        CoinsAllocatedModel coinsAllocatedModel = null;
        if (!TextUtils.isEmpty(meterialId)) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            if (courseUtilities != null && courseUtilities.size() > 0) {
                int courseSize = courseUtilities.size();
                for (int i = 0; i < courseUtilities.size(); i++) {
                    CourseUtility courseUtility = courseUtilities.get(i);
                    if (courseUtility != null) {
                        ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtility.getModulesUtilityArrayList();
                        if (modulesUtilityArrayList != null && modulesUtilityArrayList.size() > 0) {
                            int moduleSize = modulesUtilityArrayList.size();
                            for (int j = 0; j < moduleSize; j++) {
                                ModulesUtility modulesUtility = modulesUtilityArrayList.get(j);
                                if (modulesUtility != null) {
                                    ArrayList<MeterialUtility> meterialUtilityArrayList = modulesUtility.getMeterialUtilityArrayList();
                                    if (meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0) {
                                        int materialSize = meterialUtilityArrayList.size();
                                        for (int k = 0; k < materialSize; k++) {
                                            MeterialUtility meterialUtility = meterialUtilityArrayList.get(k);
                                            if (meterialUtility != null && meterialUtility.getMaterial_id().equals(meterialId)) {
                                                return meterialUtility.getCoinsAllocatedModel();
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }


            }
        }
        return coinsAllocatedModel;
    }

    /**
     * This method is used to get Total Coins Count from static coursesUtilityArraylist according to meterialId,moduleId,courseId
     *
     * @param meterialId,courseId,moduleId
     * @return coinsAllocatedModel
     */

    public static CoinsAllocatedModel getCoinsAllocatedFromCourseList(String courseId, String moduleId, String meterialId) {
        CoinsAllocatedModel coinsAllocatedModel = null;
        if (!TextUtils.isEmpty(courseId) && !TextUtils.isEmpty(meterialId) && !TextUtils.isEmpty(moduleId)) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            if (courseUtilities != null && courseUtilities.size() > 0) {
                int courseSize = courseUtilities.size();
                for (int i = 0; i < courseUtilities.size(); i++) {
                    CourseUtility courseUtility = courseUtilities.get(i);
                    if (courseUtility != null) {
                        if (courseUtility.getCourse_id().equals(courseId)) {
                            ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtility.getModulesUtilityArrayList();
                            if (modulesUtilityArrayList != null && modulesUtilityArrayList.size() > 0) {
                                int moduleSize = modulesUtilityArrayList.size();
                                for (int j = 0; j < moduleSize; j++) {
                                    ModulesUtility modulesUtility = modulesUtilityArrayList.get(j);
                                    if (modulesUtility != null) {
                                        if (modulesUtility.getModule_id().equals(moduleId)) {
                                            ArrayList<MeterialUtility> meterialUtilityArrayList = modulesUtility.getMeterialUtilityArrayList();
                                            if (meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0) {
                                                int materialSize = meterialUtilityArrayList.size();
                                                for (int k = 0; k < materialSize; k++) {
                                                    MeterialUtility meterialUtility = meterialUtilityArrayList.get(k);
                                                    if (meterialUtility != null && meterialUtility.getMaterial_id().equals(meterialId)) {
                                                        return meterialUtility.getCoinsAllocatedModel();
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }
        }
        return coinsAllocatedModel;
    }

    /**
     * This method is used to get Total Coins Count from static coursesUtilityArraylist according to meterialId,moduleId,courseId
     *
     * @param meterialUtility
     * @return coinsAllocatedModel
     */

    public static MeterialUtility getMaterialFromCourse(MeterialUtility meterialUtility) {
        if (meterialUtility != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            if (courseUtilities != null && courseUtilities.size() > 0) {
                int courseSize = courseUtilities.size();
                for (int i = 0; i < courseSize; i++) {
                    CourseUtility courseUtility = courseUtilities.get(i);
                    if (courseUtility != null) {
                        if (courseUtility.getCourse_id().equals(meterialUtility.getCourse_id())) {
                            ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtility.getModulesUtilityArrayList();
                            if (modulesUtilityArrayList != null && modulesUtilityArrayList.size() > 0) {
                                int moduleSize = modulesUtilityArrayList.size();
                                for (int j = 0; j < moduleSize; j++) {
                                    ModulesUtility modulesUtility = modulesUtilityArrayList.get(j);
                                    if (modulesUtility != null) {
                                        if (modulesUtility.getModule_id().equals(meterialUtility.getModule_id())) {
                                            ArrayList<MeterialUtility> meterialUtilityArrayList = modulesUtility.getMeterialUtilityArrayList();
                                            if (meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0) {
                                                int materialSize = meterialUtilityArrayList.size();
                                                for (int k = 0; k < materialSize; k++) {
                                                    MeterialUtility meterialUtility1 = meterialUtilityArrayList.get(k);
                                                    if (meterialUtility1 != null && meterialUtility1.getMaterial_id().equals(meterialUtility.getMaterial_id())) {
                                                        return meterialUtility1;
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }
        }
        return null;
    }


    /**
     * It is used to get the material arraylist of the module
     *
     * @param courseId
     * @param moduleId
     * @return
     */
    public static ArrayList<MeterialUtility> getMaterialsFromModule(String courseId, String moduleId) {
        ArrayList<MeterialUtility> meterialUtilityArrayList = null;
        if (!TextUtils.isEmpty(courseId) && !TextUtils.isEmpty(moduleId) && HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null && course.getCourse_id().equalsIgnoreCase(courseId)) {
                    ArrayList<ModulesUtility> modulesUtilities = course.getModulesUtilityArrayList();
                    for (ModulesUtility module : modulesUtilities) {
                        if (module != null && module.getModule_id().equals(moduleId)) {
                            meterialUtilityArrayList = module.getMeterialUtilityArrayList();
                            return meterialUtilityArrayList;
                        }
                    }
                }
            }
        }
        return meterialUtilityArrayList;
    }

    /**
     * It is used to get the material arraylist of the module
     *
     * @return
     */
    public static MeterialUtility getMaterialByMaterialId(String materialId) {

        if (!TextUtils.isEmpty(materialId) && HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null) {
                    ArrayList<ModulesUtility> modulesUtilities = course.getModulesUtilityArrayList();
                    for (ModulesUtility module : modulesUtilities) {
                        if (module != null) {
                            ArrayList<MeterialUtility> meterialUtilityArrayList = meterialUtilityArrayList = module.getMeterialUtilityArrayList();
                            if (meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0) {
                                for (MeterialUtility meterialUtility : meterialUtilityArrayList) {
                                    if (meterialUtility != null) {
                                        if (meterialUtility.getMaterial_id().equals(materialId))
                                            return meterialUtility;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * It is used to get the course model from CourseList
     *
     * @return
     */
    public static CourseUtility getCourseByCourseId(String courseId) {

        if (!TextUtils.isEmpty(courseId) && HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null) {
                    if (course.getCourse_id().equalsIgnoreCase(courseId)) {
                        return course;
                    }
                }
            }
        }
        return null;
    }

    /**
     * It is used to get the course position of the CourseList
     *
     * @return
     */
    public static int getCoursePosition(String courseID) {
        if (!TextUtils.isEmpty(courseID)) {
            ArrayList<CourseUtility> courseUtilityArrayList = HomeActivity.courseUtilities;
            if (courseUtilityArrayList != null) {
                for (int i = 0; i < courseUtilityArrayList.size(); i++) {
                    if (courseUtilityArrayList.get(i).getCourse_id().equals(courseID))
                        return i;
                }
            }
        }
        return -1;
    }

    /**
     * It is used to get module position in moudles array list of a course
     *
     * @param courseUtility
     * @param moduleId
     * @return
     */
    public static int getModulePosition(CourseUtility courseUtility, String moduleId) {
        if (courseUtility != null && !TextUtils.isEmpty(moduleId)) {
            ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtility.getModulesUtilityArrayList();
            if (modulesUtilityArrayList != null) {
                for (int i = 0; i < modulesUtilityArrayList.size(); i++) {
                    if (modulesUtilityArrayList.get(i).getModule_id().equals(moduleId))
                        return i;
                }
            }
        }
        return -1;
    }

    /**
     * It is used to get module position in moudles array list
     *
     * @param modulesUtilityArrayList
     * @param moduleId
     * @return
     */
    public static int getModulePositionFromModuleList(ArrayList<ModulesUtility> modulesUtilityArrayList, String moduleId) {
        for (int i = 0; i < modulesUtilityArrayList.size(); i++) {
            if (modulesUtilityArrayList.get(i).getModule_id().equals(moduleId))
                return i;
        }
        return -1;
    }

    /**
     * It is used to get module position in modules array list of a course
     *
     * @param courseUtility
     * @param moduleId
     * @return
     */
    public static ModulesUtility getModuleFromCourse(CourseUtility courseUtility, String moduleId) {
        if (courseUtility != null && !TextUtils.isEmpty(moduleId)) {
            ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtility.getModulesUtilityArrayList();
            if (modulesUtilityArrayList != null) {
                for (int i = 0; i < modulesUtilityArrayList.size(); i++) {
                    if (modulesUtilityArrayList.get(i).getModule_id().equals(moduleId))
                        return modulesUtilityArrayList.get(i);
                }
            }
        }
        return null;
    }

    /**
     * It is used to get Course Object from module position
     *
     * @param moduleId
     * @return
     */
    public static CourseUtility getCourseFromModuleId(String moduleId) {
        if (!TextUtils.isEmpty(moduleId) && HomeActivity.courseUtilities != null) {
            for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
                CourseUtility courseUtility = HomeActivity.courseUtilities.get(i);
                if (courseUtility != null) {
                    ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtility.getModulesUtilityArrayList();
                    if (modulesUtilityArrayList != null) {
                        for (int j = 0; j < modulesUtilityArrayList.size(); j++) {
                            ModulesUtility modulesUtility = modulesUtilityArrayList.get(j);
                            if (modulesUtility != null) {
                                if (modulesUtility.getModule_id().equals(moduleId))
                                    return courseUtility;
                            }
                        }
                    }
                }
            }

        }
        return null;
    }


    /**
     * It is used to get the material arraylist of the module
     *
     * @param moduleId
     * @return
     */
    public static ArrayList<MeterialUtility> getMaterialsFromModule(String moduleId) {
        ArrayList<MeterialUtility> meterialUtilityArrayList = null;
        if (!TextUtils.isEmpty(moduleId) && HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null) {
                    ArrayList<ModulesUtility> modulesUtilities = course.getModulesUtilityArrayList();
                    for (ModulesUtility module : modulesUtilities) {
                        if (module != null && module.getModule_id().equals(moduleId)) {
                            meterialUtilityArrayList = module.getMeterialUtilityArrayList();
                            return meterialUtilityArrayList;
                        }
                    }
                }
            }
        }
        return meterialUtilityArrayList;
    }

    /**
     * It is used to get all materials
     *
     * @return
     */
    public static ArrayList<MeterialUtility> getAllMaterials() {
        ArrayList<MeterialUtility> meterialUtilityArrayList = new ArrayList<>();
        if (HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null && (course.getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) || course.getCourse_type().equalsIgnoreCase(AppConstants.CourseType.SURVEY))) {
                    ArrayList<ModulesUtility> modulesUtilities = course.getModulesUtilityArrayList();
                    for (ModulesUtility module : modulesUtilities) {
                        if (module != null) {
                            meterialUtilityArrayList.addAll(module.getMeterialUtilityArrayList());
                        }
                    }
                }
            }
            Collections.sort(meterialUtilityArrayList);
            Collections.reverse(meterialUtilityArrayList);
        }
        return meterialUtilityArrayList;
    }
    public static ArrayList<MeterialUtility> getAllAssessmentMaterials() {
        ArrayList<MeterialUtility> meterialUtilityArrayList = new ArrayList<>();
        if (HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null && (course.getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) || course.getCourse_type().equalsIgnoreCase(AppConstants.CourseType.SURVEY))) {
                    ArrayList<ModulesUtility> modulesUtilities = course.getModulesUtilityArrayList();
                    for (ModulesUtility module : modulesUtilities) {
                        if (module != null) {
                           for(MeterialUtility meterialUtility:module.getMeterialUtilityArrayList()){
                               if(!TextUtils.isEmpty(meterialUtility.getMaterial_type())&& meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)||
                                       meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.MULTIMEDIAQUIZ)){
                                   meterialUtilityArrayList.add(meterialUtility);
                               }

                           }
                        }
                    }
                }
            }
           /* Collections.sort(meterialUtilityArrayList);
            Collections.reverse(meterialUtilityArrayList);*/
        }
        return meterialUtilityArrayList;
    }

   /* *//**
     * It is used to get all materials types
     *
     * @return
     *//*
    public static ArrayList<MeterialUtility> getAllMaterialsType() {
        ArrayList<MaterialTypeModel> materialTypeModelArrayList = new ArrayList<>();
        ArrayList<MeterialUtility> meterialUtilityArrayList = getAllMaterials();
        for(int i=0;i<meterialUtilityArrayList.size();i++){
            MeterialUtility meterialUtility = meterialUtilityArrayList.get(i);
            for(int j = 0;j<materialTypeModelArrayList.size();j++){
                MaterialTypeModel materialTypeModel = materialTypeModelArrayList.get(i);
                if(materialTypeModel!=null){
                    if(materialTypeMode)
                }
            }
        }
        return meterialUtilityArrayList;
    }*/

    /**
     * It is used to get all materials
     *
     * @return
     */
    public static ArrayList<ModulesUtility> getAllModules() {
        ArrayList<ModulesUtility> moduleUtilityArrayList = new ArrayList<>();
        if (HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null && (course.getCourse_type().equalsIgnoreCase(AppConstants.CourseType.COURSE) || course.getCourse_type().equalsIgnoreCase(AppConstants.CourseType.SURVEY))) {
                    moduleUtilityArrayList.addAll(course.getModulesUtilityArrayList());

                }
            }
        }
        return moduleUtilityArrayList;
    }

    public static List<MeterialUtility> getRecentFiveAssignedMaterials() {
        if (getAllMaterials() != null && getAllMaterials().size() >= 5)
            return getAllMaterials().subList(0, 5);

        return getAllMaterials();
    }

    /**
     * It is used to get the material arraylist of the module
     *
     * @param materialId,isMaterial
     * @return
     */
    public static ArrayList<MeterialUtility> getMaterialsFromModule(String materialId, boolean isMaterial) {
        ArrayList<MeterialUtility> meterialUtilityArrayList = null;
        if (!TextUtils.isEmpty(materialId) && HomeActivity.courseUtilities != null) {
            ArrayList<CourseUtility> courseUtilities = HomeActivity.courseUtilities;
            for (CourseUtility course : courseUtilities) {
                if (course != null) {
                    ArrayList<ModulesUtility> modulesUtilities = course.getModulesUtilityArrayList();
                    for (ModulesUtility module : modulesUtilities) {
                        if (module != null) {
                            meterialUtilityArrayList = module.getMeterialUtilityArrayList();
                            if (meterialUtilityArrayList != null) {
                                for (MeterialUtility meterialUtility : meterialUtilityArrayList) {
                                    if (meterialUtility != null && meterialUtility.getMaterial_id().equals(materialId)) {
                                        return meterialUtilityArrayList;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return meterialUtilityArrayList;
    }

    public static int getPositionOfMeterial(String meterialId, ArrayList<MeterialUtility> meterialArrayList) {
        int currentPosition = -1;
        if (meterialArrayList != null) {
            for (int i = 0; i < meterialArrayList.size(); i++) {
                if (meterialArrayList.get(i).getMaterial_id().equals(meterialId)) {
                    currentPosition = i;
                    return currentPosition;
                }
            }
        }
        return currentPosition;
    }

    /**
     * It is used to check the btn status of previous and next  button
     *
     * @param currentPosition
     * @param meterialArrayList
     * @return
     */
    public static int[] showPrevNextButtonStatus(Activity mContext, int currentPosition,
                                                 ArrayList<MeterialUtility> meterialArrayList) {
        int[] btnStatus = new int[]{View.GONE, View.GONE};
        int size = meterialArrayList.size();
        if (!APIUtility.showFlowingCourse || size == 1)
            return btnStatus;
        if (currentPosition != -1) {
            RelativeLayout rlFlowingCourse = (RelativeLayout) mContext.findViewById(R.id.rl_flowing_course);
            if (rlFlowingCourse != null)
                rlFlowingCourse.setVisibility(View.VISIBLE);
            if (currentPosition == 0)
                btnStatus = new int[]{View.GONE, View.VISIBLE};
            else if (currentPosition == size - 1)
                btnStatus = new int[]{View.VISIBLE, View.GONE};
            else
                btnStatus = new int[]{View.VISIBLE, View.VISIBLE};

        }
        return btnStatus;
    }

    public static int[] showPrevNextModuleButtonStatus(Activity mContext, int currentPosition,
                                                       ArrayList<MeterialUtility> meterialArrayList,
                                                       int modulePos, ArrayList<ModulesUtility> moduleUtilityList) {
        int[] btnStatus = new int[]{View.GONE, View.GONE};
        if (moduleUtilityList != null /*&& currentPosition == (meterialArrayList.size() - 1)*/) {
            int size = moduleUtilityList.size();
            if (!APIUtility.showFlowingCourse || size == 1)
                return btnStatus;
            if (modulePos != -1) {
                RelativeLayout rlFlowingCourse = (RelativeLayout) mContext.findViewById(R.id.rl_module_flowing_course);
                if (rlFlowingCourse != null)
                    rlFlowingCourse.setVisibility(View.VISIBLE);
                if (modulePos == 0)
                    btnStatus = new int[]{View.GONE, View.VISIBLE};
                else if (modulePos == size - 1)
                    btnStatus = new int[]{View.VISIBLE, View.GONE};
                else
                    btnStatus = new int[]{View.VISIBLE, View.VISIBLE};
            }
        }
        return btnStatus;
    }

    public static void setVisibilityOfButton(int btnStatus, Button btn) {
        switch (btnStatus) {
            case 0:
                btn.setVisibility(View.VISIBLE);
                break;
            case 4:
                btn.setVisibility(View.INVISIBLE);
                break;
            case 8:
                btn.setVisibility(View.GONE);
                break;
            default:
                btn.setVisibility(View.GONE);
        }
    }

    public static void callFlowingCourseMaterial(final Context context, ArrayList<MeterialUtility> meterialUtilityAl, int position, boolean isNext) {
        MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(context);
        DataBase mDatabase = DataBase.getInstance(context);
        try {
            int meterialPos;
            if (isNext)
                meterialPos = position + 1;
            else
                meterialPos = position - 1;
            if (meterialUtilityAl == null || meterialUtilityAl.size() < meterialPos)
                return;
            MeterialUtility meterialUtility = meterialUtilityAl.get(meterialPos);
            if (meterialUtility != null) {
                mixPanelManager.selectmaterial((Activity) context, WebServices.mLoginUtility.getEmail(), meterialUtility.getCourse_name(), meterialUtility.getTitle(), meterialUtility.getMaterial_type(), true);
                new MaterialOpenController(context, mixPanelManager, mDatabase).openMaterial(meterialUtility, true, true, false);
            }
                /*if (meterialUtility.getMaterial_type().equalsIgnoreCase("Video")) {
                    Intent intent = new Intent(context, PlayVideo_Activity.class);
                    intent.putExtra("isSaved", false);
                    intent.putExtra("file_name", meterialUtility.getCourse_name());
                    intent.putExtra("course_id", meterialUtility.getCourse_id());
                    intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                    intent.putExtra("title", meterialUtility.getTitle());
                    intent.putExtra("desc", meterialUtility.getDescription());
                    intent.putExtra("pos", (0) + "");
                    intent.putExtra("type", meterialUtility.getMaterial_type());
                    intent.putExtra("file_url", meterialUtility.getMaterial_media_file_url());
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("link")) {

                    if (WebServices.isNetworkAvailable(context)) {
                        Intent intent = new Intent(context, Link_Activity.class);
                        intent.putExtra("name", meterialUtility.getTitle());
                        intent.putExtra("url", meterialUtility.getMaterial_media_file_url());
                        intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    } else {
                        Toast.makeText(context, "This file is not available offline. Please connect to internet.", Toast.LENGTH_LONG).show();
                    }

                } else {
                    new DownloadFiles(context).downloadFiles(meterialUtilityAl, meterialPos, isNext, true, true);
                }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callFlowingCourseModule(final Context context, MeterialUtility meterialUtility, boolean isNext) {
        int position = -1;
        MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(context);
      /*  DataBase mDatabase = DataBase.getInstance(context);*/
        ArrayList<ModulesUtility> moduleUtilityList = getModuleListFromModuleId(meterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            position = getModulePositionFromModuleList(moduleUtilityList, meterialUtility.getModule_id());

        try {
            int modulePos;
            if (isNext)
                modulePos = position + 1;
            else
                modulePos = position - 1;

            if (moduleUtilityList == null || moduleUtilityList.size() < modulePos)
                return;
            ModulesUtility modulesUtility = moduleUtilityList.get(modulePos);
            if (modulesUtility != null) {
                mixPanelManager.selectmaterial((Activity) context, WebServices.mLoginUtility.getEmail(),
                        modulesUtility.getCourse_name(), modulesUtility.getModule_name(), "", true);
                openNextModuleController((Activity) context, getCoursePosition(modulesUtility.getCourse_id()), modulesUtility,moduleUtilityList.get(position));
            }
                /*if (meterialUtility.getMaterial_type().equalsIgnoreCase("Video")) {
                    Intent intent = new Intent(context, PlayVideo_Activity.class);
                    intent.putExtra("isSaved", false);
                    intent.putExtra("file_name", meterialUtility.getCourse_name());
                    intent.putExtra("course_id", meterialUtility.getCourse_id());
                    intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                    intent.putExtra("title", meterialUtility.getTitle());
                    intent.putExtra("desc", meterialUtility.getDescription());
                    intent.putExtra("pos", (0) + "");
                    intent.putExtra("type", meterialUtility.getMaterial_type());
                    intent.putExtra("file_url", meterialUtility.getMaterial_media_file_url());
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("link")) {

                    if (WebServices.isNetworkAvailable(context)) {
                        Intent intent = new Intent(context, Link_Activity.class);
                        intent.putExtra("name", meterialUtility.getTitle());
                        intent.putExtra("url", meterialUtility.getMaterial_media_file_url());
                        intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    } else {
                        Toast.makeText(context, "This file is not available offline. Please connect to internet.", Toast.LENGTH_LONG).show();
                    }

                } else {
                    new DownloadFiles(context).downloadFiles(meterialUtilityAl, meterialPos, isNext, true, true);
                }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void openNextModuleController(Context mContext, int coursePos, ModulesUtility modulesUtility
            ,ModulesUtility lastModulesUtility) {
        Intent intent = new Intent(mContext, StartModuleFlowingCourse.class);
        intent.putExtra("position", coursePos);
        intent.putExtra("mod_id", modulesUtility.getModule_id());
        intent.putExtra("is_link", false);
        intent.putExtra("next_module", modulesUtility);
        intent.putExtra("last_module", lastModulesUtility);
        mContext.startActivity(intent);
        ((Activity)mContext).finish();
    }

    /**
     * It is used to download material and open from anywhere
     *
     * @param context
     * @param meterialId
     */
    public static void openMaterial(final Context context, String meterialId, boolean wannaFinish) {
        MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(context);
        DataBase mDatabase = DataBase.getInstance(context);
        MeterialUtility meterialUtility = getMaterialFromCourse(meterialId);
        DataBase dataBase = DataBase.getInstance(context);
        if (meterialUtility != null) {
            new MaterialOpenController(context, mixPanelManager, mDatabase).openMaterial(meterialUtility, true, wannaFinish, false);
        }
           /* if (meterialUtility.getTitle().startsWith("Hospital")) {
                *//*if (dataBase.getHospitalsData(WebServices.mLoginUtility.getUser_id()).equalsIgnoreCase("")) {
                    if (WebServices.isNetworkAvailable(context))
                        new DownloadFiles(context).getAllHospitals(meterialUtility.getTitle(), true);
                    else
                        Toast.makeText(context, "This file is not available offline. Please connect to internet.", Toast.LENGTH_LONG).show();
                } else {
                    new WebServices().getAllHospitals(dataBase.getHospitalsData(WebServices.mLoginUtility.getUser_id()));
                    Intent intent = new Intent(context, HospitalSearch_Activity.class);
                    intent.putExtra("title", meterialUtility.getTitle());
                    context.startActivity(intent);
                    if (WebServices.isNetworkAvailable(context))
                        new DownloadFiles(context).getAllHospitals(meterialUtility.getTitle(), false);
                }*//*
                String allHospitals = APIUtility.loadJSONFromAsset(context);
                if (!TextUtils.isEmpty(allHospitals)) {
                    new WebServices().getAllHospitals(allHospitals);
                    Intent intent = new Intent(context, HospitalSearch_Activity.class);
                    intent.putExtra("title", meterialUtility.getTitle());
                    context.startActivity(intent);
                }

                return;
            } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("Video")) {
                Intent intent = new Intent(context, PlayVideo_Activity.class);
                intent.putExtra("isSaved", false);
                intent.putExtra("file_name", meterialUtility.getCourse_name());
                intent.putExtra("course_id", meterialUtility.getCourse_id());
                intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                intent.putExtra("title", meterialUtility.getTitle());
                intent.putExtra("desc", meterialUtility.getDescription());
                intent.putExtra("pos", (0) + "");
                intent.putExtra("type", meterialUtility.getMaterial_type());
                intent.putExtra("file_url", meterialUtility.getMaterial_media_file_url());
                context.startActivity(intent);
                if (wannaFinish)
                    ((Activity) context).finish();
            } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("link")) {

                if (WebServices.isNetworkAvailable(context)) {
                    Intent intent = new Intent(context, Link_Activity.class);
                    intent.putExtra("name", meterialUtility.getTitle());
                    intent.putExtra("url", meterialUtility.getMaterial_media_file_url());
                    intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                    context.startActivity(intent);
                    if (wannaFinish)
                        ((Activity) context).finish();
                } else {
                    Toast.makeText(context, "This file is not available offline. Please connect to internet.", Toast.LENGTH_LONG).show();
                }

            } else {
                new DownloadFiles(context).downloadFiles(meterialUtility, true, wannaFinish);
            }*/
    }


    /**
     * It is used to download material and open from anywhere
     *
     * @param context
     * @param meterialUtility
     */
    public static void openMaterial(final Context context, MeterialUtility meterialUtility) {
        MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(context);
        DataBase mDatabase = DataBase.getInstance(context);
        if (meterialUtility != null) {
            new MaterialOpenController(context, mixPanelManager, mDatabase).openMaterial(meterialUtility, true, true, false);
        }

    }

    /**
     * It is used to get material from all courses list
     *
     * @param materialId
     * @return
     */
    public static MeterialUtility getMaterialFromCourse(String materialId) {
        int courseSize = HomeActivity.courseUtilities.size();
        for (int i = 0; i < courseSize; i++) {
            CourseUtility courseUtility = HomeActivity.courseUtilities.get(i);
            if (courseUtility != null && courseUtility.getModulesUtilityArrayList() != null) {
                int moduleSize = courseUtility.getModulesUtilityArrayList().size();
                for (int k = 0; k < moduleSize; k++) {
                    ModulesUtility modulesUtility = courseUtility.getModulesUtilityArrayList().get(k);
                    if (modulesUtility != null && modulesUtility.getMeterialUtilityArrayList() != null) {
                        ArrayList<MeterialUtility> meterialUtilities = modulesUtility.getMeterialUtilityArrayList();
                        int materialSize = meterialUtilities.size();
                        for (int l = 0; l < materialSize; l++) {
                            MeterialUtility meterialUtility = meterialUtilities.get(l);
                            if (meterialUtility.getMaterial_id().equals(materialId))
                                return meterialUtility;

                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * It is used to get module from all courses list
     *
     * @param moduleId
     * @return
     */
    public static ModulesUtility getModuleFromCourse(String moduleId) {
        int courseSize = HomeActivity.courseUtilities.size();
        for (int i = 0; i < courseSize; i++) {
            CourseUtility courseUtility = HomeActivity.courseUtilities.get(i);
            if (courseUtility != null && courseUtility.getModulesUtilityArrayList() != null) {
                int moduleSize = courseUtility.getModulesUtilityArrayList().size();
                for (int k = 0; k < moduleSize; k++) {
                    ModulesUtility modulesUtility = courseUtility.getModulesUtilityArrayList().get(k);
                    if (modulesUtility != null && modulesUtility.getMeterialUtilityArrayList() != null) {
                        if (modulesUtility.getModule_id().equalsIgnoreCase(moduleId)) {
                            modulesUtility.setModule_pos(k);
                            return modulesUtility;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * It is used to get module list from course
     *
     * @param moduleId
     * @return
     */
    public static ArrayList<ModulesUtility> getModuleListFromModuleId(String moduleId) {
        CourseUtility courseUtility = getCourseFromModuleId(moduleId);
        return courseUtility.getModulesUtilityArrayList();
    }


}
