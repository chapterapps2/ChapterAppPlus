package com.chaptervitamins.utility;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abcd on 4/25/2016.
 */
public class ModulesUtility implements Serializable {
    String module_id;
    //    String module_media_id;
    String course_id,course_name;
    String module_name;
    String module_image;
    int module_pos=0;
    //    String price="0";
//    String currency;
//    String author_name;
//    String info_title="";
    String info = "";
    //    String keywords;
    String sort_order;
    String is_incremented;
    ArrayList<ModulesUtility> subModuleArraylist = new ArrayList<>();
    String availability;
    String show_status;
    String totalPDF;
    String totalQuiz;
    String totalVideo;
    String totalAudio;
    String totalFlashCard;
    String totalElearning;
    String totalProgress = "0";
    String isTodo = "false";
    String isOnGoing = "false";
    String isOnComplete = "false";
    String course_size = "0";
    String total_New = "0", lastaccessed = "", is_flowing_course;
    ArrayList<MeterialUtility> meterialUtilityArrayList = new ArrayList<>();
    boolean isSelected = false;
    int completedMaterials;
    boolean isCompleted = false;
    String completion_per;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    /*public String getModule_media_id() {
        return module_media_id;
    }

    public void setModule_media_id(String module_media_id) {
        this.module_media_id = module_media_id;
    }*/

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getModule_name() {
        return module_name;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }

    public String getModule_image() {
        return module_image;
    }

    public void setModule_image(String module_image) {
        this.module_image = module_image;
    }

    /* public String getPrice() {
         return price;
     }

     public void setPrice(String price) {
         this.price = price;
     }
 */
    /*public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getInfo_title() {
        return info_title;
    }

    public void setInfo_title(String info_title) {
        this.info_title = info_title;
    }
*/
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    /* public String getKeywords() {
         return keywords;
     }

     public void setKeywords(String keywords) {
         this.keywords = keywords;
     }
 */
    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getIs_incremented() {
        return is_incremented;
    }

    public void setIs_incremented(String is_incremented) {
        this.is_incremented = is_incremented;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getShow_status() {
        return show_status;
    }

    public void setShow_status(String show_status) {
        this.show_status = show_status;
    }

    public ArrayList<MeterialUtility> getMeterialUtilityArrayList() {
        return meterialUtilityArrayList;
    }

    public void setMeterialUtilityArrayList(ArrayList<MeterialUtility> meterialUtilityArrayList) {
        this.meterialUtilityArrayList = meterialUtilityArrayList;
    }

    public String getTotalPDF() {
        return totalPDF;
    }

    public void setTotalPDF(String totalPDF) {
        this.totalPDF = totalPDF;
    }

    public String getTotalQuiz() {
        return totalQuiz;
    }

    public void setTotalQuiz(String totalQuiz) {
        this.totalQuiz = totalQuiz;
    }

    public String getTotalVideo() {
        return totalVideo;
    }

    public void setTotalVideo(String totalVideo) {
        this.totalVideo = totalVideo;
    }

    public String getTotalAudio() {
        return totalAudio;
    }

    public void setTotalAudio(String totalAudio) {
        this.totalAudio = totalAudio;
    }

    public String getTotalFlashCard() {
        return totalFlashCard;
    }

    public void setTotalFlashCard(String totalFlashCard) {
        this.totalFlashCard = totalFlashCard;
    }

    public String getTotalElearning() {
        return totalElearning;
    }

    public void setTotalElearning(String totalElearning) {
        this.totalElearning = totalElearning;
    }

    public String getIsTodo() {
        return isTodo;
    }

    public void setIsTodo(String isTodo) {
        this.isTodo = isTodo;
    }

    public String getTotalProgress() {
        return totalProgress;
    }

    public void setTotalProgress(String totalProgress) {
        this.totalProgress = totalProgress;
    }

    public String getIsOnGoing() {
        return isOnGoing;
    }

    public void setIsOnGoing(String isOnGoing) {
        this.isOnGoing = isOnGoing;
    }

    public String getIsOnComplete() {
        return isOnComplete;
    }

    public void setIsOnComplete(String isOnComplete) {
        this.isOnComplete = isOnComplete;
    }

    public String getCourse_size() {
        return course_size;
    }

    public void setCourse_size(String course_size) {
        this.course_size = course_size;
    }

    public String getTotal_New() {
        return total_New;
    }

    public void setTotal_New(String total_New) {
        this.total_New = total_New;
    }

    public String getLastaccessed() {
        return lastaccessed;
    }

    public void setLastaccessed(String lastaccessed) {
        this.lastaccessed = lastaccessed;
    }

    public boolean getIs_flowing_course() {
        if (!TextUtils.isEmpty(is_flowing_course) && is_flowing_course.equalsIgnoreCase("yes"))
            return true;
        return false;
    }

    public void setIs_flowing_course(String is_flowing_course) {
        this.is_flowing_course = is_flowing_course;
    }

    public int getCompletedMaterials() {
        return completedMaterials;
    }

    public void setCompletedMaterials(int completedMaterials) {
        this.completedMaterials = completedMaterials;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public String getCompletion_per() {
        return completion_per;
    }

    public void setCompletion_per(String completion_per) {
        this.completion_per = completion_per;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public ArrayList<ModulesUtility> getSubModuleArraylist() {
        return subModuleArraylist;
    }

    public void setSubModuleArraylist(ArrayList<ModulesUtility> subModuleArraylist) {
        this.subModuleArraylist = subModuleArraylist;
    }

    public int getModule_pos() {
        return module_pos;
    }

    public void setModule_pos(int module_pos) {
        this.module_pos = module_pos;
    }
}
