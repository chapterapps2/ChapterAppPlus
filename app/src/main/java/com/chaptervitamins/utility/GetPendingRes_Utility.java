package com.chaptervitamins.utility;

/**
 * Created by abcd on 6/8/2016.
 */
public class GetPendingRes_Utility {
    String pending_response_id;
    String user_id;
    String material_id;
    String response_status;
    String response_data;
    String add_by;
    String complete_per="0";

    public String getPending_response_id() {
        return pending_response_id;
    }

    public void setPending_response_id(String pending_response_id) {
        this.pending_response_id = pending_response_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public String getResponse_status() {
        return response_status;
    }

    public void setResponse_status(String response_status) {
        this.response_status = response_status;
    }

    public String getResponse_data() {
        return response_data;
    }

    public void setResponse_data(String response_data) {
        this.response_data = response_data;
    }

    public String getAdd_by() {
        return add_by;
    }

    public void setAdd_by(String add_by) {
        this.add_by = add_by;
    }

    public String getComplete_per() {
        return complete_per;
    }

    public void setComplete_per(String complete_per) {
        this.complete_per = complete_per;
    }
}
