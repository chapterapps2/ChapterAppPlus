package com.chaptervitamins.utility;

import java.util.ArrayList;

/**
 * Created by Anjali on 25-Apr-16.
 */
public class Quiz_Response_Util {
    String response_id;
    String user_id;
    String material_id;
    String title;
    String test_pattern;
    String course_name;
    String response_type;
    String result;
    String correct_question;
    String incorrect_question;
    String taken_device;
    String time_taken;
    String start_time;
    String finish_time;
    String added_on;
    String isPass="fail";
    boolean ishighest=false;
    ArrayList<Question_Response_util> mQuestionResponseArraylist=new ArrayList<Question_Response_util>();

    public ArrayList<Question_Response_util> getmQuestionResponseArraylist() {
        return mQuestionResponseArraylist;
    }

    public void setmQuestionResponseArraylist(ArrayList<Question_Response_util> mQuestionResponseArraylist) {
        this.mQuestionResponseArraylist = mQuestionResponseArraylist;
    }

    public String getResponse_id() {
        return response_id;
    }

    public void setResponse_id(String response_id) {
        this.response_id = response_id;
    }

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTest_pattern() {
        return test_pattern;
    }

    public void setTest_pattern(String test_pattern) {
        this.test_pattern = test_pattern;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getResponse_type() {
        return response_type;
    }

    public void setResponse_type(String response_type) {
        this.response_type = response_type;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCorrect_question() {
        return correct_question;
    }

    public void setCorrect_question(String correct_question) {
        this.correct_question = correct_question;
    }

    public String getIncorrect_question() {
        return incorrect_question;
    }

    public void setIncorrect_question(String incorrect_question) {
        this.incorrect_question = incorrect_question;
    }

    public String getTime_taken() {
        return time_taken;
    }

    public void setTime_taken(String time_taken) {
        this.time_taken = time_taken;
    }

    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

    public String getTaken_device() {
        return taken_device;
    }

    public void setTaken_device(String taken_device) {
        this.taken_device = taken_device;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getIsPass() {
        return isPass;
    }

    public void setIsPass(String isPass) {
        this.isPass = isPass;
    }

    public boolean ishighest() {
        return ishighest;
    }

    public void setIshighest(boolean ishighest) {
        this.ishighest = ishighest;
    }
}
