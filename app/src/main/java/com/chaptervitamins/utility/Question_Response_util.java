package com.chaptervitamins.utility;

import java.util.ArrayList;

/**
 * Created by Anjali on 28-Apr-16.
 */
public class Question_Response_util {
    String question_response_id;
    String question_id;
    String question_bank_id;
    String assign_question_id;
    ArrayList<String> answer_key;
    String marks;
    String response_id;
    String time_taken;

    public String getQuestion_response_id() {
        return question_response_id;
    }

    public void setQuestion_response_id(String question_response_id) {
        this.question_response_id = question_response_id;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getQuestion_bank_id() {
        return question_bank_id;
    }

    public void setQuestion_bank_id(String question_bank_id) {
        this.question_bank_id = question_bank_id;
    }

    public String getAssign_question_id() {
        return assign_question_id;
    }

    public void setAssign_question_id(String assign_question_id) {
        this.assign_question_id = assign_question_id;
    }

    public ArrayList<String> getAnswer_key() {
        return answer_key;
    }

    public void setAnswer_key(ArrayList answer_key) {
        this.answer_key = answer_key;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getResponse_id() {
        return response_id;
    }

    public void setResponse_id(String response_id) {
        this.response_id = response_id;
    }

    public String getTime_taken() {
        return time_taken;
    }

    public void setTime_taken(String time_taken) {
        this.time_taken = time_taken;
    }
}
