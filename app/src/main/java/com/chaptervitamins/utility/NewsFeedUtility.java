package com.chaptervitamins.utility;

import java.util.ArrayList;

/**
 * Created by Android on 7/16/2016.
 */

public class NewsFeedUtility {
    String notice_board_id;
    String title;
    String desc;
    String time;
    String total_fev = "0";
    String total_view = "0";
    String link = "";
    String image_url="";
    String file_url="";
    String file_type="";
    String start_date="";

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    String end_date="";
    boolean isVideo = false;

    public boolean isImage() {
        return isImage;
    }

    public void setIsImage(boolean isImage) {
        this.isImage = isImage;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    boolean isImage = false;
    ArrayList<NewsUtility> viewArrayList = new ArrayList<>();
    ArrayList<NewsUtility> likeArrayList = new ArrayList<>();

    public String getNotice_board_id() {
        return notice_board_id;
    }

    public void setNotice_board_id(String notice_board_id) {
        this.notice_board_id = notice_board_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotal_fev() {
        return total_fev;
    }

    public void setTotal_fev(String total_fev) {
        this.total_fev = total_fev;
    }

    public String getTotal_view() {
        return total_view;
    }

    public void setTotal_view(String total_view) {
        this.total_view = total_view;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }



    public ArrayList<NewsUtility> getViewArrayList() {
        return viewArrayList;
    }

    public void setViewArrayList(ArrayList<NewsUtility> viewArrayList) {
        this.viewArrayList = viewArrayList;
    }

    public ArrayList<NewsUtility> getLikeArrayList() {
        return likeArrayList;
    }

    public void setLikeArrayList(ArrayList<NewsUtility> likeArrayList) {
        this.likeArrayList = likeArrayList;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
