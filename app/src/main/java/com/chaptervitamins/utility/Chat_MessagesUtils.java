package com.chaptervitamins.utility;

import java.util.ArrayList;

/**
 * Created by Android on 7/1/2016.
 */

public class Chat_MessagesUtils {
    String forum_id;
    String group_id;
    String user_id;
    String firstname;
    String lastname;
    String email;
    String forum_title;
    String forum_description;
    String forum_type;
    String added_on;
    String media="";
    String media_type="";
    String photo;
    boolean isSection;
    String date="";
    ArrayList<CommentsUtils> commentsUtilsArrayList = new ArrayList<>();

    public String getForum_id() {
        return forum_id;
    }

    public void setForum_id(String forum_id) {
        this.forum_id = forum_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForum_title() {
        return forum_title;
    }

    public void setForum_title(String forum_title) {
        this.forum_title = forum_title;
    }

    public String getForum_description() {
        return forum_description;
    }

    public void setForum_description(String forum_description) {
        this.forum_description = forum_description;
    }

    public String getForum_type() {
        return forum_type;
    }

    public void setForum_type(String forum_type) {
        this.forum_type = forum_type;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ArrayList<CommentsUtils> getCommentsUtilsArrayList() {
        return commentsUtilsArrayList;
    }

    public void setCommentsUtilsArrayList(ArrayList<CommentsUtils> commentsUtilsArrayList) {
        this.commentsUtilsArrayList = commentsUtilsArrayList;
    }

    public boolean isSection() {
        return isSection;
    }

    public void setSection(boolean section) {
        isSection = section;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }
}
