package com.chaptervitamins.utility;

/**
 * Created by Android on 8/5/2016.
 */

public class NewsUtility {
    String id;
    String user_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
