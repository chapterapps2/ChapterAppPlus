package com.chaptervitamins.utility;

/**
 * Created by Android on 6/17/2016.
 */
public class SurveyUtils {
    String survey_id;
    String title;
    String description;
    String alloted_time;
    String sort_order;
    String last_updated_time="0000-00-00 00:00:00";
    int read=0;
    String isDownloadable="NO";

    public String getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(String survey_id) {
        this.survey_id = survey_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlloted_time() {
        return alloted_time;
    }

    public void setAlloted_time(String alloted_time) {
        this.alloted_time = alloted_time;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getLast_updated_time() {
        return last_updated_time;
    }

    public void setLast_updated_time(String last_updated_time) {
        this.last_updated_time = last_updated_time;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public String getIsDownloadable() {
        return isDownloadable;
    }

    public void setIsDownloadable(String isDownloadable) {
        this.isDownloadable = isDownloadable;
    }
}
