package com.chaptervitamins.utility;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by test on 19-12-2017.
 */
public class CustomDialog extends DialogFragment implements View.OnClickListener {
    Context mContext;
    String matId;
    String matType;
    RatingBar rateBar;
    EditText editText;
    WebServices webServices;
    boolean wanaFinish;
    private RatingListener mListener;

    public void setParamCustomDialog(Context mContext, String matId, boolean wanaFinish, RatingListener listener) {
        this.mContext = mContext;
        this.matId = matId;
        this.wanaFinish = wanaFinish;
        webServices = new WebServices();
        this.mListener = listener;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rating_screen_layout, container, false);
        getDialog().setTitle("Rate Us.");
        getDialog().setCanceledOnTouchOutside(false);
        Button doneBtn = (Button) view.findViewById(R.id.submitBtn);
        Button notNowBtn = (Button) view.findViewById(R.id.notNowBtn);
        rateBar = (RatingBar) view.findViewById(R.id.rateBar);
        rateBar.setNumStars(5);
        editText = (EditText) view.findViewById(R.id.feedEditText);
        doneBtn.setOnClickListener(this);
        notNowBtn.setOnClickListener(this);
      /*  if(com.chaptervitamins.newcode.utils.Constants.ORGANIZATION_ID.equalsIgnoreCase("25")) {
            ((Activity)mContext).finish();
            return null;
        }else {*/
          return view;
       // }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submitBtn) {
            if (rateBar.getRating() != 0.0) {
                ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("item_type", "MATERIAL"));
                nameValuePair.add(new BasicNameValuePair("item_id", matId));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
                nameValuePair.add(new BasicNameValuePair("value", rateBar.getRating() + ""));
                nameValuePair.add(new BasicNameValuePair("comment", editText.getText().toString()));
                new GenericApiCall(mContext, APIUtility.RateItem, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                    @Override
                    public void onSuccess(Object result) {
                        if (webServices.isValid((String) result)) {
                            try {
                                JSONObject jsonObject = new JSONObject((String) result);
                                if (jsonObject != null) {
                                    JSONObject dataObject = jsonObject.optJSONObject("data");
                                    if (dataObject != null) {
                                        String avgRating = dataObject.optString("average_rating");
                                        if (webServices != null) {
                                            webServices.updateRating(matId, avgRating, rateBar.getRating() + "");
                                            if (!((Activity) mContext).isFinishing()) {
                                                getDialog().dismiss();
                                            }
                                            if (wanaFinish)
                                                ((Activity) mContext).finish();
                                            else
                                                mListener.onRatingBack();
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Toast.makeText(mContext, "Your feedback successfully submitted..!!", Toast.LENGTH_LONG).show();


                        }
                    }

                    @Override
                    public void onError(com.chaptervitamins.nomination.networks.api.ErrorModel error) {
                        if (!((Activity) mContext).isFinishing())
                            Toast.makeText(mContext, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
                        if (!((Activity) mContext).isFinishing()) {
                            getDialog().dismiss();
                        }
                        if (wanaFinish)
                            ((Activity) mContext).finish();
                        else
                            mListener.onRatingBack();
                    }
                }).execute();
            } else
                Toast.makeText(mContext, "Please mark some rating!", Toast.LENGTH_SHORT).show();


        } else if (v.getId() == R.id.notNowBtn) {
            if (!((Activity) mContext).isFinishing()) {
                getDialog().dismiss();
            }
            if (wanaFinish)
                ((Activity) mContext).finish();
            else
                mListener.onRatingBack();
        }
    }
}