package com.chaptervitamins.utility;

/**
 * Created by Android on 12/23/2016.
 */

public class Notification_Material_Util {
    String course_id="";
    String material_id="";
    String notification_user_id;
    String notification_id;
    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public String getNotification_user_id() {
        return notification_user_id;
    }

    public void setNotification_user_id(String notification_user_id) {
        this.notification_user_id = notification_user_id;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }
}
