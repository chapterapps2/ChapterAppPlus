package com.chaptervitamins.utility;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abcd on 4/25/2016.
 */
public class CourseUtility implements Serializable{
    String course_id;
    String assign_course_id;
    String course_name;
    String about_course,assign_course_module_id;
    String image;
    String price;
    String currency,course_type,link;
//    String is_trending;
//    String is_featured;
    String sort_order;
    String show_status;
    String start_date;
    String end_date;
    String course_color;
    String course_size="0",completion_per;
    ArrayList<ModulesUtility> modulesUtilityArrayList=new ArrayList<>();
    ArrayList<SurveyUtils> surveyUtilses=new ArrayList<>();
    private int completedModules=0;

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getAssign_course_id() {
        return course_id;
    }

    public void setAssign_course_id(String assign_course_id) {
        this.assign_course_id = assign_course_id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getAbout_course() {
        return about_course;
    }

    public void setAbout_course(String about_course) {
        this.about_course = about_course;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

  /*  public String getIs_trending() {
        return is_trending;
    }

    public void setIs_trending(String is_trending) {
        this.is_trending = is_trending;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }*/

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getShow_status() {
        return show_status;
    }

    public void setShow_status(String show_status) {
        this.show_status = show_status;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getCourse_size() {
        return course_size;
    }

    public void setCourse_size(String course_size) {
        this.course_size = course_size;
    }

    public ArrayList<ModulesUtility> getModulesUtilityArrayList() {
        return modulesUtilityArrayList;
    }

    public void setModulesUtilityArrayList(ArrayList<ModulesUtility> modulesUtilityArrayList) {
        this.modulesUtilityArrayList = modulesUtilityArrayList;
    }

    public ArrayList<SurveyUtils> getSurveyUtilses() {
        return surveyUtilses;
    }

    public void setSurveyUtilses(ArrayList<SurveyUtils> surveyUtilses) {
        this.surveyUtilses = surveyUtilses;
    }

    public String getCourse_type() {
        return course_type;
    }

    public void setCourse_type(String course_type) {
        this.course_type = course_type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAssign_course_module_id() {
        return assign_course_module_id;
    }

    public void setAssign_course_module_id(String assign_course_module_id) {
        this.assign_course_module_id = assign_course_module_id;
    }

    public int getCompletedModules() {
        return completedModules;
    }

    public void setCompletedModules(int completedModules) {
        this.completedModules = completedModules;
    }

    public String getCompletion_per() {
        return completion_per;
    }

    public void setCompletion_per(String completion_per) {
        this.completion_per = completion_per;
    }

    public String getCourse_color() {
        return course_color;
    }

    public void setCourse_color(String course_color) {
        this.course_color = course_color;
    }
}
