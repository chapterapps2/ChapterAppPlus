package com.chaptervitamins.utility;

/**
 * Created by Android on 11/21/2016.
 */

public class Notification_Utility {
    String notification_user_id;
    String notification_id;
    String title;
    String message;
    String notification_type;
    String sent_time;
    String sent_status;
    String material_type="";
    String url="";
    public String getNotification_user_id() {
        return notification_user_id;
    }

    public void setNotification_user_id(String notification_user_id) {
        this.notification_user_id = notification_user_id;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }

    public String getSent_time() {
        return sent_time;
    }

    public void setSent_time(String sent_time) {
        this.sent_time = sent_time;
    }

    public String getSent_status() {
        return sent_status;
    }

    public void setSent_status(String sent_status) {
        this.sent_status = sent_status;
    }

    public String getMaterial_type() {
        return material_type;
    }

    public void setMaterial_type(String material_type) {
        this.material_type = material_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
