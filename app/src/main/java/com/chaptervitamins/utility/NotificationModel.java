package com.chaptervitamins.utility;

import android.content.Context;

/**
 * Created by Mohit Chauhan on 31 May, 2015
 * Email: mohitc@leewayhertz.com
 */
public class NotificationModel {

	public boolean isError;
	public Context context;
	public String notificationName;
	public Object requestObj;
	public Object responseObj;
}