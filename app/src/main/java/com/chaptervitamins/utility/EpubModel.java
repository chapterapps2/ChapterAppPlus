package com.chaptervitamins.utility;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by arun on 01/10/15.
 */
public class EpubModel implements Parcelable {
    public String topicName;
    public String topicLink;

    public EpubModel()
    {

    }

    protected EpubModel(Parcel in) {
        topicName = in.readString();
        topicLink = in.readString();
    }

    public static final Creator<EpubModel> CREATOR = new Creator<EpubModel>() {
        @Override
        public EpubModel createFromParcel(Parcel in) {
            return new EpubModel(in);
        }

        @Override
        public EpubModel[] newArray(int size) {
            return new EpubModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(topicName);
        parcel.writeString(topicLink);
    }
}
