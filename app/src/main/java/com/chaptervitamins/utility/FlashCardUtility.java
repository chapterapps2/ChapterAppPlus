package com.chaptervitamins.utility;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Devstringx on 5/17/2016.
 */
public class FlashCardUtility implements Parcelable {
    String flash_card_id;
    String question_name;
    String question_description;
    String answer;
    String correct_option;
    String is_answer_image = "0";
    String is_question_image = "0";
    String UserAns = "";
    String question_description_image_url;
    String answer_image_url;
    String questionNo;
    String time = "30";
    boolean isOpen = false;

    public FlashCardUtility() {
    }

    public FlashCardUtility(String flash_card_id, String question_name, String question_description, String answer, String correct_option, String is_answer_image,
                            String is_question_image, String userAns, String question_description_image_url, String answer_image_url, String questionNo, String time, boolean isOpen) {
        this.flash_card_id = flash_card_id;
        this.question_name = question_name;
        this.question_description = question_description;
        this.answer = answer;
        this.correct_option = correct_option;
        this.is_answer_image = is_answer_image;
        this.is_question_image = is_question_image;
        UserAns = userAns;
        this.question_description_image_url = question_description_image_url;
        this.answer_image_url = answer_image_url;
        this.questionNo = questionNo;
        this.time = time;
        this.isOpen = isOpen;
    }

    protected FlashCardUtility(Parcel in) {
        flash_card_id = in.readString();
        question_name = in.readString();
        question_description = in.readString();
        answer = in.readString();
        correct_option = in.readString();
        is_answer_image = in.readString();
        is_question_image = in.readString();
        UserAns = in.readString();
        question_description_image_url = in.readString();
        answer_image_url = in.readString();
        questionNo = in.readString();
        time = in.readString();
        isOpen = in.readByte() != 0;
    }

    public static final Creator<FlashCardUtility> CREATOR = new Creator<FlashCardUtility>() {
        @Override
        public FlashCardUtility createFromParcel(Parcel in) {
            return new FlashCardUtility(in);
        }

        @Override
        public FlashCardUtility[] newArray(int size) {
            return new FlashCardUtility[size];
        }
    };

    public String getFlash_card_id() {
        return flash_card_id;
    }

    public void setFlash_card_id(String flash_card_id) {
        this.flash_card_id = flash_card_id;
    }

    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }

    public String getQuestion_description() {
        return question_description;
    }

    public void setQuestion_description(String question_description) {
        this.question_description = question_description;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCorrect_option() {
        return correct_option;
    }

    public void setCorrect_option(String correct_option) {
        this.correct_option = correct_option;
    }

    public String getIs_answer_image() {
        return is_answer_image;
    }

    public void setIs_answer_image(String is_answer_image) {
        this.is_answer_image = is_answer_image;
    }

    public String getIs_question_image() {
        return is_question_image;
    }

    public void setIs_question_image(String is_question_image) {
        this.is_question_image = is_question_image;
    }

    public String getUserAns() {
        return UserAns;
    }

    public void setUserAns(String userAns) {
        UserAns = userAns;
    }

    public String getQuestion_description_image_url() {
        return question_description_image_url;
    }

    public void setQuestion_description_image_url(String question_description_image_url) {
        this.question_description_image_url = question_description_image_url;
    }

    public String getAnswer_image_url() {
        return answer_image_url;
    }

    public void setAnswer_image_url(String answer_image_url) {
        this.answer_image_url = answer_image_url;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(flash_card_id);
        parcel.writeString(question_name);
        parcel.writeString(question_description);
        parcel.writeString(answer);
        parcel.writeString(correct_option);
        parcel.writeString(is_answer_image);
        parcel.writeString(is_question_image);
        parcel.writeString(UserAns);
        parcel.writeString(question_description_image_url);
        parcel.writeString(answer_image_url);
        parcel.writeString(questionNo);
        parcel.writeString(time);
        parcel.writeByte((byte) (isOpen ? 1 : 0));
    }
}
