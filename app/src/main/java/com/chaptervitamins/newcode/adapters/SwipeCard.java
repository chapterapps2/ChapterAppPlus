package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeHead;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.mindorks.placeholderview.annotations.swipe.SwipeView;

/**
 * Created by janisharali on 19/08/16.
 */
@NonReusable
@Layout(R.layout.item_swipe_card)
public class SwipeCard {

    /*@View(R.id.profileImageView)
    ImageView profileImageView;*/
    private String question, answer, position;

    public SwipeCard(Context context, String question, String answer, String position) {
        this.answer = answer;
        this.question = question;
        this.position = position;
        this.context = context;
        /*this.animator_scale_down = animator_scale_down;
        this.animator_scale_up = animator_scale_up;*/
    }

    Context context;

    @View(R.id.swipe_card_layout)
    FrameLayout frameCard;

    @View(R.id.text_question)
    TextView questionTxt;

    @View(R.id.text_answer)
    TextView answerTxt;

    @View(R.id.text_question_number)
    TextView questionNumberTxt;

    @SwipeView
    android.view.View view;

    @Resolve
    public void onResolve() {
        questionTxt.setText(question);
        answerTxt.setText(answer);
        questionNumberTxt.setText(position);
    }

    @SwipeOut
    public void onSwipedOut() {
        Log.d("DEBUG", "onSwipedOut");
    }

    @SwipeCancelState
    public void onSwipeCancelState() {
        Log.d("DEBUG", "onSwipeCancelState");
    }

    @SwipeIn
    public void onSwipeIn() {
        Log.d("DEBUG", "onSwipedIn");
    }

    @SwipeInState
    public void onSwipeInState() {
        Log.d("DEBUG", "onSwipeInState");
    }

    @SwipeOutState
    public void onSwipeOutState() {
        Log.d("DEBUG", "onSwipeOutState");
    }

    @SwipeHead
    public void onSwipeHead() {
        //profileImageView.setBackgroundColor(Color.BLUE);
        Log.d("DEBUG", "onSwipeHead");
    }

    @Click(R.id.swipe_card_layout)
    public void onAnswerButtonClicked() {

    }
}
