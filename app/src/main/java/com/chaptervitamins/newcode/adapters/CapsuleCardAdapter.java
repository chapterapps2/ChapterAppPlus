package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.SwipeCardActivity;
import com.chaptervitamins.newcode.models.Capsule;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;

public class CapsuleCardAdapter extends ArrayAdapter<Capsule> {

    public CapsuleCardAdapter(Context context) {
        super(context, 0);
    }

    @NonNull
    @Override
    public View getView(int position, View contentView, @NonNull ViewGroup parent) {
        final ViewHolderQuiz holderQuiz;
        ViewHolderCoIS holderCoIS;
        ViewHolderVideo holderVideo;

        final Capsule capsule = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        assert capsule != null;
        if (contentView == null) {
            if (capsule.getMaterial_type().equalsIgnoreCase("QUIZ")) {

                contentView = inflater.inflate(R.layout.item_capsule_quiz
                        , parent, false);
                assert contentView != null;
                holderQuiz = new ViewHolderQuiz(contentView);
                holderQuiz.tvQuizQues.setText(capsule.getDescription());

                final ArrayAdapter<String> adapter = new ArrayAdapter<>(contentView.getContext(),
                        R.layout.text_answers, android.R.id.text1, capsule.getAnswers());

                holderQuiz.ansList.setSelection(1);
                holderQuiz.ansList.setAdapter(adapter);

                holderQuiz.ansList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        Toast.makeText(adapterView.getContext(), holderQuiz.ansList.getSelectedItemPosition()
                                , Toast.LENGTH_SHORT).show();
                        capsule.setUserAns((i+1) +"");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            } else if (capsule.getMaterial_type().equalsIgnoreCase("CONTENTINSHORT")) {

                contentView = inflater.inflate(R.layout.item_capsule_cois
                        , parent, false);
                holderCoIS = new ViewHolderCoIS(contentView);
                holderCoIS.title.setText(capsule.getTitle());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holderCoIS.description.setText(Html.fromHtml(capsule.getDescription()
                            , Html.FROM_HTML_MODE_COMPACT));
                } else
                    holderCoIS.description.setText(Html.fromHtml(capsule.getDescription()));

                Glide
                        .with(contentView.getContext())
                        .load(capsule.getMedia())
                        .centerCrop()
                        .placeholder(R.drawable.add_group_icon)
                        .into(holderCoIS.image);
            } else if (capsule.getMaterial_type().equalsIgnoreCase("VIDEO")) {
                contentView = inflater.inflate(R.layout.item_capsule_video
                        , parent, false);
                holderVideo = new ViewHolderVideo(contentView);
                holderVideo.initData();
                if (Util.SDK_INT > 23) {
                    if (!(capsule.getMaterialMediaUrl().equals("") || capsule.getMaterialMediaUrl() == null))
                        holderVideo.initializePlayer(capsule.getMaterialMediaUrl());
                }
            }
        }
        return contentView;
    }

    private static class ViewHolderVideo implements SwipeCardActivity.OnVideoPlayerStopped {
        SimpleExoPlayerView videoView;
        SimpleExoPlayer player;
        Timeline.Window window;
        DefaultTrackSelector trackSelector;
        DataSource.Factory mediaDataSourceFactory;
        boolean shouldAutoPlay;
        BandwidthMeter bandwidthMeter;
        ImageView ivHideControllerButton;

        public ViewHolderVideo(View view) {
            this.videoView = (SimpleExoPlayerView) view.findViewById(R.id.video_view_capsule);
        }

        private void initData() {
            shouldAutoPlay = false;
            bandwidthMeter = new DefaultBandwidthMeter();
            mediaDataSourceFactory = new DefaultDataSourceFactory(videoView.getContext(), Util.getUserAgent(videoView.getContext(),
                    "mediaPlayerSample"), (TransferListener<? super DataSource>) bandwidthMeter);
            window = new Timeline.Window();
//        ivHideControllerButton = (ImageView) findViewById(R.id.exo_controller);
        }

        private void initializePlayer(String filepath) {
            videoView.requestFocus();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            player = ExoPlayerFactory.newSimpleInstance(videoView.getContext(), trackSelector);
            videoView.setPlayer(player);
            player.setPlayWhenReady(shouldAutoPlay);
            DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(filepath),
                    mediaDataSourceFactory, extractorsFactory, null, null);
            player.prepare(mediaSource);
            if (ivHideControllerButton != null)
                ivHideControllerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        videoView.hideController();
                    }
                });
        }

        private void releasePlayer() {
            if (player != null) {
                shouldAutoPlay = player.getPlayWhenReady();
                player.release();
                player = null;
                trackSelector = null;
            }
        }

        @Override
        public void onPlayerStopped() {
            releasePlayer();
        }
    }

    private static class ViewHolderCoIS {
        public TextView title;
        public TextView description;

        public ImageView image;

        public ViewHolderCoIS(View view) {
            this.title = (TextView) view.findViewById(R.id.text_capsule_title_cois);
            this.description = (TextView) view.findViewById(R.id.text_capsule_description_cois);
            this.image = (ImageView) view.findViewById(R.id.image_capsule_cois);
        }

    }

    private static class ViewHolderQuiz {
        public TextView tvQuizQues, tvAns1, tvAns2, tvAns3;

        ListView ansList;

        public ViewHolderQuiz(View view) {
            tvQuizQues = (TextView) view.findViewById(R.id.text_capsule_ques);

            ansList = (ListView) view.findViewById(R.id.list_answer);
            tvAns1 = (TextView) view.findViewById(R.id.tv1);
            tvAns2 = (TextView) view.findViewById(R.id.tv2);
            tvAns3 = (TextView) view.findViewById(R.id.tv3);
        }

    }
}
