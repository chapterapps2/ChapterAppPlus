package com.chaptervitamins.newcode.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.Suggestions.Suggestion_Main_Activity;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.home.AllGroupsActivity;
import com.chaptervitamins.home.AssessmentListActivity;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.CourseDashboardActivity;
import com.chaptervitamins.newcode.activities.EventCalendarActivity;
import com.chaptervitamins.newcode.activities.IntroductionActivity;
import com.chaptervitamins.newcode.activities.MaterialsLeaderboardActivity;
import com.chaptervitamins.newcode.activities.ModuleActivity;
import com.chaptervitamins.newcode.activities.MultiModuleActivity;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.play_video.Link_Activity;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by tanuj on 20-11-2017.
 */
public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<CourseUtility> mCourseList;
    private DataBase mDatabase;
    private WebServices webServices;
    private MeterialUtility meterialUtility;
    private View view;

    public CourseAdapter(ArrayList<CourseUtility> mCourseList) {
        this.mCourseList = mCourseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
            this.mDatabase = DataBase.getInstance(mContext);
            webServices = new WebServices();
        }
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("34") &&
                Constants.BRANCH_ID.equalsIgnoreCase("57")) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_course_temp3, parent, false);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_course, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            CourseUtility courseUtility = mCourseList.get(position);
            if (courseUtility != null) {
                holder.tvCourseName.setText(courseUtility.getCourse_name());
//                if (position % 2 != 0) {
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                            LinearLayout.LayoutParams.MATCH_PARENT,
//                            LinearLayout.LayoutParams.WRAP_CONTENT
//                    );
//                    params.setMargins(0, 5, 10, 5);
//                    holder.itemView.setLayoutParams(params);
//                }
                ViewGroup.MarginLayoutParams marginLayoutParams =
                        (ViewGroup.MarginLayoutParams) holder.itemView.getLayoutParams();
                marginLayoutParams.setMargins(10, 10, 10, 10);
                holder.itemView.setLayoutParams(marginLayoutParams);
                if (!Constants.ORGANIZATION_ID.equalsIgnoreCase("34") &&
                        !Constants.BRANCH_ID.equalsIgnoreCase("57")) {
                    if (position == mCourseList.size() - 1 && position % 2 == 0) {
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(280, 180);
                        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        holder.ivCourse.setLayoutParams(params);
                    } else
                        holder.ivCourse.setLayoutParams(holder.ivCourse.getLayoutParams());
                }
                if (!TextUtils.isEmpty(courseUtility.getImage()))
                    new Picasso.Builder(mContext)
//                            .downloader(new OkHttpDownloader(mContext, Integer.MAX_VALUE))
                            .build().load(courseUtility.getImage()).noPlaceholder().error(R.drawable.def).into(holder.ivCourse);
                else
                    setImage(courseUtility.getCourse_type(), holder.ivCourse);

            }

        }

    }

    private void setImage(String materialType, ImageView imageView) {
        switch (materialType.toUpperCase()) {
            case AppConstants.CourseType.COURSE:
                imageView.setImageResource(R.drawable.def);
                break;
            case AppConstants.CourseType.LINK:
                imageView.setImageResource(R.drawable.link);
                break;
            case AppConstants.CourseType.NEWSFEED:
                imageView.setImageResource(R.drawable.news_update);
                break;
            case AppConstants.CourseType.SUGGESTION_QUERY:
                imageView.setImageResource(R.drawable.start_icon_suggestion);
                break;
            case AppConstants.CourseType.DISCUSSION:
                imageView.setImageResource(R.drawable.discussion);
                break;
            case AppConstants.CourseType.SURVEY:
                imageView.setImageResource(R.drawable.survey);
                break;
            default:
                imageView.setImageResource(R.drawable.def);
                break;
        }
    }

    private void selectItems(CourseUtility courseUtility, int adapterPosition) {
        MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(mContext);
        mixPanelManager.selectCourse((Activity) mContext, WebServices.mLoginUtility.getEmail(), courseUtility.getCourse_name());
        Intent intent = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        switch (courseUtility.getCourse_type().toUpperCase()) {
            case AppConstants.CourseType.DISCUSSION:
                intent = new Intent(mContext, AllGroupsActivity.class);
                intent.putExtra("name", courseUtility.getCourse_name());
                intent.putExtra("desc", courseUtility.getAbout_course());
                intent.putExtra("date", courseUtility.getEnd_date());
                mContext.startActivity(intent);
                break;
            case AppConstants.CourseType.SUGGESTION_QUERY:
                if (WebServices.isNetworkAvailable(mContext)) {
                    intent = new Intent(mContext, Suggestion_Main_Activity.class);
                    intent.putExtra("loginscreen", "");
                    mContext.startActivity(intent);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
                break;
            case AppConstants.CourseType.SNAPSHOT:
                intent = new Intent(mContext, CourseDashboardActivity.class);
                mContext.startActivity(intent);
                break;
            case AppConstants.CourseType.EVENT:
                intent = new Intent(mContext, EventCalendarActivity.class);
                mContext.startActivity(intent);
                break;
            case AppConstants.CourseType.LEADERBOARD:
                intent = new Intent(mContext, MaterialsLeaderboardActivity.class);
                mContext.startActivity(intent);
                break;
            case AppConstants.CourseType.ASSESSMENT:
                intent = new Intent(mContext, AssessmentListActivity.class);
                mContext.startActivity(intent);
                break;
            case AppConstants.CourseType.LINK:
                try {
                    if (courseUtility.getModulesUtilityArrayList() != null && courseUtility.getModulesUtilityArrayList().size() > 0) {
                        if (courseUtility.getModulesUtilityArrayList().get(0).getMeterialUtilityArrayList().size() > 1) {
                            intent = new Intent(mContext, ModuleActivity.class);
                            intent.putExtra("course", courseUtility);
                            intent.putExtra("position", adapterPosition);
                            intent.putExtra("is_link", true);
                            mContext.startActivity(intent);
                        } else {
                            MeterialUtility meterialUtility = courseUtility.getModulesUtilityArrayList().get(0).getMeterialUtilityArrayList().get(0);
                            meterialUtility.setMaterial_media_file_url(courseUtility.getLink());
                            if (!TextUtils.isEmpty(meterialUtility.getLinkOpenOutside()) && meterialUtility.getLinkOpenOutside().equalsIgnoreCase("YES")) {
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(meterialUtility.getMaterial_media_file_url()));
                                mContext.startActivity(i);
                            } else {
                                meterialUtility.setMaterialStartTime(df.format(c.getTime()));
                                intent = new Intent(mContext, Link_Activity.class);
                                intent.putExtra("meterial_utility", meterialUtility);
                                mContext.startActivity(intent);
                            }
                        }

                    } else {
                        if (WebServices.isNetworkAvailable(mContext)) {
                            intent = new Intent(mContext, Link_Activity.class);
                            intent.putExtra("loginscreen", "");
                            intent.putExtra("url", courseUtility.getLink());
                            intent.putExtra("name", courseUtility.getCourse_name());
                            mContext.startActivity(intent);
                        } else {
                            Toast.makeText(mContext, mContext.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case AppConstants.CourseType.GEO_AUDIT:
                meterialUtility = courseUtility.getModulesUtilityArrayList().get(0).getMeterialUtilityArrayList().get(0);
                meterialUtility.setMaterial_media_file_url(courseUtility.getLink());
                meterialUtility.setMaterialStartTime(df.format(c.getTime()));
                Utils.askLocationPermission(mContext, meterialUtility);

                break;
            case AppConstants.CourseType.ZOOM:
                try {
                    intent = mContext.getPackageManager().getLaunchIntentForPackage("us.zoom.diyaayaad.zoom1");
                    if (intent != null) {
                        mContext.startActivity(intent);//null pointer check in case package name was not found
                    } else
                        Toast.makeText(mContext, "Sorry! You cannot use this feature", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(mContext, "Sorry! You cannot use this feature", Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.CourseType.REGISTER:
                String contact_admin = "contact@com.chapterapps.net";
                if (!APIUtility.organizationModel.getAdminEmail().equalsIgnoreCase(""))
                    contact_admin = APIUtility.organizationModel.getAdminEmail();
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                emailIntent.setType("plain/text");

                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                        new String[]{contact_admin});

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Query for Admission");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Hey, I want to talk to you admission team \n Thanks !\n\n\n\n "
                        + WebServices.mLoginUtility.getFirstname() + "\n " + WebServices.mLoginUtility.getEmail());

                mContext.startActivity(Intent.createChooser(
                        emailIntent, "Send mail..."));
                break;

            case AppConstants.CourseType.COURSE:
                mContext.startActivity(new Intent(mContext, IntroductionActivity.class)
                        .putExtra("module", courseUtility.getModulesUtilityArrayList().get(0)));
                break;
            default:
                openMaterial(courseUtility, adapterPosition);
                break;

        }

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.LOCATION_PERM:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (meterialUtility != null)
                        Utils.askLocationPermission(mContext, meterialUtility);
                } else {
                    Toast.makeText(mContext, "You cannot access this without allow your location permission!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 101:
                if (meterialUtility != null)
                    Utils.askLocationPermission(mContext, meterialUtility);
                break;
        }
    }
        /*else {
            Toast.makeText(mContext, "You cannot access this without allow your location permission!", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void openMaterial(CourseUtility courseUtility, int adapterPosition) {
        Intent intent = null;
        if (courseUtility != null) {
            ArrayList<ModulesUtility> modulesUtilities = courseUtility.getModulesUtilityArrayList();
            if (modulesUtilities != null && modulesUtilities.size() == 1 && modulesUtilities.get(0) != null
                    && modulesUtilities.get(0).getMeterialUtilityArrayList() != null &&
                    modulesUtilities.get(0).getMeterialUtilityArrayList().size() == 1 &&
                    !modulesUtilities.get(0).getMeterialUtilityArrayList().get(0).getMaterial_type().
                            equalsIgnoreCase(AppConstants.MaterialType.QUIZ) && !modulesUtilities.get(0).getMeterialUtilityArrayList().get(0).getMaterial_type().
                    equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
                new MaterialOpenController(mContext, MixPanelManager.getInstance(), mDatabase).openMaterial(modulesUtilities.get(0).getMeterialUtilityArrayList().get(0), true, false, true);
            } else if (modulesUtilities != null && modulesUtilities.size() == 1) {
                intent = new Intent(mContext, ModuleActivity.class);
                intent.putExtra("course", courseUtility);
                intent.putExtra("position", adapterPosition);
                intent.putExtra("is_link", false);
                mContext.startActivity(intent);
                ((Activity) mContext).overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
            } else {
                intent = new Intent(mContext, MultiModuleActivity.class);
                intent.putExtra("course", courseUtility);
                intent.putExtra("position", adapterPosition);
                intent.putExtra("is_link", false);
                mContext.startActivity(intent);
                ((Activity) mContext).overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
            }

        }
    }

    @Override
    public int getItemCount() {
        return mCourseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCourseName;
        private ImageView ivCourse;
        private LinearLayout llCourse;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCourseName = (TextView) itemView.findViewById(R.id.tv_course_name);
            ivCourse = (ImageView) itemView.findViewById(R.id.iv_course);
            llCourse = (LinearLayout) itemView.findViewById(R.id.ll_course);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() >= 0)
                        selectItems(mCourseList.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }
}
