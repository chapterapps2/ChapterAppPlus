package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.fragments.NavigationDrawerFragment;
import com.chaptervitamins.newcode.models.NavDrawerModel;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by VijayAntil on 29/03/17.
 */
public class NavDrawerAdapter extends RecyclerView.Adapter<NavDrawerAdapter.ViewHolder> {
    private ArrayList<NavDrawerModel> alNavDrawerModels;
    private Context mContext;
    private OnDrawerItemSelectedListener mListener;
    private static final int TYPE_PROFILE = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    public NavDrawerAdapter(ArrayList<NavDrawerModel> alNavDrawerModels, NavigationDrawerFragment navigationDrawerFragment) {
        this.alNavDrawerModels = alNavDrawerModels;
        mListener = navigationDrawerFragment;
    }

    @Override
    public NavDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (this.mContext == null) {
            this.mContext = parent.getContext();
        }
        View view = null;
        switch(viewType){
            case TYPE_PROFILE:
                view = LayoutInflater.from(mContext).inflate(R.layout.drawer_header_layout, parent, false);
                break;
            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_drawer_layout, parent, false);
                break;
            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_footer_layout, parent, false);
                break;
        }
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final NavDrawerAdapter.ViewHolder holder, final int position) {
        switch (holder.holderType) {
            case TYPE_PROFILE:
                String profilePicUrl = WebServices.mLoginUtility.getPhoto();
                if (!TextUtils.isEmpty(profilePicUrl) && !WebServices.mLoginUtility.getPhoto().contains("no-image"))
                    Picasso.with(mContext).load(profilePicUrl).placeholder(R.drawable.profile).error(R.drawable.profile).resize(90,90).into(holder.ivProfile);
                else {
                    holder.tvUsername.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(WebServices.mLoginUtility.getFirstname()) && WebServices.mLoginUtility.getFirstname().length() > 1)
                        holder.tvUsername.setText(WebServices.mLoginUtility.getFirstname().substring(0, 1));
                    if (TextUtils.isEmpty(Constants.BRANCH_ID))
                        holder.tvUsername.setBackground(Utils.createShapeByColor(Utils.getColorPrimary(), 270, 0, R.color.colorPrimary));
                    else
                        holder.tvUsername.setBackground(Utils.createShapeByColor(R.color.login_bg, 270, 0, R.color.login_bg));
                }
                if (!TextUtils.isEmpty(WebServices.mLoginUtility.getFirstname()))
                    holder.tvName.setText(WebServices.mLoginUtility.getFirstname());
                if(Constants.ORGANIZATION_ID.equalsIgnoreCase("25")){
                    holder.tvDesignation.setVisibility(View.GONE);
                }else{
                    holder.tvDesignation.setText(WebServices.mLoginUtility.getBranch_name());
                }

                break;
            case TYPE_ITEM:
                if (alNavDrawerModels != null) {
                    NavDrawerModel navDrawerModel = alNavDrawerModels.get(position - 1);
                    if (navDrawerModel != null) {
                        holder.tvItemName.setText(navDrawerModel.getTitle());
                        holder.ivItem.setImageResource(navDrawerModel.getImageId());
                    }
                }
                break;
            case TYPE_FOOTER:
//                holder.tvVersion.setText("Ver : "+ BuildConfig.VERSION_NAME);
                holder.tvVersion.setText("Ver : "+ Constants.APP_VERSION);
                holder.tvPoweredBy.setText(mContext.getString(R.string.poweredby));
                break;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDrawerModel navDrawerModel = null;
                switch (holder.holderType) {
                    case TYPE_PROFILE:
                         navDrawerModel = new NavDrawerModel("1","My Profile",0);

                        if (navDrawerModel != null) {
                            mListener.onDrawerItemSelected(navDrawerModel);
                        }
                        break;

                    case TYPE_ITEM:
                        navDrawerModel = alNavDrawerModels.get(position - 1);
                        if (navDrawerModel != null) {
                            mListener.onDrawerItemSelected(navDrawerModel);
                        }
                        break;
                }
            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionProfile(position))
            return TYPE_PROFILE;
        else if (position == alNavDrawerModels.size() + 1)
            return TYPE_FOOTER;

        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return alNavDrawerModels.size() + 2;
    }

    /**
     * it is used to get proifieposition
     *
     * @param position
     * @return
     */
    private boolean isPositionProfile(int position) {
        return position == 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivProfile, ivItem;
        private TextView tvName, tvItemName, tvDesignation,tvVersion,tvPoweredBy,tvUsername;
        private int holderType;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            switch(viewType){
                case TYPE_PROFILE:
                    ivProfile = (ImageView) itemView.findViewById(R.id.iv_profile);
                    tvName = (TextView) itemView.findViewById(R.id.tv_profile_name);
                    tvUsername = (TextView) itemView.findViewById(R.id.tv_user_name);
                    tvDesignation = (TextView) itemView.findViewById(R.id.tv_profile_designation);
                    holderType = TYPE_PROFILE;
                    break;
                case TYPE_ITEM:
                    ivItem = (ImageView) itemView.findViewById(R.id.iv_item_drawer);
                    tvItemName = (TextView) itemView.findViewById(R.id.tv_item_drawer);
                    holderType = TYPE_ITEM;
                    break;
                case TYPE_FOOTER:
                    tvVersion = (TextView) itemView.findViewById(R.id.version);
                    tvPoweredBy = (TextView) itemView.findViewById(R.id.tv_powered_by);
                    holderType = TYPE_FOOTER;
                    break;
            }

        }


    }

    // HomeActivity Activity must implement this interface
    public interface OnDrawerItemSelectedListener {
        void onDrawerItemSelected(NavDrawerModel model);
    }


}
