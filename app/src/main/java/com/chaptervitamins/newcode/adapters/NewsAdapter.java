package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.play_video.Link_Activity;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 21-11-2017.
 */
public class NewsAdapter extends PagerAdapter {
    private Context mContext;
    private int[] mResources;
    RelativeLayout cardRelative;
    TextView title_txt;
    ImageView logo_img;
    ArrayList<MeterialUtility> newsFeedUtilities;
    TextView moreTxt;
    private MeterialUtility meterialUtility;

    public NewsAdapter(ArrayList<MeterialUtility> newsFeedUtilities,
                       Context mContext) {
        this.mContext = mContext;
        this.newsFeedUtilities = newsFeedUtilities;
    }

    @Override
    public int getCount() {
        return newsFeedUtilities.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_news_list_view, container, false);
        RelativeLayout linearParentLayout = (RelativeLayout) convertView.findViewById(R.id.linearParentLayout);
        LinearLayout llMaterial = (LinearLayout) convertView.findViewById(R.id.ll_material);
        LinearLayout materialLiLayout = (LinearLayout) convertView.findViewById(R.id.materialLiLayout);
        ImageView imageCal = (ImageView) convertView.findViewById(R.id.imageCal);
        TextView calTextView = (TextView) convertView.findViewById(R.id.calTextView);
        TextView timerTextView = (TextView) convertView.findViewById(R.id.timerTextView);
        TextView titleTextView = (TextView) convertView.findViewById(R.id.titleTextView);
        TextView bottomTextView = (TextView) convertView.findViewById(R.id.bottomTextView);
        ImageView ivNews = (ImageView) convertView.findViewById(R.id.iv_news);
        LinearLayout linearDuration = (LinearLayout) convertView.findViewById(R.id.ll_duration);
       /* logo_img = (ImageView) convertView.findViewById(R.id.logo_img);
        title_txt = (TextView) convertView.findViewById(R.id.title_txt);

        cardRelative = (RelativeLayout) convertView.findViewById(R.id.card_ll);

        title_txt.setText(Html.fromHtml(newsFeedUtilities.get(position).getTitle()));
        logo_img.setImageResource(R.mipmap.banner3);*/
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("38") &&
                Constants.BRANCH_ID.equalsIgnoreCase("56")) {
            if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.quiz).resize(200,200).error(R.drawable.quiz).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.quiz);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.VIDEO)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.video).resize(200,200).error(R.drawable.video).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.video);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.SURVEY)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.survey).resize(200,200).error(R.drawable.survey).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.survey);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.FLASHCARD)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.flashcard).resize(200,200).error(R.drawable.flashcard).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.flashcard);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.scorm).
                            resize(200,200).error(R.drawable.scorm).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.scorm);
                }

            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.FULL_TEXT)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.epub).
                            resize(200,200).error(R.drawable.epub).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.epub);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.PDF)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.pdf).
                            resize(200,200).error(R.drawable.pdf).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.pdf);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.AUDIO)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.audio).
                            resize(200,200).error(R.drawable.audio).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.audio);
                }
            }else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.MULTIMEDIAQUIZ)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.quiz).
                            resize(200,200).error(R.drawable.quiz).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.quiz);
                }
            }else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.LINK)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.link).
                            resize(200,200).error(R.drawable.link).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.link);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.CONTENTINSHORT)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.content).
                            resize(200,200).error(R.drawable.content).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.content);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.FLASH)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.link).
                            resize(200,200).error(R.drawable.link).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.link);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.IMAGECARD)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.flashcard_icon).
                            resize(200,200).error(R.drawable.flashcard_icon).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.flashcard_icon);
                }
            } else if (newsFeedUtilities.get(position).getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.EPUB)) {
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.epub).
                            resize(200,200).error(R.drawable.epub).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.epub);
                }

            }else{
                if (!TextUtils.isEmpty(newsFeedUtilities.get(position).getMaterial_image())) {
                    Picasso.with(mContext).load(newsFeedUtilities.get(position).getMaterial_image()).placeholder(R.drawable.link).
                            resize(200,200).error(R.drawable.link).into(imageCal);
                } else {
                    imageCal.setImageResource(R.drawable.link);
                }
            }

        }else{
            materialLiLayout.setBackgroundResource(R.drawable.rounded_border);
        }
        if (newsFeedUtilities != null && newsFeedUtilities.size() > 0) {
            meterialUtility = newsFeedUtilities.get(position);
            if (meterialUtility.isNews()) {
                titleTextView.setVisibility(View.GONE);
                llMaterial.setVisibility(View.GONE);
//                bottomTextView.setText(Html.fromHtml(meterialUtility.getDescription()));
                ivNews.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(meterialUtility.getThumbnailUrl()))
                    Picasso.with(mContext).load(meterialUtility.getThumbnailUrl()).placeholder(R.drawable.default_news).error(R.drawable.default_news).into(ivNews);
            } else {
                bottomTextView.setVisibility(View.GONE);
                llMaterial.setVisibility(View.VISIBLE);
                ivNews.setVisibility(View.GONE);
                titleTextView.setText(Html.fromHtml(meterialUtility.getTitle()));
                if (!TextUtils.isEmpty(meterialUtility.getStart_date()) && !TextUtils.isEmpty(meterialUtility.getEnd_date()))
                    calTextView.setText(Utils.getQuizStartDateNewFormat(meterialUtility.getStart_date()) + " to " + Utils.getQuizEndDateNewFormat(meterialUtility.getEnd_date()));
                if (!TextUtils.isEmpty(meterialUtility.getAlloted_time()) && !meterialUtility.getAlloted_time().equals("0"))
                    timerTextView.setText("Duration - " + meterialUtility.getAlloted_time() + " mins");
                else {
                    convertView.findViewById(R.id.ll_duration).setVisibility(View.GONE);
//                timerTextView.setVisibility(View.GONE);
                }

            }


        }
        linearParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meterialUtility = newsFeedUtilities.get(position);
                if (meterialUtility != null) {
                    if (meterialUtility.isNews()) {
                        if (!TextUtils.isEmpty(meterialUtility.getMaterial_type()) && meterialUtility.getMaterial_type().equalsIgnoreCase("IMAGE")) {
                            new MaterialOpenController(mContext, MixPanelManager.getInstance(),
                                    DataBase.getInstance(mContext)).openActivity(meterialUtility, true, false);
                        } else {
                            if (WebServices.isNetworkAvailable(mContext)) {
                                Intent intent = new Intent(mContext, Link_Activity.class);
                                intent.putExtra("meterial_utility", meterialUtility);
                                mContext.startActivity(intent);
                            } else
                                Toast.makeText(mContext, R.string.no_internet, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        meterialUtility = FlowingCourseUtils.getMaterialByMaterialId(meterialUtility.getMaterial_id());
                        new MaterialOpenController(mContext, MixPanelManager.getInstance(),
                                DataBase.getInstance(mContext)).openMaterial(meterialUtility, true, false, false);
                    }


                }


            }
        });
        /*moreTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (newsFeedUtilities.get(position).l().equalsIgnoreCase("")) return;
                //                    String type="pdf";
                //                    if (newsFeedUtilities.get(position).isVideo()||newsFeedUtilities.get(position).isImage())type="";
                //                    Intent intent = new Intent(getActivity(), NewsFeedDetail_Activity.class);
                //                    intent.putExtra("link", newsFeedUtilities.get(position).getLink());
                //                    intent.putExtra("title", newsFeedUtilities.get(position).getTitle());
                //                    intent.putExtra("type",type);
                //                    startActivity(intent);
                Intent intent = new Intent(mContext, NewsFeedFragment.class);

                mContext.startActivity(intent);
                   // ...Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(newsFeedUtilities.get(position).getLink()));
                    mContext.startActivity(browserIntent);
                    int view = Integer.parseInt(newsFeedUtilities.get(position).getTotal_view()) + 1;
                    newsFeedUtilities.get(position).setTotal_view(view + "");...*//*
                ///..  new getLikeView().execute(newsFeedUtilities.get(position).getNotice_board_id(),"view");
            }
        });*/
           /* play_video_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra("file_url", newsFeedUtilities.get(position).getVideoLink());
                    intent.putExtra("isSaved", false);
                    mContext.startActivity(intent);
                    int view = Integer.parseInt(newsFeedUtilities.get(position).getTotal_view()) + 1;
                    newsFeedUtilities.get(position).setTotal_view(view + "");
                    ///. new getLikeView().execute(newsFeedUtilities.get(position).getNotice_board_id(),"view");
                }
            });*/

        container.addView(convertView);
        return convertView;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
