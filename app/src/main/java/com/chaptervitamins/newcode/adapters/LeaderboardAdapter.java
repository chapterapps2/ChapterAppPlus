package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.ReadResponseUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by test on 29-12-2017.
 */
public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<ReadResponseUtility> readResponseUtilities;
    private DataBase mDatabase;
    private WebServices webServices;
    ViewGroup parent;

    public LeaderboardAdapter(ArrayList<ReadResponseUtility> readResponseUtilities) {
        this.readResponseUtilities = readResponseUtilities;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        if (mContext == null) {
            mContext = parent.getContext();
            this.mDatabase = DataBase.getInstance(mContext);
            webServices = new WebServices();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_leaderboard, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            ReadResponseUtility readResponseUtility = readResponseUtilities.get(position);
            if (readResponseUtility != null) {
                holder.tvRank.setText(position + 1 + "");
                holder.tvScore.setText(readResponseUtility.getResult());
                holder.tvUser.setText(readResponseUtility.getFirstname());
                holder.tvTimeTaken.setText(Utils.diffDate(Long.parseLong(readResponseUtility.getTime_taken())));
                if (readResponseUtility.getUser_id().equals(WebServices.mLoginUtility.getUser_id())) {
                    if (Utils.getColorPrimary() != 0)
                        holder.itemView.setBackgroundColor(Utils.getColorPrimary());
                    else
                        holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    holder.tvRank.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                    holder.tvScore.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                    holder.tvTimeTaken.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                    holder.tvUser.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                    if (!TextUtils.isEmpty(WebServices.mLoginUtility.getPhoto()) && !WebServices.mLoginUtility.getPhoto().contains("no-image"))
                        Picasso.with(mContext).load(WebServices.mLoginUtility.getPhoto()).into(holder.ivUser);
                   /* else
                        setDefaultImage(holder,WebServices.mLoginUtility.getFirstname());*/
                }else{
                   // setDefaultImage(holder,readResponseUtility.getFirstname());
                }
            }
        }
    }

    public void setDefaultImage(ViewHolder holder,String username) {
        holder.tvUsername.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(username) && username.length() > 1)
            holder.tvUsername.setText(username.substring(0, 1));
        holder.tvUsername.setBackground(((BaseActivity) mContext).createShapeByColor(Utils.getColorPrimary(), 270, 0, R.color.colorPrimary));
    }

    @Override
    public int getItemCount() {
        if (readResponseUtilities.size() >= 10)
            return 10;
        return readResponseUtilities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvRank, tvUser, tvScore, tvTimeTaken, tvUsername;
        private ImageView ivUser;

        public ViewHolder(View itemView) {
            super(itemView);
            tvUsername = (TextView) itemView.findViewById(R.id.tv_user_name);
            tvRank = (TextView) itemView.findViewById(R.id.tv_rank);
            tvUser = (TextView) itemView.findViewById(R.id.tv_user);
            tvScore = (TextView) itemView.findViewById(R.id.tv_score);
            tvTimeTaken = (TextView) itemView.findViewById(R.id.tv_time_taken);
            ivUser = (ImageView) itemView.findViewById(R.id.iv_user);

        }
    }
}