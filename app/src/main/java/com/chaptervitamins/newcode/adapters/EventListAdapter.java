package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.EventDetailActivity;
import com.chaptervitamins.newcode.models.EventModel;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 29-01-2018.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<EventModel> mEventModelArrayList;

    public EventListAdapter(ArrayList<EventModel> eventModelArrayList) {
        this.mEventModelArrayList = eventModelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_event, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            EventModel eventModel = mEventModelArrayList.get(position);
            if(eventModel!=null){
                holder.tvProgrammeName.setText(setNull(eventModel.getName()));
                holder.tvStartDate.setText(setNull(eventModel.getStartDate()));
                holder.tvStartTime.setText(setNull(eventModel.getStartTime()));
                holder.tvTrainerName.setText(setNull(eventModel.getTrainerName()));
                holder.tvStatus.setText(setNull(eventModel.getStatus()));
            }
        }

    }

    private String setNull(String text) {
        if (!TextUtils.isEmpty(text))
            return text;
        return "N/A";
    }

    @Override
    public int getItemCount() {
        return mEventModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvProgrammeName, tvStartDate, tvStartTime, tvStatus, tvTrainerName;

        public ViewHolder(View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            tvProgrammeName = (TextView) itemView.findViewById(R.id.tv_programme_name);
            tvStartDate = (TextView) itemView.findViewById(R.id.tv_start_date);
            tvStartTime = (TextView) itemView.findViewById(R.id.tv_start_time);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            tvTrainerName = (TextView) itemView.findViewById(R.id.tv_trainer_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, EventDetailActivity.class);
                    intent.putExtra("KEY_EVENT",mEventModelArrayList.get(getAdapterPosition()));
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
