package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.interfaces.SearchMaterialListener;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 30-01-2018.
 */

public class SearchMaterialAdapter extends RecyclerView.Adapter<SearchMaterialAdapter.ViewHolder> {
    private ArrayList<MeterialUtility> mMaterialsList;
    private int lastSelectPos = -1;
    private Context mContext;
    private SearchMaterialListener mListener;

    public SearchMaterialAdapter(ArrayList<MeterialUtility> mMaterialsList) {
        this.mMaterialsList = mMaterialsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            MeterialUtility meterialUtility = mMaterialsList.get(position);
            if (meterialUtility != null) {
                holder.tvMaterialName.setText(meterialUtility.getTitle());
                Utils.setMaterialImage(mContext, meterialUtility, holder.ivMaterial);
            }

        }
    }

    @Override
    public int getItemCount() {
        return mMaterialsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMaterialName;
        private ImageView ivMaterial;

        public ViewHolder(View itemView) {
            super(itemView);

            findViews(itemView);
        }

        private void findViews(View itemView) {
            tvMaterialName = (TextView) itemView.findViewById(R.id.tv_material_name);
            ivMaterial = (ImageView) itemView.findViewById(R.id.iv_material);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MixPanelManager mixPanelManager = MixPanelManager.getInstance();
                    DataBase dataBase = DataBase.getInstance(mContext);
                    MaterialOpenController downloadFile = new MaterialOpenController(mContext, mixPanelManager, dataBase);
                    downloadFile.openMaterial(mMaterialsList.get(getAdapterPosition()), true, false, false);
                }
            });
        }
    }
}
