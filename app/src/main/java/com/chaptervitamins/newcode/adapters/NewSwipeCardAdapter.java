package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chaptervitamins.R;
import com.chaptervitamins.utility.FlashCardUtility;

public class NewSwipeCardAdapter extends ArrayAdapter<FlashCardUtility> {

    public NewSwipeCardAdapter(Context context) {
        super(context, R.layout.item_swipe_card);
    }

    @NonNull
    @Override
    public View getView(int position, View contentView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            contentView = inflater.inflate(R.layout.item_tourist_spot_card, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

        FlashCardUtility flashCardUtility = getItem(position);
        assert flashCardUtility != null;
        holder.name.setText(flashCardUtility.getQuestion_description());
        holder.city.setText(flashCardUtility.getAnswer());
        Glide.with(getContext()).load(R.drawable.addgroup_icon)
                .into(holder.image);

        return contentView;
    }

    private static class ViewHolder {
        public TextView name;
        public TextView city;
        public ImageView image;

        public ViewHolder(View view) {
            this.name = (TextView) view.findViewById(R.id.item_tourist_spot_card_name);
            this.city = (TextView) view.findViewById(R.id.item_tourist_spot_card_city);
            this.image = (ImageView) view.findViewById(R.id.item_tourist_spot_card_image);
        }
    }

}
