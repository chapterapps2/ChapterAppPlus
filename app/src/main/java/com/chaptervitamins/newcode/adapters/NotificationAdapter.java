package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.Notification_Utility;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 21-11-2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Context context;
    DataBase dataBase;
    private ArrayList<Notification_Utility> notificationArrayList;
    private String dateText;


    public NotificationAdapter(Context context, ArrayList<Notification_Utility> notificationArrayList) {
        this.notificationArrayList = notificationArrayList;
        this.context = context;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (context == null)
            context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.notification_list_row_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, final int position) {
        /*if (WebServices.notificationUtilityArrayList.get(position).getMaterial_type().equalsIgnoreCase("")
                || WebServices.notificationUtilityArrayList.get(position).getMaterial_type().equalsIgnoreCase("null"))*/
//                    type_ll.setVisibility(View.GONE);
        holder.title_txt.setText(notificationArrayList.get(position).getTitle());
        holder.todo_row_desc.setText(notificationArrayList.get(position).getMessage());
        holder.todo_row_date.setText(Utils.changeDateFormat(notificationArrayList.get(position).getSent_time(),"dd MMM, yy"));
        holder.tvTime.setText(Utils.changeDateFormat(notificationArrayList.get(position).getSent_time(),"hh:mm"));
        if (position == 0) {
            dateText = Utils.changeDateFormat(notificationArrayList.get(position).getSent_time(),"dd MMM, yy");
            holder.todo_row_date.setVisibility(View.VISIBLE);
        } else {
            if (!TextUtils.isEmpty(dateText)) {
                if (dateText.equalsIgnoreCase(Utils.changeDateFormat(notificationArrayList.get(position).getSent_time(),"dd MMM, yy"))) {
                    holder.todo_row_date.setVisibility(View.GONE);
                } else {
                    dateText = Utils.changeDateFormat(notificationArrayList.get(position).getSent_time(),"dd MMM, yy");
                    holder.todo_row_date.setVisibility(View.VISIBLE);
                }
            }
        }

       Utils.setMaterialImage(notificationArrayList.get(position).getMaterial_type(),holder.typeImage,true);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String SMS_URL = "";
            dataBase = DataBase.getInstance(context);
            if (WebServices.isNetworkAvailable(context)) {
                if (!WebServices.notificationUtilityArrayList.get(position).getUrl().equalsIgnoreCase("")) {
                    SMS_URL = WebServices.notificationUtilityArrayList.get(position).getUrl();
                    Utils.loadDeepLinking(context, SMS_URL);
                }
                if (WebServices.notificationUtilityArrayList.get(position).getSent_status().equalsIgnoreCase("SENT")) {
                    WebServices.notificationUtilityArrayList.get(position).setSent_status("READ");
                    int count = 0;
                    for (int i = 0; i < WebServices.notificationUtilityArrayList.size(); i++) {
                        if (!WebServices.notificationUtilityArrayList.get(i).getSent_status().equalsIgnoreCase("READ"))
                            count++;
                    }

                    notifyDataSetChanged();
                    dataBase.addReadNotificationData(WebServices.mLoginUtility.getUser_id(), WebServices.notificationUtilityArrayList.get(position).getNotification_id(), WebServices.notificationUtilityArrayList.get(position).getNotification_user_id());
                }
            }

            }

        });
    }


    @Override
    public int getItemCount() {
        return notificationArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title_txt;
        private LinearLayout typeLL;
        private TextView todo_row_desc,tvTime;
        private TextView todo_row_date;
        private ImageView typeImage;
        private CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            title_txt = (TextView) itemView.findViewById(R.id.todo_row_title);
            todo_row_desc = (TextView) itemView.findViewById(R.id.todo_row_desc);
            todo_row_date = (TextView) itemView.findViewById(R.id.todo_row_date);
            tvTime = (TextView) itemView.findViewById(R.id.timeTxtView);
            typeLL = (LinearLayout) itemView.findViewById(R.id.type_ll);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            typeImage = (ImageView) itemView.findViewById(R.id.type_img);

        }


    }
}

