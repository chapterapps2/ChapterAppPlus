package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.ModuleActivity;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

/**
 * Created by tanuj on 20-11-2017.
 */

public class MultiModuleAdapter extends RecyclerView.Adapter<MultiModuleAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<ModulesUtility> modulesUtilityArrayList;
    private DataBase mDatabase;
    private WebServices webServices;
    private int coursePos;
    ViewGroup parent;

    public MultiModuleAdapter(Context mContext, ArrayList<ModulesUtility> modulesUtilityArrayList, int coursePos) {
        this.modulesUtilityArrayList = modulesUtilityArrayList;
        this.mContext = mContext;
        this.coursePos = coursePos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        if (mContext == null) {
            mContext = parent.getContext();
            this.mDatabase = DataBase.getInstance(mContext);
            webServices = new WebServices();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_module_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (holder != null && position != -1) {
            final ModulesUtility modulesUtility = modulesUtilityArrayList.get(position);
            if (modulesUtility != null) {
                holder.moduleNameTxtView.setText(/*modulesUtility.getCourse_name()+" > "+*/ modulesUtility.getModule_name());
                String completionPer = modulesUtility.getCompletion_per();
                if (!TextUtils.isEmpty(completionPer)) {
                    holder.tvCompletionPer.setText(modulesUtility.getCompletion_per()+"%");
                    holder.progressBarModule.setProgress(Integer.parseInt(modulesUtility.getCompletion_per()));
                }
                else {
                    holder.tvCompletionPer.setText("0%");
                    holder.progressBarModule.setProgress(0);
                }
                MultiModuleMaterialAdapter mutiModuleMaterialAdapter = null;
                if (modulesUtility.getMeterialUtilityArrayList().size() > 4) {
                    mutiModuleMaterialAdapter = new MultiModuleMaterialAdapter(modulesUtility.getMeterialUtilityArrayList().subList(0, 5), coursePos, modulesUtility.getModule_id());
                } else {
                    mutiModuleMaterialAdapter = new MultiModuleMaterialAdapter(modulesUtility.getMeterialUtilityArrayList(), coursePos, modulesUtility.getModule_id());
                }

                holder.recyclerView.setAdapter(mutiModuleMaterialAdapter);
                holder.moreTxtView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ModuleActivity.class);
                        intent.putExtra("position", coursePos);
                        intent.putExtra("mod_id", modulesUtility.getModule_id());
                        intent.putExtra("is_link", false);
                        mContext.startActivity(intent);
                    }
                });

               /* if (!TextUtils.isEmpty(courseUtility.getImage()))
                    new Picasso.Builder(mContext)
//                            .downloader(new OkHttpDownloader(mContext, Integer.MAX_VALUE))
                            .build().load(courseUtility.getImage()).error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher).into(holder.ivCourse);
                else
                    loadImage(courseUtility.getCourse_name(), holder.ivCourse);*/
            }
        }

    }


    @Override
    public int getItemCount() {
        return modulesUtilityArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView moduleNameTxtView,tvCompletionPer;
        private TextView moreTxtView;
        private RecyclerView recyclerView;
        private ProgressBar progressBarModule;

        public ViewHolder(View itemView) {
            super(itemView);
            moduleNameTxtView = (TextView) itemView.findViewById(R.id.moduleNameTxtView);
            moreTxtView = (TextView) itemView.findViewById(R.id.moreTxtView);
            tvCompletionPer = (TextView) itemView.findViewById(R.id.tv_progress);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.matRecyclerView);
            progressBarModule = (ProgressBar) itemView.findViewById(R.id.progressbar_module);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);


        }
    }
}
