package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 20-03-2018.
 */

public class UpdateMaterialAdapter extends RecyclerView.Adapter<UpdateMaterialAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<MeterialUtility> alMaterials;

    public UpdateMaterialAdapter(ArrayList<MeterialUtility> alMeterials) {
        this.alMaterials = alMeterials;
    }

    @Override
    public UpdateMaterialAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_update, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(UpdateMaterialAdapter.ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            MeterialUtility meterialUtility = alMaterials.get(position);
            if (meterialUtility != null) {
                holder.tvMaterialName.setText(meterialUtility.getTitle());
                    Utils.setMaterialImage(mContext,meterialUtility, holder.ivMaterial);
                if (meterialUtility.isSelected()) {
                    holder.cbMaterial.setChecked(true);
                } else {
                    holder.cbMaterial.setChecked(false);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return alMaterials.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivMaterial;
        private TextView tvMaterialName;
        private CheckBox cbMaterial;

        public ViewHolder(View itemView, UpdateMaterialAdapter updateMaterialAdapter) {
            super(itemView);
            findViews(itemView, updateMaterialAdapter);
        }

        private void findViews(View itemView, final UpdateMaterialAdapter updateMaterialAdapter) {
            ivMaterial = (ImageView) itemView.findViewById(R.id.iv_material);
            tvMaterialName = (TextView) itemView.findViewById(R.id.tv_material_name);
            cbMaterial = (CheckBox) itemView.findViewById(R.id.cb_material);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MeterialUtility meterialUtility = alMaterials.get(getAdapterPosition());
                    if (meterialUtility.isSelected()) {
                        meterialUtility.setSelected(false);
                    } else {
                        meterialUtility.setSelected(true);
                    }
                    updateMaterialAdapter.notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
