package com.chaptervitamins.newcode.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chaptervitamins.CustomView.Custom_Progress;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.utility.CourseUtility;


/**
 * Created by Tanuj on 21-11-2017.
 */
public class CoursePagerAdapter extends PagerAdapter {
    private Context mContext;
    private int[] mResources;
    private TextView mTvTotalMaterials, mTvPendingMaterials, tvbCompletionPer, tvType,tvCourseName;
    CourseUtility courseUtility;
    Activity activity;

    public CoursePagerAdapter(CourseUtility courseUtility, Context mContext) {
        this.mContext = mContext;
        this.courseUtility = courseUtility;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.viewpager_module_row, container, false);
        ImageView cardRelative = (ImageView) convertView.findViewById(R.id.logo_img);
        mTvTotalMaterials = (TextView) convertView.findViewById(R.id.tv_completed_materials);
        mTvPendingMaterials = (TextView) convertView.findViewById(R.id.tv_pending_materials);
        tvbCompletionPer = (TextView) convertView.findViewById(R.id.tv_per_comp);
        tvType = (TextView) convertView.findViewById(R.id.tv_type);
        tvCourseName = (TextView) convertView.findViewById(R.id.tv_title);
        Custom_Progress completionProgress = (Custom_Progress) convertView.findViewById(R.id.custom_progress);
        completionProgress.setStartingDegree(270);
        // ImageView logoImage = (ImageView) convertView.findViewById(R.id.logo_img);
//        if (position == 0 || position == 3 || position == 6 || position == 9 || position == 12 || position == 15 || position == 18 || position == 21)
//            cardRelative.setImageResource(R.mipmap.banner1);
//        convertView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.news_feed_bg));
//        else if (position == 1 || position == 4 || position == 7 || position == 10 || position == 13 || position == 16 || position == 19 || position == 22)
//            convertView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_yellow));
////            cardRelative.setImageResource(R.mipmap.banner2);
//        else if (position == 2 || position == 5 || position == 8 || position == 11 || position == 14 || position == 17 || position == 20 || position == 23)
//            convertView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.indicator_blue));
//            cardRelative.setImageResource(R.mipmap.banner3);

        mTvTotalMaterials.setText(courseUtility.getModulesUtilityArrayList().size() + "");
        tvCourseName.setText(courseUtility.getCourse_name());
//        tvbCompletionPer.setText(courseUtility.getCompletion_per() + "%");
        if (!TextUtils.isEmpty(courseUtility.getCompletion_per()))
            completionProgress.setProgress(Integer.parseInt(courseUtility.getCompletion_per()));
        else
            completionProgress.setProgress(0);
        mTvPendingMaterials.setText(courseUtility.getModulesUtilityArrayList().size() - courseUtility.getCompletedModules() + "");
        if (courseUtility.getModulesUtilityArrayList() != null) {
            int materialSize = courseUtility.getModulesUtilityArrayList().size();
            if (materialSize > 1) {
                if (Constants.ORGANIZATION_ID.equalsIgnoreCase("34")&&
                        Constants.BRANCH_ID.equalsIgnoreCase("57")) {
                    tvType.setText("WEEKS");
                }else{
                    tvType.setText("MODULES");
                }
            }else {
                if (Constants.ORGANIZATION_ID.equalsIgnoreCase("34")&&
                        Constants.BRANCH_ID.equalsIgnoreCase("57")) {
                    tvType.setText("WEEK");
                }else{
                    tvType.setText("MODULE");
                }
            }
        }

       /* title_txt = (TextView) convertView.findViewById(R.id.title_txt);
        moreTxt = (TextView) convertView.findViewById(R.id.moreTxt);
        cardRelative = (RelativeLayout) convertView.findViewById(R.id.card_ll);
        title_txt.setText(Html.fromHtml(courseUtility.get(position).getModule_name()));*/
        //   imageDownloader.DisplayImage(courseUtility.get(position).getModule_image(), logo_img);
        /*cardRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeterialUtility meterialUtility = new MeterialUtility();
                meterialUtility.setTitle(courseUtility.get(position).getTitle());
                meterialUtility.setMaterial_id(courseUtility.get(position).getNotice_board_id());
                meterialUtility.setMaterial_type(courseUtility.get(position).getFile_type());
                meterialUtility.setMaterial_media_file_url(courseUtility.get(position).getFile_url());
                meterialUtility.setDescription(courseUtility.get(position).getDesc());
                if (meterialUtility.getMaterial_type().equalsIgnoreCase("Image")) {
                    Intent intent = new Intent(mContext, FullScreenImageActivity.class);
                    intent.putExtra("imgurl", meterialUtility.getMaterial_media_file_url());
                    intent.putExtra("title", courseUtility.get(position).getTitle());
                    mContext.startActivity(intent);
                } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("Video")) {
                    Intent intent = new Intent(mContext, PlayVideo_Activity.class);
                    intent.putExtra("isSaved", false);
                    intent.putExtra("file_name", meterialUtility.getTitle());
                    intent.putExtra("course_id", meterialUtility.getCourse_id());
                    intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                    intent.putExtra("title", meterialUtility.getTitle());
                    intent.putExtra("desc", meterialUtility.getDescription());
                    intent.putExtra("pos", (0) + "");
                    intent.putExtra("type", meterialUtility.getMaterial_type());
                    intent.putExtra("file_url", meterialUtility.getMaterial_media_file_url());
                    ((Activity)mContext).startActivityForResult(intent, 000);
                } else if (meterialUtility.getMaterial_type().equalsIgnoreCase("")) {
                    if (WebServices.isNetworkAvailable(mContext)) {
                        Intent intent = new Intent(mContext, Link_Activity.class);
                        intent.putExtra("name", meterialUtility.getTitle());
                        intent.putExtra("url", courseUtility.get(position).getLink());
                        intent.putExtra("meterial_id", meterialUtility.getMaterial_id());
                        ((Activity)mContext).startActivityForResult(intent, 000);
                    } else {
                        Toast.makeText(mContext, "This file is not available offline. Please connect to internet.", Toast.LENGTH_LONG).show();
                    }

                } else {
                    new DownloadFiles(mContext);
                }
            }
        });*/
   /*     moreTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (courseUtility.get(position).getLink().equalsIgnoreCase("")) return;
                //                    String type="pdf";
                //                    if (courseUtility.get(position).isVideo()||courseUtility.get(position).isImage())type="";
                //                    Intent intent = new Intent(getActivity(), NewsFeedDetail_Activity.class);
                //                    intent.putExtra("link", courseUtility.get(position).getLink());
                //                    intent.putExtra("title", courseUtility.get(position).getTitle());
                //                    intent.putExtra("type",type);
                //                    startActivity(intent);
                Intent intent = new Intent(mContext, NewsFeedFragment.class);

                mContext.startActivity(intent);
                   *//* ...Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(courseUtility.get(position).getLink()));
                    mContext.startActivity(browserIntent);
                    int view = Integer.parseInt(courseUtility.get(position).getTotal_view()) + 1;
                    courseUtility.get(position).setTotal_view(view + "");...*//*
                ///..  new getLikeView().execute(courseUtility.get(position).getNotice_board_id(),"view");
            }
        });*/
           /* play_video_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra("file_url", courseUtility.get(position).getVideoLink());
                    intent.putExtra("isSaved", false);
                    mContext.startActivity(intent);
                    int view = Integer.parseInt(courseUtility.get(position).getTotal_view()) + 1;
                    courseUtility.get(position).setTotal_view(view + "");
                    ///. new getLikeView().execute(courseUtility.get(position).getNotice_board_id(),"view");
                }
            });*/

        container.addView(convertView);

        return convertView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
