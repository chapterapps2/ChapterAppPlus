package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.interfaces.SearchMaterialListener;
import com.chaptervitamins.newcode.models.MaterialTypeModel;
import com.chaptervitamins.newcode.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 30-01-2018.
 */

public class MaterialTypeAdapter extends RecyclerView.Adapter<MaterialTypeAdapter.ViewHolder> {
    private ArrayList<MaterialTypeModel> mMaterialTypeList;
    private int lastSelectPos = 0;
    private Context mContext;
    private SearchMaterialListener mListener;

    public MaterialTypeAdapter(ArrayList<MaterialTypeModel> mMaterialTypeList, SearchMaterialListener listener) {
        this.mMaterialTypeList = mMaterialTypeList;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_material_type, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            MaterialTypeModel materialTypeModel = mMaterialTypeList.get(position);
            holder.tvMaterialType.setText(materialTypeModel.getName());
            if (materialTypeModel.isSelected()) {
                holder.cardView.setCardBackgroundColor(Utils.getColorPrimary());
                holder.tvMaterialType.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            } else {
                holder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                holder.tvMaterialType.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            }

        }
    }

    @Override
    public int getItemCount() {
        return mMaterialTypeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMaterialType;
        private CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            tvMaterialType = (TextView) itemView.findViewById(R.id.tv_material_type);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MaterialTypeModel model = mMaterialTypeList.get(getAdapterPosition());
                    if (!model.isSelected()) {
                        model.setSelected(true);
                        mMaterialTypeList.get(lastSelectPos).setSelected(false);
                        notifyItemChanged(lastSelectPos);
                        lastSelectPos = getAdapterPosition();
                        notifyItemChanged(getAdapterPosition());
                        mListener.onMaterialTypeSelect(model.getMaterialType());
                    }
                }
            });
        }
    }
}
