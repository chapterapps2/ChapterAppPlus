package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.models.DashboardModel;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 25-01-2018.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private ArrayList<DashboardModel> mDashboardModelArrayList;
    private int mDashboardType;
    private Context mContext;

    public DashboardAdapter(ArrayList<DashboardModel> dashboardModelArrayList, int dashboardType) {
        this.mDashboardModelArrayList = dashboardModelArrayList;
        this.mDashboardType = dashboardType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = null;
        switch (mDashboardType) {
            case 1:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_course_dashboard, parent, false);
                break;
            case 2:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_course_dashboard, parent, false);
                break;
            case 3:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_course_dashboard, parent, false);
                break;
            case 4:
                break;
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            DashboardModel dashboardModel = mDashboardModelArrayList.get(position );
            if (dashboardModel != null) {
                switch (mDashboardType) {
                    case 1:
                        holder.tvName.setText(dashboardModel.getCourseName());
                        holder.tvCompletion.setText(dashboardModel.getCompletion());
                        break;
                    case 2:
                        holder.tvName.setText(dashboardModel.getModuleName());
                        holder.tvCompletion.setText(dashboardModel.getCompletion());
                        break;
                    case 3:
                        holder.tvName.setText(dashboardModel.getMaterialName());
                        holder.tvCompletion.setText(dashboardModel.getCompletion());
                        break;

                }

            }
        }
    }

    @Override
    public int getItemCount() {
        return mDashboardModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvCompletion;

        public ViewHolder(View itemView) {
            super(itemView);

            findViews(itemView);
        }

        private void findViews(View itemView) {
            tvName = (TextView) itemView.findViewById(R.id.tv_course_name);
            tvCompletion = (TextView) itemView.findViewById(R.id.tv_completion);
        }
    }
}
