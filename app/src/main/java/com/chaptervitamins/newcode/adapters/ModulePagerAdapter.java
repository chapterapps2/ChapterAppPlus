package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chaptervitamins.CustomView.Custom_Progress;
import com.chaptervitamins.R;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;


/**
 * Created by Tanuj on 21-11-2017.
 */
public class ModulePagerAdapter extends PagerAdapter {
    private Context mContext;
    private TextView mTvTotalMaterials, mTvPendingMaterials,tvbCompletionPer,tvType,tvModuleName;
    ArrayList<ModulesUtility> modulesUtilityArrayList;

    public ModulePagerAdapter(ArrayList<ModulesUtility> modulesUtilityArrayList,
                               Context mContext) {
        this.mContext = mContext;
        this.modulesUtilityArrayList = modulesUtilityArrayList;
    }

    @Override
    public int getCount() {
        return modulesUtilityArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.viewpager_module_row, container, false);
        ImageView cardRelative = (ImageView) convertView.findViewById(R.id.logo_img);
        mTvTotalMaterials = (TextView) convertView.findViewById(R.id.tv_completed_materials);
        mTvPendingMaterials = (TextView) convertView.findViewById(R.id.tv_pending_materials);
        tvbCompletionPer = (TextView) convertView.findViewById(R.id.tv_per_comp);
        tvType = (TextView) convertView.findViewById(R.id.tv_type);
        tvModuleName = (TextView) convertView.findViewById(R.id.tv_title);
        Custom_Progress completionProgress = (Custom_Progress) convertView.findViewById(R.id.custom_progress);
        completionProgress.setStartingDegree(270);

         ImageView logoImage = (ImageView) convertView.findViewById(R.id.logo_img);
//        if (position == 0 || position == 3 || position == 6 || position == 9 || position == 12 || position == 15 || position == 18 || position == 21)
//            cardRelative.setImageResource(R.mipmap.banner1);
//            convertView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.news_feed_bg));
        /*else if (position == 1 || position == 4 || position == 7 || position == 10 || position == 13 || position == 16 || position == 19 || position == 22)
            convertView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_yellow));
//            cardRelative.setImageResource(R.mipmap.banner2);
        else if (position == 2 || position == 5 || position == 8 || position == 11 || position == 14 || position == 17 || position == 20 || position == 23)
            convertView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.indicator_blue));*/
//            cardRelative.setImageResource(R.mipmap.banner3);
        ModulesUtility modulesUtility = modulesUtilityArrayList.get(position);
        tvModuleName.setText(modulesUtility.getModule_name());
        if(modulesUtility.getMeterialUtilityArrayList()!=null) {
            int materialSize = modulesUtility.getMeterialUtilityArrayList().size();
            if (materialSize > 1)
                tvType.setText("MATERIALS");
            else
                tvType.setText("MATERIAL");
        }
        mTvTotalMaterials.setText(modulesUtility.getMeterialUtilityArrayList().size()+"");
//        tvbCompletionPer.setText(modulesUtility.getCompletion_per()+"%");
        if (!TextUtils.isEmpty(modulesUtility.getCompletion_per()))
            completionProgress.setProgress(Integer.parseInt(modulesUtility.getCompletion_per()));
        else
            completionProgress.setProgress(0);
        mTvPendingMaterials.setText(modulesUtility.getMeterialUtilityArrayList().size() - modulesUtility.getCompletedMaterials() + "");

   /*     moreTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modulesUtilityArrayList.get(position).getLink().equalsIgnoreCase("")) return;
                //                    String type="pdf";
                //                    if (modulesUtilityArrayList.get(position).isVideo()||modulesUtilityArrayList.get(position).isImage())type="";
                //                    Intent intent = new Intent(getActivity(), NewsFeedDetail_Activity.class);
                //                    intent.putExtra("link", modulesUtilityArrayList.get(position).getLink());
                //                    intent.putExtra("title", modulesUtilityArrayList.get(position).getTitle());
                //                    intent.putExtra("type",type);
                //                    startActivity(intent);
                Intent intent = new Intent(mContext, NewsFeedFragment.class);

                mContext.startActivity(intent);
                   *//* ...Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(modulesUtilityArrayList.get(position).getLink()));
                    mContext.startActivity(browserIntent);
                    int view = Integer.parseInt(modulesUtilityArrayList.get(position).getTotal_view()) + 1;
                    modulesUtilityArrayList.get(position).setTotal_view(view + "");...*//*
                ///..  new getLikeView().execute(modulesUtilityArrayList.get(position).getNotice_board_id(),"view");
            }
        });*/
           /* play_video_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra("file_url", modulesUtilityArrayList.get(position).getVideoLink());
                    intent.putExtra("isSaved", false);
                    mContext.startActivity(intent);
                    int view = Integer.parseInt(modulesUtilityArrayList.get(position).getTotal_view()) + 1;
                    modulesUtilityArrayList.get(position).setTotal_view(view + "");
                    ///. new getLikeView().execute(modulesUtilityArrayList.get(position).getNotice_board_id(),"view");
                }
            });*/

        container.addView(convertView);

        return convertView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
