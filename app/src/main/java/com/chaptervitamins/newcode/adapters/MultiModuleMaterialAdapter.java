package com.chaptervitamins.newcode.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.ModuleActivity;
import com.chaptervitamins.newcode.server.ApiCallUtils;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.MeterialUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class MultiModuleMaterialAdapter extends RecyclerView.Adapter<MultiModuleMaterialAdapter.ViewHolder> {
    private Context mContext;
    private List<MeterialUtility> meterialUtilityArrayList;
    private DataBase mDatabase;
    private WebServices webServices;
    private String apiUrl, modId;
    private int coursePos;

    public MultiModuleMaterialAdapter(List<MeterialUtility> meterialUtilityArrayList, int coursePos, String modId) {
        this.meterialUtilityArrayList = meterialUtilityArrayList;
        this.coursePos = coursePos;
        this.modId = modId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
            this.mDatabase = DataBase.getInstance(mContext);
            webServices = new WebServices();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_grid_material_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            if (position == meterialUtilityArrayList.size()) {
                holder.viewMoreCard.setVisibility(View.VISIBLE);
                holder.cardViewMat.setVisibility(View.GONE);
            } else {
                holder.cardViewMat.setVisibility(View.VISIBLE);
                holder.viewMoreCard.setVisibility(View.GONE);
                MeterialUtility meterialUtility = meterialUtilityArrayList.get(position);
                if (meterialUtility != null) {
                    holder.tvName.setText(meterialUtility.getTitle());
                    if (WebServices.isFavouritesMaterial(meterialUtility.getMaterial_id())) {
                        holder.ivFavourite.setImageResource(R.drawable.favorite_done);
                    } else {
                        holder.ivFavourite.setImageResource(R.drawable.favorite_not);
                    }
                    if (!TextUtils.isEmpty(meterialUtility.getComplete_per())) {
                        //  holder.pBMaterial.setProgress(Integer.parseInt(meterialUtility.getComplete_per()));
                        holder.tvProgress.setText(meterialUtility.getComplete_per() + "%");
                        if (meterialUtility.getComplete_per().equals("100")) {
                            holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green_text));
                            if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.LINK) ||
                                    meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.FLASH)) {
                                holder.tvCompletedStatus.setText("ACCESSED");
                                holder.tvProgress.setVisibility(View.GONE);
                            } else {
                                holder.tvProgress.setVisibility(View.VISIBLE);
                                holder.tvCompletedStatus.setText(mContext.getString(R.string.completed));
                            }
                        }else if (meterialUtility.getComplete_per().equals("0")) {
                            if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ) && !TextUtils.isEmpty(meterialUtility.getLastplayed()) &&
                                    !meterialUtility.getLastplayed().equalsIgnoreCase("0000-00-00 00:00:00")) {
                                holder.tvProgress.setVisibility(View.GONE);
                                holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green_text));
                                holder.tvCompletedStatus.setText(mContext.getString(R.string.accessed));
                            } else {
                                holder.tvProgress.setVisibility(View.GONE);
                                holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red_text));
                                holder.tvCompletedStatus.setText(mContext.getString(R.string.pending));
                            }
                        } else {
                            holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.orange_text));
                            holder.tvCompletedStatus.setText(mContext.getString(R.string.completed));
                        }
                        if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM)) {
                            if (!TextUtils.isEmpty(meterialUtility.getScrom_status()) && meterialUtility.getScrom_status().equalsIgnoreCase("COMPLETED")) {
                                if( meterialUtility.isPassed()) {
                                    holder.tvCompletedStatus.setText("COMPLETED");
                                    holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green_text));
                                }else {
                                    holder.tvCompletedStatus.setText("FAILED");
                                    holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red_text));
                                }
                            } else if (meterialUtility.getIs_accessed()) {
                                holder.tvCompletedStatus.setText("IN PROGRESS");
                                holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.orange_text));
                            } else {
                                holder.tvCompletedStatus.setText("NOT STARTED");
                                holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red_text));
                            }
                        }
                    } else {
                        // holder.pBMaterial.setProgress(0);
                        holder.tvProgress.setText("0%");
                        holder.tvProgress.setVisibility(View.GONE);
                        holder.tvCompletedStatus.setText(mContext.getString(R.string.pending));
                        holder.tvCompletedStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red_text));

                    }

                    Utils.setMaterialImage(mContext, meterialUtility, holder.ivMaterial);

                    if (meterialUtility.getIs_offline_available().equalsIgnoreCase("yes")) {
                        holder.ivDownload.setVisibility(View.VISIBLE);
                        if (Utils.checkDownloadStatus(mContext, meterialUtility, mDatabase))
                            holder.ivDownload.setImageResource(R.drawable.download_done);
                        else
                            holder.ivDownload.setImageResource(R.drawable.not_download);
                    } else
                        holder.ivDownload.setVisibility(View.GONE);

                    checkMenuVisibility(holder, meterialUtility);
                }
            }
        }
    }

    private void checkMenuVisibility(ViewHolder holder, MeterialUtility meterialUtility) {
        if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ)) {
//            if (meterialUtility.getShow_leaderboard().equals("yes") || meterialUtility.getShow_certificate().equalsIgnoreCase("yes")) {
            holder.ivMenu.setVisibility(View.VISIBLE);
//            }
        } else
            holder.ivMenu.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        if (meterialUtilityArrayList.size() >= 5)
            return meterialUtilityArrayList.size() + 1;
        else
            return meterialUtilityArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName, tvNoOfLikes, tvRating, tvMatnum, tvProgress, tvPoints, tvCompletedTime, tvCompletedStatus;
        private ImageView ivIsComplete, ivDownload, ivFavourite, ivMaterial, ivPoints, ivMenu;
        private CardView viewMoreCard;
        private CardView cardViewMat;


        public ViewHolder(View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            ivMaterial = (ImageView) itemView.findViewById(R.id.iv_material);
            ivDownload = (ImageView) itemView.findViewById(R.id.iv_download);
            ivFavourite = (ImageView) itemView.findViewById(R.id.iv_favorite);
            ivMenu = (ImageView) itemView.findViewById(R.id.iv_menu);
            tvProgress = (TextView) itemView.findViewById(R.id.tv_progress);
            viewMoreCard = (CardView) itemView.findViewById(R.id.viewMoreCard);
            cardViewMat = (CardView) itemView.findViewById(R.id.cardViewMat);
            tvCompletedStatus = (TextView) itemView.findViewById(R.id.tv_completed_status);
            ivFavourite.setOnClickListener(this);
            ivMenu.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_favorite:
                    favouriteApiCall(meterialUtilityArrayList.get(getAdapterPosition()), ivFavourite);
                    break;
                case R.id.iv_menu:
                    showMenu(this, meterialUtilityArrayList.get(getAdapterPosition()), v);
                    break;
                default:
                    if (getAdapterPosition() == meterialUtilityArrayList.size()) {
                        Intent intent = new Intent(mContext, ModuleActivity.class);
                        intent.putExtra("position", coursePos);
                        intent.putExtra("mod_id", modId);
                        intent.putExtra("is_link", false);
                        mContext.startActivity(intent);
                    } else {
                        MixPanelManager mixPanelManager = MixPanelManager.getInstance();
//                        HomeActivity.ASSIGNMATERIALID = meterialUtilityArrayList.get(getAdapterPosition()).getAssign_material_id();
//                        HomeActivity.MODULEID = meterialUtilityArrayList.get(getAdapterPosition()).getModule_id();
                        DataBase dataBase = DataBase.getInstance(mContext);
                        MaterialOpenController downloadFile = new MaterialOpenController(mContext, mixPanelManager, dataBase);
                        downloadFile.openMaterial(meterialUtilityArrayList.get(getAdapterPosition()), true, false, true);
                    }
                    break;
            }
            notifyItemChanged(getAdapterPosition());
        }


        /**
         * It is used to make favourite and unfavourite material
         *
         * @param meterialUtility
         * @param ivFavourite
         */
        private void favouriteApiCall(MeterialUtility meterialUtility, ImageView ivFavourite) {
            if (meterialUtility != null) {
                if (!WebServices.isFavouritesMaterial(meterialUtility.getMaterial_id())) {
                    apiUrl = APIUtility.ADD_FAVORITES;
                    ivFavourite.setImageResource(R.drawable.favorite_done);
                    webServices.addFavoriteMaterial(meterialUtility);
                    mDatabase.addFData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), meterialUtility.getMaterial_type(), "", "yes");
                } else {
                    apiUrl = APIUtility.REMOVE_FAVORITES;
                    ivFavourite.setImageResource(R.drawable.favorite_not);
                    if (!WebServices.isNetworkAvailable(mContext)) {
                        webServices.removeFavoriteMaterial(meterialUtility.getMaterial_id());
                    }

                    mDatabase.RemoveFav(WebServices.mLoginUtility.getUser_id(),meterialUtility.getMaterial_id());
                }
                if (WebServices.isNetworkAvailable(mContext)) {
                    final ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("item_id", meterialUtility.getMaterial_id()));
                    nameValuePair.add(new BasicNameValuePair("item_type", meterialUtility.getMaterial_type()));
                    if (apiUrl.equals(APIUtility.REMOVE_FAVORITES)) {
                        nameValuePair.add(new BasicNameValuePair("favourite_id", WebServices.getFavouriteid(meterialUtility.getMaterial_id())));
                        webServices.removeFavoriteMaterial(meterialUtility.getMaterial_id());
                    }
                    notifyItemChanged(getAdapterPosition());
                    new GenericApiCall(mContext, apiUrl, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (webServices.isValid((String) result)) {
                                if (apiUrl.equals(APIUtility.ADD_FAVORITES)) {
                                    Toast.makeText(mContext, "Item has been added to your Favourites List.", Toast.LENGTH_LONG).show();
                                    webServices.addFavoriteMaterial((String) result);
                                } else
                                    Toast.makeText(mContext, "Item has been removed from your Favourites List.", Toast.LENGTH_LONG).show();
                            }

                        }

                        @Override
                        public void onError(ErrorModel error) {
                            Toast.makeText(mContext, error.getErrorDescription(), Toast.LENGTH_LONG).show();
                            //DialogUtils.showDialog(mContext, error.getErrorDescription());
                        }
                    }).execute();
                }
            }
        }
    }

    private void showMenu(ViewHolder holder, final MeterialUtility meterialUtility, View view) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.menu_layout);
        LinearLayout llLeaderboard = (LinearLayout) dialog.findViewById(R.id.ll_leaderboard);
        LinearLayout llShowResult = (LinearLayout) dialog.findViewById(R.id.ll_show_result);
        LinearLayout llCertificate = (LinearLayout) dialog.findViewById(R.id.ll_certificate);
        if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes") &&
                meterialUtility.getIs_accessed())
            llShowResult.setVisibility(View.VISIBLE);
        else
            llShowResult.setVisibility(View.GONE);
        if (meterialUtility.getShow_leaderboard().equalsIgnoreCase("YES"))
            llLeaderboard.setVisibility(View.VISIBLE);
        if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && meterialUtility.isPassed()) {
            if (meterialUtility.getIsResultPublishRequired().equalsIgnoreCase("YES")) {
                if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes"))
                    llCertificate.setVisibility(View.VISIBLE);
                else
                    llCertificate.setVisibility(View.GONE);
            } else
                llCertificate.setVisibility(View.VISIBLE);
        } else
            llCertificate.setVisibility(View.GONE);
        setDialogPosition(dialog, view);
        llCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null)
                    dialog.dismiss();
                ApiCallUtils.showCertificate(mContext, MixPanelManager.getInstance(), mDatabase, meterialUtility);
            }
        });
        llLeaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null)
                    dialog.dismiss();

                ApiCallUtils.leaderboardApiCall(mContext, webServices, mDatabase, meterialUtility);
            }
        });
        llShowResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null)
                    dialog.dismiss();

                ApiCallUtils.resultPublishApiCall(mContext, webServices, mDatabase, meterialUtility);
            }
        });

        dialog.show();

    }

    private void setDialogPosition(Dialog dialog, View view) {
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM | Gravity.LEFT);

        WindowManager.LayoutParams params = window.getAttributes();
        params.y = dpToPx(60);
        window.setAttributes(params);
    }

    private int dpToPx(int dp) {
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

}
