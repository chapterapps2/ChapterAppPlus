package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.EvaluationActivity;
import com.chaptervitamins.newcode.models.VideoQuizAllResponseModel;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 20-02-2018.
 */

public class VideoQuizHistoryAdapter extends RecyclerView.Adapter<VideoQuizHistoryAdapter.ViewHolder> {

    private Context mContext;
    private MeterialUtility meterialUtility;
    private ArrayList<VideoQuizAllResponseModel> allResponseModels;

    public VideoQuizHistoryAdapter(ArrayList<VideoQuizAllResponseModel> allResponseModels, MeterialUtility meterialUtility) {
        this.allResponseModels = allResponseModels;
        this.meterialUtility= meterialUtility;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_video_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            VideoQuizAllResponseModel videoQuizAllResponseModel = allResponseModels.get(position);
            if (videoQuizAllResponseModel != null) {
                holder.tvDate.setText(Utils.changeDateFormat(videoQuizAllResponseModel.getFinishDate(),"dd MMM yy"));
                holder.tvTime.setText(Utils.changeDateFormat(videoQuizAllResponseModel.getFinishDate(),"hh:mm"));
                if(videoQuizAllResponseModel.isEvaluated()) {
                    holder.tvStatus.setText("Evaluated");
                    holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green_text));
                }else{
                    holder.tvStatus.setText("Yet to evaluate");
                    holder.tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red_text));
                }
                holder.tvAttemptNo.setText("Attempt "+(position+1));
            }
        }
    }

    @Override
    public int getItemCount() {
        return allResponseModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDate, tvTime, tvAttemptNo, tvStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvAttemptNo = (TextView) itemView.findViewById(R.id.tv_attempt);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VideoQuizAllResponseModel videoQuizAllResponseModel = allResponseModels.get(getAdapterPosition());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("meterial_utility", meterialUtility);
                    bundle.putSerializable("res", videoQuizAllResponseModel.getAlVideoQuizResponse());
                    bundle.putSerializable("res_list", allResponseModels);
                    bundle.putSerializable("is_from_history", true);
                    Intent intent = new Intent(mContext, EvaluationActivity.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
