package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.server.ApiCallUtils;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.MeterialUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class MaterialAdapter extends RecyclerView.Adapter<MaterialAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private DataBase mDatabase;
    private WebServices webServices;
    private String apiUrl;
    private boolean allMaterialScreenCheck = false;

    public MaterialAdapter(ArrayList<MeterialUtility> meterialUtilityArrayList) {
        this.meterialUtilityArrayList = meterialUtilityArrayList;
    }

    public MaterialAdapter(ArrayList<MeterialUtility> meterialUtilityArrayList, boolean allMaterialScreenCheck) {
        this.meterialUtilityArrayList = meterialUtilityArrayList;
        this.allMaterialScreenCheck = allMaterialScreenCheck;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
            this.mDatabase = DataBase.getInstance(mContext);
            webServices = new WebServices();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.content_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int matNumPos = 0;
        if (holder != null && position != -1) {
            MeterialUtility meterialUtility = meterialUtilityArrayList.get(position);
            GradientDrawable bgShape = (GradientDrawable) holder.tvMatnum.getBackground();
            if (meterialUtility != null) {
                if (meterialUtilityArrayList.size() > position) {
                    matNumPos = (position + 1);
                }
                if (meterialUtilityArrayList.size() == 1) {
                    holder.vConDiv2.setVisibility(View.INVISIBLE);
                    holder.vConDiv1.setVisibility(View.INVISIBLE);
                } else if (matNumPos == meterialUtilityArrayList.size()) {
                    holder.vConDiv2.setVisibility(View.INVISIBLE);
                    holder.vConDiv1.setVisibility(View.VISIBLE);
                } else if (matNumPos == 1) {
                    holder.vConDiv1.setVisibility(View.INVISIBLE);
                    holder.vConDiv2.setVisibility(View.VISIBLE);
                } else {
                    holder.vConDiv2.setVisibility(View.VISIBLE);
                    holder.vConDiv1.setVisibility(View.VISIBLE);
                }

                holder.tvName.setText(meterialUtility.getTitle());
                if (WebServices.isFavouritesMaterial(meterialUtility.getMaterial_id())) {
                    holder.ivFavourite.setImageResource(R.drawable.favorite_done);
                } else {
                    holder.ivFavourite.setImageResource(R.drawable.favorite_not);
                }
                if (meterialUtility.getCoinsAllocatedModel() != null                                                     && !TextUtils.isEmpty(meterialUtility.getCoinsAllocatedModel().getMaxCoins())) {
                    holder.tvPoints.setText(meterialUtility.getCoinsAllocatedModel().getMaxCoins() + " Points");
                    holder.tvPoints.setVisibility(View.VISIBLE);
                    holder.ivPoints.setVisibility(View.VISIBLE);
                } else {
                    holder.ivPoints.setVisibility(View.GONE);
                    holder.tvPoints.setVisibility(View.INVISIBLE);
                }
                if (meterialUtility.isSeen())
                    holder.ivIsComplete.setImageResource(R.drawable.seen);
                else
                    holder.ivIsComplete.setImageResource(R.drawable.not_seen);

               /* if(meterialUtility.getIs_offline_available().equalsIgnoreCase("NO")&&
                        meterialUtility.getMaterial_type().equalsIgnoreCase("pdf")){
                    if (!TextUtils.isEmpty(meterialUtility.getComplete_per())) {
                        if (!meterialUtility.getComplete_per().equalsIgnoreCase("0")) {
                            holder.tvProgress.setText("ACCESSED");
                            holder.pBMaterial.setVisibility(View.INVISIBLE);
                            holder.tvProgress.setTextColor(ContextCompat.getColor(mContext, R.color.green_text));
                        } else {
                            holder.pBMaterial.setProgress(0);
                            holder.tvProgress.setText("0%");
                        }
                    } else {
                        holder.pBMaterial.setProgress(0);
                        holder.tvProgress.setText("0%");
                    }
                }else {*/
                    if (!TextUtils.isEmpty(meterialUtility.getComplete_per())) {
                        setProgressData(holder, meterialUtility);
                    } else {
                        holder.pBMaterial.setProgress(0);
                        holder.tvProgress.setText("0%");
                    }
                //}

                if (!allMaterialScreenCheck) {
                    if (meterialUtility.getIs_flowing_course())
                        if (!TextUtils.isEmpty(meterialUtility.getComplete_per()) && meterialUtility.getComplete_per().equals("100")) {
                            bgShape.setColor(ContextCompat.getColor(mContext, R.color.green_text));
//                        holder.tvMatnum.setTextColor(ContextCompat.getColor(mContext,R.color.white));
                        } else {
//                        holder.tvMatnum.setTextColor(ContextCompat.getColor(mContext,R.color.hint_text_color));
                            bgShape.setColor(ContextCompat.getColor(mContext, R.color.red_text));
                        }
                    else
                        holder.llFlowingCourse.setVisibility(View.GONE);
                } else {
                    holder.llFlowingCourse.setVisibility(View.GONE);
                }

                Utils.setMaterialImage(mContext, meterialUtility, holder.ivMaterial);
                if (!TextUtils.isEmpty(meterialUtility.getAverage_rating()) && !meterialUtility.getAverage_rating().equals("null")) {
                    String avgRating = meterialUtility.getAverage_rating();
                    if (avgRating.length() >= 3)
                        holder.tvRating.setText(meterialUtility.getAverage_rating().substring(0, 3));
                    else
                        holder.tvRating.setText(meterialUtility.getAverage_rating());
                } else
                    holder.llRating.setVisibility(View.GONE);
                String completedDate = WebServices.getCompletedDate(meterialUtility);
                if (!TextUtils.isEmpty(completedDate)) {
                    holder.tvCompletedTime.setVisibility(View.VISIBLE);
                    holder.tvCompletedTime.setText("Accessed On : " + Utils.getQuizStartDateNewFormat(completedDate));
                } else {
                    holder.tvCompletedTime.setVisibility(View.GONE);
                }
                holder.ratingBar.setRating(Float.parseFloat("4.3"));
                holder.ratingBar.setStepSize(Float.parseFloat("0.1"));
                if (meterialUtility.getIs_offline_available().equalsIgnoreCase("yes")) {
                    holder.ivDownload.setVisibility(View.VISIBLE);
                    if (Utils.checkDownloadStatus(mContext, meterialUtility, mDatabase))
                        holder.ivDownload.setImageResource(R.drawable.download_done);
                    else
                        holder.ivDownload.setImageResource(R.drawable.not_download);
                } else
                    holder.ivDownload.setVisibility(View.GONE);
            }
            holder.tvMatnum.setText("" + matNumPos);

            setVisibilityOfMoreDetails(meterialUtility, holder);

        }
    }

    private void setProgressData(ViewHolder holder, MeterialUtility meterialUtility) {
        holder.ratingBar.setVisibility(View.GONE);
        switch (meterialUtility.getMaterial_type().toUpperCase()) {
            case AppConstants.MaterialType.LINK:
            case AppConstants.MaterialType.FLASH:
                if (!meterialUtility.getComplete_per().equalsIgnoreCase("0")) {
                    holder.tvProgress.setText("ACCESSED");
                    holder.pBMaterial.setVisibility(View.INVISIBLE);
                    holder.tvProgress.setTextColor(ContextCompat.getColor(mContext, R.color.green_text));
                } else {
                    holder.pBMaterial.setProgress(0);
                    holder.tvProgress.setText("0%");
                }
                break;
            case AppConstants.MaterialType.TINCAN_SCROM:
                holder.pBMaterial.setVisibility(View.INVISIBLE);
                if (!TextUtils.isEmpty(meterialUtility.getScrom_status()) && meterialUtility.getScrom_status().equalsIgnoreCase("COMPLETED")) {
                    if (meterialUtility.isPassed()) {
                        holder.tvProgress.setText("COMPLETED");
                        holder.tvProgress.setTextColor(ContextCompat.getColor(mContext, R.color.green_text));
                    } else {
                        holder.tvProgress.setText("FAILED");
                        holder.tvProgress.setTextColor(ContextCompat.getColor(mContext, R.color.red_text));
                    }
//                } else if (Integer.parseInt(meterialUtility.getComplete_per()) >= 0) {
                } else if (meterialUtility.getIs_accessed()) {
                    holder.tvProgress.setText("IN PROGRESS");
                    holder.tvProgress.setTextColor(ContextCompat.getColor(mContext, R.color.orange_text));
                } else {
                    holder.tvProgress.setText("NOT STARTED");
                    holder.tvProgress.setTextColor(ContextCompat.getColor(mContext, R.color.red_text));
                }
                break;
            case AppConstants.MaterialType.QUIZ:
                String message = "", status = "";

                if (!TextUtils.isEmpty(meterialUtility.getLastplayed()) && !meterialUtility.getLastplayed().equalsIgnoreCase("0000-00-00 00:00:00"))
                    if (meterialUtility.isPassed()) {
                        status = "Passed";
                        if (meterialUtility.getPassing_percentage().equals("0") || meterialUtility.getShow_score().equals("no"))
                            status = "ACCESSED";

                        if (meterialUtility.getRemainingAttempt().equals("1000"))
                            message = "<font color='green' size='25'>" + status + "</font>";
                        else
                            message = "Attempts Left : " + meterialUtility.getRemainingAttempt() + "  " + "<font color='green' size='25'>" + status + "</font>";
                    } else {
                        status = "Not Passed";
                        if (meterialUtility.getPassing_percentage().equals("0") || meterialUtility.getShow_score().equals("no"))
                            status = "ACCESSED";
                        if (meterialUtility.getRemainingAttempt().equals("1000"))
                            message = "<font color='red' size='25'>" + status + "</font>";
                        else
                            message = "Attempts Left : " + meterialUtility.getRemainingAttempt() + "  " + "<font color='red' size='25'>" + status + "</font>";
                    }
                else {
                    if (!meterialUtility.getRemainingAttempt().equals("1000"))
                        message = "No.Of Attempts:" + meterialUtility.getRemainingAttempt();
                }
                holder.tvProgress.setText(Html.fromHtml(message));
                holder.pBMaterial.setVisibility(View.INVISIBLE);
                break;
            default:
                holder.pBMaterial.setVisibility(View.VISIBLE);
                holder.pBMaterial.setProgress(Integer.parseInt(meterialUtility.getComplete_per()));
                holder.tvProgress.setText(meterialUtility.getComplete_per() + "%");
                holder.tvProgress.setTextColor(ContextCompat.getColor(mContext, R.color.black));

        }
    }


    private void setVisibilityOfMoreDetails(MeterialUtility meterialUtility, ViewHolder holder) {
        switch (meterialUtility.getMaterial_type().toUpperCase()) {
            case AppConstants.MaterialType.MULTIMEDIAQUIZ:
                holder.tvShowResult.setVisibility(View.VISIBLE);
                holder.tvShowResult.setText("History");
                holder.tvSize.setVisibility(View.GONE);
                if (meterialUtility.getShow_leaderboard().equalsIgnoreCase("yes"))
                    holder.tvViewLeaderboard.setVisibility(View.VISIBLE);
                else
                    holder.tvViewLeaderboard.setVisibility(View.GONE);

                if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && meterialUtility.getComplete_per().equals("100"))
                    holder.tvCertificate.setVisibility(View.VISIBLE);
                else
                    holder.tvCertificate.setVisibility(View.GONE);
//                holder.tvViewLeaderboard.setVisibility(View.GONE);
//                holder.tvCertificate.setVisibility(View.GONE);
                break;
            case AppConstants.MaterialType.QUIZ:
                if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && meterialUtility.isPassed()) {
                    if (meterialUtility.getIsResultPublishRequired().equalsIgnoreCase("YES")) {
                        if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes"))
                            holder.tvCertificate.setVisibility(View.VISIBLE);
                        else
                            holder.tvCertificate.setVisibility(View.GONE);
                    } else
                        holder.tvCertificate.setVisibility(View.VISIBLE);
                } else
                    holder.tvCertificate.setVisibility(View.GONE);
                if (meterialUtility.getIs_result_published().equalsIgnoreCase("yes") &&
                        meterialUtility.getIs_accessed())
                    holder.tvShowResult.setVisibility(View.VISIBLE);
                else
                    holder.tvShowResult.setVisibility(View.GONE);
                holder.tvResult.setText(meterialUtility.getResult());
                if (meterialUtility.getShow_leaderboard().equalsIgnoreCase("yes")) {
                    holder.tvViewLeaderboard.setVisibility(View.VISIBLE);
                } else
                    holder.tvViewLeaderboard.setVisibility(View.GONE);
                holder.tvSize.setVisibility(View.GONE);
                break;
            case AppConstants.MaterialType.VIDEO:
            case AppConstants.MaterialType.AUDIO:
            case AppConstants.MaterialType.FLASH:
            case AppConstants.MaterialType.PDF:
                holder.tvShowResult.setVisibility(View.GONE);
                holder.tvViewLeaderboard.setVisibility(View.GONE);
                if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && meterialUtility.getComplete_per().equals("100"))
                    holder.tvCertificate.setVisibility(View.VISIBLE);
                else
                    holder.tvCertificate.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(meterialUtility.getFile_size()) && !meterialUtility.getFile_size().equals("0")) {
                    holder.tvSize.setVisibility(View.VISIBLE);
                    holder.tvSize.setText("Size : " + meterialUtility.getFile_size() + "MB");
                }
                break;
            case AppConstants.MaterialType.TINCAN_SCROM:
                holder.tvShowResult.setVisibility(View.GONE);
                if (meterialUtility.getShow_leaderboard().equalsIgnoreCase("yes"))
                    holder.tvViewLeaderboard.setVisibility(View.VISIBLE);
                else
                    holder.tvViewLeaderboard.setVisibility(View.GONE);
                if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && !TextUtils.isEmpty(meterialUtility.getScrom_status()) && meterialUtility.getScrom_status().equalsIgnoreCase("COMPLETED") && meterialUtility.isPassed())
                    holder.tvCertificate.setVisibility(View.VISIBLE);
                else
                    holder.tvCertificate.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(meterialUtility.getFile_size()) && !meterialUtility.getFile_size().equals("0")) {
                    holder.tvSize.setVisibility(View.VISIBLE);
                    holder.tvSize.setText("Size : " + meterialUtility.getFile_size() + " MB");
                }
                break;
            case AppConstants.MaterialType.CONTENT:
            case AppConstants.MaterialType.FULL_TEXT:
            case AppConstants.MaterialType.LINK:
            case AppConstants.MaterialType.FLASHCARD:
            case AppConstants.MaterialType.CONTENTINSHORT:
                holder.tvSize.setVisibility(View.GONE);
                if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && meterialUtility.getComplete_per().equals("100"))
                    holder.tvCertificate.setVisibility(View.VISIBLE);
                else
                    holder.tvCertificate.setVisibility(View.GONE);
                holder.tvShowResult.setVisibility(View.GONE);
                holder.tvViewLeaderboard.setVisibility(View.GONE);
                break;
            default:
                holder.tvSize.setVisibility(View.GONE);
                if (meterialUtility.getShow_certificate().equalsIgnoreCase("yes") && meterialUtility.getComplete_per().equals("100"))
                    holder.tvCertificate.setVisibility(View.VISIBLE);
                else
                    holder.tvCertificate.setVisibility(View.GONE);
                holder.tvShowResult.setVisibility(View.GONE);
                holder.tvViewLeaderboard.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return meterialUtilityArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName, tvSize, tvNoOfLikes, tvRating, tvMatnum, tvProgress, tvPoints, tvCompletedTime, tvViewLeaderboard, tvCertificate, tvShowResult, tvResult;
        private ImageView ivIsComplete, ivDownload, ivFavourite, ivMaterial, ivPoints, ivArrow;
        private RatingBar ratingBar;
        private LinearLayout llMoreData, llFlowingCourse, llRating;
        private LinearLayout extraViewParent;
        private View vConDiv1, vConDiv2;
        private ProgressBar pBMaterial;


        public ViewHolder(View itemView) {
            super(itemView);
            findViews(itemView);
        }

        private void findViews(View itemView) {
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvSize = (TextView) itemView.findViewById(R.id.tv_size);
            extraViewParent = (LinearLayout) itemView.findViewById(R.id.extraViewParent);
            vConDiv2 = (View) itemView.findViewById(R.id.con_div_2);
            vConDiv1 = (View) itemView.findViewById(R.id.con_div_1);
            tvMatnum = (TextView) itemView.findViewById(R.id.matnum_tv);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
            tvCompletedTime = (TextView) itemView.findViewById(R.id.tv_completed_time);
            tvViewLeaderboard = (TextView) itemView.findViewById(R.id.tv_view_leaderboard);
            tvShowResult = (TextView) itemView.findViewById(R.id.tv_show_result);
            tvCertificate = (TextView) itemView.findViewById(R.id.tv_certificate);
            tvResult = (TextView) itemView.findViewById(R.id.tv_result);
            tvNoOfLikes = (TextView) itemView.findViewById(R.id.tv_no_of_likes);
            tvPoints = (TextView) itemView.findViewById(R.id.tv_points);
            ivIsComplete = (ImageView) itemView.findViewById(R.id.iv_iscomplete);
            ivMaterial = (ImageView) itemView.findViewById(R.id.iv_material);
            ivArrow = (ImageView) itemView.findViewById(R.id.iv_down);
            ivDownload = (ImageView) itemView.findViewById(R.id.iv_download);
            ivPoints = (ImageView) itemView.findViewById(R.id.iv_points);
            ivFavourite = (ImageView) itemView.findViewById(R.id.iv_favorite);
            ratingBar = (RatingBar) itemView.findViewById(R.id.rating_bar);
            tvProgress = (TextView) itemView.findViewById(R.id.tv_progress);
            pBMaterial = (ProgressBar) itemView.findViewById(R.id.pb_material);
            llMoreData = (LinearLayout) itemView.findViewById(R.id.ll_more_data);
            llRating = (LinearLayout) itemView.findViewById(R.id.ll_rating);
            llFlowingCourse = (LinearLayout) itemView.findViewById(R.id.flow_ll);
            ivFavourite.setOnClickListener(this);
            ivArrow.setOnClickListener(this);
            tvShowResult.setOnClickListener(this);
            tvViewLeaderboard.setOnClickListener(this);
            tvCertificate.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            MeterialUtility meterialUtility = meterialUtilityArrayList.get(getAdapterPosition());
            switch (v.getId()) {
                case R.id.iv_favorite:
                    favouriteApiCall(meterialUtility, ivFavourite);
                    break;
                case R.id.iv_down:
                    if (meterialUtility.isOpen()) {
                        meterialUtility.setOpen(false);
                        llMoreData.setVisibility(View.GONE);
                        ivArrow.setImageResource(R.drawable.bottom_arrow);
                        vConDiv2.setLayoutParams(new LinearLayout.LayoutParams(3, 140));
                        vConDiv1.setLayoutParams(new LinearLayout.LayoutParams(3, 140));
                    } else {
                        meterialUtility.setOpen(true);
                        llMoreData.setVisibility(View.VISIBLE);
                        ivArrow.setImageResource(R.drawable.top_arrow);
                        vConDiv2.setLayoutParams(new LinearLayout.LayoutParams(3, ((extraViewParent.getHeight()) / 2) + 65));
                        vConDiv1.setLayoutParams(new LinearLayout.LayoutParams(3, ((extraViewParent.getHeight()) / 2) + 65));
                    }
                    break;
                case R.id.tv_show_result:
                    if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ))
                        ApiCallUtils.resultPublishApiCall(mContext, webServices, mDatabase, meterialUtility);
                    else
                        ApiCallUtils.quizEvaluateApiCall(mContext, webServices, meterialUtility);
                    break;
                case R.id.tv_certificate:
                    ApiCallUtils.showCertificate(mContext, MixPanelManager.getInstance(), mDatabase, meterialUtility);
                    break;
                case R.id.tv_view_leaderboard:
                    if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ) ||
                            meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM))
                        ApiCallUtils.leaderboardApiCall(mContext, webServices, mDatabase, meterialUtility);
                    break;
                default:
                    MixPanelManager mixPanelManager = MixPanelManager.getInstance();
//                    HomeActivity.ASSIGNMATERIALID = meterialUtilityArrayList.get(getAdapterPosition()).getAssign_material_id();
//                    HomeActivity.MODULEID = meterialUtilityArrayList.get(getAdapterPosition()).getModule_id();
                    DataBase dataBase = DataBase.getInstance(mContext);
                    MaterialOpenController downloadFile = new MaterialOpenController(mContext, mixPanelManager, dataBase);
                    downloadFile.openMaterial(meterialUtilityArrayList.get(getAdapterPosition()), true, false, true);
                    break;
            }
//            notifyItemChanged(getAdapterPosition());
        }

        /**
         * It is used to make favourite and unfavourite material
         *
         * @param meterialUtility
         * @param ivFavourite
         */
        private void favouriteApiCall(MeterialUtility meterialUtility, ImageView ivFavourite) {
            if (meterialUtility != null) {
                if (!WebServices.isFavouritesMaterial(meterialUtility.getMaterial_id())) {
                    apiUrl = APIUtility.ADD_FAVORITES;
                    ivFavourite.setImageResource(R.drawable.favorite_done);
                    webServices.addFavoriteMaterial(meterialUtility);
                    mDatabase.addFData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), meterialUtility.getMaterial_type(), "", "yes");
                } else {
                    apiUrl = APIUtility.REMOVE_FAVORITES;
                    ivFavourite.setImageResource(R.drawable.favorite_not);
                    if (!WebServices.isNetworkAvailable(mContext)) {
                        webServices.removeFavoriteMaterial(meterialUtility.getMaterial_id());
                    }

                    mDatabase.RemoveFav(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                }
                if (WebServices.isNetworkAvailable(mContext)) {
                    final ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("item_id", meterialUtility.getMaterial_id()));
                    nameValuePair.add(new BasicNameValuePair("item_type", meterialUtility.getMaterial_type()));
                    if (apiUrl.equals(APIUtility.REMOVE_FAVORITES)) {
                        nameValuePair.add(new BasicNameValuePair("favourite_id", WebServices.getFavouriteid(meterialUtility.getMaterial_id())));
                        webServices.removeFavoriteMaterial(meterialUtility.getMaterial_id());
                    }
                    notifyItemChanged(getAdapterPosition());
                    new GenericApiCall(mContext, apiUrl, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                        @Override
                        public void onSuccess(Object result) {
                            if (webServices.isValid((String) result)) {
                                if (apiUrl.equals(APIUtility.ADD_FAVORITES)) {
                                    Toast.makeText(mContext, "Item has been added to your Favourites List.", Toast.LENGTH_LONG).show();
                                    webServices.addFavoriteMaterial((String) result);
                                } else
                                    Toast.makeText(mContext, "Item has been removed from your Favourites List.", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ErrorModel error) {
                            Toast.makeText(mContext, error.getErrorDescription(), Toast.LENGTH_LONG).show();
                       /* DialogUtils.showDialog(mContext, error.getErrorDescription());*/
                        }
                    }).execute();
                }
            }
        }
    }

}
