package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

/**
 * Created by tanuj on 20-11-2017.
 */

public class NewMaterialAdapter extends RecyclerView.Adapter<NewMaterialAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private DataBase mDatabase;
    private WebServices webServices;

    public NewMaterialAdapter(Context mContext, ArrayList<MeterialUtility> meterialUtilityArrayList) {
        this.meterialUtilityArrayList = meterialUtilityArrayList;
        this.mContext = mContext;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
            this.mDatabase = DataBase.getInstance(mContext);
            webServices = new WebServices();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.material_row_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            MeterialUtility meterialUtility = meterialUtilityArrayList.get(position);
            if (meterialUtility != null) {

                holder.matTextView.setText(meterialUtility.getDescription());
              //  holder.matImageView.setImageResource(meterialUtility.getMaterial_image());
               /* if (mDatabase.getImage(WebServices.mLoginUtility.getUser_id(), meterialUtility.getImage()) != null) {
                    holder.ivCourse.setImageBitmap(mDatabase.getImage(WebServices.mLoginUtility.getUser_id(), meterialUtility.getImage()));
                } else {
                    loadImage(meterialUtility.getCourse_name(), holder.ivCourse);
                }
                if (WebServices.isNetworkAvailable(mContext)) {
//                    dataBase.deleteImage(WebServices.mLoginUtility.getUser_id(), courseUtilities.get(total_size).getImage());
                    courseImageLoader.DisplayImage(courseUtilities.get(total_size).getImage(), f_list_row_img);
                }*/
                /*if (!TextUtils.isEmpty(meterialUtility.getImage()))
                    new Picasso.Builder(mContext)
//                            .downloader(new OkHttpDownloader(mContext, Integer.MAX_VALUE))
                            .build().load(meterialUtility.getImage()).error(R.drawable.default_icon).placeholder(R.drawable.default_icon).into(holder.ivCourse);
                else
                    loadImage(meterialUtility.getCourse_name(), holder.ivCourse);*/
            }
        }

    }







    @Override
    public int getItemCount() {
        return meterialUtilityArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView matTextView;
        private ImageView matImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            matTextView = (TextView) itemView.findViewById(R.id.matTextView);
            matImageView = (ImageView) itemView.findViewById(R.id.matImageView);
           /* itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectItems(meterialUtilityArrayList.get(getAdapterPosition()));
                }
            });*/
        }
    }
}
