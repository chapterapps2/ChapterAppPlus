package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.models.ContentInShortsModel;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 07-02-2018.
 */

public class ContentsAdapter extends PagerAdapter {
    private Context mContext;
    private ArrayList<ContentInShortsModel> mAlContentInShorts;
    private MediaPlayer mediaPlayer;
    //private JWPlayerView mPlayerView;

    public ContentsAdapter(ArrayList<ContentInShortsModel> mAlContentInShorts) {
        this.mAlContentInShorts = mAlContentInShorts;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (mContext == null)
            mContext = container.getContext();
        ViewGroup view = (ViewGroup) LayoutInflater.from(mContext).inflate(R.layout.item_content_shorts, container, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        if (viewHolder != null && position >= 0) {
            final ContentInShortsModel contentInShortsModel = mAlContentInShorts.get(position);
            if (contentInShortsModel != null) {
                ArrayList<String> sequence = new ArrayList<>();
                switch (contentInShortsModel.getTemplate().toUpperCase()) {
                    case "TEMPLATE-1":
                        sequence.add("TITLE");
                        sequence.add("MEDIA");
                        sequence.add("CONTENT");
                        showContent(viewHolder, sequence, contentInShortsModel);
                        break;
                    case "TEMPLATE-2":
                        sequence.add("MEDIA");
                        sequence.add("TITLE");
                        sequence.add("CONTENT");
                        showContent(viewHolder, sequence, contentInShortsModel);
                        break;
                    case "TEMPLATE-3":
                        sequence.add("TITLE");
                        sequence.add("CONTENT");
                        sequence.add("MEDIA");
                        showContent(viewHolder, sequence, contentInShortsModel);
                        break;
                    case "TEMPLATE-4":
                        sequence.add("TITLE");
                        sequence.add("MEDIA");
                        sequence.add("CONTENT");
                        showContent(viewHolder, sequence, contentInShortsModel);
                        break;
                    default:
                }
                if (TextUtils.isEmpty(contentInShortsModel.getMedia())) {
                    viewHolder.mediaHolderReLayout.setVisibility(View.GONE);
                } else {
                    viewHolder.mediaHolderReLayout.setVisibility(View.VISIBLE);
                    setVisibilityAccToMedia(viewHolder, contentInShortsModel);
                }
                setOnClickListener(viewHolder, contentInShortsModel);
            }
        }

        ((ViewPager) container).addView(view);
        return view;
    }


    private void setOnClickListener(final ViewHolder viewHolder, final ContentInShortsModel contentInShortsModel) {
        viewHolder.ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playVideo(contentInShortsModel.getMedia(), viewHolder);
            }
        });
        viewHolder.ivAudioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAudio(contentInShortsModel.getMedia(), viewHolder);
            }
        });

        viewHolder.mAudioSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer.isPlaying()) {
                    int playPositionInMillisecconds = (mediaPlayer.getDuration() / 100) * seekBar.getProgress();
                    mediaPlayer.seekTo(playPositionInMillisecconds);
                }
            }
        });
    }

    private void setVisibilityAccToMedia(final ViewHolder viewHolder, ContentInShortsModel contentInShortsModel) {
        switch (contentInShortsModel.getContentType().toUpperCase()) {
            case "IMAGE":
                viewHolder.rlVideo.setVisibility(View.GONE);
                viewHolder.llAudio.setVisibility(View.GONE);
                viewHolder.ivContent.setVisibility(View.VISIBLE);
                Glide.with(mContext)
                        .load(contentInShortsModel.getMedia())
                        .error(R.mipmap.ic_launcher)
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                viewHolder.progressBar.hide();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                viewHolder.progressBar.hide();
                                return false;
                            }
                        })
                        .into(viewHolder.ivContent);
                break;
            case "VIDEO":
                viewHolder.ivContent.setVisibility(View.GONE);
                viewHolder.llAudio.setVisibility(View.GONE);
                viewHolder.rlVideo.setVisibility(View.VISIBLE);
                break;
            case "AUDIO":
                viewHolder.rlVideo.setVisibility(View.GONE);
                viewHolder.ivContent.setVisibility(View.GONE);
                viewHolder.llAudio.setVisibility(View.VISIBLE);
                startAudio(contentInShortsModel.getMedia(), viewHolder);
                break;
        }
    }

    private void startAudio(String fileUrl, final ViewHolder viewHolder) {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.reset();
        openAudioFile(fileUrl);
        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                viewHolder.mAudioSeekbar.setSecondaryProgress(percent);
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                viewHolder.ivAudioPlay.setImageResource(R.drawable.button_play);
                viewHolder.mAudioSeekbar.setSecondaryProgress(0);
                viewHolder.mAudioSeekbar.setProgress(0);
            }
        });
    }

    private void openAudioFile(String audiofile) {
        if (!audiofile.equalsIgnoreCase("")) {
            try {
                mediaPlayer.setDataSource(audiofile); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void playVideo(String mediaUrl, ViewHolder holder) {
        /*holder.ivPlay.setVisibility(View.GONE);
        PlayerConfig playerConfig = new PlayerConfig.Builder()
                .file(mediaUrl).autostart(true)
                .build();
        mPlayerView = new JWPlayerView(mContext, playerConfig);
        mPlayerView.setFullscreen(false, false);*/
        /*mPlayerView.addOnAdErrorListener(new AdvertisingEvents.OnAdErrorListener() {
            @Override
            public void onAdError(String s, String s1) {
                System.out.println("=======" + s);
            }
        });
        mPlayerView.addOnErrorListener(new VideoPlayerEvents.OnErrorListenerV2() {
            @Override
            public void onError(ErrorEvent errorEvent) {
                System.out.println("=======" + errorEvent.getMessage());
            }
        });*/
        /*holder.videoView.addView(mPlayerView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));*/
    }

    private void showContent(ViewHolder viewHolder, ArrayList<String> materialSequence, ContentInShortsModel model) {

        if (materialSequence != null && materialSequence.size() == 3) {
            switch (materialSequence.get(0).toUpperCase()) {
                case "TITLE":
                    if (materialSequence.get(1).equalsIgnoreCase("CONTENT")) {
                        viewHolder.tvContentTitle1.setVisibility(View.VISIBLE);
                        viewHolder.tvContentTitle1.setText(Html.fromHtml(model.getTitle()));
                        viewHolder.tvDescription1.setVisibility(View.VISIBLE);
                        viewHolder.tvDescription1.setText(Html.fromHtml(model.getDescription()));
                    } else if (materialSequence.get(1).equalsIgnoreCase("MEDIA")) {
                        viewHolder.tvContentTitle1.setVisibility(View.VISIBLE);
                        viewHolder.tvContentTitle1.setText(Html.fromHtml(model.getTitle()));
                        viewHolder.tvDescription.setVisibility(View.VISIBLE);
                        viewHolder.tvDescription.setText(Html.fromHtml(model.getDescription()));
                    }
                    break;
                case "CONTENT":
                    if (materialSequence.get(1).equalsIgnoreCase("TITLE")) {
                        viewHolder.tvDescription1.setVisibility(View.VISIBLE);
                        viewHolder.tvDescription1.setText(Html.fromHtml(model.getDescription()));
                        viewHolder.tvContentTitle1.setVisibility(View.VISIBLE);
                        viewHolder.tvContentTitle1.setText(Html.fromHtml(model.getTitle()));
                    } else if (materialSequence.get(1).equalsIgnoreCase("MEDIA")) {
                        viewHolder.tvDescription1.setVisibility(View.VISIBLE);
                        viewHolder.tvDescription1.setText(Html.fromHtml(model.getDescription()));
                        viewHolder.tvContentTitle.setVisibility(View.VISIBLE);
                        viewHolder.tvContentTitle.setText(Html.fromHtml(model.getTitle()));
                    }
                    break;
                case "MEDIA":
                    if (materialSequence.get(1).equalsIgnoreCase("TITLE")) {
                        viewHolder.tvContentTitle.setVisibility(View.VISIBLE);
                        viewHolder.tvContentTitle.setText(Html.fromHtml(model.getTitle()));
                        viewHolder.tvDescription.setVisibility(View.VISIBLE);
                        viewHolder.tvDescription.setText(Html.fromHtml(model.getDescription()));
                    } else if (materialSequence.get(1).equalsIgnoreCase("CONTENT")) {
                        viewHolder.tvTitle.setVisibility(View.VISIBLE);
                        viewHolder.tvTitle.setText(Html.fromHtml(model.getDescription()));
                        viewHolder.tvTitle.setTextSize(18);
                        viewHolder.tvDescription.setVisibility(View.VISIBLE);
                        viewHolder.tvDescription.setText(Html.fromHtml(model.getTitle()));
                        viewHolder.tvDescription.setTextSize(14);
                    }
                    break;
            }
        } else {
            viewHolder.tvContentTitle.setText(Html.fromHtml(model.getTitle()));
            viewHolder.tvDescription.setText(Html.fromHtml(model.getDescription()));
        }
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mAlContentInShorts.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    private class ViewHolder {
        private ImageView ivContent, ivPlay, ivAudioPlay;
        private TextView tvContentTitle, tvDescription, tvContentTitle1, tvDescription1, tvTitle;
        private RelativeLayout rlVideo, videoView, mediaHolderReLayout;
        private LinearLayout llAudio;
        private SeekBar mAudioSeekbar;
        private ContentLoadingProgressBar progressBar;

        public ViewHolder(View itemView) {
            findViews(itemView);
        }

        private void findViews(View itemView) {
            ivContent = (ImageView) itemView.findViewById(R.id.iv_content);
            ivPlay = (ImageView) itemView.findViewById(R.id.iv_play);
            progressBar = (ContentLoadingProgressBar) itemView.findViewById(R.id.progressBar);
            tvContentTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            tvContentTitle1 = (TextView) itemView.findViewById(R.id.tv_title1);
            tvDescription1 = (TextView) itemView.findViewById(R.id.tv_description1);
            rlVideo = (RelativeLayout) itemView.findViewById(R.id.rl_video);
            llAudio = (LinearLayout) itemView.findViewById(R.id.ll_audio);
            ivAudioPlay = (ImageView) itemView.findViewById(R.id.iv_audio_play);
            mAudioSeekbar = (SeekBar) itemView.findViewById(R.id.seekbar_audio);
            videoView = (RelativeLayout) itemView.findViewById(R.id.video_view);
            mediaHolderReLayout = (RelativeLayout) itemView.findViewById(R.id.mediaHolderReLayout);
        }


    }
}
