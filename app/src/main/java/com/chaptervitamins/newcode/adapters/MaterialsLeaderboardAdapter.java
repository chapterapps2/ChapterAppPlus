package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.server.ApiCallUtils;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

/**
 * Created by Vijay Antil on 08-06-2018.
 */

public class MaterialsLeaderboardAdapter extends RecyclerView.Adapter<MaterialsLeaderboardAdapter.ViewHolder> {

    private ArrayList<MeterialUtility> meterialUtilities;
    private Context mContext;

    public MaterialsLeaderboardAdapter(ArrayList<MeterialUtility> meterialUtilities) {
        this.meterialUtilities = meterialUtilities;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_materials_leaderboard, parent, false);
        return new MaterialsLeaderboardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            MeterialUtility meterialUtility = meterialUtilities.get(position);
            if(meterialUtility!=null){
                Utils.setMaterialImage(mContext, meterialUtility, holder.ivMaterial);
            }
            holder.tvMaterialName.setText(meterialUtility.getTitle());
        }

    }

    @Override
    public int getItemCount() {
        return meterialUtilities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvMaterialName;
        private ImageView ivMaterial;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMaterialName = (TextView) itemView.findViewById(R.id.tv_material_name);
            ivMaterial = (ImageView) itemView.findViewById(R.id.iv_material);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MeterialUtility meterialUtility = meterialUtilities.get(getAdapterPosition());
                    if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.QUIZ) ||
                            meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM))
                        ApiCallUtils.leaderboardApiCall(mContext, new WebServices(), DataBase.getInstance(mContext), meterialUtility);
                }
            });
        }
    }
}
