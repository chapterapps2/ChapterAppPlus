package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.models.TrainerModel;

import java.util.ArrayList;

/**
 * Created by tanuj on 20-11-2017.
 */

public class EvaluationAdapter extends RecyclerView.Adapter<EvaluationAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<TrainerModel> trainerModelsAl;

    public EvaluationAdapter(ArrayList<TrainerModel> trainerModelsAl) {
        this.trainerModelsAl = trainerModelsAl;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_evaluation_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null && position >= 0) {
            TrainerModel trainerModel = trainerModelsAl.get(position);
            if (trainerModel != null) {
                holder.tvTrainerName.setText(Html.fromHtml("<b>Trainer Name - </b>"+trainerModel.getTrainer_name()));
                holder.tvRate.setText(Html.fromHtml("<b>Rating - </b>"+trainerModel.getRating()));
                if (!TextUtils.isEmpty(trainerModel.getComment()) && !trainerModel.getComment().equalsIgnoreCase("null"))
                    holder.tvComment.setText(Html.fromHtml("<b>Comment - </b>"+trainerModel.getComment()));
                else
                    holder.tvComment.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public int getItemCount() {
        return trainerModelsAl.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTrainerName, tvRate,tvComment;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTrainerName = (TextView) itemView.findViewById(R.id.tv_trainer_name);
            tvRate = (TextView) itemView.findViewById(R.id.tv_rate);
            tvComment = (TextView) itemView.findViewById(R.id.tv_comment);

        }
    }
}
