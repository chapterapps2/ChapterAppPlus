package com.chaptervitamins.newcode.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.ModuleActivity;
import com.chaptervitamins.newcode.models.EventModel;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.FlowingCourseUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by test on 24-08-2017.
 */
public class GridAdapter extends ArrayAdapter {
    private static final String TAG = GridAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private List<Date> monthlyDates;
    private Calendar currentDate;
    Context context;
    private List<EventModel> allEvents;

    public GridAdapter(Context context, List<Date> monthlyDates, Calendar currentDate, List<EventModel> allEvents) {
        super(context, R.layout.cal_grid_row);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        this.allEvents = allEvents;
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public void setUpdateEventsArray(List<EventModel> allEvents) {
        this.allEvents = allEvents;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Date mDate = monthlyDates.get(position);
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);
        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        mDate = Utils.getDateFormat(mDate);
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.cal_grid_row, parent, false);
        }

        //Add day to calendar
        TextView cellNumber = (TextView) view.findViewById(R.id.calendar_date_id);
        final LinearLayout childLinearLayout = (LinearLayout) view.findViewById(R.id.ll_main);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linLayout);
        View circleView = (View) view.findViewById(R.id.circleView);

        cellNumber.setText(String.valueOf(dayValue));
        if (displayMonth == currentMonth && displayYear == currentYear) {
            cellNumber.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            cellNumber.setTextColor(context.getResources().getColor(R.color.gray_light));
        }
        //Add events to the calendar
        TextView eventIndicator = (TextView) view.findViewById(R.id.event_id);
        try {
            if (allEvents != null && allEvents.size() > 0) {
                for (int i = 0; i < allEvents.size(); i++) {
                    Date startDate = null;
                    if (allEvents.get(i).getStartDate() != null && allEvents.get(i).getEndDate() != null) {
                        if (allEvents.get(i).getStartDate() != null) {
                            startDate = Utils.getDateFormat(allEvents.get(i).getStartDate());
                        }
                        Date endDate = null;
                        if (!TextUtils.isEmpty(allEvents.get(i).getEndDate()))
                            endDate = Utils.getDateFormat(allEvents.get(i).getEndDate());
                        if (mDate.equals(endDate) || mDate.equals(startDate) || (mDate.before(endDate) && mDate.after(startDate))) {
                            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.button_color));
                            childLinearLayout.setVisibility(View.VISIBLE);
                            childLinearLayout.setTag(allEvents.get(i).getModuleId());
                            if (mDate.equals(startDate)) {
                                eventIndicator.setText(allEvents.get(i).getName());
                                eventIndicator.setVisibility(View.VISIBLE);
                                circleView.setVisibility(View.VISIBLE);
                            } else {
                                eventIndicator.setVisibility(View.GONE);
                                circleView.setVisibility(View.GONE);
                            }
                        }
                    } else
                        childLinearLayout.setVisibility(View.INVISIBLE);

                }
                /*linLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for(int i=0;i<allEvents.size();i++) {
                            eventCalendar.getTime()
                            allEvents.get(i).getStart_date()
                        }
                    }
                });*/


            }
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (childLinearLayout.getTag() != null) {
                        CourseUtility courseUtility = FlowingCourseUtils.getCourseFromModuleId((String) childLinearLayout.getTag());
                        if (courseUtility != null) {
                            Intent intent = new Intent(context, ModuleActivity.class);
                            intent.putExtra("course", courseUtility);
                            intent.putExtra("mod_id", (String) childLinearLayout.getTag());
                            intent.putExtra("is_link", false);
                            context.startActivity(intent);
                        } else
                            Toast.makeText(context, "This training is not assigned to you", Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(context, "This training is not assigned to you", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public int getCount() {
        return monthlyDates.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return monthlyDates.get(position);
    }

    @Override
    public int getPosition(Object item) {
        return monthlyDates.indexOf(item);
    }
}