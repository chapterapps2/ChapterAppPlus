package com.chaptervitamins.newcode.server;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.discussions.AndroidMultiPartEntity;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Created by Vijay Antil on 30-01-2018.
 */

public class UploadFileToServer extends AsyncTask<Void,Integer,Void> {
    private Dialog progressDialog;
    private Context mContext;
    private ProgressBar mProgressBar;
    private TextView tvTitle,tvEndSize,tvProgress,tvStartSize;
    private Button btnAbort;
    private String url;

    public UploadFileToServer(Context context,String url) {
        this.mContext = context;
        this.url = url;
    }

    private void addProgressDialog(){
        progressDialog = new Dialog(mContext);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setCancelable(false);
        progressDialog.setContentView(R.layout.mprogressbar);
        mProgressBar = (ProgressBar) progressDialog.findViewById(R.id.download_progressbar);
        mProgressBar.setMax(100);
        tvTitle = (TextView) progressDialog.findViewById(R.id.title_txt);
        tvEndSize = (TextView) progressDialog.findViewById(R.id.total_file_txt);
        tvProgress = (TextView) progressDialog.findViewById(R.id.total_files_txt);
        tvStartSize = (TextView) progressDialog.findViewById(R.id.total_progress_txt);
        btnAbort = (Button) progressDialog.findViewById(R.id.abort_btn);
        mProgressBar.setProgress(0);
        tvStartSize.setText("0 MB");
        tvProgress.setText("0%");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        addProgressDialog();

    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // updating progress bar value
        mProgressBar.setProgress(progress[0]);

        // updating percentage value
        tvProgress.setText(String.valueOf(progress[0]) + "%");
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new AndroidMultiPartEntity.ProgressListener() {
            @Override
            public void transferred(long num) {

            }
        });
        return null;
    }

    @Override
    protected void onPostExecute(Void params) {
        super.onPostExecute(params);
    }
}
