package com.chaptervitamins.newcode.server;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.epub.MainActivity;
import com.chaptervitamins.home.OnlineScromActivity;
import com.chaptervitamins.hospitil_search.HospitalSearch_Activity;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.ContentInShortsActivity;
import com.chaptervitamins.newcode.activities.ExoPlayerVideoActivity;
import com.chaptervitamins.newcode.activities.Image_Quiz_Activity;
import com.chaptervitamins.newcode.activities.ModuleActivity;
import com.chaptervitamins.newcode.activities.NewContentInShortsActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.FileHelper;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.play_video.AboutFlashCard_Activity;
import com.chaptervitamins.play_video.Audio_Player_Activity;
import com.chaptervitamins.play_video.FullScreenImageActivity;
import com.chaptervitamins.play_video.Link_Activity;
import com.chaptervitamins.play_video.PDFViewerActivity;
import com.chaptervitamins.quiz.AboutQuiz_Activity;
import com.chaptervitamins.quiz.SurveyActivity;
import com.chaptervitamins.quiz.ViewCertificateActivity;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.video_quiz.AboutVideoQuizActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by Vijay Antil on 24-11-2017.
 */

public class MaterialOpenController {
    private MixPanelManager mixPanelManager;
    private Context mContext;
    private DataBase mDatabase;
    private boolean isDownload = true;
    private String FILEPATH = "", MSG, totalVal = "";
    private int totallenghtOfFile = 0;
    private DownloadMaterialListener mListener;

    public interface DownloadMaterialListener {
        void onSuccess(MeterialUtility meterialUtility);
    }


    public MaterialOpenController(Context context, MixPanelManager mixPanelManager, DataBase dataBase) {
        this.mContext = context;
        this.mixPanelManager = mixPanelManager;
        this.mDatabase = dataBase;
    }

    public void openMaterial(MeterialUtility meterialUtility, boolean isOpen, boolean wannaFinish, boolean checkMandatory
            , DownloadMaterialListener mListener) {
        this.mListener = mListener;
        openMaterial(meterialUtility, isOpen, wannaFinish, checkMandatory);
    }

    /**
     * It is used to open all types of materials
     *
     * @param meterialUtility
     * @param isOpen
     * @param wannaFinish
     */
    public void openMaterial(MeterialUtility meterialUtility, boolean isOpen, boolean wannaFinish, boolean checkMandatory) {
        if (meterialUtility != null) {
            String resp = "";
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String playedTime = df.format(Calendar.getInstance().getTime());
            meterialUtility.setLastplayed(playedTime);
            if (!TextUtils.isEmpty(meterialUtility.getTitle()) && meterialUtility.getTitle().startsWith("Hospital")) {
                meterialUtility.setMaterial_type("HOSPITAL");
            }
            if (!TextUtils.isEmpty(meterialUtility.getIs_incremented()) && meterialUtility.getIs_incremented().equalsIgnoreCase("yes")) {
                if (checkMandatoryMaterials(meterialUtility)) {
                    if (checkMandatory)
                        DialogUtils.showDialog(mContext, mContext.getString(R.string.please_complete));
                    else {
                        CourseUtility courseUtility = FlowingCourseUtils.getCourseFromModuleId(meterialUtility.getModule_id());
                        if (courseUtility != null) {
                            Intent intent = new Intent(mContext, ModuleActivity.class);
                            intent.putExtra("course", courseUtility);
                            intent.putExtra("mod_id", meterialUtility.getModule_id());
                            intent.putExtra("is_link", false);
                            mContext.startActivity(intent);
                            Toast.makeText(mContext, mContext.getString(R.string.please_complete), Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(mContext, "Material not found!", Toast.LENGTH_LONG).show();
                    }
                    return;
                }
            }
            switch (meterialUtility.getMaterial_type().toUpperCase()) {
                case AppConstants.MaterialType.MULTIMEDIAQUIZ:
                    if (Integer.parseInt(meterialUtility.getRemainingAttempt()) <= 0) {
                        mixPanelManager.selectmaterialNoAttempts((Activity) mContext, WebServices.mLoginUtility.getEmail(), meterialUtility.getMaterial_id(), meterialUtility.getTitle(), meterialUtility.getMaterial_type());
                        ApiCallUtils.quizEvaluateApiCall(mContext, new WebServices(), meterialUtility);
                    } else {
                        resp = mDatabase.getQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                        openFile(meterialUtility, APIUtility.VIDEO_QUIZ_DETAILS, resp, isOpen, wannaFinish);
                    }
                    break;
                case AppConstants.MaterialType.QUIZ:
                    resp = mDatabase.getQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                        /*if (!TextUtils.isEmpty(resp) && meterialUtility.getTest_pattern().equalsIgnoreCase("RANDOM") && WebServices.isNetworkAvailable(mContext)) {
                            Utils.deleteFile(mContext, meterialUtility, mDatabase);
                            //resp = "";
                        }*/
                    openFile(meterialUtility, APIUtility.QUIZ_DETAILS, resp, isOpen, wannaFinish);
//                        mListener.onSuccess(meterialUtility);
                    break;
                case AppConstants.MaterialType.CERTIFICATE:
                    openFile(meterialUtility, meterialUtility.getMaterial_media_file_url(), resp, isOpen, wannaFinish);
                    break;
                case AppConstants.MaterialType.HOSPITAL:
                    openHospital(meterialUtility, APIUtility.GET_All_HOSPITALS, isOpen, wannaFinish);
                    break;
                case AppConstants.MaterialType.SURVEY:
                    if (Integer.parseInt(meterialUtility.getRemainingAttempt()) <= 0) {
                        mixPanelManager.selectmaterialNoAttempts((Activity) mContext, WebServices.mLoginUtility.getEmail(), meterialUtility.getMaterial_id(), meterialUtility.getTitle(), meterialUtility.getMaterial_type());
                        Toast.makeText(mContext, "You have exceeded the allowed number of Attempts!", Toast.LENGTH_SHORT).show();
                    } else {
                        resp = mDatabase.getSurveyData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                        openFile(meterialUtility, APIUtility.SURVEY_DETAILS, resp, isOpen, wannaFinish);
                    }
                    break;
                case AppConstants.MaterialType.CONTENTINSHORT:
                    resp = mDatabase.getSurveyData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                    openFile(meterialUtility, APIUtility.CONTENTINSHORTS_DETAILS, resp, isOpen, wannaFinish);
                    //mListener.onSuccess(meterialUtility);
                    break;
                case AppConstants.MaterialType.IMAGECARD:
                    resp = mDatabase.getFlashCardData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                    openFile(meterialUtility, APIUtility.IMAGE_FLASHCARD_DETAILS, resp, isOpen, wannaFinish);
                    break;
                case AppConstants.MaterialType.FLASHCARD:
                    resp = mDatabase.getFlashCardData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                    openFile(meterialUtility, APIUtility.FLASHCARD_DETAILS, resp, isOpen, wannaFinish);
                    break;

                case AppConstants.MaterialType.VIDEO:
                    //mListener.onSuccess(meterialUtility);
                case AppConstants.MaterialType.FLASH:
                case AppConstants.MaterialType.CONTENT:
                case AppConstants.MaterialType.AUDIO:
                case AppConstants.MaterialType.PDF:
                case AppConstants.MaterialType.FULL_TEXT:
                case AppConstants.MaterialType.TINCAN_SCROM:
                    openMedia(meterialUtility, isOpen, wannaFinish);
                    break;
                case AppConstants.MaterialType.IMAGE:
                case AppConstants.MaterialType.LINK:
                    openActivity(meterialUtility, isOpen, wannaFinish);
                    break;
            }
        }
    }

    private boolean checkMandatoryMaterials(MeterialUtility meterialUtility) {
        boolean isMandatory = false;
        ArrayList<MeterialUtility> meterialUtilities = FlowingCourseUtils.getMaterialsFromModule(meterialUtility.getCourse_id(), meterialUtility.getModule_id());
        if (meterialUtilities != null && meterialUtilities.size() > 0) {
            int position = FlowingCourseUtils.getPositionOfMeterial(meterialUtility.getMaterial_id(), meterialUtilities);
            for (int i = 0; i < meterialUtilities.size(); i++) {
                MeterialUtility meterialUtility1 = meterialUtilities.get(i);
                if (i == position)
                    break;
                if (meterialUtility1.getIs_incremented().equalsIgnoreCase("yes")) {
                    if (!meterialUtility1.getIsComplete()) {
                        isMandatory = true;
                        break;
                    }

                }
            }
        }
        return isMandatory;
    }

    private void openHospital(MeterialUtility meterialUtility, String apiUrl, boolean isOpen, boolean wannaFinish) {
        String resp = mDatabase.getHospitalsData(WebServices.mLoginUtility.getUser_id());
        if (TextUtils.isEmpty(resp)) {
            if (WebServices.isNetworkAvailable(mContext))
                downloadFiles(meterialUtility, APIUtility.GET_All_HOSPITALS, isOpen, wannaFinish);
            else
                Toast.makeText(mContext, R.string.no_internet, Toast.LENGTH_SHORT).show();
        } else {
            new WebServices().getAllHospitals(resp);
            openActivity(meterialUtility, isOpen, wannaFinish);
        }
    }

    /**
     * It is used to open quiz ,flashcards types of data which we have to store in database
     *
     * @param meterialUtility
     * @param apiUrl
     * @param resp
     * @param isOpen
     * @param wannaFinish
     */
    private void openFile(MeterialUtility meterialUtility, String apiUrl, String resp, boolean isOpen, boolean wannaFinish) {
        if (TextUtils.isEmpty(resp)/* || meterialUtility.getIs_offline_available().equalsIgnoreCase("NO")*/) {
            if (WebServices.isNetworkAvailable(mContext))
                downloadFiles(meterialUtility, apiUrl, isOpen, wannaFinish);
            else
                Toast.makeText(mContext, R.string.no_internet, Toast.LENGTH_SHORT).show();
        } else {
            if (mListener == null)
                openActivity(meterialUtility, isOpen, wannaFinish);
            else
                mListener.onSuccess(meterialUtility);

        }
    }

    private void openMediaActivity(MeterialUtility meterialUtility, boolean isOpen, boolean wannaFinish) {
        openMediaActivity(meterialUtility, isOpen, wannaFinish, true);
    }

    /**
     * It is used to open media activities in which we have to fatch data from local storage
     *
     * @param meterialUtility
     * @param isOpen
     * @param wannaFinish
     */
    private void openMediaActivity(MeterialUtility meterialUtility, boolean isOpen, boolean wannaFinish, boolean isOfflineAvailable) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        meterialUtility.setMaterialStartTime(df.format(c.getTime()));
        mixPanelManager.selectmaterial((Activity) mContext, WebServices.mLoginUtility.getEmail(), meterialUtility.getCourse_name(), meterialUtility.getTitle(), meterialUtility.getMaterial_type(), false);
        meterialUtility.setSeen(true);
        File decodedfile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());
        Uri uri = null;
        if (decodedfile != null)
            uri = Uri.parse(decodedfile.getAbsolutePath());
        switch (meterialUtility.getMaterial_type().toUpperCase()) {
            case AppConstants.MaterialType.PDF:
                Intent intent = new Intent(mContext, PDFViewerActivity.class);
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(uri);
                intent.putExtra("meterial_utility", meterialUtility);
                intent.putExtra("filename", decodedfile.getAbsolutePath());
                //if document protected with password
                intent.putExtra("password", "encrypted PDF password");

                //if you need highlight link boxes
                intent.putExtra("linkhighlight", true);
//                    intent.putExtra("filename", filename);
                intent.putExtra("id", meterialUtility.getMaterial_id());
                //if you don't need device sleep on reading document
                intent.putExtra("idleenabled", false);

                //set true value for horizontal page scrolling, false value for vertical page scrolling
                intent.putExtra("horizontalscrolling", true);

                //document name
                intent.putExtra("docname", meterialUtility.getTitle());

                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.FULL_TEXT:
                Intent i = new Intent(mContext, MainActivity.class);
                System.out.println("==file path ==" + decodedfile.getAbsolutePath());
                i.putExtra("filename", decodedfile.getAbsolutePath());
                i.putExtra("meterial_utility", meterialUtility);
                i.putExtra("id", meterialUtility.getMaterial_id());
                i.putExtra("meterial_utility", meterialUtility);
                i.putExtra("title", meterialUtility.getTitle());
                mContext.startActivity(i);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.AUDIO:
                intent = new Intent(mContext, Audio_Player_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("meterial_utility", meterialUtility);
                if (decodedfile != null && decodedfile.exists())
                    bundle.putString("file_path", decodedfile.getAbsolutePath());
                else
                    bundle.putString("file_path", null);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.VIDEO:
                intent = new Intent(mContext, ExoPlayerVideoActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("meterial utility", meterialUtility);
                intent.putExtras(bundle1);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.CONTENT:
                intent = new Intent(mContext, ContentInShortsActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("meterial_utility", meterialUtility);
                bundle.putString("file_path", decodedfile.getAbsolutePath());
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.FLASH:
                intent = new Intent(mContext, OnlineScromActivity.class);
                intent.putExtra("meterial_utility", meterialUtility);
                intent.putExtra("url", meterialUtility.getScrom_url());
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.TINCAN_SCROM:
                decodedfile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());

                if (decodedfile.exists()) {
                    openTincanActivity(meterialUtility, isOpen, wannaFinish);
                } else {
                    intent = new Intent(mContext, OnlineScromActivity.class);
                    intent.putExtra("meterial_utility", meterialUtility);
                    intent.putExtra("url", meterialUtility.getTincan_url());
                    mContext.startActivity(intent);
                    if (wannaFinish)
                        ((Activity) mContext).finish();
                }
               /* intent = new Intent(mContext, OnlineScromActivity.class);
                intent.putExtra("meterial_utility", meterialUtility);
                intent.putExtra("url", meterialUtility.getTincan_url());
                mContext.startActivity(intent);*/
                break;
            default:
                Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * It is used to download material from server which we have to save in database
     *
     * @param meterialUtility
     * @param apiUrl
     * @param isOpen
     * @param wannaFinish
     */
    private void downloadFiles(final MeterialUtility meterialUtility, String apiUrl, final boolean isOpen, final boolean wannaFinish) {
        mixPanelManager.selectDownloadmaterial((Activity) mContext, WebServices.mLoginUtility.getEmail(), meterialUtility.getCourse_name(), meterialUtility.getTitle(), meterialUtility.getMaterial_type());
        final ProgressDialog dialog = ProgressDialog.show(mContext, "", "Please wait..");
        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("material_id", meterialUtility.getMaterial_id()));
        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
        nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
        nameValuePair.add(new BasicNameValuePair("assign_material_id", meterialUtility.getAssign_material_id()));
        new GenericApiCall(mContext, apiUrl, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {

                String resp = (String) result;
                if (!TextUtils.isEmpty(resp)) {
                    switch (meterialUtility.getMaterial_type().toUpperCase()) {
                        case AppConstants.MaterialType.MULTIMEDIAQUIZ:
                        case AppConstants.MaterialType.QUIZ:
                            resp = new WebServices().parseQuiz(resp);
                            mDatabase.addDataToDb(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), resp, mDatabase.QUIZTABLE);
                            break;
                        case AppConstants.MaterialType.CONTENTINSHORT:
                            mDatabase.addDataToDb(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), resp, mDatabase.SURVEYTABLE);
                            break;
                        case AppConstants.MaterialType.SURVEY:
                            mDatabase.addDataToDb(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), resp, mDatabase.SURVEYTABLE);
                            break;
                        case AppConstants.MaterialType.IMAGECARD:
                            mDatabase.addDataToDb(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), resp, mDatabase.FLASHCARDTABLE);
                            new WebServices().getImageFlashcardData(resp);
                            break;
                        case AppConstants.MaterialType.FLASHCARD:
                            mDatabase.addDataToDb(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), resp, mDatabase.FLASHCARDTABLE);
                            break;
                        case AppConstants.MaterialType.HOSPITAL:
                            mDatabase.addHospitalsData(WebServices.mLoginUtility.getUser_id(), resp);
                            new WebServices().getAllHospitals(resp);
                            break;
                    }
                    if (dialog != null && !((Activity) mContext).isFinishing())
                        dialog.dismiss();
                    if (mListener == null)
                        openActivity(meterialUtility, isOpen, wannaFinish);
                    else
                        mListener.onSuccess(meterialUtility);
                } else {
                    if (dialog != null && !((Activity) mContext).isFinishing())
                        dialog.dismiss();
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(ErrorModel error) {
                if (dialog != null && !((Activity) mContext).isFinishing())
                    dialog.dismiss();
                Toast.makeText(mContext, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
            }
        }).execute();
    }

    /**
     * It is used to open all types of activities in which we have to fatch data from database
     *
     * @param meterialUtility
     * @param isOpen
     * @param wannaFinish
     */
    public void openActivity(MeterialUtility meterialUtility, boolean isOpen, boolean wannaFinish) {
        Intent intent = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        meterialUtility.setMaterialStartTime(df.format(c.getTime()));
        mixPanelManager.selectmaterial((Activity) mContext, WebServices.mLoginUtility.getEmail(), meterialUtility.getCourse_name(), meterialUtility.getTitle(), meterialUtility.getMaterial_type(), false);
        meterialUtility.setSeen(true);
        Bundle bundle = new Bundle();
        switch (meterialUtility.getMaterial_type().toUpperCase()) {
            case AppConstants.MaterialType.MULTIMEDIAQUIZ:
                intent = new Intent(mContext, AboutVideoQuizActivity.class);
                bundle.putSerializable("meterial_utility", meterialUtility);
                bundle.putString("coursename", meterialUtility.getCourse_name());
                bundle.putString("resume", "no");
                bundle.putString("res_data", "");
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                mixPanelManager.startquizflash((Activity) mContext, WebServices.mLoginUtility.getEmail(),
                        meterialUtility.getTitle(), meterialUtility.getMaterial_type());
                break;
            case AppConstants.MaterialType.QUIZ:
                intent = new Intent(mContext, AboutQuiz_Activity.class);
                bundle.putSerializable("meterial_utility", meterialUtility);
                bundle.putString("resume", "no");
                bundle.putString("res_data", "");
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                mixPanelManager.startquizflash((Activity) mContext, WebServices.mLoginUtility.getEmail(),
                        meterialUtility.getTitle(), meterialUtility.getMaterial_type());
                break;
            case AppConstants.MaterialType.CONTENTINSHORT:
                intent = new Intent(mContext, NewContentInShortsActivity.class);
                bundle.putSerializable("meterial_utility", meterialUtility);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.SURVEY:
                intent = new Intent(mContext, SurveyActivity.class);
                intent.putExtra("meterial_utility", meterialUtility);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                mixPanelManager.startquizflash((Activity) mContext, WebServices.mLoginUtility.getEmail(),
                        meterialUtility.getTitle(), meterialUtility.getMaterial_type());
                break;
            case AppConstants.MaterialType.IMAGECARD:
                intent = new Intent(mContext, Image_Quiz_Activity.class);
                intent.putExtra("meterial_utility", meterialUtility);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.FLASHCARD:
                if (Constants.ORGANIZATION_ID.equalsIgnoreCase("38") && Constants.BRANCH_ID.
                        equalsIgnoreCase("56")) {
                    if (WebServices.isNetworkAvailable(mContext)) {
                        intent = new Intent(mContext, AboutFlashCard_Activity.class);
                        intent.putExtra("meterial_utility", meterialUtility);
                        mContext.startActivity(intent);
                        mixPanelManager.startquizflash((Activity) mContext, WebServices.mLoginUtility.getEmail(),
                                meterialUtility.getTitle(), meterialUtility.getMaterial_type());
                        if (wannaFinish)
                            ((Activity) mContext).finish();
                    } else {
                        Toast.makeText(mContext, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    intent = new Intent(mContext, AboutFlashCard_Activity.class);
                    intent.putExtra("meterial_utility", meterialUtility);
                    mContext.startActivity(intent);
                    mixPanelManager.startquizflash((Activity) mContext, WebServices.mLoginUtility.getEmail(),
                            meterialUtility.getTitle(), meterialUtility.getMaterial_type());
                    if (wannaFinish)
                        ((Activity) mContext).finish();
                }
                break;
            case AppConstants.MaterialType.IMAGE:
                intent = new Intent(mContext, FullScreenImageActivity.class);
                intent.putExtra("imgurl", meterialUtility.getMaterial_media_file_url());
                intent.putExtra("title", meterialUtility.getTitle());
                intent.putExtra("meterial_utility", meterialUtility);
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
            case AppConstants.MaterialType.LINK:
                if (WebServices.isNetworkAvailable(mContext)) {
                    intent = new Intent(mContext, Link_Activity.class);
                    intent.putExtra("meterial_utility", meterialUtility);
                    mContext.startActivity(intent);
                    if (wannaFinish)
                        ((Activity) mContext).finish();
                } else
                    Toast.makeText(mContext, R.string.no_internet, Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.MaterialType.HOSPITAL:
                intent = new Intent(mContext, HospitalSearch_Activity.class);
                intent.putExtra("title", meterialUtility.getTitle());
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();
                break;
        }
    }


    /**
     * It is used to open and download media materials
     *
     * @param meterialUtility
     * @param isOpen
     * @param wannaFinish
     */
    public void openMedia(final MeterialUtility meterialUtility, final boolean isOpen, final boolean wannaFinish) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        File decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());
        meterialUtility.setLastplayed(formattedDate);
        meterialUtility.setMaterialStartTime(df.format(c.getTime()));
        if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.VIDEO)) {
            decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + ".mp4");
            if (!decodefile.exists())
                decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());

        } else if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM))
            decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip");
        if (!TextUtils.isEmpty(meterialUtility.getMaterial_type()) && meterialUtility.getMaterial_type().equalsIgnoreCase("video") && !TextUtils.isEmpty(meterialUtility.getMaterial_media_file_type()) && !meterialUtility.getMaterial_media_file_type().equalsIgnoreCase("null") &&
                meterialUtility.getMaterial_media_file_type().equalsIgnoreCase("URL")) {
            if (WebServices.isNetworkAvailable(mContext))
                openMediaActivity(meterialUtility, isOpen, wannaFinish, false);
            else
                Toast.makeText(mContext, mContext.getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        } else {
            if (meterialUtility.getIs_offline_available().equalsIgnoreCase("YES")) {
                if (decodefile.exists()) {
                    openMediaActivity(meterialUtility, isOpen, wannaFinish);
                } else {
                    if (mListener == null)
                        if (meterialUtility.getMaterial_type().equalsIgnoreCase("video") || meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.AUDIO)) {
                            DialogUtils.showDialog(mContext, mContext.getString(R.string.offline_or_stream), new Runnable() {
                                @Override
                                public void run() {
                                    downloadMediaLogic(meterialUtility, isOpen, wannaFinish);
                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    openMediaActivity(meterialUtility, isOpen, wannaFinish, false);
                                }
                            });
                        } else {
                            downloadMediaLogic(meterialUtility, isOpen, wannaFinish);
                        }
                    else
                        mListener.onSuccess(meterialUtility);
                }
            } else {
                if (meterialUtility.getIs_offline_available().equalsIgnoreCase("NO") && meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.PDF)) {
                    if (WebServices.isNetworkAvailable(mContext)) {
                        Intent intent = new Intent(mContext, Link_Activity.class);
                        intent.putExtra("meterial_utility", meterialUtility);
                        mContext.startActivity(intent);
                        if (wannaFinish)
                            ((Activity) mContext).finish();
                    } else
                        Toast.makeText(mContext, R.string.no_internet, Toast.LENGTH_SHORT).show();
                } else if (meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.VIDEO) || meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.AUDIO)
                        || meterialUtility.getMaterial_type().equalsIgnoreCase(AppConstants.MaterialType.TINCAN_SCROM))
                    if (decodefile.exists()) {
                        decodefile.delete();
                        openMediaActivity(meterialUtility, isOpen, wannaFinish, false);
                    } else {
                        openMediaActivity(meterialUtility, isOpen, wannaFinish, false);
                    }
                else {
                    downloadMediaLogic(meterialUtility, isOpen, wannaFinish);
                }
            }
        }

    }

    /**
     * It is used to downloadmedia and check internet is available or not
     *
     * @param meterialUtility
     * @param isOpen
     * @param wannaFinish
     */
    public void downloadMediaLogic(final MeterialUtility meterialUtility, final boolean isOpen, final boolean wannaFinish) {
        if (WebServices.isNetworkAvailable(mContext))
            if (TextUtils.isEmpty(meterialUtility.getMaterial_media_file_url()) || meterialUtility.getMaterial_media_file_url().equalsIgnoreCase("null"))
                Toast.makeText(mContext, "Sorry! Content is missing", Toast.LENGTH_SHORT).show();
            else
                downloadMedia(meterialUtility, isOpen, wannaFinish);
        else
            Toast.makeText(mContext, R.string.no_internet, Toast.LENGTH_SHORT).show();
    }

    /**
     * It is used to download media from server and save in local storage
     *
     * @param meterialUtility
     */
    public void downloadMedia(final MeterialUtility meterialUtility, final String url1) {
        mixPanelManager.selectDownloadmaterial((Activity) mContext, WebServices.mLoginUtility.getEmail(), meterialUtility.getCourse_name(), meterialUtility.getTitle(), meterialUtility.getMaterial_type());

        final Dialog mDialog = new Dialog(mContext);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mDialog.dismiss();

                if (msg.what == 0) {
                    if (FILEPATH != null) {

                        Intent intent = new Intent(mContext, ViewCertificateActivity.class);
                        intent.putExtra("filename", FILEPATH);
                        mContext.startActivity(intent);

                    } else {
                        try {
                            APIUtility.isDeletedFileFromURL(mContext, meterialUtility.getMaterial_id());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                        AppUtility.showToast(context, MSG);
                        Toast.makeText(mContext, MSG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        APIUtility.isDeletedFileFromURL(mContext, meterialUtility.getMaterial_id());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    AppUtility.showToast(context, MSG);
                    Toast.makeText(mContext, MSG, Toast.LENGTH_LONG).show();
                }
            }
        };
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        final ProgressBar mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        final TextView title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        final TextView total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        final TextView totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        final TextView abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDownload = false;
                FILEPATH = null;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(1);
            }
        });
        mDialog.show();

        new Thread() {
            @Override
            public void run() {
                int lenghtOfFile = 0;
                int count;
                File file = null;

                try {
                    URL url = new URL(url1);

                    URLConnection conection = url.openConnection();
                    conection.setConnectTimeout(10000);
                    conection.setReadTimeout(10000);
                    conection.connect();
                    // getting file length
                    lenghtOfFile = conection.getContentLength();
                    System.out.println("=====file length===" + (lenghtOfFile / (1024 * 1024)));
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);


                    // Output stream to write file
                    //Download epub file in app private directory /sdcard/Android/data/<package-name>/files/chapters
                    file = getFileFromURL(mContext, meterialUtility.getMaterial_id() + ".pdf");
                    OutputStream output = new FileOutputStream(file);
                    byte data[] = new byte[1024];
//                    runnable.run();
                    long total = 0;
                    while ((count = input.read(data)) > 0) {
                        if (isDownload) {
                            total += count;
                            totalVal = "" + (int) ((total * 100) / lenghtOfFile);
                            totallenghtOfFile = lenghtOfFile;
//                            publishProgress();
//                            runnable.notify();//run();
                            // writing data to file

                            ((Activity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    abort_btn.setEnabled(true);
                                    if (totallenghtOfFile > 0) {
                                        String total = (totallenghtOfFile / (1024 * 1000f)) + "";
                                        String[] t = total.split("\\.");
                                        total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                    } else {
                                        total_file_txt.setText("0 MB");
                                    }
                                    title_txt.setText("Fetching... Please wait!");
                                    if (!totalVal.contains("-")) {
                                        mBar.setProgress(Integer.parseInt(totalVal));
                                        totalPer_txt.setText(Integer.valueOf(totalVal) + " %");
                                    } else {
//                                        isDownload = false;
//                                        MSG = "Network issues.. You have been Timed-out";
//                                        mBar.setProgress(0);
//                                        totalPer_txt.setText("0 %");
//                                        handler.sendEmptyMessage(2);
                                    }
                                }
                            });
                            output.write(data, 0, count);
                        } else {
                            // flushing output
                            output.flush();

                            // closing streams
                            output.close();
                            input.close();
                            return;
                        }
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    MSG = "Network issues.. You have been Timed-out";
                    FILEPATH = null;
                    handler.sendEmptyMessage(2);
                    return;
                }

//                try {
//                    new AESFileEncryption("DES/ECB/PKCS5Padding", file.getAbsolutePath()).encrypt();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                FILEPATH = file.getAbsolutePath();


                handler.sendEmptyMessage(0);
            }

        }.start();


    }

    /**
     * It is used to download media from server and save in local storage
     *
     * @param meterialUtility
     * @param isOpen
     * @param wannaFinish
     */
    private void downloadMedia(final MeterialUtility meterialUtility, final boolean isOpen, final boolean wannaFinish) {
        mixPanelManager.selectDownloadmaterial((Activity) mContext, WebServices.mLoginUtility.getEmail(), meterialUtility.getCourse_name(), meterialUtility.getTitle(), meterialUtility.getMaterial_type());
        final Dialog mDialog = new Dialog(mContext);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mDialog.dismiss();
                if (msg.what == 0) {
                    if (FILEPATH != null) {
                        if (mListener == null)
                            openMediaActivity(meterialUtility, isOpen, wannaFinish);
                        else
                            mListener.onSuccess(meterialUtility);

//                                }
//                            });
//                            dialog.show();
//
//                        } else {
//                        }

                    } else {
                        try {
                            APIUtility.isDeletedFileFromURL(mContext, meterialUtility.getMaterial_id());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                        AppUtility.showToast(context, MSG);
                        Toast.makeText(mContext, MSG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        APIUtility.isDeletedFileFromURL(mContext, meterialUtility.getMaterial_id());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    AppUtility.showToast(context, MSG);
                    Toast.makeText(mContext, MSG, Toast.LENGTH_LONG).show();
                }
            }
        };

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        final ProgressBar mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        final TextView title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        final TextView total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        final TextView totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        final TextView abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDownload = false;
                FILEPATH = null;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(1);
            }
        });
        mDialog.show();

        new Thread() {
            @Override
            public void run() {
                int lenghtOfFile = 0;
                int count;
                File file = null;

                try {
                    URL url = new URL(meterialUtility.getMaterial_media_file_url());

                    URLConnection conection = url.openConnection();
                    conection.setConnectTimeout(10000);
                    conection.setReadTimeout(10000);
                    conection.connect();
                    // getting file length
                    lenghtOfFile = conection.getContentLength();
                    System.out.println("=====file length===" + (lenghtOfFile / (1024 * 1024)));
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);


                    // Output stream to write file
                    //Download epub file in app private directory /sdcard/Android/data/<package-name>/files/chapters
                    file = getFileFromURL(mContext, meterialUtility.getMaterial_id());
                    OutputStream output = new FileOutputStream(file);
                    byte data[] = new byte[1024];
//                    runnable.run();
                    long total = 0;
                    while ((count = input.read(data)) > 0) {
                        if (isDownload) {
                            total += count;
                            totalVal = "" + (int) ((total * 100) / lenghtOfFile);
                            totallenghtOfFile = lenghtOfFile;
//                            publishProgress();
//                            runnable.notify();//run();
                            // writing data to file

                            ((Activity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    abort_btn.setEnabled(true);
                                    if (totallenghtOfFile > 0) {
                                        String total = (totallenghtOfFile / (1024 * 1000f)) + "";
                                        String[] t = total.split("\\.");
                                        total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                    } else {
                                        total_file_txt.setText("0 MB");
                                    }
                                    title_txt.setText("Fetching... Please wait!");
                                    if (!totalVal.contains("-")) {
                                        mBar.setProgress(Integer.parseInt(totalVal));
                                        totalPer_txt.setText(Integer.valueOf(totalVal) + " %");
                                    } else {
//                                        isDownload = false;
//                                        MSG = "Network issues.. You have been Timed-out";
//                                        mBar.setProgress(0);
//                                        totalPer_txt.setText("0 %");
//                                        handler.sendEmptyMessage(2);
                                    }
                                }
                            });
                            output.write(data, 0, count);
                        } else {
                            // flushing output
                            output.flush();

                            // closing streams
                            output.close();
                            input.close();
                            return;
                        }
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    MSG = "Network issues.. You have been Timed-out";
                    FILEPATH = null;
                    handler.sendEmptyMessage(2);
                    return;
                }

//                try {
//                    new AESFileEncryption("DES/ECB/PKCS5Padding", file.getAbsolutePath()).encrypt();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                FILEPATH = file.getAbsolutePath();


                handler.sendEmptyMessage(0);
            }

        }.start();


    }

    /**
     * It is used to get local path acc to material name
     *
     * @param context
     * @param mediaName
     * @return
     * @throws IOException
     */
    private File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());

        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    /**
     * It is used to open Tincan scorm
     *
     * @param meterialUtility
     * @param isOpen
     * @param wannaFinish
     */
    private void openTincanActivity(final MeterialUtility meterialUtility, final boolean isOpen, final boolean wannaFinish) {
        final ProgressDialog dialog = ProgressDialog.show(mContext, "", "Please wait..");
        final File decodedfile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip");
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                if (dialog != null && !((Activity) mContext).isFinishing()) {
                    dialog.dismiss();
                }
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                meterialUtility.setMaterialStartTime(df.format(c.getTime()));
                Intent intent = new Intent(mContext, OnlineScromActivity.class);
                String[] ss = meterialUtility.getScrom_url().split("/");
                String filename = ss[ss.length - 1];
                File file = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip/" + filename);
                if (file.exists()) {
                    String filepath = "file:///" + file.getAbsolutePath() + "?" + meterialUtility.getTincan_url().split("\\?")[1];
                    intent.putExtra("url", filepath);
                } else {
                    File file1 = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip/" + "shell/" + filename);
                    if (file1.exists()) {
                        String filepath = "file:///" + file1.getAbsolutePath() + "?" + meterialUtility.getTincan_url().split("\\?")[1];
                        intent.putExtra("url", filepath);
                    }
                }
//                String url = decodedfile.getAbsolutePath()+;
                intent.putExtra("meterial_utility", meterialUtility);
//                intent.putExtra("url",decodedfile.getAbsolutePath()+)
                mContext.startActivity(intent);
                if (wannaFinish)
                    ((Activity) mContext).finish();

            }

            ;
        };
        if (!decodedfile.exists()) {
            new Thread() {
                public void run() {

                    try {
//                        if (!TextUtils.isEmpty(meterialUtility.getTincan_vendor()) && meterialUtility.getTincan_vendor().equalsIgnoreCase("sleeve"))
                        FileHelper.unzipFolder(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id(), mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip");
//                        else
//                            FileHelper.unzip(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id(), mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip");
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        handler.sendEmptyMessage(0);
                    }

                }

                ;
            }.start();
        } else {
            handler.sendEmptyMessage(0);
        }
    }

}
