package com.chaptervitamins.newcode.server;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.EvaluationActivity;
import com.chaptervitamins.newcode.activities.LeaderboardActivity;
import com.chaptervitamins.newcode.activities.VideoQuizHistory;
import com.chaptervitamins.newcode.models.VideoQuizAllResponseModel;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.quiz.SpecificHistoryActivity;
import com.chaptervitamins.quiz.ViewCertificateActivity;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Vijay Antil on 10-01-2018.
 */

public class ApiCallUtils {

    public static void resultPublishApiCall(final Context mContext, final WebServices webServices, final DataBase mDatabase, final MeterialUtility meterialUtility) {
        if (WebServices.isNetworkAvailable(mContext)) {
            final ProgressDialog dialog = ProgressDialog.show(mContext, "", "Please wait...");
            dialog.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar));
            final ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("material_id", meterialUtility.getMaterial_id()));
            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("assign_material_id", meterialUtility.getAssign_material_id()));
            new GenericApiCall(mContext, APIUtility.QUIZ_DETAILS, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {

                    if (webServices.isValid((String) result)) {
                        mDatabase.addQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), (String) result);

                        new GenericApiCall(mContext, APIUtility.GETSPECIFICQUIZHISTORY2, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                            @Override
                            public void onSuccess(Object result) {
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                                if (webServices.isValid((String) result)) {
                                   JSONArray jsonArray= webServices.isResultData(((String) result));
                                   if(jsonArray!=null&&jsonArray.length()>0) {
                                       Intent intent = new Intent(mContext, SpecificHistoryActivity.class);
                                       intent.putExtra("resp", (String) result);
                                       intent.putExtra("meterial_utility", meterialUtility);
                                       mContext.startActivity(intent);
                                   }else{
                                       Toast.makeText(mContext, "Result has not been published for this Quiz, Please contact your Admin to know more.", Toast.LENGTH_LONG).show();
                                   }
                                } else {
                                    Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(ErrorModel error) {
                                if (dialog != null)
                                    dialog.dismiss();
                                if (error.getErrorDescription().equalsIgnoreCase("INVALID_SESSION")) {
                                    Utils.callInvalidSession(mContext, APIUtility.GETSPECIFICQUIZHISTORY2);
                                } else
                                    DialogUtils.showDialog(mContext, error.getErrorDescription());
                            }
                        }).execute();
                    } else {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                }

                @Override
                public void onError(ErrorModel error) {
                    if (dialog != null)
                        dialog.dismiss();
                    if (error.getErrorDescription().equalsIgnoreCase("INVALID_SESSION")) {
                        Utils.callInvalidSession(mContext, APIUtility.GETSPECIFICQUIZHISTORY2);
                    } else
                        DialogUtils.showDialog(mContext, error.getErrorDescription());
                }
            }).execute();
        } else
            Toast.makeText(mContext, mContext.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
    }

    public static void leaderboardApiCall(final Context mContext, final WebServices webServices, final DataBase mDatabase, final MeterialUtility meterialUtility) {
        if (WebServices.isNetworkAvailable(mContext)) {
            final ProgressDialog dialog = ProgressDialog.show(mContext, "", "Please wait...");
            dialog.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar));
            ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("material_id", meterialUtility.getMaterial_id()));
            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            new GenericApiCall(mContext, APIUtility.GETSPECIFICQUIZHISTORY, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    Intent intent = new Intent(mContext, LeaderboardActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("resp", (String)result);
                    bundle.putBoolean("show_details", false);
                    bundle.putSerializable("meterial_utility", meterialUtility);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }

                @Override
                public void onError(ErrorModel error) {
                    if (dialog != null)
                        dialog.dismiss();
                    if (error.getErrorDescription().equalsIgnoreCase("INVALID_SESSION")) {
                        Utils.callInvalidSession(mContext, APIUtility.GETSPECIFICQUIZHISTORY2);
                    } else
                        DialogUtils.showDialog(mContext, error.getErrorDescription());
                }
            }).execute();
        } else
            Toast.makeText(mContext, mContext.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
    }

    public static void showCertificate(Context context, MixPanelManager mixPanelManager, DataBase dataBase, MeterialUtility meterialUtility) {
//        meterialUtility.setMaterial_media_file_url(APIUtility.GENERATE_CERTIFICATE + meterialUtility.getMaterial_id() + "/" + WebServices.mLoginUtility.getUser_id());
//        meterialUtility.setMaterial_type("CERTIFICATE");
//        new MaterialOpenController(context,mixPanelManager,dataBase).openMaterial(meterialUtility,true,false);
        File dir = new File(context.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getTitle());
        if (dir.exists()) {
            Intent intent = new Intent(context, ViewCertificateActivity.class);
            intent.putExtra("filename", dir.getAbsoluteFile().getPath());
            context.startActivity(intent);
        } else {
            String url = APIUtility.GENERATE_CERTIFICATE + meterialUtility.getMaterial_id() + "/" + WebServices.mLoginUtility.getUser_id();
            if (WebServices.isNetworkAvailable(context))
                new MaterialOpenController(context, mixPanelManager, dataBase).downloadMedia(meterialUtility, url);
            else
                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public static void quizEvaluateApiCall(final Context mContext, final WebServices webServices, final String responseId, final String meterialId) {
        quizEvaluateApiCall(mContext, webServices, null, responseId, meterialId);
    }

    public static void quizEvaluateApiCall(final Context mContext, final WebServices webServices, MeterialUtility meterialUtility) {
        quizEvaluateApiCall(mContext, webServices, meterialUtility, "", "");
    }

    public static void quizEvaluateApiCall(final Context mContext, final WebServices webServices, final MeterialUtility meterialUtility, final String responseId, final String meterialId) {
        if (WebServices.isNetworkAvailable(mContext)) {
            final ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            if (meterialUtility != null) {
                nameValuePair.add(new BasicNameValuePair("assign_material_id", meterialUtility.getAssign_material_id()));
                nameValuePair.add(new BasicNameValuePair("material_id", meterialUtility.getMaterial_id()));
            } else
                nameValuePair.add(new BasicNameValuePair("material_id", meterialId));
            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
            nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
//            if (!TextUtils.isEmpty(meterialUtility.getResponse_id()) && !meterialUtility.getResponse_id().equalsIgnoreCase("null")) {
//                nameValuePair.add(new BasicNameValuePair("response_id", meterialUtility.getResponse_id()));
            final ProgressDialog dialog = ProgressDialog.show(mContext, "", "Please wait...");
            dialog.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar));
            new GenericApiCall(mContext, APIUtility.VIDEO_QUIZ_EVALLUATE, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    if (dialog != null)
                        dialog.dismiss();
                    if (webServices.isValid((String) result)) {
                        ArrayList<VideoQuizAllResponseModel> videoQuizAllResponseArrayList =
                                webServices.parseVideoQuizAllResponse(result.toString());
                        if (videoQuizAllResponseArrayList != null && videoQuizAllResponseArrayList.size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("meterial_utility", meterialUtility);
                            if (videoQuizAllResponseArrayList.size() == 1) {
                                bundle.putSerializable("res", videoQuizAllResponseArrayList.get(0).getAlVideoQuizResponse());
                                bundle.putSerializable("res_list", videoQuizAllResponseArrayList);
                                Intent intent = new Intent(mContext, EvaluationActivity.class);
                                intent.putExtras(bundle);
                                mContext.startActivity(intent);
                            } else {
                                if (!TextUtils.isEmpty(responseId)) {
                                    MeterialUtility meterialUtility1;
                                    if (meterialUtility == null) {
                                        meterialUtility1 = FlowingCourseUtils.getMaterialByMaterialId(meterialId);
                                        bundle.putSerializable("meterial_utility", meterialUtility1);
                                    } else
                                        bundle.putSerializable("meterial_utility", meterialUtility);

                                    getVideoResponse(videoQuizAllResponseArrayList, responseId);
                                    bundle.putSerializable("res", getVideoResponse(videoQuizAllResponseArrayList, responseId).getAlVideoQuizResponse());
                                    bundle.putSerializable("res_list", videoQuizAllResponseArrayList);
                                    Intent intent = new Intent(mContext, EvaluationActivity.class);
                                    intent.putExtras(bundle);
                                    mContext.startActivity(intent);
                                } else {
                                    bundle.putSerializable("res_list", videoQuizAllResponseArrayList);
                                    Intent intent = new Intent(mContext, VideoQuizHistory.class);
                                    intent.putExtras(bundle);
                                    mContext.startActivity(intent);
                                }
                            }
                        } else
                            DialogUtils.showDialog(mContext, "Evaluation is not completed yet");
                    }
                }

                @Override
                public void onError(ErrorModel error) {
                    if (dialog != null)
                        dialog.dismiss();
                    if (error.getErrorDescription().equalsIgnoreCase("INVALID_SESSION")) {
                        Utils.callInvalidSession(mContext, APIUtility.VIDEO_QUIZ_EVALLUATE);
                    } else
                        DialogUtils.showDialog(mContext, error.getErrorDescription());
                }
            }).execute();
            /*}else{
                DialogUtils.showDialog(mContext, "No Data Found");
            }*/
        } else
            Toast.makeText(mContext, mContext.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();

    }

    public static void loginApiCall(final Context mContext, final WebServices webServices, final DataBase dataBase, final boolean isHomeShow) {
        Toast.makeText(mContext, "Please sync your data!", Toast.LENGTH_SHORT).show();
        ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("email", WebServices.mLoginUtility.getEmail()));
        nameValuePair.add(new BasicNameValuePair("password", SplashActivity.mPref.getString("pass", "")));
        nameValuePair.add(new BasicNameValuePair("push_token", WebServices.mLoginUtility.getDevice().getPush_token()));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        new GenericApiCall(mContext, APIUtility.LOGIN, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                String resp = (String) result;
                boolean isSuccess = webServices.isLogined(resp);
                if (isSuccess) {
                    if (dataBase.isData(SplashActivity.mPref.getString("id", ""), "Login"))
                        dataBase.addLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), resp);
                    else {
                        dataBase.updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), resp);
                    }
//                    if (isHomeShow)
//                        Utils.callHomeActivity(mContext);

                } else {
                    Toast.makeText(mContext, "Something went wrong! Please contact to Admin.", Toast.LENGTH_SHORT).show();
//                    if (isHomeShow)
//                        Utils.callHomeActivity(mContext);
                }
            }

            @Override
            public void onError(ErrorModel error) {
                Toast.makeText(mContext, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
//                if (isHomeShow)
//                    Utils.callHomeActivity(mContext);
            }
        }).execute();
    }

    private static VideoQuizAllResponseModel getVideoResponse(ArrayList<VideoQuizAllResponseModel> videoQuizAllResponseArrayList, String responseId) {
        if (videoQuizAllResponseArrayList != null && videoQuizAllResponseArrayList.size() > 0 && !TextUtils.isEmpty(responseId)) {
            for (int i = 0; i < videoQuizAllResponseArrayList.size(); i++) {
                if (videoQuizAllResponseArrayList.get(i).getResponseId().equals(responseId))
                    return videoQuizAllResponseArrayList.get(i);
            }
        }
        return null;
    }
}

