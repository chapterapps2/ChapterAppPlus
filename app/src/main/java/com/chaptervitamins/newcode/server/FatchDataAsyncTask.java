package com.chaptervitamins.newcode.server;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.Favourites_Utils;
import com.chaptervitamins.utility.Like_Utility;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.chaptervitamins.utility.ReadResponseUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vijay Antil on 21-11-2017.
 */

public class FatchDataAsyncTask extends AsyncTask<String, Void, Boolean> {
    //  private ProgressDialog dialog = null;
    private boolean isshow = false;
    private boolean isFirstTime = false;
    private String error_msg = "";
    private Context mContext;
    private WebServices webServices = new WebServices();
    private DataBase dataBase;
    private FatchDataListener mListener;

    public FatchDataAsyncTask(Context context, boolean isShow) {
        this.mContext = context;
        this.mListener = (FatchDataListener) context;
        isshow = isShow;
        dataBase = DataBase.getInstance(mContext);
        if (isShow) {
            if (!((Activity) mContext).isFinishing()) {
              /*//  dialog = new ProgressDialog(context);
                dialog.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar));
                dialog.setTitle("");
                dialog.setMessage("Please wait...");
//                sync_img.setImageResource(R.drawable.yellow);
                dialog.show();*/
            }
        }
    }

    public FatchDataAsyncTask(Context context, boolean isShow, boolean isFirstTime) {
        this.mContext = context;
        isshow = isShow;
        this.mListener = (FatchDataListener) context;
        this.isFirstTime = isFirstTime;
        dataBase = DataBase.getInstance(mContext);
        if (isShow && isFirstTime) {
            if (!((Activity) mContext).isFinishing()) {
              /*  dialog = new ProgressDialog(context);
                dialog.setIndeterminateDrawable(mContext.getResources().getDrawable(R.drawable.progress_bar));
                dialog.setTitle("");
                dialog.setMessage("Please wait...");
                dialog.show();*/
            }
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try {
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            String resp = "";
            if (params[0].equalsIgnoreCase("course")) {
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                resp = webServices.callServices(nameValuePair, APIUtility.ASSIGNCOURSE);
                Log.d(" Response:", resp.toString());
                if (webServices.isValid(resp)) {
                    if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "MyCourse"))
                        dataBase.addCourseData(WebServices.mLoginUtility.getUser_id(), resp);
                    else {
                        dataBase.updateCourseData(WebServices.mLoginUtility.getUser_id(), resp);
                    }
                    HomeActivity.courseUtilities.clear();
                    HomeActivity.courseUtilities.addAll(webServices.getAssignCourseData(resp));
                    System.out.println("====courseUtilities====" + HomeActivity.courseUtilities.size());

                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    resp = webServices.getLogin(nameValuePair, APIUtility.ORGANIZATION_DETAILS);
                    if (!resp.equalsIgnoreCase("")) {
                        if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "Organisation_setting"))
                            dataBase.addOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                        else {
                            dataBase.updateOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                        }
                        webServices.parseOrganization(resp);
                        AutoLogout.autoLogout(mContext);
                    }
                } else {

                    return false;
                }
            } else {
                Log.d("Response", "empty");

                if (isFirstTime) {
                    Log.d(" Response:", "firstTime");
                    ArrayList<ReadResponseUtility> list = dataBase.getResponseData();
                    for (int i = 0; i < list.size(); i++) {
                        ReadResponseUtility readResponseUtility = list.get(i);
                        nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("user_id", readResponseUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("material_id", readResponseUtility.getMaterial_id()));
                        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("correct_question", readResponseUtility.getCorrect_question()));
                        nameValuePair.add(new BasicNameValuePair("finish_time", readResponseUtility.getFinish_time()));
                        nameValuePair.add(new BasicNameValuePair("incorrect_question", readResponseUtility.getIncorrect_question()));
                        nameValuePair.add(new BasicNameValuePair("question_response", readResponseUtility.getQuiz_response()));
                        nameValuePair.add(new BasicNameValuePair("response_type", readResponseUtility.getResponse_type()));
                        nameValuePair.add(new BasicNameValuePair("result", readResponseUtility.getResult()));
                        nameValuePair.add(new BasicNameValuePair("start_time", readResponseUtility.getStart_time()));
                        nameValuePair.add(new BasicNameValuePair("taken_device", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("time_taken", readResponseUtility.getTime_taken()));
                        nameValuePair.add(new BasicNameValuePair("coins_allocated", readResponseUtility.getCoins_allocated()));
                        nameValuePair.add(new BasicNameValuePair("assign_material_id", readResponseUtility.getAssign_material_id()));
                        nameValuePair.add(new BasicNameValuePair("organization_id", readResponseUtility.getOrganization_id()));
                        nameValuePair.add(new BasicNameValuePair("branch_id", readResponseUtility.getBranch_id()));
                        nameValuePair.add(new BasicNameValuePair("module_id", readResponseUtility.getModuleId()));
                        nameValuePair.add(new BasicNameValuePair("seen", readResponseUtility.getSeen_count()));
                        nameValuePair.add(new BasicNameValuePair("total", readResponseUtility.getTotal_count()));
                        nameValuePair.add(new BasicNameValuePair("completion_per", readResponseUtility.getCompletion_per()));
                        if (readResponseUtility.getCompletion_per().equals("100"))
                            nameValuePair.add(new BasicNameValuePair("is_completed", "YES"));
                        else
                            nameValuePair.add(new BasicNameValuePair("is_completed", "NO"));
                        String resp1 = webServices.callServices(nameValuePair, APIUtility.SUBMIT_RESPONSE);
                        Log.d(" Response:", resp1.toString());
                        if (webServices.isValid(resp1)) {
                            DataBase.getInstance(mContext).deleteRowGetResponse(list.get(i).getMaterial_id(), WebServices.mLoginUtility.getUser_id(), list.get(i).getStart_time());
                        } else {
                            String errorCode = webServices.getErrorCode(resp);
                            if (!TextUtils.isEmpty(errorCode) && errorCode.equalsIgnoreCase("1005"))
                                DataBase.getInstance(mContext).deleteRowGetResponse(readResponseUtility.getMaterial_id(), readResponseUtility.getUser_id(), readResponseUtility.getStart_time());
                        }
                    }
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
//                    if (noDataFount.equalsIgnoreCase("3"))
                    resp = webServices.callServices(nameValuePair, APIUtility.ASSIGNCOURSE);
                    Log.d(" Response:", APIUtility.ASSIGNCOURSE + resp.toString());
                    if (webServices.isValid(resp)) {
                        if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "MyCourse"))
                            dataBase.addCourseData(WebServices.mLoginUtility.getUser_id(), resp);
                        else {
                            dataBase.updateCourseData(WebServices.mLoginUtility.getUser_id(), resp);
                        }
                        HomeActivity.courseUtilities.clear();
                        HomeActivity.courseUtilities.addAll(webServices.getAssignCourseData(resp));

                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        resp = webServices.getLogin(nameValuePair, APIUtility.GETALL_NOTIFICATION);
                        Log.d(" Response:", APIUtility.GETALL_NOTIFICATION + resp.toString());
                        if (!resp.equalsIgnoreCase("")) {
                            if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "Notifications"))
                                dataBase.addNotificationData(WebServices.mLoginUtility.getUser_id(), resp);
                            else {
                                dataBase.updateNotificationData(WebServices.mLoginUtility.getUser_id(), resp);
                            }
                            webServices.getAllNotification(resp);
                        }


                    } else {
//                    HomeActivity.courseUtilities.clear();

                    }

                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    resp = webServices.callServices(nameValuePair, APIUtility.GETREADRESPONSE);
                    Log.d(" Response:", APIUtility.GETREADRESPONSE + resp.toString());
                    if (webServices.isValid(resp)) {
                        if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "MyReadResponse"))
                            dataBase.addReadResponseData(WebServices.mLoginUtility.getUser_id(), resp);
                        else {
                            dataBase.updateReadResponseData(WebServices.mLoginUtility.getUser_id(), resp);
                        }

                        HomeActivity.mReadResponse = webServices.read_response_method(resp);
                    }

                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
                    resp = webServices.getLogin(nameValuePair, APIUtility.ORGANIZATION_DETAILS);
                    Log.d(" Response:", APIUtility.ORGANIZATION_DETAILS + resp.toString());
                    if (!resp.equalsIgnoreCase("")) {
                        if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "Organisation_setting"))
                            dataBase.addOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                        else {
                            dataBase.updateOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id(), resp);
                        }
                        webServices.parseOrganization(resp);
                    }
                    webServices.setProgress(dataBase, "", true);
                } else {
                    try {
                        Log.d(" Response:", "secondtime");
                        for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
                            ArrayList<ModulesUtility> modulesUtilityArrayList = HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList();
                            for (int j = 0; j < modulesUtilityArrayList.size(); j++) {
                                ArrayList<MeterialUtility> meterialUtilities = modulesUtilityArrayList.get(j).getMeterialUtilityArrayList();
                                for (int k = 0; k < meterialUtilities.size(); k++) {
                                    for (int i1 = 0; i1 < HomeActivity.tempCourseUtilities.size(); i1++) {
                                        if (HomeActivity.courseUtilities.get(i).getCourse_id().equals(HomeActivity.tempCourseUtilities.get(i1).getCourse_id())) {
                                            ArrayList<ModulesUtility>  modulesUtilityArrayList2 = HomeActivity.tempCourseUtilities.get(i1).getModulesUtilityArrayList();
                                            for (int j2 = 0; j2 < modulesUtilityArrayList2.size(); j2++) {
                                                if (modulesUtilityArrayList.get(j).getModule_id().equals(modulesUtilityArrayList2.get(j2).getModule_id())) {
                                                    ArrayList<MeterialUtility> meterialUtilities2 = modulesUtilityArrayList2.get(j2).getMeterialUtilityArrayList();
                                                    for (int k2 = 0; k2 < meterialUtilities2.size(); k2++) {
                                                        if (meterialUtilities.get(k).getMaterial_id().equalsIgnoreCase(meterialUtilities2.get(k2).getMaterial_id()) &&
                                                                meterialUtilities.get(k).getAssign_material_id().equalsIgnoreCase(meterialUtilities2.get(k2).getAssign_material_id())) {
                                                            if (meterialUtilities.get(k).getMaterial_type().equalsIgnoreCase("pdf") && meterialUtilities.get(k).getIs_offline_available().equalsIgnoreCase("no"))
                                                                Utils.deleteFile(mContext, meterialUtilities2.get(k2), dataBase);
                                                            if (meterialUtilities.get(k).getMaterial_type().equalsIgnoreCase("quiz"))
                                                                deleteFile(meterialUtilities2.get(k2), true);

                                                           Utils.deleteFile(mContext,meterialUtilities.get(k),meterialUtilities2.get(k2),dataBase);

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//                    dataBase.deleteGetResponse();


                    ArrayList<Like_Utility> like_utilities = dataBase.getLDData(WebServices.mLoginUtility.getUser_id());
                    for (int i = 0; i < like_utilities.size(); i++) {
                        nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("item_id", like_utilities.get(i).getItem_id()));
                        nameValuePair.add(new BasicNameValuePair("item_type", like_utilities.get(i).getItem_type()));
                        nameValuePair.add(new BasicNameValuePair("value", like_utilities.get(i).getValue()));
                        nameValuePair.add(new BasicNameValuePair("comment", like_utilities.get(i).getComment()));
                        resp = webServices.callServices(nameValuePair, APIUtility.LIKE);
                        Log.d(" Response:", APIUtility.LIKE + resp.toString());
                        if (webServices.isValid(resp)) {
                            dataBase.RemoveLD(WebServices.mLoginUtility.getUser_id(), like_utilities.get(i).getItem_id());
                        }
                    }


                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    resp = webServices.getLogin(nameValuePair, APIUtility.GETALLITEM_LIKE);
                    Log.d(" Response:", APIUtility.GETALLITEM_LIKE + resp.toString());
                    if (!resp.equalsIgnoreCase("")) {

                        webServices.getAllLikedItems(resp);
                    }

                    ArrayList<Favourites_Utils> favourites_utilses = dataBase.getFavData(WebServices.mLoginUtility.getUser_id());
                    for (int i = 0; i < favourites_utilses.size(); i++) {
                        if (favourites_utilses.get(i).getFavourite_id().equalsIgnoreCase("")) {
                            nameValuePair = new ArrayList<NameValuePair>();
                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                            nameValuePair.add(new BasicNameValuePair("item_id", favourites_utilses.get(i).getItem_id()));
                            nameValuePair.add(new BasicNameValuePair("item_type", favourites_utilses.get(i).getItem_type()));
                            resp = webServices.callServices(nameValuePair, APIUtility.ADD_FAVORITES);
                            Log.d(" Response:", APIUtility.ADD_FAVORITES + resp.toString());
                            if (webServices.isValid(resp)) {
                                dataBase.RemoveFav(WebServices.mLoginUtility.getUser_id(), favourites_utilses.get(i).getItem_id());
                            }
                        } else {
                            nameValuePair = new ArrayList<NameValuePair>();
                            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                            nameValuePair.add(new BasicNameValuePair("item_id", favourites_utilses.get(i).getItem_id()));
                            nameValuePair.add(new BasicNameValuePair("favourite_id", favourites_utilses.get(i).getFavourite_id()));
                            resp = webServices.callServices(nameValuePair, APIUtility.REMOVE_FAVORITES);
                            Log.d(" Response:", APIUtility.REMOVE_FAVORITES + resp.toString());
                            if (webServices.isValid(resp)) {
                                dataBase.RemoveFav(WebServices.mLoginUtility.getUser_id(), favourites_utilses.get(i).getItem_id());
                            }
                        }
                    }

                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    try {
                        resp = webServices.getLogin(nameValuePair, APIUtility.GET_ALL_FAVORITES);
                        Log.d(" Response:", APIUtility.GET_ALL_FAVORITES + resp.toString());
                        if (!resp.equalsIgnoreCase("")) {
                            dataBase.addFavouritesData(WebServices.mLoginUtility.getUser_id(), resp);
                            webServices.getAllFavouritesItems(resp);
                            //                            System.out.println("====resp====" + dataBase.getFavouritesData(WebServices.mLoginUtility.getUser_id()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    ArrayList<String> notificationData = dataBase.getReadNotificationData(WebServices.mLoginUtility.getUser_id());

                    int size = notificationData.size();
                    String notifi_id = "", not_user_id = "";
                    for (int i = 0; i < size; i++) {
                        notifi_id = notificationData.get(i).split(",")[0];
                        if (i < size - 1)
                            not_user_id = not_user_id + notificationData.get(i).split(",")[1] + ",";
                        else
                            not_user_id = not_user_id + notificationData.get(i).split(",")[1];
                    }

                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("notification_user_id", not_user_id));
                    nameValuePair.add(new BasicNameValuePair("notification_id", notifi_id));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("sent_status", "READ"));
                    resp = webServices.getLogin(nameValuePair, APIUtility.READ_NOTIFICATION);
                    Log.d(" Response:", APIUtility.READ_NOTIFICATION + resp.toString());
                    if (webServices.isValid(resp)) {
                        for (int i = 0; i < size; i++) {
                            dataBase.deleteReadNotificationData(WebServices.mLoginUtility.getUser_id(), notificationData.get(i).split(",")[0]);
                        }
                    }

                    notificationData = dataBase.getReadNotificationData(WebServices.mLoginUtility.getUser_id());
                    for (int i = 0; i < notificationData.size(); i++) {
                        notifi_id = notificationData.get(i).split(",")[0];
                        String notifi_user_id = notificationData.get(i).split(",")[1];
                        for (int j = 0; j < WebServices.notificationUtilityArrayList.size(); j++) {
                            if (notifi_id.equalsIgnoreCase(WebServices.notificationUtilityArrayList.get(j).getNotification_id())) {
                                WebServices.notificationUtilityArrayList.get(j).setSent_status("READ");
                            }
                        }
                    }

                    nameValuePair = new ArrayList<NameValuePair>();
                    nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                    nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                    nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                    nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                    nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                    nameValuePair.add(new BasicNameValuePair("assign_course_id", WebServices.mLoginUtility.getOrganization_id()));
                    resp = webServices.callServices(nameValuePair, APIUtility.GETALLGROUP);
                    Log.d(" Response:", resp.toString());
                    if (webServices.isValid(resp)) {
                        if (dataBase.isData(WebServices.mLoginUtility.getUser_id(), "All_Groups"))
                            dataBase.addallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                        else {
                            dataBase.updateallgroupsData(WebServices.mLoginUtility.getUser_id(), resp);
                        }
                        webServices.getAllGroup(resp);
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        try {
            if (!((Activity) mContext).isFinishing()) {
                //   if (dialog != null && dialog.isShowing()) dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
            ((Activity) mContext).finish();
        }

        if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
            Utils.callInvalidSession(mContext, "FATCH DATA");
            if (isFirstTime && mListener != null) {
                mListener.onSuccess(isFirstTime);
            }
            return;
        }

        if (isFirstTime) {
            if (mListener != null) {
                mListener.onSuccess(isFirstTime);
            }
            Log.d(" Response:", "first time onPostExecute");
            new FatchDataAsyncTask(mContext, false, false).execute("");
            /*if (WebServices.isNetworkAvailable(mContext)) {
                new HomeActivity.getNewsFeed(true).execute("");
            } else {
                findViewById(R.id.ll_news).setVisibility(View.GONE);
                Toast.makeText(HomeActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
            }*/
        } else {
            if (mListener != null) {
                mListener.onSecondTimeSuccess();
            }
        }


    }

    private void deleteFile(MeterialUtility meterialUtility, boolean isCertificate) {
        if (meterialUtility.getMaterial_type().equalsIgnoreCase("quiz")) {
            File decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + ".pdf");
            if (decodefile.exists()) {
                decodefile.delete();
            }
        }

    }


    public interface FatchDataListener {
        void onSuccess(boolean isFirstTime);

        void onSecondTimeSuccess();
    }
}
