package com.chaptervitamins.newcode.server;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.MeterialUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vijay Antil on 21-11-2017.
 */

public class GetNewsFeedsAsyncTask extends AsyncTask<String,Void,Boolean> {

    private boolean isShow = false;
    private WebServices webServices;
    private Context mContext;
    private GetNewsFeedListener mListener;
    private ArrayList<MeterialUtility> newsFeedUtilities;

    public GetNewsFeedsAsyncTask(Context context,boolean isShow, WebServices webServices) {
        this.mListener = (GetNewsFeedListener)context;
        this.isShow = isShow;
        this.mContext = context;
        this.webServices = webServices;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair = new ArrayList<NameValuePair>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        String resp = webServices.callServices(nameValuePair, APIUtility.GET_NEWS_FEED);
        Log.d(" Response:", resp.toString());
        if (webServices.isValid(resp)) {
            newsFeedUtilities = webServices.getNewsFeedData(resp);
            return true;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if(status && mListener!=null)
            mListener.onNewsFeedSuccess(newsFeedUtilities);
        else
            mListener.onNewsFeedFailed();
    }

    public interface GetNewsFeedListener{
        void onNewsFeedSuccess(ArrayList<MeterialUtility> newsFeedUtilities);
        void onNewsFeedFailed();
    }
}
