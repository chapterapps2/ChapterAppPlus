package com.chaptervitamins.newcode.models;

import java.io.Serializable;

/**
 * Created by Vijay Antil on 22-01-2018.
 */

public class TrainerModel implements Serializable{
    private String trainer_id,trainer_name,trainer_emp_id,rating,comment;

    public String getTrainer_id() {
        return trainer_id;
    }

    public String getTrainer_name() {
        return trainer_name;
    }

    public String getTrainer_emp_id() {
        return trainer_emp_id;
    }

    public String getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public TrainerModel(String trainer_id, String trainer_name, String trainer_emp_id, String rating, String comment) {

        this.trainer_id = trainer_id;
        this.trainer_name = trainer_name;
        this.trainer_emp_id = trainer_emp_id;
        this.rating = rating;
        this.comment = comment;
    }
}
