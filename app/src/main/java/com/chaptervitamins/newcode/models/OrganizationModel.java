package com.chaptervitamins.newcode.models;

/**
 * Created by Vijay Antil on 09-02-2018.
 */

public class OrganizationModel {
    private String autoLogoutTime = "4320", dataClearTime = "7200", showModuleAddedOn = "NO", showMaterialAddedOn = "YES", appVersion="", moduleAddedOnLabel="Added On",
            moduleCompleteByLabel="Complete By", adminEmail="", languagePreference="", showModuleEndDate="YES", notificationStartTime="8:00:00", notificationEndTime="20:00:00", enableScreenshot="NO",
            queryCategory, suggestionQuery, sessionExtend="YES", isForceUpdateOptionally="YES";
    private Long notificationTimer;
    private String branch_app_version="";
    private String home_temp="";

    public String getAutoLogoutTime() {
        return autoLogoutTime;
    }

    public void setAutoLogoutTime(String autoLogoutTime) {
        this.autoLogoutTime = autoLogoutTime;
    }

    public String getDataClearTime() {
        return dataClearTime;
    }

    public void setDataClearTime(String dataClearTime) {
        this.dataClearTime = dataClearTime;
    }

    public String getShowModuleAddedOn() {
        return showModuleAddedOn;
    }

    public void setShowModuleAddedOn(String showModuleAddedOn) {
        this.showModuleAddedOn = showModuleAddedOn;
    }

    public String getShowMaterialAddedOn() {
        return showMaterialAddedOn;
    }

    public void setShowMaterialAddedOn(String showMaterialAddedOn) {
        this.showMaterialAddedOn = showMaterialAddedOn;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getModuleAddedOnLabel() {

        return moduleAddedOnLabel;
    }

    public void setModuleAddedOnLabel(String moduleAddedOnLabel) {
        this.moduleAddedOnLabel = moduleAddedOnLabel;
    }

    public String getModuleCompleteByLabel() {
        return moduleCompleteByLabel;
    }

    public void setModuleCompleteByLabel(String moduleCompleteByLabel) {
        this.moduleCompleteByLabel = moduleCompleteByLabel;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getLanguagePreference() {
        return languagePreference;
    }

    public void setLanguagePreference(String languagePreference) {
        this.languagePreference = languagePreference;
    }

    public String getShowModuleEndDate() {
        return showModuleEndDate;
    }

    public void setShowModuleEndDate(String showModuleEndDate) {
        this.showModuleEndDate = showModuleEndDate;
    }

    public String getNotificationStartTime() {
        return notificationStartTime;
    }

    public void setNotificationStartTime(String notificationStartTime) {
        this.notificationStartTime = notificationStartTime;
    }

    public String getNotificationEndTime() {
        return notificationEndTime;
    }

    public void setNotificationEndTime(String notificationEndTime) {
        this.notificationEndTime = notificationEndTime;
    }

    public String getEnableScreenshot() {
        return enableScreenshot;
    }

    public void setEnableScreenshot(String enableScreenshot) {
        this.enableScreenshot = enableScreenshot;
    }

    public String getQueryCategory() {
        return queryCategory;
    }

    public void setQueryCategory(String queryCategory) {
        this.queryCategory = queryCategory;
    }

    public String getSuggestionQuery() {
        return suggestionQuery;
    }

    public void setSuggestionQuery(String suggestionQuery) {
        this.suggestionQuery = suggestionQuery;
    }

    public String getSessionExtend() {
        return sessionExtend;
    }

    public void setSessionExtend(String sessionExtend) {
        this.sessionExtend = sessionExtend;
    }

    public String getIsForceUpdateOptionally() {
        return isForceUpdateOptionally;
    }

    public void setIsForceUpdateOptionally(String isForceUpdateOptionally) {
        this.isForceUpdateOptionally = isForceUpdateOptionally;
    }

    public Long getNotificationTimer() {
        return notificationTimer;
    }

    public void setNotificationTimer(Long notificationTimer) {
        this.notificationTimer = notificationTimer;
    }

    public String getBranch_app_version() {
        return branch_app_version;
    }

    public void setBranch_app_version(String branch_app_version) {
        this.branch_app_version = branch_app_version;
    }

    public String getHome_temp() {
        return home_temp;
    }

    public void setHome_temp(String home_temp) {
        this.home_temp = home_temp;
    }
}
