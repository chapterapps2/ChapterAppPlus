package com.chaptervitamins.newcode.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Vijay Antil on 20-02-2018.
 */

public class VideoQuizAllResponseModel implements Serializable {
    private String finishDate,responseId;
    private boolean isEvaluated;
    private ArrayList<VideoQuizResponse> alVideoQuizResponse;

    public ArrayList<VideoQuizResponse> getAlVideoQuizResponse() {
        return alVideoQuizResponse;
    }

    public void setAlVideoQuizResponse(ArrayList<VideoQuizResponse> alVideoQuizResponse) {
        this.alVideoQuizResponse = alVideoQuizResponse;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public boolean isEvaluated() {
        return isEvaluated;
    }

    public void setEvaluated(boolean evaluated) {
        isEvaluated = evaluated;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }
}
