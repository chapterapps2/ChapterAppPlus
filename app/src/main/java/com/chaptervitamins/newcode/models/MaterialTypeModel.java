package com.chaptervitamins.newcode.models;

/**
 * Created by Vijay Antil on 30-01-2018.
 */

public class MaterialTypeModel {
    private String materialType,name;
    boolean isSelected;

    public MaterialTypeModel(String materialType,String name) {
        this.materialType = materialType;
        this.name = name;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }
}
