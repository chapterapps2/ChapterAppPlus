package com.chaptervitamins.newcode.models;

import java.io.Serializable;

/**
 * Created by Vijay Antil on 07-02-2018.
 */

public class ContentInShortsModel implements Serializable {
    private String id, title, description, contentType, media, template;

    public ContentInShortsModel(String id, String title, String description, String contentType, String media, String template) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.contentType = contentType;
        this.media = media;
        this.template = template;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getContentType() {
        return contentType;
    }

    public String getMedia() {
        return media;
    }

    public String getTemplate() {
        return template;
    }
}
