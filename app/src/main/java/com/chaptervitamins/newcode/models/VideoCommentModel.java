package com.chaptervitamins.newcode.models;

import java.io.Serializable;

/**
 * Created by Vijay Antil on 20-01-2018.
 */

public class VideoCommentModel implements Serializable{
    private String file_size;

    private String media_id;

    private String added_on;

    private String type;

    private String file_name;

    private String file_url;

    public String getFile_size ()
    {
        return file_size;
    }

    public void setFile_size (String file_size)
    {
        this.file_size = file_size;
    }

    public String getMedia_id ()
    {
        return media_id;
    }

    public void setMedia_id (String media_id)
    {
        this.media_id = media_id;
    }

    public String getAdded_on ()
    {
        return added_on;
    }

    public void setAdded_on (String added_on)
    {
        this.added_on = added_on;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getFile_name ()
    {
        return file_name;
    }

    public void setFile_name (String file_name)
    {
        this.file_name = file_name;
    }

    public String getFile_url ()
    {
        return file_url;
    }

    public void setFile_url (String file_url)
    {
        this.file_url = file_url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [file_size = "+file_size+", media_id = "+media_id+", added_on = "+added_on+", type = "+type+", file_name = "+file_name+", file_url = "+file_url+"]";
    }
}
