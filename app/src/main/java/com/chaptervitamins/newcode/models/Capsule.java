package com.chaptervitamins.newcode.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.model.Vertical;

import java.util.ArrayList;

public class Capsule implements Parcelable{
    String material_id;
    String materialMediaUrl;
    ArrayList<Vertical> verticalList;
    String material_media_id;
    String module_id;
    String title;
    String description;
    String instruction;
    String material_type;
    String test_pattern;
    String question_sequence;
    String option_sequence;
    String no_of_question,questionBankId,assignQuestionId;

    ArrayList<String> answers;
    ArrayList<Data_util> data_utils = new ArrayList<>();
    String warning_time = "", warning_message, each_ques_warning_time;
    private String content_id, contentType, media, template,correctOption,userAns;

    public Capsule() {

    }

    protected Capsule(Parcel in) {
        material_id = in.readString();
        materialMediaUrl = in.readString();
        material_media_id = in.readString();
        module_id = in.readString();
        title = in.readString();
        description = in.readString();
        instruction = in.readString();
        material_type = in.readString();
        test_pattern = in.readString();
        question_sequence = in.readString();
        option_sequence = in.readString();
        no_of_question = in.readString();
        answers = in.createStringArrayList();
        warning_time = in.readString();
        warning_message = in.readString();
        each_ques_warning_time = in.readString();
        content_id = in.readString();
        contentType = in.readString();
        media = in.readString();
        template = in.readString();
        correctOption = in.readString();
        userAns = in.readString();
    }

    public static final Creator<Capsule> CREATOR = new Creator<Capsule>() {
        @Override
        public Capsule createFromParcel(Parcel in) {
            return new Capsule(in);
        }

        @Override
        public Capsule[] newArray(int size) {
            return new Capsule[size];
        }
    };

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public String getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(String material_id) {
        this.material_id = material_id;
    }

    public String getMaterialMediaUrl() {
        return materialMediaUrl;
    }

    public void setMaterialMediaUrl(String materialMediaUrl) {
        this.materialMediaUrl = materialMediaUrl;
    }

    public ArrayList<Vertical> getVerticalList() {
        return verticalList;
    }

    public void setVerticalList(ArrayList<Vertical> verticalList) {
        this.verticalList = verticalList;
    }

    public String getMaterial_media_id() {
        return material_media_id;
    }

    public void setMaterial_media_id(String material_media_id) {
        this.material_media_id = material_media_id;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getMaterial_type() {
        return material_type;
    }

    public void setMaterial_type(String material_type) {
        this.material_type = material_type;
    }

    public String getTest_pattern() {
        return test_pattern;
    }

    public void setTest_pattern(String test_pattern) {
        this.test_pattern = test_pattern;
    }

    public String getQuestion_sequence() {
        return question_sequence;
    }

    public void setQuestion_sequence(String question_sequence) {
        this.question_sequence = question_sequence;
    }

    public String getOption_sequence() {
        return option_sequence;
    }

    public void setOption_sequence(String option_sequence) {
        this.option_sequence = option_sequence;
    }

    public String getNo_of_question() {
        return no_of_question;
    }

    public void setNo_of_question(String no_of_question) {
        this.no_of_question = no_of_question;
    }

    public ArrayList<Data_util> getData_utils() {
        return data_utils;
    }

    public void setData_utils(ArrayList<Data_util> data_utils) {
        this.data_utils = data_utils;
    }

    public String getWarning_time() {
        return warning_time;
    }

    public void setWarning_time(String warning_time) {
        this.warning_time = warning_time;
    }

    public String getWarning_message() {
        return warning_message;
    }

    public void setWarning_message(String warning_message) {
        this.warning_message = warning_message;
    }

    public String getEach_ques_warning_time() {
        return each_ques_warning_time;
    }

    public void setEach_ques_warning_time(String each_ques_warning_time) {
        this.each_ques_warning_time = each_ques_warning_time;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }


    public String getCorrectOption() {
        return correctOption;
    }

    public void setCorrectOption(String correctOption) {
        this.correctOption = correctOption;
    }

    public String getUserAns() {
        return userAns;
    }

    public void setUserAns(String userAns) {
        this.userAns = userAns;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(material_id);
        parcel.writeString(materialMediaUrl);
        parcel.writeString(material_media_id);
        parcel.writeString(module_id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(instruction);
        parcel.writeString(material_type);
        parcel.writeString(test_pattern);
        parcel.writeString(question_sequence);
        parcel.writeString(option_sequence);
        parcel.writeString(no_of_question);
        parcel.writeStringList(answers);
        parcel.writeString(warning_time);
        parcel.writeString(warning_message);
        parcel.writeString(each_ques_warning_time);
        parcel.writeString(content_id);
        parcel.writeString(contentType);
        parcel.writeString(media);
        parcel.writeString(template);
        parcel.writeString(correctOption);
        parcel.writeString(userAns);
    }

    public String getQuestionBankId() {
        return questionBankId;
    }

    public void setQuestionBankId(String questionBankId) {
        this.questionBankId = questionBankId;
    }

    public String getAssignQuestionId() {
        return assignQuestionId;
    }

    public void setAssignQuestionId(String assignQuestionId) {
        this.assignQuestionId = assignQuestionId;
    }
}
