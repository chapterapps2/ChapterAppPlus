package com.chaptervitamins.newcode.models;

/**
 * Created by Vijay Antil on 15-01-2018.
 */

public class AnswerModel {
    private String id,option,isOptionImage,optionMarks,optionType,option_url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getIsOptionImage() {
        return isOptionImage;
    }

    public void setIsOptionImage(String isOptionImage) {
        this.isOptionImage = isOptionImage;
    }

    public String getOptionMarks() {
        return optionMarks;
    }

    public void setOptionMarks(String optionMarks) {
        this.optionMarks = optionMarks;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public String getOption_url() {
        return option_url;
    }

    public void setOption_url(String option_url) {
        this.option_url = option_url;
    }
}
