package com.chaptervitamins.newcode.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Vijay Antil on 20-01-2018.
 */

public class VideoQuizResponse implements Serializable {

    private String answer_type;

    private String question_comment,question_description;

    private String answer_key;

    private String question_response_id;

    private VideoCommentModel answer;

    private String time_taken;

    private String question_bank_id;

    private String response_id;

    private ArrayList<TrainerModel> trainerModelArrayList;



    public String getAnswer_type() {
        return answer_type;
    }

    public void setAnswer_type(String answer_type) {
        this.answer_type = answer_type;
    }

    public String getQuestion_comment() {
        return question_comment;
    }

    public void setQuestion_comment(String question_comment) {
        this.question_comment = question_comment;
    }

    public String getAnswer_key() {
        return answer_key;
    }

    public void setAnswer_key(String answer_key) {
        this.answer_key = answer_key;
    }

    public String getQuestion_response_id() {
        return question_response_id;
    }

    public void setQuestion_response_id(String question_response_id) {
        this.question_response_id = question_response_id;
    }

    public VideoCommentModel getAnswer() {
        return answer;
    }

    public void setAnswer(VideoCommentModel answer) {
        this.answer = answer;
    }

    public String getTime_taken() {
        return time_taken;
    }

    public void setTime_taken(String time_taken) {
        this.time_taken = time_taken;
    }

    public String getQuestion_bank_id() {
        return question_bank_id;
    }

    public void setQuestion_bank_id(String question_bank_id) {
        this.question_bank_id = question_bank_id;
    }

    public String getResponse_id() {
        return response_id;
    }

    public void setResponse_id(String response_id) {
        this.response_id = response_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [answer_type = " + answer_type  + ", question_comment = " + question_comment + ", answer_key = " + answer_key + ", question_response_id = " + question_response_id + ", answer = " + answer + ", time_taken = " + time_taken + ", question_bank_id = " + question_bank_id + ", response_id = " + response_id + "]";
    }

    public ArrayList<TrainerModel> getTrainerModelArrayList() {
        return trainerModelArrayList;
    }

    public void setTrainerModelArrayList(ArrayList<TrainerModel> trainerModelArrayList) {
        this.trainerModelArrayList = trainerModelArrayList;
    }

    public String getQuestion_description() {
        return question_description;
    }

    public void setQuestion_description(String question_description) {
        this.question_description = question_description;
    }
}
