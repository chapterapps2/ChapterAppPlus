package com.chaptervitamins.newcode.models;

/**
 * Created by VijayAntil on 29/03/17.
 */

public class NavDrawerModel {
    private String id, title;
    private int imageId;

    public NavDrawerModel(String id, String title, int imageId) {
        this.id = id;
        this.title = title;
        this.imageId = imageId;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getImageId() {
        return imageId;
    }
}
