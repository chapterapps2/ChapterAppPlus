package com.chaptervitamins.newcode.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Patterns;
import android.widget.ImageView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.activities.BaseActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.activities.ModuleActivity;
import com.chaptervitamins.newcode.adapters.NotificationAdapter;
import com.chaptervitamins.newcode.server.ApiCallUtils;
import com.chaptervitamins.newcode.server.FatchDataAsyncTask;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.play_video.Link_Activity;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.Favourites_Utility;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Vijay Antil on 22-11-2017.
 */

public class Utils {
    static DataBase mDatabase;
    static WebServices mWebServices;
    static MixPanelManager mixPanelManager;

    //1 minute = 60 seconds
//1 hour = 60 x 60 = 3600
//1 day = 3600 x 24 = 86400
    public static String printDifference(String startStr, String endStr) {
        Date startDate = null, endDate = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            startDate = simpleDateFormat.parse(startStr);
            endDate = simpleDateFormat.parse(endStr);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();
       /* System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        String hours = "", minutes = "", seconds = "";
        if (elapsedHours < 10)
            hours = "0" + elapsedHours;
        else hours = elapsedHours + "";
        if (elapsedMinutes < 10)
            minutes = "0" + elapsedMinutes;
        else minutes = elapsedMinutes + "";
        if (elapsedSeconds < 10)
            seconds = "0" + elapsedSeconds;
        else seconds = elapsedSeconds + "";

        /*System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);*/
        return hours + ":" + minutes + ":" + seconds;
    }

    public static String getCurrentTimeStamp() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(AppConstants.TIME_ZONE));
        Date date = calendar.getTime();
        return DateFormat.format(AppConstants.DATE_FORMAT_NEW, date).toString();
    }

    public static boolean isValidEmaillId(String email) {

        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void setHomeData(DataBase dataBase, WebServices webServices, MixPanelManager mixPanel) {
        mDatabase = dataBase;
        mWebServices = webServices;
        mixPanelManager = mixPanel;
    }


    public static String getQuizStartDateNewFormat(String strInput) {
        if (!TextUtils.isEmpty(strInput)) {
            /*  Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                      .parse(str.replaceAll("(?<=\\d)(st|nd|rd|th)", ""));
              SimpleDateFormat date1 = new SimpleDateFormat("yyyy-Mmm-dd");
              return date.getDay() + "/" + date.getDate();*/
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "dd MMM,yy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(strInput);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
        return "";
    }

    public static String changeDateFormat(String strInput, String format) {
        if (!TextUtils.isEmpty(strInput)) {
            /*  Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                      .parse(str.replaceAll("(?<=\\d)(st|nd|rd|th)", ""));
              SimpleDateFormat date1 = new SimpleDateFormat("yyyy-Mmm-dd");
              return date.getDay() + "/" + date.getDate();*/
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = format;
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(strInput);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
        return "";
    }

    public static Date getDateFormat(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dateStr);
            //     System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * it is used to check if location is enabled
     *
     * @param context
     * @return
     */
    public static boolean isLocationEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    public static Date getDateFormat(Date date2) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date1;
        Date date3 = null;
        date1 = format.format(date2);
        try {
            date3 = format.parse(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //     System.out.println(date);
        return date3;
    }

    public static String getQuizEndDateNewFormat(String strInput) {
        if (!TextUtils.isEmpty(strInput)) {
            /*  Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                      .parse(str.replaceAll("(?<=\\d)(st|nd|rd|th)", ""));
              SimpleDateFormat date1 = new SimpleDateFormat("yyyy-Mmm-dd");
              return date.getDay() + "/" + date.getDate();*/
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "dd MMM,yy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(strInput);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
        return "";
    }

    public static String diffDate(long second) {
        //HH converts hour in 24 hours format (0-23), day calculation


        long diffSeconds = second % 60;
        long diffMinutes = (second / (60)) % 60;
        long diffHours = (second / (60 * 60)) % 24;
        long diffDays = second / (24 * 60 * 60);

        String h = "", m = "", s = "";
        if (diffHours < 10) {
            h = "0" + diffHours;
        } else {
            h = diffHours + "";
        }
        if (diffMinutes < 10) {
            m = "0" + diffMinutes;
        } else {
            m = diffMinutes + "";
        }
        if (diffSeconds < 10) {
            s = "0" + diffSeconds;
        } else {
            s = diffSeconds + "";
        }
        if (!h.equalsIgnoreCase("00") || !h.equals("0"))
            return h + ":" + m + ":" + s;
        else return m + ":" + s;
    }

    /**
     * It iwill work for quiz review
     *
     * @param startTime
     * @param endTime
     * @param oldTime
     * @return
     */
    public static String getTimeDifferenceForReview(long startTime, long endTime, String oldTime) {
        long diff = endTime - startTime;
        if (!TextUtils.isEmpty(oldTime)) {
            return (String.valueOf(((Long.parseLong(oldTime) + diff) / 1000) % 60));
        } else {
            return (String.valueOf((diff / 1000) % 60));
        }
    }

    public static String getTimeDifference(long startTime, long endTime, String oldTime) {
        long diff = startTime - endTime;
        if (!TextUtils.isEmpty(oldTime)) {
            return (String.valueOf(Long.parseLong(oldTime) + diff));
        } else {
            return (String.valueOf(diff));
        }
    }


    public static String diffDate(String dateStart, String dateStop) {
        if (!TextUtils.isEmpty(dateStart) && !TextUtils.isEmpty(dateStop)) {
            //HH converts hour in 24 hours format (0-23), day calculation
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date d1 = null;
            Date d2 = null;

            try {
                d1 = format.parse(dateStart);
                d2 = format.parse(dateStop);

                //in milliseconds
                long diff = d2.getTime() - d1.getTime();

                long diffSeconds = diff / 1000;
              /*  long diffMinutes = diff / (60 * 1000);
                long diffHours = diff / (60 * 60 * 1000);
                long diffDays = diff / (24 * 60 * 60 * 1000);


             *//*   long diffSeconds = diff / 1000 % 60;
                long diffMinutes = diff / (60 * 1000) % 60;
                long diffHours = diff / (60 * 60 * 1000) % 24;
                long diffDays = diff / (24 * 60 * 60 * 1000);*//*

                System.out.println(diffDays + " days, ");
                System.out.print(diffHours + " hours, ");
                System.out.print(diffMinutes + " minutes, ");
                System.out.print(diffSeconds + " seconds.");*/
                if (diffSeconds == 0) diffSeconds = 1;
                return diffSeconds + "";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "1";
    }

    public static int getColorPrimary() {
        try {
            if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1()) &&
                        !WebServices.mLoginUtility.getBranch_color1().equalsIgnoreCase("null")) {
                    return (Color.parseColor(WebServices.mLoginUtility.getBranch_color1()));
                } else
                    return (Color.parseColor(WebServices.mLoginUtility.getOrganization_color1()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getColorPrimaryinString() {
        try {
            if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1()) &&
                        !WebServices.mLoginUtility.getBranch_color1().equalsIgnoreCase("null")) {
                    return WebServices.mLoginUtility.getBranch_color1();
                } else
                    return WebServices.mLoginUtility.getOrganization_color1();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getColorSecondaryinString() {
        try {
            if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())) {
                if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2()) &&
                        !WebServices.mLoginUtility.getBranch_color2().equalsIgnoreCase("null")) {
                    return WebServices.mLoginUtility.getBranch_color2();
                } else
                    return WebServices.mLoginUtility.getOrganization_color2();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * It is used to set materials default image
     *
     * @param materialType
     * @param ivMaterial
     */
    public static void setMaterialImage(String materialType, ImageView ivMaterial, boolean isNotification) {
        switch (materialType.toUpperCase()) {
            case AppConstants.MaterialType.MULTIMEDIAQUIZ:
            case AppConstants.MaterialType.QUIZ:
                ivMaterial.setImageResource(R.drawable.quiz);
                break;
            case AppConstants.MaterialType.VIDEO:
                ivMaterial.setImageResource(R.drawable.video);
                break;
            case AppConstants.MaterialType.AUDIO:
                ivMaterial.setImageResource(R.drawable.audio);
                break;
            case AppConstants.MaterialType.LINK:
                ivMaterial.setImageResource(R.drawable.link);
                break;
            case AppConstants.MaterialType.FLASH:
            case AppConstants.MaterialType.TINCAN_SCROM:
                ivMaterial.setImageResource(R.drawable.scrom);
                break;
            case AppConstants.MaterialType.PDF:
                ivMaterial.setImageResource(R.drawable.pdf_new);
                break;
            case AppConstants.MaterialType.SURVEY:
                ivMaterial.setImageResource(R.drawable.survey);
                break;
            case AppConstants.MaterialType.FLASHCARD:
                ivMaterial.setImageResource(R.drawable.flashcard);
                break;
            case AppConstants.MaterialType.CONTENT:
            case AppConstants.MaterialType.CONTENTINSHORT:
                ivMaterial.setImageResource(R.drawable.content_bytes);
                break;
            case AppConstants.MaterialType.FULL_TEXT:
                ivMaterial.setImageResource(R.drawable.epub);
                break;
            default:
                if (isNotification)
                    ivMaterial.setImageResource(R.drawable.info_notification);
                else
                    ivMaterial.setImageResource(R.mipmap.ic_launcher);
                break;

        }
    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static void callInvalidSession(Context mContext, String apiUrl) {
        MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(mContext);
        mixPanelManager.AutoLogout(mContext, apiUrl);
       /* if (Constants.ORGANIZATION_ID.equals("2"))
            callInvalidSession(mContext, true);
        else callInvalidSession(mContext, false);*/

        callInvalidSession(mContext, true);
    }

    public static void callInvalidSession(Context mContext, boolean showExpired) {
        if (showExpired) {
//            Toast.makeText(mContext, mContext.getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
//            DataBase.getInstance(mContext).updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
//            SharedPreferences.Editor editor = SplashActivity.mPref.edit();
//            editor.putBoolean("islogin", false);
//            editor.clear();
//            editor.commit();
            if (mContext instanceof HomeActivity) {
                ApiCallUtils.loginApiCall(mContext, new WebServices(), DataBase.getInstance(mContext), false);
            } else {
                if (WebServices.isNetworkAvailable(mContext)) {
                    ApiCallUtils.loginApiCall(mContext, new WebServices(), DataBase.getInstance(mContext), true);
                }
            }
        } else {
            /*Toast.makeText(mContext, mContext.getString(R.string.session_invalid), Toast.LENGTH_SHORT).show();
            DataBase.getInstance(mContext).updateLoginData(SplashActivity.mPref.getString("id", ""), SplashActivity.mPref.getString("pass", ""), "");
            SharedPreferences.Editor editor = SplashActivity.mPref.edit();
            editor.putBoolean("islogin", false);
            editor.clear();
            editor.commit();
            Intent intent = new Intent(mContext, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("sms_url", "");
            mContext.startActivity(intent);
            ((Activity) mContext).finish();*/
        }

    }

    public static void callHomeActivity(Context mContext) {
        Intent intent = new Intent(mContext, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("sms_url", "");
        mContext.startActivity(intent);
        ((Activity) mContext).finish();
    }

    public static boolean checkDownloadStatus(Context mContext, MeterialUtility meterialUtility, DataBase mDatabase) {
        boolean isDownloaded = false;
        File decodefile;
        switch (meterialUtility.getMaterial_type().toUpperCase()) {
            case AppConstants.MaterialType.MULTIMEDIAQUIZ:
            case AppConstants.MaterialType.QUIZ:
                isDownloaded = mDatabase.isOfflineDataAvailable(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), mDatabase.QUIZTABLE);
                break;
            case AppConstants.MaterialType.LINK:
                isDownloaded = false;
                break;
            case AppConstants.MaterialType.SURVEY:
            case AppConstants.MaterialType.CONTENTINSHORT:
                isDownloaded = mDatabase.isOfflineDataAvailable(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), mDatabase.SURVEYTABLE);
                break;
            case AppConstants.MaterialType.FLASHCARD:
            case AppConstants.MaterialType.IMAGECARD:
                isDownloaded = mDatabase.isOfflineDataAvailable(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), mDatabase.FLASHCARDTABLE);
                break;

            case AppConstants.MaterialType.VIDEO:
                decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + ".mp4");
                if (decodefile != null && decodefile.exists())
                    isDownloaded = true;
                else {
                    decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());
                    if (decodefile != null && decodefile.exists())
                        isDownloaded = true;
                }
                break;
            case AppConstants.MaterialType.CONTENT:
            case AppConstants.MaterialType.AUDIO:
            case AppConstants.MaterialType.FULL_TEXT:
            case AppConstants.MaterialType.PDF:
                decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());
                if (decodefile != null && decodefile.exists())
                    isDownloaded = true;

                break;
            case AppConstants.MaterialType.FLASH:
            case AppConstants.MaterialType.TINCAN_SCROM:
                decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip");
                if (decodefile != null && decodefile.exists())
                    isDownloaded = true;
                break;

        }
        return isDownloaded;
    }

    public static void deleteFile(Context mContext, MeterialUtility newMaterialUtility, MeterialUtility oldMaterialUtility, DataBase dataBase) {
        switch (oldMaterialUtility.getMaterial_type()) {
            case AppConstants.MaterialType.MULTIMEDIAQUIZ:
            case AppConstants.MaterialType.QUIZ:
            case AppConstants.MaterialType.CONTENTINSHORT:
            case AppConstants.MaterialType.SURVEY:
            case AppConstants.MaterialType.IMAGECARD:
            case AppConstants.MaterialType.FLASHCARD:
                if (!TextUtils.isEmpty(newMaterialUtility.getLast_updated_time()) && !TextUtils.isEmpty(oldMaterialUtility.getLast_updated_time()) &&
                        !newMaterialUtility.getLast_updated_time().equalsIgnoreCase(oldMaterialUtility.getLast_updated_time())) {
                    Utils.deleteFile(mContext, oldMaterialUtility, dataBase);
                }
                break;
            case AppConstants.MaterialType.TINCAN_SCROM:
            case AppConstants.MaterialType.FLASH:
            case AppConstants.MaterialType.VIDEO:
            case AppConstants.MaterialType.CONTENT:
            case AppConstants.MaterialType.AUDIO:
            case AppConstants.MaterialType.PDF:
            case AppConstants.MaterialType.FULL_TEXT:
                if (!TextUtils.isEmpty(newMaterialUtility.getMaterial_media_file_url()) && !TextUtils.isEmpty(oldMaterialUtility.getMaterial_media_file_url()) &&
                        !newMaterialUtility.getMaterial_media_file_url().equalsIgnoreCase(oldMaterialUtility.getMaterial_media_file_url())) {
                    Utils.deleteFile(mContext, oldMaterialUtility, dataBase);
                }
                break;
        }

    }

    public static void deleteFile(Context mContext, MeterialUtility meterialUtility, DataBase dataBase) {
        String resp = "";
        switch (meterialUtility.getMaterial_type().toUpperCase()) {
            case AppConstants.MaterialType.MULTIMEDIAQUIZ:
            case AppConstants.MaterialType.QUIZ:
                resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                if (!TextUtils.isEmpty(resp)) {
                    dataBase.updateQuizData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), "");
                }
                break;
            case AppConstants.MaterialType.HOSPITAL:
                resp = dataBase.getHospitalsData(WebServices.mLoginUtility.getUser_id());
                if (!TextUtils.isEmpty(resp)) {
                    dataBase.updateHospitalsData(WebServices.mLoginUtility.getUser_id(), "");
                }
                break;
            case AppConstants.MaterialType.CONTENTINSHORT:
            case AppConstants.MaterialType.SURVEY:
                resp = dataBase.getSurveyData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                if (!TextUtils.isEmpty(resp)) {
                    dataBase.deleteSurveyData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                    dataBase.addDataToDb(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), "", DataBase.SURVEYTABLE);
                }
                break;
            case AppConstants.MaterialType.IMAGECARD:
            case AppConstants.MaterialType.FLASHCARD:
                resp = dataBase.getFlashCardData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
                if (!resp.equalsIgnoreCase("")) {
                    dataBase.updateFlashcardData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id(), "");
                }
                break;
            case AppConstants.MaterialType.TINCAN_SCROM:
            case AppConstants.MaterialType.FLASH:
                File decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip");
                File decodefile2 = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());
                if (decodefile2.exists()) decodefile2.delete();
                if (decodefile.isDirectory()) {
                    deleteRecursive(decodefile);

                    boolean isdelete = decodefile.delete();
                    if (!isdelete) {
                        File file = new File(decodefile.getPath());
                        if (file.exists()) file.delete();
                    }
                }
                break;
            case AppConstants.MaterialType.VIDEO:
                File decodefile3 = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + ".mp4");

                if (decodefile3.exists()) {
                    decodefile3.delete();
                }
                break;
            case AppConstants.MaterialType.CONTENT:
            case AppConstants.MaterialType.AUDIO:
            case AppConstants.MaterialType.PDF:
            case AppConstants.MaterialType.FULL_TEXT:
                File decodefile1 = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id());
                if (decodefile1.exists()) {
                    decodefile1.delete();
                }
                File decodefile4 = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + "zip");
                if (decodefile4.exists()) {
                    if (decodefile4.isDirectory()) {
                        String[] children = decodefile4.list();
                        for (int i = 0; i < children.length; i++) {
                            new File(decodefile4, children[i]).delete();
                        }
                    }
                    decodefile4.delete();
                }
                break;
        }
        File decodefile = new File(mContext.getExternalFilesDir(null) + Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/" + meterialUtility.getMaterial_id() + ".pdf");
        if (decodefile.exists()) {
            decodefile.delete();
        }
    }

    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }


    public static void shareMaterial(Context context, MeterialUtility meterialUtility, MixPanelManager mixPanelManager) {
        if (meterialUtility != null && !TextUtils.isEmpty(meterialUtility.getMaterial_media_file_url())) {
//            mixPanelManager.shareMaterial((Activity) context, WebServices.mLoginUtility.getEmail(), meterialUtility.getCourse_name(), meterialUtility.getTitle(), meterialUtility.getMaterial_type());
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, meterialUtility.getMaterial_media_file_url());
            context.startActivity(Intent.createChooser(sharingIntent, "Pick a Share provider"));
        } else {
            Toast.makeText(context, "No share thing found!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * It is used to create custom shapes by code for dynamically color changes
     *
     * @param fillColor
     * @param roundRadius
     * @param strokeWidth
     * @param strokeColor
     * @return
     */
    public static GradientDrawable createShapeByColor(int fillColor, float roundRadius, int strokeWidth, int strokeColor) {
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(fillColor);
        gd.setCornerRadius(roundRadius);
        gd.setStroke(strokeWidth, strokeColor);
        return gd;
    }

    public static void loadDeepLinking(Context context, final String URL) {
        if (URL.contains("birthday")) {
            if (WebServices.isNetworkAvailable(context)) {
                Intent intent = new Intent(context, Link_Activity.class);
                intent.putExtra("name", "Birthday Notification");
                intent.putExtra("url", URL);
                context.startActivity(intent);
            } else
                Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
        } else {
            if (mDatabase.getCourseData(WebServices.mLoginUtility.getUser_id()).equalsIgnoreCase("")) {
                //   mSwipeRefreshLayout.setRefreshing(true);
                new FatchDataAsyncTask(context, true).execute("course");
            } else {

                boolean isAvailableMaterial = false;
                String cid = "", mid = "", isMod = "", responseId = "", mid1 = "";
                try {
                    String str = URL.split("\\?")[1];
                    String str1[] = str.split("&");
                    cid = str1[0].split("=")[1];
                    mid1 = str1[1].split("=")[1];
                    isMod = str1[1].split("=")[0];
                    mid = mid1.split(" ")[0];
                    responseId = str1[2].split("=")[1];


                    if (WebServices.notificationUtilityArrayList.size() != 0) {
//                    mRlNotification.setVisibility(View.VISIBLE);
                        int count = 0;
                        for (int i = 0; i < WebServices.notificationUtilityArrayList.size(); i++) {
                            if (!WebServices.notificationUtilityArrayList.get(i).getSent_status().equalsIgnoreCase("READ"))
                                count++;
                        }
                      /*  if (count == 0) mTvNotificationCount.setVisibility(View.GONE);
                        else mTvNotificationCount.setVisibility(View.VISIBLE);
                        mTvNotificationCount.setText(count + "");*/
                    }
                } catch (Exception e) {
                }
                if (cid.equalsIgnoreCase("user")) {
                    ApiCallUtils.quizEvaluateApiCall(context, mWebServices, responseId, mid1);
                } else if (cid.equalsIgnoreCase("trainer")) {
                    if (WebServices.isNetworkAvailable(context)) {
                        Intent intent = new Intent(context, Link_Activity.class);
                        intent.putExtra("name", "Evaluation");
                        intent.putExtra("url", URL);
                        context.startActivity(intent);

                    } else
                        Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
                } else {
                    MeterialUtility meterialUtility = new MeterialUtility();
                    int SELECTEDPOS = 0;
                    for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
                        if (HomeActivity.courseUtilities.get(i).getCourse_id().equalsIgnoreCase(cid)) {

                            ArrayList<ModulesUtility> modulesUtilityArrayList = HomeActivity.courseUtilities.get(i).getModulesUtilityArrayList();
                            for (int j = 0; j < modulesUtilityArrayList.size(); j++) {
                                if (isMod.equalsIgnoreCase("moid")) {
                                    if (modulesUtilityArrayList.get(j).getModule_id().equalsIgnoreCase(mid)) {
                                        SELECTEDPOS = i;
                                        isAvailableMaterial = true;
                                        //  System.out.println("===match data ===" + getIntent().getStringExtra("sms_url"));
                                    }
                                } else {
                                    ArrayList<MeterialUtility> meterialUtilities = modulesUtilityArrayList.get(j).getMeterialUtilityArrayList();
                                    for (int k = 0; k < meterialUtilities.size(); k++) {
                                        if (meterialUtilities.get(k).getMaterial_id().equalsIgnoreCase(mid)) {
                                            isAvailableMaterial = true;
                                            meterialUtility = meterialUtilities.get(k);
                                            break;
                                        }
                                    }
                                    if (isAvailableMaterial) break;
                                }
                            }
                        }
                        if (isAvailableMaterial) break;
                    }
                    if (isMod.equalsIgnoreCase("moid") && isAvailableMaterial) {
                        CourseUtility courseUtility = FlowingCourseUtils.getCourseFromModuleId(mid);

                        Intent intent = new Intent(context, ModuleActivity.class);
                        intent.putExtra("course", courseUtility);
                        intent.putExtra("mod_id", mid);
                        context.startActivity(intent);
                    } else if (isAvailableMaterial) {
                        new MaterialOpenController(context, mixPanelManager, mDatabase).openMaterial(meterialUtility, true, false, false);
                    } else {
                        if (WebServices.isNetworkAvailable(context)) {
                            Intent intent = new Intent(context, Link_Activity.class);
                            intent.putExtra("name", "Assessment");
                            intent.putExtra("url", URL);
                            context.startActivity(intent);
                        } else
                            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }
    }

    public static void deleteAllNotification(final Context context, final NotificationAdapter notificationAdapter) {
        if (WebServices.notificationUtilityArrayList != null && WebServices.notificationUtilityArrayList.size() > 0) {
            final ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...");
            dialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.progress_bar));
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (dialog != null) dialog.dismiss();
                    if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                        Utils.callInvalidSession(context, APIUtility.READ_NOTIFICATION);
                        return;
                    }
//                if (WebServices.notificationUtilityArrayList.size() != 0) {

                    int count = 0;
                    for (int i = 0; i < WebServices.notificationUtilityArrayList.size(); i++) {
                        if (!WebServices.notificationUtilityArrayList.get(i).getSent_status().equalsIgnoreCase("READ"))
                            count++;
                    }

//                    showNorificationPopup();
                    notificationAdapter.notifyDataSetChanged();
                   /* if (count == 0)
                        notifyTxtView.setText("Notifications");
                    else
                        notifyTxtView.setText("Notifications");*/
//
                }
            };
            new Thread() {
                @Override
                public void run() {
                    if (WebServices.notificationUtilityArrayList != null) {
                        String not_user_id = "";
                        String notifi_id = "";
                        int size = WebServices.notificationUtilityArrayList.size();
                        for (int i = 0; i < size; i++) {
                            if (i < size - 1)
                                not_user_id = not_user_id + WebServices.notificationUtilityArrayList.get(i).getNotification_user_id() + ",";
                            else {
                                not_user_id = not_user_id + WebServices.notificationUtilityArrayList.get(i).getNotification_user_id();
                            }
                        }
                        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair = new ArrayList<NameValuePair>();
                        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                        nameValuePair.add(new BasicNameValuePair("notification_user_id", not_user_id));
                        nameValuePair.add(new BasicNameValuePair("notification_id", notifi_id));
                        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                        nameValuePair.add(new BasicNameValuePair("sent_status", "READ"));
                        String resp = new WebServices().getLogin(nameValuePair, APIUtility.READ_NOTIFICATION);
                    }
                    WebServices.notificationUtilityArrayList.clear();
                    handler.sendEmptyMessage(0);
                }
            }.start();
        }
    }

    public static String convertSecondsToHMmSs(long seconds) {
//        long s = seconds % 60;
//        long m = (seconds / 60) % 60;
        long h = seconds / 3600;
        long m = (seconds % 3600) / 60;
        long s = seconds % 60;

        String timeString = String.format("%02d:%02d:%02d", h, m, s);
        System.out.println(timeString + "=================");
        return timeString;
    }

    public static void askLocationPermission(final Context mContext, MeterialUtility meterialUtility) {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((BaseActivity) mContext, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions((BaseActivity) mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        AppConstants.LOCATION_PERM);

            } else {
                ActivityCompat.requestPermissions((BaseActivity) mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        AppConstants.LOCATION_PERM);
            }
        } else {
            if (!Utils.isLocationEnabled(mContext))
                DialogUtils.showDialog(mContext, "Please enable your location", new Runnable() {
                    @Override
                    public void run() {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        ((Activity) mContext).startActivityForResult(myIntent, 101);
                    }
                }, null);
            else {
                Intent intent = new Intent(mContext, Link_Activity.class);
                intent.putExtra("meterial_utility", meterialUtility);
                mContext.startActivity(intent);
            }
        }
    }


    public static void setMaterialImage(Context context, final MeterialUtility meterialUtility, final ImageView imageView) {
        if (!TextUtils.isEmpty(meterialUtility.getMaterial_image())) {
           /* Picasso.Builder builder = new Picasso.Builder(context);
            builder.listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    Utils.setMaterialImage(meterialUtility.getMaterial_type(), imageView,false);
                }
            });
            builder.build().load(meterialUtility.getMaterial_image()).placeholder(R.mipmap.ic_launcher).into(imageView);*/
            Picasso.with(context)
                    .load(meterialUtility.getMaterial_image())
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .config(Bitmap.Config.RGB_565)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                        }
                        @Override
                        public void onError() {
                            Utils.setMaterialImage(meterialUtility.getMaterial_type(), imageView,false);
                        }


                    });
        } else
            Utils.setMaterialImage(meterialUtility.getMaterial_type(), imageView,false);
    }

    public static void setMaterialImage(Context context, final Favourites_Utility meterialUtility, final ImageView imageView) {
        if (!TextUtils.isEmpty(meterialUtility.getImage())) {

            Picasso.Builder builder = new Picasso.Builder(context);
            builder.listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    Utils.setMaterialImage(meterialUtility.getType(), imageView,false);
                }
            });
            builder.build().load(meterialUtility.getImage()).placeholder(R.mipmap.ic_launcher).into(imageView);
        } else
            Utils.setMaterialImage(meterialUtility.getType(), imageView,false);
    }

}

