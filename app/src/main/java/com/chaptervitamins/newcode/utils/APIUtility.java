package com.chaptervitamins.newcode.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.discussions.UploadListener;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.models.OrganizationModel;
import com.chaptervitamins.utility.NotificationModel;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by abcd on 4/19/2016.
 */
public class APIUtility {
    public static final String DEVICE = "Android";
    public static String DEVICEID = "";
    public static OrganizationModel organizationModel = new OrganizationModel();
    public static String SESSION = "";
    public static String SECOND_LANGUAGE_PREFERENCE = "";
    public static final String APIKEY = "df07120fdb13c62777351771054b53b6";
    //    private static final String BASE = " https://abhkmscms.adityabirlahealth.com/";//https://abhkmscms.chaptervitamins.com/";//http://alphacmspnbmetlife.chaptervitamins.com/webservice/";
//    private static final String BASE="http://staging.chaptervitamins.com/abhkmscms/";
//    private static final String BASE = "https://abhkmscms.chaptervitamins.com/";//https://abhkmsuatcms.chaptervitamins.com/";
//    private static final String BASE = "https://dev.chaptervitamins.com/centumcms";
    //  private static final String BASE = "http://centumcms.mobilestudyguide.in/";

    //    private static final String BASE = "https://dev.chaptervitamins.com/dcbcms/";
//    private static final String BASE = "http://abhkmsuatcms.chaptervitamins.co.in/";
    private static final String BASEURL = Constants.BASE + "webservice/";
    public static String LOGIN = BASEURL + "login";
    public static String SIGNUP = BASEURL + "open_register";
    public static String ASSIGNCOURSE = BASEURL + "get_assigned_course";
    public static String GETMYPROFILE = BASEURL + "get_my_profile";
    public static String QUIZ_DETAILS = BASEURL + "get_quiz_details";
    public static String VIDEO_QUIZ_DETAILS = BASEURL + "getMultiMediaQuizDetails";
    public static String SURVEY_DETAILS = BASEURL + "get_survey_details";
    public static String CONTENTINSHORTS_DETAILS = BASEURL + "get_content_inshort_details";
    public static String LOGOUT = BASEURL + "logout";
    public static String SUBMIT_ATTENDANCE = BASEURL + "submit_attendance";
    public static String GETREADRESPONSE = BASEURL + "get_response";
    public static String GET_SESSION_TIME = BASEURL + "get_session_time";
    public static String QUIZ_RESPONSE = BASEURL + "get_quiz_response";
    public static String CHANGE_PASSWORD = BASEURL + "change_password";
    public static String FORGET_PASSWORD = BASEURL + "forgot_password";
    public static String SUBMIT_RESPONSE = BASEURL + "submit_response";
    public static String FLASHCARD_DETAILS = BASEURL + "get_flash_card_details";
    public static String IMAGE_FLASHCARD_DETAILS = BASEURL + "get_image_card_details";
    public static String ORGANIZATION_DETAILS = BASEURL + "get_organisation_setting";
    public static String GETPENDINGQUIZ = BASEURL + "get_pending_response";
    public static String SUBMITPENDINGQUIZ = BASEURL + "submit_pending_response";
    public static String SUBMIT_SURVEY = BASEURL + "submit_survey";
    public static String GETALLGROUP = BASEURL + "get_assigned_groups";
    public static String GETSPECIFICQUIZHISTORY = BASEURL + "get_leaderboard";
    public static String GETSPECIFICQUIZHISTORY2 = BASEURL + "get_publish_result";
    public static String GETALLGROUPMEMBERS = BASEURL + "get_group_members";
    public static final String GETPOSTS = BASEURL + "get_group_forums";
    public static final String GETTRAINIGCALENDAR = BASEURL + "get_training_calendar";
    public static String CREATE_GROUP = BASEURL + "create_group";
    public static String ADD_GROUP_MEMBER = BASEURL + "add_group_member";
    public static String SUBMIT_FORUM = BASEURL + "submit_forum";
    public static String UPLOAD_FILE = BASEURL + "upload_media";
    public static String VIDEO_QUIZ_EVALLUATE = BASEURL + "getMultiMediaQuizResponse";
    public static String SUBMIT_COMMENT = BASEURL + "submit_forum_comment";
    public static String DELETE_FORUM = BASEURL + "delete_forum";
    public static String DELETE_COMMENT = BASEURL + "delete_forum_comment";
    public static String GET_NEWS_FEED = BASEURL + "get_notice_board";
    public static String LIKE = BASEURL + "like_item";
    public static String VIEW = BASEURL + "view_item";
    public static String SEND_OTP = BASEURL + "request_otp";
    public static String LOGIN_BY_OTP = BASEURL + "login";
    public static String RESEND_OTP = BASEURL + "resend_otp";
    public static String RESET_DEVICE = BASEURL + "reset_device";
    public static String ADD_FAVORITES = BASEURL + "add_favourite_item";
    public static String GET_ALL_FAVORITES = BASEURL + "get_favourite_item";
    public static String REMOVE_FAVORITES = BASEURL + "remove_favourite_item";
    public static String GETALLITEM_LIKE = BASEURL + "get_like_item";
    public static String UPDATE_PROFILE = BASEURL + "update_user_profile";
    public static String GETALL_NOTIFICATION = BASEURL + "get_notification";
    public static String READ_NOTIFICATION = BASEURL + "update_notification_status";
    public static String ADD_SUGGESTION_QUERY = BASEURL + "add_suggestion_query";
    public static String GET_SUGGESTION_QUERY = BASEURL + "get_suggestion_query";
    public static String GENERATE_CERTIFICATE = com.chaptervitamins.newcode.utils.Constants.BASE + "home/certificate/";
    public static final String GET_ASSIGNED_COMPETENCY = BASEURL + "get_assigned_competency";
    public static String GET_All_HOSPITALS = BASEURL + "get_hospitals";
    public static String GET_FAQ = BASEURL + "get_faq";
    public static String ROLE_ID = "1";
    public static String RateItem = BASEURL + "rate_item";
    private Dialog mDialog;
    private ProgressBar mBar;
    private TextView totalPer_txt, title_txt, total_file_txt;
    private Button abort_btn = null;
    private boolean isDownload = true;
    String MSG = "Network issues.. You have been Timed-out";
    private String FILEPATH = null;
    private int totallenghtOfFile = 0;
    private String totalVal = "-1";
    public static boolean showFlowingCourse = true;

    public enum ServiceEnum {
        GETFAQ(APIUtility.BASEURL + "get_faq"),
        DOWNLOAD("dffnjdhsdjfs");

        private String url;

        public String getUrl() {
            return url;
        }

        ServiceEnum(String url) {
            this.url = url;

        }
    }

    public static void isDeletedFileFromURL(Context context, String filename) throws IOException {
        File dir = new File(context.getExternalFilesDir(null) + com.chaptervitamins.newcode.utils.Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, filename);
        if (file.exists()) {
            file.delete();

        }
    }

    public static void isDeletedDirectory(Context context) throws IOException {

        File dir = new File(context.getExternalFilesDir(null) + com.chaptervitamins.newcode.utils.Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id());
        if (dir.exists())
            dir.delete();

    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static MixPanelManager getMixPanelManager(Context context) {
        MixPanelManager mixPanelManager = MixPanelManager.getInstance();
        if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_mixpanel_token()))
            AppConstants.mixpanelAPI = MixpanelAPI.getInstance(context, WebServices.mLoginUtility.getBranch_mixpanel_token());
        else
            AppConstants.mixpanelAPI = MixpanelAPI.getInstance(context, com.chaptervitamins.newcode.utils.Constants.MIXPANEL_PROJECT_TOKEN);
//        mixPanelManager.setDeviceId(context);
        return mixPanelManager;
    }
//    public static String getDeviceID(Context context)
//    {
//        String deviceID = "";
//
//        StringBuilder uniqueDeviceID = new StringBuilder();
//
//        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY))
//        {
//            deviceID = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
//            uniqueDeviceID.append(deviceID);
//        }
//
//		/*
//		 * Settings.Secure.ANDROID_ID returns the unique DeviceID Works for
//		 * Android 2.2 and above
//		 */
//        String androidId = Settings.Secure.getString(context.getContentResolver(),
//                Settings.Secure.ANDROID_ID);
//
//        uniqueDeviceID.append(androidId);
//
//        return uniqueDeviceID.toString();
//    }

    //    public static void shootBackgroundEvent(final Context context) {
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                String pausedActivity = SharedPreferenceManager.getSharedInstance().getPausedActivityName();
//                String currentActivity = SharedPreferenceManager.getSharedInstance().getCurrentActivityName();
//
//                if (pausedActivity.equals(currentActivity)) {
//                    MixPanelManager mixPanelManager = getMixPanelManager(context);
//                    mixPanelManager.appIsInBackground();
//                }
//            }
//        }, 2000);
//    }
    public static NotificationModel getNotificationModel(Context context, String notificationName, Object request, Object response) {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.context = context;
        notificationModel.notificationName = notificationName;
        notificationModel.requestObj = request;
        notificationModel.responseObj = response;

        return notificationModel;
    }

    public static void copyDirectory(File sourceLocation, File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists() && !targetLocation.mkdirs()) {
                throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            // make sure the directory we plan to store the recording in exists
            File directory = targetLocation.getParentFile();
            if (directory != null && !directory.exists() && !directory.mkdirs()) {
                throw new IOException("Cannot create dir " + directory.getAbsolutePath());
            }

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    public static File getFileFromURL(Context context, String mediaName) throws IOException {
        File oldFolder = new File(context.getExternalFilesDir(null) + com.chaptervitamins.newcode.utils.Constants.DIRECTORYNAME);
        if (oldFolder.exists()) {
            File newFolder = new File(context.getExternalFilesDir(null) + com.chaptervitamins.newcode.utils.Constants.DIRECTORYNAME);
            boolean success = oldFolder.renameTo(newFolder);
        }

        File dir = new File(context.getExternalFilesDir(null) + com.chaptervitamins.newcode.utils.Constants.DIRECTORYNAME + WebServices.mLoginUtility.getUser_id() + "/Downloaded/");
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir, mediaName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    public void DownloadFIle(final Context context, final String mUrl, final String filename, final UploadListener uploadListener) {
        isDownload = true;
        //        private ProgressDialog pDialog;
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mDialog.dismiss();

                if (msg.what == 0) {
                    if (FILEPATH != null) {
                        try {
                            String fname = getFileFromURL(context, filename).getAbsolutePath().toString();
                            uploadListener.complete(0, fname, "");
                        } catch (IOException e) {

                        }
                    } else {
                        uploadListener.error("Error Download File!");
                        try {
                            APIUtility.isDeletedFileFromURL(context, filename);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                        AppUtility.showToast(context, MSG);
                        Toast.makeText(context, MSG, Toast.LENGTH_LONG).show();
                    }
                } else {
                    uploadListener.error("Error Download File!");
                    try {
                        APIUtility.isDeletedFileFromURL(context, filename);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    AppUtility.showToast(context, MSG);
                    Toast.makeText(context, MSG, Toast.LENGTH_LONG).show();
                }
            }
        };
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.mprogressbar);
        mBar = (ProgressBar) mDialog.findViewById(R.id.download_progressbar);
        mBar.setMax(100);
        title_txt = (TextView) mDialog.findViewById(R.id.title_txt);
        total_file_txt = (TextView) mDialog.findViewById(R.id.total_file_txt);
        totalPer_txt = (TextView) mDialog.findViewById(R.id.total_progress_txt);
        abort_btn = (Button) mDialog.findViewById(R.id.abort_btn);
        abort_btn.setEnabled(false);
        abort_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDownload = false;
                FILEPATH = null;
                MSG = "You have aborted the Download";
                mDialog.dismiss();
                handler.sendEmptyMessage(1);
            }
        });
        mDialog.show();

        new Thread() {
            @Override
            public void run() {
                int lenghtOfFile = 0;
                int count;
                File file = null;

                try {
                    URL url = new URL(mUrl);

                    URLConnection conection = url.openConnection();
                    conection.setConnectTimeout(10000);
                    conection.setReadTimeout(10000);
                    conection.connect();
                    // getting file length
                    lenghtOfFile = conection.getContentLength();
                    System.out.println("=====file length===" + (lenghtOfFile / (1024 * 1024)));
                    // input stream to read file - with 8k buffer
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);


                    // Output stream to write file
                    //Download epub file in app private directory /sdcard/Android/data/<package-name>/files/chapters
                    file = getFileFromURL(context, filename);
                    OutputStream output = new FileOutputStream(file);
                    byte data[] = new byte[1024];
//                    runnable.run();
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        if (isDownload) {
                            total += count;
                            totalVal = "" + (int) ((total * 100) / lenghtOfFile);
                            totallenghtOfFile = lenghtOfFile;
//                            publishProgress();
//                            runnable.notify();//run();
                            // writing data to file

                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    abort_btn.setEnabled(true);
                                    if (totallenghtOfFile != -1) {
                                        String total = (totallenghtOfFile / (1024 * 1000f)) + "";
                                        String[] t = total.split("\\.");
                                        total_file_txt.setText(t[0] + "." + t[1].substring(0, 2) + " MB");
                                    } else {
                                        total_file_txt.setText("0 MB");
                                    }
                                    title_txt.setText("Fetching... Please wait!");
                                    if (!totalVal.contains("-")) {
                                        mBar.setProgress(Integer.parseInt(totalVal));
                                        totalPer_txt.setText(Integer.valueOf(totalVal) + " %");
                                    } else {
//                                        isDownload = false;
//                                        MSG = "Network issues.. You have been Timed-out";
//                                        mBar.setProgress(0);
//                                        totalPer_txt.setText("0 %");
//                                        handler.sendEmptyMessage(2);
                                    }
                                }
                            });
                            output.write(data, 0, count);
                        } else {
                            // flushing output
                            output.flush();

                            // closing streams
                            output.close();
                            input.close();
                            return;
                        }
                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    MSG = "Network issues.. You have been Timed-out";
                    FILEPATH = null;
                    handler.sendEmptyMessage(2);
                    return;
                }

//                try {
//                    new AESFileEncryption("DES/ECB/PKCS5Padding", file.getAbsolutePath()).encrypt();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                FILEPATH = file.getAbsolutePath();


                handler.sendEmptyMessage(0);
            }

        }.start();

//    public static boolean
    }


//    public static boolean doUnzip(String inputZipFile, String destinationDirectory)
//    {
//        try {
//            int BUFFER = 2048;
//            List<String> zipFiles = new ArrayList<String>();
//            File sourceZipFile = new File(inputZipFile);
//            File unzipDestinationDirectory = new File(destinationDirectory);
//            unzipDestinationDirectory.mkdir();
//            ZipFile zipFile;
//            zipFile = new ZipFile(sourceZipFile, ZipFile.OPEN_READ);
//            Enumeration zipFileEntries = zipFile.entries();
//            while (zipFileEntries.hasMoreElements()) {
//                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
//                String currentEntry = entry.getName();
//                File destFile = new File(unzipDestinationDirectory, currentEntry);
//                if (currentEntry.endsWith(".zip")) {
//                    zipFiles.add(destFile.getAbsolutePath());
//                }
//
//                File destinationParent = destFile.getParentFile();
//
//                destinationParent.mkdirs();
//
//                try {
//                    if (!entry.isDirectory()) {
//                        BufferedInputStream is =
//                                new BufferedInputStream(zipFile.getInputStream(entry));
//                        int currentByte;
//                        byte data[] = new byte[BUFFER];
//
//                        FileOutputStream fos = new FileOutputStream(destFile);
//                        BufferedOutputStream dest =
//                                new BufferedOutputStream(fos, BUFFER);
//                        while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
//                            dest.write(data, 0, currentByte);
//                        }
//                        dest.flush();
//                        dest.close();
//                        is.close();
//                    }
//                } catch (IOException ioe) {
//                    ioe.printStackTrace();
//                }
//            }
//            zipFile.close();
//
//            for (Iterator<String> iter = zipFiles.iterator(); iter.hasNext();) {
//                String zipName = (String)iter.next();
//                doUnzip(
//                        zipName,
//                        destinationDirectory +
//                                File.separatorChar +
//                                zipName.substring(0,zipName.lastIndexOf(".zip"))
//                );
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false ;
//        }
//        return true;
//    }

    public static String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("hospitals.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
