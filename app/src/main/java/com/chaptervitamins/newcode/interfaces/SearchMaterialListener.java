package com.chaptervitamins.newcode.interfaces;

/**
 * Created by Vijay Antil on 30-01-2018.
 */

public interface SearchMaterialListener {
    void onMaterialTypeSelect(String materialType);
}
