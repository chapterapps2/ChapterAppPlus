package com.chaptervitamins.newcode.interfaces;

/**
 * Created by Vijay Antil on 02-02-2018.
 */

public interface RatingListener {
    void onRatingBack();
}
