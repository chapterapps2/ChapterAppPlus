package com.chaptervitamins.newcode.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.adapters.DashboardAdapter;
import com.chaptervitamins.newcode.models.DashboardModel;
import com.chaptervitamins.socialkms.fragments.BaseFragment;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseDashboardFragment extends BaseFragment {
    private static final String DASHBOARD_TYPE = "DASHBOARD_TYPE";
    private int mDashboardType = 0;
    private RecyclerView mRvDashboard;
    private TextView mTvNoDataFound, mTvName, mTvCompletion;
    private ArrayList<DashboardModel> mDashboardModelArrayList;


    public CourseDashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CourseDashboardFragment.
     */
    public static CourseDashboardFragment newInstance(int dashboardType) {
        CourseDashboardFragment fragment = new CourseDashboardFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(DASHBOARD_TYPE, dashboardType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getArguments() != null)
            mDashboardType = getArguments().getInt(DASHBOARD_TYPE, 0);
        mDashboardModelArrayList = new ArrayList<>();
        return inflater.inflate(R.layout.fragment_course_dashboard, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
        setData();
    }

    private void setData() {
        switch (mDashboardType) {
            case 1:
                getAllCoursesList();
                break;
            case 2:
                getModulesList();
                break;
            case 3:
                getMaterialList();
                break;
            case 4:
                getLeaderboardList();
                break;
        }

    }

    private void getLeaderboardList() {
        mTvName.setText("USER NAME");
        mTvCompletion.setText("SCORE");
        mTvNoDataFound.setVisibility(View.VISIBLE);
        mTvNoDataFound.setText("No Data Found");
    }

    private void getMaterialList() {
        mTvName.setText("MATERIAL NAME");
        ArrayList<MeterialUtility> allMaterialsList = FlowingCourseUtils.getAllMaterials();
        if (allMaterialsList != null && allMaterialsList.size() > 0) {
            for (int i = 0; i < allMaterialsList.size(); i++) {
                MeterialUtility meterialUtility = allMaterialsList.get(i);
                DashboardModel dashboardModel = new DashboardModel();
                if (meterialUtility != null) {
                    dashboardModel.setCourseName(meterialUtility.getCourse_name());
                    dashboardModel.setCompletion(meterialUtility.getComplete_per());
                    dashboardModel.setModuleName(meterialUtility.getModuleName());
                    dashboardModel.setMaterialName(meterialUtility.getTitle());
                    mDashboardModelArrayList.add(dashboardModel);
                }
            }
            setAdapter("No Material Found");
        } else {
            mTvNoDataFound.setVisibility(View.VISIBLE);
            mTvNoDataFound.setText("No Material Found");
        }
    }

    private void getModulesList() {
        mTvName.setText("MODULE NAME");
        ArrayList<ModulesUtility> allModulesList = FlowingCourseUtils.getAllModules();
        if (allModulesList != null && allModulesList.size() > 0) {
            for (int i = 0; i < allModulesList.size(); i++) {
                ModulesUtility modulesUtility = allModulesList.get(i);
                DashboardModel dashboardModel = new DashboardModel();
                if (modulesUtility != null) {
                    dashboardModel.setCourseName(modulesUtility.getCourse_name());
                    dashboardModel.setCompletion(modulesUtility.getCompletion_per());
                    dashboardModel.setModuleName(modulesUtility.getModule_name());
                    mDashboardModelArrayList.add(dashboardModel);
                }
            }
            setAdapter("No Module Found");
        } else {
            mTvNoDataFound.setVisibility(View.VISIBLE);
            mTvNoDataFound.setText("No Module Found");
        }
    }

    private void setAdapter(String message) {

        if (mDashboardModelArrayList.size() > 0) {
            DashboardAdapter dashboardAdapter = new DashboardAdapter(mDashboardModelArrayList, mDashboardType);
            mRvDashboard.setAdapter(dashboardAdapter);
        } else {
            mTvNoDataFound.setVisibility(View.VISIBLE);
            mTvNoDataFound.setText(message);
        }
    }

    private void getAllCoursesList() {
        mTvName.setText("COURSE NAME");
        if (HomeActivity.courseUtilities != null && HomeActivity.courseUtilities.size() > 0) {
            for (int i = 0; i < HomeActivity.courseUtilities.size(); i++) {
                CourseUtility courseUtility = HomeActivity.courseUtilities.get(i);
                DashboardModel dashboardModel = new DashboardModel();
                if (courseUtility != null && !courseUtility.getCourse_type().equalsIgnoreCase("SUGGESTION_QUERY") && !courseUtility.getCourse_type().equalsIgnoreCase("DISCUSSION")) {
                    dashboardModel.setCourseName(courseUtility.getCourse_name());
                    dashboardModel.setCompletion(courseUtility.getCompletion_per());
                    mDashboardModelArrayList.add(dashboardModel);
                }
            }
            setAdapter("No Data Found");

        } else {
            mTvNoDataFound.setVisibility(View.VISIBLE);
            mTvNoDataFound.setText("No Data Found");
        }
    }

    private void findViews(View view) {
        mRvDashboard = (RecyclerView) view.findViewById(R.id.rv_dashboard);
        mTvNoDataFound = (TextView) view.findViewById(R.id.tv_no_data_found);
        mTvName = (TextView) view.findViewById(R.id.tv_course_name);
        mTvCompletion = (TextView) view.findViewById(R.id.tv_completion);
    }

    @Override
    protected String getTitle() {
        return null;
    }
}
