package com.chaptervitamins.newcode.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.utility.FlashCardUtility;
import com.squareup.picasso.Picasso;

public class TemplateSixFragment extends Fragment {
    ImageView imageView;
    TextView textView;
    Bundle cardData;
    FlashCardUtility flashCardUtility;
    int position;

    public static TemplateSixFragment newInstance(FlashCardUtility flashCardUtility, int position) {

        Bundle args = new Bundle();
        args.putParcelable("cardData", flashCardUtility);
        args.putInt("position", position);
        TemplateSixFragment fragment = new TemplateSixFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cardData = getArguments();
        assert cardData != null;
        if (cardData.containsKey("cardData")) {
            try {
                this.position = cardData.getInt("position");
                this.flashCardUtility = cardData.getParcelable("cardData");
            } catch (Exception e) {
                Log.e(TemplateSixFragment.class.getName(), e.getMessage());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_template_6, container, false);
        imageView = (ImageView) view.findViewById(R.id.image_template_6);
        textView = (TextView) view.findViewById(R.id.text_template_6);
        textView.setText(flashCardUtility.getQuestion_description());
        Toast.makeText(getContext(), flashCardUtility.getAnswer() + position, Toast.LENGTH_SHORT).show();

        Picasso.with(getContext())
                .load(R.drawable.poor_icon)
                .into(imageView);
        return view;
    }
}
