package com.chaptervitamins.newcode.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.utility.FlashCardUtility;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TemplateTwoFragment extends Fragment {
    ImageView imageView;

    @BindView(R.id.tv_ques_temp2)
    TextView tvQues;
    @BindView(R.id.tv_ques_no_temp2)
    TextView tvQuesNo;
    @BindView(R.id.tv_ans_temp2)
    TextView tvAns;

    Bundle cardData;
    FlashCardUtility flashCardUtility;
    int position;

    public static TemplateTwoFragment newInstance(FlashCardUtility flashCardUtility, int position) {

        Bundle args = new Bundle();
        args.putParcelable("cardData", flashCardUtility);
        args.putInt("position", position);
        TemplateTwoFragment fragment = new TemplateTwoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cardData = getArguments();
        assert cardData != null;
        if (cardData.containsKey("cardData")) {
            try {
                this.position = cardData.getInt("position");
                this.flashCardUtility = cardData.getParcelable("cardData");
            } catch (Exception e) {
                Log.e(TemplateTwoFragment.class.getName(), e.getMessage());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_template_2, container, false);
        ButterKnife.bind(this, view);
        tvQuesNo.setText("Question: " + position);
        tvQues.setText(flashCardUtility.getQuestion_description());
        tvAns.setText(flashCardUtility.getAnswer());
        Toast.makeText(getContext(), flashCardUtility.getAnswer() + position, Toast.LENGTH_SHORT).show();
        return view;
    }
}
