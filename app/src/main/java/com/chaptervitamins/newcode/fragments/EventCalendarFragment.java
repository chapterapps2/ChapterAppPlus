package com.chaptervitamins.newcode.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.customviews.CalendarCustomView;
import com.chaptervitamins.newcode.models.EventModel;
import com.chaptervitamins.socialkms.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventCalendarFragment extends BaseFragment {

    private static final String KEY_EVENT_LIST = "KEY_EVENT_LIST";
    private CalendarCustomView mCalendarCustomView;
    private ArrayList<EventModel> mEventArrayList;

    public EventCalendarFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EventCalendarFragment.
     */
    public static EventCalendarFragment newInstance(ArrayList<EventModel> eventModelArrayList) {
        EventCalendarFragment fragment = new EventCalendarFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_EVENT_LIST, eventModelArrayList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            mEventArrayList = (ArrayList<EventModel>) getArguments().getSerializable(KEY_EVENT_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_calendar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCalendarCustomView = (CalendarCustomView) view.findViewById(R.id.event_calendar);
        mCalendarCustomView.initialization(mEventArrayList);
    }

    @Override
    protected String getTitle() {
        return null;
    }
}
