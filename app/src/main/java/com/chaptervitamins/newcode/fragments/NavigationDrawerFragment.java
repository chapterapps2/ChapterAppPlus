package com.chaptervitamins.newcode.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.chaptervitamins.Suggestions.Suggestion_Main_Activity;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.LoginActivity;
import com.chaptervitamins.R;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.home.ChangePass_Activity;
import com.chaptervitamins.home.FavouritesList_Activity;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.myprofile.DashBoardActivity;
import com.chaptervitamins.myprofile.ManageDataActivity;
import com.chaptervitamins.myprofile.MyPointsActivity;
import com.chaptervitamins.myprofile.MyProfile_Activity;
import com.chaptervitamins.newcode.activities.CourseDashboardActivity;
import com.chaptervitamins.newcode.activities.EventCalendarActivity;
import com.chaptervitamins.newcode.activities.HomeActivity;
import com.chaptervitamins.newcode.adapters.NavDrawerAdapter;
import com.chaptervitamins.newcode.models.NavDrawerModel;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.socialkms.activity.SocialActivity;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment implements NavDrawerAdapter.OnDrawerItemSelectedListener {

    private RecyclerView drawerRecyclerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<NavDrawerModel> navDrawerModelArrayList = new ArrayList<>();
    private Toolbar mToolbar;
    public NavDrawerAdapter mAdapter;
    private HomeActivity mHomeActivity;

    public void inItUi() {
        drawerRecyclerView = (RecyclerView) getView().findViewById(R.id.rv_drawer);
        if (drawerRecyclerView != null)
            drawerRecyclerView.setHasFixedSize(true);
    }

    private void loadDrawerData() {
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("38") &&
                Constants.BRANCH_ID.equalsIgnoreCase("56")) {
            navDrawerModelArrayList.add(new NavDrawerModel("2", "Learning Snapshot", R.drawable.statistic));
        } else {
            navDrawerModelArrayList.add(new NavDrawerModel("2", "DashBoard", R.drawable.statistic));
        }
        navDrawerModelArrayList.add(new NavDrawerModel("9", "Course Dashboard", R.drawable.course_dash));
        navDrawerModelArrayList.add(new NavDrawerModel("5", "Favourites", R.drawable.to_b_favorites));
        navDrawerModelArrayList.add(new NavDrawerModel("9", "Points Earned", R.drawable.my_points));
        navDrawerModelArrayList.add(new NavDrawerModel("10", "Event Calendar", R.drawable.calendar_menu));
        navDrawerModelArrayList.add(new NavDrawerModel("6", "Manage Offline Content", R.drawable.manage_data));
        navDrawerModelArrayList.add(new NavDrawerModel("1", "My Profile", R.drawable.my_profile));
//        navDrawerModelArrayList.add(new NavDrawerModel("11", "Social KMS", R.drawable.calendar_menu));
        navDrawerModelArrayList.add(new NavDrawerModel("3", "Sync Data", R.drawable.ic_refresh_icon));
        navDrawerModelArrayList.add(new NavDrawerModel("4", "Change Password", R.drawable.change_password_icon));
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_id()) && WebServices.mLoginUtility.getBranch_id().equalsIgnoreCase("19")) {
           /* navDrawerModelArrayList.add(new NavDrawerModel("7", "ASK HR Toll Free", R.drawable.mailicon));
            navDrawerModelArrayList.add(new NavDrawerModel("11", "Mail to HR", R.drawable.mailicon));*/
        } else {
            navDrawerModelArrayList.add(new NavDrawerModel("7", "Contact Admin", R.drawable.mailicon));
        }
        navDrawerModelArrayList.add(new NavDrawerModel("8", "Logout", R.drawable.logout));

        if (navDrawerModelArrayList != null) {
            mAdapter = new NavDrawerAdapter(navDrawerModelArrayList, NavigationDrawerFragment.this);
            drawerRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            drawerRecyclerView.setAdapter(mAdapter);
        }
    }

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inItUi();
        loadDrawerData();
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar, HomeActivity activity) {
        mHomeActivity = activity;
        View containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mToolbar = toolbar;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
//                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }


    private void logout() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_dialog);
        Button cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!getActivity().isFinishing())
                    dialog.dismiss();
            }
        });
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mDatabase.deleteTable("MySurvey");
                if (!getActivity().isFinishing())
                    dialog.dismiss();
                logoutApiCall();

            }
        });
        dialog.show();
    }

    private void logoutApiCall() {
        if (WebServices.isNetworkAvailable(getActivity())) {
            final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Please wait...");
            dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String formattedDate = df.format(c.getTime());
            final ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("logout_time", formattedDate));
            new GenericApiCall(getActivity(), APIUtility.LOGOUT, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    clearData();
                }

                @Override
                public void onError(ErrorModel error) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    clearData();
                }
            }).execute();
        } else {
            clearData();
        }
    }

    private void clearData() {
        @SuppressLint("WifiManagerLeak")
        WifiManager wm = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        final String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        MixPanelManager mixPanelManager = APIUtility.getMixPanelManager(getActivity());
        userLogout(getActivity(), WebServices.mLoginUtility.getUser_id(), SplashActivity.mPref.getString("id", ""), WebServices.mLoginUtility.getFirstname(), ip);
        SharedPreferences.Editor editor = SplashActivity.mPref.edit();
        editor.clear();
        editor.commit();
        HomeActivity.courseUtilities = new ArrayList<CourseUtility>();
        HomeActivity.modulesUtilityArrayList = new ArrayList<ModulesUtility>();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.putExtra("sms_url", "");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    public void userLogout(Activity activity, String userID, String userEmail, String fname, String ipaddress) {

        String id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        if (userID == null)
            return;

        MixpanelAPI.People people = AppConstants.mixpanelAPI.getPeople();
        JSONObject jsonObject = new JSONObject();
        try {
            if (Utils.isValidEmaillId(userEmail))
                jsonObject.put(AppConstants.MIXPANAL_EMAIL, userEmail);
            else
                jsonObject.put(AppConstants.MIXPANAL_OTP, userEmail);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_TYPE, AppConstants.MIXPANAL_DEVICE_TYPE_ANDROID);
            jsonObject.put(AppConstants.MIXPANAL_LOGOUT_TIMESTAMP, Utils.getCurrentTimeStamp());
            jsonObject.put(AppConstants.MIXPANAL_FIRSTNAME, fname);
            jsonObject.put(AppConstants.MIXPANAL_DEVICE_ID, id);
            jsonObject.put(AppConstants.MIXPANAL_IPADDRESS, ipaddress);
            jsonObject.put(AppConstants.MIXPANAL_ORGANIZATION_NAME, WebServices.mLoginUtility.getOrganization_name());
        } catch (JSONException e) {

        }
        AppConstants.mixpanelAPI.identify(userID);
        people.identify(userID);
        AppConstants.mixpanelAPI.track(AppConstants.USER_LOGOUT_EVENT, jsonObject);
        AppConstants.mixpanelAPI.getPeople().showSurveyIfAvailable(activity);
        people.set(jsonObject);
    }

    @Override
    public void onDrawerItemSelected(NavDrawerModel model) {
        new MixPanelManager().selectDrawerItem(getActivity(), model.getTitle());
        Intent intent = null;
        switch (model.getTitle()) {
            case "Logout":
                logout();
                break;
            case "Points Earned":
                intent = new Intent(getActivity(), MyPointsActivity.class);
                startActivity(intent);
                break;
            case "Change Password":
                intent = new Intent(getActivity(), ChangePass_Activity.class);
                intent.putExtra("loginscreen", "");
                startActivity(intent);
                break;
            case "Favourites":
                intent = new Intent(getActivity(), FavouritesList_Activity.class);
                intent.putExtra("loginscreen", "");
                startActivity(intent);
                break;
            case "Manage Offline Content":
                intent = new Intent(getActivity(), ManageDataActivity.class);
                intent.putExtra("loginscreen", "");
                startActivity(intent);
                break;
            case "DashBoard":
                intent = new Intent(getActivity(), DashBoardActivity.class);
                startActivity(intent);
                break;
            case "Learning Snapshot":
                intent = new Intent(getActivity(), DashBoardActivity.class);
                startActivity(intent);
                break;
            case "Event Calendar":
                intent = new Intent(getActivity(), EventCalendarActivity.class);
                startActivity(intent);
                break;
            case "Course Dashboard":
                intent = new Intent(getActivity(), CourseDashboardActivity.class);
                startActivity(intent);
                break;
            case "ASK HR Toll Free":
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:1800 3000 1205"));
                startActivity(intent);
                break;
            case "Contact Admin":
                String contact_admin = "contact@com.chapterapps.net";
                if (!APIUtility.organizationModel.getAdminEmail().equalsIgnoreCase(""))
                    contact_admin = APIUtility.organizationModel.getAdminEmail();
                String[] bcc = {"contact@com.chapterapps.net"};
                Intent i = new Intent(Intent.ACTION_SENDTO);
//                i.setType("text/plain");

                i.putExtra(Intent.EXTRA_EMAIL, new String[]{contact_admin});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                i.putExtra(Intent.EXTRA_BCC, bcc);
                i.setType("message/rfc822");
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", contact_admin, null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case "Mail to HR":
                String contact_admin1 = "askhr@mahindrafcs.com";
                if (!APIUtility.organizationModel.getAdminEmail().equalsIgnoreCase(""))
                    contact_admin1 = APIUtility.organizationModel.getAdminEmail();
                String[] bcc1 = {"askhr@mahindrafcs.com"};
                Intent i1 = new Intent(Intent.ACTION_SEND);
                i1.setType("text/plain");
                i1.putExtra(Intent.EXTRA_EMAIL, new String[]{contact_admin1});
                i1.putExtra(Intent.EXTRA_SUBJECT, "");
                i1.putExtra(Intent.EXTRA_TEXT, "");
                i1.putExtra(Intent.EXTRA_BCC, bcc1);
                i1.setType("message/rfc822");
                try {
                    startActivity(Intent.createChooser(i1, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case "My Profile":
                intent = new Intent(getActivity(), MyProfile_Activity.class);
                getActivity().startActivityForResult(intent, 9999);
                break;
            case "Suggestion and Query Manager":
                intent = new Intent(getActivity(), Suggestion_Main_Activity.class);
                intent.putExtra("loginscreen", "");
                startActivity(intent);
                break;
            case "Guru":
               /* if (WebServices.isNetworkAvailable(HomeActivity.this)) {
                    intent = new Intent(HomeActivity.this, OnlineLinkOpenActivity.class);
                    intent.putExtra("loginscreen", "");
                    intent.putExtra("url", guruLink());
                    intent.putExtra("name", title);
                    startActivity(intent);
                } else {
                    Toast.makeText(HomeActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }*/

                break;
            case "Sync Data":
                mHomeActivity.syncDataToServer();
                break;
            case "Social KMS":
                if (WebServices.isNetworkAvailable(getActivity())) {
                    startActivity(new Intent(getActivity(), SocialActivity.class));

                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
                break;
        }
        if (mDrawerLayout.isShown()) mDrawerLayout.closeDrawers();

    }


}
