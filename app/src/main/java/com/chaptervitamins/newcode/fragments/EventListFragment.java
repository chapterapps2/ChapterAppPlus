package com.chaptervitamins.newcode.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.EventListAdapter;
import com.chaptervitamins.newcode.models.EventModel;
import com.chaptervitamins.socialkms.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventListFragment extends BaseFragment {


    private static final String KEY_EVENT_LIST = "KEY_EVENT_LIST";
    private ArrayList<EventModel> mEventArrayList;
    private RecyclerView mRvEvents;

    public EventListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EventListFragment.
     */
    public static EventListFragment newInstance(ArrayList<EventModel> eventModelArrayList) {
        EventListFragment fragment = new EventListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_EVENT_LIST, eventModelArrayList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            mEventArrayList = (ArrayList<EventModel>) getArguments().getSerializable(KEY_EVENT_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
        initData();

    }

    private void initData() {
        if(mEventArrayList!=null && mEventArrayList.size()>0){
            EventListAdapter adapter = new EventListAdapter(mEventArrayList);
            mRvEvents.setAdapter(adapter);
        }
    }

    private void findViews(View view) {
        mRvEvents = (RecyclerView) view.findViewById(R.id.rv_events);
    }

    @Override
    protected String getTitle() {
        return null;
    }
}
