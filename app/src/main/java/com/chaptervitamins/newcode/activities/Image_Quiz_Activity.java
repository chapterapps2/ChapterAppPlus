package com.chaptervitamins.newcode.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.downloadImages.Course_ImageLoader;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.customviews.NonSwipeableViewPager;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.quiz.ThankYou_Activity;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.FlashCardUtility;
import com.chaptervitamins.utility.MeterialUtility;

import java.text.DateFormat;
import java.util.Date;


public class Image_Quiz_Activity extends BaseActivity{
    NonSwipeableViewPager viewPager;
    private LinearLayout prev_ll, next_ll, timer_ll;
    private TextView total_txt, time_txt, prev_txt, next_txt, title;
    private ImageView back;
    int pos = 1;
    private long TIMERVALUE = 30;
    private boolean TimerVal = true;
    private Handler handler2 = new Handler();
    LayoutInflater inflater;
    private ProgressDialog dialog;
    private Course_ImageLoader imageDownloader;
    private static boolean onClickImg = false;
    private DataBase dataBase;
    String resp = "";
    WebServices webServices;
    Handler handler;
    MixPanelManager mixPanelManager;
    String startTime;
    String endTime;
    LinearLayout backLayout;
    private MeterialUtility mMeterialUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image__quiz);
//        Mint.initAndStartSession(this, Constants.CRASH_REPORT_KEY);
        prev_ll = (LinearLayout) findViewById(R.id.prev_ll);
        backLayout = (LinearLayout) findViewById(R.id.backLayout);
        next_ll = (LinearLayout) findViewById(R.id.next_ll);
        timer_ll = (LinearLayout) findViewById(R.id.timer_ll);
        total_txt = (TextView) findViewById(R.id.total_txt);
        time_txt = (TextView) findViewById(R.id.time_txt);
        prev_txt = (TextView) findViewById(R.id.prev_txt);
        next_txt = (TextView) findViewById(R.id.next_txt);
        back = (ImageView) findViewById(R.id.back);
        title = (TextView) findViewById(R.id.toolbar_title);
        dataBase = DataBase.getInstance(Image_Quiz_Activity.this);
        webServices = new WebServices();
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        imageDownloader = new Course_ImageLoader(Image_Quiz_Activity.this, WebServices.mLoginUtility.getUser_id());
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimerVal = false;
                endTime = DateFormat.getDateTimeInstance().format(new Date());
                submitDataToServer();
                finish();
            }
        });

        viewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager);
        dialog = ProgressDialog.show(Image_Quiz_Activity.this, "", "Please wait..");
        resp = dataBase.getFlashCardData(WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getMaterial_id());
        mixPanelManager = APIUtility.getMixPanelManager(Image_Quiz_Activity.this);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (dialog != null && dialog.isShowing() && !isFinishing()) dialog.dismiss();
//                if (isoffline) new FatchBgData().execute();
                if (msg.what == 1) {
                    Toast.makeText(Image_Quiz_Activity.this, WebServices.ERRORMSG, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    if (WebServices.imageFlashcardMeterialUtilities != null)
                        title.setText(WebServices.imageFlashcardMeterialUtilities.getTitle());
                    if (WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size() != 0) {
//                        if (WebServices.imageFlashcardMeterialUtilities.getQuestion_sequence().equalsIgnoreCase("RANDOM"))
//                        startTime();
                        viewPager.setAdapter(new CustomPagerAdapter(Image_Quiz_Activity.this));
                        prev_ll.setEnabled(false);
                        viewPager.setCurrentItem(pos-1);
                        TIMERVALUE = Integer.parseInt(WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(pos-1).getTime());
                        time_txt.setText(convertSecondsToHMmSs(TIMERVALUE) + " Sec");
                        startTime();

                    } else {
                        total_txt.setText("");
                        noFlashCardData();
                    }
                }
            }
        };

        new Thread() {
            public void run() {
                if (webServices.isValid(resp)) {
                    webServices.getImageFlashcardData(resp);
//                    list = WebServices.flashcardMeterialUtilities.getFlashCardUtilities();

                    handler.sendEmptyMessage(0);
                } else {
                    handler.sendEmptyMessage(1);
                }

            }
        }.start();


        next_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (next_txt.getText().toString().equalsIgnoreCase("Done")) {
                    submitDataToServer();
                    Intent intent = new Intent(Image_Quiz_Activity.this, ThankYou_Activity.class);
                    intent.putExtra("meterial_Utility",mMeterialUtility);
                    intent.putExtra("msg", "Thank You for completing the Image Cards.");
                    startActivity(intent);
                    finish();
                } else if (pos < WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size()) {
                    final ProgressDialog dialog = ProgressDialog.show(Image_Quiz_Activity.this, "", "");
                    final Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (dialog != null && dialog.isShowing() && !isFinishing())
                                dialog.dismiss();
                            pos++;
                            viewPager.setCurrentItem(pos - 1);

                            if (pos > 1) {
                                prev_txt.setVisibility(View.VISIBLE);
                                prev_ll.setEnabled(true);
                            }
                            if (pos == WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size())
                                next_txt.setText("DONE");
                            else next_txt.setText("NEXT");
                            total_txt.setText(pos + "/" + WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size());
                            if (!WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(pos - 1).isOpen()) {

                                TimerVal = true;
                                TIMERVALUE = Integer.parseInt(WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(pos - 1).getTime());
                                timer_ll.setVisibility(View.VISIBLE);
                                next_ll.setVisibility(View.GONE);
//                                prev_ll.setEnabled(false);
                                prev_txt.setTextColor(Color.WHITE);
                                time_txt.setText(convertSecondsToHMmSs(TIMERVALUE) + " Sec");
                                System.out.println("====TIMERVALUE===" + TIMERVALUE);
                                startTime();
                            }
                        }
                    };
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                handler.sendEmptyMessage(0);
                            }
                        }
                    }.start();
                }
            }
        });

        prev_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos > 0) {
                    TimerVal = false;
                    final ProgressDialog dialog = ProgressDialog.show(Image_Quiz_Activity.this, "", "");
                    final Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (dialog != null && dialog.isShowing() && !isFinishing())
                                dialog.dismiss();
                            pos--;
                            total_txt.setText(pos + "/" + WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size());
                            viewPager.setCurrentItem(pos - 1);
                            if (pos == 1) {
                                prev_txt.setVisibility(View.GONE);
                                prev_ll.setEnabled(false);
                            }
                            timer_ll.setVisibility(View.GONE);
                            next_ll.setVisibility(View.VISIBLE);
                            if (pos == WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size())
                                next_txt.setText("DONE");
                            else next_txt.setText("NEXT");
                        }
                    };
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                handler.sendEmptyMessage(0);
                            }
                        }
                    }.start();
                }
            }
        });
      /*//  ImageView imageView=(ImageView)viewPager.getChildAt(pos - 1).findViewById(R.id.img_row);
        TIMERVALUE=Integer.parseInt(WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(pos).getTime());
        time_txt.setText(convertSecondsToHMmSs(TIMERVALUE) + " Sec");
        startTime();*/
    }

    /*@Override
    public void onBackPressed() {
    }*/

    private void submitDataToServer() {
        try {
            mixPanelManager.selectTimeTrack(Image_Quiz_Activity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                    , WebServices.imageFlashcardMeterialUtilities.getTitle(), "Imagequiz");
            CoinsAllocatedModel coinsAllocatedModel = mMeterialUtility.getCoinsAllocatedModel();
            String coinsCollected = "0", redeem = "false";
            int total = WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size();
            int result = total;
            if (pos != 0)
                result = (pos * 100) / total;
            if (coinsAllocatedModel != null) {
                redeem = coinsAllocatedModel.getRedeem();
                coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility,result, coinsAllocatedModel.getRedeem()));
            }
            new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, pos + "", WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size() + "", coinsCollected);

            new WebServices().addSubmitResponse(dataBase, mMeterialUtility, result + "", "0", "0", "0", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

            if (WebServices.isNetworkAvailable(Image_Quiz_Activity.this))
                new SubmitData(Image_Quiz_Activity.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), null, dataBase).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void noFlashCardData() {
        new AlertDialog.Builder(Image_Quiz_Activity.this)
                .setTitle("No Imagecard")
                .setMessage("Not Available Imagecard for you.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TimerVal) {
            TimerVal = true;
            startTime();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!onClickImg)
            TimerVal = false;

    }

    public static String convertSecondsToHMmSs(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        return String.format("%02d:%02d", m, s);
    }

    private void startTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (TimerVal) {
                    try {
                        Thread.sleep(1000);
                        handler2.post(new Runnable() {

                            @Override
                            public void run() {
                                if (TIMERVALUE != 0) {
                                    time_txt.setText(convertSecondsToHMmSs(--TIMERVALUE) + " Sec");
                                } else {
                                    FlashCardUtility utility = WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(pos - 1);
                                    utility.setOpen(true);
                                    WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().set(pos - 1, utility);
                                    TimerVal = false;
                                    timer_ll.setVisibility(View.GONE);
                                    next_ll.setVisibility(View.VISIBLE);
//                                    prev_ll.setEnabled(true);
                                    prev_txt.setTextColor(Color.WHITE);
                                    time_txt.setText("00:30 Sec");
                                    if (pos == WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size())
                                        next_txt.setText("DONE");
                                    else next_txt.setText("NEXT");
                                }

                            }
                        });
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onClickImg = false;
    }

    public class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;


        public CustomPagerAdapter(Context context) {
            mContext = context;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
//            ModelObject modelObject = ModelObject.values()[position];
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.img_quiz_row, collection, false);
            TextView ans_txt = (TextView) layout.findViewById(R.id.ans_txt);
            ImageView imageView = (ImageView) layout.findViewById(R.id.img_row);
            Bitmap imageBitmap = dataBase.getImage(WebServices.mLoginUtility.getUser_id(), WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(position).getQuestion_description());
            if (imageBitmap != null) {
                imageView.setImageBitmap(imageBitmap);
            } else {
                imageDownloader.DisplayImage(WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(position).getQuestion_description(), imageView);
            }

//            imageView.setImageResource(images[position]);
            ans_txt.setText(WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(position).getAnswer());
           /* imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickImg=true;
                    Intent intent=new Intent(Image_Quiz_Activity.this, FullScreenImageActivity.class);
                    intent.putExtra("imgurl",WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().get(position).getQuestion_description());
                    intent.putExtra("pos",position);
                    startActivityForResult(intent,200);
                }
            });*/
            collection.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return WebServices.imageFlashcardMeterialUtilities.getFlashCardUtilities().size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

//        @Override
//        public CharSequence getPageTitle(int position) {
//            ModelObject customPagerEnum = ModelObject.values()[position];
//            return mContext.getString(customPagerEnum.getTitleResId());
//        }

    }
}
