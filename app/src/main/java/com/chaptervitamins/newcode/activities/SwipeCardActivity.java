package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.adapters.CapsuleCardAdapter;
import com.chaptervitamins.newcode.models.Capsule;
import com.chaptervitamins.utility.MeterialUtility;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SwipeCardActivity extends BaseActivity implements CardStackView.CardEventListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    @BindView(R.id.swipeView)
    CardStackView mSwipeView;
    CapsuleCardAdapter adapter;
    ArrayList<Capsule> mList;
    @BindView(R.id.seekbar_capsule)
    SeekBar seekBar;
    FloatingActionButton fabBack;
    int count = 0;
    OnVideoPlayerStopped mListener;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public interface OnVideoPlayerStopped {
        void onPlayerStopped();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_cards);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        mList = bundle.getParcelableArrayList("capsuleList");
        meterialUtilityArrayList = (ArrayList<MeterialUtility>) bundle.getSerializable("meterial_list");
        assert mList != null;

        adapter = new CapsuleCardAdapter(this);
        fabBack = (FloatingActionButton) findViewById(R.id.backButton);
        adapter.addAll(mList);
        mSwipeView.setAdapter(adapter);
        mSwipeView.setVisibleCount(mList.size());
        seekBar.setProgress(0);
        seekBar.setMax(mList.size());
        seekBar.setEnabled(false);
        /*Toast.makeText(this, mList.size(), Toast.LENGTH_SHORT).show();*/
        mSwipeView.setCardEventListener(this);
        mSwipeView.setElevationEnabled(true);
        fabBack.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onCardDragging(float percentX, float percentY) {

    }

    @Override
    public void onCardSwiped(SwipeDirection direction) {
        if (direction == SwipeDirection.Left) {
            Toast.makeText(this, "Left Swipe", Toast.LENGTH_SHORT).show();
        }
        if (direction == SwipeDirection.Right) {
            Toast.makeText(this, "Right Swipe", Toast.LENGTH_SHORT).show();
        }
        if (direction == SwipeDirection.Bottom) {
            Toast.makeText(this, "Bottom Swipe", Toast.LENGTH_SHORT).show();
        }
        if (direction == SwipeDirection.Top) {
            Toast.makeText(this, "Top Swipe", Toast.LENGTH_SHORT).show();
        }
        seekBar.setProgress(mSwipeView.getTopIndex());
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        /*mListener.onPlayerStopped();*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*mListener.onPlayerStopped();*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*mListener.onPlayerStopped();*/
    }


    @Override
    public void onCardReversed() {
    }

    @Override
    public void onCardMovedToOrigin() {

    }

    @Override
    public void onCardClicked(int index) {

    }

    @Override
    public void onClick(View view) {
        mSwipeView.reverse();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        seekBar.setProgress(0);

        submitDataToServer();
    }

    private void submitDataToServer() {
        for (int i = 0; i < meterialUtilityArrayList.size(); i++) {
            int startIndex = Integer.parseInt(meterialUtilityArrayList.get(i).getCapsuleStartIndex());
            int endIndex = Integer.parseInt(meterialUtilityArrayList.get(i).getCapsuleEndIndex());
            int skipQues = 0, correctQues = 0, incorrectQues = 0, percent = 0;
            if (meterialUtilityArrayList.get(i).getMaterial_type().equalsIgnoreCase("quiz")) {
                for (int j = startIndex; j <= endIndex; j++) {
                    Capsule capsule = mList.get(j);
                    if (capsule != null) {
                        if (!TextUtils.isEmpty(capsule.getUserAns())) {
                            if (capsule.getUserAns().equalsIgnoreCase(capsule.getCorrectOption()))
                                correctQues++;
                            else
                                incorrectQues++;
                        } else {
                            skipQues++;
                        }
                    }
                }
                percent = ((correctQues) * 100) / ((endIndex+1) - startIndex);
                savequizResponse(percent + "", correctQues + "", incorrectQues + "", true, startIndex, (endIndex+1), meterialUtilityArrayList.get(i));
            } else if (meterialUtilityArrayList.get(i).getMaterial_type().equalsIgnoreCase("VIDEO")) {
                submitContentINShortsToServer(false, meterialUtilityArrayList.get(i));
            } else if (meterialUtilityArrayList.get(i).getMaterial_type().equalsIgnoreCase("CONTENTINSHORT")) {
                submitContentINShortsToServer(false, meterialUtilityArrayList.get(i));
            }
        }
    }


    private void savequizResponse(String persentage, String corr, String incorr, boolean isBack, int startINdex, int endINdex, MeterialUtility meterialUtility) {
        JSONArray jsonArray = new JSONArray();
        for (int i = startINdex; i < endINdex; i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("question_bank_id", mList.get(i).getQuestionBankId());
                jsonObject.put("assign_question_id", mList.get(i).getAssignQuestionId());
                jsonObject.put("answer_key", mList.get(i).getUserAns());
                jsonObject.put("correct_option", mList.get(i).getCorrectOption());
                jsonObject.put("answer", mList.get(i).getUserAns());
                jsonObject.put("question_description", mList.get(i).getDescription());
                jsonObject.put("answer_type", mList.get(i).getUserAns());
                if (!TextUtils.isEmpty(mList.get(i).getCorrectOption()) && !TextUtils.isEmpty(mList.get(i).getUserAns()) &&
                        mList.get(i).getCorrectOption().equalsIgnoreCase(mList.get(i).getUserAns()))
                    jsonObject.put("marks", "1");
                else
                    jsonObject.put("marks", "0");
                jsonObject.put("time_taken", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, (startINdex + endINdex) + "", (startINdex + endINdex) + "", "0");
        new WebServices().addSubmitResponse(dataBase, meterialUtility, persentage, corr, incorr, jsonArray.toString(), "0", "false", WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());
        if (WebServices.isNetworkAvailable(SwipeCardActivity.this)) {
//            dataBase.deleteRowGetResponse(WebServices.questionUtility.getMaterial_id());
            new SubmitData(SwipeCardActivity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), null, dataBase).execute(corr, incorr, jsonArray.toString(), persentage);
        } else {
            if (!isBack) {
                /*coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(Integer.valueOf(persentage), redeem));
                if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                    coinsAllocatedModel.setRedeem("TRUE");
                    if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                        WebServices.updateTotalCoins(MainActivity.this, coinsCollected);

                }*/
            }
//            new WebServices().addSubmitResponse(dataBase, meterialUtility.getCourse_id(), WebServices.questionUtility.getMaterial_id(), "QUIZ", persentage, corr, incorr, WebServices.questionUtility.getTest_pattern(), WebServices.questionUtility.getTitle(), jsonArray.toString(), coinsCollected, redeem, com.chap_tempone.home.MainActivity.MODULEID, com.chap_tempone.home.MainActivity.ASSIGNMATERIALID);


        }
    }

    private void submitContentINShortsToServer(boolean wannaFinish, MeterialUtility mMeterialUtility) {
        new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, ((Integer.parseInt(mMeterialUtility.getCapsuleEndIndex()) - Integer.parseInt(mMeterialUtility.getCapsuleStartIndex()))+1) + "", (Integer.parseInt(mMeterialUtility.getCapsuleEndIndex())+1) + "", "0");
        new WebServices().setSeenCountOfMaterialInStaticList(mMeterialUtility);
        new WebServices().addSubmitResponse(dataBase, mMeterialUtility, "100", "", "", "", "0", "false", WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(SwipeCardActivity.this))
            new SubmitData(SwipeCardActivity.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), null, DataBase.getInstance(SwipeCardActivity.this)).execute();

    }
}
