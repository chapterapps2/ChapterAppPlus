package com.chaptervitamins.newcode.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.adapters.MaterialTypeAdapter;
import com.chaptervitamins.newcode.adapters.SearchMaterialAdapter;
import com.chaptervitamins.newcode.interfaces.SearchMaterialListener;
import com.chaptervitamins.newcode.models.MaterialTypeModel;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

public class SearchActivity extends BaseActivity implements SearchMaterialListener, SearchView.OnQueryTextListener {

    private RecyclerView mRvMaterialType, mRvMaterials;
    private ArrayList<MeterialUtility> meterialUtilityArrayList, mTempMeterialsList;
    private String mSearchText = "", mMaterialType = "ALL";
    private SearchMaterialAdapter searchMaterialAdapter;
    private TextView mTvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViews();
        initData();
        setData();
    }

    private void setData() {
        searchMaterialAdapter = new SearchMaterialAdapter(mTempMeterialsList);
        mRvMaterials.setAdapter(searchMaterialAdapter);
        searchData(mMaterialType);
    }

    private void initData() {
        meterialUtilityArrayList = FlowingCourseUtils.getAllMaterials();
        mTempMeterialsList = new ArrayList<>();
        ArrayList<MaterialTypeModel> materialTypeModelArrayList = new ArrayList<>();
        MaterialTypeModel model = new MaterialTypeModel("ALL","ALL");
        model.setSelected(true);
        materialTypeModelArrayList.add(model);
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.VIDEO,"VIDEO"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.QUIZ,"QUIZ"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.PDF,"PDF"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.FLASHCARD,"FLASHCARD"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.MULTIMEDIAQUIZ,"VIDEO QUIZ"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.CONTENTINSHORT,"CONTENT IN SHORT"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.TINCAN_SCROM,"TINCAN SCORM"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.AUDIO,"AUDIO"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.FLASH,"FLASH"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.SURVEY,"SURVEY"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.IMAGECARD,"IMAGECARD"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.CONTENT,"CONTENT"));
        materialTypeModelArrayList.add(new MaterialTypeModel(AppConstants.MaterialType.FULL_TEXT,"EPUB"));
        MaterialTypeAdapter materialTypeAdapter = new MaterialTypeAdapter(materialTypeModelArrayList, this);
        mRvMaterialType.setAdapter(materialTypeAdapter);
    }

    private void findViews() {
        mRvMaterials = (RecyclerView) findViewById(R.id.rv_material);
        mRvMaterialType = (RecyclerView) findViewById(R.id.rv_material_type);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvMaterialType.setLayoutManager(layoutManager);
    }

    @Override
    public void onMaterialTypeSelect(String materialType) {
        this.mMaterialType = materialType;
        if (!TextUtils.isEmpty(materialType)) {
            searchData(materialType);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_groups, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = (SearchView) searchItem.getActionView();
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setSubmitButtonEnabled(false);
            searchView.setOnQueryTextListener(this);
            searchView.setIconifiedByDefault(false);
        }
        return true;
    }

    private void searchData(String materialType) {
        this.mTempMeterialsList.clear();
        for (int i = 0; i < meterialUtilityArrayList.size(); i++) {
            MeterialUtility meterialUtility = meterialUtilityArrayList.get(i);
            if (meterialUtility != null) {
                if (materialType.equalsIgnoreCase("ALL")) {
                    if (meterialUtility.getTitle().toLowerCase().startsWith(mSearchText.toLowerCase()) || meterialUtility.getMaterial_type().toLowerCase().startsWith(mSearchText.toLowerCase())) {
                        mTempMeterialsList.add(meterialUtility);
                    }
                } else {
                    if (meterialUtility.getMaterial_type().equalsIgnoreCase(materialType)) {
                        if (meterialUtility.getTitle().toLowerCase().startsWith(mSearchText.toLowerCase()) || meterialUtility.getMaterial_type().toLowerCase().startsWith(mSearchText.toLowerCase())) {
                            mTempMeterialsList.add(meterialUtility);
                        }
                    }
                }
            }
        }
        if (searchMaterialAdapter != null)
            searchMaterialAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        this.mSearchText = newText;
        searchData(mMaterialType);
        return true;
    }
}
