package com.chaptervitamins.newcode.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.DialogUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tanuj on 10/4/2016.
 */

public class AttendenceActivity extends GetLocationActivity implements View.OnClickListener {
    MixPanelManager mixPanelManager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.locBtn)
    Button locBtn;
    @BindView(R.id.dateBtn)
    Button dateBtn;
    @BindView(R.id.branBtn)
    Button branBtn;
    @BindView(R.id.subBTN)
    Button subBTN;
    @BindView(R.id.noteEditText)
    EditText noteEditText;
    @BindView(R.id.radGrp)
    RadioGroup radGrp;
    WebServices webServices;
    String branch;
    String selection;
    Calendar calendar;
    String dateSelect;
    boolean status;
    String note;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.attendence_layout);
        ButterKnife.bind(this);
        calendar = Calendar.getInstance();
        webServices = new WebServices();
        back.setOnClickListener(this);
        back.setOnClickListener(this);
        subBTN.setOnClickListener(this);
        dateBtn.setOnClickListener(this);
        locBtn.setOnClickListener(this);
        title.setText(getIntent().getStringExtra("name"));
        locBtn.setText(getIntent().getStringExtra("location"));
        branBtn.setText(WebServices.mLoginUtility.getBranch_name().toString());
        note = noteEditText.getText().toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(AttendenceActivity.this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back) {
            finish();
        } else if (v.getId() == R.id.subBTN) {
            if (branBtn.getText() != null)
                if (radGrp.getCheckedRadioButtonId() != -1) {
                    int id = radGrp.getCheckedRadioButtonId();
                    View radioButton = radGrp.findViewById(id);
                    int radioId = radGrp.indexOfChild(radioButton);
                    RadioButton btn = (RadioButton) radGrp.getChildAt(radioId);
                    selection = (String) btn.getText();
                }
            if (!dateBtn.getText().toString().equalsIgnoreCase("Select Date")) {
                if (WebServices.isNetworkAvailable(AttendenceActivity.this)) {
                    submitApiCall();
                } else {
                    Toast.makeText(AttendenceActivity.this, "Can't proceed. No internet connection.", Toast.LENGTH_SHORT).show();
                }
            } else
                Toast.makeText(AttendenceActivity.this, "Please select one time date for attendance", Toast.LENGTH_LONG).show();

        } else if (v.getId() == R.id.dateBtn) {
            new DatePickerDialog(AttendenceActivity.this, date, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show();
        } else if (v.getId() == R.id.locBtn) {
            String uri = "http://maps.google.com/maps?q=loc:" + getIntent().getStringExtra("lat_log");
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(intent);
        }
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        try {
            String myFormat = "yyyy-MM-dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            dateBtn.setText(sdf.format(calendar.getTime()));
            dateSelect = dateBtn.getText().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submitApiCall() {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait...");
        ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
        String resp = "";
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("branch_id",WebServices.mLoginUtility.getBranch_id()));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("location", getIntent().getStringExtra("lat_log") + "," + getIntent().getStringExtra("location")));
        nameValuePair.add(new BasicNameValuePair("date", dateSelect));
        nameValuePair.add(new BasicNameValuePair("note", note));
        nameValuePair.add(new BasicNameValuePair("branch", WebServices.mLoginUtility.getBranch_name()));
        nameValuePair.add(new BasicNameValuePair("working_location", selection));
        nameValuePair.add(new BasicNameValuePair("employee_id", WebServices.mLoginUtility.getEmployee_id()));
        new GenericApiCall(AttendenceActivity.this, APIUtility.SUBMIT_ATTENDANCE, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                if (webServices.isValid((String) result)) {
                    try {
                        if (dialog != null && dialog.isShowing()) {
                            DialogUtils.showDialog(AttendenceActivity.this, "Thank you, Your attendance is successfully marked.!!");
                            dialog.dismiss();
                            noteEditText.getText().clear();
                        } else {
                            DialogUtils.showDialog(AttendenceActivity.this, "Your attendance already marked,Please select new date");
                            dialog.dismiss();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        finish();
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onError(ErrorModel error) {
            }
        }).execute();
    }

}
