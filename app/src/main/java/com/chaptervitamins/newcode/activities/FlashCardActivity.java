package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.util.Log;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.fragments.TemplateFlashCard;
import com.chaptervitamins.utility.FlashCardUtility;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

public class FlashCardActivity extends BaseActivity {
    ArrayList<FlashCardUtility> mList;
    MeterialUtility meterialUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_card2);

        try {
            meterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
            String response = dataBase.getFlashCardData(WebServices.mLoginUtility.getUser_id(), meterialUtility.getMaterial_id());
            mList = mWebServices.getNewFlashcardData(response);
        } catch (Exception e) {
            Log.e("Database Error", e.getMessage());
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_flashcard_container, TemplateFlashCard.newInstance(mList), "")
                    .commit();
        }
    }
}
