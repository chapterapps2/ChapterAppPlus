package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.LeaderboardAdapter;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ReadResponseUtility;

import java.util.ArrayList;

public class LeaderboardActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTvTitle, mTvMyRank, mTvMarks, mTvCorrect, mTvIncorrect, mTvSkipped;
    private RecyclerView mRvLeaderboard;
    private MeterialUtility mMeterialUtility;
    private LinearLayout llQuestions;
    private ArrayList<ReadResponseUtility> allQuizResponseAl;
    private boolean showDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_leaderboard);
        findViews();
        setListeners();
        initData();
    }

    private void initData() {
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            mMeterialUtility = (MeterialUtility) bundle.getSerializable("meterial_utility");
            showDetails = bundle.getBoolean("show_details");
            if (!showDetails)
                llQuestions.setVisibility(View.GONE);
            else {
                mTvMarks.setText(bundle.getInt("marks") + "%");
                mTvCorrect.setText(bundle.getInt("correct_ques") + " CORRECT");
                mTvIncorrect.setText(bundle.getInt("incorrect_ques") + " INCORRECT");
                mTvSkipped.setText(bundle.getInt("skipped_ques") + " UNANSWERED");
            }
            if (mMeterialUtility != null) {
                mTvTitle.setText("Leaderboard");
            }
            String resp = bundle.getString("resp");
            if (!TextUtils.isEmpty(resp)) {
                allQuizResponseAl = new WebServices().parsespecificquiz(resp);
            }
        }
//        showResultData();
        setData();
        setAdapter();
    }

    private void showResultData() {
        boolean showResult = false;
        for (int i = 0; i < HomeActivity.mReadResponse.size(); i++) {
            ReadResponseUtility readResponseUtility = HomeActivity.mReadResponse.get(i);
            if (readResponseUtility != null) {
                if (readResponseUtility.getMaterial_id().equals(mMeterialUtility.getMaterial_id()) &&
                        readResponseUtility.getAssign_material_id().equals(mMeterialUtility.getAssign_material_id())) {
                    showResult = true;
                    if (!showDetails )
                        mTvMarks.setText(readResponseUtility.getResult());
                   /* mTvCorrect.setText(readResponseUtility.getCorrect_question() + " CORRECT");
                    mTvIncorrect.setText(readResponseUtility.getIncorrect_question() + " INCORRECT");
                    mTvSkipped.setText(readResponseUtility.getUnanswered_question() + " UNANSWERED");*/
                    break;
                }
            }
        }
        if (!showResult || allQuizResponseAl.size()==0) {
            findViewById(R.id.card_personal).setVisibility(View.GONE);
            try {
                TextView tvNotAttempted = (TextView) findViewById(R.id.tv_not_attempted_msg);
                tvNotAttempted.setVisibility(View.VISIBLE);
                tvNotAttempted.setText(getString(R.string.not_attempted) + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setData() {
        boolean showResult = false;
        for (int i = 0; i < allQuizResponseAl.size(); i++) {
            ReadResponseUtility readResponseUtility = allQuizResponseAl.get(i);
            if (readResponseUtility != null) {
                if (readResponseUtility.getUser_id().equals(WebServices.mLoginUtility.getUser_id())) {
                    showResult= true;
                    mTvMyRank.setText(i + 1 + "");
                    if(!showDetails)
                        mTvMarks.setText(readResponseUtility.getResult()+"%");
                    break;
                }
            }
        }
        if (!showResult) {
            findViewById(R.id.card_personal).setVisibility(View.GONE);
            try {
                TextView tvNotAttempted = (TextView) findViewById(R.id.tv_not_attempted_msg);
                tvNotAttempted.setVisibility(View.VISIBLE);
                tvNotAttempted.setText(getString(R.string.not_attempted) + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter() {
        if (allQuizResponseAl != null) {
            LeaderboardAdapter leaderboardAdapter = new LeaderboardAdapter(allQuizResponseAl);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            mRvLeaderboard.setLayoutManager(layoutManager);
            mRvLeaderboard.setAdapter(leaderboardAdapter);
        }
    }

    private void setListeners() {
        findViewById(R.id.back).setOnClickListener(this);
    }

    private void findViews() {
        mTvTitle = (TextView) findViewById(R.id.title);
        mRvLeaderboard = (RecyclerView) findViewById(R.id.rv_leaderboard);
        mTvMyRank = (TextView) findViewById(R.id.tv_my_rank);
        mTvMarks = (TextView) findViewById(R.id.tv_marks);
        mTvCorrect = (TextView) findViewById(R.id.tv_correct);
        mTvIncorrect = (TextView) findViewById(R.id.tv_incorrect);
        mTvSkipped = (TextView) findViewById(R.id.tv_unaswered);
        llQuestions = (LinearLayout) findViewById(R.id.ll_questions);
        findViewById(R.id.search_img).setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }
}
