package com.chaptervitamins.newcode.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.chaptervitamins.utility.MeterialUtility;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;

/**
 * Created by Vijay Antil on 08-05-2018.
 */

abstract class BaseVideoActivity extends BaseActivity {
    protected SimpleExoPlayerView simpleExoPlayerView;
    protected SimpleExoPlayer player;
    protected Timeline.Window window;
    protected DefaultTrackSelector trackSelector;
    protected DataSource.Factory mediaDataSourceFactory;
    protected boolean shouldAutoPlay;
    protected BandwidthMeter bandwidthMeter;
    protected ImageView ivHideControllerButton;
    protected String filepath = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSimpleExoPlayerView();
    }

    public void setSimpleExoPlayerView() {
        simpleExoPlayerView = (SimpleExoPlayerView) findViewById(getPlayerId());
    }

    private void initData() {
        shouldAutoPlay = false;
        bandwidthMeter = new DefaultBandwidthMeter();
        mediaDataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"), (TransferListener<? super DataSource>) bandwidthMeter);
        window = new Timeline.Window();
//        ivHideControllerButton = (ImageView) findViewById(R.id.exo_controller);
    }

    private void initializePlayer() {
        simpleExoPlayerView.requestFocus();

        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);

        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        simpleExoPlayerView.setPlayer(player);
        /*if (!TextUtils.isEmpty(mMeterialUtility.getSeen_count()) && !mMeterialUtility.getComplete_per().equals("100")) {
            player.seekTo(Long.parseLong(mMeterialUtility.getSeen_count()));
        }*/

        player.setPlayWhenReady(shouldAutoPlay);

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(filepath),
                mediaDataSourceFactory, extractorsFactory, null, null);

        player.prepare(mediaSource);
        if (ivHideControllerButton != null)
            ivHideControllerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    simpleExoPlayerView.hideController();
                }
            });
    }

    abstract int getPlayerId();

    private void releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player.getPlayWhenReady();
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        initData();

        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

}
