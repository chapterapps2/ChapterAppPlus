package com.chaptervitamins.newcode.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.MaterialAdapter;
import com.chaptervitamins.newcode.adapters.ModulePagerAdapter;
import com.chaptervitamins.newcode.adapters.MultiModuleAdapter;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.newcode.utils.WrapLinearLayoutManager;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.splunk.mint.Mint;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ModuleActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //    @BindView(R.id.app_bar)
//    AppBarLayout appBarLayout;
    //    @BindView(R.id.toolbar_collapse_layout)
//    CollapsingToolbarLayout collapsingToolbarLayout;
    String moduleTitle;
    protected CourseUtility mCourseUtility;
    protected String modId = "0";
    protected boolean isCompletedClicked;
    protected ModulesUtility modulesUtility;
    private int modulePosition = 0;
    @BindView(R.id.ll_module)
    LinearLayout llModule;
    @BindView(R.id.ll_completed)
    LinearLayout llCompleted;
    @BindView(R.id.ll_all)
    LinearLayout llAll;
    @BindView(R.id.ll_sorting)
    LinearLayout llSorting;
    @BindView(R.id.ll_pending)
    LinearLayout llPending;
    @BindView(R.id.tv_comp_label)
    TextView tvCompLabel;
    @BindView(R.id.tv_pending_label)
    TextView tvPendingLabel;
    @BindView(R.id.tv_all_label)
    TextView tvAllLabel;
    @BindView(R.id.tv_comp_no)
    TextView tvCompNo;
    @BindView(R.id.tv_pending_no)
    TextView tvPendingNo;
    @BindView(R.id.tv_all_no)
    TextView tvAllNo;
    @BindView(R.id.f_list_row_ll)
    ViewPager viewPager;
    @BindView(R.id.tv_no_data_found)
    TextView tvNoDataFound;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.nested_sv)
    NestedScrollView nestedSV;
    protected ArrayList<ModulesUtility> modulesUtilityList = new ArrayList<>();
    protected ArrayList<MeterialUtility> meterialUtilityArrayList = new ArrayList<>();
    protected MaterialAdapter materialAdapter;
    protected MultiModuleAdapter multiModuleAdapter;
    protected ModulePagerAdapter modulePagerAdapter;
    GradientDrawable gradientDrawableEnabled;
    GradientDrawable gradientDrawableDisabled;
    private String currentStatus="all";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module);
        Mint.initAndStartSession(ModuleActivity.this, Constants.CRASH_REPORT_KEY);
        ButterKnife.bind(this);
        RecyclerView.LayoutManager mLayoutManager = new WrapLinearLayoutManager(ModuleActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        setToolBar();
        setDataInViews();
        setListeners();
        if (getIntent().hasExtra("course")) {
            mCourseUtility = (CourseUtility) getIntent().getSerializableExtra("course");
        }
        if (HomeActivity.courseUtilities.size() == 0)
            finish();
        if (getIntent().hasExtra("position") && HomeActivity.courseUtilities != null &&
                HomeActivity.courseUtilities.size() > getIntent().getIntExtra("position", 0))
            mCourseUtility = HomeActivity.courseUtilities.get(getIntent().getIntExtra("position", 0));
        if (getIntent().hasExtra("mod_id")) {
            modId = getIntent().getStringExtra("mod_id");
            modulesUtility = FlowingCourseUtils.getModuleFromCourse(mCourseUtility, modId);
            modulePosition = FlowingCourseUtils.getModulePosition(mCourseUtility, modId);
        } else {
            modulesUtility = mCourseUtility.getModulesUtilityArrayList().get(0);
        }

        if (modulesUtility!=null&&modulesUtility.getIs_flowing_course()) {
            llSorting.setVisibility(View.GONE);
        }
        boolean isLink = getIntent().getBooleanExtra("is_link", false);
        if (isLink)
            llModule.setVisibility(View.GONE);
        modulesUtilityList.addAll(mCourseUtility.getModulesUtilityArrayList());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvTitle.setText("MODULE");
        setPendingCompletedData();
        if (modulesUtilityList != null && modulesUtilityList.size() > 0) {
            setModuleViewpager();
            materialAdapter = new MaterialAdapter(meterialUtilityArrayList);
            recyclerView.setAdapter(materialAdapter);
            if (!modulesUtility.getIs_flowing_course())
                getSortedArraylist(modulesUtilityList.get(viewPager.getCurrentItem()).getMeterialUtilityArrayList(), currentStatus);
//            meterialUtilityArrayList.addAll(modulesUtilityList.get(viewPager.getCurrentItem()).getMeterialUtilityArrayList());

        }
        viewPager.setCurrentItem(modulePosition);
        nestedSV.smoothScrollTo(0, 0);
        meterialUtilityArrayList.clear();
        meterialUtilityArrayList.addAll(modulesUtilityList.get(modulePosition).getMeterialUtilityArrayList());
        materialAdapter.notifyDataSetChanged();
    }

    private void setPendingCompletedData() {
        if(modulesUtility.getMeterialUtilityArrayList()!=null)
        tvAllLabel.setText("All \n MATERIALS");

        if (modulesUtility.getMeterialUtilityArrayList().size() - modulesUtility.getCompletedMaterials() > 1)
            tvPendingLabel.setText(getString(R.string.pend_text) + "\n MATERIALS");
        else
            tvPendingLabel.setText(getString(R.string.pend_text) + "\n MATERIAL");
        if (modulesUtility.getCompletedMaterials() > 1)
            tvCompLabel.setText(getString(R.string.comp_text) + "\n MATERIALS");
        else
            tvCompLabel.setText(getString(R.string.comp_text) + "\nMATERIAL");
        if(modulesUtility!=null&&modulesUtility.getMeterialUtilityArrayList()!=null) {
            tvAllNo.setText(modulesUtility.getMeterialUtilityArrayList().size() + "");
            tvCompNo.setText(modulesUtility.getCompletedMaterials() + "");
            tvPendingNo.setText((modulesUtility.getMeterialUtilityArrayList().size() - modulesUtility.getCompletedMaterials()) + "");
        }

    }

    private void setDataInViews() {
        if (Utils.getColorPrimary() != 0) {
            gradientDrawableEnabled = createShapeByColor(Utils.getColorPrimary(),
                    50, 1, Utils.getColorPrimary());
            if (gradientDrawableEnabled != null)
                llAll.setBackground(gradientDrawableEnabled);
        } else {
            gradientDrawableEnabled = createShapeByColor(ContextCompat.getColor(this, R.color.colorPrimary),
                    50, 1, ContextCompat.getColor(this, R.color.colorPrimary));
            if (gradientDrawableEnabled != null)
                llAll.setBackground(gradientDrawableEnabled);
        }
        gradientDrawableDisabled = createShapeByColor(Color.WHITE,
                50, 1, Color.GRAY);
        if (gradientDrawableDisabled != null) {
            llCompleted.setBackground(gradientDrawableDisabled);
            llPending.setBackground(gradientDrawableDisabled);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);
        return true;
    }


    private void setListeners() {
        llCompleted.setOnClickListener(this);
        llPending.setOnClickListener(this);
        llAll.setOnClickListener(this);
    }

    /* private void setBgColor(CardView cardview, int pos) {
         if (pos == 0) {
             cardview.setCardBackgroundColor(getResources().getColor(R.color.color_yellow));
         } else if (pos == 1) {
             cardview.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
         } else if (pos == 2) {
             cardview.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
         }

     }*/
    private void setToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setModuleViewpager() {
        modulePagerAdapter = new ModulePagerAdapter(modulesUtilityList, ModuleActivity.this);
        viewPager.setAdapter(modulePagerAdapter);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset < -300) {
            viewPager.setVisibility(View.GONE);
//            title_txt.setVisibility(View.GONE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            collapsingToolbarLayout.setTitle(moduleTitle);

        } else {
            viewPager.setVisibility(View.VISIBLE);
            // title_txt.setVisibility(View.VISIBLE);
//            collapsingToolbarLayout.setTitle(" ");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                onBackPressed();
                break;
            case R.id.action_search:
                Intent intent = new Intent(ModuleActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (materialAdapter != null) {
            if (!modulesUtility.getIs_flowing_course())
                getSortedArraylist(modulesUtilityList.get(viewPager.getCurrentItem()).getMeterialUtilityArrayList(), currentStatus);

        }
        if (materialAdapter != null)
        materialAdapter.notifyDataSetChanged();

        if (modulePagerAdapter != null) {
            modulesUtilityList.clear();
            modulesUtilityList.addAll(mCourseUtility.getModulesUtilityArrayList());
            if(modulesUtility!=null&&modulesUtility.getMeterialUtilityArrayList()!=null) {
                tvAllNo.setText(modulesUtility.getMeterialUtilityArrayList().size() + "");
                tvCompNo.setText(modulesUtility.getCompletedMaterials() + "");
                tvPendingNo.setText((modulesUtility.getMeterialUtilityArrayList().size() - modulesUtility.getCompletedMaterials()) + "");
            }
            modulePagerAdapter.notifyDataSetChanged();
        }
        setIsOpenFalse();
    }

   /* @Override
    protected void onRestart() {
        super.onRestart();
        if (materialAdapter != null)
            materialAdapter.notifyDataSetChanged();

        if (modulePagerAdapter != null) {
            modulesUtilityList.clear();
            modulesUtilityList.addAll(mCourseUtility.getModulesUtilityArrayList());
            if(modulesUtility!=null&&modulesUtility.getMeterialUtilityArrayList()!=null) {
                tvAllNo.setText(modulesUtility.getMeterialUtilityArrayList().size() + "");
                tvCompNo.setText(modulesUtility.getCompletedMaterials() + "");
                tvPendingNo.setText((modulesUtility.getMeterialUtilityArrayList().size() - modulesUtility.getCompletedMaterials()) + "");
            }
            modulePagerAdapter.notifyDataSetChanged();
        }
        setIsOpenFalse();

    }*/

    protected void changeStyleOfButtons(String clickStatus) {
        if (!TextUtils.isEmpty(clickStatus)) {
            if (clickStatus.equalsIgnoreCase("complete")) {
             /*   llCompleted.setBackgroundResource(R.drawable.btn_comp_enable_bg);
                llPending.setBackgroundResource(R.drawable.btn_comp_disable_bg);*/
                if (gradientDrawableEnabled != null)
                    llCompleted.setBackground(gradientDrawableEnabled);
                if (gradientDrawableDisabled != null) {
                    llPending.setBackground(gradientDrawableDisabled);
                    llAll.setBackground(gradientDrawableDisabled);
                }

                tvCompLabel.setTextColor(ContextCompat.getColor(this, R.color.module_text_color));
                tvPendingLabel.setTextColor(ContextCompat.getColor(this, R.color.black));
                tvAllLabel.setTextColor(ContextCompat.getColor(this, R.color.black));

                tvCompNo.setBackgroundResource(R.drawable.comp_text_circle_bg);
                tvPendingNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                tvAllNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                getSortedArraylist(modulesUtilityList.get(viewPager.getCurrentItem()).getMeterialUtilityArrayList(), clickStatus);
            } else if (clickStatus.equalsIgnoreCase("pending")) {
            /*    llPending.setBackgroundResource(R.drawable.btn_comp_enable_bg);
                llCompleted.setBackgroundResource(R.drawable.btn_comp_disable_bg);*/

                if (gradientDrawableEnabled != null)
                    llPending.setBackground(gradientDrawableEnabled);
                if (gradientDrawableDisabled != null) {
                    llCompleted.setBackground(gradientDrawableDisabled);
                    llAll.setBackground(gradientDrawableDisabled);
                }

                tvPendingLabel.setTextColor(ContextCompat.getColor(this, R.color.module_text_color));
                tvCompLabel.setTextColor(ContextCompat.getColor(this, R.color.black));
                tvAllLabel.setTextColor(ContextCompat.getColor(this, R.color.black));

                tvPendingNo.setBackgroundResource(R.drawable.comp_text_circle_bg);
                tvCompNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                tvAllNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                getSortedArraylist(modulesUtilityList.get(viewPager.getCurrentItem()).getMeterialUtilityArrayList(), clickStatus);
            } else if (clickStatus.equalsIgnoreCase("all")) {
            /*    llPending.setBackgroundResource(R.drawable.btn_comp_enable_bg);
                llCompleted.setBackgroundResource(R.drawable.btn_comp_disable_bg);*/

                if (gradientDrawableEnabled != null)
                    llAll.setBackground(gradientDrawableEnabled);
                if (gradientDrawableDisabled != null) {
                    llCompleted.setBackground(gradientDrawableDisabled);
                    llPending.setBackground(gradientDrawableDisabled);
                }

                tvAllLabel.setTextColor(ContextCompat.getColor(this, R.color.module_text_color));
                tvCompLabel.setTextColor(ContextCompat.getColor(this, R.color.black));
                tvPendingLabel.setTextColor(ContextCompat.getColor(this, R.color.black));

                tvAllNo.setBackgroundResource(R.drawable.comp_text_circle_bg);
                tvPendingNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                tvCompNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                getSortedArraylist(modulesUtilityList.get(viewPager.getCurrentItem()).getMeterialUtilityArrayList(), clickStatus);
            }
        }

    }

    protected void getSortedArraylist(ArrayList<MeterialUtility> meterialUtilities, String clickStatus) {
        meterialUtilityArrayList.clear();
        if (clickStatus.equalsIgnoreCase("complete")) {
            isCompletedClicked = true;
            for (int i = 0; i < meterialUtilities.size(); i++) {
                MeterialUtility meterialUtility = meterialUtilities.get(i);
                if (meterialUtility.getIsComplete())
                    meterialUtilityArrayList.add(meterialUtility);
            }
        } else if (clickStatus.equalsIgnoreCase("pending")) {
            isCompletedClicked = false;
            for (int i = 0; i < meterialUtilities.size(); i++) {
                MeterialUtility meterialUtility = meterialUtilities.get(i);
                if (!meterialUtility.getIsComplete())
                    meterialUtilityArrayList.add(meterialUtility);
            }
        } else if (clickStatus.equalsIgnoreCase("all")) {
            isCompletedClicked = false;
            for (int i = 0; i < meterialUtilities.size(); i++) {
                MeterialUtility meterialUtility = meterialUtilities.get(i);
               /* if (!meterialUtility.getIsComplete())*/
                meterialUtilityArrayList.add(meterialUtility);
            }
        }
        if (meterialUtilityArrayList == null || meterialUtilityArrayList.size() == 0) {
            tvNoDataFound.setVisibility(View.VISIBLE);
            if (clickStatus.equalsIgnoreCase("complete"))
                tvNoDataFound.setText(getString(R.string.no_complete_material_message));
            else
                tvNoDataFound.setText(getString(R.string.no_pending_material_message));
            recyclerView.setVisibility(View.GONE);
        } else {
            tvNoDataFound.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        if (materialAdapter != null)
            materialAdapter.notifyDataSetChanged();
        if (modulePagerAdapter != null)
            modulePagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_completed:
                currentStatus = "complete";
                changeStyleOfButtons(currentStatus);
                break;
            case R.id.ll_pending:
                currentStatus = "pending";
                changeStyleOfButtons(currentStatus);
                break;
            case R.id.ll_all:
                currentStatus = "all";
                changeStyleOfButtons(currentStatus);
                break;
        }
    }

    protected void setIsOpenFalse() {
        for (int i = 0; i < meterialUtilityArrayList.size(); i++) {
            meterialUtilityArrayList.get(i).setOpen(false);
        }
    }

}
