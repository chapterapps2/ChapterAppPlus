package com.chaptervitamins.newcode.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.fragments.EventCalendarFragment;
import com.chaptervitamins.newcode.fragments.EventListFragment;
import com.chaptervitamins.newcode.models.EventModel;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.socialkms.fragments.BaseFragment;
import com.chaptervitamins.newcode.utils.APIUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class EventCalendarActivity extends BaseActivity {

    private WebServices mWebService;
    private TextView mTvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_calendar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Event Calendar");
        findViews();
        initData();

    }

    private void initData() {
        mWebService = new WebServices();
        getTrainingListApiCall();
    }

    private void getTrainingListApiCall() {
        if (WebServices.isNetworkAvailable(this)) {
            final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait..");
            ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("user_id", mWebService.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));

            new GenericApiCall(this, APIUtility.GETTRAINIGCALENDAR, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    if (dialog != null)
                        dialog.dismiss();
                    if (mWebService.isValid((String) result)) {
                        setEventData(mWebService.getTrainingEventList((String) result));
                    } else
                        noDataFound();
                }

                @Override
                public void onError(ErrorModel error) {
                    if (dialog != null)
                        dialog.dismiss();
                    noDataFound();
                }
            }).execute();
        } else {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

    private void setEventData(ArrayList<EventModel> eventList) {
        if (eventList != null && eventList.size() > 0) {
            LinkedHashMap<String, BaseFragment> fragmentHashMap = new LinkedHashMap<>();
            fragmentHashMap.put("List View", EventListFragment.newInstance(eventList));
            fragmentHashMap.put("Calendar", EventCalendarFragment.newInstance(eventList));
            setTabLayout(fragmentHashMap, true);
        } else {
            noDataFound();
        }

    }

    private void noDataFound() {
        mTvNoDataFound.setVisibility(View.VISIBLE);
        mTabLayout.setVisibility(View.GONE);
        mViewPager.setVisibility(View.GONE);
    }

    /**
     * This method is used to find all views
     */
    private void findViews() {
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mTvNoDataFound = (TextView) findViewById(R.id.tv_no_data_found);
    }
}
