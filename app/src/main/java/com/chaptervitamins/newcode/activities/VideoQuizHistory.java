package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.VideoQuizHistoryAdapter;
import com.chaptervitamins.newcode.models.VideoQuizAllResponseModel;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

public class VideoQuizHistory extends BaseActivity {
    private RecyclerView mRvQuizHistory;
    private ArrayList<VideoQuizAllResponseModel> alVideoQuizAllResponse;
    private MeterialUtility meterialUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_quiz_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViews();
        initData();
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_utility");
        alVideoQuizAllResponse = (ArrayList<VideoQuizAllResponseModel>) bundle.getSerializable("res_list");
        getSupportActionBar().setTitle(meterialUtility.getTitle());
        if(alVideoQuizAllResponse!=null && alVideoQuizAllResponse.size()>0){
            VideoQuizHistoryAdapter adapter = new VideoQuizHistoryAdapter(alVideoQuizAllResponse,meterialUtility);
            mRvQuizHistory.setAdapter(adapter);
        }
    }


    private void findViews() {
        mRvQuizHistory = (RecyclerView) findViewById(R.id.rv_history);
    }
}
