package com.chaptervitamins.newcode.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.models.EventModel;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.FlowingCourseUtils;

public class EventDetailActivity extends BaseActivity {

    private static final String KEY_EVENT = "KEY_EVENT";
    private TextView mTvTrainingName, mTvDescription, mTvTraining, mTvStartDate, mTvStartTime, mTvEndDate, mTvEndTime, mTvZone, mTvLocation,
            mTvVenue, mTvCity, mTvRegion, mTvTrainingType, mTvStatus;
    private EventModel mEventModel;
    private Button mBtnOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mEventModel = (EventModel) getIntent().getSerializableExtra(KEY_EVENT);
        findViews();
        setData();
    }

    private void setData() {
        if (mEventModel != null) {
            getSupportActionBar().setTitle(mEventModel.getName());
            mTvTrainingName.setText(setNull(mEventModel.getName()));
            mTvDescription.setText(mEventModel.getDescription());
            mTvTraining.setText(setNull(mEventModel.getTraining()));
            mTvStartDate.setText(setNull(mEventModel.getStartDate()));
            mTvStartTime.setText(setNull(mEventModel.getStartTime()));
            mTvEndDate.setText(setNull(mEventModel.getEndDate()));
            mTvEndTime.setText(setNull(mEventModel.getEndTime()));
            mTvZone.setText(setNull(mEventModel.getZone()));
            mTvLocation.setText(setNull(mEventModel.getLoaction()));
            mTvVenue.setText(setNull(mEventModel.getVenue()));
            mTvCity.setText(setNull(mEventModel.getCity()));
            mTvRegion.setText(setNull(mEventModel.getRegion()));
            mTvTrainingType.setText(setNull(mEventModel.getType()));
            mTvStatus.setText(setNull(mEventModel.getStatus()));
        }
    }

    private String setNull(String text) {
        if (!TextUtils.isEmpty(text))
            return text;
        return "N/A";
    }

    private void findViews() {
        mTvTrainingName = (TextView) findViewById(R.id.tv_programme_name);
        mTvDescription = (TextView) findViewById(R.id.tv_description);
        mTvTraining = (TextView) findViewById(R.id.tv_training);
        mTvStartDate = (TextView) findViewById(R.id.tv_start_date);
        mTvStartTime = (TextView) findViewById(R.id.tv_start_time);
        mTvEndDate = (TextView) findViewById(R.id.tv_end_date);
        mTvEndTime = (TextView) findViewById(R.id.tv_end_time);
        mTvZone = (TextView) findViewById(R.id.tv_zone);
        mTvLocation = (TextView) findViewById(R.id.tv_location);
        mTvVenue = (TextView) findViewById(R.id.tv_venue);
        mTvCity = (TextView) findViewById(R.id.tv_city);
        mTvRegion = (TextView) findViewById(R.id.tv_region);
        mTvTrainingType = (TextView) findViewById(R.id.tv_type);
        mTvStatus = (TextView) findViewById(R.id.tv_status);
        mBtnOpen = (Button) findViewById(R.id.btn_open);
        mBtnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CourseUtility courseUtility = FlowingCourseUtils.getCourseFromModuleId(mEventModel.getModuleId());
                if (courseUtility != null) {
                    Intent intent = new Intent(EventDetailActivity.this, ModuleActivity.class);
                    intent.putExtra("course", courseUtility);
                    intent.putExtra("mod_id", mEventModel.getModuleId());
                    intent.putExtra("is_link", false);
                    startActivity(intent);
                } else
                    Toast.makeText(EventDetailActivity.this, "This training is not assigned to you", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
