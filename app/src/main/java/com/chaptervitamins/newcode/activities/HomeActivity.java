package com.chaptervitamins.newcode.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.BuildConfig;
import com.chaptervitamins.CustomView.AutoLogout;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.chat.activities.ChatGroupActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.home.AppUpdate_Activity;
import com.chaptervitamins.mixpanalManager.AppConstants;
import com.chaptervitamins.newcode.adapters.CourseAdapter;
import com.chaptervitamins.newcode.adapters.NewsAdapter;
import com.chaptervitamins.newcode.adapters.NotificationAdapter;
import com.chaptervitamins.newcode.fragments.NavigationDrawerFragment;
import com.chaptervitamins.newcode.server.ApiCallUtils;
import com.chaptervitamins.newcode.server.FatchDataAsyncTask;
import com.chaptervitamins.newcode.server.GetNewsFeedsAsyncTask;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.newcode.utils.WrapGridLayoutManager;
import com.chaptervitamins.play_video.Link_Activity;
import com.chaptervitamins.socialkms.activity.SocialActivity;
import com.chaptervitamins.utility.Chat_MessagesUtils;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.chaptervitamins.utility.ReadResponseUtility;
import com.splunk.mint.Mint;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends BaseActivity implements View.OnClickListener,
        FatchDataAsyncTask.FatchDataListener, GetNewsFeedsAsyncTask.GetNewsFeedListener, SwipeRefreshLayout.OnRefreshListener {
    private TextView mTvNotificationCount, mTvProfileName, mTvDescription;
    private ImageView mIvSearch, mIvAlarm, mIvSync, mIvLeftArrow, mIvRightArrow, mIvProfile;
    private NestedScrollView nestedScrollView;
    private RelativeLayout mRlNotification, mRlNewsFeed, mLlNews;
    private ViewPager mVpNews;
    private TabLayout mTlNews;
    private LinearLayout llfab1, llfab2, llProfile;
    private Toolbar mToolbar;
    private DataBase mDatabase;
    private WebServices mWebServices;
    private Timer timer;
    private int page = 0;
    public ArrayList<MeterialUtility> newsFeedUtilities = new ArrayList<>();
    public static ArrayList<CourseUtility> courseUtilities = new ArrayList<>();
    public static ArrayList<CourseUtility> tempCourseUtilities = new ArrayList<>();
    public static ArrayList<ReadResponseUtility> mReadResponse = new ArrayList<ReadResponseUtility>();
    public static ArrayList<ReadResponseUtility> mSubmitResponse = new ArrayList<ReadResponseUtility>();
    public static ArrayList<ModulesUtility> modulesUtilityArrayList = new ArrayList<>();
    private ProgressDialog dialog;
    private TextView notifyTxtView;
    private NotificationAdapter notificationAdapter;
    private Handler handler;
    private LayoutInflater mInflater;
    private RecyclerView rvCourse;
    private CourseAdapter courseAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private NewsAdapter newsAdapter;
    private FloatingActionButton fab;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private Boolean isFabOpen = false;
    private DrawerLayout mDrawer;
    private NavigationDrawerFragment mDrawerFragment;
    public static ArrayList<Chat_MessagesUtils> chat_messagesUtilses = new ArrayList<>();
    private boolean doubleBackToExitPressedOnce;
    private ImageView logoBranchImgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("38") &&
                Constants.BRANCH_ID.equalsIgnoreCase("56")) {
            createHomeTemplate2();
        } else {
            setContentView(R.layout.activity_home);
        }
        Mint.initAndStartSession(HomeActivity.this, Constants.CRASH_REPORT_KEY);
        findViews();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initData();
        setListeners();
        setData();
    }

    private void createHomeTemplate2() {
        setContentView(R.layout.activity_home_temp2);
        ((TextView) findViewById(R.id.tv_health)).setText(WebServices.mLoginUtility.getCobrand_msg());
    }

    @Override
    protected void onResume() {
        super.onResume();
        APIUtility.showFlowingCourse = true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getProfileData();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        /*Snackbar snackbar = Snackbar.make(mDrawer, "Please click BACK again to exit", Snackbar.LENGTH_SHORT);
        snackbar.show();*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    private void getProfileData() {
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("organization_id", WebServices.mLoginUtility.getOrganization_id()));
                nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
                String resp = mWebServices.callServices(nameValuePair, APIUtility.GETMYPROFILE);
                Log.d(" Response:", resp.toString());
                if (mWebServices.isValid(resp))
                    mWebServices.isProfileData(HomeActivity.this, resp);
            }
        }.start();
    }


    /**
     * It is used to set all listeners
     */
    private void setListeners() {
//       findViewById(R.id.float_btn).setOnClickListener(this);
        findViewById(R.id.notification_rl).setOnClickListener(this);
        findViewById(R.id.search_img).setOnClickListener(this);
        findViewById(R.id.sync_img).setOnClickListener(this);
        findViewById(R.id.swipe_layout).setOnClickListener(this);
        fab.setOnClickListener(this);
        llfab1.setOnClickListener(this);
        llfab2.setOnClickListener(this);
    }

    /**
     * It is used to inititalize data for the screen
     */
    private void initData() {
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
        String smsUrl = getIntent().getStringExtra("sms_url");
        setSupportActionBar(mToolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawer, mToolbar, this);
        rvCourse.setNestedScrollingEnabled(false);
        mDatabase = DataBase.getInstance(this);
        mWebServices = new WebServices();
        mInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        pageSwitcher(2);
        setNewsData();
        setCourseData();
        if (!TextUtils.isEmpty(smsUrl))
            loadDeepLinking(smsUrl);
    }

    private void setCourseData() {
        courseUtilities.clear();
        HomeActivity.mSubmitResponse = mDatabase.getResponseData();
        ArrayList<ReadResponseUtility> readResponseUtilities = mWebServices.read_response_method(mDatabase.getReadResponseData(WebServices.mLoginUtility.getUser_id()));
        if (readResponseUtilities != null)
            HomeActivity.mReadResponse = readResponseUtilities;
        courseUtilities.addAll(mWebServices.getAssignCourseData(mDatabase.getCourseData(WebServices.mLoginUtility.getUser_id())));
        tempCourseUtilities.clear();
        tempCourseUtilities.addAll(courseUtilities);
        mWebServices.setProgress(mDatabase, "", true);
        WrapGridLayoutManager gridLayoutManager = null;
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("38") &&
                Constants.BRANCH_ID.equalsIgnoreCase("56")) {
            gridLayoutManager = new WrapGridLayoutManager(this, 3);
        } else if (Constants.ORGANIZATION_ID.equalsIgnoreCase("34") &&
                Constants.BRANCH_ID.equalsIgnoreCase("57")) {
            gridLayoutManager = new WrapGridLayoutManager(this, 1);
        } else {
            gridLayoutManager = new WrapGridLayoutManager(this, 2);
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (courseUtilities != null && courseUtilities.size() % 2 != 0 && position == courseUtilities.size() - 1) {
                        return 2;
                    }
                    return 1;
                }
            });
        }
        rvCourse.setLayoutManager(gridLayoutManager);
        courseAdapter = new CourseAdapter(courseUtilities);
        rvCourse.setAdapter(courseAdapter);
        checkSessionExpired();

        if (WebServices.isNetworkAvailable(HomeActivity.this)) {
            mSwipeRefreshLayout.setRefreshing(true);
            new FatchDataAsyncTask(HomeActivity.this, true, true).execute("");
        } else {
            getOfflineData();
        }
    }

    /**
     * It is used to check if session expired or not
     */
    private void checkSessionExpired() {
        String resp = mDatabase.getOrganizationData(WebServices.mLoginUtility.getUser_id(), WebServices.mLoginUtility.getOrganization_id());
        mWebServices.parseOrganization(resp);
        AutoLogout.autoLogout(this);
    }

    /**
     * It is used to load course from database if new is not available
     */
    private void getOfflineData() {
        mWebServices.getAllGroup(mDatabase.getallgroupsData(WebServices.mLoginUtility.getUser_id()));
        checkUpdate();
    }

    /**
     * It is used to set news data and adapter
     */
    private void setNewsData() {
        newsAdapter = new NewsAdapter(newsFeedUtilities, HomeActivity.this);
        mVpNews.setAdapter(newsAdapter);
//        mVpNews.setClipToPadding(false);
       /* mVpNews.setPageMargin(15);
        mVpNews.setPadding(48, 8, 48, 8);*/
        mVpNews.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (page == newsFeedUtilities.size()) { // In my case the number of pages are 5
                    page = 0;
                } else
                    page = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTlNews.setupWithViewPager(mVpNews);

        if (WebServices.isNetworkAvailable(this))
            new GetNewsFeedsAsyncTask(HomeActivity.this, true, mWebServices).execute("");
        else {
            findViewById(R.id.ll_news).setVisibility(View.GONE);
            llProfile.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                animateFAB();
                break;
           /* case R.id.hamburger:
                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawer.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawer.openDrawer(Gravity.RIGHT);
                }
                break;*/
            case R.id.ll_fab1:
                //  animateFAB();
                if (WebServices.isNetworkAvailable(HomeActivity.this)) {
                    startActivity(new Intent(HomeActivity.this, SocialActivity.class));

                } else {
                    Toast.makeText(HomeActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.ll_fab2:
                animateFAB();
                Intent intent = new Intent(HomeActivity.this, ChatGroupActivity.class);
//                intent.putExtra("name","Discussion and interaction");
                startActivity(intent);
                break;
            case R.id.left_arrow_img:
                mVpNews.setCurrentItem(mVpNews.getCurrentItem() - 1);
                break;
            case R.id.right_arrow_img:
                mVpNews.setCurrentItem(mVpNews.getCurrentItem() + 1);
                break;
            case R.id.sync_img:
                syncDataToServer();
                break;
            case R.id.search_img:
                mixPanelManager.sendEvent(HomeActivity.this, AppConstants.MIXPANAL_SEARCH_CLICK);
                intent = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.notification_rl:
                if (WebServices.isNetworkAvailable(HomeActivity.this)) {
                    mixPanelManager.sendEvent(HomeActivity.this, AppConstants.NOTIFICATION_CLICK_EVENT);
                    read_Notification();
                } else {
                    Toast.makeText(HomeActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void read_Notification() {
        final ProgressDialog dialog = ProgressDialog.show(HomeActivity.this, "", "Please wait...");
        dialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_bar));
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (dialog != null && !isFinishing()) dialog.dismiss();
                if (WebServices.DEVICE_ERRORMSG.equalsIgnoreCase("INVALID_SESSION")) {
                    Utils.callInvalidSession(HomeActivity.this, APIUtility.READ_NOTIFICATION + APIUtility.GETALL_NOTIFICATION);
                    return;
                }
//                if (WebServices.notificationUtilityArrayList.size() != 0) {

                int count = 0;
                for (int i = 0; i < WebServices.notificationUtilityArrayList.size(); i++) {
                    if (!WebServices.notificationUtilityArrayList.get(i).getSent_status().equalsIgnoreCase("READ"))
                        count++;
                }
                if (count == 0) mTvNotificationCount.setVisibility(View.GONE);
                else mTvNotificationCount.setVisibility(View.VISIBLE);
                mTvNotificationCount.setText(count + "");
                // showNorificationPopup();
                Utils.setHomeData(mDatabase, mWebServices, mixPanelManager);
                Intent intent = new Intent(HomeActivity.this, NotificationActivity.class);
                startActivity(intent);
               /* if (count == 0)
                    notifyTxtView.setText("Notifications");
                else
//                    notifyTxtView.setText("Notifications (Unread: " + count + ")");
                    notifyTxtView.setText("Notifications");*/
//
            }
        };
        new Thread() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                ArrayList<String> notificationData = DataBase.getInstance(HomeActivity.this).getReadNotificationData(WebServices.mLoginUtility.getUser_id());
                String not_user_id = "";
                String notifi_id = "";
                int size = notificationData.size();
                for (int i = 0; i < size; i++) {
                    notifi_id = notificationData.get(i).split(",")[0];
                    if (i < size - 1)
                        not_user_id = not_user_id + notificationData.get(i).split(",")[1] + ",";
                    else
                        not_user_id = not_user_id + notificationData.get(i).split(",")[1];
                }
                nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("notification_user_id", not_user_id));
                nameValuePair.add(new BasicNameValuePair("notification_id", notifi_id));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                nameValuePair.add(new BasicNameValuePair("sent_status", "READ"));
                String resp = new WebServices().getLogin(nameValuePair, APIUtility.READ_NOTIFICATION);
                for (int i = 0; i < size; i++) {
                    notifi_id = notificationData.get(i).split(",")[0];
                    if (new WebServices().isValid(resp))
                        DataBase.getInstance(HomeActivity.this).deleteReadNotificationData(WebServices.mLoginUtility.getUser_id(), notifi_id);

                }
                nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
                nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
                nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
                nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
                nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
                String resp1 = new WebServices().getLogin(nameValuePair, APIUtility.GETALL_NOTIFICATION);
                if (new WebServices().isValid(resp1)) {
                    if (DataBase.getInstance(HomeActivity.this).isData(WebServices.mLoginUtility.getUser_id(), "Notifications"))
                        DataBase.getInstance(HomeActivity.this).addNotificationData(WebServices.mLoginUtility.getUser_id(), resp1);
                    else {
                        DataBase.getInstance(HomeActivity.this).updateNotificationData(WebServices.mLoginUtility.getUser_id(), resp1);
                    }
                    System.out.println("====respresprespresp==" + resp1);

                    new WebServices().getAllNotification(resp1);
                }
                handler.sendEmptyMessage(0);
            }
        }.start();
    }


    /**
     * It is used to sync your data to server and get new data from server
     */
    public void syncDataToServer() {
        mixPanelManager.sendEvent(HomeActivity.this, AppConstants.SYNC);
        if (WebServices.isNetworkAvailable(HomeActivity.this)) {
            mSwipeRefreshLayout.setRefreshing(true);
            new GetNewsFeedsAsyncTask(HomeActivity.this, true, mWebServices).execute("");
            new FatchDataAsyncTask(HomeActivity.this, true, true).execute("");

        } else {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(HomeActivity.this, "Internet connection unavailable. App running in offline mode.", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * It is used to open the chat screen link
     */
   /* private void openChat() {
        if (WebServices.isNetworkAvailable(HomeActivity.this)) {
            Intent intent = new Intent(HomeActivity.this, OnlineLinkOpenActivity.class);
            intent.putExtra("loginscreen", "");
            intent.putExtra("url", APIUtility.CHAT_BOT_URL);
            intent.putExtra("name", "Chat");
            startActivity(intent);
        } else {
            Toast.makeText(HomeActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }*/
    @Override
    public void onSuccess(boolean isFirstTime) {
        try {
            if (isFirstTime) {
                nestedScrollView.fullScroll(ScrollView.FOCUS_UP);
                checkUpdate();
                if (WebServices.notificationUtilityArrayList != null && WebServices.notificationUtilityArrayList.size() > 0) {
                    mTvNotificationCount.setText(WebServices.notificationUtilityArrayList.size() + "");
                    mTvNotificationCount.setVisibility(View.VISIBLE);
                }
//            newsFeedUtilities.addAll(new ArrayList<>(FlowingCourseUtils.getRecentFiveAssignedMaterials()));
                courseAdapter.notifyDataSetChanged();
                if (courseUtilities != null && courseUtilities.size() == 0) {
                    DialogUtils.showDialog(HomeActivity.this, "Sorry! No course is assigned to you");
                }
                if (mSwipeRefreshLayout != null)
                    mSwipeRefreshLayout.setRefreshing(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSecondTimeSuccess() {
        HomeActivity.tempCourseUtilities.clear();
        HomeActivity.tempCourseUtilities.addAll(HomeActivity.courseUtilities);
    }

    /**
     * It is used to check update for the app
     */
    private void checkUpdate() {
        String appVersion = BuildConfig.VERSION_NAME;
        if (Constants.BRANCH_ID.equals("25")) {
            if (Constants.SHOW_UPDATE.equalsIgnoreCase("yes") && !TextUtils.isEmpty(APIUtility.organizationModel.getBranch_app_version()) && !appVersion.equalsIgnoreCase(APIUtility.organizationModel.getBranch_app_version())) {
                if (SplashActivity.mPref.getLong("update_msg_time", 0) == 0 || APIUtility.organizationModel.getIsForceUpdateOptionally().equalsIgnoreCase("no") || System.currentTimeMillis() - SplashActivity.mPref.getLong("update_msg_time", 0) > 8.64e+7) {
                    Intent intent = new Intent(HomeActivity.this, AppUpdate_Activity.class);
//                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        } else {
            if (Constants.SHOW_UPDATE.equalsIgnoreCase("yes") && !TextUtils.isEmpty(APIUtility.organizationModel.getAppVersion()) && !appVersion.equalsIgnoreCase(APIUtility.organizationModel.getAppVersion())) {
                if (SplashActivity.mPref.getLong("update_msg_time", 0) == 0 || APIUtility.organizationModel.getIsForceUpdateOptionally().equalsIgnoreCase("no") || System.currentTimeMillis() - SplashActivity.mPref.getLong("update_msg_time", 0) > 8.64e+7) {
                    Intent intent = new Intent(HomeActivity.this, AppUpdate_Activity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        }

    }

    /**
     * It is used to set data
     */
    private void setData() {
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(R.id.fab1).setBackgroundTintList(getResources().getColorStateList(Utils.getColorPrimary()));
        }*/
        if (Constants.ORGANIZATION_ID.equalsIgnoreCase("25") && Constants.BRANCH_ID.equalsIgnoreCase("32"))
            mToolbar.setBackgroundColor(this.getResources().getColor(R.color.white));
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getWelcome_msg())) {
            mTvProfileName.setText("Hi, " + WebServices.mLoginUtility.getFirstname() + "!" + "\n" + WebServices.mLoginUtility.getWelcome_msg());
        } else {
            mTvProfileName.setText("Hi, " + WebServices.mLoginUtility.getFirstname() + "!");
            mTvDescription.setText("Welcome to " + WebServices.mLoginUtility.getBranch_name());
        }


        if (SplashActivity.mPref == null)
            SplashActivity.mPref = getSharedPreferences("prefdata", MODE_PRIVATE);

        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getPhoto()) && !WebServices.mLoginUtility.getPhoto().contains("no-image")) {
            Picasso.with(this).load(WebServices.mLoginUtility.getPhoto()).placeholder(R.drawable.profile).error(R.drawable.profile).resize(90, 90).into(mIvProfile);
        } else
            setDefaultImage();
        if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_image())) {
            Picasso.with(this).load(WebServices.mLoginUtility.getBranch_image()).placeholder(R.drawable.splash_logo).error(R.drawable.splash_logo).into(logoBranchImgView);
        }

    }

    @Override
    public void onNewsFeedSuccess(ArrayList<MeterialUtility> newsFeedUtilities) {
        if (newsFeedUtilities != null && newsFeedUtilities.size() > 0) {
            llProfile.setVisibility(View.GONE);
            mLlNews.setVisibility(View.VISIBLE);
            this.newsFeedUtilities.clear();
            this.newsFeedUtilities.addAll(0, newsFeedUtilities);
            newsAdapter.notifyDataSetChanged();
        } else {
            mLlNews.setVisibility(View.GONE);
            llProfile.setVisibility(View.VISIBLE);
        }
        nestedScrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    @Override
    public void onNewsFeedFailed() {
        this.newsFeedUtilities.clear();
        newsAdapter.notifyDataSetChanged();
        nestedScrollView.fullScroll(ScrollView.FOCUS_UP);
        mLlNews.setVisibility(View.GONE);
        llProfile.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        syncDataToServer();
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {

                    if (page == newsFeedUtilities.size()) { // In my case the number of pages are 5
                        page = 0;
                        // Showing a toast for just testing purpose
                           /* Toast.makeText(getApplicationContext(), "Timer stoped",
                                    Toast.LENGTH_LONG).show();*/
                    } else {
                        mVpNews.setCurrentItem(page++);
                    }
                }
            });

        }

    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 2000); // delay
        // in
        // milliseconds
    }

    /**
     * It is used to find views
     */
    private void findViews() {
        mTvNotificationCount = (TextView) findViewById(R.id.count_txt);
        mTvProfileName = (TextView) findViewById(R.id.tv_profile_name);
        mTvDescription = (TextView) findViewById(R.id.tv_description);
        mIvAlarm = (ImageView) findViewById(R.id.alarm_img);
        mIvSync = (ImageView) findViewById(R.id.sync_img);
        logoBranchImgView = (ImageView) findViewById(R.id.logoBranchImgView);
        mIvLeftArrow = (ImageView) findViewById(R.id.left_arrow_img);
        mIvRightArrow = (ImageView) findViewById(R.id.right_arrow_img);
        mIvProfile = (ImageView) findViewById(R.id.iv_profile);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        nestedScrollView = (NestedScrollView) findViewById(R.id.scroll_view);
        rvCourse = (RecyclerView) findViewById(R.id.rv_course);
        mVpNews = (ViewPager) findViewById(R.id.viewPager);
        mTlNews = (TabLayout) findViewById(R.id.tabLayout);
        mLlNews = (RelativeLayout) findViewById(R.id.ll_news);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(
                Color.RED, Color.GREEN, Color.BLUE, Color.CYAN);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        llfab1 = (LinearLayout) findViewById(R.id.ll_fab1);
        llfab2 = (LinearLayout) findViewById(R.id.ll_fab2);
        llProfile = (LinearLayout) findViewById(R.id.ll_profile);
    }

    /**
     * It is used ot load ui
     *
     * @param URL
     */
    private void loadDeepLinking(final String URL) {
        if (URL.contains("birthday")) {
            if (WebServices.isNetworkAvailable(this)) {
                Intent intent = new Intent(this, Link_Activity.class);
                intent.putExtra("name", "Birthday Notification");
                intent.putExtra("url", URL);
                startActivity(intent);
            } else
                Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        } else {
            if (mDatabase.getCourseData(WebServices.mLoginUtility.getUser_id()).equalsIgnoreCase("")) {
                mSwipeRefreshLayout.setRefreshing(true);
                new FatchDataAsyncTask(HomeActivity.this, true).execute("course");
            } else {

                boolean isAvailableMaterial = false;
                String cid = "", mid = "", isMod = "", responseId = "", mid1 = "";
                try {
                    String str = URL.split("\\?")[1];
                    String str1[] = str.split("&");
                    cid = str1[0].split("=")[1];
                    mid1 = str1[1].split("=")[1];
                    isMod = str1[1].split("=")[0];
                    mid = mid1.split(" ")[0];
                    responseId = str1[2].split("=")[1];


                    if (WebServices.notificationUtilityArrayList.size() != 0) {
//                    mRlNotification.setVisibility(View.VISIBLE);
                        int count = 0;
                        for (int i = 0; i < WebServices.notificationUtilityArrayList.size(); i++) {
                            if (!WebServices.notificationUtilityArrayList.get(i).getSent_status().equalsIgnoreCase("READ"))
                                count++;
                        }
                        if (count == 0) mTvNotificationCount.setVisibility(View.GONE);
                        else mTvNotificationCount.setVisibility(View.VISIBLE);
                        mTvNotificationCount.setText(count + "");
                    }
                } catch (Exception e) {
                }
                if (cid.equalsIgnoreCase("user")) {
                    ApiCallUtils.quizEvaluateApiCall(HomeActivity.this, mWebServices, responseId, mid1);
                } else if (cid.equalsIgnoreCase("trainer")) {
                    if (WebServices.isNetworkAvailable(this)) {
                        Intent intent = new Intent(this, Link_Activity.class);
                        intent.putExtra("name", "Evaluation");
                        intent.putExtra("url", URL);
                        startActivity(intent);

                    } else
                        Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                } else {
                    MeterialUtility meterialUtility = new MeterialUtility();
                    int SELECTEDPOS = 0;
                    for (int i = 0; i < courseUtilities.size(); i++) {
                        if (courseUtilities.get(i).getCourse_id().equalsIgnoreCase(cid)) {

                            ArrayList<ModulesUtility> modulesUtilityArrayList = courseUtilities.get(i).getModulesUtilityArrayList();
                            for (int j = 0; j < modulesUtilityArrayList.size(); j++) {
                                if (isMod.equalsIgnoreCase("moid")) {
                                    if (modulesUtilityArrayList.get(j).getModule_id().equalsIgnoreCase(mid)) {
                                        SELECTEDPOS = i;
                                        isAvailableMaterial = true;
                                        System.out.println("===match data ===" + getIntent().getStringExtra("sms_url"));
                                    }
                                } else {
                                    ArrayList<MeterialUtility> meterialUtilities = modulesUtilityArrayList.get(j).getMeterialUtilityArrayList();
                                    for (int k = 0; k < meterialUtilities.size(); k++) {
                                        if (meterialUtilities.get(k).getMaterial_id().equalsIgnoreCase(mid)) {
                                            isAvailableMaterial = true;
                                            meterialUtility = meterialUtilities.get(k);
                                            break;
                                        }
                                    }
                                    if (isAvailableMaterial) break;
                                }
                            }
                        }
                        if (isAvailableMaterial) break;
                    }
                    if (isMod.equalsIgnoreCase("moid") && isAvailableMaterial) {
                        CourseUtility courseUtility = FlowingCourseUtils.getCourseFromModuleId(mid);
                        Intent intent = new Intent(HomeActivity.this, IntroductionActivity.class);
                        intent.putExtra("course", courseUtility);
                        intent.putExtra("mod_id", mid);
                        startActivity(intent);
                    } else if (isAvailableMaterial) {
                        new MaterialOpenController(HomeActivity.this, mixPanelManager, mDatabase).openMaterial(meterialUtility, true, false, false);
                    } else {
                        if (WebServices.isNetworkAvailable(this)) {
                            Intent intent = new Intent(this, Link_Activity.class);
                            intent.putExtra("name", "Assessment");
                            intent.putExtra("url", URL);
                            startActivity(intent);
                        } else
                            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }
    }

    private boolean isGuru() {
        boolean isGuru = false;
        for (int i = 0; i < courseUtilities.size(); i++) {
            if (courseUtilities.get(i).getCourse_name().toLowerCase().startsWith("guru")) {
                isGuru = true;
                break;
            }
        }
        return isGuru;
    }

    private String guruLink() {
        String isGuru = "";
        for (int i = 0; i < courseUtilities.size(); i++) {
            if (courseUtilities.get(i).getCourse_name().toLowerCase().startsWith("guru")) {
                isGuru = courseUtilities.get(i).getLink();
                break;
            }
        }
        return isGuru;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 9999:
                    setData();
                    if (mDrawerFragment.mAdapter != null)
                        mDrawerFragment.mAdapter.notifyDataSetChanged();
                    break;
                case 101:
                    courseAdapter.onActivityResult(requestCode, resultCode, data);
                    break;
            }
        }
    }

    public void animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            llfab1.startAnimation(fab_close);
            llfab2.startAnimation(fab_close);
            llfab1.setClickable(false);
            llfab2.setClickable(false);
            isFabOpen = false;

        } else {

            fab.startAnimation(rotate_forward);
            llfab1.startAnimation(fab_open);
            llfab2.startAnimation(fab_open);
            llfab1.setClickable(true);
            llfab2.setClickable(true);
            isFabOpen = true;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        courseAdapter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}
