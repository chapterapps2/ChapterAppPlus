package com.chaptervitamins.newcode.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.chaptervitamins.SplashActivity;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.newcode.utils.Constants;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.socialkms.adapters.ViewPagerAdapter;
import com.chaptervitamins.socialkms.fragments.BaseFragment;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.DialogUtils;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.splunk.mint.Mint;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

public abstract class BaseActivity extends AppCompatActivity {
    protected ViewPagerAdapter mAdapter;
    protected ViewPager mViewPager;
    protected TabLayout mTabLayout;
    protected MixPanelManager mixPanelManager;
    protected CountDownTimer timer;
    protected DataBase dataBase;
    protected Long sessionTimeCounter;
    protected WebServices mWebServices;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (APIUtility.organizationModel.getEnableScreenshot().equalsIgnoreCase("no"))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        mixPanelManager = APIUtility.getMixPanelManager(this);
        mWebServices = new WebServices();
        dataBase = DataBase.getInstance(this);

        Mint.initAndStartSession(this, Constants.CRASH_REPORT_KEY);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setStatusbarColor();
        updateSessionTime();
        sessionTimeCounter = Long.parseLong(APIUtility.organizationModel.getAutoLogoutTime());
        sessionTimeCounter = sessionTimeCounter - (sessionTimeCounter * 20 / 100);
        if (APIUtility.organizationModel.getSessionExtend().equalsIgnoreCase("YES")) {
            checkTimerForSession();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (APIUtility.organizationModel.getSessionExtend().equalsIgnoreCase("YES")) {
            checkTimerForSession();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null)
            timer.cancel();

        if (DialogUtils.dialog != null) {
            DialogUtils.dialog.dismiss();
            DialogUtils.dialog = null;
        }

    }

    public void setDefaultImage() {
        TextView tvUsername = (TextView) findViewById(R.id.tv_user_name);
        tvUsername.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(mWebServices.mLoginUtility.getFirstname()) && mWebServices.mLoginUtility.getFirstname().length() > 1)
            tvUsername.setText(WebServices.mLoginUtility.getFirstname().substring(0, 1));
        if (TextUtils.isEmpty(Constants.BRANCH_ID))
            tvUsername.setBackground(createShapeByColor(Utils.getColorPrimary(), 270, 0, R.color.colorPrimary));
        else
            tvUsername.setBackground(createShapeByColor(R.color.login_bg, 270, 0, R.color.login_bg));
    }

    private void checkTimerForSession() {
        timer = new CountDownTimer(sessionTimeCounter * 60 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (Constants.ORGANIZATION_ID.equals("2")) {
                    Utils.callInvalidSession(BaseActivity.this, "Session expired from app side in timer");
                } else {
                    /*------Dialog for session extended/expired option--------*/
                    DialogUtils.showDialog(BaseActivity.this, getString(R.string.session_str), new Runnable() {
                        @Override
                        public void run() {
                            updateSessionTime();
                        }
                    }, new Runnable() {
                        @Override
                        public void run() {
                            Utils.callInvalidSession(BaseActivity.this, "Session expired from app side in timer");
                        }
                    });
                }
            }
        }.start();
    }

    private void setStatusbarColor() {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                    if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1())) {

                        int newColor = getColorWithAlpha(WebServices.mLoginUtility.getBranch_color1());
                        //  window.setStatusBarColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color1()));
                        window.setStatusBarColor(newColor);
                    } else {
                        //  window.setStatusBarColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color1()));
                        int newColor = getColorWithAlpha(WebServices.mLoginUtility.getBranch_color1());
                        //  window.setStatusBarColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color1()));
                        window.setStatusBarColor(newColor);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getColorWithAlpha(String colorStr) {
        String strPrimaryColor = null;
        int intColorPrimary = 0;
        try {
            String[] strColor = colorStr.split("#");
            strPrimaryColor = "#ff" + strColor[1];
            intColorPrimary = Color.parseColor(strPrimaryColor);
        } catch (Exception e) {
            intColorPrimary = Color.parseColor(WebServices.mLoginUtility.getOrganization_color1());
            e.printStackTrace();
        }

       /* int newColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        newColor = Color.argb(alpha, r, g, b);*/


        return intColorPrimary;
    }

    public void setToolbarColor() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            try {
                if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                    if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1())) {
                        toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(WebServices.mLoginUtility.getBranch_color1())));
                    } else
                        toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(WebServices.mLoginUtility.getOrganization_color1())));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        setToolbarColor();
    }

   /* @Override
    public Resources getResources() {
        if (res == null) {
            res = new Res(super.getResources());
        }
        return res;
    }*/

    /**
     * To set tab layout
     */
    public void setTabLayout(LinkedHashMap<String, BaseFragment> fragmentsHashMap, boolean isFixed) {
        if (mViewPager == null || mTabLayout == null)
            return;
        /*if (isFixed || fragmentsHashMap.size() == 2)
            mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        else
            mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);*/

        setUpViewPager(mViewPager, fragmentsHashMap);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (mAdapter != null && mViewPager != null) {
                    mViewPager.setCurrentItem(tab.getPosition());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (mAdapter == null || mViewPager == null)
                    return;

                int tabPosition = tab.getPosition();
                Fragment fragment = ((ViewPagerAdapter) mViewPager.getAdapter()).getFragmentAtPosition(tabPosition);
                if (fragment != null) {
                    switch (tabPosition) {
                        case '1':

                            break;
                        case '2':
                            break;

                    }
                }

            }
        });

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

    }


    /**
     * To set Viewpager
     *
     * @param mViewPager
     * @param fragmentsHashMap
     */
    public void setUpViewPager(final ViewPager mViewPager, HashMap<String, BaseFragment> fragmentsHashMap) {
        try {
            mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            Iterator<String> fragmentsIterator = fragmentsHashMap.keySet().iterator();
            while (fragmentsIterator.hasNext()) {
                String key = fragmentsIterator.next();
                BaseFragment fragment = fragmentsHashMap.get(key);
                mAdapter.addFragment(fragment, key);
                mTabLayout.addTab(mTabLayout.newTab().setText(key));
            }

            /*Display display = getActivity().getWindowManager().getDefaultDisplay();
            if (display.getWidth() < 1100) {
                if (fragmentsHashMap.size() == 4)
                    mTabLayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                if (fragmentsHashMap.size() == 4)
                    mTabLayout.setTabMode(TabLayout.MODE_FIXED);
            }*/


            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                   /* if (mAdapter != null && mViewPager != null) {
                        Fragment fragment = mAdapter.getFragmentAtPosition(position);
                        if (fragment != null) {
                            fragment.onResume();
                        }

                    }*/
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

            mViewPager.setAdapter(mAdapter);
            mViewPager.setOffscreenPageLimit(fragmentsHashMap.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void updateSessionTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        if (SplashActivity.mPref == null)
            SplashActivity.mPref = getSharedPreferences("prefdata", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = SplashActivity.mPref.edit();
        editor.putString("startTime", dateFormat.format(date));
        editor.commit();
        if (WebServices.isNetworkAvailable(BaseActivity.this))
            sessionAPICall();

    }

    private void sessionAPICall() {
        ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
        final WebServices webServices = new WebServices();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("push_token", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
        nameValuePair.add(new BasicNameValuePair("organization_id", Constants.ORGANIZATION_ID));
        new GenericApiCall(this, APIUtility.GET_SESSION_TIME, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                String res = (String) result;
             /*if(!webServices.isValid(res)){
                 DialogUtils.showDialog(BaseActivity.this,"");
             }*/
            }

            @Override
            public void onError(ErrorModel error) {
                Utils.callInvalidSession(BaseActivity.this, APIUtility.GET_SESSION_TIME);
            }
        }).execute();
    }


    /**
     * It is used to create custom shapes by code for dynamically color changes
     *
     * @param fillColor
     * @param roundRadius
     * @param strokeWidth
     * @param strokeColor
     * @return
     */
    public GradientDrawable createShapeByColor(int fillColor, float roundRadius, int strokeWidth, int strokeColor) {
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(fillColor);
        gd.setCornerRadius(roundRadius);
        gd.setStroke(strokeWidth, strokeColor);
        return gd;
    }

    public void showRatingDialog(MeterialUtility meterialUtility, boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(this, meterialUtility.getMaterial_id(), wannaFinish, (RatingListener) this);
        custom.show(fm, "");
    }


    protected void setModuleFlowingCourse(Context context, int position, ArrayList<MeterialUtility> meterialUtilityArrayList, Button btnNextCourse, int modulePos,
                                          ArrayList<ModulesUtility> moduleUtilityList) {

        if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && moduleUtilityList.get(modulePos).getIs_flowing_course()) {
            findViewById(R.id.parentRelativeLayout).setVisibility(View.VISIBLE);
            int[] btnStatus = FlowingCourseUtils.showPrevNextModuleButtonStatus(this, position, meterialUtilityArrayList, modulePos, moduleUtilityList);
            // FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPreviousCourse);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNextCourse);
            // btnPreviousCourse.setOnClickListener(this);
            btnNextCourse.setOnClickListener((View.OnClickListener) context);
        }
    }


}
