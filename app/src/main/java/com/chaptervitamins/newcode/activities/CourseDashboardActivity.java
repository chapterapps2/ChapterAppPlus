package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.fragments.CourseDashboardFragment;
import com.chaptervitamins.socialkms.fragments.BaseFragment;

import java.util.LinkedHashMap;

public class CourseDashboardActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_dashboard);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Course Dashboard");
        findViews();
        LinkedHashMap<String, BaseFragment> fragmentHashMap = new LinkedHashMap<>();
        fragmentHashMap.put("Courses", CourseDashboardFragment.newInstance(1));
        fragmentHashMap.put("Modules", CourseDashboardFragment.newInstance(2));
        fragmentHashMap.put("Materials", CourseDashboardFragment.newInstance(3));
//        fragmentHashMap.put("Leaderboard", CourseDashboardFragment.newInstance(4));
        setTabLayout(fragmentHashMap, true);
    }

    /**
     * This method is used to find all views
     */
    private void findViews() {
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
    }


}
