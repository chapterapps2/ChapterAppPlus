package com.chaptervitamins.newcode.activities;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;

public class ContentInShortsActivity extends BaseActivity implements View.OnClickListener {
    private ImageView ivContent, ivBack, ivPlay, ivAudioPlay;
    private TextView tvContentTitle, tvDescription, tvContentTitle1, tvDescription1, tvTitle;
    private MeterialUtility meterialUtility;
    private String mediaType, filePath;
    private Button btnPrevious, btnNext;
    private VideoView mVideoView;
    private SeekBar mAudioSeekbar;
    private GifTextView goldGif;
    private MediaPlayer mediaPlayer;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1;
    private DataBase dataBase;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "";
    private RelativeLayout rlVideo;
    private LinearLayout llAudio;
    boolean isPlay = false;
    private final Handler handler = new Handler();
    private int mediaFileLengthInMilliseconds;
    //private JWPlayerView mPlayerView;
    private String endTime, startTime;
    private MixPanelManager mixPanelManager;

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_content_in_shorts);
        setLayout();
        findViews();
        mixPanelManager = APIUtility.getMixPanelManager(ContentInShortsActivity.this);
        setOnClickListener();
        setData();
    }

    private void setLayout() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            filePath = bundle.getString("file_path");
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_utility");
        }
    }

    private void findViews() {
        ivContent = (ImageView) findViewById(R.id.iv_content);
        ivBack = (ImageView) findViewById(R.id.back);
        ivPlay = (ImageView) findViewById(R.id.iv_play);
        goldGif = (GifTextView) findViewById(R.id.gif_gold_coins);
        tvContentTitle = (TextView) findViewById(R.id.tv_title);
        tvDescription = (TextView) findViewById(R.id.tv_description);
        tvContentTitle1 = (TextView) findViewById(R.id.tv_title1);
        tvDescription1 = (TextView) findViewById(R.id.tv_description1);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        rlVideo = (RelativeLayout) findViewById(R.id.rl_video);
        mVideoView = (VideoView) findViewById(R.id.video_view);
        llAudio = (LinearLayout) findViewById(R.id.ll_audio);
        ivAudioPlay = (ImageView) findViewById(R.id.iv_audio_play);
        mAudioSeekbar = (SeekBar) findViewById(R.id.seekbar_audio);
    }

    private void setOnClickListener() {
        ivBack.setOnClickListener(this);
        ivPlay.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        ivAudioPlay.setOnClickListener(this);
    }

    private void setData() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.reset();
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        dataBase = DataBase.getInstance(ContentInShortsActivity.this);
        if (meterialUtility != null) {
            ArrayList<String> materialSequence = meterialUtility.getMaterialSequence();
            if (materialSequence != null && materialSequence.size() == 3) {
                switch (materialSequence.get(0).toUpperCase()) {
                    case "TITLE":
                        if (materialSequence.get(1).equalsIgnoreCase("CONTENT")) {
                            tvContentTitle1.setVisibility(View.VISIBLE);
                            tvContentTitle1.setText(Html.fromHtml(meterialUtility.getTitle()));
                            tvDescription1.setVisibility(View.VISIBLE);
                            tvDescription1.setText(Html.fromHtml(meterialUtility.getDescription()));
                        } else if (materialSequence.get(1).equalsIgnoreCase("MEDIA")) {
                            tvContentTitle1.setVisibility(View.VISIBLE);
                            tvContentTitle1.setText(Html.fromHtml(meterialUtility.getTitle()));
                            tvDescription.setVisibility(View.VISIBLE);
                            tvDescription.setText(Html.fromHtml(meterialUtility.getDescription()));
                        }
                        break;
                    case "CONTENT":
                        if (materialSequence.get(1).equalsIgnoreCase("TITLE")) {
                            tvDescription1.setVisibility(View.VISIBLE);
                            tvDescription1.setText(Html.fromHtml(meterialUtility.getDescription()));
                            tvContentTitle1.setVisibility(View.VISIBLE);
                            tvContentTitle1.setText(Html.fromHtml(meterialUtility.getTitle()));
                        } else if (materialSequence.get(1).equalsIgnoreCase("MEDIA")) {
                            tvDescription1.setVisibility(View.VISIBLE);
                            tvDescription1.setText(Html.fromHtml(meterialUtility.getDescription()));
                            tvContentTitle.setVisibility(View.VISIBLE);
                            tvContentTitle.setText(Html.fromHtml(meterialUtility.getTitle()));
                        }
                        break;
                    case "MEDIA":
                        if (materialSequence.get(1).equalsIgnoreCase("TITLE")) {
                            tvContentTitle.setVisibility(View.VISIBLE);
                            tvContentTitle.setText(Html.fromHtml(meterialUtility.getTitle()));
                            tvDescription.setVisibility(View.VISIBLE);
                            tvDescription.setText(Html.fromHtml(meterialUtility.getDescription()));
                        } else if (materialSequence.get(1).equalsIgnoreCase("CONTENT")) {
                            tvTitle.setVisibility(View.VISIBLE);
                            tvTitle.setText(Html.fromHtml(meterialUtility.getDescription()));
                            tvTitle.setTextSize(18);
                            tvDescription.setVisibility(View.VISIBLE);
                            tvDescription.setText(Html.fromHtml(meterialUtility.getTitle()));
                            tvDescription.setTextSize(14);
                        }
                        break;
                }
            } else {
                tvContentTitle.setText(Html.fromHtml(meterialUtility.getTitle()));
                tvDescription.setText(Html.fromHtml(meterialUtility.getDescription()));
            }
            mediaType = meterialUtility.getMedia_type();
            switch (mediaType.toUpperCase()) {
                case "IMAGE":
                    ivContent.setVisibility(View.VISIBLE);
                    Picasso.with(ContentInShortsActivity.this)
                            .load(meterialUtility.getMaterial_media_file_url())
                            .error(R.mipmap.ic_launcher)
                            .placeholder(R.mipmap.ic_launcher)
                            .into(ivContent);
                    submitDataToServer();
                    break;
                case "VIDEO":
                    rlVideo.setVisibility(View.VISIBLE);
                    break;
                case "AUDIO":
                    llAudio.setVisibility(View.VISIBLE);
                    submitDataToServer();
                    startAudio();
                    break;
            }

            meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(meterialUtility.getCourse_id(), meterialUtility.getModule_id());
            position = FlowingCourseUtils.getPositionOfMeterial(meterialUtility.getMaterial_id(), meterialUtilityArrayList);
            if (position != -1 && meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
                int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(ContentInShortsActivity.this, position, meterialUtilityArrayList);
                FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
                FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
                btnPrevious.setOnClickListener(this);
                btnNext.setOnClickListener(this);
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
//                    coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getCourse_id(), meterialUtility.getMaterial_id());
                    coinsAllocatedModel = meterialUtility.getCoinsAllocatedModel();
                    /*if (coinsAllocatedModel == null) {
                        coinsAllocatedModel = FlowingCourseUtils.getCoinsAllocatedFromCourseList(meterialUtility.getMaterial_id());
                    }*/
                    if (coinsAllocatedModel != null) {
                        redeem = coinsAllocatedModel.getRedeem();
                    } else
                        coinsAllocatedModel = new CoinsAllocatedModel();

                    submitDataToServer();
                }
            }).start();

            mAudioSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (mediaPlayer.isPlaying()) {
                        int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * seekBar.getProgress();
                        mediaPlayer.seekTo(playPositionInMillisecconds);
                    }
                }
            });

        }

    }
/*

    private void startVideo() {
        if (!filePath.equalsIgnoreCase("")) {
            File f = new File(filePath);
            if (f.exists()) {

                PlayerConfig playerConfig = new PlayerConfig.Builder()
                        .file(filePath).autostart(true)
                        .build();
                mPlayerView = new JWPlayerView(ContentInShortsActivity.this, playerConfig);
                mPlayerView.setFullscreen(false, false);

                RelativeLayout container = (RelativeLayout) findViewById(R.id.rl_video);
                container.addView(mPlayerView, new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            }
        }
    }
*/

    private void playVideo() {
        Uri uri = Uri.parse(filePath); //Declare your url here.

        VideoView mVideoView = (VideoView) findViewById(R.id.video_view);
        MediaController mc = new MediaController(this);
        mc.setAnchorView(mVideoView);
        mc.setMediaPlayer(mVideoView);
        mVideoView.setMediaController(mc);
        mVideoView.setVideoURI(uri);
        mVideoView.requestFocus();
        mVideoView.start();
    }

    private void startAudio() {

        openAudioFile(filePath);

        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                mAudioSeekbar.setSecondaryProgress(percent);
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                ivAudioPlay.setImageResource(R.drawable.button_play);
                mAudioSeekbar.setSecondaryProgress(0);
                mAudioSeekbar.setProgress(0);
            }
        });
    }

    private void openAudioFile(String audiofile) {
        /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
        if (!audiofile.equalsIgnoreCase("")) {
            File f = new File(audiofile);
            if (f.exists()) {
                try {
                    mediaPlayer.setDataSource(audiofile); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                    mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL

            }
        }
    }

    private void submitDataToServer() {

        final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility, 100, redeem));
        if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0")) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showGoldGif();
                    Toast.makeText(ContentInShortsActivity.this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
                }
            });
        }
        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, "1", "1", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, meterialUtility, "100", "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(ContentInShortsActivity.this))
            new SubmitData(ContentInShortsActivity.this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, dataBase).execute();
        else {
           /* if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                coinsAllocatedModel.setRedeem("TRUE");
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    WebServices.updateTotalCoins(ContentInShortsActivity.this, coinsCollected);

            }*/
        }
    }

    private void showGoldGif() {
        goldGif.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (goldGif != null)
                    goldGif.setVisibility(View.GONE);
            }
        }, 2000);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_prev:
                endTime = DateFormat.getDateTimeInstance().format(new Date());
                mixPanelManager.selectTimeTrack(ContentInShortsActivity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                        , getIntent().getStringExtra("title"), "Video");
                FlowingCourseUtils.callFlowingCourseMaterial(ContentInShortsActivity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                endTime = DateFormat.getDateTimeInstance().format(new Date());
                mixPanelManager.selectTimeTrack(ContentInShortsActivity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                        , getIntent().getStringExtra("title"), "Video");
                FlowingCourseUtils.callFlowingCourseMaterial(ContentInShortsActivity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.iv_audio_play:
                playAudio();
                break;
            case R.id.iv_play:
                if (mediaType.equalsIgnoreCase("VIDEO")) {
                    ivPlay.setVisibility(View.GONE);
                    playVideo();
                } else if (mediaType.equalsIgnoreCase("AUDIO")) {
                   /* ivPlay.setVisibility(View.GONE);
                    startAudio();*/
                }
                break;

        }

    }


    private void playAudio() {
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        meterialUtility.setMaterialStartTime(df.format(c.getTime()));
        if (!isPlay) {
            isPlay = true;
            mediaPlayer.start();
            ivAudioPlay.setImageResource(R.drawable.button_pause);
            submitDataToServer();
        } else {
            isPlay = false;
            mediaPlayer.pause();
            ivAudioPlay.setImageResource(R.drawable.button_play);
        }
        primarySeekBarProgressUpdater();
    }


    private void primarySeekBarProgressUpdater() {
        mAudioSeekbar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            endTime = DateFormat.getDateTimeInstance().format(new Date());
            mixPanelManager.selectTimeTrack(ContentInShortsActivity.this, startTime, endTime, WebServices.mLoginUtility.getEmail()
                    , getIntent().getStringExtra("title"), "Content in shorts");
            if (mPlayerView != null) {
                if (mPlayerView.getFullscreen()) {
                } else {
//                    submitDataToServer();
                    super.onBackPressed();
                }

            } else {
//                submitDataToServer();
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /*@Override
    public void onPause() {

        if (mPlayerView != null) {
            // Allow background audio playback.
            try {
                mPlayerView.setBackgroundAudio(false);
                mPlayerView.onPause();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (mediaPlayer != null)
            mediaPlayer.stop();

        super.onPause();
    }*/

    @Override
    public void onStop() {
        super.onStop();
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        new AutoLogout().autoLogout(ContentInShortsActivity.this);
        try {
            if (mPlayerView != null) {
                mPlayerView.setBackgroundAudio(true);
                mPlayerView.onResume();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        if (mPlayerView != null)
            mPlayerView.onDestroy();
        super.onDestroy();
    }*/

}
