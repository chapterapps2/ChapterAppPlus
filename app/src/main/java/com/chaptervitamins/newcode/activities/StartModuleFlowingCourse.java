package com.chaptervitamins.newcode.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.utility.ModulesUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartModuleFlowingCourse extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.go_back)
    Button goButton;
    @BindView(R.id.moduleTxtView)
    Button moduleTxtView;
    @BindView(R.id.startInsTxtView)
    TextView startInsTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_module_flowing_course);
        ButterKnife.bind(this);
        setDataIntoViews();
        setClickListenerIntoViews();
    }

    private void setClickListenerIntoViews() {
        goButton.setOnClickListener(this);
        moduleTxtView.setOnClickListener(this);
    }

    private void setDataIntoViews() {
        if( getIntent().getSerializableExtra("next_module")!=null){
            ModulesUtility modulesUtility=(ModulesUtility)getIntent().getSerializableExtra("next_module");
            moduleTxtView.setText("Next Module: "+modulesUtility.getModule_name());
        }
        if( getIntent().getSerializableExtra("last_module")!=null){
            ModulesUtility modulesUtility=(ModulesUtility)getIntent().getSerializableExtra("last_module");
            startInsTxtView.setText("You have Completed the Module: \n"+modulesUtility.getModule_name());
        }



    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.go_back){
            finish();
        }else if(v.getId()==R.id.moduleTxtView){
            Intent intent = new Intent(this, ModuleActivity.class);
            intent.putExtra("position", getIntent().getIntExtra("position",0));
            intent.putExtra("mod_id", getIntent().getStringExtra("mod_id"));
            intent.putExtra("is_link", getIntent().getStringExtra("is_link"));
            startActivity(intent);
        }

    }
}
