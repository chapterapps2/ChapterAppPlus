package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.newcode.adapters.NotificationAdapter;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.Notification_Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_clear_all)
    AppCompatButton clearTextView;
    @BindView(R.id.toolbar_title)
    TextView toolBarTitle;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_no_data_found)
    TextView tvNoDataFound;
    NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        createShapeByColor(Utils.getColorPrimary(), 10f, 0, Utils.getColorPrimary());
        setDataIntoViews();
        back.setOnClickListener(this);
        clearTextView.setOnClickListener(this);
    }

    private void setDataIntoViews() {
        toolBarTitle.setText("Notifications");
        if (WebServices.notificationUtilityArrayList != null && WebServices.notificationUtilityArrayList.size() > 0) {
            notificationAdapter = new NotificationAdapter(this,
                    WebServices.notificationUtilityArrayList);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemViewCacheSize(WebServices.notificationUtilityArrayList.size());
            recyclerView.setAdapter(notificationAdapter);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back) {
            finish();
        } else if (v.getId() == R.id.tv_clear_all) {
            Utils.deleteAllNotification(NotificationActivity.this, notificationAdapter);
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (WebServices.notificationUtilityArrayList != null && WebServices.notificationUtilityArrayList.size() > 0) {
            ArrayList<Notification_Utility> tempList = new ArrayList<>();
            for (int i = 0; i < WebServices.notificationUtilityArrayList.size(); i++) {
                if (!TextUtils.isEmpty(WebServices.notificationUtilityArrayList.get(i).getSent_status()) && WebServices.notificationUtilityArrayList.get(i).getSent_status().equalsIgnoreCase("SENT")) {
                    tempList.add(WebServices.notificationUtilityArrayList.get(i));
                }
            }
            WebServices.notificationUtilityArrayList.clear();
            WebServices.notificationUtilityArrayList.addAll(tempList);
        }
        if (notificationAdapter != null)
            notificationAdapter.notifyDataSetChanged();
    }
}
