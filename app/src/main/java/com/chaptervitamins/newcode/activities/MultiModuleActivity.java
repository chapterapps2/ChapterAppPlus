package com.chaptervitamins.newcode.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.CoursePagerAdapter;
import com.chaptervitamins.newcode.adapters.MultiModuleAdapter;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.utility.CourseUtility;
import com.chaptervitamins.utility.ModulesUtility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MultiModuleActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    String moduleTitle;
    private int coursePos;
    protected CourseUtility mCourseUtility;
    protected int modulePosition = 0;
    protected boolean isCompletedClicked;
    protected ModulesUtility modulesUtility;
    @BindView(R.id.ll_module)
    LinearLayout llModule;
    @BindView(R.id.ll_completed)
    LinearLayout llCompleted;
    @BindView(R.id.ll_pending)
    LinearLayout llPending;
    @BindView(R.id.tv_comp_label)
    TextView tvCompLabel;
    @BindView(R.id.tv_pending_label)
    TextView tvPendingLabel;
    @BindView(R.id.tv_comp_no)
    TextView tvCompNo;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_pending_no)
    TextView tvPendingNo;
    @BindView(R.id.f_list_row_ll)
    ViewPager viewPager;
    protected ArrayList<ModulesUtility> modulesUtilityList = new ArrayList<>();
    protected MultiModuleAdapter multiModuleAdapter;
    protected CoursePagerAdapter coursePagerAdapter;
    @BindView(R.id.tv_no_data_found)
    TextView tvNoDataFound;
    @BindView(R.id.nested_sv)
    NestedScrollView nestedSV;
    GradientDrawable gradientDrawableEnabled;
    GradientDrawable gradientDrawableDisabled;

    @BindView(R.id.ll_all)
    LinearLayout llAll;
    @BindView(R.id.tv_all_no)
    TextView tvAllNo;
    @BindView(R.id.tv_all_label)
    TextView tvAllLabel;
    private String currentStatus = "all";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_module);
//        Mint.initAndStartSession(this, Constants.CRASH_REPORT_KEY);
        ButterKnife.bind(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        setTooBar();
        setDataInViews();
        setListeners();
        try {
            if (getIntent().hasExtra("course")) {
                coursePos = getIntent().getIntExtra("position", 0);
                if (HomeActivity.courseUtilities.size() == 0)
                    finish();
                mCourseUtility = HomeActivity.courseUtilities.get(coursePos);
                if (getIntent().hasExtra("mod_pos")) {
                    modulePosition = getIntent().getIntExtra("mod_pos", 0);
                }
                boolean isLink = getIntent().getBooleanExtra("is_link", false);
                if (isLink)
                    llModule.setVisibility(View.GONE);
            }
            modulesUtilityList.addAll(mCourseUtility.getModulesUtilityArrayList());
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            tvTitle.setText("COURSE");
            setPendingCompletedData();
            if (modulesUtilityList != null && modulesUtilityList.size() > 0) {
                setModuleViewpager();
//            changeStyleOfButtons(false);
                multiModuleAdapter = new MultiModuleAdapter(MultiModuleActivity.this, modulesUtilityList, coursePos);
                recyclerView.setAdapter(multiModuleAdapter);
                getSortedArraylist(currentStatus, mCourseUtility.getModulesUtilityArrayList());
            }
            viewPager.setCurrentItem(modulePosition);
            nestedSV.smoothScrollTo(0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchview, menu);
        return true;
    }

    private void setListeners() {
        llCompleted.setOnClickListener(this);
        llPending.setOnClickListener(this);
        llAll.setOnClickListener(this);
    }

    private void setBgColor(CardView cardview, int pos) {
        if (pos == 0) {
            cardview.setCardBackgroundColor(getResources().getColor(R.color.color_yellow));
        } else if (pos == 1) {
            cardview.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else if (pos == 2) {
            cardview.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }

    }

    private void setTooBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setModuleViewpager() {
        coursePagerAdapter = new CoursePagerAdapter(mCourseUtility, MultiModuleActivity.this);
        viewPager.setAdapter(coursePagerAdapter);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset < -300) {
            viewPager.setVisibility(View.GONE);
          /*  title_txt.setVisibility(View.GONE);*/
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            collapsingToolbarLayout.setTitle(moduleTitle);

        } else {
            viewPager.setVisibility(View.VISIBLE);
            // title_txt.setVisibility(View.VISIBLE);
//            collapsingToolbarLayout.setTitle(" ");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                onBackPressed();
                break;
            case R.id.action_search:
                Intent intent = new Intent(MultiModuleActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (multiModuleAdapter != null) {
            getSortedArraylist(currentStatus, mCourseUtility.getModulesUtilityArrayList());
            multiModuleAdapter.notifyDataSetChanged();
        }
        if (multiModuleAdapter != null)
            multiModuleAdapter.notifyDataSetChanged();

        if (coursePagerAdapter != null) {
            setPendingCompletedData();
            coursePagerAdapter.notifyDataSetChanged();
        }

    }

    private void setPendingCompletedData() {
        if (mCourseUtility.getModulesUtilityArrayList() != null)
            tvAllLabel.setText("All \nMODULES");

        if (mCourseUtility.getModulesUtilityArrayList().size() - mCourseUtility.getCompletedModules() > 1)
            tvPendingLabel.setText(getString(R.string.pend_text) + "\n MODULES");
        else
            tvPendingLabel.setText(getString(R.string.pend_text) + "\n MODULE");
        if (mCourseUtility.getCompletedModules() > 1)
            tvCompLabel.setText(getString(R.string.comp_text) + "\n MODULES");
        else
            tvCompLabel.setText(getString(R.string.comp_text) + "\n MODULE");
        if (mCourseUtility != null && mCourseUtility.getModulesUtilityArrayList() != null) {
            tvAllNo.setText(mCourseUtility.getModulesUtilityArrayList().size() + "");
            tvCompNo.setText(mCourseUtility.getCompletedModules() + "");
            tvPendingNo.setText((mCourseUtility.getModulesUtilityArrayList().size() - mCourseUtility.getCompletedModules()) + "");
        }
    }

    protected void changeStyleOfButtons(String clickStatus) {
        if (!TextUtils.isEmpty(clickStatus)) {
            if (clickStatus.equalsIgnoreCase("complete")) {
               /* llCompleted.setBackgroundResource(R.drawable.btn_comp_enable_bg);
                llPending.setBackgroundResource(R.drawable.btn_comp_disable_bg);*/
                if (gradientDrawableEnabled != null)
                    llCompleted.setBackground(gradientDrawableEnabled);
                if (gradientDrawableDisabled != null) {
                    llPending.setBackground(gradientDrawableDisabled);
                    llAll.setBackground(gradientDrawableDisabled);
                }

                tvCompLabel.setTextColor(ContextCompat.getColor(this, R.color.module_text_color));
                tvPendingLabel.setTextColor(ContextCompat.getColor(this, R.color.black));
                tvAllLabel.setTextColor(ContextCompat.getColor(this, R.color.black));

                tvCompNo.setBackgroundResource(R.drawable.comp_text_circle_bg);
                tvPendingNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                tvAllNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                getSortedArraylist(clickStatus, mCourseUtility.getModulesUtilityArrayList());
            } else if (clickStatus.equalsIgnoreCase("pending")) {
                /*llPending.setBackgroundResource(R.drawable.btn_comp_enable_bg);
                llCompleted.setBackgroundResource(R.drawable.btn_comp_disable_bg);*/
                if (gradientDrawableEnabled != null)
                    llPending.setBackground(gradientDrawableEnabled);
                if (gradientDrawableDisabled != null) {
                    llCompleted.setBackground(gradientDrawableDisabled);
                    llAll.setBackground(gradientDrawableDisabled);
                }

                tvPendingLabel.setTextColor(ContextCompat.getColor(this, R.color.module_text_color));
                tvCompLabel.setTextColor(ContextCompat.getColor(this, R.color.black));
                tvAllLabel.setTextColor(ContextCompat.getColor(this, R.color.black));

                tvPendingNo.setBackgroundResource(R.drawable.comp_text_circle_bg);
                tvCompNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                tvAllNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                getSortedArraylist(clickStatus, mCourseUtility.getModulesUtilityArrayList());
            } else if (clickStatus.equalsIgnoreCase("all")) {
                if (gradientDrawableEnabled != null)
                    llAll.setBackground(gradientDrawableEnabled);
                if (gradientDrawableDisabled != null) {
                    llCompleted.setBackground(gradientDrawableDisabled);
                    llPending.setBackground(gradientDrawableDisabled);
                }

                tvAllLabel.setTextColor(ContextCompat.getColor(this, R.color.module_text_color));
                tvCompLabel.setTextColor(ContextCompat.getColor(this, R.color.black));
                tvPendingLabel.setTextColor(ContextCompat.getColor(this, R.color.black));

                tvAllNo.setBackgroundResource(R.drawable.comp_text_circle_bg);
                tvPendingNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                tvCompNo.setBackgroundResource(R.drawable.comp_text_disable_bg);
                getSortedArraylist(clickStatus, mCourseUtility.getModulesUtilityArrayList());
            }

        }
    }

    protected void getSortedArraylist(String clickStatus, ArrayList<ModulesUtility> modulesUtilities) {
        modulesUtilityList.clear();
        if (clickStatus.equalsIgnoreCase("complete")) {
            isCompletedClicked = true;
            for (int i = 0; i < modulesUtilities.size(); i++) {
                ModulesUtility modulesUtility = modulesUtilities.get(i);
                if (modulesUtility.isCompleted())
                    modulesUtilityList.add(modulesUtility);
            }
        } else if (clickStatus.equalsIgnoreCase("pending")) {
            isCompletedClicked = false;
            for (int i = 0; i < modulesUtilities.size(); i++) {
                ModulesUtility modulesUtility = modulesUtilities.get(i);
                if (!modulesUtility.isCompleted())
                    modulesUtilityList.add(modulesUtility);
            }
        } else if (clickStatus.equalsIgnoreCase("all")) {
            isCompletedClicked = false;
            for (int i = 0; i < modulesUtilities.size(); i++) {
                ModulesUtility modulesUtility = modulesUtilities.get(i);
              /*  if (!modulesUtility.isCompleted())*/
                modulesUtilityList.add(modulesUtility);
            }
        }

        if (modulesUtilityList == null || modulesUtilityList.size() == 0) {
            tvNoDataFound.setVisibility(View.VISIBLE);
            if (clickStatus.equalsIgnoreCase("complete"))
                tvNoDataFound.setText(getString(R.string.no_complete_module_message));
            else
                tvNoDataFound.setText(getString(R.string.no_pending_module_message));
            recyclerView.setVisibility(View.GONE);
        } else {
            tvNoDataFound.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        if (multiModuleAdapter != null)
            multiModuleAdapter.notifyDataSetChanged();
    }

    private void setDataInViews() {
        if (Utils.getColorPrimary() != 0) {
            gradientDrawableEnabled = createShapeByColor(Utils.getColorPrimary(),
                    50, 1, Utils.getColorPrimary());
            if (gradientDrawableEnabled != null)
                llAll.setBackground(gradientDrawableEnabled);
        } else {
            gradientDrawableEnabled = createShapeByColor(ContextCompat.getColor(this, R.color.colorPrimary),
                    50, 1, ContextCompat.getColor(this, R.color.colorPrimary));
            if (gradientDrawableEnabled != null)
                llAll.setBackground(gradientDrawableEnabled);
        }
        gradientDrawableDisabled = createShapeByColor(Color.WHITE,
                50, 1, Color.GRAY);
        if (gradientDrawableDisabled != null) {
            llCompleted.setBackground(gradientDrawableDisabled);
            llPending.setBackground(gradientDrawableDisabled);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_completed:
                currentStatus = "complete";
                changeStyleOfButtons(currentStatus);
                break;
            case R.id.ll_pending:
                currentStatus = "pending";
                changeStyleOfButtons(currentStatus);
                break;
            case R.id.ll_all:
                currentStatus = "all";
                changeStyleOfButtons(currentStatus);
                break;
        }
    }

}
