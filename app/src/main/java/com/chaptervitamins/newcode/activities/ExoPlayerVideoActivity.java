package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.utility.MeterialUtility;

public class ExoPlayerVideoActivity extends BaseVideoActivity {
    Toolbar toolbar;
    MeterialUtility meterialUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player_video);
        setSimpleExoPlayerView();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("meterial utility")) {
            meterialUtility = (MeterialUtility) bundle.get("meterial utility");
            assert meterialUtility != null;
            filepath = "http://chaptervitaminsnew.s3.amazonaws.com/media/1530251323.mp4";
            Toast.makeText(this, "Video Url:" + filepath, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    int getPlayerId() {
        return R.id.exo_player_view;
    }

}
