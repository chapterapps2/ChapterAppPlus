package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.chaptervitamins.R;

import butterknife.BindView;

public class SubModuleActivity extends BaseActivity {

    @BindView(R.id.rv_sub_modules)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_module);
    }
}
