package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.adapters.FlashCardViewPagerAdapter;
import com.chaptervitamins.newcode.fragments.TemplateSixFragment;
import com.chaptervitamins.newcode.fragments.TemplateTwoFragment;
import com.chaptervitamins.utility.CustomViewPager;
import com.chaptervitamins.utility.FlashCardUtility;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

public class NewFlashCardActivity extends AppCompatActivity {
    CustomViewPager viewPager;

    FlashCardViewPagerAdapter adapter;
    ArrayList<Fragment> fragments;
    WebServices webServices;
    DataBase dataBase;
    private ArrayList<FlashCardUtility> list = new ArrayList<>();
    private MeterialUtility mMeterialUtility;
    String resp;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_flash_card);

        viewPager = (CustomViewPager) findViewById(R.id.new_viewpager);

        webServices = new WebServices();
        dataBase = DataBase.getInstance(NewFlashCardActivity.this);
        list = new ArrayList<>();
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        this.resp = dataBase.getFlashCardData(WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getMaterial_id());
        assert mMeterialUtility != null;
        list = webServices.getNewFlashcardData(resp);

        setViewPager(viewPager);
    }

    public void setViewPager(CustomViewPager viewPager) {
        this.viewPager = viewPager;
        fragments = new ArrayList<>();
        for (int i = 0; i < list.size(); i++)
            fragments.add(layoutFragment(mMeterialUtility.getMaterial_templete(), list.get(i), i));
        adapter = new FlashCardViewPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(adapter);
    }

    private Fragment layoutFragment(String fragment, FlashCardUtility flashCardUtility, int position) {
        Fragment fragmentLayout = null;
        switch (fragment) {
            case "TEMPLATE-1":
                fragmentLayout = TemplateSixFragment.newInstance(flashCardUtility, position);
                break;
            case "TEMPLATE-2":
                fragmentLayout = TemplateTwoFragment.newInstance(flashCardUtility, position);
                break;
        }
        return fragmentLayout;
    }
}
