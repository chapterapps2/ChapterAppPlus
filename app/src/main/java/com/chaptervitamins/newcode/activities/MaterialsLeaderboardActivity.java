package com.chaptervitamins.newcode.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.MaterialsLeaderboardAdapter;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

public class MaterialsLeaderboardActivity extends BaseActivity {
    private TextView mTvTitle;
    private RecyclerView mRvLeaderboard;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materials_leaderboard);
        findViews();
        initData();
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initData() {
        mTvTitle.setText("Leaderboards");
        meterialUtilityArrayList = FlowingCourseUtils.getAllAssessmentMaterials();
        MaterialsLeaderboardAdapter adapter = new MaterialsLeaderboardAdapter(meterialUtilityArrayList);
        mRvLeaderboard.setAdapter(adapter);
    }

    private void findViews() {
        mTvTitle = (TextView) findViewById(R.id.title);
        mRvLeaderboard = (RecyclerView)findViewById(R.id.rv_leaderboards);
        mRvLeaderboard.setLayoutManager(new LinearLayoutManager(this));
        findViewById(R.id.search_img).setVisibility(View.GONE);
    }
}
