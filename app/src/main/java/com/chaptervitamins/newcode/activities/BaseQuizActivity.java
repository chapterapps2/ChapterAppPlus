package com.chaptervitamins.newcode.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.newcode.utils.Utils;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.quiz.MainActivity;
import com.chaptervitamins.quiz.QuizUtils;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.MeterialUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class BaseQuizActivity extends BaseActivity {
    protected long TIMERVALUE = 0;
    private Handler handler2 = new Handler();
    protected TextView tvTimer;
    protected MeterialUtility meterialUtility;
    protected CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    protected String redeem = "", noOfCoins = "";
    protected String coinsCollected = "0";
    protected DataBase dataBase;
    public boolean TimerVal;
    public int timerColor = R.color.white;

    public void setTimer(boolean isTimerStart,int timerColor) {
        //set timer
        dataBase = DataBase.getInstance(this);
        this.timerColor = timerColor;
        MainActivity.TimerVal = false;
        TimerVal = true;
        startTime();
    }

    private void startTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (TimerVal) {
                    try {

                        Thread.sleep(1000);
                        handler2.post(new Runnable() {

                            @Override
                            public void run() {
                                if (TimerVal) {
                                    Log.d("time", TIMERVALUE + " Base");
                                    if (TIMERVALUE != 0) {
                                        tvTimer.setTextColor(ContextCompat.getColor(BaseQuizActivity.this, timerColor));
                                        tvTimer.setVisibility(View.VISIBLE);
                                        tvTimer.setText(Utils.convertSecondsToHMmSs(--TIMERVALUE));
                                    } else {
//                                    WebServices.questionUtility.setData_utils(list);
                                        TimerVal = false;
                                        int corr_que = 0, incorr_que = 0, skip_que = 0;
                                        Calendar c = Calendar.getInstance();
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                    QUIZEND_TIME = df.format(c.getTime());
                                        int size = WebServices.questionUtility.getData_utils().size();
                                        for (int i = 0; i < size; i++) {
                                            Data_util dataUtil = WebServices.questionUtility.getData_utils().get(i);
                                            if (dataUtil.getQuestion_type().equalsIgnoreCase("MULTIPLE")) {
                                                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                                                    ArrayList<String> correctAnswerAl = QuizUtils.convertStringToAl(dataUtil.getCorrect_option());
                                                    if (correctAnswerAl != null && correctAnswerAl.size() > 0) {
                                                        String correctOptionType = dataUtil.getCorrect_option_type();
                                                        switch (correctOptionType) {
                                                            case "ALL":
                                                                if (QuizUtils.checkAnsForAll(dataUtil.getUser_ans(), correctAnswerAl)) {
                                                                    dataUtil.setCorrect(true);
                                                                    corr_que++;
                                                                } else {
                                                                    WebServices.questionUtility.getData_utils().get(i).setCorrect(false);
                                                                    incorr_que++;
                                                                }
                                                                break;
                                                            case "ANY":
                                                                if (QuizUtils.checkAnsForAny(dataUtil.getUser_ans(), correctAnswerAl)) {
                                                                    dataUtil.setCorrect(true);
                                                                    corr_que++;
                                                                } else {
                                                                    WebServices.questionUtility.getData_utils().get(i).setCorrect(false);
                                                                    incorr_que++;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                } else {
                                                    skip_que++;
                                                }

                                            } else {
                                                if (dataUtil.getUser_ans() != null && dataUtil.getUser_ans().size() > 0 && !dataUtil.getUser_ans().get(0).equalsIgnoreCase("")) {
                                                    if (dataUtil.getUser_ans().get(0).equalsIgnoreCase(dataUtil.getCorrect_option())) {
                                                        dataUtil.setCorrect(true);
                                                        corr_que++;
                                                    } else {
                                                        WebServices.questionUtility.getData_utils().get(i).setCorrect(false);
                                                        incorr_que++;
                                                    }
                                                } else {
                                                    skip_que++;
                                                }
                                            }
                                        }
                                        if ((corr_que + incorr_que + skip_que) == WebServices.questionUtility.getData_utils().size()) {
                                            int percent = 0;

                                            percent = ((corr_que) * 100) / WebServices.questionUtility.getData_utils().size();
                                            long completed = ((Long.parseLong(meterialUtility.getAlloted_time()) * 60) - TIMERVALUE);
//
                                            TimerVal = false;
                                            savequizResponse(percent + "", corr_que + "", incorr_que + "", false);
                                            mixPanelManager.timeoutmaterial(BaseQuizActivity.this, WebServices.mLoginUtility.getEmail(), meterialUtility.getTitle(), "Quiz", percent + "");
                                            finish();

                                        } else {
                                            if (!isFinishing()) {
                                                new AlertDialog.Builder(BaseQuizActivity.this)
                                                        .setTitle("Error")
                                                        .setMessage("Some error occurred. Can not submit Quiz this time. Please try again.")
                                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {

                                                                finish();
                                                            }
                                                        })

                                                        .show();
                                            }
                                        }

                                    }
                                }

                            }
                        });
                    } catch (Exception e) {
                    }
                }
            }
        }).start();
    }

    private void savequizResponse(String persentage, String corr, String incorr, boolean isBack) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < WebServices.questionUtility.getData_utils().size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("question_bank_id", WebServices.questionUtility.getData_utils().get(i).getQuestion_bank_id());
                jsonObject.put("assign_question_id", WebServices.questionUtility.getData_utils().get(i).getAssign_question_id());
                jsonObject.put("answer_key",QuizUtils.getRealCorrectOption(WebServices.questionUtility.getData_utils().get(i)));
                jsonObject.put("correct_option", WebServices.questionUtility.getData_utils().get(i).getRealCorrectOption());
                jsonObject.put("answer", WebServices.questionUtility.getData_utils().get(i).getUser_input());
                jsonObject.put("question_description", WebServices.questionUtility.getData_utils().get(i).getQuestion_description());
                jsonObject.put("answer_type", WebServices.questionUtility.getData_utils().get(i).getOption_type());
                if (WebServices.questionUtility.getData_utils().get(i).isCorrect())
                    jsonObject.put("marks", WebServices.questionUtility.getData_utils().get(i).getMarks());
                else
                    jsonObject.put("marks", "0");
                jsonObject.put("time_taken", WebServices.questionUtility.getData_utils().get(i).getTime_taken());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        if (coinsAllocatedModel != null) {
            redeem = coinsAllocatedModel.getRedeem();
            noOfCoins = coinsAllocatedModel.getMaxCoins();
        } else
            coinsAllocatedModel = new CoinsAllocatedModel();
        String coinsCollected = "0";
//        Toast.makeText(this, "Your response is submitted", Toast.LENGTH_SHORT).show();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        meterialUtility.setMaterialEndTime(df.format(c.getTime()));
        new WebServices().setProgressOfMaterial(dataBase, meterialUtility, WebServices.questionUtility.getData_utils().size() + "", WebServices.questionUtility.getData_utils().size() + "", coinsCollected);
        new WebServices().addSubmitResponse(dataBase, meterialUtility, persentage, corr, incorr, jsonArray.toString(), coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());
        if (WebServices.isNetworkAvailable(this)) {
//            dataBase.deleteRowGetResponse(WebServices.questionUtility.getMaterial_id());
            if (!isBack) {
                coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(meterialUtility,Integer.valueOf(persentage), redeem));
                if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                    Toast.makeText(this, getString(R.string.you_have_been_earned) + coinsCollected + getString(R.string.coins), Toast.LENGTH_SHORT).show();
            }
            new SubmitData(this, meterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, dataBase).execute(corr, incorr, jsonArray.toString(), persentage);
        } else {
            if (!isBack) {
                /*coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(Integer.valueOf(persentage), redeem));
                if (coinsAllocatedModel != null && coinsAllocatedModel.getRedeem().equalsIgnoreCase("FALSE")) {
                    coinsAllocatedModel.setRedeem("TRUE");
                    if (!TextUtils.isEmpty(coinsCollected) && !coinsCollected.equals("0"))
                        WebServices.updateTotalCoins(MainActivity.this, coinsCollected);

                }*/
            }
//            new WebServices().addSubmitResponse(dataBase, meterialUtility.getCourse_id(), WebServices.questionUtility.getMaterial_id(), "QUIZ", persentage, corr, incorr, WebServices.questionUtility.getTest_pattern(), WebServices.questionUtility.getTitle(), jsonArray.toString(), coinsCollected, redeem, com.chap_tempone.home.MainActivity.MODULEID, com.chap_tempone.home.MainActivity.ASSIGNMATERIALID);


        }
    }
}
