package com.chaptervitamins.newcode.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.mixpanalManager.MixPanelManager;
import com.chaptervitamins.newcode.models.Capsule;
import com.chaptervitamins.newcode.models.ContentInShortsModel;
import com.chaptervitamins.newcode.server.MaterialOpenController;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.quiz.Data_util;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;
import com.chaptervitamins.utility.QuestionUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IntroductionActivity extends AppCompatActivity implements View.OnClickListener {

    String resp;
    @BindView(R.id.btn_intro)
    Button btnIntro;

    DataBase dataBase;
    ModulesUtility modulesUtility;
    Bundle bundle;
    WebServices webServices;
    ArrayList<Data_util> quizList;
    ArrayList<MeterialUtility> mList;
    ArrayList<ContentInShortsModel> contentInShorts = new ArrayList<>();
    ArrayList<Capsule> mCapsuleList = new ArrayList<>();
    MixPanelManager mixPanelManager;
    MaterialOpenController materialOpenController;
    QuestionUtility questionUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        ButterKnife.bind(this);

        dataBase = DataBase.getInstance(this);
        mixPanelManager = APIUtility.getMixPanelManager(this);
        webServices = new WebServices();

        materialOpenController = new MaterialOpenController(this, mixPanelManager, dataBase);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            modulesUtility = (ModulesUtility) bundle.get("module");
            assert modulesUtility != null;
            mList = modulesUtility.getMeterialUtilityArrayList();
        }

        for (int i = 0; i < mList.size(); i++)
            materialOpenController.openMaterial(mList.get(i), false, false,
                    false, downloadMaterialListener);

        btnIntro.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this,SwipeCardActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("meterial_list",mList);
        bundle.putParcelableArrayList("capsuleList", mCapsuleList);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    private MaterialOpenController.DownloadMaterialListener downloadMaterialListener = new MaterialOpenController.DownloadMaterialListener() {
        @Override
        public void onSuccess(MeterialUtility meterialUtility) {
//            Toast.makeText(IntroductionActivity.this, "Downloaded: " + meterialUtility.getMaterial_type(), Toast.LENGTH_LONG).show();
            if (meterialUtility.getMaterial_type().equalsIgnoreCase("QUIZ")) {
                downloadQuiz(mList.indexOf(meterialUtility),meterialUtility);
            }
            if (meterialUtility.getMaterial_type().equalsIgnoreCase("VIDEO")) {
                playVideo(mList.indexOf(meterialUtility),meterialUtility);
            }
            if (meterialUtility.getMaterial_type().equalsIgnoreCase("CONTENTINSHORT")) {
                downloadContent(mList.indexOf(meterialUtility),meterialUtility);
            }
        }
    };

    private void playVideo(int i, MeterialUtility meterialUtility) {
        if (mList.get(i).getMaterial_type().equalsIgnoreCase("VIDEO")) {
            /*resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(),
                    mList.get(i).getMaterial_id());

            if (!(resp.equals("") || resp == null)) {*/
            meterialUtility.setCapsuleStartIndex(mCapsuleList.size()+"");
            meterialUtility.setCapsuleEndIndex(mCapsuleList.size()+"");
            Capsule capsule = new Capsule();
            capsule.setMaterialMediaUrl(mList.get(i).getMaterial_media_file_url());
            capsule.setMaterial_id(mList.get(i).getMaterial_id());
            capsule.setMaterial_type(mList.get(i).getMaterial_type());
            mCapsuleList.add(capsule);
            /*}*/

        }
    }

    /*private MaterialOpenController.DownloadMaterialListener downloadMaterialListener1 = new MaterialOpenController.DownloadMaterialListener() {
        @Override
        public void onSuccess(MeterialUtility meterialUtility) {
            Toast.makeText(IntroductionActivity.this, "Downloaded: " + meterialUtility.getMaterial_type(), Toast.LENGTH_LONG).show();
            downloadContent();
        }
    };*/


    private void downloadQuiz(int i, MeterialUtility meterialUtility) {
        if (mList.get(i).getMaterial_type().equalsIgnoreCase("QUIZ")) {
            resp = dataBase.getQuizData(WebServices.mLoginUtility.getUser_id(),
                    mList.get(i).getMaterial_id());

            if (!(resp.equals("") || resp == null)) {
                questionUtility = webServices.parseQuestionData(resp);
                quizList = questionUtility.getData_utils();
                meterialUtility.setCapsuleStartIndex(mCapsuleList.size()+"");
                for (int j = 0; j < quizList.size(); j++) {
                    Capsule capsule = new Capsule();
                    capsule.setMaterial_id(mList.get(i).getMaterial_id());
                    capsule.setMaterial_type(mList.get(i).getMaterial_type());
                    capsule.setDescription(quizList.get(j).getQuestion_description());
                    ArrayList<String> answers = new ArrayList<>();
                    answers.add(quizList.get(j).getOption1());
                    answers.add(quizList.get(j).getOption2());
                    answers.add(quizList.get(j).getOption3());
                    capsule.setAssignQuestionId(quizList.get(j).getAssign_question_id());
                    capsule.setQuestionBankId(quizList.get(j).getQuestion_bank_id());
                    capsule.setAnswers(answers);
                    capsule.setUserAns(quizList.get(j).getCorrect_option());
                    mCapsuleList.add(capsule);
                }
                meterialUtility.setCapsuleEndIndex(mCapsuleList.size()+"");
            }
            /*materialOpenController.openMaterial(mList.get(2), false, false,
                    false, downloadMaterialListener1);*/
            //5 options in quiz
            //type of quiz
            //hint
            //explanation
        }
    }

    private void downloadContent(final int j, final MeterialUtility meterialUtility) {
        if (mList.get(j).getMaterial_type().equalsIgnoreCase("CONTENTINSHORT")) {

            final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait..");
            ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
            nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
            nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
            nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
            nameValuePair.add(new BasicNameValuePair("material_id", mList.get(j).getMaterial_id()));
            nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
            nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
            nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
            nameValuePair.add(new BasicNameValuePair("assign_material_id", mList.get(j).getAssign_material_id()));
            new GenericApiCall(this, APIUtility.CONTENTINSHORTS_DETAILS, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
                @Override
                public void onSuccess(Object result) {
                    if (dialog != null && !isFinishing())
                        dialog.dismiss();
                    String resp = (String) result;
                    if (!TextUtils.isEmpty(resp)) {
                        ArrayList<ContentInShortsModel> tempList = new WebServices().parseContentInShortsData(resp);
                        contentInShorts.addAll(tempList);
                        meterialUtility.setCapsuleStartIndex(mCapsuleList.size()+"");
                        for (int i = 0; i < contentInShorts.size(); i++) {
                            Capsule capsule = new Capsule();
                            capsule.setMaterial_id(mList.get(j).getMaterial_id());
                            capsule.setMaterial_type(mList.get(j).getMaterial_type());

                            capsule.setTitle(contentInShorts.get(i).getTitle());
                            capsule.setDescription(contentInShorts.get(i).getDescription());
                            capsule.setMedia(contentInShorts.get(i).getMedia());
                            mCapsuleList.add(capsule);
                        }
                        meterialUtility.setCapsuleEndIndex(mCapsuleList.size()+"");
                    } else {
                        Toast.makeText(IntroductionActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

                @Override
                public void onError(ErrorModel error) {
                    if (dialog != null && !isFinishing())
                        dialog.dismiss();
                    Toast.makeText(IntroductionActivity.this, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }).execute();
        }
    }
}
