package com.chaptervitamins.newcode.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.chaptervitamins.R;
import com.chaptervitamins.newcode.adapters.EvaluationAdapter;
import com.chaptervitamins.newcode.models.TrainerModel;
import com.chaptervitamins.newcode.models.VideoQuizAllResponseModel;
import com.chaptervitamins.newcode.models.VideoQuizResponse;
import com.chaptervitamins.utility.MeterialUtility;

import java.util.ArrayList;

public class EvaluationActivity extends BaseActivity implements View.OnClickListener {
    ImageView youTubeImg;
    VideoView videoView;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    private TextView tvNoDataFound, mTvCount, mTvQuestion, mTvComment, mTvTitle;
    private LinearLayout mLlMain, llRatingLayout;
    private ArrayList<VideoQuizResponse> videoQuizResponseArrayList;
    private ArrayList<VideoQuizAllResponseModel> videoQuizAllResponseModels;
    private Button mBtnPrevious, mBtnNext;
    private int mQuesPos = 0;
    private EvaluationAdapter evaluationAdapter;
    private ArrayList<TrainerModel> trainerModelArrayList;
    //private JWPlayerView mPlayerView;
    private RelativeLayout container;
    private boolean isFromHistory;
    private MeterialUtility meterialUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation_activtity);
        trainerModelArrayList = new ArrayList<>();
        findViews();
        initViews();
    }

    private void initViews() {
        mTvTitle.setText("Evaluation");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            videoQuizResponseArrayList = (ArrayList<VideoQuizResponse>) bundle.getSerializable("res");
            videoQuizAllResponseModels = (ArrayList<VideoQuizAllResponseModel>) bundle.getSerializable("res_list");
            meterialUtility = (MeterialUtility) bundle.getSerializable("meterial_utility");
            isFromHistory = bundle.getBoolean("is_from_history", false);
            if (videoQuizResponseArrayList != null && videoQuizResponseArrayList.size() > 0) {
                //show(mQuesPos);
            } else {
                mLlMain.setVisibility(View.GONE);
                tvNoDataFound.setVisibility(View.VISIBLE);
            }
        }
    }

    /*private void show(int mQuesPos) {
        try {
            if (videoQuizResponseArrayList != null && videoQuizResponseArrayList.size() > mQuesPos) {
                VideoQuizResponse questionObject = videoQuizResponseArrayList.get(mQuesPos);
                if (questionObject != null) {
                    setVisibilityOfFooter();
                    if (mPlayerView != null) {
                        mPlayerView.stop();
                        mPlayerView = null;
                        container.removeAllViews();
                        youTubeImg.setVisibility(View.VISIBLE);
                    }


                    if (!TextUtils.isEmpty(questionObject.getQuestion_description())) {
                        mTvQuestion.setVisibility(View.VISIBLE);
                        mTvQuestion.setText("Q" + (++mQuesPos) + ". " + questionObject.getQuestion_description());
                    } else
                        mTvQuestion.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(questionObject.getQuestion_comment())) {
                        mTvComment.setVisibility(View.VISIBLE);
                        mTvComment.setText("My Comment - " + questionObject.getQuestion_comment());
                    } else
                        mTvComment.setVisibility(View.GONE);
                    if (evaluationAdapter == null) {
                        trainerModelArrayList.clear();
                        if (questionObject.getTrainerModelArrayList() != null)
                            trainerModelArrayList.addAll(questionObject.getTrainerModelArrayList());
                        if(trainerModelArrayList!=null && trainerModelArrayList.size()>0) {
                            llRatingLayout.setVisibility(View.VISIBLE);
                            evaluationAdapter = new EvaluationAdapter(trainerModelArrayList);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            recyclerView.setAdapter(evaluationAdapter);
                        }else{
                            llRatingLayout.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        trainerModelArrayList.clear();
                        if (questionObject.getTrainerModelArrayList() != null) {
                            trainerModelArrayList.addAll(questionObject.getTrainerModelArrayList());
                            evaluationAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    mLlMain.setVisibility(View.GONE);
                    tvNoDataFound.setVisibility(View.VISIBLE);
                }
            } else
                onBackPressed();
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    private void setVisibilityOfFooter() {
        mTvCount.setText(mQuesPos + 1 + "/" + videoQuizResponseArrayList.size());
        if (mQuesPos == 0) {
            mBtnPrevious.setVisibility(View.GONE);
        } else if (mQuesPos == videoQuizResponseArrayList.size() - 1) {
            mBtnNext.setText("FINISH");
            mBtnPrevious.setVisibility(View.VISIBLE);
        } else {
            mBtnPrevious.setVisibility(View.VISIBLE);
            mBtnNext.setText("NEXT >");
        }
    }

    private void findViews() {
        mTvTitle = (TextView) findViewById(R.id.title);
        youTubeImg = (ImageView) findViewById(R.id.youtube_img);
        recyclerView = (RecyclerView) findViewById(R.id.rv_review);
        videoView = (VideoView) findViewById(R.id.VideoView);
        tvNoDataFound = (TextView) findViewById(R.id.tv_no_data_found);
        mLlMain = (LinearLayout) findViewById(R.id.main_layout);
        llRatingLayout = (LinearLayout) findViewById(R.id.ll_rating_layout);
        mBtnNext = (Button) findViewById(R.id.btn_next);
        container = (RelativeLayout) findViewById(R.id.rl);
        mBtnPrevious = (Button) findViewById(R.id.btn_previous);
        mBtnNext = (Button) findViewById(R.id.btn_next);
        mTvCount = (TextView) findViewById(R.id.tv_count);
        mTvQuestion = (TextView) findViewById(R.id.tv_question);
        mTvComment = (TextView) findViewById(R.id.tv_comment);
        findViewById(R.id.search_img).setVisibility(View.GONE);
        findViewById(R.id.back).setOnClickListener(this);
        youTubeImg.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
        mBtnPrevious.setOnClickListener(this);
    }

    /*public void playVideo(String videopath) {
        try {
            *//*progressDialog = ProgressDialog.show(EvaluationActivity.this, "",
                    "Buffering video...", false);
            progressDialog.setCancelable(true);
            getWindow().setFormat(PixelFormat.TRANSLUCENT);

            MediaController mediaController = new MediaController(EvaluationActivity.this);

            Uri video = Uri.parse(videopath);
            videoView.setMediaController(mediaController);
            videoView.setVideoURI(video);

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    progressDialog.dismiss();
                    videoView.start();
                    youTubeImg.setVisibility(View.GONE);
                }
            });
*//*
            if (WebServices.isNetworkAvailable(EvaluationActivity.this)) {
                PlayerConfig playerConfig = new PlayerConfig.Builder()
                        .file(videopath).autostart(true)
                        .build();
                youTubeImg.setVisibility(View.GONE);
                mPlayerView = new JWPlayerView(EvaluationActivity.this, playerConfig);
                mPlayerView.setFullscreen(false, false);
               *//* mPlayerView.addOnAdErrorListener(new OnAdErrorListener() {
                    @Override
                    public void onAdError(String s, String s1) {
                    }
                });*//*

                container.addView(mPlayerView, new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            } else
                Toast.makeText(EvaluationActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();


        } catch (Exception e) {
            progressDialog.dismiss();
            System.out.println("Video Play Error :" + e.getMessage());
        }

    }*/
/*

    @Override
    protected void onDestroy() {
        try {
            if (mPlayerView != null)
                mPlayerView.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (mPlayerView != null) {
            try {
                mPlayerView.setBackgroundAudio(false);
                mPlayerView.onPause();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mPlayerView != null) {
            try {
                mPlayerView.setBackgroundAudio(true);
                mPlayerView.onResume();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onResume();
    }
*/

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_video_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.history) {
            if (isFromHistory)
                finish();
            else {
                Bundle bundle = new Bundle();
                bundle.putSerializable("meterial_utility", meterialUtility);
                bundle.putSerializable("res_list", videoQuizAllResponseModels);
                Intent intent = new Intent(EvaluationActivity.this, VideoQuizHistory.class);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.btn_next:
                mQuesPos++;
                //show(mQuesPos);
                break;
            case R.id.btn_previous:
                --mQuesPos;
                //show(mQuesPos);
                break;
            case R.id.youtube_img:
                if (videoQuizResponseArrayList.get(mQuesPos) != null && videoQuizResponseArrayList.get(mQuesPos).getAnswer() != null &&
                        !TextUtils.isEmpty(videoQuizResponseArrayList.get(mQuesPos).getAnswer().getFile_url()));
                //  playVideo(videoQuizResponseArrayList.get(mQuesPos).getAnswer().getFile_url());
                else
                Toast.makeText(this, "No answer found", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
