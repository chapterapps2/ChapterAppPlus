package com.chaptervitamins.newcode.activities;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.chaptervitamins.Materials.SubmitData;
import com.chaptervitamins.R;
import com.chaptervitamins.WebServices.WebServices;
import com.chaptervitamins.database.DataBase;
import com.chaptervitamins.home.OnlineScromActivity;
import com.chaptervitamins.newcode.adapters.ContentsAdapter;
import com.chaptervitamins.newcode.interfaces.RatingListener;
import com.chaptervitamins.newcode.models.ContentInShortsModel;
import com.chaptervitamins.newcode.utils.APIUtility;
import com.chaptervitamins.nomination.networks.api.BaseApiCall;
import com.chaptervitamins.nomination.networks.api.ErrorModel;
import com.chaptervitamins.nomination.networks.apiCalls.GenericApiCall;
import com.chaptervitamins.utility.CoinsAllocatedModel;
import com.chaptervitamins.utility.CustomDialog;
import com.chaptervitamins.utility.FlowingCourseUtils;
import com.chaptervitamins.utility.MeterialUtility;
import com.chaptervitamins.utility.ModulesUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NewContentInShortsActivity extends BaseActivity implements RatingListener, View.OnClickListener, TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {
    private ViewPager mVpContents;
    private MeterialUtility mMeterialUtility;
    private ArrayList<ContentInShortsModel> contentInShortsModelArrayList;
    private Button btnPrevious, btnNext;
    private ArrayList<MeterialUtility> meterialUtilityArrayList;
    private int position = -1, currentPage = 1;
    private boolean isNextButtonClicked, isAudioPlay;
    private TextToSpeech tts;
    private Menu menu;
    private String endTime, startTime;
    private CoinsAllocatedModel coinsAllocatedModel = new CoinsAllocatedModel();
    private String redeem = "", noOfCoins = "";
    private TabLayout mTabLayout;

    private ArrayList<ModulesUtility> moduleUtilityList;
    private int modulePos=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_new_content_in_shorts);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViews();
        initData();
        setData();
    }

    private void initData() {
        startTime = DateFormat.getDateTimeInstance().format(new Date());
        tts = new TextToSpeech(this, this);
        contentInShortsModelArrayList = new ArrayList<>();
    }

    private void setData() {
        getDataFromBundle();
        if (mMeterialUtility != null) {
            ArrayList<ContentInShortsModel> tempList = new WebServices().parseContentInShortsData(DataBase.getInstance(this).getSurveyData(WebServices.mLoginUtility.getUser_id(), mMeterialUtility.getMaterial_id()));
            if (tempList == null || tempList.size() == 0) {
                downloadContent();
            } else {
                contentInShortsModelArrayList.addAll(tempList);
                setContent();
            }

        }
    }

    private void downloadContent() {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait..");
        ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("api_key", APIUtility.APIKEY));
        nameValuePair.add(new BasicNameValuePair("device_type", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("user_id", WebServices.mLoginUtility.getUser_id()));
        nameValuePair.add(new BasicNameValuePair("session_token", APIUtility.SESSION));
        nameValuePair.add(new BasicNameValuePair("material_id", mMeterialUtility.getMaterial_id()));
        nameValuePair.add(new BasicNameValuePair("push_tokenn", APIUtility.DEVICE));
        nameValuePair.add(new BasicNameValuePair("device_uuid", APIUtility.DEVICEID));
        nameValuePair.add(new BasicNameValuePair("branch_id", WebServices.mLoginUtility.getBranch_id()));
        nameValuePair.add(new BasicNameValuePair("assign_material_id", mMeterialUtility.getAssign_material_id()));
        new GenericApiCall(this, APIUtility.CONTENTINSHORTS_DETAILS, nameValuePair, new BaseApiCall.OnApiCallCompleteListener() {
            @Override
            public void onSuccess(Object result) {
                if (dialog != null && !isFinishing())
                    dialog.dismiss();
                String resp = (String) result;
                if (!TextUtils.isEmpty(resp)) {
                    ArrayList<ContentInShortsModel> tempList = new WebServices().parseContentInShortsData(resp);
                    contentInShortsModelArrayList.addAll(tempList);
                    setContent();
                } else {
                    Toast.makeText(NewContentInShortsActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onError(ErrorModel error) {
                if (dialog != null && !isFinishing())
                    dialog.dismiss();
                Toast.makeText(NewContentInShortsActivity.this, error.getErrorDescription(), Toast.LENGTH_SHORT).show();
                finish();
            }
        }).execute();
    }


    private void setContent() {
        if (contentInShortsModelArrayList.size() == 1) {
            setFlowingCourseData();
            getModuleData();
            setModuleFlowingCourse(this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
        }
        if (contentInShortsModelArrayList != null) {
            mVpContents.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    if (currentPage < position + 1)
                        currentPage = position + 1;
                    if (tts != null) {
                        tts.stop();
                    }
                    if (position == contentInShortsModelArrayList.size() - 1) {
                        setFlowingCourseData();
                        getModuleData();
                        setModuleFlowingCourse(NewContentInShortsActivity.this, position, meterialUtilityArrayList, (Button) findViewById(R.id.btn_next_course), modulePos, moduleUtilityList);
                    } else {
                        findViewById(R.id.rl_flowing_course).setVisibility(View.GONE);
                        findViewById(R.id.rl_module_flowing_course).setVisibility(View.GONE);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
            mTabLayout.setupWithViewPager(mVpContents);
        }
        if (contentInShortsModelArrayList != null && contentInShortsModelArrayList.size() > 0) {
            mVpContents.setAdapter(new ContentsAdapter(contentInShortsModelArrayList));
            mVpContents.setCurrentItem(0);
        }

    }

    private void getModuleData() {
        moduleUtilityList = FlowingCourseUtils.getModuleListFromModuleId(mMeterialUtility.getModule_id());
        if (moduleUtilityList != null && moduleUtilityList.size() > 0)
            modulePos = FlowingCourseUtils.getModulePositionFromModuleList(moduleUtilityList, mMeterialUtility.getModule_id());

    }

    private void getDataFromBundle() {
        mMeterialUtility = (MeterialUtility) getIntent().getSerializableExtra("meterial_utility");
        getSupportActionBar().setTitle(mMeterialUtility.getTitle());
    }

    private void findViews() {
        mVpContents = (ViewPager) findViewById(R.id.view_pager);
        btnPrevious = (Button) findViewById(R.id.btn_prev);
        btnNext = (Button) findViewById(R.id.btn_next);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
    }

    private void showRatingDialog(boolean wannaFinish) {
        FragmentManager fm = getSupportFragmentManager();
        CustomDialog custom = new CustomDialog();
        custom.setParamCustomDialog(this, mMeterialUtility.getMaterial_id(), wannaFinish, this);
        custom.show(fm, "");
    }

    private void setFlowingCourseData() {
        meterialUtilityArrayList = FlowingCourseUtils.getMaterialsFromModule(mMeterialUtility.getCourse_id(), mMeterialUtility.getModule_id());
        position = FlowingCourseUtils.getPositionOfMeterial(mMeterialUtility.getMaterial_id(), meterialUtilityArrayList);
        if (meterialUtilityArrayList != null && meterialUtilityArrayList.size() > 0 && position != -1 && meterialUtilityArrayList.get(position).getIs_flowing_course()) {
            int[] btnStatus = FlowingCourseUtils.showPrevNextButtonStatus(this, position, meterialUtilityArrayList);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[0], btnPrevious);
            FlowingCourseUtils.setVisibilityOfButton(btnStatus[1], btnNext);
            btnPrevious.setOnClickListener(this);
            btnNext.setOnClickListener(this);
        }
    }

    @Override
    public void onRatingBack() {
        FlowingCourseUtils.callFlowingCourseMaterial(NewContentInShortsActivity.this, meterialUtilityArrayList, position, isNextButtonClicked);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_content_shorts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_play) {
            if (!isAudioPlay) {
                isAudioPlay = true;
                item.setIcon(android.R.drawable.ic_media_pause);
                speakOut();
            } else {
                isAudioPlay = false;
                item.setIcon(android.R.drawable.ic_media_play);
                if (tts != null) {
                    tts.stop();
                }
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_prev:
                isNextButtonClicked = false;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) &&
                        mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(NewContentInShortsActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum()))
                    showRatingDialog(false);
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(NewContentInShortsActivity.this, meterialUtilityArrayList, position, false);
                break;
            case R.id.btn_next:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(NewContentInShortsActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum()))
                    showRatingDialog(false);
                else
                    FlowingCourseUtils.callFlowingCourseMaterial(NewContentInShortsActivity.this, meterialUtilityArrayList, position, true);
                break;
            case R.id.btn_next_course:
                isNextButtonClicked = true;
                if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) && mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(NewContentInShortsActivity.this) &&
                        mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum())) {
                    showRatingDialog(false);
                } else
                    FlowingCourseUtils.callFlowingCourseModule(NewContentInShortsActivity.this, mMeterialUtility, true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        try {
            submitDataToServer(true);
            if (!TextUtils.isEmpty(mMeterialUtility.getShow_rating()) &&
                    mMeterialUtility.getShow_rating().equalsIgnoreCase("Yes") && WebServices.isNetworkAvailable(NewContentInShortsActivity.this) && mMeterialUtility != null && TextUtils.isEmpty(mMeterialUtility.getRateNum()))
                showRatingDialog(true);
            else
                super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    private void speakOut() {
        if (contentInShortsModelArrayList != null && contentInShortsModelArrayList.size() > 0) {
            String text = contentInShortsModelArrayList.get(currentPage - 1).getDescription();

            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onUtteranceCompleted(String utteranceId) {
        if (tts != null) {
            tts.stop();
            menu.findItem(R.id.action_play).setIcon(android.R.drawable.ic_media_play);
        }
    }

    private void submitDataToServer(boolean wannaFinish) {
        endTime = DateFormat.getDateTimeInstance().format(new Date());
        DataBase dataBase = DataBase.getInstance(NewContentInShortsActivity.this);
        final String coinsCollected = String.valueOf(coinsAllocatedModel.getCoinsAccToPercentage(mMeterialUtility, 100, redeem));
        new WebServices().setProgressOfMaterial(dataBase, mMeterialUtility, currentPage + "", contentInShortsModelArrayList.size() + "", coinsCollected);
        if (currentPage != contentInShortsModelArrayList.size()) {
            mMeterialUtility.setSeen_count(currentPage + "");
            new WebServices().setSeenCountOfMaterialInStaticList(mMeterialUtility);
        }
        new WebServices().addSubmitResponse(dataBase, mMeterialUtility, "100", "", "", "", coinsCollected, redeem, WebServices.mLoginUtility.getOrganization_id(), WebServices.mLoginUtility.getBranch_id());

        if (WebServices.isNetworkAvailable(NewContentInShortsActivity.this))
            new SubmitData(NewContentInShortsActivity.this, mMeterialUtility, WebServices.mLoginUtility.getUser_id(), coinsAllocatedModel, DataBase.getInstance(NewContentInShortsActivity.this)).execute();

    }
}
