package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by test on 11-10-2017.
 */
public class CustomRelativeLayout extends RelativeLayout {
    public CustomRelativeLayout(Context context) {
        this(context,null);
    }

    public CustomRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

  /*  public CustomRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }*/

   /* @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }*/

    private void setBackground(){
        try {
            if(WebServices.mLoginUtility!=null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())){
                if(!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2())){
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color2()));
                }else
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
