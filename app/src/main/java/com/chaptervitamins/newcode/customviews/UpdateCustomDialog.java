package com.chaptervitamins.newcode.customviews;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chaptervitamins.R;

/**
 * Created by Vijay Antil on 20-03-2018.
 */

public class UpdateCustomDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rating_screen_layout, container, false);
        findViews(view);
        return view;

    }

    private void findViews(View view) {

    }
}
