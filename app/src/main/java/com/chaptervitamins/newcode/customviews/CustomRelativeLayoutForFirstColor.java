package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by test on 11-10-2017.
 */
public class CustomRelativeLayoutForFirstColor extends RelativeLayout {
    public CustomRelativeLayoutForFirstColor(Context context) {
        this(context,null);
    }

    public CustomRelativeLayoutForFirstColor(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

 /*   public CustomRelativeLayoutForFirstColor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomRelativeLayoutForFirstColor(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
*/
    private void setBackground(){
        try {
            if(WebServices.mLoginUtility!=null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())){
                if(!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1())){
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color1()));
                }else
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color1()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
