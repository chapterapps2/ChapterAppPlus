package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by Vijay Antil on 01-02-2018.
 */

public class CustomRecyclerView extends RecyclerView {
    public CustomRecyclerView(Context context) {
        super(context);
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void setBackground(){
        try {
            if(WebServices.mLoginUtility!=null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())){
                if(!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2())){
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color2()));
                }else
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
