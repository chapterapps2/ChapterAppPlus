package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by Vijay Antil on 01-02-2018.
 */

public class CustomCardView extends CardView {
    public CustomCardView(Context context) {
        super(context);
    }

    public CustomCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

    public CustomCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void setBackground(){
        try {
            if(WebServices.mLoginUtility!=null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())){
                if(!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2())){
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color2()));
                }else
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
