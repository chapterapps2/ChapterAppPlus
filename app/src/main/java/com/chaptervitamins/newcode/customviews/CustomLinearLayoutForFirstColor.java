package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by test on 11-10-2017.
 */
public class CustomLinearLayoutForFirstColor extends LinearLayout {

    public CustomLinearLayoutForFirstColor(Context context){
        super(context, null);
        setBackground();

    }

     private void setBackground(){
         try {
             if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color1())) {
                 if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color1()) &&
                         !WebServices.mLoginUtility.getBranch_color1().equalsIgnoreCase("null")) {
                     setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color1()));
                 } else
                     setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color1()));
             }
         }catch(Exception e){
             e.printStackTrace();
         }
    }

    public CustomLinearLayoutForFirstColor(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

  /*  public CustomLinearLayoutForFirstColor(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
        setBackground();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomLinearLayoutForFirstColor(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setBackground();

    }*/
}
