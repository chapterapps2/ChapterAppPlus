package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by Vijay Antil on 01-02-2018.
 */

public class CustomTabLayout extends TabLayout {
    public CustomTabLayout(Context context) {
        super(context);
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBackground();
    }

    private void setBackground(){
        try {
            if(WebServices.mLoginUtility!=null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())){
                if(!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2())){
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color2()));
                }else
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
