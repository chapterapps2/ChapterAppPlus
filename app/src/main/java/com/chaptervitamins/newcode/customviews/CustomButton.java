package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;

import com.chaptervitamins.newcode.utils.Utils;

/**
 * Created by test on 11-10-2017.
 */
public class CustomButton extends android.support.v7.widget.AppCompatButton {
    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /*private void setBackground(){
        try {
            if(WebServices.mLoginUtility!=null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())){
                if(!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2())){
                    setBackground(createShapeByColor(Utils.getColorPrimary(),5,0,Utils.getColorPrimary()));
//                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color2()));
                }else
//                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2()));
                    setBackground(createShapeByColor(Utils.getColorPrimary(),5,0,Utils.getColorPrimary()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void setBackground(){
        if(Utils.getColorPrimary()!=0){
            setBackground(createShapeByColor(Utils.getColorPrimary(),5,0,Utils.getColorPrimary()));
        }
    }

    /**
     * It is used to create custom shapes by code for dynamically color changes
     * @param fillColor
     * @param roundRadius
     * @param strokeWidth
     * @param strokeColor
     * @return
     */
    public GradientDrawable createShapeByColor(int fillColor, float roundRadius, int strokeWidth, int strokeColor){
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(fillColor);
        gd.setCornerRadius(roundRadius);
        gd.setStroke(strokeWidth, strokeColor);
        return gd;
    }
}
