package com.chaptervitamins.newcode.customviews;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.chaptervitamins.WebServices.WebServices;

/**
 * Created by test on 11-10-2017.
 */
public class CustomLinearLayout extends LinearLayout {

    public CustomLinearLayout(Context context) {
        super(context, null);
        setBackground();

    }

    private void setBackground() {
        try {
            if (WebServices.mLoginUtility != null && !TextUtils.isEmpty(WebServices.mLoginUtility.getOrganization_color2())) {
                if (!TextUtils.isEmpty(WebServices.mLoginUtility.getBranch_color2()) &&
                        !WebServices.mLoginUtility.getBranch_color2().equalsIgnoreCase("null")) {
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getBranch_color2()));
                } else
                    setBackgroundColor(Color.parseColor(WebServices.mLoginUtility.getOrganization_color2()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CustomLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setBackground();
    }

   /* public CustomLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr,0);
        setBackground();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setBackground();
    }*/
}
