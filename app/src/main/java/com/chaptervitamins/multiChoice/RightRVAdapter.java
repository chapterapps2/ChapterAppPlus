package com.chaptervitamins.multiChoice;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.quiz.Data_util;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Vijay Antil on 17-07-2017.
 */

public class RightRVAdapter extends RecyclerView.Adapter<RightRVAdapter.ViewHolder> implements ItemTouchHelperAdapter {
    private ArrayList<Data_util> answerAl, questionAl;
    private Context mContext;
    private boolean isChanged, shouldShuffle;

    public RightRVAdapter(ArrayList<Data_util> questionAl, ArrayList<Data_util> answerAl, boolean shouldShuffle) {
        this.answerAl = answerAl;
        this.questionAl = questionAl;
        this.shouldShuffle = shouldShuffle;
    }

    @Override
    public RightRVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null)
            mContext = parent.getContext();
        View v = LayoutInflater.from(mContext).inflate(R.layout.right_item_layout, parent, false);
        return new RightRVAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RightRVAdapter.ViewHolder holder, int position) {
        if (holder != null && position != -1) {
            if (position % 2 == 0)
                holder.rlMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color.gray2));
            else
                holder.rlMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            Data_util dataUtil = answerAl.get(position);
            if (isChanged || !shouldShuffle)
                if (dataUtil.getAssign_question_id().equals(questionAl.get(position).getAssign_question_id())) {
                    if (dataUtil.getUser_ans().size() > 0 && dataUtil.getUser_ans().get(0).equals(dataUtil.getCorrect_option()))
                        holder.ivTick.setVisibility(View.GONE);
                } else
                    holder.ivTick.setVisibility(View.GONE);

            if (!shouldShuffle) {
                if (dataUtil.getUser_ans().size() > 0)
                    setAnswerAccToCorrectOpt(holder, dataUtil, dataUtil.getUser_ans().get(0));
                else
                    setAnswerAccToCorrectOpt(holder, dataUtil);
            } else if (dataUtil != null && !TextUtils.isEmpty(dataUtil.getCorrect_option()))
                setAnswerAccToCorrectOpt(holder, dataUtil);

            holder.tvQuestion.setText(dataUtil.getQuestion_description());
        }
    }

    private void setAnswerAccToCorrectOpt(ViewHolder holder, Data_util dataUtil) {
        setAnswerAccToCorrectOpt(holder, dataUtil, "");
    }


    private void setAnswerAccToCorrectOpt(ViewHolder holder, Data_util dataUtil, String userAns) {
        String ans;
        if (!TextUtils.isEmpty(userAns))
            ans = userAns;
        else
            ans = dataUtil.getCorrect_option();
        switch (ans) {
            case "1":
                if (dataUtil.getIs_option1_image().equals("0"))
                    holder.tvAnswer.setText(dataUtil.getOption1());
                else {
                    byte[] decodedString = Base64.decode(dataUtil.getOption1(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.ivAnswer.setImageBitmap(decodedByte);
                }
                break;
            case "2":
                if (dataUtil.getIs_option2_image().equals("0"))
                    holder.tvAnswer.setText(dataUtil.getOption2());
                else {
                    byte[] decodedString = Base64.decode(dataUtil.getOption2(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.ivAnswer.setImageBitmap(decodedByte);
                }
                break;
            case "3":
                if (dataUtil.getIs_option3_image().equals("0"))
                    holder.tvAnswer.setText(dataUtil.getOption3());
                else {
                    byte[] decodedString = Base64.decode(dataUtil.getOption3(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.ivAnswer.setImageBitmap(decodedByte);
                }
                break;
            case "4":
                if (dataUtil.getIs_option4_image().equals("0"))
                    holder.tvAnswer.setText(dataUtil.getOption4());
                else {
                    byte[] decodedString = Base64.decode(dataUtil.getOption4(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.ivAnswer.setImageBitmap(decodedByte);
                }
                break;
        }
    }


    @Override
    public int getItemCount() {
        return answerAl.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
//        notifyItemMoved(fromPosition, toPosition);
        return true;
    }


    @Override
    public void onItemDismiss(int position) {

    }

    @Override
    public void onDrop(int fromPosition, int toPosition) {
        isChanged = true;
        try {
            Collections.swap(answerAl, fromPosition, toPosition);
            ArrayList<String> userAnsTo = new ArrayList<>();
            ArrayList<String> userAnsFrom = new ArrayList<>();

            userAnsTo.add(0, answerAl.get(toPosition).getCorrect_option());
            questionAl.get(toPosition).setUser_ans(userAnsTo);

            userAnsFrom.add(0, answerAl.get(fromPosition).getCorrect_option());
            questionAl.get(fromPosition).setUser_ans(userAnsFrom);

            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAnswer, tvQuestion;
        private ImageView ivTick, ivAnswer;
        private RelativeLayout rlMain;

        public ViewHolder(View itemView) {
            super(itemView);
            tvQuestion = (TextView) itemView.findViewById(R.id.tv_ques);
            tvAnswer = (TextView) itemView.findViewById(R.id.tv_answer);
            ivTick = (ImageView) itemView.findViewById(R.id.iv_tick);
            ivAnswer = (ImageView) itemView.findViewById(R.id.iv_ans_img);
            rlMain = (RelativeLayout) itemView.findViewById(R.id.rl_main);
        }
    }
}

