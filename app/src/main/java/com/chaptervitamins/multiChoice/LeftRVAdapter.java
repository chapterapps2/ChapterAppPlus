package com.chaptervitamins.multiChoice;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chaptervitamins.R;
import com.chaptervitamins.quiz.Data_util;

import java.util.ArrayList;

/**
 * Created by Sagar on 17-07-2017.
 */

public class LeftRVAdapter extends RecyclerView.Adapter<LeftRVAdapter.ViewHolder> {
    private ArrayList<Data_util> questionAl;
    private Context mContext;

    public LeftRVAdapter(ArrayList<Data_util> questionAl) {
        this.questionAl = questionAl;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        View v = LayoutInflater.from(mContext).inflate(R.layout.left_item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null) {
            if (position % 2 == 0)
                holder.rlMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color.gray2));
            else
                holder.rlMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            Data_util dataUtil = questionAl.get(position);
            if (dataUtil.getIs_question_image().equalsIgnoreCase("0")) {
                View view = holder.itemView;
                int height = view.getMeasuredHeight();
                dataUtil.setHeight(height);
                holder.ivQuestion.setVisibility(View.GONE);
                holder.tvQuestion.setVisibility(View.VISIBLE);
                holder.tvQuestion.setText(dataUtil.getQuestion_description());
            } else {
//            Picasso.with(mContext).load(dataUtil.get).into(holder.ivQuestion);
                holder.ivQuestion.setVisibility(View.VISIBLE);
                holder.tvQuestion.setVisibility(View.GONE);
                byte[] decodedString = Base64.decode(dataUtil.getQuestion_description(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.ivQuestion.setImageBitmap(decodedByte);
            }
        }
    }

    @Override
    public int getItemCount() {
        return questionAl.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivQuestion;
        private TextView tvQuestion;
        private RelativeLayout rlMain;

        public ViewHolder(View itemView) {
            super(itemView);
            ivQuestion = (ImageView) itemView.findViewById(R.id.iv_ques_img);
            tvQuestion = (TextView) itemView.findViewById(R.id.tv_ques);
            rlMain = (RelativeLayout) itemView.findViewById(R.id.rl_main);
        }
    }
}
