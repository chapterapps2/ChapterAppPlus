package com.lht.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ListIndexingControl
{
	private ArrayList<String> hashKeyList;
	
	private Hashtable<String, ArrayList<String>> hashTableObject;
	
	private HashMap<String, Element> dataMap;
	
	/**
	 * Function to list into hash table
	 * @param vendorType
	 * @param vendorDesc
	 * @param nodeList
	 */
	public ListIndexingControl(ArrayList<String> vendorType, ArrayList<String> vendorDesc, NodeList nodeList)
	{
		
		hashKeyList=new ArrayList<String>();
		ArrayList<String> hashElement=new ArrayList<String>();
		hashTableObject=new Hashtable<String, ArrayList<String>>();
		dataMap = new HashMap<String, Element>();
		if(!vendorType.isEmpty()){
		
			for(int index=0;index<vendorType.size();index++){
				String type=vendorType.get(index).toString();
				if(type.length() == 1)
					type=type.toUpperCase();
				String desc=vendorDesc.get(index).toString();
				dataMap.put(desc, (Element)nodeList.item(index));
				
				if(hashKeyList.contains(type)){
					hashElement = hashTableObject.get(type);
					hashElement.add(desc);
				}
				else
				{
					hashElement=new ArrayList<String>();
					hashElement.add(desc);
										
					hashKeyList.add(type);
					hashTableObject.put(type, hashElement);
				}
			}
		}
	
	}
	
	/**
	 * Return sorted hash key list
	 * @return
	 */
	public ArrayList<String> getHashKeyList()
	{
		Collections.sort(hashKeyList);
		return hashKeyList;
	}
	/**
	 * Returns hash table object
	 * @return
	 */
	public Hashtable<String, ArrayList<String>> getHashTableObject()
	{
		return hashTableObject;
	}
	/**
	 * Return hash map
	 * @return
	 */
	public HashMap<String, Element> getDataHashMap()
	{
		return dataMap;
	}

}