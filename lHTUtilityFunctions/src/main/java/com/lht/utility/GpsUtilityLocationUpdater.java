package com.lht.utility;

public interface GpsUtilityLocationUpdater {

	/**
	 * This method gets the updated location of the user
	 * @param latitude
	 * @param longitude
	 */
	public void getUpdatedLocation(double latitude, double longitude);

}
