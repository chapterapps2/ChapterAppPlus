package com.lht.utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;



public class GpsUtility
{
	
	private double latitude;
	private double longitude;
	private LocationManager networkProviderLocationManager;
	private LocationManager gpsProviderLocationManager;
	private LocationListener listener;
//	private static GpsUtility sharedInstance;
	private ProgressDialog dialog;


	/**
	 * Initialize network provider and gps provider and sets the latitude and longitude
	 * @param context
	 */
	public GpsUtility(final Context context) 
	{

		if (!Utility.isLocationFeatureExist(context)) {
			return;
		}

		if (dialog == null) {

			dialog = new ProgressDialog(context);
			dialog.setMessage("Loading...");
		}

		networkProviderLocationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		gpsProviderLocationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
//		gpsProviderLocationManager.getBestProvider(criteria, enabledOnly)
		listener = new LocationListener() {

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {

			}

			@Override
			public void onProviderEnabled(String provider) {
				Log.v("MapLocation", "Provider Enabled");
			}

			@Override
			public void onProviderDisabled(String provider) {
				Log.v("MapLocation", "Provider Disabled");
			}

			@Override
			public void onLocationChanged(Location location) {

				Log.v("Location Chaged", "provider: " + location.getProvider());
				setLatitude(location.getLatitude());
				setLongitude(location.getLongitude());
				if(latitude != 0.0 && longitude != 0.0)
				{
					if(dialog.isShowing())
					{
						dialog.dismiss();
					}

					double factor = 1e6;
					latitude = Math.round(latitude * factor) / factor;
					longitude = Math.round(longitude * factor) / factor;

					((GpsUtilityLocationUpdater)context).getUpdatedLocation(latitude, longitude);
					disableGps();
				}
			}
		};

	}
	/**
	 * Disable GPS services
	 */
	public void disableGps() {

		if (networkProviderLocationManager == null || gpsProviderLocationManager == null || listener == null) {
			return;
		}
		networkProviderLocationManager.removeUpdates(listener);
		gpsProviderLocationManager.removeUpdates(listener);

	}

	/** Set conditional requests for updation of location */
	public void setRequestForLocationUpdates() {


		if (!isProviderEnabled())
		{
			if(dialog != null && dialog.isShowing())
				dialog.dismiss();
			return;
		}

		if(dialog != null && !dialog.isShowing())

			dialog.show();

		if(networkProviderLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
			networkProviderLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
		
		if(gpsProviderLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
			gpsProviderLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
	}
	/**
	 * Set latitude for GPS
	 * @param latitude
	 */

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Returns the latitude
	 * @return
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Set longitude for GPS
	 * @param longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Returns the longitude
	 * @return
	 */
	public double getLongitude() {
		return longitude;
	}


	/**
	 * Checks for network provider is enabled or not
	 * @return
	 */
	public boolean isProviderEnabled() {

		if (networkProviderLocationManager == null || gpsProviderLocationManager == null || listener == null)
			return false;

		Log.v("", "Network provider: "+networkProviderLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
		Log.v("", "GPS provider: "+gpsProviderLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));

		if(gpsProviderLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || networkProviderLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
			return true;
		else
			return false;
	}
	
}
