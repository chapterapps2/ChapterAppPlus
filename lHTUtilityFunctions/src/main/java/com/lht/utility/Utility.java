package com.lht.utility;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utility {

	/**
	 * Here we find out the dpi of display.
	 * @param context
	 * @return
	 */
	public static DisplayMetrics getDisplayDpi(Context context)
	{
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		return metrics;
	}
	
	/**
	 * Here we display an under development alert.
	 * @param context
	 * 
	 */
	public static void showUnderDevelopmentAlert(Context context) 
	{
		showAlert(context, "Alert", "Under Development", "OK", 0);
	}
	
	
	
	/**
	 * Function to sort the array list
	 * @param listData Data Array
	 * @param key
	 * @param ascending
	 * @return
	 */
	public static ArrayList<HashMap<String,Object>> sortArrayListOfHashMapForKey(ArrayList<HashMap<String,Object>> listData,final String key, final boolean ascending) {

		Collections.sort(listData, new Comparator<HashMap<String, Object>>() {

			@Override
			public int compare(HashMap<String, Object> first, HashMap<String, Object> second) {
				

				try{
					float firstValue = Float.parseFloat(first.get(key).toString());
				    float secondValue = Float.parseFloat(second.get(key).toString());
				    return sortingLogic(firstValue, secondValue, ascending);

				}catch (NumberFormatException e) {
					// TODO: handle exception

					String firstValue = first.get(key).toString().toLowerCase();
				    String secondValue = second.get(key).toString().toLowerCase();
				    return sortingLogic(firstValue, secondValue, ascending);


				}

			}

			private int sortingLogic(String firstValue, String secondValue,boolean ascending) {
				
				if(firstValue.compareTo(secondValue)==0)
			    	return 0;

				else if(!ascending){
				   if(firstValue.compareTo(secondValue)> 0 )
				    	return -1;
				   else
					   return 1;
				}
				else
				{
					if(firstValue.compareTo(secondValue)>0)
						return 1;
				    else
				    	return -1;

				}
			}

			


			private int sortingLogic(float firstValue, float secondValue,
					boolean ascending) {
				
				if(firstValue == secondValue)
			    	return 0;

				else if(!ascending){
				   if(firstValue < secondValue)
				    	return 1;
				   else
					   return -1;
				}
				else
				{
					if(firstValue < secondValue)
						return -1;
				    else
				    	return 1;

				}
			}
		});

		return listData;

	}
	
	/**
	 * This method is called to check whether the camera feature is available in the Application
	 * @param context
	 * @return
	 */
	public static boolean isCameraFeatureExist(Context context)
	{
		return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}
	
	/**
	 * It checks if the directory is empty or not.
	 * @param path
	 * @return
	 */
	public static boolean isDirectoryEmpty(String path) {
		
		File file = new File(path);
	    if (file.isDirectory()) {
	      String[] files = file.list();

			return files.length <= 0;
		}
	    Utility.LHTLogs(""+path+": Its a file not directory ", true);
	    return false;
	}

	/**
	 * This method is called to get the screen width in pixels
	 * @param context
	 * @return width of screen
	 */
	public static float getScreenWidthInPx(Context context)
	{
		Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
		return display.getWidth();
	}
	
	/**
	 * This function returns a list of all files in a directory
	 * @param parentDir
	 * @param recursive
	 * @param extention: to filter a particular type of files. Specify * if you want to all all files
	 * @return
	 */
	public static  ArrayList<File> getAllFilesInDirectory(File parentDir, boolean recursive, String extention) {
	    ArrayList<File> inFiles = new ArrayList<File>();
	    File[] files = parentDir.listFiles();
	    if(files == null)
	    {
	    		LHTLogs("The invalid directory path: "+ parentDir, true);
	    		return null;
	    }
	    for (File file : files) {
	        if (file.isDirectory() && recursive) 
	            inFiles.addAll(getAllFilesInDirectory(file, recursive, extention));
	         else
	        {
	        		if (extention.equals("*")) 
	        			inFiles.add(file);
	        		else if(file.getName().endsWith(extention)){
	        			inFiles.add(file);
	            }
	        }
	    }
	    Collections.sort(inFiles);
	    return inFiles;
	}
	
	/**
	 * This method is called to get screen height
	 * @param context
	 * @return height of screen in pixels
	 */
	public static float getScreenHeightInPx(Context context)
	{
		Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
		return display.getHeight();
	}
	
	/**
	 * Function to get Device unique ID
	 * @param context
	 * @return
	 */
	public static String getDeviceID(Context context)
	{
		String deviceID = "";

		StringBuilder uniqueDeviceID = new StringBuilder();

		if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) 
		{
			deviceID = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
			uniqueDeviceID.append(deviceID);
		}

		/*
		 * Settings.Secure.ANDROID_ID returns the unique DeviceID Works for
		 * Android 2.2 and above
		 */
		String androidId = Settings.Secure.getString(context.getContentResolver(),
				Settings.Secure.ANDROID_ID);
		
		uniqueDeviceID.append(androidId);
		
		return uniqueDeviceID.toString();
	}

	/**
	 * Get destination file path according to application storage
	 * @param context
	 * @return
	 */
	public static String getDestinationFilePath(Context context)
    {
    	ContextWrapper contextWrapper = new ContextWrapper(context);
    	String filePath = contextWrapper.getFilesDir().toString();
    	
    	if((context.getApplicationInfo().flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) == ApplicationInfo.FLAG_EXTERNAL_STORAGE)
    	{
    		filePath = contextWrapper.getExternalFilesDir(null).toString();
    		Log.v("", "External File Dir: "+filePath);
    	}
    	
    	
    	return filePath + "/";
    }
	
//	/**
//	 * Get Database Path according to application storage
//	 * @param context
//	 * @return
//	 */
//	public static String getDatabasePath(Context context)
//    {
//    	ContextWrapper contextWrapper = new ContextWrapper(context);
//    	String filePath = contextWrapper.getD
//    	
//    	if((context.getApplicationInfo().flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) == ApplicationInfo.FLAG_EXTERNAL_STORAGE)
//    	{
//    		filePath = contextWrapper.getExternalCacheDir().toString();
//    		Log.v("", "External Cache Dir: "+filePath);
//    	}
//    	
//    	return filePath + "/";
//    }
	
	/**
	 * This method is used to check whether the application is saved on SD card
	 * @param context
	 * @return boolean 
	 */
	public static boolean isAppInstalledOnSDCard(Context context)
	{
		boolean appInstalledOnSDCard = false;
    	
    		if((context.getApplicationInfo().flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) == ApplicationInfo.FLAG_EXTERNAL_STORAGE)
    		{
    			appInstalledOnSDCard = true;
    		}
    	
    		return appInstalledOnSDCard;
	}
	
	/**
	 * It will copy a file from source to destination.
	 * @param context
	 * @param sourceFilePath
	 * @param destFilePath
	 * @param overwrite
	 */
	public static void copyFile(Context context, String sourceFilePath, String destFilePath, boolean overwrite)
	{
		File file = new File(sourceFilePath);
		if(!file.exists())
			return;
		
		file = new File(destFilePath);
		if(file.exists() && !overwrite)
			return;
		FileChannel inChannel = null;
		FileChannel outChannel = null;
		try {
			inChannel = new FileInputStream(sourceFilePath).getChannel();
			outChannel = new FileOutputStream(destFilePath).getChannel();
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (inChannel != null)
						inChannel.close();
				if (outChannel != null)
					outChannel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
//		InputStream in;
//		try {
//			in = new FileInputStream(sourceFilePath);
//	        OutputStream out = new FileOutputStream(destFilePath);
//	        
//	        // Copy the bits from instream to outstream
//	        byte[] buf = new byte[1024];
//	        int len;
//	        while ((len = in.read(buf)) > 0) {
//	            out.write(buf, 0, len);
//	        }
//	        in.close();
//	        out.close();
//		} catch (FileNotFoundException e) {
//			
//			e.printStackTrace();
//		} catch (IOException e) {
//			
//			e.printStackTrace();
//		}
		
	}
	
	/**
	 * This function get string from shared preferances. If it not
	 * find the any value for key then it return empty string.  
	 * @param context
	 * @param key
	 * @return
	 */
	public static String getStringFromSharedPreferancesForKey(Context context, String key)
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getClass().getSimpleName(), Context.MODE_PRIVATE);
		String data = sharedPreferences.getString(key, "");
		return data;
		
	}
	
	/**
	 * This function gets device unique Id 
	 * @param context
	 * @return
	 * 
	 */
	public static String getDeviceUniqueId(Context context) 
	{		
		return Secure.getString(context.getContentResolver(),Secure.ANDROID_ID);		
	}

	
	public static String getDeviceName(Context context)
	{
		String manufacturer = Build.MANUFACTURER;
		  String model = Build.MODEL;
		  if (model.startsWith(manufacturer)) 
		  {
		    return capitalize(model);
		  } 
		  else 
		  {
		    return capitalize(manufacturer) + " " + model;
		  }
		}
	
	private static String capitalize(String s) 
	{
		  if (s == null || s.length() == 0) {
		    return "";
		  }
		  char first = s.charAt(0);
		  if (Character.isUpperCase(first)) 
		  {
		    return s;
		  } else 
		  {
		    return Character.toUpperCase(first) + s.substring(1);
		  }
	}
	
	/**
	 * This function will set a string in shared preferances for a key.
	 * @param context
	 * @param key
	 * @param value
	 * @return
	 */
	public static boolean setStringInSharedPreferancesForKey(Context context, String key, String value)
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getClass().getSimpleName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		return editor.commit();
	}
	
	/**
	 * This function get int from shared preferances. If it not
	 * find the any value for key then it return 0.  
	 * @param context
	 * @param key
	 * @return
	 */
	public static int getIntFromSharedPreferancesForKey(Context context, String key)
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getClass().getSimpleName(), Context.MODE_PRIVATE);
		int data = sharedPreferences.getInt(key, 0);
		return data;
		
	}
	
	/**
	 * This function will set a int in shared preferances for a key.
	 * @param context
	 * @param key
	 * @param value
	 * @return
	 */
	public static boolean setIntInSharedPreferancesForKey(Context context, String key, int value)
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(context.getClass().getSimpleName(), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(key, value);
		return editor.commit();
	}
	
	/**
	 * I will return the width of any image stored in drawable directory in pixels. 
	 * @param context
	 * @param drawableId
	 * @return
	 */
	public static float getDrawableWidth(Context context, int drawableId)
	{
		BitmapDrawable bd=(BitmapDrawable) context.getResources().getDrawable(drawableId);
		return bd.getBitmap().getWidth();
	}
	
	/**
	 * I will return the height of any image stored in drawable directory in pixels.
	 * @param context
	 * @param drawableId
	 * @return
	 */
	public static float getDrawableHeight(Context context, int drawableId)
	{
		BitmapDrawable bd=(BitmapDrawable) context.getResources().getDrawable(drawableId);
		return bd.getBitmap().getHeight();
	}
	
	
	/**
	 * 
	 * Function to send email with subject and message to recipents
	 * @param context
	 * @param emailText Email body text
	 * @param emailSubject Email subject
	 * @param recipents list of recipients
	 */
	public static void sendEmail (Context context, String emailText, String emailSubject, String[] recipents)
	{
		/* Create the Intent */
		final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

		/* Fill it with Data */
		emailIntent.setType("text/html");
		if(recipents == null)
			recipents = new String[0];
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipents);
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, emailSubject);
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(emailText));

		/* Send it off to the Activity-Chooser */
		context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		
	}

	/**
	 * It will send email with attachment.
	 * @param context
	 * @param emailText
	 * @param emailSubject
	 * @param recipents 
	 * @param mimeType mime type of the file which needs to be send.
	 * @param uri The location of file on the memory.
	 */
	public static void sendEmail (Context context, String emailText, String emailSubject, String[] recipents, String mimeType, String uri)
	{
		/* Create the Intent */
		final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

		/* Fill it with Data */
		emailIntent.setType("text/html");
		if(recipents == null)
			recipents = new String[0];
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipents);
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, emailSubject);
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(emailText));
		
		if(mimeType != null && uri != null)
		{
			if(!mimeType.trim().equals("") && !uri.trim().equals(""))
			{
				emailIntent.setType(mimeType);
				File file = new File(uri);
				Uri uriObj =  Uri.fromFile(file);
				file = null;
				LHTLogs("FIle URI: " + uriObj.toString(), false);
				emailIntent.putExtra(Intent.EXTRA_STREAM, 	uriObj);
//				emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri));
				
			}else
				LHTLogs("Problem with URI",true);
		}else
			LHTLogs("Problem with URI",true);
		

		/* Send it off to the Activity-Chooser */
		context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		
	}


	/**
	 * Function to make a telephonic call from app
	 * @param context
	 * @param number
	 */
//	public static void makeCall(Context context, String number) {
//
//		// you have to add user permission in Manifest.
//		// <uses-permission android:name="android.permission.CALL_PHONE"/>
//		try
//		{
//			Intent callIntent = new Intent(Intent.ACTION_CALL);
//			callIntent.setData(Uri.parse("tel:" + number));// 123456789"));
//			context.startActivity(callIntent);
//			callIntent = null;
//		}
//		catch(Exception e)
//		{
//
//			Log.e("Dialing ", "Call failed", e);
//		}
//
//	}

	/**
	 * Method to convert pixels to point
	 * @param pixels
	 * @param dpi
	 * @return points
	 */
	public static float getPointsFromPixels(float pixels, float dpi) {
		
		float points = 72*(pixels/dpi);
		return points;
	}
	

//	/**
//	 * Method to convert pixels to point
//	 * @param pixels
//	 * @param dpi
//	 * @return points
//	 */
//	public static float getPixelsFromPoints(float point, float dpi) {
//
//		float pixels = (point*dpi)/72;
//		return pixels;
//	}
	

	/**
	 * We create this function to resolve the problem of 
	 * @param tableRow2 View
	 * @param editText2 Edit text
	 * @param context
	 */
	public static void transferFocusToEditTextFromView(View tableRow2,
			final EditText editText2, final Context context) {

		tableRow2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				editText2.setFocusable(true);
				editText2.requestFocus();

				  InputMethodManager imm = (InputMethodManager)
				  context.getSystemService(Context.INPUT_METHOD_SERVICE);

				  imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);

			}
		});

	}
	
	
//	/**
//	 * This function resize any bitmap
//	 * @param Source Bitmap Object
//	 * @param new height
//	 * @param new width
//	 * @return
//	 */
//
//	public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
//
//	    int width = bm.getWidth();
//	    int height = bm.getHeight();
//	    float scaleWidth = ((float) newWidth) / width;
//	    float scaleHeight = ((float) newHeight) / height;
//	    // CREATE A MATRIX FOR THE MANIPULATION
//	    Matrix matrix = new Matrix();
//	    // RESIZE THE BIT MAP
//	    matrix.postScale(scaleWidth, scaleHeight);
//
//	    // "RECREATE" THE NEW BITMAP
//	    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
//	    return resizedBitmap;
//	}


	/**
	 * Function to create rounded corner bitmaps
	 * @param radius Radius of round corners
	 * @param src Bitmap source
	 * @return
	 */
	public static Bitmap createRoundedCornerBitmap(float radius, Bitmap src) {
	    //Create a *mutable* location, and a canvas to draw into it
	    int width = src.getWidth();
	    int height = src.getHeight();
	    Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	    Canvas canvas = new Canvas(result);
	    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

	    RectF rect = new RectF(0, 0, width, height);
	    paint.setColor(Color.BLACK);
	    canvas.drawRoundRect(rect, radius, radius, paint);
	    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
	    canvas.drawBitmap(src, 0, 0, paint);
	    paint.setXfermode(null);

	    return result;
	}
//
//	public static double getCurrentLongitude(Context context) {
//
//		LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
//		Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//		double longitude = location.getLongitude();
//		return longitude;
//	}
//
//	public static double getCurrentLatitude (Context context) {
//
//		LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
//		Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//		double latitude = location.getLatitude();
//		return latitude;
//	}


	/**
	 * Function to validate email
	 * @param email
	 * @return
	 */
	public static boolean isEmailValid(String email) {
		
//		Pattern pattern = Pattern.compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}");
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	/**
	 * Function to get drawable element by ID
	 * @param imageName
	 * @param activityContext
	 * @return
	 */
	public static int getDrawableIDByName(String imageName, Context activityContext)
	{
		int drawableId = activityContext.getResources().getIdentifier(imageName, "drawable", activityContext.getPackageName());
		return drawableId;

	}

	/**
	 * Function to get resource by name
	 * @param name Resource name
	 * @param type Resource type
	 * @param context
	 * @return
	 */
	public static int getResourceByName(String name, String type, Context context)
	{
		int id = context.getResources().getIdentifier(name, type, context.getPackageName());
		return id;
	}


	/**
	 * Function to get drawable image by name
	 * @param name
	 * @param context
	 * @return
	 */
	public static Drawable getImageDrawableByName(String name, Context context)
	{
		return context.getResources().getDrawable(getDrawableIDByName(name, context));
	}


	/**
	 * Function to get drawable element style by name
	 * @param imageName
	 * @param context
	 * @return
	 */
	public static int getStyleIDByName(String imageName, Context context)
	{
		int drawableId = context.getResources().getIdentifier(imageName, "style", context.getPackageName());
		return drawableId;

	}

	/**
	 * Function to get layout by name
	 * @param layoutName
	 * @param activityContext
	 * @return
	 */
	public static int getLayoutByName(String layoutName, Context activityContext)
	{
		int layoutId = activityContext.getResources().getIdentifier(layoutName, "layout", activityContext.getPackageName());
		return layoutId;

	}

	/**
	 * Function to get ID by name
	 * @param idName
	 * @param activityContext
	 * @return
	 */
	public static int getIdByName(String idName, Context activityContext)
	{
		int Id = activityContext.getResources().getIdentifier(idName, "id", activityContext.getPackageName());
		return Id;

	}

	/**
	 * Function to get string by its name
	 * @param stringName
	 * @param activityContext
	 * @return
	 */
	public static int getStringByName(String stringName, Context activityContext)
	{
		int stringId = activityContext.getResources().getIdentifier(stringName, "string", activityContext.getPackageName());
		return stringId;

	}
	/**
	 * Returns array resource by name
	 * @param arrayName
	 * @param activityContext
	 * @return
	 */

	public static int getArrayResourceByName(String arrayName, Context activityContext)
	{

	  int arrayId = activityContext.getResources().getIdentifier(arrayName , "array", activityContext.getPackageName());
	  return arrayId;
	}

	/**
	 * Function to convert pixel into DP
	 * @param context
	 * @param pixel
	 * @return
	 */
	public static float getDpFromPixel(Context context,float pixel){
		final float scale = context.getResources().getDisplayMetrics().density;

		float mGestureThreshold = pixel / scale + 0.5f;
		return mGestureThreshold;
	}

	/**
	 * Function to convert DP into pixel
	 * @param context
	 * @param Dp
	 * @return
	 */
	public static float getPixelFromDp(Context context,float Dp){
		final float scale = context.getResources().getDisplayMetrics().density;

		float mGestureThreshold = Dp * scale + 0.5f;
		return mGestureThreshold;
	}

	/**
	 * Function to convert date string into date format
	 * @param date Date string
	 * @param sourceFormat 
	 * @param destinationFormat
	 * @return
	 */
	public static String convertDateStringInFormat(String date, String sourceFormat, String destinationFormat)
	{
		SimpleDateFormat sourceDateFormat = new SimpleDateFormat(sourceFormat);
		String formattedDate = "";
		try {
			Date dateObject = sourceDateFormat.parse(date);
			SimpleDateFormat destinationDateFormat = new SimpleDateFormat(destinationFormat);
			formattedDate = destinationDateFormat.format(dateObject);
			dateObject = null;
			destinationDateFormat = null;

		} catch (ParseException e) {
			e.printStackTrace();
		}
		sourceDateFormat = null;

		return formattedDate;
	}
	
	/**
	 * Function to convert date format to date according to time zone
	 * @param date
	 * @param sourceFormat
	 * @param timezone
	 * @return
	 */
	public static Date convertFormattedStringInDateByTimezone(String date, String sourceFormat, TimeZone timezone)
	{
		SimpleDateFormat sourceDateFormat = new SimpleDateFormat(sourceFormat);
		sourceDateFormat.setTimeZone(timezone);
		Date dateObject = null;
		try {
			dateObject = sourceDateFormat.parse(date);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		sourceDateFormat = null;

		return dateObject;
	}

	/**
	 * Function to convert date format string into date 
	 * @param date
	 * @param sourceFormat
	 * @return Date
	 */
	public static Date convertFormattedStringInDate(String date, String sourceFormat)
	{
		SimpleDateFormat sourceDateFormat = new SimpleDateFormat(sourceFormat);
		Date dateObject = null;
		try {
			dateObject = sourceDateFormat.parse(date);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		sourceDateFormat = null;

		return dateObject;
	}

	/**
	 * Function to return formatted date 
	 * @param date
	 * @param destinationFormat
	 * @return
	 */
	public static String convertDateObjectInFormat(Date date, String destinationFormat)
	{
		SimpleDateFormat destinationDateFormat = new SimpleDateFormat(destinationFormat);
		String formattedDate = destinationDateFormat.format(date);
		destinationDateFormat = null;
		return formattedDate;
	}

	/**
	 * Function to convert seconds into format of HH:MM:SS
	 * @param seconds
	 * @return
	 */
	public static String convertSecondsIntoHH_MM_SS(long seconds)
	{
		int displaySeconds = (int) (seconds % 60);
		int displayMinutes = (int) ((seconds/60) % 60);
		int displayHour = (int) (seconds / 3600);

		String displayHourString = String.format("%02d", displayHour);
		String displayMinString = String.format("%02d", displayMinutes);
		String displaySecString = String.format("%02d", displaySeconds);

//		if (seconds < 3600) {
//			return displayMinString + ":" + displaySecString;
//		}

		return displayHourString + ":" + displayMinString + ":" + displaySecString;
	}

/**
 * Function to hide keyboard in initial state of the activity
 * @param context
 */
	public static void hideSoftKeyboard(Context context)
	{
		((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	/***
	 * Function to hide keyboard in the running state of the activity
	 * @param context
	 * @param binder
	 */
	public static void hideSoftKeyboard(Context context, IBinder binder)
	{
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(binder, 0);
	}
	
	/**
	 * This method is to get json array from json object at a particular key
	 * @param jsonObject
	 * @param key
	 * @return data
	 */
	public static JSONArray getJSONArrayFromJSONObjectAtKey(JSONObject jsonObject, String key)
	{
		JSONArray data = null;
		try
		{
			data = jsonObject.getJSONArray(key);
		}
		catch (JSONException e) 
		{
//			Log.e(Utility.class.getSimpleName(), "Exception occured in getting JSONArray from JSONObject for key " + key, e);
		}
		return data;
	}

	/**
	 * Method to get string from json object at particular key
	 * @param jsonObject
	 * @param key
	 * @return data
	 */
	public static String getStringFromJSONObjectAtKey(JSONObject jsonObject, String key)
	{
		String data = null;
		data = jsonObject.optString(key);

		if (data != null && data.equals("null"))
			return null;

		return data;
	}
	

	/**
	 * Method to get object from json object at particular key
	 * @param jsonObject
	 * @param key
	 * @return data
	 */
	public static Object getObjectFromJSONObjectAtKey(JSONObject jsonObject, String key)
	{
		Object data = null;
		try
		{
			data = jsonObject.get(key);
		}
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting Object from JSONObject for key " + key, e);
		}
		return data;
	}
	
	/**
	 * Method to get string from json array at index value
	 * @param jsonArray
	 * @param index
	 * @return data
	 */
	public static String getStringFromJSONArray(JSONArray jsonArray, int index)
	{
		String data = null;
		try 
		{
			data = jsonArray.getString(index);
		}
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting string from JSONArray for index " + index, e);
		}
		return data;
	}


//	/**
//	 * Method to get integer value  from json array at particular index
//	 * @param jsonObject
//	 * @param key
//	 * @return data
//	 */
	public static int getIntFromJSONArray(JSONArray jsonArray, int index)
	{
		int data = -1;
		try 
		{
			data = jsonArray.getInt(index);
		}
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting int from JSONArray for index " + index, e);
		}
		return data;
	}

//	/**
//	 * Method to get boolean value from json array at particular index
//	 * @param jsonObject
//	 * @param key
//	 * @return data
//	 */
	public static boolean getBooleanFromJSONArray(JSONArray jsonArray, int index)
	{
		boolean data = false;
		try 
		{
			data = jsonArray.getBoolean(index);
		}
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting int from JSONArray for index " + index, e);
		}
		return data;
	}
	

//	/**
//	 * Method to get json array from json array at particular index value
//	 * @param jsonObject
//	 * @param key
//	 * @return data
	public static JSONArray getJSONArrayFromJSONArray(JSONArray jsonArray, int index)
	{
		JSONArray data = null;
		try
		{
			data = jsonArray.getJSONArray(index);
		} 
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting JsonArray from JSONArray for index " + index, e);
		}
		return data;
	}
	
	/**
	 * This method get double from json object at key 
	 * @param jsonObject
	 * @param key
	 * @return
	 */
	public static double getDoubleFromJSONObjectAtKey(JSONObject jsonObject, String key)
	{
		double data = -1;
		try
		{
			data = jsonObject.getDouble(key);
		}
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting Double from JSONObject for key " + key, e);
		}
		return data;
	}


	/**
	 * Method to get integer from json object at particular key
	 * @param jsonObject
	 * @param key
	 * @return data
	 */
	public static int getIntFromJSONObjectAtKey(JSONObject jsonObject, String key)
	{
		int data = -1;
		try 
		{
			data = jsonObject.getInt(key);
		} 
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting integer from JSONObject for key " + key, e);
		}
		return data;
	}


	/**
	 * Method to get boolean from json object at particular key
	 * @param jsonObject
	 * @param key
	 * @return data
	 */
	public static boolean getBoolFromJSONObjectAtKey(JSONObject jsonObject, String key)
	{
		boolean data = false;
		try
		{
			data = jsonObject.getBoolean(key);
		} 
		catch (JSONException e)
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting boolean from JSONObject for key " + key, e);
		}
		return data;
	}

	/**
	 * This function checks the value on the sucess key is true or not. if it is true the function 
	 * assume it as a valid response other wise it return false. 
	 * @param jsonObject
	 * @return
	 */
	public static boolean validateJSONObject(JSONObject jsonObject)
	{
		boolean success = getBoolFromJSONObjectAtKey(jsonObject, "success");
		if (!success)
			if (jsonObject.has("message")) 
			{
				Utility.LHTLogs(getJSONArrayFromJSONObjectAtKey(jsonObject ,"message").toString(),true);
			}
		return success;
	}


//	/**
//	 * Method to get json object from json object at particular key
//	 * @param jsonObject
//	 * @param key
//	 * @return data
//	 */
	public static JSONObject getJSONObjectFromString(String JSONString)
	{
		JSONObject data = null;
		try
		{
			data = new JSONObject(JSONString);
		} 
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting JSONObject from JSONstring " + JSONString, e);
		}
		return data;
	}
	
	/**
	 * Method to get json array from json array from string
	 * @param JSONString
	 * @return
	 */
	public static JSONArray getJSONArrayFromString(String JSONString)
	{
		JSONArray data = null;
		try {
			data = new JSONArray(JSONString);
		} catch (JSONException e) {
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting JSONArray from JSONstring " + JSONString, e);
		}
		return data;
	}


	/**
	 * Method to get json object from json object at particular key
	 * @param jsonObject
	 * @param key
	 * @return data
	 */
	public static JSONObject getJSONObjectFromJSONObjectAtKey(JSONObject jsonObject, String key)
	{
		JSONObject data = null;
		try
		{
			data = jsonObject.getJSONObject(key);
		}
		catch (JSONException e) 
		{
//			Log.e(Utility.class.getSimpleName(), "Exception occured in getting JSONObject from JSONObject for key " + key, e);
		}
		return data;
	}


//	/**
//	 * Method to get json object from json object from array at particular index
//	 * @param jsonObject
//	 * @param key
//	 * @return data
//	 */
	public static JSONObject getJSONObjectFromJSONArrayAtIndex(JSONArray jsonArray, int index)
	{
		JSONObject data = null;
		try 
		{
			data = jsonArray.getJSONObject(index);
		} 
		catch (JSONException e) 
		{
			Log.e(Utility.class.getSimpleName(), "Exception occured in getting JSONObject from JSONArray At index " + index, e);
		}
		return data;

	}
	

	/**
	 * Function to scale image according to device width
	 * @param context
	 * @param imageWidth
	 * @return
	 */
	public static int getImageScalingFactor(Context context, int imageWidth)
	{
    	Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = Double.valueOf(width)/Double.valueOf(imageWidth);// new Double(width)/new Double(imageWidth);
        val = val * 100d;
        return val.intValue();
    }
	
	
	/**
	 *  This method will call for all the log statements in the project so that all logs can be desable by one place
	 * 	You can perform any operations on log msgs here
	 */
	public static void LHTLogs(String logMsg, boolean isErrorLog)
	{
//		if (isErrorLog)
//			Log.e("LHT Logs Errror", logMsg);
//		else
//			Log.v("LHT Logs", logMsg);

	}
	
	/**
	 * This function edit the bitmap and make its corner rounded. This function takes radius in pixels and calculate the result according 
	 * to display pixel density.    
	 * @param context
	 * @param input
//	 * @param cornerRadiousInPixels
	 * @param isRoundedTL
	 * @param isRoundedTR
	 * @param isRoundedBL
	 * @param squareBR
	 * @return
	 */
	public static Bitmap getRoundedCornerBitmap(Context context, Bitmap input, int cornerRadiusInPixels , boolean isRoundedTL, boolean isRoundedTR, boolean isRoundedBL, boolean squareBR  ) {

		int w = input.getWidth() ;
		int h = input.getHeight() ;
		
	    Bitmap output = Bitmap.createBitmap(w, h, Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);
	    final float densityMultiplier = context.getResources().getDisplayMetrics().density;

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, w, h);
	    final RectF rectF = new RectF(rect);

	    //make sure that our rounded corner is scaled appropriately
	    final float roundPx = cornerRadiusInPixels*densityMultiplier;

	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

	    //draw rectangles over the corners we want to be square
	    if (!isRoundedTL ){
	        canvas.drawRect(0, 0, w/2, h/2, paint);
	    }
	    if (!isRoundedTR ){
	        canvas.drawRect(w/2, 0, w, h/2, paint);
	    }
	    if (!isRoundedBL ){
	        canvas.drawRect(0, h/2, w/2, h, paint);
	    }
	    if (!squareBR ){
	        canvas.drawRect(w/2, h/2, w, h, paint);
	    }

	    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
	    canvas.drawBitmap(input, 0,0, paint);
	    canvas = null;
	    
	    return output;
	}
	
	/**
	 * Method to create folder on SD card
	 * @param path
	 */
	public static void createFolderOnSdCard(String path)
	{
		
		File dir = new File(path);
		if(!dir.exists())
	    {
	        if(dir.mkdir()); //directory is created;

	    }
		
	}
	
//	public static Object[] removeNullElementsFromArray(Object sourceArray[])
//	{
//		int size = sourceArray.length;
//		for (int index = 0; index < sourceArray.length; index++) {
//			if (sourceArray[index] == null) 
//				size--;
//		}
//		if (size == sourceArray.length) 
//			return sourceArray;
//		Object resultObj[] = new Object[size];
//		for (int i = 0, j = 0; i < sourceArray.length; i++){
//			if (sourceArray[i] != null) 
//				resultObj[j++] = sourceArray[i];
//			
//		}
//		return resultObj;
//	}
	
	/**
	 * This function copy the file from projects asset directory to sdcard. 
	 * @param context
	 * @param assetFilePath
	 * @param SDCardFilePath
	 * @return
	 */
	public static boolean copyFileFromAssetToSDCard(Context context, String assetFilePath, String SDCardFilePath)
	{
		try{
			InputStream myInput = context.getAssets().open(assetFilePath);
	    	
			OutputStream myOutput = new FileOutputStream(SDCardFilePath);
	    	 
	    	byte[] buffer = new byte[1024]; 
	    	int length; 
	    	while ((length = myInput.read(buffer))>0)
	    	{ 
	    		myOutput.write(buffer, 0, length); 
	    	}
	    	 
	    	//Close the streams 
	    	myOutput.flush(); 
	    	myOutput.close(); 
	    	myInput.close();
	    	return true;
		}catch (IOException e){
		      //Logging exception
		}
		return false;
	}
	
	/**
	 * This function will show the alert with user preferences of title and message with icon of alert.
	 * @param context
	 * @param title
	 * @param alertMsg
	 * @param icon
	 */
	public static void showAlert(Context context,
			String title, String alertMsg,String buttonText, int icon) 
	{
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(alertMsg);
		alertDialog.setIcon(icon);
		alertDialog.setCancelable(false);
		alertDialog.setButton(-2, buttonText,new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int which) 
			{
			dialog.dismiss();
			}});
		alertDialog.show();
		TextView messageView = (TextView) alertDialog.findViewById(android.R.id.message);
		messageView.setGravity(Gravity.CENTER);
		alertDialog = null;
	}
	
	/**
	 * This function check if the device is connected to any network connection or not.
	 * @param context
	 * @return
	 */
	public static boolean isNetworkConnected(Context context) 
	{
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null;	
	 }
	
	/**
	 * Returns the bitmap with reduced size in proportion
	 * @param thumbnailPath - the path where bitmap is stored
	 * @param thumbnailSize - size of the thumbnail to be returned after p 
	 * @return
	 */
	public static Bitmap getThumbnailFromPath(File thumbnailPath, int thumbnailSize)
	{
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither=true;
        BitmapFactory.decodeFile(thumbnailPath.getAbsolutePath(), onlyBoundsOptions);
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;
        
        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;
        double ratio = (originalSize > thumbnailSize) ? (originalSize / thumbnailSize) : 1.0;
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither=true;
        Bitmap bitmap = BitmapFactory.decodeFile(thumbnailPath.getAbsolutePath(), bitmapOptions);
        
        int rotationAngle = getExifRotationAngle(thumbnailPath.getAbsolutePath());
		
        if(rotationAngle <= 0)
        	return bitmap;

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, 0, 0);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        
        bitmap.recycle();
        bitmap = null;
        
		return rotatedBitmap;
	}
	
	/**
	 * Method to create sample for image of avatar after cropping it
	 * @param ratio
	 * @return
	 */
	public static int getPowerOfTwoForSampleRatio(double ratio)
	{
        int k = Integer.highestOneBit((int)Math.floor(ratio));
        if(k==0)
        	return 1;
        else 
        	return k;
    }
	
	/**
	 * Method to get file count from directory
	 * @param file
	 * @return file length
	 */
	public static int getFileCountFromDirectory(File file)
	{
		if(file==null)
			return 0;
		if(file.isDirectory())
		{
			return file.list().length;
		}
		
		return 0;
	}
	
	/**
	 * Method to round double to 2 decimals
	 * @param val
	 * @return
	 */
	public static double roundTo2Decimals(double val) 
	{
		if(val==0)
			return 0;
       DecimalFormat df2 = new DecimalFormat("###.##");
       return Double.valueOf(df2.format(val));
	}
	
	/**
	 * Method to get string representation of rounded values upto two decimals
	 * @param val
	 * @return
	 */
	public static String getStringRepresentationOfRoundedValuesUptoTwoDecimalPlaces(double val)
	{
		if(val==0)
			return "";
	    return String.format("%.2f", val);
	}
	
	/**
	 * Method to round the value upto one decimal places
	 * @param val
	 * @return
	 */
	public static double roundToOneDecimalPlace(double val)
	{
		DecimalFormat df2 = new DecimalFormat("###.#");
	       return Double.valueOf(df2.format(val));
	}

	
	
	/**
	 * Method get round off value to nearest integer
	 * @param value
	 * @return
	 */
	public static int getRoundedOffValueToNearestWholeNumber(double value)
	{
		int newValue=(int) Math.round(value);
		return newValue;
	}
		
	/**
	 * Method to get current time stamp for file name
	 * @return
	 */
	public static String getCurrentTimeStampForFileName()
	{
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssS").format(new Date());
		return timeStamp;
	}	
	
	/**
	 * Returns the quadrant, the coordinates belong to,according to x and y values supplied
	 * @param x
	 * @param y
	 * @return
	 */
	public static int getQuadrant(double x, double y) 
	{
		if (x >= 0) {
			return y >= 0 ? 1 : 4;
		} else {
			return y >= 0 ? 2 : 3;
		}
	}
	
	/**
	 * Method to convert stream to string
	 * @param is
	 * @return
	 * @throws Exception
	 */
	public static String convertStreamToString(InputStream is) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line+"\n");
		}
		is.close();
		return sb.toString();
	}
	
	
	/**
	 * Returns the Location Name
	 * @return
	 */
	public static void getLocationName(Context context,double latitude,double longitude,String callBackMethodName)
	{
		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
	    try {
	        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
	        if (callBackMethodName == null || callBackMethodName.equals(""))
				return;
			try 
			{
				Method callBackMethod =context.getClass().getDeclaredMethod(callBackMethodName,List.class);
				callBackMethod.invoke(context,addresses);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	        
	    } catch (IOException e) {
	        
	        e.printStackTrace();
	        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
	    }
	}
	
	/**
	 * Check for GPS feature available or not
	 * @param context
	 * @return
	 */
	public static boolean isLocationFeatureExist(Context context)
	{
		return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION);
	}

	/**
	 * Method to get package information 
	 * @param context
	 * @return
	 */
	public static PackageInfo getPackageInfo(Context context)
	{
		PackageInfo packageInfo = null;
		try {
			
			packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			
		} catch (NameNotFoundException e) {
			
			e.printStackTrace();
		}
		
		return packageInfo;
		
	}
	
	/**
	 * Method to convert date to calender
	 * @param date
	 * @return
	 */
	public static Calendar convertDateToCalendar(Date date)
	{
		if(date == null)
			return Calendar.getInstance();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}
	
	/**
	 * Method to get formatted date 
	 * @param unixSeconds
	 * @param format
	 * @return
	 */
	public static String getFormattedDate(long unixSeconds, String format)
	{
		Date date = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);//"yyyy-MM-dd HH:mm:ss z"); // the format of your date
		Calendar cal = Calendar.getInstance();
		TimeZone tz = cal.getTimeZone();
		sdf.setTimeZone(tz);
		String formattedDate = sdf.format(date);
		return formattedDate;
	}
	
	/**
	 * Method to get mime type from call
	 * @param file
	 * @return
	 */
	public static String getMimeTypeForFile(File file) 
	{
		if(!file.exists())
			return null;
		
		String type = null;
	    String extension = MimeTypeMap.getFileExtensionFromUrl(file.getAbsolutePath());
	    if(extension == null || extension.equals(""))
	    	extension = getExtensionUsingDotCharacter(file.getName());
	    if (extension != null && !extension.equals("")) 
	    {
	        MimeTypeMap mime = MimeTypeMap.getSingleton();
	        type = mime.getMimeTypeFromExtension(extension);
	    }
	    return type;
	}
	
	
	public static String getExtensionUsingDotCharacter(String name)
	{
		String ext = name.substring(name.lastIndexOf(".")+1);
		return ext;
	}

	/**
	 * Function to rotate bitmap acccording to EXIF data
	 * @param thumbnailPath Path of the bitmap where it is located
	 * @param compressedOutput A number ranging between 0 - 100, specifies the compression. 100 meaning store it without compression. Lower the value higher will be the compression
	 * @throws IOException - Throws IO Exception if file is not found
	 */
	@SuppressWarnings("rawtypes")
	public static void rotateBitmapIfRequired(final Context context, final Object targetObject, final File thumbnailPath,final  int compressedOutput, final Class className, final String callBackMethodName)
	{
		new AsyncTask<Void, Void, String>()
		{
			Dialog dialog;
			protected void onPreExecute() 
			{
				dialog = ProgressDialog.show(context, "", "Loading...", true, false);
			}
			
			@Override
			protected String doInBackground(Void... params) 
			{
				String absolutePath = thumbnailPath.getAbsolutePath();
				try
				{
			        int rotationAngle = getExifRotationAngle(absolutePath);
			
			        if(rotationAngle <= 0)
			        	return absolutePath;
			
			        BitmapFactory.Options bitmapBoundsOptions = new BitmapFactory.Options();
			        Bitmap bmp = BitmapFactory.decodeFile(absolutePath, bitmapBoundsOptions);
			        
			        Matrix matrix = new Matrix();
			        matrix.setRotate(rotationAngle, 0, 0);
			        Bitmap rotatedBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getHeight(), bmp.getWidth(), matrix, true);
			        
			        bmp.recycle();
			        bmp = null;
			        compressBitmap(rotatedBitmap, thumbnailPath, compressedOutput);
			        rotatedBitmap.recycle();
			        rotatedBitmap = null;
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				return absolutePath;
			}
			
			
			protected void onPostExecute(String result) 
			{
				Method method;
				try
				{
					method = className.getDeclaredMethod(callBackMethodName, String.class);
					method.invoke(targetObject, result);
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					
				}
				dialog.dismiss();
				dialog = null;
			}
		}.execute();
	}
	
	public static int getExifRotationAngle(String absolutePath)
	{
		int rotationAngle = -1;
		
		ExifInterface exif;
		try {
			exif = new ExifInterface(absolutePath);
			String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
			int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
			
			rotationAngle = 0;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_90) 
				rotationAngle = 90;
			else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) 
				rotationAngle = 180;
			else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) 
				rotationAngle = 270;
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}

        return rotationAngle;
	}
	
	public static boolean compressBitmap(Bitmap bitmap, File newFile, int compressedOutput) 
	{
		if(!newFile.exists())
			return false;
		FileOutputStream out = null;
		try 
		{
			out = new FileOutputStream(newFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, compressedOutput, out);
			out.close();
			return true;
		}
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
		return false;
	}

	
	public static int getSystemTimeFormat(Context context)
	{
		int timeFormat = 12;
		try
		{
		    timeFormat = Settings.System.getInt(context.getContentResolver(), Settings.System.TIME_12_24);
		}
		catch (SettingNotFoundException e) 
		{
//		    e.printStackTrace();
		}

		return timeFormat;
	}
	
	
//	public static boolean isYesterday(Date then, Date now)
//	{
//		Calendar currentTime = Calendar.getInstance();
//		Calendar savedTime = Calendar.getInstance();
//		currentTime.setTime(now);
//		savedTime.setTime(then);
//		if(then.getYear()==now.getYear())
//		{
//			if(currentTime.get(Calendar.DAY_OF_YEAR)-savedTime.get(Calendar.DAY_OF_YEAR)==1)
//				return true;
//		}
//		else
//		{
//			if(currentTime.get(Calendar.MONTH)==1 && savedTime.get(Calendar.MONTH)==12)
//			{
//				if(currentTime.get(Calendar.DAY_OF_MONTH)==1 && savedTime.get(Calendar.DAY_OF_MONTH)==31)
//					return true;
//			}
//		}
//		return false;
//	}
	
	
	public static String getStringFromHtmlText(String htmlText)
	{
		String lang = "en";
		String charset = "utf-8";

		String completeHtmlString = "<!DOCTYPE html><html lang=" + lang + "><head><meta charset=" + charset + " /><title></title></head><body>"
		 	+ htmlText + "</body></html>";

		return completeHtmlString;
	}
	
	public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable!=null) 
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else 
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }
	
	public static boolean saveBitmap(Bitmap bitmap, File newFile, int compressedOutput) 
	{
		FileOutputStream out = null;
		try 
		{
			out = new FileOutputStream(newFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, compressedOutput, out);
			out.close();
			return true;
		}
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
		return false;
	}
	
	
	public static float getDistanceBetweenTwoLatLongs(Double lat1, Double lng1, Double lat2, Double lng2) {
	    double earthRadius = 3958.75;
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    int meterConversion = 1609;
	    return new Float(dist * meterConversion).floatValue();
	}	
	
	/**
	 * Method for showing toast
	 * @param context
	 * @param message
	 * @param duration
	 */
	public static void LHTToast(Context context, String message, int duration){
		Toast.makeText(context, message, duration).show();
	}
	
	public static SpannableString getClickableSpannableString(String text, String clickableText, final OnClickListener listener)
	{
		SpannableString link = new SpannableString(text);
		  
		ClickableSpan linkSpan = new ClickableSpan()
		{
			@Override 
			public void onClick(View widget){
				listener.onClick(widget);
		    }
		    
			@Override 
			public void updateDrawState(TextPaint ds){
				ds.setUnderlineText(true);
				ds.setFakeBoldText(true);
		    }
		};
		
		
		link.setSpan(linkSpan, text.indexOf(clickableText), text.indexOf(clickableText) + clickableText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		return link;
	}
	
	/**
	 * This method returns the total height of the listview provided.
	 * @param listView
	 * @return height of the listview
	 */
	public static int getTotalHeightofListView(ListView listView) {

	    ListAdapter mAdapter = listView.getAdapter();

	    int totalHeight = 0;

	    for (int i = 0; i < mAdapter.getCount(); i++) {
	        View mView = mAdapter.getView(i, null, listView);

	        mView.measure(
	                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),

	                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

	        totalHeight += mView.getMeasuredHeight();
	        
	    }
	    return totalHeight;
	    }
}